﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;
using System;

namespace TDTK {
	
	
	public class ResourceManager : MonoBehaviour {
		
		public delegate void RscChangedHandler(List<float> changedValue);
		public static event RscChangedHandler onRscChangedE;


        public static event RscChangedHandler onRscIncomeChangedE;

        public bool carryFromLastScene=false;
		private static List<float> lastLevelValueList=new List<float>();
		private static List<float> initialValueList=new List<float>();
		
		public bool enableRscGen=false;
		public List<float> rscGenRateList=new List<float>();
        
        public List<float> rscMaxRateList = new List<float>();

        public List<Rsc> rscList=new List<Rsc>();
		
		public static ResourceManager instance;
        
		
		void Awake(){
			if(instance!=null)
                instance =this;
        }
		
		public void Init(bool ApplyaMultiplier = false)
        {	//to match the rscList with the DB
			instance=this;

           // rscList = ResourceDB.Load();

            if (ApplyaMultiplier)
            {
                if (CardAndLevelSelectPanel.BigPortal)
                {
                    rscList[0].value = (float)System.Math.Floor(rscList[0].value * Mathf.Pow(GameControl.instance.RscStartMultiplier, (float)CardAndLevelSelectPanel.LevelDifficultySelect)*10)/10f;
                }
                else
                {
                    rscList[0].value = (float)System.Math.Floor(rscList[0].value * GameControl.instance.RscStartMultiplier*10)/10f;
                }

                List<Card> CardTower = PlayerManager.CardsList;

                if (PlayerManager.CardsList != null)
                {
                    float min = 9999;
                    float cost;
                    for (int i = 0; i < CardTower.Count; i++)
                    {
                        if (CardTower[i].disableInBuildManager == false)
                        {
                            cost = CardTower[i].GetCost()[0];
                            if (min > cost)
                            {
                                min = cost;
                            }
                        }
                    }
                    if (min > rscList[0].value)
                        rscList[0].value = min;
                }

            }



            for (int i = 0; i < rscList.Count; i++)
            {
                rscList[i].valueSecurity = (float)Math.Floor(rscList[i].value * 10)/ 10f;
            }

            rscList[0].valueSecurity *= (1f + LevelModification.procentUpMoney) * (1f + StarsPerk.ProcentUpMoney);

           // Debug.LogWarning("maxR:" + rscMaxRateList[1] + "+" + StarsPerk.AddManaMax);
            rscMaxRateList[1] += StarsPerk.AddManaMax;
            if (StarsPerk.ProcentRegenMana > 0)
                rscGenRateList[1] *= (1f + StarsPerk.ProcentRegenMana);
           // Debug.LogWarning("gen:" + rscGenRateList[1] + "*" + StarsPerk.ProcentRegenMana);

            if (carryFromLastScene)
            {
				for(int i=0; i<lastLevelValueList.Count; i++)
                    rscList[i].valueSecurity = lastLevelValueList[i];
			}
			
			for(int i=0; i<rscList.Count; i++)
                initialValueList.Add(rscList[i].valueSecurity);
			
			if(enableRscGen)
                StartCoroutine(RscGenRoutine());
		}
		
		void OnEnable(){
		//	GameControl.onGameOverE += OnGameOver;
		}
		void OnDisable(){
		//	GameControl.onGameOverE -= OnGameOver;
		}

#if UNITY_EDITOR
        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.KeypadPlus))
            {
                GainResource(new List<float>() { 1, 0 });
            }

            if (Input.GetKeyDown(KeyCode.KeypadMinus))
            {
                GainResource(new List<float>() { 0, 1 });
            }
        }
#endif
        void OnGameOver(bool flag){
			if(!flag) return;
			
			lastLevelValueList=new List<float>();
			for(int i=0; i<rscList.Count; i++) lastLevelValueList.Add(rscList[i].valueSecurity);
		}
		public static void OnRestartLevel(){
			lastLevelValueList=new List<float>(initialValueList);
		}
		
		
		IEnumerator RscGenRoutine()
        {
            MyLog.Log("Start Regen");
            List<float> temp=new List<float>();
			for(int i=0; i<rscList.Count; i++) temp.Add(0);
			
			while(true)
            {
                if (GameControl.IsGameStarted() == false)
                {
                    yield return null;
                    continue;
                }


                yield return new WaitForSeconds(1);
                              

                List<float> perkRegenRate=PerkManager.GetRscRegen();
                List<float> perkPercentRegenRate = PerkManager.GetRscPercentRegen();
				List<float> valueList=new List<float>();
				
				bool increased=false;
				
				for(int i=0; i<rscList.Count; i++)
                {
					temp[i]+=(rscGenRateList[i]+perkRegenRate[i])*(1+ perkPercentRegenRate[i]);
					
					valueList.Add(0);
					
					if(temp[i]>=1)
                    { 
						while(temp[i]>=1)
                        {
							valueList[i]+=1;
							temp[i]-=1;
						}
						increased=true;
					}
				}
				if(increased) GainResource(valueList);
			}
		}
		
		
		
		public static int GetResourceCount(){
			return instance.rscList.Count;
		}

        private static float[] buffRscArray;
		public static float[] GetResourceArray(){
            if(buffRscArray==null)
                buffRscArray = new float[instance.rscList.Count];
            for (int i = 0; i < instance.rscList.Count; i++)
            {
                buffRscArray[i] = instance.rscList[i].valueSecurity;
            }
            return buffRscArray;
		}

        private static string[] buffRscNameArray;
        public static string[] GetResourceNameArray()
        {
            if (buffRscNameArray == null)
                buffRscNameArray = new string[instance.rscList.Count];
            for (int i = 0; i < instance.rscList.Count; i++)
            {
                buffRscNameArray[i] = instance.rscList[i].name;
            }
            return buffRscNameArray;
        }


        private static Sprite[] buffRscIconArray;
        public static Sprite[] GetResourceIconArray()
        {
            if (buffRscIconArray == null)
                buffRscIconArray = new Sprite[instance.rscList.Count];
            for (int i = 0; i < instance.rscList.Count; i++)
            {
                buffRscIconArray[i] = instance.rscList[i].icon;
            }
            return buffRscIconArray;
        }

        public static int HasSufficientResource(List<float> rscL){ return instance._HasSufficientResource(rscL); }
		public int _HasSufficientResource(List<float> rscL){
			if(rscList.Count!=rscL.Count){
               for (int i = 0; i < rscL.Count; i++)
                    if (rscList[i].valueSecurity < rscL[i])
                        return i;
                    Debug.Log("error, resource number doesnt match!    "+rscList.Count+"     "+rscL.Count);
				return -99;
			}
			for(int i=0; i<rscList.Count; i++){
				if(rscList[i].valueSecurity<rscL[i]) return i;
			}
			return -1;
		}
		
		public static void SpendResource(List<float> rscL){ instance._GainResource(rscL, null, false, -1); }
		public static void GainResource(List<float> rscL, List<float> mulL=null, bool useMul=true){ instance._GainResource(rscL, mulL, useMul); }
		public void _GainResource(List<float> rscL, List<float> mulL=null, bool useMul=true, float sign=1f){
			if(rscList.Count!=rscL.Count){
				MyLog.LogWarning("GainResource error, resource number doesnt match!");
				return;
			}
			
			//if this is gain, apply perks multiplier
			if(sign==1 && useMul){
				List<float> multiplierL=PerkManager.GetRscGain();
				
				if(mulL!=null){
					for(int i=0; i<multiplierL.Count; i++) multiplierL[i]+=mulL[i];
				}
				
				for(int i=0; i<multiplierL.Count; i++) rscL[i]=((float)rscL[i]*(1f+multiplierL[i]));
			}
			
			for(int i=0; i<rscList.Count; i++)
            {
				rscList[i].valueSecurity=Mathf.Max(0, rscList[i].valueSecurity +rscL[i]*sign);
                if(rscMaxRateList.Count>i && rscMaxRateList[i]>0)
                {
                    rscList[i].valueSecurity = Mathf.Min(rscMaxRateList[i], rscList[i].valueSecurity);
                }
			}
			
			if(onRscChangedE!=null) onRscChangedE(rscL);
		}


        public static void GainIncomeResource(List<float> rscL) { instance._GainIncomeResource(rscL); }
        public void _GainIncomeResource(List<float> rscL)
        {
            if (rscList.Count != rscL.Count)
            {
                Debug.Log("error, resource number doesnt match!");
                return;
            }

            for (int i = 0; i < rscList.Count; i++)
            {
                rscList[i].valueSecurity = Mathf.Max(0, rscList[i].valueSecurity + rscL[i] );
                if (rscMaxRateList.Count > i && rscMaxRateList[i] > 0)
                {
                    rscList[i].valueSecurity = Mathf.Min(rscMaxRateList[i], rscList[i].valueSecurity);
                }
            }

            if (onRscIncomeChangedE != null) onRscIncomeChangedE(rscL);
        }





        //not in use at the moment
        public static void NewSceneNotification(){
			//resourcesA=resourceManager.resources;
		}
		public static void ResetCummulatedResource(){
			//for(int i=0; i<resourcesA.Length; i++){
			//	resourcesA[i].value=0;
			//}
		}
		
		
		
		
	}

}
