﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;
using DigitalRuby.LightningBolt;

namespace TDTK
{

    public enum _ShootObjectType { Projectile, Missile, Beam, Effect, TimerBomb, AbilityBomb, FPSProjectile, FPSBeam, FPSEffect }

    public class ShootObject : MonoBehaviour
    {
        public delegate void HitHengler(ShootObject shootObject, AttackInstance attackInstance);
        public static event HitHengler OnHitE;

        public _ShootObjectType type;

        public float speed = 5;
        public float beamDuration = .5f;

        private Transform shootPoint;

        private bool hit = false;
		public bool isLightning = false;
		public bool isMortar = false;

        public bool autoSearchLineRenderer = true;
        public List<LineRenderer> lineList = new List<LineRenderer>();
        private List<TrailRenderer> trailList = new List<TrailRenderer>();
		private List<LightningBoltScript> lightningList = new List<LightningBoltScript>();


        public GameObject shootEffect;
        public GameObject hitEffect;

        public GameObject Splash;

        public float TimerBombLife;

        public Ability abilityBomb;

        private AttackInstance attInstance;

        private GameObject thisObj;
        private Transform thisT;

        void Awake()
        {
            thisObj = gameObject;
            thisT = transform;

            thisObj.layer = LayerManager.LayerShootObject();

            if (autoSearchLineRenderer)
            {
                LineRenderer[] lines = thisObj.GetComponentsInChildren<LineRenderer>(true);
                for (int i = 0; i < lines.Length; i++) lineList.Add(lines[i]);
            }

			LightningBoltScript[] lightnings = thisObj.GetComponentsInChildren<LightningBoltScript>(true);
			for (int i = 0; i < lightnings.Length; i++) lightningList.Add(lightnings[i]);


            TrailRenderer[] trails = thisObj.GetComponentsInChildren<TrailRenderer>(true);
            for (int i = 0; i < trails.Length; i++) trailList.Add(trails[i]);

            if (type == _ShootObjectType.FPSProjectile)
            {
                SphereCollider sphereCol = GetComponent<SphereCollider>();
                if (sphereCol == null)
                {
                    sphereCol = thisObj.AddComponent<SphereCollider>();
                    sphereCol.radius = 0.15f;
                }
                hitRadius = sphereCol.radius;
            }

            if (shootEffect != null) ObjectPoolManager.New(shootEffect);
            if (hitEffect != null) ObjectPoolManager.New(hitEffect);
        }

        void Start()
        {

        }

        void OnEnable()
        {
            //for (int i = 0; i < trailList.Count; i++)
            //    if (trailList[i] != null)
            //         StartCoroutine(ClearTrail(trailList[i],trailList[i].time));
        }
        void OnDisable()
        {

        }







        public void Shoot(AttackInstance attInst = null, Transform sp = null)
        {
            switch(type)
            {
                case _ShootObjectType.TimerBomb:
                    StartCoroutine(TimerBombRoutine());
                    break;
                case _ShootObjectType.AbilityBomb:

                    StartCoroutine(AbilityBombRoutine());
                    break;
                default:
                    if (attInst.tgtUnit == null || (attInst.tgtUnit as Unit).GetTargetT() == null)
                    {
                        ObjectPoolManager.Unspawn(thisObj);
                        return;
                    }

                    break;
            }

            

            attInstance = attInst;
            target = attInstance.tgtUnit as Unit;
            if (target != null)
            {
                targetPos = target.GetTargetT().position;
                hitThreshold = Mathf.Max(0.1f, target.hitThreshold);
            }
            else
            {
                targetPos = thisT.position;
            }

            shootPoint = sp;
            if (shootPoint != null)
                thisT.rotation = shootPoint.rotation;

            if (shootEffect != null)
                ObjectPoolManager.Spawn(shootEffect, thisT.position, thisT.rotation);

            hit = false;

            switch(type)
            {
                case _ShootObjectType.Projectile:
                    StartCoroutine(ProjectileRoutine());
                    break;
                case _ShootObjectType.Beam:
                    StartCoroutine(BeamRoutine());
                    break;
                case _ShootObjectType.Missile:
                    StartCoroutine(MissileRoutine());
                    break;
                case _ShootObjectType.Effect:
                    StartCoroutine(EffectRoutine());
                    break;
            }
        }



        public void ShootFPS(AttackInstance attInst = null, Transform sp = null)
        {
            shootPoint = sp;
            if (shootPoint != null) thisT.rotation = shootPoint.rotation;

            if (shootEffect != null) ObjectPoolManager.Spawn(shootEffect, thisT.position, thisT.rotation);

            hit = false;
            attInstance = attInst;
            if (type == _ShootObjectType.FPSProjectile) StartCoroutine(FPSProjectileRoutine());
            if (type == _ShootObjectType.FPSBeam) StartCoroutine(FPSBeamRoutine(sp));
            if (type == _ShootObjectType.FPSEffect) StartCoroutine(FPSEffectRoutine());
        }

        IEnumerator TimerBombRoutine()
        {
            yield return new WaitForSeconds(TimerBombLife);
            Hit();

            yield break;
        }
        IEnumerator AbilityBombRoutine()
        {

            if (shootEffect != null) Instantiate(shootEffect, thisT.position, thisT.rotation);

            float timeShot = Time.time;

            //make sure the shootObject is facing the target and adjust the projectile angle
            thisT.LookAt(targetPos);
            float angle = Mathf.Min(1, Vector3.Distance(thisT.position, targetPos) / maxShootRange) * maxShootAngle;
            //clamp the angle magnitude to be less than 45 or less the dist ratio will be off
            thisT.rotation = thisT.rotation * Quaternion.Euler(-angle, 0, 0);

            Vector3 startPos = thisT.position;
            float iniRotX = thisT.rotation.eulerAngles.x;

            float y = Mathf.Min(targetPos.y, startPos.y);
            float totalDist = Vector3.Distance(startPos, targetPos);


            //while the shootObject havent hit the target
            while (!hit)
            {
                if (target != null)
                {
                    if (!this.isMortar && !_targetDead)
                        targetPos = target.GetTargetT().position;
                
                    _targetDead = target.dead;
                }

                //calculating distance to targetPos
                Vector3 curPos = thisT.position;
                curPos.y = y;
                float currentDist = Vector3.Distance(curPos, targetPos);
                float curDist = Vector3.Distance(thisT.position, targetPos);

                if (Time.time - timeShot < 3.5f)
                {
                    //calculate ratio of distance covered to total distance
                    float invR = 1 - Mathf.Min(1, currentDist / totalDist);

                    //use the distance information to set the rotation, 
                    //as the projectile approach target, it will aim straight at the target
                    Vector3 wantedDir = targetPos - thisT.position;
                    if (wantedDir != Vector3.zero)
                    {
                        Quaternion wantedRotation = Quaternion.LookRotation(wantedDir);
                        float rotX = Mathf.LerpAngle(iniRotX, wantedRotation.eulerAngles.x, invR);

                        //make y-rotation always face target
                        thisT.rotation = Quaternion.Euler(rotX, wantedRotation.eulerAngles.y, wantedRotation.eulerAngles.z);
                    }
                }
                else
                {
                    //this shoot time exceed 3.5sec, abort the trajectory and just head to the target
                    thisT.LookAt(targetPos);
                }

                //if the target is close enough, trigger a hit
                if ((curDist < hitThreshold || curDist <= speed * Time.deltaTime) && !hit)
                {
                    thisT.Translate(Vector3.forward * Mathf.Min(speed * Time.deltaTime, curDist));
                    yield return null;
                    hit = true;
                    break;
                }

                yield return null;
                //move forward
                thisT.Translate(Vector3.forward * Mathf.Min(speed * Time.deltaTime, curDist));

            }
            

            yield return new WaitForSeconds(TimerBombLife);

            AbilityManager.instanceCastAbility(abilityBomb, thisT.position);
            ObjectPoolManager.Unspawn(thisObj);
            yield break;
        }


        public float hitRadius = .1f;
        IEnumerator FPSEffectRoutine()
        {
            yield return new WaitForSeconds(0.05f);

            RaycastHit raycastHit;
            Vector3 dir = thisT.TransformDirection(new Vector3(0, 0, 1));
            if (Physics.SphereCast(thisT.position, hitRadius / 2, dir, out raycastHit))
            {
                Unit unit = raycastHit.transform.GetComponent<Unit>();
                FPSHit(unit, raycastHit.point);

                if (hitEffect != null) ObjectPoolManager.Spawn(hitEffect, raycastHit.point, Quaternion.identity);
            }

            yield return new WaitForSeconds(0.1f);
            ObjectPoolManager.Unspawn(thisObj);
        }

        IEnumerator FPSBeamRoutine(Transform sp)
        {
            thisT.parent = sp;
            float duration = 0;
            while (duration < beamDuration)
            {
                RaycastHit raycastHit;
                Vector3 dir = thisT.TransformDirection(new Vector3(0, 0, 1));
                bool hitCollider = Physics.SphereCast(thisT.position, hitRadius, dir, out raycastHit);
                if (hitCollider)
                {
                    if (!hit)
                    {
                        hit = true;
                        Unit unit = raycastHit.transform.GetComponent<Unit>();
                        FPSHit(unit, raycastHit.point);

                        if (hitEffect != null) ObjectPoolManager.Spawn(hitEffect, raycastHit.point, Quaternion.identity);
                    }
                }

                float lineDist = raycastHit.distance == 0 ? 9999 : raycastHit.distance;
                for (int i = 0; i < lineList.Count; i++) lineList[i].SetPosition(1, new Vector3(0, 0, lineDist));

                duration += Time.fixedDeltaTime;
                yield return new WaitForSeconds(Time.fixedDeltaTime);
            }

            thisT.parent = null;
            ObjectPoolManager.Unspawn(thisObj);
        }

        IEnumerator FPSProjectileRoutine()
        {
            float timeShot = Time.time;
            while (true)
            {
                RaycastHit raycastHit;
                Vector3 dir = thisT.TransformDirection(new Vector3(0, 0, 1));
                float travelDist = speed * Time.fixedDeltaTime;
                bool hitCollider = Physics.SphereCast(thisT.position, hitRadius, dir, out raycastHit, travelDist);
                if (hitCollider) travelDist = raycastHit.distance + hitRadius;

                thisT.Translate(Vector3.forward * travelDist);
                if (Time.time - timeShot > 5) break;
                yield return new WaitForSeconds(Time.fixedDeltaTime);
            }

            ObjectPoolManager.Unspawn(thisObj);
            yield return null;
        }
      
        void OnTriggerEnter(Collider collider)
        {

            if (!hit && type != _ShootObjectType.FPSProjectile) return;

            hit = true;

            if (hitEffect != null) ObjectPoolManager.Spawn(hitEffect, thisT.position, Quaternion.identity);

            attInstance.impactPoint = thisT.position;

            Unit unit = collider.gameObject.GetComponent<Unit>();
            FPSHit(unit, thisT.position);

            if (hitEffect != null) ObjectPoolManager.Spawn(hitEffect, thisT.position, thisT.rotation);

            ObjectPoolManager.Unspawn(thisObj);
        }


        void FPSHit(Unit hitUnit, Vector3 hitPoint)
        {
            if (attInstance.srcWeapon.GetAOERange() > 0)
            {
                LayerMask mask1 = 1 << LayerManager.LayerCreep();
                LayerMask mask2 = 1 << LayerManager.LayerCreepF();
                LayerMask mask = mask1 | mask2;

                Collider[] cols = Physics.OverlapSphere(hitPoint, attInstance.srcWeapon.GetAOERange(), mask);
                if (cols.Length > 0)
                {
                    List<Unit> tgtList = new List<Unit>();
                    for (int i = 0; i < cols.Length; i++)
                    {
                        Unit unit = cols[i].gameObject.GetComponent<Unit>();
                        if (!unit.dead) tgtList.Add(unit);
                    }
                    if (tgtList.Count > 0)
                    {
                        for (int i = 0; i < tgtList.Count; i++)
                        {
                            AttackInstance attInst = new AttackInstance();
                            attInst.srcWeapon = attInstance.srcWeapon;
                            attInst.tgtUnit = tgtList[i];
                            tgtList[i].ApplyEffect(attInst);
                        }
                    }
                }
            }
            else
            {
                if (hitUnit != null && hitUnit.IsCreep)
                {
                    attInstance.tgtUnit = hitUnit;
                    hitUnit.ApplyEffect(attInstance);
                }
            }
        }









        private Unit target;
        private bool _targetDead = false;
        private Vector3 targetPos;
        public float maxShootAngle = 30f;
        public float maxShootRange = 0.5f;
        public float hitThreshold = 0.15f;


        public float GetMaxShootRange()
        {
            if (type == _ShootObjectType.Projectile || type == _ShootObjectType.Missile) return maxShootRange;
            return 1;
        }
        public float GetMaxShootAngle()
        {
            if (type == _ShootObjectType.Projectile || type == _ShootObjectType.Missile) return maxShootAngle;
            return 0;
        }


        IEnumerator EffectRoutine()
        {
            yield return new WaitForSeconds(0.125f);
            Hit();
        }

        IEnumerator BeamRoutine()
        {
            float timeShot = Time.time;
            Vector3 startPoint = shootPoint.position;
			bool hited = false;
            while (true)
            {
                if (target != null && !_targetDead)
                    targetPos = target.GetTargetT().position;
                if (target != null)
                {
                    _targetDead = target.dead;
                }

				if (!hited) {
					hited = true;
					Hit();
				}
                float dist = Vector3.Distance(startPoint, targetPos);
                Ray ray = new Ray(startPoint, (targetPos - startPoint));
                Vector3 targetPosition = ray.GetPoint(dist - hitThreshold);

				if (!isLightning) {
					for (int i = 0; i < lineList.Count; i++) {					
						lineList [i].SetPosition (0, startPoint);
						lineList [i].SetPosition (1, targetPosition);
					}
				} else {
					for (int i = 0; i < lightningList.Count; i++) {					
						lightningList [i].StartPosition = startPoint;
						lightningList [i].EndPosition = targetPosition;
					}
				}


                if (Time.time - timeShot > beamDuration)
                {
                    break;
                }

                yield return null;
            }
			ObjectPoolManager.Unspawn(thisObj);
        }


        IEnumerator ProjectileRoutine()
        {
            if (shootEffect != null) Instantiate(shootEffect, thisT.position, thisT.rotation);

            float timeShot = Time.time;

            //make sure the shootObject is facing the target and adjust the projectile angle
            thisT.LookAt(targetPos);
            float angle = Mathf.Min(1, Vector3.Distance(thisT.position, targetPos) / maxShootRange) * maxShootAngle;
            //clamp the angle magnitude to be less than 45 or less the dist ratio will be off
            thisT.rotation = thisT.rotation * Quaternion.Euler(-angle, 0, 0);

            Vector3 startPos = thisT.position;
            float iniRotX = thisT.rotation.eulerAngles.x;

            float y = Mathf.Min(targetPos.y, startPos.y);
            float totalDist = Vector3.Distance(startPos, targetPos);


            //while the shootObject havent hit the target
            while (!hit)
            {
                if (!this.isMortar && target != null && !_targetDead)
                    targetPos = target.GetTargetT().position;
                if (target != null)
                {
                    _targetDead = target.dead;
                }

                //calculating distance to targetPos
                Vector3 curPos = thisT.position;
                curPos.y = y;
                float currentDist = Vector3.Distance(curPos, targetPos);
                float curDist = Vector3.Distance(thisT.position, targetPos);

                if (Time.time - timeShot < 3.5f)
                {
                    //calculate ratio of distance covered to total distance
                    float invR = 1 - Mathf.Min(1, currentDist / totalDist);

                    //use the distance information to set the rotation, 
                    //as the projectile approach target, it will aim straight at the target
                    Vector3 wantedDir = targetPos - thisT.position;
                    if (wantedDir != Vector3.zero)
                    {
                        Quaternion wantedRotation = Quaternion.LookRotation(wantedDir);
                        float rotX = Mathf.LerpAngle(iniRotX, wantedRotation.eulerAngles.x, invR);

                        //make y-rotation always face target
                        thisT.rotation = Quaternion.Euler(rotX, wantedRotation.eulerAngles.y, wantedRotation.eulerAngles.z);
                    }
                }
                else
                {
                    //this shoot time exceed 3.5sec, abort the trajectory and just head to the target
                    thisT.LookAt(targetPos);
                } 

                //if the target is close enough, trigger a hit
                if ((curDist < hitThreshold || curDist <= speed * Time.deltaTime) && !hit)
                {
                    thisT.Translate(Vector3.forward * Mathf.Min(speed * Time.deltaTime, curDist));
                    yield return null;
                    Hit();
                    break;
                }

                yield return null;
                //move forward
                thisT.Translate(Vector3.forward * Mathf.Min(speed * Time.deltaTime, curDist));

            }
        }


        public float shootAngleY = 20;
        private float missileSpeedModifier = 1;
        IEnumerator MissileSpeedRoutine()
        {
            missileSpeedModifier = .05f;
            float duration = 0;
            while (duration < 1)
            {
                missileSpeedModifier = Mathf.Sin(Mathf.Sin(duration * Mathf.PI / 2) * Mathf.PI / 2);
                duration += Time.deltaTime * 1f;
                yield return null;
            }
            missileSpeedModifier = 1;
        }

        public float offsetTargetX, offsetTargetZ;
        public Vector2 speedRot;
        private Vector2 offsetTarget;
        IEnumerator MissileRoutine()
        {
            StartCoroutine(MissileSpeedRoutine());
            offsetTarget = new Vector2(Random.Range(-offsetTargetX, offsetTargetX), Random.Range(-offsetTargetZ, offsetTargetZ));
            float angleX = Random.Range(maxShootAngle / 2, maxShootAngle);
            float angleY = Random.Range(shootAngleY / 2, maxShootAngle);
            //if (Random.Range(0f, 1f) > 0.5f) angleY *= -1;
            thisT.LookAt(targetPos);
            Quaternion wantedRotation = thisT.rotation * Quaternion.Euler(-angleX, angleY, 0);
            float rand = Random.Range(speedRot.x, speedRot.y);

            //float totalDist = Vector3.Distance(thisT.position, targetPos);

            //float estimateTime = totalDist / speed;
            float shootTime = Time.time;

            Vector3 startPos = thisT.position;
            float currentDist = 999;
            float eTime;
            while (!hit)
            {
                if (target != null && !_targetDead)
                { 
                    targetPos = target.GetTargetT().position;
                    targetPos.x += offsetTarget.x;
                    targetPos.z += offsetTarget.y;

                    _targetDead = target.dead;
                }

                currentDist = Vector3.Distance(thisT.position, targetPos);

                /*delta = totalDist - Vector3.Distance(startPos, targetPos);*/
                
                eTime = Vector3.Distance(startPos, targetPos) / speed;// estimateTime - delta / speed;

                if (Time.time - shootTime > eTime)
                {
                    Vector3 wantedDir = targetPos - thisT.position;
                    if (wantedDir != Vector3.zero)
                    {
                        wantedRotation = Quaternion.LookRotation(wantedDir);
                        float val1 = (Time.time - shootTime) - (eTime);
                        thisT.rotation = Quaternion.Slerp(thisT.rotation, wantedRotation, val1 / (eTime * currentDist));
                    }
                }
                else
                {
                    thisT.rotation = Quaternion.Slerp(thisT.rotation, wantedRotation, Time.deltaTime * rand);
                }

                if (currentDist < hitThreshold || speed * Time.deltaTime * missileSpeedModifier > currentDist)
                {
                    thisT.Translate(Vector3.forward * Mathf.Min(speed * Time.deltaTime * missileSpeedModifier, currentDist));
                    yield return null;
                    //Debug.LogWarning("cur:" + currentDist + " ht:" + hitThreshold + " step:" + (speed * Time.deltaTime * missileSpeedModifier));
                    Hit();
                    break;
                }
                else
                {
                    //Debug.Log("cur:"+currentDist+" ht:"+hitThreshold+" step:"+(speed * Time.deltaTime * missileSpeedModifier));
                }

                yield return null;

                thisT.Translate(Vector3.forward * Mathf.Min(speed * Time.deltaTime * missileSpeedModifier, currentDist));

            }
        }




        Collider[] colsAOE;
        GameObject objSplash;
        void Hit()
        {
            hit = true;

            if (hitEffect != null) ObjectPoolManager.Spawn(hitEffect, targetPos, Quaternion.identity);

            //thisT.position=targetPos;
            attInstance.impactPoint = targetPos;

            attInstance.Process();
            if (attInstance.isAOE)
            {
                if (Splash != null)
                {
                    objSplash = ObjectPoolManager.Spawn(Splash, attInstance.impactPoint, Quaternion.identity);
                    objSplash.GetComponent<SplashSpecialEffect>().Emit(attInstance.AOERadius);
                }

               // Debug.Log("HIT AOE:" + attInstance.AOERadius);
                colsAOE = Physics.OverlapSphere(targetPos, attInstance.AOERadius/*(1 + attInstance.AOERadius) * 2*/, attInstance.getTargetMask());
                //Debug.Log(string.Format( "Hit: cols {0}, range {1} ",colsAOE.Length,attInstance.AOERadius));
                if (colsAOE.Length > 0)
                {
                    for (int i = 0; i < colsAOE.Length; i++)
                    {
                        Unit unit = colsAOE[i].GetComponent<Unit>();
                        if (unit != null && !unit.dead)
                        {
                            float d = Vector3.Distance(targetPos, unit.transform.position);
                            //Debug.Log(string.Format("Hit aoe:{3} d:{0}, tp:{1}, unit:{2}", d, targetPos, unit.transform.position, attInstance.AOERadius));
                            if (unit == target)
                            {
                                attInstance.CallEventCounterTargets(colsAOE.Length);
                                attInstance.ApplyDistance(d);
                                unit.ApplyEffect(attInstance);
                            }
                            else
                            {
                                AttackInstance attIns = attInstance.CloneNewTargetProcess(unit);
                                attIns.CallEventCounterTargets(colsAOE.Length);
                                attIns.ApplyDistance(d);
                                unit.ApplyEffect(attIns);
                            }
                        }
                    }
                }
                //else
                //    Debug.Log(string.Format("Not Hit aoe:{1}, d:{2}, tp:{0}", targetPos, attInstance.AOERadius,Vector3.Distance(targetPos,target.transform.position)));

            }
            else
            {
                if (target != null)
                {
                    target.ApplyEffect(attInstance);
                }
            }

            if (OnHitE != null)
                OnHitE(this, attInstance);

            /*
            if (attInstance.srcUnit != null && attInstance.srcUnit.GetAOERadius() > 0)
            {
                LayerMask mask = attInstance.srcUnit.GetTargetMask();

                Collider[] cols = Physics.OverlapSphere(targetPos,(1+ attInstance.srcUnit.GetAOERadius())*2, mask);
                //Debug.Log("Hit: cols "+cols.Length);
                if (cols.Length > 0)
                {
                    List<Unit> tgtList = new List<Unit>();
                    for (int i = 0; i < cols.Length; i++)
                    {
                        Unit unit = cols[i].gameObject.GetComponent<Unit>();
                        if (!unit.dead) tgtList.Add(unit);
                    }
                    if (tgtList.Count > 0)
                    {
                        for (int i = 0; i < tgtList.Count; i++)
                        {
                            if (false && (tgtList[i] == target))
                                target.ApplyEffect(attInstance);
                            else
                            {
                                AttackInstance attInst = new AttackInstance();
                                attInst.srcUnit = attInstance.srcUnit;
                                attInst.tgtUnit = tgtList[i];
                                attInst.DistanceFromCenter = Mathf.Clamp(-0.5f * (Vector3.Distance(targetPos, tgtList[i].transform.position) / attInstance.srcUnit.GetAOERadius()) + 1, 0, float.MaxValue);
                                //Debug.Log("Hit:dis " + attInst.DistanceFromCenter);
                                attInst.Process();
                                tgtList[i].ApplyEffect(attInst);
                            }
                        }
                    }
                }
            }
            else if (attInstance.srcPropretiesAttak != null && attInstance.srcPropretiesAttak.GetAOERange() > 0)
            {
                LayerMask mask = attInstance.srcPropretiesAttak.GetTargetMask();
                MyLog.Log("srcPropretiesAttak hit");
                Collider[] cols = Physics.OverlapSphere(targetPos, attInstance.srcPropretiesAttak.GetAOERadius(), mask);
                if (cols.Length > 0)
                {
                    List<Unit> tgtList = new List<Unit>();
                    for (int i = 0; i < cols.Length; i++)
                    {
                        Unit unit = cols[i].gameObject.GetComponent<Unit>();
                        if (!unit.dead) tgtList.Add(unit);
                    }
                    if (tgtList.Count > 0)
                    {
                        for (int i = 0; i < tgtList.Count; i++)
                        {
                            if (false && (tgtList[i] == target))
                                target.ApplyEffect(attInstance);
                            else
                            {
                                AttackInstance attInst = new AttackInstance();
                                attInst.srcPropretiesAttak = attInstance.srcPropretiesAttak;
                                attInst.tgtUnit = tgtList[i];
                                attInst.DistanceFromCenter = Mathf.Clamp(-0.5f * (Vector3.Distance(targetPos, tgtList[i].transform.position) / attInstance.srcPropretiesAttak.GetAOERadius()) + 1, 0, float.MaxValue);
                                attInst.Process();
                                tgtList[i].ApplyEffect(attInst);
                            }
                        }
                    }
                }
            }
            else
            {
                if (target != null) target.ApplyEffect(attInstance);
            }*/

            if (type != _ShootObjectType.Beam) {
				ObjectPoolManager.Unspawn (thisObj);
			}
            //Destroy(thisObj);

            
        }









        IEnumerator ClearTrail(TrailRenderer trail, float time)
        {
            if (gameObject.activeSelf == false)
                yield break;
            Debug.Log("t:" + time);
            trail.time = -1;
            yield return null;
            trail.time = time;
            Debug.Log("t2:" + trail.time);
        }



    }

}