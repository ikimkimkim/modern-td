﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class CardUpgradeData
{
    [System.Serializable]
    public class info
    {
        public _CardRareness rareness;
        public string name;
        public string desp, shortDesp, craftDesp;
    }

    public int id;
    public Sprite icon;
    public string generalDesp;
    public info[] data;

    private info getInfo(_CardRareness rareness)
    {
        return data.First(i => i.rareness == rareness);
    }

    public string GetDesp(_CardRareness rareness)
    {
        var info = getInfo(rareness);
        if(info!=null)
        {
            return info.desp;
        }
        return "";
    }

    public string GetShortDesp(_CardRareness rareness)
    {
        var info = getInfo(rareness);
        if (info != null)
        {
            return info.shortDesp;
        }
        return "";
    }

    public string GetCraftDesp(_CardRareness rareness)
    {
        var info = getInfo(rareness);
        if (info != null)
        {
            return info.craftDesp;
        }
        return "";
    }

}
