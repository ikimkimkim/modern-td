﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TDTK
{

    public enum _CreepType { Default, Offense, Support }
    public enum _CreepClass { none, Insects=1, Plants=2, Mushrooms=3, Warmblooded=4, Elementals=5, Ogro=6 }


    public class UnitCreep : Unit
    {
        
        public delegate void DestinationHandler(UnitCreep unit);
        public static event DestinationHandler onDestinationE;
        [Header("Creep")]
        public _CreepType type = _CreepType.Default;
        public _CreepClass creepClass;
        public bool flying = false;
        public bool CanTargetTower=false;
        public float flyingHeightOffset = 0;
        public float ModelHeigthOffset = 0;

        public int Generation;
        public int LimitGeneration = -1; //-1 без лимита 
        public bool spawnIsScaler = true;
        [Range(0f,1f)]
        public float spawnScaleMultiplier=0.9f;
        public float spawnMinScaleOrigin = 0.5f;
        public GameObject spawnUponDestroyed;
        public int spawnUponDestroyedCount = 0;
        public float spawnUnitHPMultiplier = 0.5f;


        public string UnitInfoText = "Нет информации";
        public string UrlIconPropertiesInfo;
        [TextArea]
        public string textPropertiesInfo = "Нет описания свойства!";

        public int waveID = 0;
        public float lifeCost = 1;
        //public int scoreValue = 1;

        public int lifeValue = 0;
        public List<float> valueRscMin = new List<float>();
        public List<float> valueRscMax = new List<float>();
        public int valueEnergyGain = 0;

        public bool stopToAttack = false;

        public bool isBoss = false;
        public bool isBossPortal = false;

        public float RandomSpawnIncrement = 1;

        public float MyPathOffset=0;
        [HideInInspector]
        public Vector3 pathOffset;
        private Vector3 pathDynamicOffset;
        public Vector3 GetPathDynamicOffset() { return pathDynamicOffset; }
        public float addDinstansForWaypoint = 0;
        private float distLimit;


        public override Vector3 getPosition
        {
            get
            {
                if (flying)
                    return thisT.position - Vector3.up * flyingHeightOffset;
                else
                    return thisT.position;
            }
        }

        public override void Awake()
        {
            if (!flying)
                gameObject.layer = LayerManager.LayerCreep();
            else
                gameObject.layer = LayerManager.LayerCreepF();

            base.Awake();

            CapsuleCollider sp;
            sp = thisObj.GetComponent<CapsuleCollider>();
            if (sp == null)
            {
                sp = thisObj.AddComponent<CapsuleCollider>();
            }
            if (sp != null)
            {
                SizeMyCollider = sp.radius * thisT.localScale.x;
                if (flying)
                {
                    sp.height = flyingHeightOffset+ ModelHeigthOffset;
                    //sp.center = - Vector3.up * flyingHeightOffset / thisT.localScale.x;
                }
               
            }
        }


        //parent unit is for unit which is spawned from destroyed unit
        public virtual void Init(PathTD p, int ID, int wID, UnitCreep parentUnit = null)
        {
            Init();

            path = p;
            instanceID = ID;
            waveID = wID;

            name = startName + "(w" + waveID + " id" + ID+")";
            

            float dynamicX = Random.Range(-path.dynamicOffset, path.dynamicOffset);
            float dynamicZ = Random.Range(-path.dynamicOffset, path.dynamicOffset);
            pathDynamicOffset = new Vector3(dynamicX, 0, dynamicZ);

            dynamicX = Random.Range(-MyPathOffset, MyPathOffset);
            dynamicZ = Random.Range(-MyPathOffset, MyPathOffset);
            pathOffset = new Vector3(dynamicX, 0, dynamicZ);

            _Agent.enabled = !flying;

            if (parentUnit == null)
            {
                waypointID = 0;
                subWaypointID = 0;
                subPath = path.GetWPSectionPath(waypointID);
                UpdateSubWaypoint();
                Generation = 0;
                distFromDestination = CalculateDistFromDestination(null);
                _Agent.speed = stats[currentActiveStat].moveSpeed;
                thisT.position += pathDynamicOffset + pathOffset + Vector3.up * flyingHeightOffset;
            }
            else
            {
                name += " child:"+parentUnit.name;
                //inherit stats and path from parent unit
                waypointID = parentUnit.waypointID;
                subPath = path.GetWPSectionPath(waypointID);
                subWaypointID = parentUnit.subWaypointID - 1;//-1 для послед. updateSubWaypoint
                //subPath = parentUnit.subPath;
                Generation = parentUnit.Generation + 1;

                isBossPortal = parentUnit.isBossPortal;

                UpdateSubWaypoint();

                if(stats!=null && stats.Count>0 && parentUnit.stats!=null && parentUnit.stats.Count>0)
                    stats[0] = parentUnit.stats[0].Clone();

                StartCoroutine(SlowSpeedDial(stats[0].moveSpeed* Random.Range(0.95f, 1.05f)));

                fullHP = parentUnit.fullHP * parentUnit.spawnUnitHPMultiplier;
                //fullShield = parentUnit.fullShield * parentUnit.spawnUnitHPMultiplier;
                HP = fullHP;
                //shield = fullShield;

                distFromDestination = parentUnit._GetDistFromDestination();
                if (_Agent != null)
                {
                    _Agent.speed = 0;
                    _Agent.avoidancePriority = _Agent.avoidancePriority - 10;
                    _Agent.acceleration = parentUnit._Agent.acceleration;
                    MoveToPointSubPath(subWaypointID);
                }
                //Debug.LogFormat("{0} WPID:{1}, subWPID:{2} ", name, waypointID, subWaypointID);
            }

            //distFromDestination = CalculateDistFromDestination(); Раньше было так 

            if (type == _CreepType.Offense)
            {
                StartCoroutine(ScanForTargetRoutine());
                CorTurretRoutine = StartCoroutine(TurretRoutine());
            }
            if (type == _CreepType.Support)
            {
               // StartCoroutine(SupportRoutine());
            }


            if (flying == false)
            {
                MoveToPoint(subPath[subWaypointID]);

            }
            else if(flyingHeightOffset!=0)
            {
                VitionCollider.transform.localPosition = -Vector3.up * flyingHeightOffset/thisT.localScale.x;
            }

        }

        public void SetWaypointID(int index)
        {
            waypointID = index;
            subPath = path.GetWPSectionPath(waypointID);
            subWaypointID = 0;
            MoveToPoint(subPath[subWaypointID]);
        }

        private void UpdateSubWaypoint()
        {
            subWaypointID += 1;
            if (subWaypointID >= subPath.Count)
            {
                subWaypointID = 0;
                waypointID += 1;
                if (waypointID >= path.GetPathWPCount())
                {
                    ReachDestination();
                }
                else
                {
                    subPath = path.GetWPSectionPath(waypointID);
                }
            }
        }

        bool EndBossSpawn = false;
        public void SpawnEndBoss()
        {
            EndBossSpawn = true;
            if (isBossPortal == false)
            {
                if (dead == false)
                {
                    Dead(false);
                }
                else
                    Debug.LogWarning("I can't kill creep, is dead!");
            }
        }

        public float Acceleration=0.5f;
        private IEnumerator SlowSpeedDial(float endMoveSpeed)
        {
            stats[0].moveSpeed = 0;
            while (stats[0].moveSpeed < endMoveSpeed)
            {
                stats[0].moveSpeed += Acceleration * Time.deltaTime;
                UpdateMoveSpeedAgent();
                 yield return null;
            }
            stats[0].moveSpeed = endMoveSpeed;
            UpdateMoveSpeedAgent();
            yield break;
        }



        public override void OnEnable()
        {
            UnitsVisionSystem.AddUnit(this);
            base.OnEnable();
            EndBossSpawn = false;
            SubPath.onPathChangedE += OnSubPathChanged;
            SpawnManager.onSpawnEndBossE += SpawnEndBoss;
        }
        public override void OnDisable()
        {
            UnitsVisionSystem.RemoveUnit(this);
            base.OnDisable();
            if (_Agent!=null)
                _Agent.enabled = false;
            SubPath.onPathChangedE -= OnSubPathChanged;
            SpawnManager.onSpawnEndBossE -= SpawnEndBoss;
        }




        public float rotateSpd = 10;

        public PathTD path;
        public List<Vector3> subPath = new List<Vector3>();
        public int waypointID = 1;
        public int subWaypointID = 0;

        void OnSubPathChanged(SubPath platformSubPath)
        {
            if (platformSubPath.parentPath == path && platformSubPath.wpIDPlatform == waypointID)
            {
                ResetSubPath(platformSubPath);
            }
        }
        private static Transform dummyT;
        void ResetSubPath(SubPath platformSubPath)
        {
            if (dummyT == null) dummyT = new GameObject().transform;

            Quaternion rot = Quaternion.LookRotation(subPath[subWaypointID] - thisT.position);
            dummyT.rotation = rot;
            dummyT.position = thisT.position;

            Vector3 pos = dummyT.TransformPoint(0, 0, BuildManager.GetGridSize() / 2);
            NodeTD startN = PathFinder.GetNearestNode(pos, platformSubPath.parentPlatform.GetNodeGraph());
            PathFinder.GetPath(startN, platformSubPath.endN, platformSubPath.parentPlatform.GetNodeGraph(), this.SetSubPath);
        }
        void SetSubPath(List<Vector3> pathList)
        {

            subPath = pathList;
            //for (int i = 0; i < pathList.Count; i++)
            //{
            //    Debug.Log("PathList Point " + i + " POS " + pathList[i].ToString());
            //}
            subWaypointID = 0;//Вот это наверника неправильно, мой метод не улучшает работу;// GetNewSubWaypointID(pathList);
            distFromDestination = CalculateDistFromDestination(pathList);
            //   Debug.Log("SubPath Changed " + distFromDestination);
        }
        //Мой метод похоже он не помогает
        int GetNewSubWaypointID(List<Vector3> pathList)
        {
            int newSubwaypoint = 0;
            float minDistance = float.MaxValue;
            for (int i = 0; i < pathList.Count; i++)
            {
                float distanceToI = Vector3.Distance(thisT.position, pathList[i]);
                if (distanceToI < minDistance)
                {
                    minDistance = distanceToI;
                    newSubwaypoint = i;
                }
            }
            return newSubwaypoint;
        }

        public override bool isMoveEnd()
        {
            if (flying)
            {
                if (type == _CreepType.Offense && stopToAttack && target != null)
                {
                    if (getDistanceFromTarget() <= GetRange())
                    {
                        return true;
                    }
                }
                return false;
            }
            else
                return base.isMoveEnd();
        }
      

        private float distWP;

        public override void SystemUpdate()
        {
            base.SystemUpdate();

            MoveUpdate();
            //UpdateMoveSpeedAgent();
            MoveUpFlying();
        }

        private void MoveUpdate()
        {
            if (lockMovePach || stunned)
                return;

            if (!dead && GameControl.GetGameState() == _GameState.Play && path != null)
            {
                if (flying)
                {
                    if (type == _CreepType.Offense && stopToAttack && target != null)
                    {
                        //MyLog.Log(thisT.name + " target:" + target.name);

                        if (getDistanceFromTarget() > GetRange())
                        {
                            MoveToPointOld(target.thisT.position);
                        }

                    }
                    else
                    {

                        if (MoveToPointOld(subPath[subWaypointID]))
                        {
                            //MyLog.Log("MoveEndOld s" + subWaypointID + " w" + waypointID);

                            UpdateSubWaypoint();

                        }
                    }
                }
                else //Не летающие
                {
                    if (type == _CreepType.Offense && stopToAttack && target != null)
                    {
                        //MyLog.Log("Target:" + Vector3.Distance(target.thisT.position, thisT.position) + " " + GetRange());
                        if (getDistanceFromTarget() > GetRange())
                        {
                            _Agent.isStopped = false;
                            _Agent.SetDestination(target.thisT.position);
                        }
                        else
                            _Agent.SetDestination(thisT.position);
                    }
                    else
                    {
                        distFromDestination = CalculateDistFromDestination(subPath);

                        distLimit = Mathf.Max(_Agent.speed * Time.deltaTime, _Agent.radius * thisT.localScale.z + addDinstansForWaypoint) + SizeMyCollider;
                        // if (_Agent.pathEndPosition == getPosition)                        

                            distWP = getPosition.DistanceYZero(getPointAddOffset(subPath[subWaypointID])) ;
                        // else
                        if (distWP < 1f)
                        {
                            distWP = _Agent.hasPath ? Vector3.Distance(_Agent.pathEndPosition, getPosition) : 0;
                        }
                        //Debug.Log(name + " sqr:" + _Agent.velocity.sqrMagnitude + " dL:"+distLimit+" PM:"+ distWP);
                        if (distWP < distLimit || distFromDestination < distLimit )
                        {
                            //MyLog.Log(name +" MoveEnd sID:" + subWaypointID + " wID:" + waypointID + " dist:"+ distFromDestination + 
                            //    " dWP:"+ distWP + " "+ (_Agent.pathEndPosition == getPosition) + " dL:"+ distLimit );

                            subWaypointID += 1;
                            if (subWaypointID >= subPath.Count)
                            {
                                //Debug.Log("Waypoint add 1!"+name);
                                subWaypointID = 0;
                                waypointID += 1;
                                if (waypointID >= path.GetPathWPCount() && distFromDestination < 3f)
                                {
                                    ReachDestination();
                                }
                                else
                                {
                                    subPath = path.GetWPSectionPath(waypointID);
                                }
                            }
                            MoveToPoint(subPath[subWaypointID]);

                            //Debug.LogFormat("{0} new WPID:{1}, subWPID:{2} ", name, waypointID, subWaypointID);
                        }
                        else if (_Agent.pathEndPosition != subPath[subWaypointID] + pathDynamicOffset + pathOffset)
                            MoveToPoint(subPath[subWaypointID]);

                        //distFromDestination -= _Agent.velocity.magnitude;
                    }
                }
            }
        }

        public override void Dead(bool getRsc = true)
        {
            if (dead)
            {
                //MyLog.LogWarning("UnitСreep " + name + " dead is now dead!");
                return;
            }
            base.Dead(getRsc);


            CreepDestroyed(getRsc);

            if (CorTurretRoutine != null)
                StopCoroutine(CorTurretRoutine);
            if (_Agent != null && _Agent.enabled)
            {
                _Agent.isStopped = true;
                _Agent.enabled = false;
            }

        }

        //function call to rotate and move toward a pecific point, return true when the point is reached
        private Vector3 pointMove;
        public bool MoveToPointSubPath(int id)
        {
            if (id >= subPath.Count || id < 0)
                return false;
            
            return MoveToPoint(subPath[id]);
        }
        public bool MoveToPoint(Vector3 point)
        {
            pointMove =point + pathDynamicOffset+ pathOffset;
            _Agent.isStopped = false;
            return _Agent.SetDestination(pointMove);
        }

        public bool MoveToPointOld(Vector3 point)
        {

            //this is for dynamic waypoint, each unit creep have it's own offset pos
            //point+=dynamicOffset;
            pointMove = getPointAddOffset(point);

            float dist = Vector3.Distance(pointMove, thisT.position);



            //rotate towards destination
            if (stats[currentActiveStat].moveSpeed > 0)
            {
                if (pointMove - thisT.position != Vector3.zero)
                {
                    Quaternion wantedRot = Quaternion.LookRotation(pointMove - thisT.position);
                    thisT.rotation = Quaternion.Slerp(thisT.rotation, wantedRot, rotateSpd * Time.deltaTime);
                }
            }

            //move, with speed take distance into accrount so the unit wont over shoot
            Vector3 dir = (pointMove - thisT.position).normalized;
            float moveDist = Mathf.Min(dist, GetMoveSpeed() * Time.deltaTime);
            thisT.Translate(dir * moveDist , Space.World);

            distFromDestination -= moveDist;//проблема не тут, если постоянно считать нижним методом то выходит одинакого 
            // distFromDestination = CalculateDistFromDestination();   //if the unit have reached the point specified
            //~ if(dist<0.15f) return true;
            //Debug.Log("moveDist:" + moveDist * slowMultiplier * Time.fixedDeltaTime + " ost:" + dist);
            if (dist < GetMoveSpeed() * Time.deltaTime)
                return true;
            return false;
            
        }

        Vector3 getPointAddOffset(Vector3 point)
        {
            return point + pathDynamicOffset + pathOffset + (flying ? Vector3.up * flyingHeightOffset:Vector3.zero);
        }

        private Terrain terrain;
        protected float HeightTerrainPoint;
        private void MoveUpFlying()
        {
            if (dead == true || flying == false)
                return;

            if (terrain == null)
                terrain = Terrain.activeTerrain;

            HeightTerrainPoint = terrain.SampleHeight(thisT.position) + terrain.transform.position.y;
            if(thisT.position.y - HeightTerrainPoint < flyingHeightOffset)
            {
                thisT.position += Vector3.up * Time.deltaTime;
            }
        }

        void ReachDestination()
        {
            MyLog.Log("ReachDestination "+name);

            if(GameControl.IsGameOver == false && onDestinationE != null)
                onDestinationE(this);

            if (SpawnManager.GetDestinationMode == _SpawnDestinationMode.Loop /*path.loop*/)
            {
                subWaypointID = 0;
                waypointID = path.GetLoopPoint();
                subPath = path.GetWPSectionPath(waypointID);
                
                RestartPath();
                return;
            }

            dead = true;
            

            if (animationUnit != null)
                animationUnit.PlayDestination(); 
            else
                DestinationEndAnimEvent();


            //StartCoroutine(_ReachDestination(delay));
        }

        /*protected IEnumerator _ReachDestination(float duration)
        {
            yield return new WaitForSeconds(duration);
            HideStunEffect();
            HideSlowEffect();
            ObjectPoolManager.Unspawn(thisObj);
        }*/

        //Вызывается аниматором в конце аниции успешного прохода пути
        public void DestinationEndAnimEvent()
        {
            ObjectPoolManager.Unspawn(thisObj);
        }


        public void RestartPath()
        {
            //MyLog.Log("HP Creep:" + HP);
            gameObject.SetActive(false);
            transform.position = subPath[0];
            float newHP = HP + 0.5f * HP;
            HP = Mathf.Clamp(newHP, HP, fullHP);
            gameObject.SetActive(true);
            _Agent.enabled = !flying;
            //MyLog.Log("New HP Creep:" + HP);
        }

        public void CreepDestroyed(bool getRsc = true)
        {
            //Debug.Log("CreepDestroyed : "+name+" " + getRsc);
            if (getRsc && isSpawner==false)
            {
                List<float> rscGain = new List<float>();
                if (valueRscMin.Count == 0)
                {
                    rscGain.Add(0);//mana
                }
                else
                {
                    for (int i = 0; i < valueRscMin.Count; i++)
                    {
                        if (CardAndLevelSelectPanel.BigPortal)
                            rscGain.Add((float)System.Math.Round(Random.Range(valueRscMin[i], valueRscMax[i]) * Mathf.Pow(GameControl.instance.RscCreepMultiplier, (float)CardAndLevelSelectPanel.LevelDifficultySelect), 2));
                        else
                            rscGain.Add((float)System.Math.Round(Random.Range(valueRscMin[i], valueRscMax[i]) * GameControl.instance.RscCreepMultiplier, 2));
                    }
                }
                if(rscGain.Count<2)
                    rscGain.Add(0);//mana
                if(rscGain[0]>0 || rscGain[1]>0)
                    ResourceManager.GainResource(rscGain, PerkManager.GetRscCreepKilled());
            }
            AbilityManager.GainEnergy(valueEnergyGain + (int)PerkManager.GetEnergyWaveClearedModifier());


        }

        public bool isSpawner
        {
            get {
                return spawnUponDestroyed != null && spawnUponDestroyedCount > 0 &&
                        (Generation < LimitGeneration || LimitGeneration < 0);
            }
        }

        //Вызывается анимацией при смерти
        public void SpawnUnitAnimEvent()
        {
            if (isSpawner && EndBossSpawn == false)
            {
                for (int i = 0; i < spawnUponDestroyedCount; i++)
                {
                    Vector3 posOffset = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)) * SizeMyCollider;
                    GameObject obj = ObjectPoolManager.Spawn(spawnUponDestroyed, getPosition + posOffset, thisT.rotation);
                    UnitCreep unit = obj.GetComponent<UnitCreep>();

                    unit.waveID = waveID;
                    int ID = SpawnManager.AddDestroyedSpawn(unit);
                    unit.Init(path, ID, waveID, this);

                    unit.pathDynamicOffset = pathDynamicOffset;
                    unit.pathOffset = posOffset;

                    HeightTerrainPoint = Terrain.activeTerrain.transform.position.y + Terrain.activeTerrain.SampleHeight(unit.thisT.position);
                    if (unit.thisT.position.y < HeightTerrainPoint)
                        unit.thisT.position = new Vector3(unit.thisT.position.x, HeightTerrainPoint, unit.thisT.position.z);

                    if (spawnIsScaler)
                    {
                        unit.thisT.localScale = thisT.localScale * spawnScaleMultiplier;
                        if (unit.thisT.localScale.x < unit.deffScale.x * spawnMinScaleOrigin)
                        {
                            unit.thisT.localScale = unit.deffScale * spawnMinScaleOrigin;
                        }
                    }

                    unit.lifeCost = lifeCost / spawnUponDestroyedCount;

                    unit.valueRscMax = new List<float>(valueRscMax);
                    for (int j = 0; j < unit.valueRscMax.Count; j++)
                    {
                        unit.valueRscMax[j] /= spawnUponDestroyedCount;
                    }

                    unit.valueRscMin = new List<float>(valueRscMin);
                    for (int j = 0; j < unit.valueRscMin.Count; j++)
                    {
                        unit.valueRscMin[j] /= spawnUponDestroyedCount;
                    }

                    if (unit.stats != null && unit.stats.Count > 0)
                    {
                        unit.stats[0].damageMin /= spawnUponDestroyedCount;
                        unit.stats[0].damageMax /= spawnUponDestroyedCount;
                    }
                }


                DeadEndAnimEvent();
            }
        }


        public void Hit() { if (animationUnit != null) animationUnit.PlayHit(); }

        public override float GetSpeedMoveAnimation
        {
            get
            {
                if (stunned || isMoveEnd())
                    return 0;
                else
                    return GetMoveSpeed();
            }
        }





        public float distFromDestination = 0;
        public float _GetDistFromDestination() { return distFromDestination; }
        public List<float> Distances = new List<float>();
        float CalculateDistFromDestinationOLD()
        {
            //неправильно считает для самолетов для танка который едет через платформу темболее
            float dist = Vector3.Distance(thisT.position, subPath[subWaypointID]);
            Distances.Clear();
            Distances.Add(dist);
            for (int i = subWaypointID + 1; i < subPath.Count; i++)
            {
                dist += Vector3.Distance(subPath[i - 1], subPath[i]);
                Distances.Add(dist);
            }
            dist += path.GetPathDistance(waypointID + 1);
            Distances.Add(dist);
            return dist;
        }
        float CalculateDistFromDestination(List<Vector3> pathList)
        {
            
            float distance = 0;
           // Distances.Clear();
           // Distances.Add(distance);
            Vector3 lastPoint = thisT.position;
            if (pathList == null)
            {
                CalcDistance(ref distance, ref lastPoint, waypointID, subWaypointID);
            }
            else
            {
                //это нужно для правильного вычисления при изменении пути
                distance = Vector3.Distance(thisT.position, pathList[subWaypointID]);
                lastPoint = pathList[subWaypointID];
            //    Distances.Add(distance);
                for (int i = subWaypointID + 1; i < pathList.Count; i++)
                {
                    distance += Vector3.Distance(pathList[i - 1], pathList[i]);
            //        Distances.Add(distance);
                    lastPoint = pathList[i];
                }
            }

            for (int i = waypointID + 1; i < path.wpSectionList.Count; i++)
            {
                CalcDistance(ref distance, ref lastPoint, i, 0);
            }
            return distance;
        }

        private void CalcDistance(ref float distance, ref Vector3 lastPoint, int i, int subPointID)
        {
            if (path.wpSectionList[i].isPlatform)
            {
                //Debug.Log("path.wpSectionList[i].pathIDOnPlatform " + path.wpSectionList[i].pathIDOnPlatform + " i " + i);
                List<Vector3> pointList = path.GetWPSectionPath(i); // path.wpSectionList[i].pathIDOnPlatform
                if(pointList.Count==0)
                {
                    Debug.LogError("CalcDist isPlatform point list = 0.");
                    return;
                }
                //Debug.Log("pointList.Count" + pointList.Count); //тут почему-то 1 а не 11 как должно быть
                distance += Vector3.Distance(lastPoint, pointList[subPointID]);
                //Distances.Add(distance);
                for (int q = subPointID + 1; q < pointList.Count; q++)
                {
                    //Debug.Log("POINTS PROCEED " + q + " POS " + pointList[q].ToString());
                    distance += Vector3.Distance(pointList[q - 1], pointList[q]);
                    //Distances.Add(distance);
                }
                lastPoint = pointList[pointList.Count - 1];
            }
            else
            {
                distance += Vector3.Distance(lastPoint, path.wpSectionList[i].waypointT.position);
                //Distances.Add(distance);
                lastPoint = path.wpSectionList[i].waypointT.position;
            }
        }

        public override float GetDistFromDestination() { return GetUnitCreep._GetDistFromDestination(); }

        public override float GetRange()
        {
            return base.GetRange() * GameControl.instance.RangeCreepMultiplier;
        }
    }

}
