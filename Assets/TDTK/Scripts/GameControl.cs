using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;
using UnityEngine.SceneManagement;

namespace TDTK {

	public enum _GameState
    {
        Training,
        Play,
        Pause,
        Over
    }

    public enum _GameType
    {
        Life, //������� � �� 
        DefenseBase, //������� ������ ����
        TimeBoss,//������� �� ����� � ����� � �����

    }

    [RequireComponent(typeof(ResourceManager))]
    [RequireComponent(typeof(DamageTable))]
    public class GameControl : MonoBehaviour
    {
        public static float sellTowerRefundRatio = 0.9f;

        public delegate void GameMessageHandler(string msg);
        public static event GameMessageHandler onGameMessageE;
        public static void DisplayMessage(string msg)
        {
            if (onGameMessageE != null)
                onGameMessageE(msg);
        }

        public delegate void GameOverHandler(int stars, ResponseData prizes); //true if win
        public static event GameOverHandler onGameOverE;

        public delegate void LifeHandler(float value);
        public static event LifeHandler onLifeE;

        public delegate void GameStartHandler(int levelID);
        public static event GameStartHandler onGameStarted;

        public static bool GET_EDITOR_ShowDamage
        {
            get {
                if (instance == null)
                    return true;
                else
                    return instance.EDITOR_ShowDamage;
            }
        }
        public bool EDITOR_ShowDamage = true;

        private bool _isGameStarted = false;

        public _GameType _gameType = _GameType.Life;

        public static _GameType GetGameType { get { return instance._gameType; } }

        public static bool IsGameStarted() { if (instance != null) return instance._isGameStarted; else return false; }
        public static bool IsGamePlay { get { return instance.gameState == _GameState.Play; } }
        public static bool IsGameOver { get { return instance.gameState == _GameState.Over; } }
        public static bool IsGamePause { get { return instance.gameState == _GameState.Pause; } }
        public static bool IsGameTrainig { get { return instance.gameState == _GameState.Training; } }

        [SerializeField] private _GameState gameState = _GameState.Training;

        public static _GameState GetGameState() { return instance.gameState; }
        
        public int levelID = 1;   //the level progression (as in lvl1, lvl2, lvl3 and so on), user defined, use to verify perk availability
        public float HPCreepMultiplier = 1.5f;
        public float DamageCreepMultiplier = 1f;
        public float RscCreepMultiplier = 1;
        public float RscStartMultiplier = 1;
        public float RangeCreepMultiplier = 1f;
        [SerializeField] private float _scoreDeadUnityMultiplier = 1;
        public static int GetLevelID() { return instance.levelID; }

        public bool capLife = false;
        public int playerLifeCap = 0;
        [SerializeField] public float _playerLife = 10;
        public static float GetPlayerLife() { if (instance != null) return instance._playerLife; else return 0; }
        public static int GetPlayerLifeCap() { return instance.capLife ? instance.playerLifeCap + PerkManager.GetLifeCapModifier() : -1; }

        public bool enableLifeGen = false;
        public int lifeRegenRate = 0;

        public Transform rangeIndicator;
        public Transform rangeIndicatorCone;
        private GameObject rangeIndicatorObj;
        private GameObject rangeIndicatorConeObj;

        Projector projectorRange;


        public int PlayerLifeInBigPortal = 5;
        public int PlayerLifeInCompany = 20;
        public int PlayerLifeIn1RedStar = 5;
        public int PlayerLifeIn2RedStar = 1;

        public int Stars { get; private set; }

        public string nextScene = "";
        public string mainMenu = "";
        public static void LoadNextScene() { if (instance.nextScene != "") Load(instance.nextScene); }
        public static void LoadMainMenu() { if (instance.mainMenu != "") Load(instance.mainMenu); }
        public static void Load(string levelName)
        {
            //if(gameState==_GameState.Ended && instance.playerLife>0){
            //	ResourceManager.NewSceneNotification();
            //}
            UIWaitPanel.Show();
            SceneManager.LoadScene(levelName);
            //Application.LoadLevel(levelName);
            //Application.LoadLevel(Application.loadedLevelName);
        }

        public static void RestartLevel()
        {
            QuestsTraining.ReturnData();
            ResourceManager.OnRestartLevel();

            PropertiesHelp.listCoomplectPropertiesBild.Clear();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            //Application.LoadLevel(Application.loadedLevelName);
        }

        private float TimeGame = 0;
        public static float GetTimeGame { get { return instance.TimeGame; } }


        public bool loadAudioManager = false;

        private float timeStep = 0.015f;

        public static GameControl instance;
        private Transform thisT;
        private PlayerManager _playerInfo;


        [SerializeField] private float _scoreDeadUnitCount = 0;
        public static float GetScoreDeadUnitCount { get { return instance._scoreDeadUnitCount; } }
        [SerializeField] private float _scoreDeadUnityLimit = 150;
        public static float GetScoreDeadUnityLimit { get { return instance._scoreDeadUnityLimit; } }
        [SerializeField] private int _timeWaitBoss = 600;
        public static int GetTimeWaitBoss { get { return instance._timeWaitBoss; } }


        void Awake()
        {
            /*if (CardAndLevelSelectPanel.BigPortal && isPortalSetting == false)
            {
                GameObject.Destroy(gameObject);
                return;
            }
            else if (CardAndLevelSelectPanel.BigPortal == false && isPortalSetting)
            {
#if UNITY_EDITOR
                if (TEST_PORTAL == false)
                {

#endif
                    GameObject.Destroy(gameObject);
                    return;
#if UNITY_EDITOR
                }
#endif
            }
#if UNITY_EDITOR
            else if (CardAndLevelSelectPanel.BigPortal == false && isPortalSetting == false && TEST_PORTAL == true)
            {
                GameObject.Destroy(gameObject);
                return;
            }
#endif*/

            Time.fixedDeltaTime = timeStep;

            instance = this;
            thisT = transform;

            ObjectPoolManager.Init();

            BuildManager buildManager = (BuildManager)FindObjectOfType(typeof(BuildManager));
            if (buildManager != null)
                buildManager.Init();

            NodeGenerator nodeGenerator = (NodeGenerator)FindObjectOfType(typeof(NodeGenerator));
            if (nodeGenerator != null)
                nodeGenerator.Awake();
            PathFinder pathFinder = (PathFinder)FindObjectOfType(typeof(PathFinder));
            if (pathFinder != null)
                pathFinder.Awake();

            //�������� ��� �� ���������� ������� 
            PathTD[] paths = FindObjectsOfType(typeof(PathTD)) as PathTD[];
            if (paths.Length > 0)
            {
                for (int i = 0; i < paths.Length; i++)
                    paths[i].Init();
            }

            if (buildManager != null)
            {
                for (int i = 0; i < buildManager.buildPlatforms.Count; i++)
                    buildManager.buildPlatforms[i].Init();
            }


            gameObject.GetComponent<ResourceManager>().Init(true);

            PerkManager perkManager = (PerkManager)FindObjectOfType(typeof(PerkManager));
            if (perkManager != null)
                perkManager.Init();

            if (loadAudioManager)
            {
                Instantiate(Resources.Load("AudioManager", typeof(GameObject)));
            }

            if (rangeIndicator)
            {
                rangeIndicator = (Transform)Instantiate(rangeIndicator);
                rangeIndicator.parent = thisT;
                rangeIndicatorObj = rangeIndicator.gameObject;
            }
            projectorRange = rangeIndicator.GetComponentInChildren<Projector>();

            if (rangeIndicatorCone)
            {
                rangeIndicatorCone = (Transform)Instantiate(rangeIndicatorCone);
                rangeIndicatorCone.parent = thisT;
                rangeIndicatorConeObj = rangeIndicatorCone.gameObject;
            }
            ClearSelectedTower();

            TimeScaleManager.RefreshTimeScale();

            if (CardAndLevelSelectPanel.LevelDifficultySelect == 0)
            {
                _playerLife = PlayerLifeInCompany;

            }
            else if (CardAndLevelSelectPanel.LevelDifficultySelect == 1)
            {
                _playerLife = PlayerLifeIn1RedStar;
            }
            else
            {
                _playerLife = PlayerLifeIn2RedStar;
            }

            if (CardAndLevelSelectPanel.BigPortal)
            {
                _playerLife = PlayerLifeInBigPortal;

                _scoreDeadUnityLimit = _scoreDeadUnityLimit * Mathf.Pow(_scoreDeadUnityMultiplier,
                    (float)CardAndLevelSelectPanel.LevelDifficultySelect);
            }
        }


        // Use this for initialization
        void Start()
        {
            _playerInfo = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();

            UnitTower[] towers = FindObjectsOfType(typeof(UnitTower)) as UnitTower[];
            for (int i = 0; i < towers.Length; i++) BuildManager.PreBuildTower(towers[i]);

            //ignore collision between shootObject so they dont hit each other
            int soLayer = LayerManager.LayerShootObject();
            Physics.IgnoreLayerCollision(soLayer, soLayer, true);

            //playerLife=playerLifeCap;
            if (capLife) _playerLife = Mathf.Min(_playerLife, GetPlayerLifeCap());

            if (enableLifeGen) StartCoroutine(LifeRegenRoutine());
        }

        void OnEnable()
        {
            UnitCreep.onDestinationE += OnUnitReachDestination;
            Unit.onDestroyedE += OnUnitDestroyed;
            SpawnManager.onWaveClearedE += OnWaveCleared;
        }

        void OnDisable()
        {
            UnitCreep.onDestinationE -= OnUnitReachDestination;
            Unit.onDestroyedE -= OnUnitDestroyed;
            SpawnManager.onWaveClearedE -= OnWaveCleared;
        }

        private void OnWaveCleared(int waveID)
        {
            if (SpawnManager.IsAllWaveCleared())
            {
                GameWon();
            }
        }

        void OnUnitDestroyed(Unit unit)
        {
            if (unit.IsCreep)
            {
                if (unit.GetUnitCreep.lifeValue > 0)
                    GainLife(unit.GetUnitCreep.lifeValue);
            }
            else if (unit.IsTower)
            {
                if (unit.GetUnitTower == selectedTower)
                    _ClearSelectedTower();
            }

            switch (_gameType)
            {
                case _GameType.TimeBoss:
                    if(unit is UnitCreep)
                    {
                        var creep = (unit as UnitCreep);
                        if (SpawnManager.isBossSpawn && creep.isBossPortal && SpawnManager.GetActiveUnitCount <= 0)
                        {
                            GameWon();
                            return;
                        }
                        _scoreDeadUnitCount += creep.lifeCost;
                        if(_scoreDeadUnitCount >= _scoreDeadUnityLimit)
                        {
                            SpawnManager.SpawnBossPortal(true);
                        }
                    }
                    break;

                case _GameType.DefenseBase:
                    if (unit is UnityArtifact)
                    {
                        if ((unit as UnityArtifact).isMain)
                        {
                            GameLose();
                        }
                    }
                    break;
            }

        }

        void OnUnitReachDestination(UnitCreep unit)
        {
            /*if(CardAndLevelSelectPanel.BigPortal)
            {
                if (unit.isBossPortal == false)
                    return;
            }*/

            switch (_gameType)
            {
                case _GameType.Life:
                    _ApplyDamagePlayerLife(unit.lifeCost);
                    break;

                case _GameType.TimeBoss:
                    if (unit.isBossPortal)
                        _ApplyDamagePlayerLife(unit.lifeCost);
                    break;
            }
        }


        private void _ApplyDamagePlayerLife(float damage)
        {
            _playerLife = Mathf.Max(0, _playerLife - damage);
#if UNITY_ANDROID || UNITY_IOS
                        Handheld.Vibrate();
#endif
            if (onLifeE != null) onLifeE(-damage);

            if (_playerLife <= 0 && gameState != _GameState.Over)
            {
                GameLose();
            }

        }

        private void _GameUnlimitedEnd()
        {
            gameState = _GameState.Over;
            Debug.Log("Game Unlimited End!");

            if (CardAndLevelSelectPanel.StarsConfig != null && CardAndLevelSelectPanel.StarsConfig[0] < UIScorePanel.GetScore)
            {
                if (CardAndLevelSelectPanel.StarsConfig[1] < UIScorePanel.GetScore)
                {
                    if (CardAndLevelSelectPanel.StarsConfig[2] < UIScorePanel.GetScore)
                    {
                        Stars = 3;
                    }
                    else
                        Stars = 2;
                }
                else
                    Stars = 1;
            }
            else
                Stars = 0;


            PlayerManager.DM.EndLevel(CardAndLevelSelectPanel.IDLevel, CardAndLevelSelectPanel.LevelDifficultySelect + 1, Stars, CardAndLevelSelectPanel.RND,
                FPSCounter.AvgFps, CardAndLevelSelectPanel.IDMap, UIScorePanel.GetScore, GetPlayerLife(), Mathf.CeilToInt(TimeGame), ResponseEndLevel);
        }

        public static void GameLose()
        {
            if (CardAndLevelSelectPanel.Survival)
            {
                instance._GameUnlimitedEnd();
            }
            else
            {
                instance._GameLose();
            }
        }

        private void _GameLose()
        {
            gameState = _GameState.Over;
            Debug.Log("_GameLose!");
            Stars = -1;

            PlayerManager.DM.EndLevel(CardAndLevelSelectPanel.IDLevel, CardAndLevelSelectPanel.LevelDifficultySelect + 1, Stars, CardAndLevelSelectPanel.RND,
                FPSCounter.AvgFps, CardAndLevelSelectPanel.IDMap, UIScorePanel.GetScore, GetPlayerLife(), Mathf.CeilToInt(TimeGame), ResponseEndLevel);
        }

        private void ResponseEndLevel(ResponseData rp)
        {
            if (rp.ok == 0)
            {
                Debug.LogError("Error endLevel: " + rp.error.code + " -> " + rp.error.text);
                TooltipMessageServer.Show("�������� ������. ���������� �������� ��������.\r\n" + rp.error.text);
            }

            PlayerManager.SetPlayerData(rp.user);

            if (rp.ok == 1)
            {
                TopManager.great_new_place = rp.great_new_place == 1;
            }

            if (onGameOverE != null)
                onGameOverE(Stars, rp);
        }


        IEnumerator LifeRegenRoutine()
        {
            float temp = 0;
            while (true)
            {
                yield return new WaitForSeconds(1);
                temp += lifeRegenRate + PerkManager.GetLifeRegenModifier();
                int value = 0;
                while (temp >= 1)
                {
                    value += 1;
                    temp -= 1;
                }
                if (value > 0)
                    _GainLife(value);
            }
        }

        public static void GainLife(int value) { instance._GainLife(value); }
        public void _GainLife(int value) {
            _playerLife += value;
            if (capLife) _playerLife = Mathf.Min(_playerLife, GetPlayerLifeCap());
            if (onLifeE != null) onLifeE(value);
        }







        public static void StartGame()
        {
            //if game is not yet started, start it now
            instance._StartGame();
        }

        public void _StartGame()
        {
            _isGameStarted = true;
            gameState = _GameState.Play;
            if (onGameStarted != null)
            {
                onGameStarted(GetLevelID());
            }
        }

        public static void GameWon()
        {
            if (instance._playerLife > 0 && IsGameOver == false)
            {
                instance.StartCoroutine(instance._GameWon());
            }
        }
        public IEnumerator _GameWon()
        {
            ResumeGame(); //call to reset ff speed
            yield return new WaitForSeconds(0.0f);
            Debug.Log("_GameWon");

            CardAndLevelSelectPanel.isLastLevelWin = true;
            gameState = _GameState.Over;
            Stars = 0;
            if (CardAndLevelSelectPanel.IDLevel < 0)
            {
                if (_gameType == _GameType.DefenseBase)
                {
                    //float p = UnityArtifact.mainArtifact.HP * 100f / UnityArtifact.mainArtifact.fullHP;

                    if (_playerLife >= 90)
                        Stars = 3;
                    else if (_playerLife >= 50)
                        Stars = 2;
                    else
                        Stars = 1;
                }
                else
                {

                    if (_playerLife >= 5)
                        Stars = 3;
                    else if (_playerLife >= 3)
                        Stars = 2;
                    else
                        Stars = 1;
                }
            }
            else
            {
                if (CardAndLevelSelectPanel.LevelDifficultySelect == 0)
                {
                    if (_gameType == _GameType.DefenseBase)
                    {
                        float p = UnityArtifact.mainArtifact.HP * 100f / UnityArtifact.mainArtifact.fullHP;

                        if (p >= 90f)
                            Stars = 3;
                        else if (p >= 50f)
                            Stars = 2;
                        else
                            Stars = 1;
                    }
                    else
                    {

                        if (_playerLife >= 19)
                            Stars = 3;
                        else if (_playerLife >= 10)
                            Stars = 2;
                        else
                            Stars = 1;
                    }
                }
                else if (CardAndLevelSelectPanel.LevelDifficultySelect == 1 || CardAndLevelSelectPanel.LevelDifficultySelect == 2)
                {
                    if (_playerLife >= PlayerLifeIn1RedStar)
                        Stars = 5;
                    else
                        Stars = 4;
                }
            }

            PlayerManager.DM.EndLevel(CardAndLevelSelectPanel.IDLevel, CardAndLevelSelectPanel.LevelDifficultySelect + 1, Stars, CardAndLevelSelectPanel.RND,
                FPSCounter.AvgFps, CardAndLevelSelectPanel.IDMap, UIScorePanel.GetScore, GetPlayerLife(), Mathf.CeilToInt(TimeGame), ResponseEndLevel);
        }




        public UnitTower selectedTower;
        public static UnitTower GetSelectedTower() { return instance.selectedTower; }
        public static UnitTower Select(Vector3 pointer)
        {
            int layer = LayerManager.LayerTower();

            LayerMask mask = 1 << layer;
            Ray ray = Camera.main.ScreenPointToRay(pointer);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask) == false)
                return null;

            var tower = hit.transform.GetComponent<UnitTower>();
            if (tower.CanSelected == false)
                return null;

            SelectTower(tower);

            return instance.selectedTower;
        }

        public static void SelectHero(UnitHero hero)
        {
            instance._SelectHero(hero);
        }

        public void _SelectHero(UnitHero hero)
        {
            _ClearSelectedTower();


            float range = hero.GetRange();

            Transform indicatorT = !hero.directionalTargeting ? rangeIndicator : rangeIndicatorCone;

            if (indicatorT != null)
            {
                indicatorT.position = hero.thisT.position;
                indicatorT.localScale = new Vector3(2 * range, 1, 2 * range);
                indicatorT.parent = hero.thisT;

                if (hero.directionalTargeting)
                    indicatorT.localRotation = Quaternion.identity * Quaternion.Euler(0, hero.dirScanAngle, 0);

                indicatorT.gameObject.SetActive(true);
            }
        }

        public static void SelectTower(UnitTower tower) { instance._SelectTower(tower); }
        public void _SelectTower(UnitTower tower)
        {
            _ClearSelectedTower();

            selectedTower = tower;

            if (tower.type == _TowerType.Block || tower.type == _TowerType.Resource) return;

            float range = tower.GetRange();

            Transform indicatorT = !tower.directionalTargeting ? rangeIndicator : rangeIndicatorCone;

            if (indicatorT != null) {
                indicatorT.position = tower.thisT.position;
                indicatorT.localScale = new Vector3(2 * range, 1, 2 * range);
                indicatorT.parent = tower.thisT;
                if (projectorRange != null)
                {
                    projectorRange.orthographicSize = range + 0.2f;
                }

                if (tower.directionalTargeting) indicatorT.localRotation = Quaternion.identity * Quaternion.Euler(0, tower.dirScanAngle, 0);

                indicatorT.gameObject.SetActive(true);
            }
        }

        public static void TowerScanAngleChanged(UnitTower tower) {
            instance.rangeIndicatorCone.localRotation = tower.thisT.localRotation * Quaternion.Euler(0, tower.dirScanAngle, 0);
        }

        public static void ClearSelectedTower() { instance._ClearSelectedTower(); }
        public void _ClearSelectedTower() {
            selectedTower = null;

            rangeIndicatorObj.SetActive(false);
            rangeIndicatorConeObj.SetActive(false);

            rangeIndicator.parent = thisT;
            rangeIndicatorCone.parent = thisT;
        }


        private _GameState _bufferPause;
        public static _GameState bufferStatePause { get { return instance._bufferPause; } }
        public static void PauseGame()
        {
            if (instance == null)
                return;
            instance._bufferPause = instance.gameState;

            instance.gameState = _GameState.Pause;
            TimeScaleManager.SetTimeScale(0);
        }
        public static void ResumeGame() {
            instance.gameState = instance._bufferPause;
            TimeScaleManager.RefreshTimeScale();
        }


        public static float GetSellTowerRefundRatio() {
            return sellTowerRefundRatio;
        }


        private void Update()
        {
            if (_isGameStarted)
            {
                TimeGame += Time.deltaTime;
                if(_gameType == _GameType.TimeBoss)
                {
                    if (TimeGame > _timeWaitBoss)
                    {
                        SpawnManager.SpawnBossPortal(true);
                    }
                }
            }
        }
		
	}

}