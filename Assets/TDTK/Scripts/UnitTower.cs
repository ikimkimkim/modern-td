﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK {

	//~ public enum _TowerType{TurretTower, AOETower, DirectionalAOETower, SupportTower, ResourceTower, Mine, Block}
	public enum _TowerType{Turret, AOE, Support, Resource, Mine, Block }
	public enum _TargetMode{Hybrid, Air, Ground, OnlyArtifact}
   

    public class UnitTower : Unit
    {
        

        public delegate void TowerSoldHandler(UnitTower tower);
		public static event TowerSoldHandler onSoldE;                                   //listen by TDTK only
        public event TowerSoldHandler onThisStartSoldE;

        public delegate void ConstructionStartHandler(UnitTower tower);
		public static event ConstructionStartHandler onConstructionStartE;		//listen by TDTK only
		
		public delegate void TowerUpgradedHandler(UnitTower tower);
		public static event TowerUpgradedHandler onUpgradedE; 
        public event TowerUpgradedHandler onThisUpgradedE;

        public delegate void ConstructionCompleteHandler(UnitTower tower);
		public static event ConstructionCompleteHandler onConstructionCompleteE;
		
		
		public delegate void PlayConstructAnimation();
		public PlayConstructAnimation playConstructAnimation;

		public delegate void PlayDeconstructAnimation();
		public PlayDeconstructAnimation playDeconstructAnimation;

        [Header("Tower")]
        public _TowerType type = _TowerType.Turret;

        public bool CanSelected = true;

        public bool disableInBuildManager=false;	//when set to true, tower wont appear in BuildManager buildList
		
		private enum _Construction{None, Constructing, Deconstructing}
		private _Construction construction=_Construction.None;
		public bool _IsInConstruction(){ return construction==_Construction.None ? false : true; }


		public GameObject buildingEffect;
		public GameObject builtEffect;

        public GameObject ResurceObj;

        #region Card
        

        public List<CardProperties> Properties
        {
            get { return MyCard.Properties; }
        }


        public void WriteValueCardInTower(Card card, bool BuldTower)
        {
            if (card == null)
                Debug.LogError("Write card in tower is null!");
            int saveCountAttakcs = stats[0].CountAttakcs;
            stats = new List<UnitStat>();
            stats.Add(CraftLevelData.GetStatGrade(card));

            stats[0].CountAttakcs = saveCountAttakcs;

            //MyLog.Log("New Tower "+BuldTower+" card pref id "+card.prefabID+" "+card.unitName);

            value = stats[currentActiveStat].cost;

            prefabID = card.prefabID;
            disableInBuildManager = card.disableInBuildManager;
            unitName = card.unitName;
            if (card.LevelTower == 1 && BuldTower || BuldTower==false)
                MyCard = card.Clone();
            else
                MyCard = card;

            foreach (CardProperties cp in Properties)
            {
                cp.Apply(this);
                if (cp.ID == CardProperties.IDCoomplect && BuldTower)
                    PropertiesHelp.listCoomplectPropertiesBild.Add(cp);
            }

            if (BuldTower)
            {
                PropertiesHelp.ActivePorioerties(Properties.FindAll(i => i.ID != CardProperties.IDCoomplect));
                PropertiesHelp.RefreshAllCoomplectProperties();
            }
            else
            {
                PropertiesHelp.ActivePorioerties(Properties);
            }

        }

        #endregion

        public override void Awake()
        {
            base.Awake();			
			if(stats.Count==0)
                stats.Add(new UnitStat());
            gameObject.layer = LayerManager.LayerTower();
        }

        public override void OnEnable()
        {
            UnitsVisionSystem.AddUnit(this);
            base.OnEnable();
        }
        public override void OnDisable()
        {
            UnitsVisionSystem.RemoveUnit(this);
            base.OnDisable();
        }

        public void InitTower(int ID, int startCountKill=0, float allDmg=0){

            MyLog.Log(name + " init!");

            Init(startCountKill,allDmg);
			
			instanceID=ID;

			
			int rscCount=ResourceManager.GetResourceCount();
			for(int i=0; i<stats.Count; i++){
				UnitStat stat=stats[i];
				if(stat.rscGain.Count!=rscCount){
					while(stat.rscGain.Count<rscCount) stat.rscGain.Add(0);
					while(stat.rscGain.Count>rscCount) stat.rscGain.RemoveAt(stat.rscGain.Count-1);
				}
			}

            switch(type)
            {
                case _TowerType.Turret:
                    StartCoroutine(ScanForTargetRoutine());
                    StartCoroutine(TurretRoutine());
                    break;
                case _TowerType.AOE:
                    StartCoroutine(AOETowerRoutine());
                    break;
                case _TowerType.Support:
                    //StartCoroutine(SupportRoutine());
                    break;
                case _TowerType.Resource:
                    StartCoroutine(ResourceTowerRoutine());
                    break;
                case _TowerType.Mine:
                    StartCoroutine(MineRoutine());
                    break;
            }

            UnitCreep.onDestroyedE += UnitCreep_onDestroyedE;
		}

        private void UnitCreep_onDestroyedE(Unit unit)
        {
            if(unit.IsCreep)
            {
                if (unit.KillMyUnit!=null && unit.KillMyUnit is Unit)
                {
                    if(unit.KillMyUnit as Unit == this)
                    {
                        countKillUnitCurrentGame++;
                    }
                }
            }
        }

        [HideInInspector] public float builtDuration;
		[HideInInspector] public float buildDuration;
		public void UnBuild(){ StartCoroutine(Building(stats[currentActiveStat].unBuildDuration, true));	}
		public void Build(){ StartCoroutine(Building(stats[currentActiveStat].buildDuration));	}
		IEnumerator Building(float duration, bool reverse=false){		//reverse flag is set to true when selling (thus unbuilding) the tower
			construction=!reverse ? _Construction.Constructing : _Construction.Deconstructing;
			
			builtDuration=0;
			buildDuration=duration;
			
			if(onConstructionStartE!=null)
                onConstructionStartE(this);

			if(buildingEffect!=null)
                ObjectPoolManager.Spawn(buildingEffect, thisT.position, thisT.rotation);
			
			yield return null;
            if (!reverse && playConstructAnimation != null)
                playConstructAnimation();
            else if (reverse)
            {
                if (onThisStartSoldE != null)
                    onThisStartSoldE(this);
                if(playDeconstructAnimation != null)
                    playDeconstructAnimation();
            }

			while(true){
				yield return null;
				builtDuration+=Time.deltaTime;
				if(builtDuration>buildDuration) break;
			}
			
			construction=_Construction.None;

            if (ResurceObj != null) GameObject.Destroy(ResurceObj);
            //Debug.Log("UT -> Building-> reverse=" + reverse);
            if (!reverse) {
                
				if(onConstructionCompleteE != null) onConstructionCompleteE (this);
				if(builtEffect!=null) ObjectPoolManager.Spawn(builtEffect, thisT.position, thisT.rotation);
			}
			
			if(reverse){
				if(onSoldE!=null)
                    onSoldE(this);

                if (occupiedPlatform!=null)
                    occupiedPlatform.UnbuildTower(occupiedNode);
				ResourceManager.GainResource(GetValue());

                foreach (CardProperties cp in this.Properties)
                {

                    if (cp.ID == CardProperties.IDCoomplect)
                        PropertiesHelp.listCoomplectPropertiesBild.Remove(cp);
                    else
                        cp.Refresh(false);
                }
                PropertiesHelp.RefreshAllCoomplectProperties();
                /*if (PropertiesHelp.aBuldTower != null)
                    PropertiesHelp.aBuldTower(true);*/
                Dead();

                DeadEndAnimEvent();                
            }


		}
		public float GetBuildProgress(){ 
			if(construction==_Construction.Constructing) return builtDuration/buildDuration;
			if(construction==_Construction.Deconstructing) return (buildDuration-builtDuration)/buildDuration;
			else return 0;
		}
		
		
		public void Sell(){
			UnBuild();
		}
		
		
		private bool isSampleTower;
		private Card srcTower;
		public void SetAsSampleTower(Card card){
			isSampleTower=true;
			srcTower=card;
			thisT.position=new Vector3(0, 9999, 0);
		}
		public bool IsSampleTower{ get { return isSampleTower; } }

		public IEnumerator DragNDropRoutine(){
			GameControl.SelectTower(this);
			yield return null;
			
			
			#if UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY
				_TileStatus status=_TileStatus.NoPlatform;
				while(Input.touchCount>=1){
					Vector3 pos=Input.touches[0].position;
					
					status=BuildManager.CheckBuildPoint(pos, -1, prefabID);
					
					if(status==_TileStatus.Available){
						BuildInfo buildInfo=BuildManager.GetBuildInfo();
						thisT.position=buildInfo.position;
						thisT.rotation=buildInfo.platform.thisT.rotation;
					}
					else{
						Ray ray = Camera.main.ScreenPointToRay(pos);
						RaycastHit hit;
						if(Physics.Raycast(ray, out hit, Mathf.Infinity)) thisT.position=hit.point;
						//this there is no collier, randomly place it 30unit from camera
						else thisT.position=ray.GetPoint(30);
					}
					
					if(Input.touches[0].phase==TouchPhase.Ended){
						string exception=BuildManager.BuildTower(srcTower);
						if(exception!="") GameControl.DisplayMessage(exception);
						else BuildManager.ClearBuildPoint();
						break;
					}
					
					yield return null;
				}
				
				//if(status==_TileStatus.Available){
				//	string exception=BuildManager.BuildTower(srcTower);
				//	if(exception!="") GameControl.DisplayMessage(exception);
				//}
				//else{
				//	GameControl.DisplayMessage("cancelled");
				//	BuildManager.ClearBuildPoint();
				//}
				
				GameControl.ClearSelectedTower();
				thisObj.SetActive(false);
				
			#else
				while(true){
					Vector3 pos=Input.mousePosition;
					
					_TileStatus status=BuildManager.CheckBuildPoint(pos, -1, prefabID);
					
					if(status==_TileStatus.Available){
						BuildInfo buildInfo=BuildManager.GetBuildInfo();
						thisT.position=buildInfo.position;
						thisT.rotation=buildInfo.platform.thisT.rotation;
					}
					else{
						Ray ray = Camera.main.ScreenPointToRay(pos);
						RaycastHit hit;
						if(Physics.Raycast(ray, out hit, Mathf.Infinity)) thisT.position=hit.point;
						//this there is no collier, randomly place it 30unit from camera
						else thisT.position=ray.GetPoint(30);
					}
					
					
					//left-click, build
					if(Input.GetMouseButtonDown(0) && !UIUtilities.IsCursorOnUI()){
						//if current mouse point position is valid, build the tower
						if(status==_TileStatus.Available){
							string exception=BuildManager.BuildTower(srcTower);
							if(exception!="") GameControl.DisplayMessage(exception);
						}
						else{
							BuildManager.ClearBuildPoint();
						}
						GameControl.ClearSelectedTower();
						thisObj.SetActive(false);
						break;
					}
					
					//right-click, cancel
					if(Input.GetMouseButtonDown(1) || GameControl.GetGameState()==_GameState.Over){
						GameControl.ClearSelectedTower();
						BuildManager.ClearBuildPoint();
						thisObj.SetActive(false);
						break;
					}
					
					yield return null;
				}
			#endif
				
			thisT.position=new Vector3(0, 9999, 0);
		}
		
		public PlatformTD occupiedPlatform;
		public NodeTD occupiedNode;
		public void SetPlatform(PlatformTD platform, NodeTD node)
        {
			occupiedPlatform=platform;
			occupiedNode=node;
		}
		
        IEnumerator AOETowerRoutine()
        {
			if(targetMode==_TargetMode.Hybrid)
            {
                VitionCollider.gameObject.layer = LayerManager.LayerHybridTower();
                LayerMask mask1=1<<LayerManager.LayerCreep();
				LayerMask mask2=1<<LayerManager.LayerCreepF();
				maskTarget=mask1 | mask2;
			}
			else if(targetMode==_TargetMode.Air)
            {
                VitionCollider.gameObject.layer = LayerManager.LayerOnlyAir();
                maskTarget =1<<LayerManager.LayerCreepF();
			}
			else if(targetMode==_TargetMode.Ground)
            {
                VitionCollider.gameObject.layer = LayerManager.LayerOnlyGround();
                maskTarget =1<<LayerManager.LayerCreep();
			}

            while (true)
            {
                yield return new WaitForSeconds(GetCooldown());

                while (stunned || IsInConstruction()) yield return null;

                Transform soPrefab = GetShootObjectT();
                if (soPrefab != null) ObjectPoolManager.Spawn(soPrefab, thisT.position, thisT.rotation);

                Collider[] cols = Physics.OverlapSphere(thisT.position, GetRange(), maskTarget);
                if (cols.Length > 0)
                {
                    for (int i = 0; i < cols.Length; i++)
                    {
                        Unit unit = cols[i].transform.GetComponent<Unit>();
                        if (unit == null && !unit.dead) continue;

                        AttackInstance attInstance = new AttackInstance();
                        attInstance.srcUnit = this;
                        attInstance.tgtUnit = unit;
                        attInstance.impactPoint = unit.getPosition;
                        attInstance.Process();
                        unit.ApplyEffect(attInstance);


                    }
                }
            }
		}
		
		IEnumerator ResourceTowerRoutine(){
			while(true)
            {

                if (GameControl.IsGameStarted() == false)
                {
                    yield return null;
                    continue;
                }
                yield return new WaitForSeconds(GetCooldown());

                while (stunned || IsInConstruction())
                {
                    yield return null;
                    continue;
                }
				Transform soPrefab=GetShootObjectT();
				if(soPrefab!=null) Instantiate(soPrefab, thisT.position, thisT.rotation);
                if (ManaCollectManager.instance != null)
                {
                    ResurceObj = ManaCollectManager.instance.CreadMana(transform);
                    
                    while(ResurceObj != null)
                    {
                        yield return null;
                    }
                    MyLog.Log("Resource Add!");
                }
				ResourceManager.GainResource(GetResourceGain(), PerkManager.GetRscTowerGain());
			}
		}
		
		IEnumerator MineRoutine(){
			LayerMask maskTarget=1<<LayerManager.LayerCreep();
            VitionCollider.gameObject.layer = LayerManager.LayerOnlyGround();

            while (true){
				if(!dead && !IsInConstruction()){
					Collider[] cols=Physics.OverlapSphere(thisT.position, GetRange(), maskTarget);
					if(cols.Length>0){
						
						Collider[] colls=Physics.OverlapSphere(thisT.position, GetAOERadius(), maskTarget);
						for(int i=0; i<colls.Length; i++){
							Unit unit=colls[i].transform.GetComponent<Unit>();
							if(unit==null && !unit.dead) continue;

                                AttackInstance attInstance = new AttackInstance();
                                attInstance.srcUnit = this;
                                attInstance.tgtUnit = unit;
                                attInstance.Process();

                                unit.ApplyEffect(attInstance);
                            
						}
						
						Transform soPrefab=GetShootObjectT();
						if(soPrefab!=null) Instantiate(soPrefab, thisT.position, thisT.rotation);
						
						Dead();
					}
				}
				yield return new WaitForSeconds(0.1f);
			}
		}
		
		
		
		//private int level=1;
		public int GetLevel(){ return MyCard.LevelTower; }
		//public void SetLevel(int lvl){ level=lvl; }
		
		public UnitTower prevLevelTower;
		public List<UnitTower> nextLevelTowerList=new List<UnitTower>();
		public int ReadyToBeUpgrade(){
            //Debug.Log("Tower ReadyToBeUpgrade");
            if (currentActiveStat<stats.Count-1) return 1;
            if (MyCard == null)
            {
                Debug.Log("Tower ReadyToBeUpgrade MyCard is null");
                return 0;
            }

            if (MyCard.NextLevelTower.Count == 0)
                return 0;
            else
                return 1;

           /* if (MyCard.LevelCard >= MyCard.NextLevelTower[0].LevelTower)
                return 1;
            else
                return 0;

			if(MyCard.NextLevelTower.Count>0)
            {
				if(MyCard.NextLevelTower.Count>=2 && MyCard.NextLevelTower[1]!=null)
                    return 2;
				else if(MyCard.NextLevelTower.Count>=1 && MyCard.NextLevelTower[0]!=null)
                    return 1;
			}
			return 0;*/
		}
		public string Upgrade(int ID=0){	//ID specify which nextTower to use
            
            if (currentActiveStat<stats.Count-1) return UpgradeToNextStat();
			else if(nextLevelTowerList.Count>0) return UpgradeToNextTower(ID);
			return "Tower is at maximum level!";
		}
		public string UpgradeToNextStat(){
			List<float> cost= MyCard.GetCost();
			int suffCost=ResourceManager.HasSufficientResource(cost);
			if(suffCost==-1){
                //level+=1;
                Debug.LogError("нету увеличение уровня!");
				currentActiveStat+=1;
				ResourceManager.SpendResource(cost);
				AddValue(stats[currentActiveStat].cost);
				Build();
				
				if(onUpgradedE!=null) onUpgradedE(this);
                if (onThisUpgradedE != null) onThisUpgradedE(this);

                return "";
			}
			return "Insufficient Resource";
		}
		public string UpgradeToNextTower(int ID=0)
        {
            Debug.Log("Tower UpgradeToNextTower");

            Card nextLevelCard = MyCard.NextLevelTower[Mathf.Clamp(ID, 0, nextLevelTowerList.Count)];

            List<float> cost = nextLevelCard.GetCost();
            int suffCost = ResourceManager.HasSufficientResource(cost);
            if (suffCost == -1)
            {
                StartUpgradeTower();
                return "";
            }

            return "Insufficient Resource";
		}

        public void StartUpgradeTower(int ID = 0)
        {
            if (MyCard == null)
            {
                Debug.LogWarningFormat("Card tower {0} is null!", name);
                return;
            }
            if (MyCard.NextLevelTower==null)
            {
                Debug.LogWarningFormat("Next level tower {0} is null!", name);
                return;
            }

            Card nextLevelCard = MyCard.NextLevelTower[
                Mathf.Clamp(ID, 0, nextLevelTowerList.Count)];
            List<float> cost = nextLevelCard.GetCost();
            ResourceManager.SpendResource(cost);
            dead = true;

            ClearProperties();

            GameObject towerObj = (GameObject)ObjectPoolManager.Spawn(nextLevelCard.towerObj, thisT.position, thisT.rotation);
            UnitTower towerInstance = towerObj.GetComponent<UnitTower>();
            towerInstance.name = towerInstance.name + instanceID + "|" + nextLevelCard.LevelTower;
            towerInstance.InitTower(instanceID, countKillUnitCurrentGame, allDmgCurrentGame);
            towerInstance.WriteValueCardInTower(nextLevelCard, true);
            towerInstance.SetPlatform(occupiedPlatform, occupiedNode);
            towerInstance.AddValue(value);
            //towerInstance.SetLevel(level + 1);
            towerInstance.Build();
            GameControl.SelectTower(towerInstance);

            if (onUpgradedE != null) onUpgradedE(towerInstance);
            if (onThisUpgradedE != null) onThisUpgradedE(towerInstance);

            MyCard = null;

            ObjectPoolManager.Unspawn(thisObj);
        }
		
		
		//only use cost from sample towers or in game tower instance, not the prefab
		//ID is for upgrade path
		public List<float> GetCost(int ID=0){
			List<float> cost=new List<float>();
			float multiplier=1;
			if(isSampleTower)
            {
				multiplier=GetBuildCostMultiplier();
				cost=new List<float>(stats[currentActiveStat].cost);
			}
			else
            {
				multiplier=GetUpgradeCostMultiplier();
				if(currentActiveStat<stats.Count-1)
                    cost =new List<float>( stats[currentActiveStat+1].cost );
				else if(ID<nextLevelTowerList.Count && nextLevelTowerList[ID]!=null)
                    cost =new List<float>( nextLevelTowerList[ID].stats[0].cost );
			}
			for(int i=0; i<cost.Count; i++)
                cost[i]=(int)Mathf.Round(cost[i]*multiplier);
			return cost;
		}
		private float GetBuildCostMultiplier(){ return 1-PerkManager.GetTowerBuildCost(prefabID); }
		private float GetUpgradeCostMultiplier(){ return 1-PerkManager.GetTowerUpgradeCost(prefabID); }
		
		
		public List<float> value=new List<float>();
		//apply the refund ratio from gamecontrol
		public List<float> GetValue(){
			List<float> newValue=new List<float>();
			for(int i=0; i<value.Count; i++) newValue.Add((value[i]*GameControl.GetSellTowerRefundRatio()));
			return newValue;
		}
		//called when tower is upgraded to bring the value forward
		public void AddValue(List<float> list){
			for(int i=0; i<value.Count; i++){
				value[i]+=list[i];
			}
		}
		
		
		public int FPSWeaponID=-1;
		
		
		public bool DealDamage(){
			if(type==_TowerType.Turret || type==_TowerType.AOE || type==_TowerType.Mine) return true;
			return false;
		}

        public override void Dead(bool getRsc = true)
        {
            base.Dead(getRsc);

            Destroy();

            ObjectPoolManager.Unspawn(thisObj);
        }

        public void Destroy()
        {
            if (occupiedPlatform != null)
            {
                occupiedPlatform.UnbuildTower(occupiedNode);
            }
            UnitCreep.onDestroyedE -= UnitCreep_onDestroyedE;
        }
		
		//not compatible with PointNBuild mode
		void OnMouseEnter()
        {
			if(UIUtilities.IsCursorOnUI()) return;
			if(AbilityManager.InTargetSelectMode()) return;
			BuildManager.ShowIndicator(this);
		}
        void OnMouseExit()
        {// Debug.Log("OnMousExit"); 
            BuildManager.HideIndicator();
        }


        public override bool IsInConstruction() { return _IsInConstruction(); }
    }

}