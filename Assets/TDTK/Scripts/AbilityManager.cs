﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System;
using TDTK;

namespace TDTK
{

    public class AbilityManager : MonoBehaviour
    {

        public static event Action<Ability> onUpgradeAbility;

        public delegate void AddNewAbilityHandler(Ability ab);
        public static event AddNewAbilityHandler onAddNewAbilityE;		//fire when a new ability is added via perk

        //public delegate void AbilityReadyHandler(Ability ab);
        //public static event AbilityReadyHandler onAbilityReadyE;			//listen by TDTK only

        public delegate void ABActivatedHandler(Ability ab);
        public static event ABActivatedHandler onAbilityActivatedE;		//fire when an ability is used

        public delegate void ABTargetSelectModeHandler(bool flag);
        public static event ABTargetSelectModeHandler onTargetSelectModeE;	//fire when enter/exit target selection for ability

        //public delegate void EnergyHandler(float valueChanged);
        //public static event EnergyHandler onEnergyE;					//listen by TDTK only

        //public delegate void EnergyFullHandler();
        //public static event EnergyFullHandler onEnergyFullE;			//listen by TDTK only


        //[HideInInspector] 
        public List<int> unavailableIDList = new List<int>();	//ID list of perk available for this level, modified in editor
        //[HideInInspector] 
        public List<Card> abilityCardList = new List<Card>();	//actual ability list, filled in runtime based on unavailableIDList
        public static List<Card> GetAbilityCardList() { return instance.abilityCardList; }

        public Transform defaultIndicator;		//generic indicator use for ability without any specific indicator

        private bool inTargetSelectMode = false;
        public static bool InTargetSelectMode() { return instance == null ? false : instance.inTargetSelectMode; }

        private bool validTarget = false;	//used for targetSelectMode, indicate when the cursor is in a valid position or on a valid target

        public int selectedAbilityID = -1;

        public static int GetSelectedAbilityID()
        {
            return instance.selectedAbilityID;
        }
        public Transform currentIndicator;		//active indicator in used

        public bool startWithFullEnergy = false;
        public bool onlyChargeOnSpawn = false;
        public float energyRate = 2;
        public float fullEnergy = 100;
        public float energy = 0;

        public Effect2D LifeEffect;
        public Effect2D GoldEffect;

        private Transform thisT;
        private static AbilityManager instance;
        //bool BlockAbilityUntilButtonUp = false;
        public static bool IsOn() { return instance == null ? false : true; }
        ServerManager _server;
        PlayerManager _player;

        private int CounterCastAb;

        private void Awake()
        {
            instance = this;
            thisT = transform;
        }
        void Start()
        {

            if (startWithFullEnergy) energy = fullEnergy;

            //List<Ability> dbList = AbilityDB.Load();

            abilityCardList = new List<Card>();
            /*for (int i = 0; i < dbList.Count; i++)
            {
                if (!unavailableIDList.Contains(dbList[i].ID))
                {
                    abilityList.Add(dbList[i].Clone());
                }
            }*/

            List<Card> AbilityCards = PlayerManager.getAbilityCards;
            for (int i = 0; i < AbilityCards.Count; i++)
            {
                if (AbilityCards[i].disableInBuildManager == false)
                {
                    abilityCardList.Add(AbilityCards[i]);
                    SetAdilityDataCard(abilityCardList[abilityCardList.Count-1].abilityObj, AbilityCards[i]);
                    abilityCardList[abilityCardList.Count - 1].abilityObj.ID = abilityCardList.Count - 1;
                }
            }

            //for (int i = 0; i < abilityList.Count; i++)
            //    abilityList[i].ID = i;

            if (defaultIndicator)
            {
                defaultIndicator = (Transform)Instantiate(defaultIndicator);
                defaultIndicator.parent = thisT;
#if UNITY_ANDROID || UNITY_IOS
                defaultIndicator.GetComponentInChildren<MeshRenderer>().enabled = false;
#endif
                defaultIndicator.gameObject.SetActive(false);
            }

            //for (int i = 0; i < abilityList.Count - 2; i++) //-2 потому что последние 2 способности используют отдельные эффекты
            //    ObjectPoolManager.New(abilityList[i].effectObj, 1);
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
            //abilityList[2].requireTargetSelection = false;
            //abilityList[4].requireTargetSelection = false;
            //abilityList[5].requireTargetSelection = false;
            //abilityList[6].requireTargetSelection = false;
#endif


            _server = (GameObject.FindGameObjectWithTag("PlayerInfo") as GameObject).GetComponent<ServerManager>();
            _player = _server.GetComponent<PlayerManager>();

            UIAbilityButton.instance.StartUI(abilityCardList);

            CounterCastAb = 0;
        }

        private void SetAdilityDataCard(Ability ab, Card card)
        {
            ab.MyCard = card;
            var stat = CraftLevelData.GetStatGrade(card);
            ab.cooldown = stat.cooldown;
            SetAbilityDmg(ab, card);

            //ab.effect.slow = card.stats[0].slow.Clone();
            //ab.effect.dot = card.stats[0].dot.Clone();
            
            ab.cost = card.stats[0].cost[1];

            foreach (CardProperties cp in card.Properties)
            {
                cp.Apply(ab);
            }
            PropertiesHelp.ActivePorioerties(card.Properties.FindAll(i => i.ID != CardProperties.IDCoomplect));
        }

        public static void SetAbilityDmg(Ability ab,Card card)
        {
            var stat = CraftLevelData.GetStatGrade(card);
            if (ab.MultiCastAbility && ab.effect.applyDamage)
            {
                ab.effect.damageMax = stat.damageMax / ab.MultiCastNumber;
                ab.effect.damageMin = stat.damageMin / ab.MultiCastNumber;
            }
            else
            {
                ab.effect.damageMax = stat.damageMax;
                ab.effect.damageMin = stat.damageMin;
            }
        }

        public static void UpdateAbility(int ID,Card newCardAbility) { instance._UpdateAbility(ID, newCardAbility); }
        public void _UpdateAbility(int id, Card c)
        {
            for (int i = 0; i < abilityCardList[id].Properties.Count; i++)
            {
                abilityCardList[id].Properties[i].Cancel();
            }
            abilityCardList[id] = c;
            SetAdilityDataCard(abilityCardList[id].abilityObj, c);
            abilityCardList[id].abilityObj.ID = id;

            if (onUpgradeAbility != null)
                onUpgradeAbility(abilityCardList[id].abilityObj);
        }




        void OnDestroy() { instance = null; }


        public static void AddNewAbility(Ability ab) { instance._AddNewAbility(ab); }
        public void _AddNewAbility(Ability ab)
        {
            MyLog.LogError("Add new ability Нужно переработать под карты!", MyLog.Type.build);
            //for (int i = 0; i < abilityCardList.Count; i++) { if (ab.ID == abilityCardList[i].abilityObj.ID) return; }
            //abilityList.Add(ab.Clone());
            //if (onAddNewAbilityE != null) onAddNewAbilityE(ab);
        }




        void FixedUpdate()
        {
            if ((onlyChargeOnSpawn && GameControl.IsGameStarted()) || !onlyChargeOnSpawn)
            {
                if (energy < fullEnergy)
                {
                    float valueGained = Time.fixedDeltaTime * GetEnergyRate();
                    energy += valueGained;
                    energy = Mathf.Min(energy, GetEnergyFull());
                }
            }
        }

        // Update is called once per frame
        void Update()
        {
#if (UNITY_WEBGL || UNITY_WEBPLAYER) || UNITY_EDITOR
            SelectAbilityTarget();
#endif
#if UNITY_ANDROID || UNITY_IOS
            if (Input.touchCount == 0)
            {
                BlockAbilityUntilButtonUp = false;
            }
            if (!BlockAbilityUntilButtonUp)
            {
                SelectAbilityTarget();
            }
#endif

        }


        //called in every frame, execute if there's an ability is selected and pending target selection
        //use only mouse input atm.
        void SelectAbilityTarget()
        {
            if (selectedAbilityID < 0) return;



            //only cast on terrain and platform
            /* 
             LayerMask mask = 1 << LayerManager.LayerPlatform();
             int terrainLayer = LayerManager.LayerTerrain();
             if (terrainLayer >= 0) mask |= 1 << terrainLayer;
             */
            LayerMask mask = 1 << LayerManager.LayerTerrain();

            Ability ability = abilityCardList[selectedAbilityID].abilityObj;

            if (ability.singleUnitTargeting)
            {
                Debug.LogWarning("Ability single targeting!");
                if (ability.targetType == Ability._TargetType.Hybrid)
                {
                    mask |= 1 << LayerManager.LayerTower();
                    mask |= 1 << LayerManager.LayerCreep();
                }
                else if (ability.targetType == Ability._TargetType.Friendly) mask |= 1 << LayerManager.LayerTower();
                else if (ability.targetType == Ability._TargetType.Hostile) mask |= 1 << LayerManager.LayerCreep();
            }


#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8
            Unit targetUnit = null;
            if (Input.touchCount >= 1)
            {
                Camera mainCam = Camera.main;
                if (mainCam != null)
                {
                    Ray ray = mainCam.ScreenPointToRay(Input.touches[0].position);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
                    {
                        currentIndicator.position = hit.point + new Vector3(0, 0.1f, 0);
                        validTarget = true;

                        if (ability.singleUnitTargeting)
                        {
                            targetUnit = hit.transform.GetComponent<Unit>();
                            if (targetUnit != null) currentIndicator.position = targetUnit.thisT.position;
                            else validTarget = false;
                        }

                        if (Input.touches[0].phase == TouchPhase.Began)
                        {
                            if (EventSystem.current.IsPointerOverGameObject(0))
                            {
                                if (EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.name != "AbilityButton")
                                {
                                    UIAbilityButton.instance.HideTooltip();
                                    ClearSelectedAbility();
                                }
                                return;
                            }
                            if (validTarget)
                            {
                                ActivateAbility(ability, currentIndicator.position, targetUnit);
                                ClearSelectedAbility();
                            }
                            else GameControl.DisplayMessage("Invalid target for ability");
                        }
                    }

                }
            }

#else
            Camera mainCam = Camera.main;
            if (mainCam != null)
            {
                Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
                {
                    currentIndicator.position = hit.point + new Vector3(0, 0.1f, 0);

                    Unit targetUnit = null;

                    validTarget = true;
                    if (ability.singleUnitTargeting)
                    {
                        targetUnit = hit.transform.GetComponent<Unit>();
                        if (targetUnit != null) currentIndicator.position = targetUnit.thisT.position;
                        else validTarget = false;
                    }

                    if (Input.GetMouseButtonDown(0))
                    {
                        if (EventSystem.current.IsPointerOverGameObject())
                        {
                            if (EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.name.Contains("AbilityButton")==false)
                            {
                                ClearSelectedAbility();
                            }
                            return;
                        }

                        if (validTarget)
                        {
                            ActivateAbility(ability, currentIndicator.position, targetUnit);
                            ClearSelectedAbility();
                        }
                        else GameControl.DisplayMessage("Invalid target for ability");
                    }
                }
            }


            if (Input.GetMouseButtonDown(1))
            {
                ClearSelectedAbility();

            }
            if (Input.GetKey(KeyCode.Escape))
            {
                ClearSelectedAbility();
            }
#endif
        }


        //called by ability button from UI, select an ability
        public static string SelectAbility(int ID) { return instance._SelectAbility(ID); }
        public string _SelectAbility(int ID)
        {
            Ability ab = abilityCardList[ID].abilityObj;


            string exception = ab.IsAvailable();
            if (exception != "")
                return exception;

            selectedAbilityID = ID;
            MyLog.Log(ab.name);

            if (!ab.requireTargetSelection)
            {
                ActivateAbility(ab);       //no target selection required, fire it away
                ClearSelectedAbility();
            }
            else
            {
                if (onTargetSelectModeE != null)
                    onTargetSelectModeE(true);	//enter target selection phase
                //BlockAbilityUntilButtonUp = true;
                inTargetSelectMode = true;
                validTarget = false;


                if (ab.indicator != null) currentIndicator = ab.indicator;
                else
                {
                    currentIndicator = defaultIndicator;
                    if (ab.autoScaleIndicator)
                    {
                        if (ab.singleUnitTargeting)
                        {
                            float gridSize = BuildManager.GetGridSize();
                            //currentIndicator.localScale = new Vector3(gridSize, 1, gridSize);
                            Projector p = currentIndicator.GetComponentInChildren<Projector>();
                            if (p != null)
                            {
                                p.orthographicSize = gridSize;
                            }
                        }
                        else
                        {
                            //currentIndicator.localScale = new Vector3(ab.GetAOERadius(), 1, ab.GetAOERadius());
                            Projector p = currentIndicator.GetComponentInChildren<Projector>();
                            if (p != null)
                            {
                                MyLog.Log(string.Format("Select {0}+{1}={2}", ab.GetAOERadius(), ab.offsetRadius, (ab.GetAOERadius() + ab.offsetRadius) * 1.3f));
                                p.orthographicSize = (ab.GetAOERadius()+ab.offsetRadius)*1.3f;
                            }
                        }
                    }
                }

                currentIndicator.gameObject.SetActive(true);
            }

            return "";
        }
        public static void ClearSelectedAbility() { instance._ClearSelectedAbility(); }
        public void _ClearSelectedAbility()
        {
            MyLog.Log("ClearSelectedAbility");
            if (GetSelectedAbilityID() != -1)
            {
                UIAbilityButton.instance.HideCancelIcon(GetSelectedAbilityID());
            }


            if (currentIndicator != null)
                currentIndicator.gameObject.SetActive(false);
            selectedAbilityID = -1;
            currentIndicator = null;

            inTargetSelectMode = false;

            if (onTargetSelectModeE != null) onTargetSelectModeE(false);
        }


        //called when an ability is fired, reduce the energy, start the cooldown and what not
        public void ActivateAbility(Ability ab, Vector3 pos = default(Vector3), Unit unit = null)
        {
            ab.usedCount += 1;
            _activateAbility(ab, pos, unit);


            //  energy -= ab.GetCost();
#if UNITY_ANDROID || UNITY_IOS 
            //GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>().AddCrystals((int)-ab.GetCost(), GameControl.instance.levelID);

#endif
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
            /*if (ab.ID < 2)
            {
                _activateAbility(ab, pos, unit);
            }
            else
            {
                List<StringPair> extra = new List<StringPair>();
                extra.Add(new StringPair("force_id", (ab.ID - 1).ToString()));
                extra.Add(new StringPair("level_id", GameControl.GetLevelID().ToString()));
                extra.Add(new StringPair("wave_id", SpawnManager.GetCurrentWaveID().ToString()));
                StartCoroutine(_server.SaveLoadServer("useForce", extra, "ASD78ADS7a8sdhas8dh089asd9-", value => GetResponse(value, ab, pos, unit)));
            }*/
#endif

        }

        void _activateAbility(Ability ab, Vector3 pos, Unit unit)
        {
            ResourceManager.SpendResource(new List<float>() { 0, ab.GetCost() });
            StartCoroutine(ab.CooldownRoutine());
            CastAbility(ab, pos, unit);
            if (onAbilityActivatedE != null)
                onAbilityActivatedE(ab);
        }

        void GetResponse(string value, Ability ab, Vector3 pos = default(Vector3), Unit unit = null)
        {
            Debug.Log("AM-> getResponse");
            AbilityResponse response = new AbilityResponse();
            response = (AbilityResponse)StringSerializationAPI.Deserialize(response.GetType(), value);
            _player._SetPlayerData(response.user);
            if (response.ok == 1)
            {
                _activateAbility(ab, pos, unit);
            }
            else
            {
                GameControl.DisplayMessage(response.error); //TODO нет достаточно кристаллов
            }
        }
        class AbilityResponse
        {
            public PlayerData user;
            public int ok;
            public string error;
            public AbilityResponse()
            {
                user = null;
                ok = 0;
                error = "null";
            }
        }

        public static void CastAnyAbility(Vector3 pos)
        {
            var ab = instance.abilityCardList[UnityEngine.Random.Range(0, instance.abilityCardList.Count)].abilityObj;
            instance.CastAbility(ab, pos);
        }

        public static void instanceCastAbility(Ability ab, Vector3 pos, Unit unit = null)
        {
            instance.CastAbility(ab, pos, unit);
        }

        //called from ActivateAbility, cast the ability, visual effect and actual effect goes here
        public void CastAbility(Ability ab, Vector3 pos, Unit unit = null)
        {
            //AbilityEffect eff = ab.GetActiveEffect();
            if (ab.effectObj != null && ab.requireTargetSelection && !(ab.MultiCastAbility))
            {
                if (!ab.SpawnEffectOnEveryUnit)
                    ObjectPoolManager.Spawn(ab.effectObj, pos, Quaternion.identity);
            }

            if (ab.effectObj != null && !ab.requireTargetSelection && !ab.SpawnEffectOnEveryUnit && !ab.MultiCastAbility)
            {
                ObjectPoolManager.Spawn(ab.effectObj, pos, Quaternion.identity);
            }

            if (ab.useDefaultEffect)
            {
                ab.StartCastEventCall(pos, CounterCastAb);
                if (ab.MultiCastAbility)
                {
                    StartCoroutine(MulticastEffect(ab, pos, ab.MultiCastNumber, CounterCastAb, unit));
                }
                else
                {
                    StartCoroutine(ApplyAbilityEffect(ab, pos, CounterCastAb, unit));
                }
            }

            CounterCastAb++;
        }

        IEnumerator MulticastEffect(Ability ab, Vector3 pos, int times, int idCastAb, Unit tgtUnit = null)
        {
            yield return new WaitForSeconds(ab.MulticastDelay);
            Vector3 NewPos;
            for (int i = 0; i < times; i++)
            {
                NewPos = pos;
                if (ab.offsetRadius > 0)
                {
                    float angl = UnityEngine.Random.Range(0f, 90f) + (i * 90f);
                    while (angl > 360f)
                        angl -= 360f;
                    NewPos = pos + new Vector3(ab.offsetRadius * Mathf.Cos(angl), 0, ab.offsetRadius * Mathf.Sin(angl));
                    //Debug.Log("Pos: "+pos +" New: " + NewPos + " angle: "+angl + " radius:"+ab.offsetRadius);
                }

                if (!ab.MultiCastOnlyOneEffect)
                {
                    ObjectPoolManager.Spawn(ab.effectObj, NewPos, Quaternion.identity);
                }
                else if (i == 0)
                {
                    ObjectPoolManager.Spawn(ab.effectObj, NewPos, Quaternion.identity);
                }

                StartCoroutine(ApplyAbilityEffect(ab, NewPos, idCastAb, tgtUnit));

                yield return new WaitForSeconds(ab.effectDelay);
            }

            ab.EndCastEventCall(pos, CounterCastAb);

            yield break;

        }
        //apply the ability effect, damage, stun, buff and so on 
        IEnumerator ApplyAbilityEffect(Ability ab, Vector3 pos, int idCastAb, Unit tgtUnit = null)
        {
           

            AbilityEffect eff = ab.GetActiveEffect();
            if (!ab.MultiCastAbility && ab.effectDelay > 0)
            { yield return new WaitForSeconds(ab.effectDelay); }

            if (ab.damageDelay > 0)
            {
                yield return new WaitForSeconds(ab.damageDelay);
            }

            float radius = ab.requireTargetSelection ? ab.GetAOERadius() : 1000f;// Mathf.Infinity;
            if (ab.splashEffectObj!=null)
            {
                GameObject splashObj= ObjectPoolManager.Spawn(ab.splashEffectObj, pos, Quaternion.identity);
                splashObj.GetComponent<SplashSpecialEffect>().Emit(radius);
            }


            LayerMask mask1 = 1 << LayerManager.LayerTower();
            LayerMask mask2 = 1 << LayerManager.LayerCreep();
            LayerMask mask3 = 1 << LayerManager.LayerCreepF();
            LayerMask mask = mask1 | mask2 | mask3;

            List<Unit> creepList = new List<Unit>();
            List<Unit> towerList = new List<Unit>();
            try
            {
                if (tgtUnit == null)
                {
                   
                    Collider[] cols = Physics.OverlapSphere(pos, radius, mask);


                    if (cols.Length > 0)
                    {
                        for (int i = 0; i < cols.Length; i++)
                        {
                            Unit unit = cols[i].gameObject.GetComponent<Unit>();
                            if(unit.dead == false)
                            {
                                if (unit.IsCreep || unit.IsEgg)
                                    creepList.Add(unit);
                                else if (unit.IsTower)
                                    towerList.Add(unit.GetUnitTower);
                            }
                        }
                    }
                }
                else
                {
                    creepList.Add(tgtUnit);
                    towerList.Add(tgtUnit);
                }
            }
            catch (Exception e)
            {
                print(e.StackTrace);
            }

            eff.damageMultiplierGlobal = 1;
            ab.ApplayEffectEventCall(eff, pos, creepList, towerList, idCastAb);


            for (int n = 0; n < creepList.Count; n++)
            {
                if (creepList[n].dead == true)
                    continue;
                eff.damageMultiplierTarget = 1f;
                ab.ApplayEffectTargetEventCall(eff, pos, creepList[n],idCastAb);

                //Debug.Log("effect count:" + eff.listEffects.Count);
                if (creepList[n].EffectsManager == null)
                {
                    Debug.LogError(string.Format("Creep {0} effectsMenager is null!", creepList[n].unitName));
                }
                else
                {
                    for (int e = 0; e < eff.baseEffects.Count; e++) //Постоянныe эффекты заклинания
                    {
                        creepList[n].EffectsManager.AddEffect(null, eff.baseEffects[e].Clone());
                        //Debug.Log("add effect");
                    }

                    for (int e = 0; e < eff.effects.Count; e++) //Эффекты добавленые только в этой атаки
                    {
                        creepList[n].EffectsManager.AddEffect(null, eff.effects[e].Clone());
                        //Debug.Log("add effect");
                    }
                }

                if (eff.applyDamage)
                {
                    //dmgModifier = DamageTable.GetModifier(creepList[n].armorType, ab.damageType);
                    creepList[n].ApplyDamage(ab, UnityEngine.Random.Range(eff.readDamageMin, eff.readDamageMax), ab.damageType,false);
                    if (ab.SpawnEffectOnEveryUnit)
                    {
                        ObjectPoolManager.Spawn(ab.effectObj, creepList[n].thisT.position, Quaternion.identity);
                    }
                }


            }

            if(ab.MultiCastAbility==false)
                ab.EndCastEventCall(pos, CounterCastAb);
        }




        public static void GainEnergy(int value) { if (instance != null) instance._GainEnergy(value); }
        public void _GainEnergy(int value)
        {
            energy += value;
            energy = Mathf.Min(energy, GetEnergyFull());
        }


        public static float GetAbilityCurrentCD(int ID) { return instance.abilityCardList[ID].abilityObj.currentCD; }

        public static float GetEnergyFull() { return instance.fullEnergy /*+ PerkManager.GetEnergyCapModifier()*/; }
        public static float GetEnergy() { return instance.energy; }

        private float GetEnergyRate() { return energyRate/* + PerkManager.GetEnergyRegenModifier()*/; }
    }








}