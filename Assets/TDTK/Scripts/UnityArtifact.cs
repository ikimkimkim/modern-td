﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TDTK
{

    public class UnityArtifact : Unit
    {
        public static UnityArtifact mainArtifact { get; private set; }

        [Header("Artifact")]
        public bool isMain;

        public override void Awake()
        {
            if(isMain)
                mainArtifact = this;

            gameObject.layer = LayerManager.LayerArtifact();

            base.Awake();

            if (thisObj.GetComponent<Collider>() == null)
            {
                thisObj.AddComponent<SphereCollider>();
            }

            if (thisObj.GetComponent<SphereCollider>() != null)
                SizeMyCollider = thisObj.GetComponent<SphereCollider>().radius * thisT.localScale.x;

            if (thisObj.GetComponent<CapsuleCollider>() != null)
                SizeMyCollider = thisObj.GetComponent<CapsuleCollider>().radius * thisT.localScale.x;

            myInAttakInstanceInitE += UnityArtifact_myInAttakInstanceInitE;

            if (listUnitDamageMy != null)
                listUnitDamageMy.Clear();
            else
                listUnitDamageMy = new List<IUnitAttacking>();


        }

        private void UnityArtifact_myInAttakInstanceInitE(AttackInstance attackInstance)
        {
            attackInstance.damage *= Mathf.Pow(GameControl.instance.DamageCreepMultiplier, SpawnManager.GetCurrentWaveID());
        }

        protected override void Start()
        {
            // StartTime = Time.time;
            if (_EffectsManager == null)
                _EffectsManager = new EffectManager(this);
            else
                _EffectsManager.ClearData();

            base.Start();

           // UIOverlay.NewUnit(this);

            if (CardAndLevelSelectPanel.IDLevel > 0)
            {

                if (CardAndLevelSelectPanel.LevelDifficultySelect == 1)
                {
                    ApplyDamageHP(null, fullHP * 0.5f);
                }
                else if (CardAndLevelSelectPanel.LevelDifficultySelect == 2)
                {
                    ApplyDamageHP(null, fullHP * 0.9f);
                }
            }

        }

        public override void Dead(bool getRsc = true)
        {
            base.Dead(getRsc);

            /*if (animationUnit != null)
                animationUnit.PlayDead(); */

            myInAttakInstanceInitE -= UnityArtifact_myInAttakInstanceInitE;

        }



        /*public override void DeadEndAnimEvent()
        {
            if (isMain && GameControl.GetGameType == _GameType.DefenseBase)
            {
                Debug.Log("Aritfact destoy");
                if (SpawnManager.instance.SpawnLimit == _SpawnLimit.Infinite)
                {
                    GameControl.GameUnlimitedEnd();
                }
                else
                {
                    GameControl.GameLose();
                }
            }
            base.DeadEndAnimEvent();
        }*/

        public override void SystemUpdate()
        {
            if (dead || GameControl.IsGamePlay==false)
                return;

            RegenStat();
            if (EffectsManager != null)
                EffectsManager.Update();
        }
    }

}
