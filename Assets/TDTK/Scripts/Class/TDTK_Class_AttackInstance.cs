﻿using UnityEngine;
using System.Collections;

using TDTK;
using System.Collections.Generic;

namespace TDTK {


    public enum enumDamageType
    {
        Light,
        Heavy,
        Magical,
        Energetic
    }

    //contains all the information of an attack
    //an instance of the class is generate each time an attack took place
    public class AttackInstance {

        public delegate void AttackInstancePorgressD(AttackInstance attack);
        public static event AttackInstancePorgressD ProgressE;

        public static float TimeLastShot { get; set; }

        public delegate void AOECounterD(int countTargets, AttackInstance attackInstance);
        public event AOECounterD AOECounterE;

        public bool processed = false;

        public PropertiesAttak srcPropretiesAttak;
        public FPSWeapon srcWeapon;
        public IUnitAttacking srcUnit;
        public IUnitAttacked tgtUnit;



        public Vector3 impactPoint;
        //public float continousDmgMul=1;	//for fps beam which do continous damage

        public bool missed = false;
        public float AddChanceCritical = 0, MultiplierChanceCritical = 1;
        private float MultiplierDmgCritical = 1;
        public bool critical {get; private set; }
		public bool destroy=false;
		
		public bool stunned=false;
		public bool slowed=false;
		public bool isDot=false;
        public bool incomDamage = false;

        public bool instantKill=false;
		public bool breakShield=false;
		public bool pierceShield=false;
		
		public float damage=0;
        public float dmgModifier;
        public float damageHP=0;
		public float damageShield=0;
		
		public Stun stun;
		public Slow slow;
		public Dot dot;

        public bool isHealing = false;
        public bool isCloneSplash = false;
        public bool ignoreArmor = false;
        public bool isAOE = false;
        public float AOERadius { get; private set; }
        public float DistanceFromCenter = 1f; // 0..1

        public List<BaseEffect> listEffects = new List<BaseEffect>();

        public float MultiplierChanceBleeding=1, MultiplierChanceDisorientation=1, MultiplierChanceStun=1;

        public AttackInstance()
        {
            critical = false;
        }

        public float GetChanceCritical(float baseChance)
        {
            return (baseChance * MultiplierChanceCritical + AddChanceCritical);
        }
        public void SetMultiplierDmgCritical(float MultiplierDmg)
        {
            if (critical == false)
            {
                critical = true;
                new TextOverlay(srcUnit.getPosition, "Crit!", Color.red, false);
            }
            if (MultiplierDmgCritical < MultiplierDmg)
                MultiplierDmgCritical = MultiplierDmg;
        }
        public bool getIgnoreArmor
        {
            get
            {
                return ignoreArmor || (srcUnit!=null && srcUnit.IgnoreTypeArmor());
            }
        }

        public void SetAOERadius(float radius)
        {
            if (radius < 0)
                return;
            if (isAOE == false)
            {
                isAOE = true;
            }
            //Debug.Log("SetAOE:" + radius + " now:"+AOERadius);
            if (AOERadius < radius)
                AOERadius = radius;
        }

        public void DownAOERadius(float radius)
        {
            if (isAOE && radius>=0)
            {
                if (AOERadius > radius)
                    AOERadius = radius;
                if(radius==0)
                isAOE = false;
            }
            //Debug.Log("SetAOE:" + radius + " now:"+AOERadius);
        }

        public int getDamageType
        {
            get
            {
                if (srcUnit != null) return srcUnit.DamageType();
                else if (srcWeapon != null) return srcWeapon.damageType;
                else if (srcPropretiesAttak != null) return srcPropretiesAttak.damageType;
                else
                {
                    Debug.LogError("Process AttackInstance getDamageType, unit and weapon is null!");
                    return -1;
                }
            }
        }

        //do the stats processing
        public void Process(){
			if(processed || isHealing) return;
			
			processed=true;

            TimeLastShot = Time.time;

            if (srcUnit != null) Process_SrcUnit();
            //else if (srcWeapon != null) Process_SrcWeapon();
            else if (srcPropretiesAttak != null) Process_srcPropretiesAttak();
            else Debug.LogWarning("Process AttackInstance unit and weapon is null!");

            if (ProgressE != null)
                ProgressE(this);
		}

        public void Process_srcPropretiesAttak()
        {
            if(tgtUnit==null)
            {
                return;
            }

            damage = Random.Range(srcPropretiesAttak.GetDamageMin(), srcPropretiesAttak.GetDamageMax());
            damage /= (float)srcPropretiesAttak.GetShootPointCount();  //divide the damage by number of shootPoint



            //Множетель для типа брони и типа урона, + свойство игнорирование брони
            dmgModifier = DamageTable.GetModifier(tgtUnit.ArmorType(), srcPropretiesAttak.damageType);
            //Debug.Log("armor:"+dmgModifier);
            dmgModifier = dmgModifier - Mathf.Clamp(tgtUnit.GetAdditiveArmor(), 0, dmgModifier);


            if (Random.Range(0f, 1f) < srcPropretiesAttak.GetShieldPierce() && damageShield > 0)
            {
                //damageHP += damageShield;
                damageShield = 0;
                pierceShield = true;
                //new TextOverlay(impactPoint,"Shield Pierced", new Color(0f, 1f, 1f, 1f));
            }


            if (Random.Range(0f, 1f) < srcPropretiesAttak.GetShieldBreak() && tgtUnit.ShieldExist())
                breakShield = true;

            /*stunned = srcPropretiesAttak.GetStun().IsApplicable();
            if (tgtUnit.immuneToStun) stunned = false;


            slowed = srcPropretiesAttak.GetSlow().IsValid();
            if (tgtUnit.immuneToSlow) slowed = false;

            if (srcPropretiesAttak.GetDot().GetTotalDamage() > 0) dotted = true;

            if (stunned) stun = srcPropretiesAttak.GetStun().Clone();
            if (slowed) slow = srcPropretiesAttak.GetSlow().Clone();
            if (dotted) dot = srcPropretiesAttak.GetDot().Clone();*/

            try
            {
                tgtUnit.CallEventInAttakInit(this);
                srcPropretiesAttak.CallEventOutAttakInit(this);

                tgtUnit.CallEventInAttakFirst(this);
                srcPropretiesAttak.CallEventOutAttakFirst(this);

                tgtUnit.CallEventInAttakLast(this);
                srcPropretiesAttak.CallEventOutAttakLast(this);
            }
            catch(System.Exception ex)
            {
                Debug.LogError("Error process propretiesAttak:" + ex.Message + "\r\n" + ex.StackTrace);
            }
            damage *= MultiplierDmgCritical;

        }

        //stats processing for attack from fps-weapon
       /* public void Process_SrcWeapon(){
			if(srcWeapon.GetInstantKill().IsApplicable(tgtUnit.HP, tgtUnit.fullHP)){
				damage=tgtUnit.HP;
				damageHP=tgtUnit.HP;
				instantKill=true;
				destroy=true;
				return;
			}

            Debug.Log(srcWeapon.GetDamageMin()+" \\ "+ srcWeapon.GetDamageMax());
            damage =Random.Range(srcWeapon.GetDamageMin(), srcWeapon.GetDamageMax());
            damage *= DistanceFromCenter; // Для AOE урона
            damage /=(float)srcWeapon.GetShootPointCount();  //divide the damage by number of shootPoint


            float critChance=srcWeapon.GetCritChance();
			if(tgtUnit.immuneToCrit) critChance=-1f;
			if(Random.Range(0f, 1f)<critChance){
				critical=true;
				damage*=srcWeapon.GetCritMultiplier();
				//new TextOverlay(impactPoint, "Critical", new Color(1f, .6f, 0f, 1f));
			}
			
			float dmgModifier=DamageTable.GetModifier(tgtUnit.armorType, srcWeapon.damageType);
			damage*=dmgModifier;
			
			if(damage>=tgtUnit.shield){
				damageShield=tgtUnit.shield;
				damageHP=damage-tgtUnit.shield;
			}
			else{
				damageShield=damage;
				damageHP=0;
			}
			
           
			
			if(Random.Range(0f, 1f)<srcWeapon.GetShieldPierce() && damageShield>0){
				damageHP+=damageShield;
				damageShield=0;
				pierceShield=true;
				//new TextOverlay(impactPoint,"Shield Pierced", new Color(0f, 1f, 1f, 1f));
			}
			if(srcWeapon.DamageShieldOnly()) damageHP=0; 
			
			if(damageHP>=tgtUnit.HP){
				destroy=true;
				return;
			}
			
			if(Random.Range(0f, 1f)<srcWeapon.GetShieldBreak() && tgtUnit.fullShield>0) breakShield=true;
			
			stunned=srcWeapon.GetStun().IsApplicable();
			if(tgtUnit.immuneToStun) stunned=false;
			
			slowed=srcWeapon.GetSlow().IsValid();
			if(tgtUnit.immuneToSlow) slowed=false;
			
			if(srcWeapon.GetDot().GetTotalDamage()>0) dotted=true;

			if(stunned) stun=srcWeapon.GetStun().Clone();
			if(slowed) slow=srcWeapon.GetSlow().Clone();
			if(dotted) dot=srcWeapon.GetDot().Clone();

            Debug.Log(damage + " \\ " + damageHP);
        }*/
		
		public LayerMask getTargetMask()
        {
            if (srcUnit != null)
                return srcUnit.GetTargetMask();
            else if (srcPropretiesAttak != null)
                return srcPropretiesAttak.GetTargetMask();

            Debug.LogError("AttInstance get target mask, layer is not init!");
            return 0;
        }
		
		//stats processing for attack from unit (tower/creep)
		public void Process_SrcUnit(){
			if(srcUnit.GetHit()<=0){
				Debug.LogWarning("Attacking unit ("+srcUnit.GetName()+") has default hitChance of 0%, is this intended?");
			}
			
			float hitChance=Mathf.Clamp(srcUnit.GetHit()-tgtUnit.GetDodge(), 0, 1);
			if(Random.Range(0f, 1f)>hitChance){
				missed=true;
                new TextOverlay(srcUnit.getPosition, "Miss!", Color.red, false);
                return;
			}

            damage = Random.Range(srcUnit.GetDamageMin(), srcUnit.GetDamageMax());

			damage/=(float)srcUnit.GetShootPointCount();    //divide the damage by number of shootPoint


            //Множетель для типа брони и типа урона, + свойство игнорирование брони
            dmgModifier=DamageTable.GetModifier(tgtUnit.ArmorType(), srcUnit.DamageType());
            //Debug.Log("armor:"+dmgModifier);
            dmgModifier = dmgModifier - Mathf.Clamp(tgtUnit.GetAdditiveArmor(), 0, dmgModifier);
            //Debug.Log("armor:" + dmgModifier);
            if (srcUnit.IgnoreTypeArmor())
                dmgModifier = 1;
            //damage *=dmgModifier;

           
			
			//Шанс пробить щит
			if(damageShield > 0 && Random.Range(0f, 1f)<srcUnit.GetShieldPierce()){
				pierceShield=true;
			}
			
            //Шанс сломать щит
			/*if(tgtUnit.ShieldExist() && Random.Range(0f, 1f)<srcUnit.GetShieldBreak())
                breakShield =true;*/

            try
            {
                srcUnit.CallEventOutAttakInit(this);
                tgtUnit.CallEventInAttakInit(this);

                srcUnit.CallEventOutAttakFirst(this);
                tgtUnit.CallEventInAttakFirst(this);

                srcUnit.CallEventOutAttakLast(this);
                tgtUnit.CallEventInAttakLast(this);
            }
            catch (System.Exception ex)
            {
                Debug.LogError("Error process SrcUnit:"+ex.Message+"\r\n" + ex.StackTrace);
            }
            damage *= MultiplierDmgCritical;//Умнажаем урон на множетель крита после всех свойств и эффектов

           // DamageHPAndShield();

		}

        //Сообщает о количестве целей попавших под аое
        public void CallEventCounterTargets(int countTargets)
        {
            if (AOECounterE != null)
                AOECounterE(countTargets, this);
        }

		
        public void ApplyDistance(float distance)
        {
            /* Мешает свойству создать атаку без расчетов.
            if(processed == false)
            {
                Debug.LogError("AttInstance apply distance, precessed is false!");
                return;
            }*/
            if(AOERadius<=0)
            {
                Debug.LogError("AttInstance apply distance, AOE radius less or equal zero!");
                return;
            }
            damage *= Mathf.Clamp(-0.5f * (distance / AOERadius) + 1, 0, float.MaxValue);

           // DamageHPAndShield();
        }

       /* private void DamageHPAndShield()
        {
            if (pierceShield)
            {
                damageHP = damage;
                damageShield = 0;
            }
            else
            {
                if (damage >= tgtUnit.shield)
                {
                    damageShield = tgtUnit.shield;
                    damageHP = damage - tgtUnit.shield;
                }
                else
                {
                    damageShield = damage;
                    damageHP = 0;
                }
            }
            //Если урон только по щиту
			if(srcUnit.DamageShieldOnly())
                damageHP =0; 

            if (damageHP >= tgtUnit.HP)
            {
                destroy = true;
                return;
            }
        }*/

		//clone an instance
		public AttackInstance Clone(){
			AttackInstance attInstance=new AttackInstance();
			
			attInstance.processed=processed;
			attInstance.srcWeapon=srcWeapon;
            attInstance.srcPropretiesAttak = srcPropretiesAttak;
            attInstance.srcUnit=srcUnit;
			attInstance.tgtUnit=tgtUnit;

            attInstance.isAOE = isAOE;
            attInstance.AOERadius = AOERadius;
			
			attInstance.missed=missed;
			attInstance.critical=critical;
			attInstance.destroy=destroy;
			
			attInstance.stunned=stunned;
			attInstance.slowed=slowed;
			attInstance.isDot = isDot;			
			attInstance.instantKill=instantKill;

			attInstance.breakShield=breakShield;
			attInstance.pierceShield=pierceShield;
			
			attInstance.damage=damage;
			attInstance.damageHP=damageHP;
			attInstance.damageShield=damageShield;

            attInstance.AddChanceCritical = AddChanceCritical;
            attInstance.MultiplierChanceCritical = MultiplierChanceCritical;
            attInstance.MultiplierDmgCritical = MultiplierDmgCritical;

            attInstance.MultiplierChanceBleeding = MultiplierChanceBleeding;
            attInstance.MultiplierChanceDisorientation = MultiplierChanceDisorientation;
            attInstance.MultiplierChanceStun = MultiplierChanceStun;

            attInstance.stun=stun;
			attInstance.slow=slow;
			attInstance.dot=dot;

            attInstance.listEffects = new List<BaseEffect>();
            for (int i = 0; i < listEffects.Count; i++)
            {
                attInstance.listEffects.Add(listEffects[i].Clone());
            }

			return attInstance;
		}
		
        public AttackInstance CloneNewTargetProcess(Unit target)
        {
            AttackInstance attInstance = new AttackInstance();

            attInstance.srcWeapon = srcWeapon;
            attInstance.srcUnit = srcUnit;
            attInstance.tgtUnit = target;
            attInstance.srcPropretiesAttak = srcPropretiesAttak;

            attInstance.AOERadius = AOERadius;

            attInstance.MultiplierDmgCritical = MultiplierDmgCritical;

            attInstance.isCloneSplash = true;

            attInstance.listEffects = new List<BaseEffect>();
            for (int i = 0; i < listEffects.Count; i++)
            {
                if(listEffects[i].isSplashUnique)
                    attInstance.listEffects.Add(listEffects[i].Clone());
            }

            attInstance.Process();

            return attInstance;
        }
	}

}