﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TDTK
{

    [System.Serializable]
    public class Ability : TDTKItem, IUnitAttacking 
    {
        public delegate void CastHandler(Ability ability, Vector3 position,int idCast);
        public delegate void CastForUnitHandler(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> creeps, List<Unit> towers, int idCast);
        public delegate void CastForOneUnitHandler(Ability ability, AbilityEffect effect, Vector3 position, Unit target, int idCast);

        public event CastHandler StartCastE;
        public event CastForUnitHandler ApplayEffectE;
        public event CastForOneUnitHandler ApplayEffectTargetE;
        public event CastHandler EndCastE;

        public enum _TargetType { Hostile, Friendly, Hybrid }

        public bool disableInAbilityManager = false;

        public float cost = 10;
        public float cooldown = 10;	//cd duration
        public float currentCD = 0;		//variable for counting cd during runtime, ability only available when this is <0

        public GameObject CastEffect;


        public bool requireTargetSelection = true;
        public bool SpawnEffectOnEveryUnit = false;
        public bool MultiCastAbility = false;
        public int MultiCastNumber = 0;
        public bool MultiCastOnlyOneEffect = false;
        public bool singleUnitTargeting = false;
        public _TargetType targetType = _TargetType.Hostile;

        public GameObject effectObj;
        public GameObject splashEffectObj;

        public int maxUseCount = -1;
        public int usedCount = 0;		////variable for counting usage during runtime

        public Transform indicator;
        public bool autoScaleIndicator = true;

        public bool useDefaultEffect = true;
        public List<AbilityEffect> effectList = new List<AbilityEffect>();

        public AbilityEffect effect = new AbilityEffect();

        public float aoeRadius = 2;
        public float offsetRadius = 0;
        public float effectDelay = 0.25f;
        public float damageDelay = 0.25f;

        public int damageType;

        public Card MyCard;

        /// <summary>
        /// Задержка перед началом мультикаста
        /// </summary>
        public float MulticastDelay;
        public bool useCustomDesp = false;

        public Vector3 getPosition { get { return Vector3.zero; } }
        public EffectManager EffectsManager { get { return null; } }

        public Ability Clone()
        {
            Ability ab = new Ability();
            ab.ID = ID;
            ab.name = name;
            ab.icon = icon;
            ab.cost = cost;
            ab.cooldown = cooldown;
            ab.currentCD = currentCD;
            ab.requireTargetSelection = requireTargetSelection;
            ab.singleUnitTargeting = singleUnitTargeting;
            ab.targetType = targetType;
            ab.effectObj = effectObj;
            ab.splashEffectObj = splashEffectObj;
            ab.maxUseCount = maxUseCount;
            ab.usedCount = usedCount;
            ab.indicator = indicator;
            ab.autoScaleIndicator = autoScaleIndicator;
            ab.useDefaultEffect = useDefaultEffect;
            ab.CastEffect = CastEffect;
            for (int i = 0; i < effectList.Count; i++)
                ab.effectList.Add(effectList[i].Clone());
            ab.effect = effect.Clone();
            ab.aoeRadius = aoeRadius;
            ab.effectDelay = effectDelay;
            ab.damageDelay = damageDelay;
            ab.useCustomDesp = useCustomDesp;
            ab.desp = desp;
            ab.SpawnEffectOnEveryUnit = SpawnEffectOnEveryUnit;
            ab.MultiCastAbility = MultiCastAbility;
            ab.MultiCastNumber = MultiCastNumber;
            ab.MulticastDelay = MulticastDelay;
            ab.MultiCastOnlyOneEffect = MultiCastOnlyOneEffect;
            ab.damageType = damageType;
            ab.ignoreTypeArmor = ignoreTypeArmor;
            return ab;
        }

        public string IsAvailable()
        {
            if (currentCD > 0) return "Ability is on cooldown";
            if (GetCost() > ResourceManager.GetResourceArray()[1])// GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>().GetCrystals())
                return "Insufficient Mana";
            if (maxUseCount > 0 && usedCount >= maxUseCount) return "Usage limit exceed";
            return "";
        }

        public void StartCastEventCall(Vector3 pos,int idCast)
        {
            if (StartCastE != null)
                StartCastE(this, pos,idCast);
        }
        public void ApplayEffectEventCall(AbilityEffect effect, Vector3 pos, List<Unit> creeps, List<Unit> towers, int idCast)
        {
            if (ApplayEffectE != null)
                ApplayEffectE(this, effect, pos, creeps, towers, idCast);
        }
        public void ApplayEffectTargetEventCall(AbilityEffect effect, Vector3 pos, Unit unit, int idCast)
    {
            if (ApplayEffectTargetE != null)
                ApplayEffectTargetE(this, effect, pos, unit, idCast);
        }
        public void EndCastEventCall(Vector3 pos, int idCast)
        {
            if (EndCastE != null)
                EndCastE(this, pos, idCast);
        }

        public IEnumerator CooldownRoutine()
        {
            currentCD = GetCooldown();
            //Debug.Log(currentCD);
            while (currentCD > 0)
            {
                currentCD -= Time.deltaTime;
                yield return null;
            }
        }

        public float GetCost() { return cost * (1 - PerkManager.GetAbilityCost(ID)); }
        public float GetCooldown() { return cooldown * (1 - PerkManager.GetAbilityCooldown(ID)); }
        public float GetAOERadius() { return aoeRadius * (1 + PerkManager.GetAbilityAOERadius(ID)); }

        public AbilityEffect GetActiveEffect()
        {
            AbilityEffect eff = new AbilityEffect();
         //   eff.duration = effect.duration * (1 + PerkManager.GetAbilityDuration(ID));
            eff.damageMin = effect.damageMin /** (1 + PerkManager.GetAbilityDamage(ID))*/;
         //   Debug.Log("  eff.damageMin " + eff.damageMin);
            eff.damageMax = effect.damageMax /** (1 + PerkManager.GetAbilityDamage(ID))*/;
         //   Debug.Log("  eff.damageMax " + eff.damageMax);
         //   eff.stunChance = effect.stunChance * (1 + PerkManager.GetAbilityStunChance(ID));

         //   eff.slow = PerkManager.ModifySlowWithPerkBonus(effect.slow, ID, 2);	//pass 2 to indicate this is for ability TODO
         //   Debug.Log("  eff.slow " + eff.slow.slowMultiplier);
         //   eff.dot = PerkManager.ModifyDotWithPerkBonus(effect.dot, ID, 2);

            eff.baseEffects = effect.baseEffects; // для эффектов с 100% шансом.
            //eff.effects = effect.effects; Список при атаки всегда пуст, для эффектов с шансом наложения

          //  eff.damageBuff = effect.damageBuff * (1 + PerkManager.GetAbilityDamageBuff(ID));
          //  eff.rangeBuff = effect.rangeBuff * (1 + PerkManager.GetAbilityRangeBuff(ID));
          //  eff.cooldownBuff = effect.cooldownBuff * (1 + PerkManager.GetAbilityCooldownBuff(ID));
          //  eff.HPGainMin = effect.HPGainMin * (1 + PerkManager.GetAbilityHPGain(ID));
          //  eff.HPGainMax = effect.HPGainMax * (1 + PerkManager.GetAbilityHPGain(ID));

            return eff;
        }


        public string GetDesp()
        {
            if (useCustomDesp) return desp;

            string text = "GetDesp text";

            /*AbilityEffect eff = GetActiveEffect();

            if (eff.damageMax > 0)
            {
                if (requireTargetSelection) text += "Deals " + eff.damageMin + "-" + eff.damageMax + " to hostile target in range\n";
                else text += "Deals " + eff.damageMin + "-" + eff.damageMax + " to all hostile on the map\n";
            }
            if (eff.stunChance > 0 && eff.duration > 0)
            {
                if (requireTargetSelection) text += (eff.stunChance * 100).ToString("f0") + "% chance to stun hostile target for " + eff.duration + "s\n";
                else text += (eff.stunChance * 100).ToString("f0") + "% chance to stun all hostile on the map for " + eff.duration + "s\n";
            }
            if (eff.slow.IsValid())
            {
                if (requireTargetSelection) text += "Slows hostile target down for " + eff.duration + "s\n";
                else text += "Slows all hostile on the map down for " + eff.duration + "s\n";
            }
            if (eff.dot.GetTotalDamage() > 0)
            {
                if (requireTargetSelection) text += "Deals " + eff.dot.GetTotalDamage().ToString("f0") + " to hostile target over " + eff.duration + "s\n";
                else text += "Deals " + eff.dot.GetTotalDamage().ToString("f0") + " to all hostile on the map over " + eff.duration + "s\n";
            }


            if (eff.HPGainMax > 0)
            {
                if (requireTargetSelection) text += "Restore " + eff.HPGainMin + "-" + eff.HPGainMax + " of friendly target HP\n";
                else text += "Restore " + eff.HPGainMin + "-" + eff.HPGainMax + " of all tower HP\n";
            }
            if (eff.duration > 0)
            {
                if (eff.damageBuff > 0)
                {
                    if (requireTargetSelection) text += "Increase friendly target damage by " + (eff.damageBuff * 100).ToString("f0") + "%\n";
                    else text += "Increase all towers damage by " + (eff.damageBuff * 100).ToString("f0") + "%\n";
                }
                if (eff.rangeBuff > 0)
                {
                    if (requireTargetSelection) text += "Increase friendly target range by " + (eff.rangeBuff * 100).ToString("f0") + "%\n";
                    else text += "Increase all towers range by " + (eff.rangeBuff * 100).ToString("f0") + "%\n";
                }
                if (eff.cooldownBuff > 0)
                {
                    if (requireTargetSelection) text += "Decrease friendly target cooldown by " + (eff.cooldownBuff * 100).ToString("f0") + "%\n";
                    else text += "Decrease all towers cooldown by " + (eff.cooldownBuff * 100).ToString("f0") + "%\n";
                }
            }
            */

            return text;
        }

        public string GetName()
        {
            return name;
        }

        public int DamageType()
        {
            return damageType;
        }

        public bool ignoreTypeArmor = false;

        public bool IgnoreTypeArmor()
        {
            return ignoreTypeArmor;
        }

        public float GetDamageMin()
        {
            return effect.damageMin;
        }

        public float GetDamageMax()
        {
            return effect.damageMax;
        }

        public int GetShootPointCount()
        {
            return MultiCastAbility ? MultiCastNumber : 1;
        }

        public float GetHit()
        {
            return 1;
        }

        public float GetShieldPierce()
        {
            return 0;
        }

        public LayerMask GetTargetMask()
        {
            return (1 << LayerManager.LayerCreep()) | (1 << LayerManager.LayerCreepF());
        }

        public void CallEventOutAttakInit(AttackInstance attackInstance)
        {
            throw new System.NotImplementedException();
        }

        public void CallEventOutAttakFirst(AttackInstance attackInstance)
        {
            throw new System.NotImplementedException();
        }

        public void CallEventOutAttakLast(AttackInstance attackInstance)
        {
            throw new System.NotImplementedException();
        }
    }


    [System.Serializable]
    public class AbilityEffect
    {
        public AbilityEffect()
        {
            baseEffects = new List<BaseEffect>();
            effects = new List<BaseEffect>();
        }
        //public float duration = 0;  //duration of the effect, shared by all (stun & buff)

        //offsense stat

        public bool applyDamage = true;
        public float damageMin = 0;
        public float damageMax = 0;

        public float damageMultiplierGlobal = 1f;
        public float damageMultiplierTarget = 1f;

        public float readDamageMin { get { return damageMin * damageMultiplierGlobal * damageMultiplierTarget; } }
        public float readDamageMax { get { return damageMax * damageMultiplierGlobal * damageMultiplierTarget; } }

        // public float stunChance = 0;
        // public Slow slow = new Slow();
        // public Dot dot = new Dot();

        public List<BaseEffect> baseEffects;
        public List<BaseEffect> effects;

        //defense stat
        /*public float damageBuff = 0;
        public float rangeBuff = 0;
        public float cooldownBuff = 0;
        public float HPGainMin = 0;
        public float HPGainMax = 0;*/


        public AbilityEffect Clone()
        {
            AbilityEffect eff = new AbilityEffect();

            //eff.duration = duration;

            eff.damageMin = damageMin;
            eff.damageMax = damageMax;
            eff.damageMultiplierTarget = damageMultiplierTarget;
            eff.damageMultiplierGlobal = damageMultiplierGlobal;
            /* eff.stunChance = stunChance;
             eff.slow = slow.Clone();
             eff.dot = dot.Clone();

             eff.slow.duration = eff.duration;
             eff.dot.duration = eff.duration;

             eff.damageBuff = damageBuff;
             eff.rangeBuff = rangeBuff;
             eff.cooldownBuff = cooldownBuff;
             eff.HPGainMin = HPGainMin;
             eff.HPGainMax = HPGainMax;*/

            for (int i = 0; i < baseEffects.Count; i++)
            {
                eff.baseEffects.Add(baseEffects[i].Clone());
            }

            return eff;
        }
    }



}
