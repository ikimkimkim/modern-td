﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TDTK {

	[System.Serializable]
	public class UnitStat{
		
		public float damageMin=5;
		public float damageMax=6;
		public float cooldown=1;
		public float clipSize=-1;		//not in used when set to <0
		public float reloadDuration=2;
		public float range=10;
        public float maxHP;
        //public float xpNextLevel;

		[HideInInspector] public float minRange=0;
		public float aoeRadius=0;	//aoe radius when shootObject hit target
		
		public float hit=0;
		public float dodge=0;
		
		public float shieldBreak=0;		//if >0, then tower has chance to break shield (disable shield)
		public float shieldPierce=0;	//if >0, then tower has chance to bypass shield
		public bool damageShieldOnly=false; //when set to true, tower damage shield only

        public float upDmgBoss = 0;
		//public Critical crit;
		//public Stun stun;
		//public Slow slow, slowAura;
        //public Contusion contusionAura;
		//public Dot dot;				//damage over time
		//public InstantKill instantKill;
       // public IncomDamageUp incomDamage;

        public float ImmunSlow = 0, ImmunSlowTime = 0;

		//public Buff buff;				//for support tower
		public List<float> rscGain=new List<float>();	//for resource tower
		
		public List<float> cost=new List<float>();
		public float buildDuration=1;
		public float unBuildDuration=1;
		
		public Transform shootObjectT;
		public ShootObject shootObject;
		
		public bool useCustomDesp=false;
		public string desp="";
        //public string despGeneral="";

        public float moveSpeed = 3;

        //new---
        public int CountAttakcs = 1;


		public UnitStat(){
			/*stun=new Stun();
			crit=new Critical();
			slow=new Slow();
            slowAura = new Slow();
            contusionAura = new Contusion();
            dot =new Dot();
			instantKill=new InstantKill();
			buff=new Buff();*/
           // incomDamage = new IncomDamageUp();

        }
		
		public UnitStat Clone(){
			UnitStat stat=new UnitStat();
			stat.damageMin=damageMin;
			stat.damageMax=damageMax;
			stat.clipSize=clipSize;
			stat.reloadDuration=reloadDuration;
			stat.minRange=minRange;
			stat.range=range;
			stat.aoeRadius=aoeRadius;
			stat.hit=hit;
            stat.cooldown = cooldown;
			stat.dodge=dodge;
			stat.shieldBreak=shieldBreak;
			stat.shieldPierce=shieldPierce;
			stat.damageShieldOnly=damageShieldOnly;
			/*stat.crit=crit.Clone();
			stat.stun=stun.Clone();
			stat.slow=slow.Clone();

            stat.slowAura = slowAura.Clone();
            stat.contusionAura = contusionAura.Clone();
            stat.dot=dot.Clone();
			stat.instantKill=instantKill.Clone();
			stat.buff=buff.Clone();*/
			stat.buildDuration=buildDuration;
			stat.unBuildDuration=unBuildDuration;
			stat.shootObjectT=shootObjectT;
			stat.desp=desp;
            stat.maxHP = maxHP;
           // stat.xpNextLevel = xpNextLevel;
            stat.moveSpeed = moveSpeed;
            stat.CountAttakcs = CountAttakcs;
			//stat.despGeneral=despGeneral;
			//for(int i=0; i<rscGain.Count; i++) stat.rscGain.Add(rscGain[i]);
			//for(int i=0; i<cost.Count; i++) stat.cost.Add(cost[i]);
			
			stat.rscGain=new List<float>(rscGain);
			stat.cost=new List<float>(cost);
			
			return stat;
		}

       /* public void ChangeSlow(float s = 0, float dur = 0)
        {
           slow.slowMultiplier = s*(1f- ImmunSlow);
           slow.duration = dur*(1f- ImmunSlowTime);
        }

        public void ChangeSlowAura(float s = 0, float dur = 0)
        {
            slowAura.slowMultiplier = s*(1f- ImmunSlow);
            slowAura.duration = dur*(1f- ImmunSlowTime);
        }

        public void ChangeContusionAura(float c = 0, float dur = 0)
        {
            contusionAura.contusionMultiplier = c ;
            contusionAura.duration = dur;
        }*/
    }
	
   

    [System.Serializable]
	public class Stun{
		public float chance=0;
		public float duration=0;
        public float cMultiplier = 0;
        public bool onlySlowCreep = false;
        public bool applyAOE = true;
        public bool CurrentFirstAtak = false;

        public Stun(float c=0, float dur=0, bool osc = false){
			chance=c; duration=dur; onlySlowCreep = osc;
		}
		
		public bool IsValid(){	//funcion to determine if the stun is available
			if(duration>0 && chance>0) return true;
			return false;
		}
		
		public bool IsApplicable(bool CreepSlow=false,bool isAOE=false, bool firstAtak=false){	//function to determine if the stun works on a unit
			if(duration>0)
            {
                if (firstAtak && CurrentFirstAtak)
                    return true;
                if((onlySlowCreep==true && CreepSlow==true || onlySlowCreep==false) && (isAOE==false || applyAOE == true))
                {
                    if (Random.Range(0f, 1f) < chance)
                        return true;
                }
            }

			return false;
		}
		
		public Stun Clone(){
			Stun stun=new Stun();
			stun.chance=chance;
			stun.duration=duration;
            stun.onlySlowCreep = onlySlowCreep;
            return stun;
            
        }
	}

    public class ShieldData
    {
        public float FullShield;
        public float Value;
        public int ArmorType;

        public ShieldData(float fullShield,int armorType)
        {
            FullShield = fullShield;
            Value = FullShield;
            ArmorType = armorType;
        }

        public void AddValue(float point)
        {
            Value = Mathf.Clamp(Value + point, 0, FullShield);
        }

    }


	[System.Serializable]
	public class Critical{
        public InstantKill instantKill;
        public float chance=0;
		public float dmgMultiplier=0;
        public bool onlySlowCreep = false;
        public bool onlyBoss = false;
        public bool onlyCreep = false;

        public Critical()
        {
            instantKill = new InstantKill();
        }

        public Critical Clone(){
			Critical clone=new Critical();
			clone.chance=chance;
			clone.dmgMultiplier=dmgMultiplier;
            clone.onlySlowCreep = onlySlowCreep;
            clone.onlyBoss = onlyBoss;
            clone.onlyCreep = onlyCreep;
            clone.instantKill = instantKill.Clone();
            return clone;
		}
	}

    [System.Serializable]
    public class Contusion
    {
        public float duration = 0;
        public float contusionMultiplier = 1f;
        public float auraRange = 0;

        public Contusion(float c = 0, float dur = 0)
        {
            contusionMultiplier = c; duration = dur;
        }
        public bool IsValid() { return (duration <= 0 || contusionMultiplier <=0) ? false : true; }

        public Contusion Clone()
        {
            Contusion clone = new Contusion();
            clone.contusionMultiplier = contusionMultiplier;
            clone.duration = duration;
            return clone;
        }
    }

   /* [System.Serializable]
    public class IncomDamageUp
    {
        public int effectID = 0;
        public float duration = 0;
        public float Multiplier = 1f;

        public IncomDamageUp(float m = 0, float dur = 0)
        {
            Multiplier = m; duration = dur;
        }
        

        public bool IsValid() { return (duration <= 0 || Multiplier <= 1) ? false : true; }

        public IncomDamageUp Clone()
        {
            IncomDamageUp clone = new IncomDamageUp();
            clone.effectID = effectID;
            clone.Multiplier = Multiplier;
            clone.duration = duration;
            return clone;
        }
    }
    */
    [System.Serializable]
	public class Slow{
		public int effectID=0;
		public float duration=0;
		public float slowMultiplier=1f;
        public float auraRange;
		//~ private float timeEnd=0;
		
		public Slow(float s=0, float dur=0){
			slowMultiplier=s; duration=dur;
		}
		
		//~ public float GetTimeEnd(){ return timeEnd; }
		//~ public void SetTimeEnd(){ timeEnd=Time.time+duration;}
		
		public bool IsValid(){ return (duration<=0 || slowMultiplier<=0) ? false : true;}
		
		public Slow Clone(){
			Slow clone=new Slow();
			clone.effectID=effectID;
			clone.slowMultiplier=slowMultiplier;
			clone.duration=duration;
			//~ clone.timeEnd=timeEnd;
			return clone;
		}
	}
	
	[System.Serializable]
	public class Dot{
		public int effectID=0;
		public float duration=0f;
		public float interval=0f;
		public float value=0f;
        public bool isAbility=false;
        public enum type
        {
            Poison,
            Fire
        }
        public type Type;
		
		public Dot(type type= type.Fire, float dur=0, float i=0, float val=0,bool ability=false)
        {
            Type = type;
            isAbility = ability;
			duration=dur; interval=i;	value=val;
		}
		
		public float GetTotalDamage(){ return (duration/interval)*value; }
		
		public Dot Clone(){
			Dot clone=new Dot();
            clone.Type = Type;
			clone.effectID=effectID;
			clone.duration=duration;
			clone.interval=interval;
			clone.value=value;
            clone.isAbility = isAbility;
			return clone;
		}
	}
	
	[System.Serializable]
	public class InstantKill{
		public float chance=0;
		public float HPThreshold=0.3f;
		
		public InstantKill(float c=0, float th=0){
			chance=c; HPThreshold=th;
		}
		
		public bool IsValid(){	//funcion to determine if the effect is available
			if(HPThreshold>0 && chance>0) return true;
			return false;
		}
		
		public bool IsApplicable(float HP, float fullHP){
			if((HP/fullHP)<=HPThreshold){
				if(Random.Range(0f, 1f)<chance) return true;
			}
			return false;
		}
		
		public InstantKill Clone(){
			InstantKill instantKill=new InstantKill();
			instantKill.chance=chance;
			instantKill.HPThreshold=HPThreshold;
			return instantKill;
		}
	}
	
	
	[System.Serializable]
	public class Buff{
		//in case of multiple buff instance, the most powerful buff from each stat is taken as the actual effective buff value
		public int effectID=0;		//this is the instanceID of the casting unit
		public float damageBuff=0f;
		public float cooldownBuff=0f;
		public float rangeBuff=0f;
		public float criticalBuff=0f;
		public float hitBuff=0f;
		public float dodgeBuff=0f;
		public float regenHP=0f;
        public float chance = 0f;
        public float duration = 0f;
		
		public Buff Clone(){
			Buff clone=new Buff();
			clone.effectID=effectID;
			clone.damageBuff=damageBuff;
			clone.cooldownBuff=cooldownBuff;
			clone.rangeBuff=rangeBuff;
			clone.criticalBuff=criticalBuff;
			clone.hitBuff=hitBuff;
			clone.dodgeBuff=dodgeBuff;
			clone.regenHP=regenHP;
            clone.chance = chance;
            clone.duration = duration;
            return clone;
		}
	}
	
}
