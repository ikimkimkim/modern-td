﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK {

	[System.Serializable]
	public class WaveGenerator{
		
		public ProceduralVariable subWaveCount=new ProceduralVariable(1, 4);
		
		public ProceduralVariable unitCount=new ProceduralVariable(5, 50);
		
		public List<PathTD> pathList=new List<PathTD>();
		
		public List<ProceduralVariable> rscSettingList=new List<ProceduralVariable>();
		
		public List<ProceduralUnitSetting> unitSettingList=new List<ProceduralUnitSetting>();
		
		public void CheckPathList(){
			for(int i=0; i<pathList.Count; i++){
				if(pathList[i]==null){
					pathList.RemoveAt(i);
					i-=1;
				}
			}
		}
		
		public Wave Generate(int waveID){
			if(pathList.Count==0){
				Debug.Log("no path at all");
				return null;
			}
			
			Wave wave=new Wave();
			wave.waveID=waveID;
			
			waveID+=1;
			
			int _subWaveCount=Mathf.Max(1, (int)subWaveCount.GetValueAtWave(waveID));
			int totalUnitCount=(int)unitCount.GetValueAtWave(waveID);
			
			_subWaveCount=Mathf.Min(totalUnitCount, _subWaveCount);

            //Debug.Log("ID волны:"+ waveID+ ", групп:" + _subWaveCount + ", крипов:" + totalUnitCount);

			//filter thru all the units, only use the one that meets the wave requirement (>minWave)
			List<ProceduralUnitSetting> availableUnitList=new List<ProceduralUnitSetting>();
			int nearestAvailableID=0;
            float currentNearestValue=Mathf.Infinity;
			for(int i=0; i<unitSettingList.Count; i++){
				if(!unitSettingList[i].enabled) continue;
				if(unitSettingList[i].minWave<=waveID) availableUnitList.Add(unitSettingList[i]);
				//while we are at it, check which unit has the lowest wave requirement, just in case
				if(availableUnitList.Count==0 && unitSettingList[i].minWave<currentNearestValue){
					currentNearestValue=unitSettingList[i].minWave;
					nearestAvailableID=i;
				}
			}
			//if no unit available, simply uses the one with lowest requirement
			if(availableUnitList.Count==0) availableUnitList.Add(unitSettingList[nearestAvailableID]);

            //we are going to just iterate thru the pathlist and assign them to each subwave. 
            //So here we introduce an offset so it doesnt always start from the first path in the list
            

			int startingPathID=Mathf.RoundToInt(Random.Range(0, pathList.Count));
            float lastWaveDelay = 0;
            int /*LastUnitID=-1,*/ unitID=0;
            List<int> LastUnitsID = new List<int>();
            int countUnitsID = Mathf.Min(3,availableUnitList.Count);
            bool bBoss = false;

            UnitCreep Creep;
            int CountPathFind = 0;

			for(int i=0; i<_subWaveCount; i++)
            {
				SubWave subWave=new SubWave();
                unitID = Random.Range(0, availableUnitList.Count);
                int countTry = 0;
                while ((LastUnitsID.Contains(unitID)==false && LastUnitsID.Count >= countUnitsID)
                    //|| LastUnitID == unitID 
                    || (LastUnitsID.Contains(unitID) == true && LastUnitsID.Count < countUnitsID)
                    || (bBoss == true && availableUnitList[unitID].unit.GetComponent<UnitCreep>().isBoss) )
                {
                    countTry++;
                    if(countTry>1000)
                    {
                        Debug.LogError("Generator Error, lastID count:" + LastUnitsID.Count+" boss:"+bBoss);
                        break;
                    }
                    if (LastUnitsID.Count==countUnitsID)
                    {
                        unitID = LastUnitsID[Random.Range(0, LastUnitsID.Count)];
                        continue;
                    }

                    if (availableUnitList.Count > 1)
                        unitID = Random.Range(0, availableUnitList.Count);
                    else
                    {
                        unitID = availableUnitList.Count;
                        break;
                    }
                }
                if (LastUnitsID.Count < countUnitsID)
                    LastUnitsID.Add(unitID);
                //LastUnitID = unitID;

                ProceduralUnitSetting unitSetting=availableUnitList[unitID];
				
				subWave.unit=unitSetting.unit.gameObject;
                if (bBoss == false)
                {
                    bBoss = subWave.unit.GetComponent<UnitCreep>().isBoss;
                }

                if(CardAndLevelSelectPanel.Survival)
                    subWave.overrideHP = unitSetting.HP.GetValueAtWave(waveID,0.02f);
                else
                    subWave.overrideHP=unitSetting.HP.GetValueAtWave(waveID);

                switch(waveID)
                {
                    case 1:
                        subWave.overrideHP = subWave.overrideHP * 0.7f;
                        break;
                    case 2:
                        subWave.overrideHP = subWave.overrideHP * 0.8f;
                        break;
                    case 3:
                        subWave.overrideHP = subWave.overrideHP * 0.9f;
                        break;
                    case 4:
                        break;
                    default:
                        if(SpawnManager.getRandomCreepHP)
                            subWave.overrideHP = subWave.overrideHP * Random.Range(0.75f, 1.1f);
                        break;
                }
				subWave.overrideShield=unitSetting.shield.GetValueAtWave(waveID);
				subWave.overrideMoveSpd=unitSetting.speed.GetValueAtWave(waveID);
				
				//limit the minimum interval to 0.25f
				subWave.interval=Mathf.Max(0.25f, unitSetting.interval.GetValueAtWave(waveID));

                //iterate through the path, randomly skip one

                //Debug.Log(i+" startingPathID:" + startingPathID);

                startingPathID++;
                while (startingPathID >= pathList.Count)
                    startingPathID -= pathList.Count;

                CountPathFind = 0;
                while (subWave.path == null)
                {
                    if (pathList[startingPathID].TypeSpawn == PathTD.Type.Hybrid)
                    {
                        subWave.path = pathList[startingPathID];
                    }
                    else
                    {
                        Creep = subWave.unit.GetComponent<UnitCreep>();
                        if (pathList[startingPathID].TypeSpawn == PathTD.Type.OnlyFly && Creep.flying==true)
                        {
                            subWave.path = pathList[startingPathID];
                        }
                        else if (pathList[startingPathID].TypeSpawn == PathTD.Type.OnlyGround && Creep.flying==false)
                        {
                            subWave.path = pathList[startingPathID];
                        }
                    }
                    CountPathFind++;
                    if (CountPathFind > pathList.Count)
                    {
                        Debug.Log("WaveGenerator Error: Path for subWave not find!");
                        subWave.path = pathList[startingPathID];
                        break;
                    }
                }
                subWave.overrideHP = subWave.overrideHP * subWave.path.HpIncrement;

                    wave.subWaveList.Add(subWave);
			}
            
            //Случайное определение количества мобов в волне с учатом что в каждой будет по 1 мин
            /* int remainingUnitCount=totalUnitCount;
             while (remainingUnitCount>0)
             {
                 for(int i=0; i<_subWaveCount; i++)
                 {
                     if(wave.subWaveList[i].count==0)
                     {
                         wave.subWaveList[i].count=1;
                         remainingUnitCount-=1;
                     }
                     else
                     {
                         int rand=Random.Range(0, 3);
                         rand=Mathf.Min(rand, remainingUnitCount);
                         wave.subWaveList[i].count+=rand;
                         remainingUnitCount-=rand;
                     }
                 }
             }*/

            int countMob = Mathf.CeilToInt(totalUnitCount / wave.subWaveList.Count);
            UnitCreep creep;
            for (int i = 0; i < _subWaveCount; i++)
            {
                //Указываю количество мобов в группе с учетом параметра моба
                //Debug.Log("Данные кол. Start:" + countMob + ", RandomSpawnIncrement:" + wave.subWaveList[i].unit.GetComponent<UnitCreep>().RandomSpawnIncrement);
                creep = wave.subWaveList[i].unit.GetComponent<UnitCreep>();
                wave.subWaveList[i].count = Mathf.FloorToInt(creep.RandomSpawnIncrement * countMob);
                if (wave.subWaveList[i].count <= 0 || creep.isBoss)
                    wave.subWaveList[i].count = 1;

                //MyLog.Log(creep.unitName + " count:"+ wave.subWaveList[i].count);
            }

            float K = Mathf.Max(-0.2f * pathList.Count, -0.8f);
            float durationSubWave = 0;
            for (int i = 0; i < _subWaveCount; i+=pathList.Count)
            {
                for (int j = 0; j < pathList.Count; j++)
                {
                    int index = i + j;
                    if (index >= _subWaveCount)
                        break;
                    if (index == 0)
                    {
                        wave.subWaveList[index].delay = 0;
                    }
                    else if(i==0)
                    {
                        wave.subWaveList[index].delay = Random.Range(0f,10f);
                    }
                    else
                    {
                        //Расчет задержки для группы с учетом количества мобов
                        //durationSubWave = (wave.subWaveList[index - pathList.Count].count ) * (wave.subWaveList[index - pathList.Count].interval);
                        durationSubWave = (wave.subWaveList[index - 1].count) * (wave.subWaveList[index - 1].interval);

                        durationSubWave = durationSubWave * Random.Range(1+K, 1.5f+K);
                        //durationSubWave = Random.Range(0.5f * durationSubWave, 1.2f * durationSubWave);
                        //durationSubWave = Mathf.Max(5, durationSubWave);
                       // MyLog.Log(index + "  delay:" + durationSubWave + " path:" + j + " lastDelay:" + lastWaveDelay);
                        //Debug.LogWarning(index + " delay:" + wave.subWaveList[index - pathList.Count].delay);
                        wave.subWaveList[index].delay = lastWaveDelay + durationSubWave;
                    }

                    //lastWaveDelayPath[j] = wave.subWaveList[index].delay;
                    lastWaveDelay = wave.subWaveList[index].delay;
                    //MyLog.Log("группа №"+ index + " задержка " + lastWaveDelay, MyLog.Type.build);

                }

            }




            wave.duration=wave.CalculateSpawnDuration();
			
			//get the slowest moving unit and the longest path so we know which subwave is going to take the longest to finish
			float longestDuration=0;
			for(int i=0; i<_subWaveCount; i++){
				float pathDist=wave.subWaveList[i].path.GetPathDistance();
				float moveSpeed=wave.subWaveList[i].overrideMoveSpd;
				float duration=pathDist/moveSpeed;
				if(duration>longestDuration) longestDuration=duration;
			}
			//add the longest to the existing duration
			wave.duration+=longestDuration*Random.Range(0.5f, 0.8f);
			
			for(int i=0; i<rscSettingList.Count; i++){
				wave.rscGainList.Add((int)rscSettingList[i].GetValueAtWave(waveID));
			}



			return wave;
		}
	}

	
	
	[System.Serializable]
	public class ProceduralUnitSetting{
		public GameObject unit;
		
		public bool enabled=true;
		public int minWave=0;	//minimum wave to appear
		
		public ProceduralVariable HP=new ProceduralVariable(1, 50);		//HP
		public ProceduralVariable shield=new ProceduralVariable(0, 25);	//shield
		public ProceduralVariable speed=new ProceduralVariable(1, 6);	//speed
		public ProceduralVariable interval=new ProceduralVariable(1, 6);	//spawn interval
	}
	
	[System.Serializable]
	public class ProceduralVariable{
		public float startValue=5;		//C as in linear equation y=mx+C		x being the wave number, start at x=0
		public float incMultiplier=1f;		//M as in linear equation y=Mx+c		
		public float devMultiplier=0.2f;	//20% deviation, deviation multiplier applied after y in y=mx+c is calcualted
		public float minValue=1;
		public float maxValue=50;
		
		public ProceduralVariable(float val1, float val2){
			startValue=val1;
			maxValue=val2;
		}
		
		//linear increament, y=mx+c
		public float GetValueAtWave(int waveID){
			float value=(incMultiplier*(waveID-1)+startValue)*(1f+Random.Range(-devMultiplier, devMultiplier));
            //Debug.Log(value +"=("+ incMultiplier+"*"+ (waveID-1)+"+" +startValue+")*(1+"+devMultiplier+") >="+minValue);
			return Mathf.Clamp(value, minValue, maxValue);
		}
        public float GetValueAtWave(int waveID, float exponent)
        {
            float value = (incMultiplier * (waveID - 1) + Mathf.Pow(startValue,1f+exponent*(waveID-1)))
                * (1f + Random.Range(-devMultiplier, devMultiplier));
            //Debug.Log(value +"=("+ incMultiplier+"*"+ (waveID-1)+"+" +startValue+")*(1+"+devMultiplier+") >="+minValue);
            return Mathf.Clamp(value, minValue, maxValue);
        }
        //non-linear, compounding increment, experimental, not in use
        //public float currentValue=5;		//also act as starting value
        //public float incMultiplier=1f;		//inc by 10% each iteration
        //public float devMultiplier=0.2f;	//10% deviation each iteration
        //currentValue=currentValue*(incMultiplier+Random.Range(-devMultiplier, devMultiplier));
    }
	
	
}