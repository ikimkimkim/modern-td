﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;
using System;
using UnityEngine.AI;
using SystemUpdate;

namespace TDTK
{

    public delegate void MyAttakInstanceHandler(AttackInstance attackInstance);
    public delegate void AttakInstanceHandler(Unit from, AttackInstance attackInstance);

    public interface IUnitAttacking
    {
        string GetName();

        Vector3 getPosition { get; }
        EffectManager EffectsManager { get; }

        int DamageType();
        bool IgnoreTypeArmor();
        float GetDamageMin();
        float GetDamageMax();
        int GetShootPointCount();
        float GetHit();
        float GetShieldPierce();

        LayerMask GetTargetMask();

        void CallEventOutAttakInit(AttackInstance attackInstance);
        void CallEventOutAttakFirst(AttackInstance attackInstance);
        void CallEventOutAttakLast(AttackInstance attackInstance);
    }
    public interface IUnitAttacked
    {
        Vector3 getPosition { get; }
        EffectManager EffectsManager { get; }

        float GetHP();
        float GetFullHP();

        int ArmorType();
        float GetAdditiveArmor();
        bool ShieldExist();
        float GetDodge();

        List<IUnitAttacking> GetListUnitDamageMy();

        void CallEventInAttakInit(AttackInstance attackInstance);
        void CallEventInAttakFirst(AttackInstance attackInstance);
        void CallEventInAttakLast(AttackInstance attackInstance);
    }


    public class Unit : MonoBehaviour, IUnitAnimation, IUnitAttacking, IUnitAttacked, IItemUpdate
    {

        //public delegate void NewUnitHandler(Unit unit);
        //public static event NewUnitHandler onNewUnitE;	//no longer in use for now?

        public delegate void DamagedHandler(Unit unit, float damag = 0);
        public static event DamagedHandler onDamagedE;	//call when unit HP/shield value is changed, for displaying unit overlay
        public delegate void MyDamagedHandler(float damag = 0);
        public event MyDamagedHandler onMyDamagedE;	

        public event MyAttakInstanceHandler myOutAttakInstanceInitE, myOutAttakInstanceFirstE, myOutAttakInstanceLastE;
        public event MyAttakInstanceHandler myInAttakInstanceInitE, myInAttakInstanceFirstE, myInAttakInstanceLastE;

        public static event AttakInstanceHandler ApplyAttackE;
        public event MyAttakInstanceHandler myApplyAttackInstance;


        public delegate void DestroyedHandler(Unit unit);
        public static event DestroyedHandler onInitE;
        public event DestroyedHandler onMyInitE;
        public static event DestroyedHandler onStartDestroyedE;
        public static event DestroyedHandler onDestroyedE;
        public event DestroyedHandler onMyDestroyedE;
        public event DestroyedHandler onMyStartDestroyedE;

        public delegate void ShotDelegate(Unit target);
        public ShotDelegate onShotTurretE;

        public delegate void HealDelegat(float countHeal);
        public event HealDelegat HealEvent;

        public List<IUnitAttacking> listUnitDamageMy;
        public List<IUnitAttacking> GetListUnitDamageMy() { return listUnitDamageMy; }
        public bool isInit { get; private set; }
        protected Card _myCard;
        public Card MyCard
        {
            get { return _myCard; }
            protected set
            {
                if (value == null) MyLog.LogWarning(String.Format("Set null card! {0}", name)); _myCard = value;
            }
        }

        public int prefabID = -1;
        public int instanceID = -1;

        public string unitName = "unit";
        public string GetName() { return unitName; }
        public Sprite iconSprite;
        //public Texture icon;

        protected EffectManager _EffectsManager;
        public EffectManager EffectsManager { get { return _EffectsManager; }  }

        public IUnitAttacking KillMyUnit { get; private set; }

        public AudioSource DeadAudio;

        public string desp = "";
        public string characteristic = "";


        public UnityEngine.AI.NavMeshAgent _Agent { get; protected set; }

        public _TargetMode targetMode = _TargetMode.Hybrid;

        public bool isShowHPBar = true;
        public bool isShowShieldBar = true;


        public Unit()
        {

        }

        public ParticleSystem[] FireEffectPS;
        public ParticleSystem[] FreezingEffectPS;
        public ParticleSystem[] PoisenEffectPS;


        public bool PropertiesAttakLock = false;

        public float defaultHP = 10;
        public float fullHP = 10;
        public float GetFullHP() { return fullHP; } 
        public float HP = 10;
        public float GetHP () { return HP; } 
        public float HPRegenRate = 0;
        public float HPStaggerDuration = 10;
        protected float currentHPStagger = 0;

        [Range(0f, 1f)]
        public float PercentAttackAnimation = 1f;

        public float SizeMyCollider = 0.5f;
        public float RangeVition = 3;
        public SphereCollider VitionCollider;

        public bool isApplyDeffScaleForInit = true;
        public Vector3 deffScale = Vector3.one;
        private Vector3 currentScale;

        private Dictionary<int, ShieldData> shields;
        public ShieldData getShield(int id)
        {
            if (shields.ContainsKey(id) == false)
                return null;
            return shields[id];
        }

        public float defaultShield = 0;
        //public float fullShield = 0;
        public float fullShield
        {
            get
            {
                if (shields == null)
                    return 0;
                float full = 0;
                foreach (var _shield in shields)
                {
                    full += _shield.Value.FullShield;
                }
                return full;
            }
        }


        //public float shield = 0;
        public float shield
        {
            get
            {
                if (shields == null)
                    return 0;
                float v = 0;
                foreach (var _shield in shields)
                {
                    v += _shield.Value.Value;
                }
                return v;
            }
        }

        public bool ShieldExist() { return shields != null && shields.Count > 0; }

        //public float shieldRegenRate = 1;
        //public float shieldStaggerDuration = 1;
        //protected float currentShieldStagger = 0;

        public bool ignoreTypeArmor = false;
        public bool IgnoreTypeArmor() { return ignoreTypeArmor; }



        public string UrlIconDamageType
        {
            get
            {
                switch (damageType)
                {
                    case 0:
                        return "EffectIcon/DmgT_0.png";
                    case 1:
                        return "EffectIcon/DmgT_1.png";
                    case 2:
                        return "EffectIcon/DmgT_2.png";
                    case 3:
                        return "EffectIcon/DmgT_3.png";
                    default:
                        Debug.LogError(name + " unit getIcomDmgType out of range: " + damageType);
                        return "EffectIcon/DmgT_0.png";
                }
            }
        }

        public int damageType = 0;
        public int DamageType() { return damageType; }
        public int armorType = 0;
        public int ArmorType() { return armorType; }
        [HideInInspector]
        public float additiveArmor = 0;
        public float GetAdditiveArmor() { return additiveArmor; }

        //public bool immuneToDmg=false;
        public bool immuneToCrit = false;
        public bool immuneToSlow = false;
        public bool immuneToStun = false;
        public bool immuneToAllAbility = false;
        [Range(0f, 1f)]
        public float damageAbilityIncrementImmune=0.5f;

        public float slowIncrement=1, slowTimeIncrement=1, stunTimeIncrement=1, 
            contusionTimeIncrement=1,  contusionIncrement = 1,
            XPIncrement = 1;

        //public int level=1;

        public int currentActiveStat = 0;
        public List<UnitStat> stats = new List<UnitStat>();


        public bool dead = false;
        public bool isDead { get { return dead; } }

        public virtual float GetSpeedMoveAnimation { get { return 0; } }
        public virtual float GetMultiplierMoveAnimation
        {
            get
            {
                return Mathf.Clamp(GetSpeedMoveAnimation, 1, 100);
            }
        }

        private Dictionary<int, bool> stunneds;

        public bool stunned
        {
            get { return stunneds!=null? stunneds.ContainsValue(true) : false; }
        }

        public void AddStun(int id)
        {
            if (stunneds.ContainsKey(id))
                stunneds[id] = true;
            else
                stunneds.Add(id, true);
        }

        public void RemoveStun(int id)
        {
            if (stunneds.ContainsKey(id))
                stunneds[id] = false;
        }


        public int countKillUnitCurrentGame = 0;
        public float allDmgCurrentGame = 0;
        public bool lockMovePach=false;

        private Dictionary<int, float> MoveMultiplier;
        public void SetMoveMultiplier(int id, float procent)
        {
            if (MoveMultiplier.ContainsKey(id))
                MoveMultiplier[id] = procent;
            else
                MoveMultiplier.Add(id, procent);
            UpdateMoveSpeedAgent();
        }
        public void RemoveMoveMultiplier(int id)
        {
            if (MoveMultiplier.ContainsKey(id))
                MoveMultiplier.Remove(id);
            UpdateMoveSpeedAgent();
        }


        public float ContusionMultiplier = 1;
        public List<Contusion> ContusionEffectList = new List<Contusion>();



        public List<Buff> buffEffect = new List<Buff>();


        [HideInInspector]
        public Transform localShootObjectT;
        [HideInInspector]
        public ShootObject localShootObject;		//get from stats and store locally, just in case the upgraded stats doesnt have any shootObject assigned
        public List<Transform> shootPoints = new List<Transform>();
        public float delayBetweenShootPoint = 0;
        public Transform targetPoint;
        public float hitThreshold = 0.25f;		//hit distance from the targetPoint for the shootObj

        [HideInInspector]
        public GameObject thisObj;
        [HideInInspector]
        public Transform thisT;
        public GameObject StunEffectPrefab;
        GameObject _stunEffect;
        public int Crystals;

       
        public int MaxTargetМе = -1;
        public List<Unit> TargetMe;

        public UnitCreepAnimation animationUnit { get; protected set; }
        public void SetAnimationComponent(UnitCreepAnimation animation) { animationUnit = animation; }

        public Coroutine CorTurretRoutine;

        public virtual void Awake()
        {
            thisObj = gameObject;
            thisT = transform;
            startName = name;

            ListTarget = new List<Unit>();
            _Agent = GetComponent<NavMeshAgent>();

            if (shootPoints.Count == 0)
                shootPoints.Add(thisT);

           // ResetBuff();

            for (int i = 0; i < stats.Count; i++)
            {
                if (stats[i].shootObject != null)
                {
                    stats[i].shootObjectT = stats[i].shootObject.transform;
                    if (localShootObject == null) localShootObject = stats[i].shootObject;
                }

                if (stats[i].shootObjectT != null)
                {
                    if (localShootObjectT == null) localShootObjectT = stats[i].shootObjectT;
                }
            }
        }

        public virtual Vector3 getPosition
        {
            get
            {
                return thisT.position;
            }
        }

        public bool IsEgg { get { return this is UnitEggCreep; } }

        public bool IsCreep { get { return this is UnitCreep; } }
        public UnitCreep GetUnitCreep { get { if (IsCreep) return this as UnitCreep; else return null; } }
        public bool IsTower { get { return this is UnitTower; } }
        public UnitTower GetUnitTower { get { if (IsTower) return this as UnitTower; else return null; } }
        public bool IsHero { get { return this is UnitHero; } }
        public UnitHero GetUnitHero { get { if (IsHero) return this as UnitHero; else return null; } }
        public bool IsArtifact { get { return this is UnityArtifact; } }
        public UnityArtifact GetUnitArtifact { get { if (IsArtifact) return this as UnityArtifact; else return null; } }

        protected string startName;
        public virtual void Init(int startCountKill = 0,float allDmg = 0)
        {
            dead = false;

            if (stunneds == null)
                stunneds = new Dictionary<int, bool>();
            else
                stunneds.Clear();
           // stunned = false;
            //isDotActive = false;
            target = null;
            KillMyUnit = null;
            lockMovePach = false;

            if (_EffectsManager == null)
                _EffectsManager = new EffectManager(this);
            else
                _EffectsManager.ClearData();

            EffectsManager.addMyEffectE += EffectsManager_addMyEffectE;
            EffectsManager.removeMyEffectE += EffectsManager_removeMyEffectE;

            if (ListTarget != null)
                ListTarget.Clear();
            else
                ListTarget = new List<Unit>();

            if (listUnitDamageMy != null)
                listUnitDamageMy.Clear();
            else
                listUnitDamageMy = new List<IUnitAttacking>();

            if (CorTurretRoutine != null)
                StopCoroutine(CorTurretRoutine);

            fullHP = defaultHP;
            HP = fullHP;
            //fullShield = GetFullShield();
            //shield = fullShield;
            //currentShieldStagger = 0;
            if (shields != null)
                shields.Clear();
            else
                shields = new Dictionary<int, ShieldData>();

            if (TargetMe != null)
                TargetMe.Clear();
            //TargetMe = 0;

            countKillUnitCurrentGame = startCountKill;
            allDmgCurrentGame = allDmg;
            currentHPStagger = 0;
            //disorientationMultiplier = 1;
            //weaknessMultiplier = 1;

            if (MoveMultiplier == null)
                MoveMultiplier = new Dictionary<int, float>();
            else
                MoveMultiplier.Clear();

            UpdateMoveSpeedAgent();

            if (isApplyDeffScaleForInit)
                thisT.localScale = deffScale;

            currentScale = thisT.localScale;
            if (VitionCollider != null && thisT.localScale.z>0)
                VitionCollider.radius = RangeVition/ thisT.localScale.z;

            isInit = true;
            if (onInitE != null)
                onInitE(this);
            if (onMyInitE != null)
                onMyInitE(this);
        }

        public void AddHeal(float healPoint)
        {
            AddHP(healPoint);
#if UNITY_EDITOR
            if (GameControl.GET_EDITOR_ShowDamage)
                new TextOverlay(thisT.position, healPoint.ToString(), Color.green, false);
#endif
            if (HealEvent != null)
                HealEvent(healPoint);
        }

        public void AddHP(float hp)
        {
            //Debug.Log("Add hp:" + hp);
            HP = Mathf.Clamp(HP + hp, HP, fullHP);
        }

        public void AddShield(int key, ShieldData data)// float shieldPoint)
        {
            //Debug.Log("Add shield:" + data.Value);
            if(shields.ContainsKey(key))
            {
                shields[key] = data;
            }
            else
            {
                shields.Add(key, data);
            }
            /*
            shield = shieldPoint;
            fullShield = shield;*/
        }

        public float CalcDmgInShield(bool ignorArmor, float dmg, int type)
        {
            if (shields == null || shields.Count == 0)
                return dmg;

            float ostDmg= dmg;
            float dmgM, realDmg;
            foreach(int id in shields.Keys)
            {
                dmgM = ignorArmor ? 1 : DamageTable.GetModifier(shields[id].ArmorType, type);
                realDmg = ostDmg * dmgM;
                if(realDmg > shields[id].Value)
                {
                    ostDmg = (realDmg - shields[id].Value);
                    shields[id].Value = 0;
                    ostDmg = ostDmg / dmgM;
                }
                else
                {
                    shields[id].Value -= realDmg;
                    ostDmg = 0;
                    break;
                }
            }
            return ostDmg;
        }

        public void RemoveShield(int key)
        {
            if (shields!=null && shields.ContainsKey(key))
                shields.Remove(key);
        }



        protected virtual void Start()
        {
            if (DeadAudio == null)
                DeadAudio = GetComponent<AudioSource>();
        }

        public virtual void OnEnable()
        {
            MainSystem.AddItem(this);
            onDestroyedE += OnDestroyUnit;
        }
        public virtual void OnDisable()
        {
            MainSystem.RemoveItem(this);
            onDestroyedE -= OnDestroyUnit;
        }

        protected virtual void OnDestroy()
        {
            ClearProperties();
        }


        public void ClearProperties()
        {
            if (MyCard == null)
                return;

            foreach (CardProperties cp in MyCard.Properties)
            {
                if (cp.ID == CardProperties.IDCoomplect)
                    PropertiesHelp.listCoomplectPropertiesBild.Remove(cp);
                else
                    cp.Refresh(false);
            }

            for (int i = 0; i < MyCard.Properties.Count; i++)
            {
                MyCard.Properties[i].Cancel();
            }
        }


        protected void RegenStat()
        {
           /* if (regenHPBuff != 0)
            {
                HP += regenHPBuff * Time.deltaTime;
                HP = Mathf.Clamp(HP, 0, fullHP);
            }*/

            if (HPRegenRate > 0 && currentHPStagger <= 0)
            {
                HP += GetHPRegenRate() * Time.deltaTime;
                HP = Mathf.Clamp(HP, 0, fullHP);
            }

            currentHPStagger -= Time.deltaTime;
        }

        public virtual void SystemUpdate()
        {
            if (dead)
                return;

            if (VitionCollider != null)
            {
                var range = GetRange();
                if (RangeVition < range)
                {
                    RangeVition = range;
                    VitionCollider.radius = RangeVition / thisT.localScale.z;
                }
                if (currentScale.z != thisT.localScale.z && thisT.localScale.z > 0)
                {
                    currentScale = thisT.localScale;
                    VitionCollider.radius = RangeVition / thisT.localScale.z;
                }
            }

            if (EffectsManager != null)
                EffectsManager.Update();

            RegenStat();
            TargetLOSUpdate();
        }


        public virtual void FixedUpdate()
        {

        }

        protected void TargetLOSUpdate()
        {
            if (target != null && !IsInConstruction() && !stunned)
            {
                if (turretObject != null)
                {
                    if (rotateTurretAimInXAxis && barrelObject != null)
                    {
                        Vector3 targetPos = target.GetTargetT().position;
                        Vector3 dummyPos = targetPos;
                        dummyPos.y = turretObject.position.y;

                        Quaternion wantedRot = Quaternion.LookRotation(dummyPos - turretObject.position);
                        turretObject.rotation = Quaternion.Slerp(turretObject.rotation, wantedRot, turretRotateSpeed * Time.deltaTime);

                        float angle = Quaternion.LookRotation(targetPos - barrelObject.position).eulerAngles.x;
                        float distFactor = Mathf.Min(1, Vector3.Distance(turretObject.position, targetPos) / GetSOMaxRange());
                        float offset = distFactor * GetSOMaxAngle();
                        wantedRot = turretObject.rotation * Quaternion.Euler(angle - offset, 0, 0);

                        barrelObject.rotation = Quaternion.Slerp(barrelObject.rotation, wantedRot, turretRotateSpeed * Time.deltaTime);

                        if (Quaternion.Angle(barrelObject.rotation, wantedRot) < aimTolerance) targetInLOS = true;
                        else targetInLOS = false;
                    }
                    else
                    {
                        Vector3 targetPos = target.GetTargetT().position;
                        if (this is UnitCreep)
                        {
                            var creep = this as UnitCreep;
                            targetPos += Vector3.up * creep.flyingHeightOffset;
                        }
                        if (!rotateTurretAimInXAxis)
                            targetPos.y = turretObject.position.y;

                        Quaternion wantedRot = Quaternion.LookRotation(targetPos - turretObject.position);
                        if (rotateTurretAimInXAxis)
                        {
                            float distFactor = Mathf.Min(1, Vector3.Distance(turretObject.position, targetPos) / GetSOMaxRange());
                            float offset = distFactor * GetSOMaxAngle();
                            wantedRot *= Quaternion.Euler(-offset, 0, 0);
                        }
                        turretObject.rotation = Quaternion.Slerp(turretObject.rotation, wantedRot, turretRotateSpeed * Time.deltaTime);

                        if (Quaternion.Angle(turretObject.rotation, wantedRot) < aimTolerance) targetInLOS = true;
                        else targetInLOS = false;
                    }
                }
                else targetInLOS = true;
            }
        }

        public virtual bool isMoveEnd()
        {
            if (dead)
            {
                //Debug.LogWarning("iME: Unit is dead");
                return true;
            }
            if ( _Agent == null )
            {
                //Debug.LogWarning("iME: Unit agent is null");
                return true;
            }
            if (_Agent.enabled == false)
            {
                //Debug.LogWarning("iME: Unit agent is enabled");
                return true;
            }

            if(_Agent.isStopped)
            {
                if(stunned)
                {
                    return false;
                }
                return true;
            }

            if (!_Agent.pathPending)
            {
                if (_Agent.remainingDistance <= _Agent.stoppingDistance)
                {
                    if (!_Agent.hasPath || _Agent.velocity.sqrMagnitude < 1f)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        

        public Transform turretObject;
        public Transform barrelObject;
        public bool rotateTurretAimInXAxis = true;
        float GetSOMaxRange()
        {
            if (stats[currentActiveStat].shootObject == null) return localShootObject.GetMaxShootRange();
            return stats[currentActiveStat].shootObject.GetMaxShootRange();
        }

        float GetSOMaxAngle()
        {
            if (stats[currentActiveStat].shootObject == null) return localShootObject.GetMaxShootAngle();
            return stats[currentActiveStat].shootObject.GetMaxShootAngle();
        }

        private float turretRotateSpeed = 10; //10
        private float aimTolerance = 15;
        public bool targetInLOS { get; protected set; }


        public Unit target, priorityTarget;

        public enum _TargetPriority { Nearest, Weakest, Toughest, First, Random };
        public _TargetPriority targetPriority = _TargetPriority.First;
        public void SwitchToNextTargetPriority()
        {
            int nextPrior = (int)targetPriority + 1;
            if (nextPrior >= 5) nextPrior = 0;
            targetPriority = (_TargetPriority)nextPrior;
        }

        public void ChangeScanAngle(int angle)
        {
            if (turretObject != null && target == null)
                turretObject.localRotation = Quaternion.identity * Quaternion.Euler(0f, dirScanAngle, 0f);
            dirScanAngle = angle;
            dirScanRot = thisT.rotation * Quaternion.Euler(0f, dirScanAngle, 0f);
            if (IsTower)
                GameControl.TowerScanAngleChanged(GetUnitTower);
        }


        public bool directionalTargeting = false;
        public float dirScanAngle = 0;
        public float dirScanFOV = 30;
        private Vector3 dirScanV;
        private Quaternion dirScanRot;
        protected LayerMask maskTarget = 0;
        public LayerMask GetTargetMask() { return maskTarget; }

        public Transform scanDirT;

        public float getDistanceFromTarget()
        {
           /* if(IsCreep && GetUnitCreep().flying)
            {                
                return getDistanceFromTarget(thisT.position - Vector3.up * GetUnitCreep().flyingHeightOffset);
            }*/
            return getDistanceFromTarget(target,thisT.position);
        }
        public float getDistanceFromTarget(Unit target, Vector3 startPos)
        {
            if (target == null)
                return float.MaxValue;
            else
            {
                Vector3 tp = target.thisT.position;
                tp.y = startPos.y;
                return Mathf.Clamp(Vector3.Distance(startPos, tp) - target.SizeMyCollider , 0, float.MaxValue);
                //if (target.IsCreep && target.GetUnitCreep.flying)
               //     return Mathf.Clamp(Vector3.Distance(startPos, target.thisT.position - Vector3.up * target.GetUnitCreep.flyingHeightOffset) - target.SizeMyCollider-SizeMyCollider, 0, float.MaxValue);
               //else
               //     return Mathf.Clamp(Vector3.Distance(startPos, target.thisT.position) - target.SizeMyCollider - SizeMyCollider, 0, float.MaxValue);
            }

        }
        public IEnumerator ScanForTargetRoutine()
        {
            if(VitionCollider==null)
            {
                Debug.LogError("VitionCollider is null! "+name);
            }
            //MyLog.Log(name+":"+ subClass+"|"+targetMode);
            if (IsTower || IsHero)
            {
                if (targetMode == _TargetMode.Hybrid)
                {
                    VitionCollider.gameObject.layer = LayerManager.LayerHybridTower();
                    LayerMask mask1 = 1 << LayerManager.LayerCreep();
                    LayerMask mask2 = 1 << LayerManager.LayerCreepF();

                    maskTarget = mask1 | mask2;
                }
                else if (targetMode == _TargetMode.Air)
                {
                    VitionCollider.gameObject.layer = LayerManager.LayerOnlyAir();
                    maskTarget = 1 << LayerManager.LayerCreepF();
                }
                else if (targetMode == _TargetMode.Ground)
                {
                    VitionCollider.gameObject.layer = LayerManager.LayerOnlyGround();
                    maskTarget = 1 << LayerManager.LayerCreep();
                }
            }
            else if (IsCreep)
            {
                if (targetMode == _TargetMode.OnlyArtifact)
                {
                    VitionCollider.gameObject.layer = LayerManager.LayerOnlyArtifact();
                      maskTarget = 1 << LayerManager.LayerArtifact();
                }
                else if(targetMode == _TargetMode.Hybrid)
                {
                    VitionCollider.gameObject.layer = LayerManager.LayerHybridCreep();
                    LayerMask mask1 = 1 << LayerManager.LayerArtifact();
                    LayerMask mask2 = 1 << LayerManager.LayerTower();
                    LayerMask mask3 = 1 << LayerManager.LayerHero();
                    maskTarget = mask1 | mask2 | mask3;
                }
            }

            yield return null;
        }

        public void SeeNewTarget(Collider other)
        {
            SeeNewTarget(other.GetComponent<Unit>());
        }
        public void SeeNewTarget(Unit other)
        {
            MyLog.Log(name + "SeeNewTarget " + other.name + " all:" + ListTarget.Count);
            ListTarget.Add(other);
            ScanForTarget();
        }
        public void LostNewTarget(Collider other)
        {
            LostNewTarget(other.GetComponent<Unit>());
        }
        public void LostNewTarget(Unit other)
        {
            MyLog.Log(name + "LostNewTarget " + other.name + " all:" + ListTarget.Count);
            ListTarget.Remove(other);
            ScanForTarget();
        }

        void OnDestroyUnit(Unit unit)
        {
            if (ListTarget.Contains(unit) == true)
                ListTarget.Remove(unit);
        }

        int CountICanTarget()
        {
            List<Unit> herolist = ListTarget.FindAll(i => i.IsHero && (i.MaxTargetМе==-1 || i.TargetMe.Count < i.MaxTargetМе));

            return herolist.Count;
        }

        public /*Collider[] cols;*/ List<Unit> ListTarget;
        private Unit newT;
        protected void ScanForTarget()
        {
            if (dead || IsInConstruction() || stunned) return;

            //creeps changes direction so the scan direction for creep needs to be update 
            if (directionalTargeting)
            {
                if (IsCreep)
                    dirScanRot = thisT.rotation;
                else
                    dirScanRot = thisT.rotation * Quaternion.Euler(0f, dirScanAngle, 0f);
            }

            if (directionalTargeting && scanDirT != null)
                scanDirT.rotation = dirScanRot;

            if (target != null)
            {
                if (target.dead)
                {
                    ListTarget.Remove(target);
                    target.TargetMe.Remove(this);
                    target = null;

                }
                else
                {
                    float dist = getDistanceFromTarget();
                    if (dist > GetRange())
                    {
                        target.TargetMe.Remove(this);
                        target = null;
                    }
                }
                
            }

            //cols = Physics.OverlapSphere(thisT.position, GetRange(), maskTarget);

            //MyLog.Log(name + " Tgt-1:" + ListTarget.Count);
            if (ListTarget.Count > 0)
            {
                float minRange = GetRangeMin();

                List<Unit> tgtList = new List<Unit>();
                for (int i = 0; i < ListTarget.Count; i++)
                {
                    if (ListTarget[i] == null)
                    {
                        ListTarget.RemoveAt(i);
                        i--;
                        continue;
                    }
                    if (minRange > 0 && Vector3.Distance(ListTarget[i].thisT.position, thisT.position) <= minRange)
                    {
                        continue;
                    }                    
                    Unit unit = ListTarget[i];
                    if (!unit.dead)
                    {
                        if(IsCreep)
                        {
                            if (unit.IsHero && (unit.MaxTargetМе == -1 || unit.MaxTargetМе > unit.TargetMe.Count) || unit.IsArtifact)
                                tgtList.Add(unit);
                            else if (unit is UnitTower && GetUnitCreep.CanTargetTower)
                                tgtList.Add(unit);
                        }
                        else
                        {
                            tgtList.Add(unit);
                        }
                    }
                    else
                    {
                        ListTarget.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
               // MyLog.Log(name + " Tgt:" + tgtList.Count);
                if (directionalTargeting)
                {
                    List<Unit> filtered = new List<Unit>();
                    for (int i = 0; i < tgtList.Count; i++)
                    {
                        Quaternion currentRot = Quaternion.LookRotation(tgtList[i].thisT.position - thisT.position);
                        if (Quaternion.Angle(dirScanRot, currentRot) <= dirScanFOV * 0.5f)
                            filtered.Add(tgtList[i]);
                    }
                    tgtList = filtered;
                }

                //MyLog.Log(name + " Tgt2:" + tgtList.Count);
                if (tgtList.Count > 0)
                {
                    if (targetPriority == _TargetPriority.Random)
                    {
                        newT = tgtList[UnityEngine.Random.Range(0, tgtList.Count - 1)];
                    }
                    else if (targetPriority == _TargetPriority.Nearest)
                    {
                        float nearest = Mathf.Infinity;
                        for (int i = 0; i < tgtList.Count; i++)
                        {
                            float dist = Vector3.Distance(thisT.position, tgtList[i].thisT.position);
                            if (dist < nearest)
                            {
                                nearest = dist;
                                newT = tgtList[i];
                            }
                        }

                    }
                    else if (targetPriority == _TargetPriority.Weakest)
                    {
                        float lowest = Mathf.Infinity;
                        for (int i = 0; i < tgtList.Count; i++)
                        {
                            if (tgtList[i].HP < lowest)
                            {
                                lowest = tgtList[i].HP;
                                newT = tgtList[i];
                            }
                        }
                    }
                    else if (targetPriority == _TargetPriority.Toughest)
                    {
                        float highest = 0;
                        for (int i = 0; i < tgtList.Count; i++)
                        {
                            if (tgtList[i].HP > highest)
                            {
                                highest = tgtList[i].HP;
                                newT = tgtList[i];
                            }
                        }
                    }
                    else if (targetPriority == _TargetPriority.First)
                    {
                        newT = TowerTargetManager.GetInstance().GetTarget(this, tgtList);
                    }

                    //MyLog.Log(name + " Tgt3:" + tgtList.Count);
                    if (priorityTarget != null && newT != priorityTarget)
                    {
                        if (priorityTarget.dead)
                        {
                            priorityTarget = null;
                        }
                        else
                        {
                            float dist = getDistanceFromTarget(priorityTarget, thisT.position);// Vector3.Distance(thisT.position, priorityTarget.thisT.position) - SizeMyCollider;
                            if (dist <= GetRange())
                            {
                                //MyLog.Log("NewT = priorT");
                                newT = priorityTarget;
                            }/*
                            else
                            {
                                MyLog.Log("NewT != priorT dist:" + dist);
                            }*/
                        }
                    }

                    if (target != newT)
                    {
                        target = newT;
                        target.TargetMe.Add(this);
                        //target.TargetMe++;
                        targetInLOS = false;
                    }
                }
            }
            else
            {
                if(target!=null)
                    target.TargetMe.Remove(this);
                target = null;
            }
                
        }

        protected void CheckedTargetMe()
        {
            for (int i = 0; i < TargetMe.Count; i++)
            {
                if (TargetMe[i].dead)
                {
                    TargetMe.RemoveAt(i);
                    i--;
                }
            }
        }



        protected bool turretOnCooldown = false;
        public IEnumerator TurretRoutine()
        {

            if(this is UnitTower && (this as UnitTower).IsSampleTower)
            {
                yield break;
            }

            for (int i = 0; i < shootPoints.Count; i++)
            {
                if (shootPoints[i] == null) { shootPoints.RemoveAt(i); i -= 1; }
            }

            if (shootPoints.Count == 0)
            {
                Debug.LogWarning("ShootPoint not assigned for unit - " + unitName + ", auto assigned", this);
                shootPoints.Add(thisT);
            }

            for (int i = 0; i < stats.Count; i++)
            {
                if (stats[i].shootObjectT != null) ObjectPoolManager.New(stats[i].shootObjectT.gameObject, 3);
            }

            yield return null;
            float cd = 0;
            float dist;

            while (true)
            {
                if (dead)
                {
                    yield return null;
                    continue;
                }

                if (target == null || stunned || IsInConstruction() || !targetInLOS || GameControl.GetGameState() != _GameState.Play)
                {
                    if (ListTarget.Count > 0)
                    {
                        //MyLog.Log(name + " stunned: " + stunned + " IsInConstruction:" + IsInConstruction()+ " !targetInLOS:" + !targetInLOS );
                        ScanForTarget();
                    }
                    yield return null;
                }
                else
                {
                    dist = getDistanceFromTarget();
                    //Debug.Log("Atak:" + dist + "  " + GetRange());
                    if (target.dead || dist > GetRange())
                    {

                        //MyLog.Log(name + " continue dist: " + dist+">"+GetRange());
                        ScanForTarget();
                        yield return null;
                        continue;
                    }

                    turretOnCooldown = true;
                    float animationDelay = 0;
                    for (int g = 0; g < stats[0].CountAttakcs; g++)
                    {
                        Unit currentTarget = target;

                        if (animationUnit != null)
                        {
                            WaitShootEvent = true;
                            animationDelay = animationUnit.PlayShoot();

                            while (WaitShootEvent && dead == false)
                            {
                                yield return null;
                            }

                            if (dead)
                                continue;
                        }

                        AttackInstance attInstance = new AttackInstance();
                        attInstance.srcUnit = this;
                        attInstance.tgtUnit = currentTarget;

                        Transform sp = shootPoints[Mathf.Clamp(g, 0, shootPoints.Count)];
                        Transform objT = ObjectPoolManager.Spawn(GetShootObjectT(), sp.position, sp.rotation);  // (Transform)Instantiate(GetShootObjectT(), sp.position, sp.rotation);
                        ShootObject shootObj = objT.GetComponent<ShootObject>();
                        shootObj.Shoot(attInstance, sp);
                        if (onShotTurretE != null)
                            onShotTurretE(currentTarget);
                        if (delayBetweenShootPoint > 0) yield return new WaitForSeconds(delayBetweenShootPoint);

                        if (animationDelay > 0)
                        {
                            yield return new WaitForSeconds(animationDelay * (1 - PercentAttackAnimation) - shootPoints.Count * delayBetweenShootPoint);
                        }
                    }

                    cd = (GetCooldown() - animationDelay - shootPoints.Count * delayBetweenShootPoint);
                    yield return new WaitForSeconds(cd);

                    turretOnCooldown = false;

                    ScanForTarget();
                }
            }
        }
        [HideInInspector]
        public bool WaitShootEvent;
        //Вызывается аниматором при анимации атаки (когда нужно/можно выпустить снаряд)
        public void AttackAnimEvent()
        {
            WaitShootEvent = false;
        }

        //Вызывается аниматором в конце аниции смерти
        public virtual void DeadEndAnimEvent()
        {
            if(animationUnit != null)
                animationUnit.EndDead();
            DestroyedInvoke();
            ObjectPoolManager.Unspawn(thisObj);
        }
        #region AttackOutEvent
        public void CallEventOutAttakInit(AttackInstance attackInstance)
        {
            if (myOutAttakInstanceInitE != null)
                myOutAttakInstanceInitE(attackInstance);
        }
        public void CallEventOutAttakFirst(AttackInstance attackInstance)
        {
            if (myOutAttakInstanceFirstE != null)
                myOutAttakInstanceFirstE(attackInstance);
        }
        public void CallEventOutAttakLast(AttackInstance attackInstance)
        {
            if (myOutAttakInstanceLastE != null)
                myOutAttakInstanceLastE(attackInstance);
        }
        #endregion
        #region AttackInEvent
        public void CallEventInAttakInit(AttackInstance attackInstance)
        {
            if (myInAttakInstanceInitE != null)
                myInAttakInstanceInitE(attackInstance);
        }
        public void CallEventInAttakFirst(AttackInstance attackInstance)
        {
            if (myInAttakInstanceFirstE != null)
                myInAttakInstanceFirstE(attackInstance);
        }
        public void CallEventInAttakLast(AttackInstance attackInstance)
        {
            if (myInAttakInstanceLastE != null)
                myInAttakInstanceLastE(attackInstance);
        }
        #endregion

        public void ApplyEffect(AttackInstance attInstance)
        {
            if (dead) return;

            if (attInstance.missed || attInstance.isHealing) return;            

            if (myApplyAttackInstance != null)
                myApplyAttackInstance(attInstance);

            if (ApplyAttackE != null)
                ApplyAttackE(this, attInstance);

            if (attInstance.srcUnit!=null && 
                listUnitDamageMy.Contains(attInstance.srcUnit) == false)
                listUnitDamageMy.Add(attInstance.srcUnit);

            if(attInstance.srcUnit !=null && attInstance.srcUnit is UnitHero)
                (attInstance.srcUnit as UnitHero).addXP(attInstance.damage*attInstance.dmgModifier);

           /* attInstance.damageHP = (attInstance.pierceShield || shield<=0 ? 
                attInstance.damage : CalcDmgInShield(attInstance.srcUnit.IgnoreTypeArmor(), attInstance.damage, attInstance.getDamageType))
                * attInstance.dmgModifier;*/
            //shield -= attInstance.damageShield;

           // HP -= attInstance.damageHP;
            

            /*if (onDamagedE != null)
                onDamagedE(this, attInstance.damage);*/

            currentHPStagger = GetHPStaggerDuration();
            //currentShieldStagger = GetShieldStaggerDuration();

            /*if (attInstance.destroy || HP <= 0)
            {
                KillMyUnit = attInstance.srcUnit;
                Dead();
                return;
            }*/

            ApplyDamage(attInstance.srcUnit, attInstance.damage, attInstance.getDamageType, attInstance.getIgnoreArmor);

            if (dead)
                return;

            foreach (BaseEffect effect in attInstance.listEffects)
            {
                if (attInstance.srcUnit is Unit)
                    EffectsManager.AddEffect(attInstance.srcUnit as Unit, effect);
                else
                {
                    Debug.LogWarningFormat("Unit {0}, add effect from not unit!",name);
                    EffectsManager.AddEffect(null, effect);
                }
            }

        }
        public void ShowStunEffetc()
        { 
            if(StunEffectPrefab==null)
            {
                MyLog.LogWarning(name + " unit, StunEffectPrefab is null");
                return;
            }

            if (_stunEffect == null)
            {
                _stunEffect = ObjectPoolManager.Spawn(StunEffectPrefab);
                _stunEffect.transform.SetParent(thisT);
                _stunEffect.transform.localPosition = Vector3.zero;
            }
            else
            {
                ObjectPoolManager.Unspawn(_stunEffect);
                _stunEffect = ObjectPoolManager.Spawn(StunEffectPrefab);
                _stunEffect.transform.SetParent(thisT);
                _stunEffect.transform.localPosition = Vector3.zero;
            }
        }
        public void HideStunEffect()
        {
            if (_stunEffect != null)
            {
                ObjectPoolManager.Unspawn(_stunEffect);
                _stunEffect = null;
            }

        }

        public void ShowSlowEffetc()
        {
            for (int i = 0; i < FreezingEffectPS.Length; i++)
            {
                FreezingEffectPS[i].Play();
            }
        }

        public void HideSlowEffect()
        {
            if (MoveMultiplier!= null && MoveMultiplier.ContainsKey((int)IDEffect.SlowControl)==false)
            {
                for (int i = 0; i < FreezingEffectPS.Length; i++)
                {
                    FreezingEffectPS[i].Stop();
                }
            }
        }

        public void ActionFireDotEffect(bool action)
        {
            //MyLog.Log("ActionFireDotEffect "+name+" "+action);
            for (int i = 0; i < FireEffectPS.Length; i++)
            {
                if(action)
                    FireEffectPS[i].Play();
                else
                    FireEffectPS[i].Stop();
            }
        }

        public void ActionPoisenDotEffect(bool action)
        {
            for (int i = 0; i < PoisenEffectPS.Length; i++)
            {
                if (action)
                    PoisenEffectPS[i].Play();
                else
                    PoisenEffectPS[i].Stop();
            }
        }


        public float GetMoveSpeed()
        {
            float move = stats[currentActiveStat].moveSpeed;
            if (MoveMultiplier != null)
            {
                foreach (var k in MoveMultiplier)
                {
                    move *= k.Value;
                }
            }
            return move;
        }
        public void UpdateMoveSpeedAgent()
        {
            if (_Agent != null)
            {
                _Agent.speed = GetMoveSpeed();
            }
        }

        private float dmgBuff;

        //for ability and what not
        public void ApplyDamage(IUnitAttacking unitAttacking, float dmg, int typeDmg, bool ignoreArmor)
        {
            if (dmg < 0)
            {
                Debug.LogErrorFormat("Dmg less 0!, src:{0} , tgt:{1}", unitAttacking!=null? unitAttacking.GetName():"null", GetName());
                return;
            }

            if (immuneToAllAbility && unitAttacking is Ability)
                dmg = dmg * damageAbilityIncrementImmune;

           // bool ignorArm = unitAttacking != null ? unitAttacking.IgnoreTypeArmor() : false;

            dmgBuff = CalcDmgInShield(ignoreArmor, dmg, typeDmg);
            if (dmgBuff > 0)
            {

                if (ignoreArmor == false)
                    dmgBuff *= DamageTable.GetModifier(armorType, typeDmg);
                ApplyDamageHP(unitAttacking, dmgBuff);
            }
            else if(dmg>0)
            {
                if (onDamagedE != null)
                    onDamagedE(this, 0);
                if (onMyDamagedE != null)
                    onMyDamagedE(0);
            }

#if UNITY_EDITOR
            //Debug.Log(dmg +" => "+dmgBuff + " *  "+ DamageTable.GetModifier(armorType, typeDmg));
            if (dmg > 0 && GameControl.GET_EDITOR_ShowDamage)
                new TextOverlay(thisT.position, dmgBuff.ToString(), new Color(1f, 1f, 1f, 1f), false);
            #endif

        }
        public void RestoreHP(float value)
        {
            //  new TextOverlay(thisT.position, value.ToString("f0"), new Color(0f, 1f, .4f, 1f),false);
            HP = Mathf.Clamp(HP + value, 0, fullHP);
        }


        //called when unit take damage
        public void ApplyDamageHP(IUnitAttacking unitAttacking, float dmg)
        {
            float realDmg = Mathf.Clamp(dmg, 0, HP);
            HP -= dmg;
            // new TextOverlay(thisT.position, dmg.ToString("f0"), new Color(1f, 1f, 1f, 1f),false);
            if (onDamagedE != null)
                onDamagedE(this, realDmg);
            if (onMyDamagedE != null)
                onMyDamagedE(realDmg);
            if (unitAttacking is Unit)
            {
                (unitAttacking as Unit).allDmgCurrentGame += realDmg;
            }
            currentHPStagger = HPStaggerDuration;

            if (HP <= 0)
            {
                KillMyUnit = unitAttacking;
                Dead();
            }
        }

        #region slowAOE
        /*private Coroutine coroutineAOESlow;
        public void StartAOESlow()
        {
            coroutineAOESlow = StartCoroutine(AOESlowRoutine());
        }

        public void StopAOESlow()
        {
            if (coroutineAOESlow != null)
                StopCoroutine(coroutineAOESlow);
        }

        IEnumerator AOESlowRoutine()
        {
            if (targetMode == _TargetMode.Hybrid)
            {
                LayerMask mask1 = 1 << LayerManager.LayerCreep();
                LayerMask mask2 = 1 << LayerManager.LayerCreepF();
                maskTarget = mask1 | mask2;
            }
            else if (targetMode == _TargetMode.Air)
            {
                maskTarget = 1 << LayerManager.LayerCreepF();
            }
            else if (targetMode == _TargetMode.Ground)
            {
                maskTarget = 1 << LayerManager.LayerCreep();
            }

            while (true)
            {
                yield return new WaitForSeconds(stats[0].slowAura.duration);

                while (stunned || IsInConstruction())
                    yield return null;

                if (stats[0].slowAura.IsValid() == false)
                    break;

                Collider[] cols = Physics.OverlapSphere(thisT.position, stats[0].slowAura.auraRange, maskTarget);
                if (cols.Length > 0)
                {
                    for (int i = 0; i < cols.Length; i++)
                    {
                        Unit unit = cols[i].transform.GetComponent<Unit>();

                        if (unit == null && !unit.dead && Vector3.Distance(unit.thisT.position,thisT.position)> stats[0].slowAura.auraRange) continue;

                        unit.EffectsManager.AddEffect(this, new SlowControlEffect(stats[0].slowAura.slowMultiplier,
                            stats[0].slowAura.duration));
                        //ApplySlow(stats[0].slowAura);
                    }
                }

            }
        }*/
        #endregion
        #region ContusionAOE
        /*private Coroutine coroutineContusionAOE;
        public void StartContusionAOE()
        {
            coroutineContusionAOE = StartCoroutine(AOEContusionRoutine());
        }

        public void StopContusionAOE()
        {
            if (coroutineContusionAOE != null)
                StopCoroutine(coroutineContusionAOE);
        }

        IEnumerator AOEContusionRoutine()
        {
            if (targetMode == _TargetMode.Hybrid)
            {
                LayerMask mask1 = 1 << LayerManager.LayerCreep();
                LayerMask mask2 = 1 << LayerManager.LayerCreepF();
                maskTarget = mask1 | mask2;
            }
            else if (targetMode == _TargetMode.Air)
            {
                maskTarget = 1 << LayerManager.LayerCreepF();
            }
            else if (targetMode == _TargetMode.Ground)
            {
                maskTarget = 1 << LayerManager.LayerCreep();
            }
            
            while (true)
            {
                yield return new WaitForSeconds(stats[0].contusionAura.duration);

                while (stunned || IsInConstruction())
                    yield return null;

                if (stats[0].contusionAura.IsValid() == false)
                    break;

                Collider[] cols = Physics.OverlapSphere(thisT.position, stats[0].contusionAura.auraRange, maskTarget);
                if (cols.Length > 0)
                {
                    for (int i = 0; i < cols.Length; i++)
                    {
                        Unit unit = cols[i].transform.GetComponent<Unit>();
                        if (unit == null && !unit.dead && Vector3.Distance(unit.thisT.position, thisT.position) > stats[0].contusionAura.auraRange) continue;

                        unit.ApplyContusion(stats[0].contusionAura);
                    }
                }

            }
        }*/
        #endregion

        public virtual void Dead(bool getRsc=true)
        {
            if (dead)
            {
                MyLog.LogWarning("Unit "+name+" dead is now dead!");
                return;
            }
            dead = true;

            StartDestroyedInvoke();

            float delay = 0;

           // if (IsCreep)
           //     delay = GetUnitCreep.CreepDestroyed(getRsc);

            if (animationUnit != null)
                delay = animationUnit.PlayDead();

            if (DeadAudio != null && !IsHero)
            {
                AudioControll.Play(DeadAudio);
            }


            EffectsManager.addMyEffectE -= EffectsManager_addMyEffectE;
            EffectsManager.removeMyEffectE -= EffectsManager_removeMyEffectE;

            // if (supportRoutineRunning) UnbuffAll();

            //DestroyedInvoke();

            HideSlowEffect();
            HideStunEffect();

            if(target!=null && target.dead==false && IsHero)
                target.TargetMe.Remove(this);

            listUnitDamageMy.Clear();
            EffectsManager.ClearData();

            if(delay>0)
                StartCoroutine(_Dead(delay));
            ClearProperties();

            isInit = false;
        }


        protected void StartDestroyedInvoke()
        {
            if (onStartDestroyedE != null)
                onStartDestroyedE(this);
            if (onMyStartDestroyedE != null)
                onMyStartDestroyedE(this);
        }


        public void DestroyedInvoke()
        {
            if (onDestroyedE != null)
                onDestroyedE(this);
            if (onMyDestroyedE != null)
                onMyDestroyedE(this);
        }

        public virtual IEnumerator _Dead(float delay)
        {
                if (IsCreep)
                {
                    RaycastHit hit;
                    float y = GetUnitCreep.flyingHeightOffset;
                    LayerMask m = 1 << LayerManager.LayerTerrain();
                    if (y > 0 && Physics.Raycast(thisT.position, -Vector3.up, out hit, 100, m))
                    {
                        for (int i = 0; i < 20; i++)
                        {
                            thisT.position = new Vector3(thisT.position.x, thisT.position.y - y / 20f, thisT.position.z);
                            yield return new WaitForSeconds((delay / 2f) / 20f);//половину времени опукаем
                        }
                        yield return new WaitForSeconds(delay / 2f);//половину ждем
                    }

                    else
                    {
                        //MyLog.Log("Y:" + y + " not hit!");
                        yield return new WaitForSeconds(delay);
                    }
                }
                else
                    yield return new WaitForSeconds(delay);
            
                //ObjectPoolManager.Unspawn(thisObj);
        }



        public Transform GetTargetT()
        {
            return targetPoint != null ? targetPoint : thisT;
        }


        //Тут определяются финальные значения характеристик юнита
        private float GetFullShield() { return defaultShield ; }
        protected float GetHPRegenRate() { return HPRegenRate ; }
        private float GetHPStaggerDuration() { return HPStaggerDuration ; }

        public float GetDamageMin() { return Mathf.Max(0, stats[currentActiveStat].damageMin /** (1 + damageBuffMul + dmgABMul + GetPerkMulDamage())*/); }
        public float GetDamageMax() { return Mathf.Max(0, stats[currentActiveStat].damageMax /** (1 + damageBuffMul + dmgABMul + GetPerkMulDamage())*/); }
        public float GetCooldown() { return Mathf.Max(0.05f, stats[currentActiveStat].cooldown /** (1 - cooldownBuffMul - cdABMul - GetPerkMulCooldown())*/ * ContusionMultiplier); }

        public float GetRangeMin() { return stats[currentActiveStat].minRange; }
        public virtual float GetRange() { return Mathf.Max(0, stats[currentActiveStat].range /** (1 + rangeBuffMul + rangeABMul + GetPerkMulRange())*/+SizeMyCollider); }
        public float GetAOERadius() { return stats[currentActiveStat].aoeRadius /** (1 + GetPerkMulAOERadius())*/; }

        public float GetHit() { return stats[currentActiveStat].hit /*+ hitBuffMod + GetPerkModHit()*/; }
        public float GetDodge() { return stats.Count == 0 ? 0 : stats[currentActiveStat].dodge /*+ dodgeBuffMod + GetPerkModDodge()*/; }


        public float GetShieldBreak() { return stats[currentActiveStat].shieldBreak /*+ GetPerkModShieldBreak()*/; }
        public float GetShieldPierce() { return stats[currentActiveStat].shieldPierce /*+ GetPerkModShieldPierce()*/; }
        public bool DamageShieldOnly() { return stats[currentActiveStat].damageShieldOnly; }



        public int GetShootPointCount() { return shootPoints.Count; }

        public Transform GetShootObjectT()
        {
            if (stats[currentActiveStat].shootObjectT == null) return localShootObjectT;
            return stats[currentActiveStat].shootObjectT;
        }

        public List<float> GetResourceGain() { return stats[currentActiveStat].rscGain; }

                      

        public virtual float GetDistFromDestination() { return 0; }

        public virtual bool IsInConstruction() { return false; }



        public static float DistanceUnitForUnit(Unit from, Unit to) //Растояние между 2 мобами с учетом их размеров
        {
            return Vector3.Distance(from.getPosition, to.getPosition) - from.SizeMyCollider - to.SizeMyCollider;
        }

        public float DistanceForUnit(Unit to)
        {
            return Unit.DistanceUnitForUnit(this, to);
        }

        void OnDrawGizmos()
        {
            if (target != null)
            {
                Gizmos.color = IsCreep ? Color.red : Color.green;
                Gizmos.DrawLine(transform.position, target.transform.position);
            }
        }


        protected virtual void EffectsManager_removeMyEffectE(Unit unit, IDEffect typeEffect)
        {
        }

        protected virtual void EffectsManager_addMyEffectE(BaseEffect effect)
        {
        }

    }

}
