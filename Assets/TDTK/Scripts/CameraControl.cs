using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using TDTK;
using UnityEngine.UI;
namespace TDTK
{

    public class CameraControl : MonoBehaviour
    {

        private float initialMousePosX;
        private float initialMousePosY;

        private float initialRotX;
        private float initialRotY;


        //private Vector3 lastTouchPos = new Vector3(9999, 9999, 9999);
        //private Vector3 moveDir = Vector3.zero;

        private float touchZoomSpeed;




        [HideInInspector]
        public Transform camT;

        public float panSpeed = 5;
        public float zoomSpeed = 0.5f; //5


        public bool enableMouseZoom = true;
        public bool enableMouseRotate = true;
        public bool enableMousePanning = false;
        public bool enableKeyPanning = true;

        public int mousePanningZoneWidth = 10;

        //for mobile/touch input 
        public bool enableTouchPan = true;
        public bool enableTouchZoom = true;
        public bool enableTouchRotate = false;
        public float rotationSpeed = 1;


        public float minPosX = -10;
        public float maxPosX = 10;

        public float minPosZ = -10;
        public float maxPosZ = 10;

        public float minZoomDistance = 8;
        public float maxZoomDistance = 30;

        public float minRotateAngle = 10;
        public float maxRotateAngle = 89;



        //calculated deltaTime based on timeScale so camera movement speed always remain constant
        private float deltaT;

        private float currentZoom = 0;

        private Transform thisT;
        public static CameraControl instance;



        public static void Disable() { if (instance != null) instance.enabled = false; }
        public static void Enable() { if (instance != null) instance.enabled = true; }

        void Awake()
        {
            thisT = transform;
            instance = this;
            camT = Camera.main.transform;
        }


        Vector3 _orignPosition;
        float _orignAspectRatio;
        float _aspectRatio;
        float _orignHeight;
        float _orignWidth;
        void Start()
        {
            _orignPosition = transform.position;
#if UNITY_ANDROID || UNITY_IOS
            InitializeMobileCameraZoom();
#endif
            currentZoom = Camera.main.orthographicSize;
        }


        void InitializeMobileCameraZoom()
        {
            _orignAspectRatio = 800f / 610f;
            _aspectRatio = Screen.width / (float)Screen.height;
            if (_aspectRatio > _orignAspectRatio)
            {
                //� ������ ������������ ������ ��� �����, ������ ������ = �������� ������
                _orignHeight = Camera.main.orthographicSize * 2;
                _orignWidth = _orignAspectRatio * _orignHeight;

                float newHeight = _orignWidth / _aspectRatio;
                Camera.main.orthographicSize = newHeight / 2;
            }
            else
            {
                //� ������ ������������ ������ ��� �����, ������ ������ = �������� ������, ���� ��� � ��� ��� �� ����� �� ����
                //��� ������ �������� �������� ����� ������
            }
            //��������� ����������� 10%
            minZoomDistance = Camera.main.orthographicSize * 0.8f;
            maxZoomDistance = Camera.main.orthographicSize;

        }
        float GetCurrnetHeight()
        {
            return Camera.main.orthographicSize * 2;
        }
        float GetCurrentWidth()
        {
            return Camera.main.orthographicSize * 2 * _aspectRatio;
        }
        void UpdateMapBorders()
        {
            float deltaX = _orignWidth - GetCurrentWidth();
            float deltaZ = _orignHeight - GetCurrnetHeight();

            if (deltaX > 0)
            {
                maxPosX = _orignPosition.x + deltaX / 2f;
                minPosX = _orignPosition.x - deltaX / 2f;
            }
            else
            {
                maxPosX = _orignPosition.x;
                minPosX = _orignPosition.x;
            }

            if (deltaZ > 0)
            {
                maxPosZ = _orignPosition.z + deltaZ / 2f;
                minPosZ = _orignPosition.z - deltaZ / 2f;
            }
            else
            {
                maxPosZ = _orignPosition.z;
                minPosZ = _orignPosition.z;
            }


        }
        //��� ������ ��� 
        private void UpdateTouchZoom()
        {
            if (Input.touchCount == 2)
            {
                Touch touch1 = Input.touches[0];
                Touch touch2 = Input.touches[1];
                if (touch1.phase == TouchPhase.Moved && touch1.phase == TouchPhase.Moved)
                {
                    Vector3 dirDelta = (touch1.position - touch1.deltaPosition) - (touch2.position - touch2.deltaPosition);
                    Vector3 dir = touch1.position - touch2.position;
                    float dot = Vector3.Dot(dirDelta.normalized, dir.normalized);

                    if (Mathf.Abs(dot) > 0.1f) // 0.7f
                    {
                        touchZoomSpeed = -dir.magnitude + dirDelta.magnitude; // ������� �������
                    }
                }

            }

            if (touchZoomSpeed < 0)
            {
                if (currentZoom > minZoomDistance)
                {
                    currentZoom += Time.deltaTime * zoomSpeed * touchZoomSpeed;
                }
            }
            else if (touchZoomSpeed > 0)
            {
                if (currentZoom < maxZoomDistance)
                {
                    currentZoom += Time.deltaTime * zoomSpeed * touchZoomSpeed;
                }
            }
            currentZoom = Mathf.Clamp(currentZoom, minZoomDistance, maxZoomDistance);
            Camera.main.orthographicSize = currentZoom;
            touchZoomSpeed = touchZoomSpeed * (1 - Time.deltaTime * 5);
        }

        //��� ������ ����������� 
        bool TapStartedFromGui = false;
        void UpdatePanMY()
        {
            Touch[] touches = Input.touches;
            if (touches.Length == 1)
            {
                if (touches[0].phase == TouchPhase.Began)
                {
                    if (EventSystem.current.IsPointerOverGameObject(0))
                    {
                        TapStartedFromGui = true;
                        return;
                    }
                }


                if (touches[0].phase == TouchPhase.Moved && TapStartedFromGui == false)
                {
                    Vector2 delta = touches[0].deltaPosition;
                    //���� �� 100 ����� �� ��������� � � world ���������� � �������� �� ������� ������(������) ������������ � ������� � �������� �� ������ ������ � ��������, 2.2f - ��� ���������
                    float posX = delta.x * (2.2f * GetCurrentWidth() * 100 / Screen.width) / 100f;
                    float posZ = delta.y * (2.2f * GetCurrnetHeight() * 100 / Screen.height) / 100f;

                    transform.position += new Vector3(-posX, 0, -posZ);

                    ////�������� ������ ��� ������������ ��������
                    //scrollDirection = new Vector3(touches[0].deltaPosition.normalized.x, 0, touches[0].deltaPosition.normalized.y);
                    //scrollVelocity = touches[0].deltaPosition.magnitude / touches[0].deltaTime;


                    //if (scrollVelocity <= 100)
                    //    scrollVelocity = 0;
                }

                //if (touches[0].phase == TouchPhase.Ended)
                //{
                //    timeTouchPhaseEnded = Time.time;
                //}
            }
            if (touches.Length == 0)
            {
                TapStartedFromGui = false;
                ////��������� �� �������
                //if (scrollVelocity != 0.0f)
                //{
                //    //slow down over time
                //    float t = (Time.time - timeTouchPhaseEnded) / inertiaDuration;
                //    float frameVelocity = Mathf.Lerp(scrollVelocity, 0.0f, t);
                //    transform.position += -scrollDirection.normalized * (frameVelocity / 100f) * Time.deltaTime;

                //    if (t >= 1.0f)
                //        scrollVelocity = 0.0f;
                //}
            }

        }

        private float scrollVelocity = 0.0f;
        private float timeTouchPhaseEnded;
        private Vector3 scrollDirection = Vector3.zero;
        public float inertiaDuration = 1.0f;
        public float moveSensitivityX = 1.0f;
        public float moveSensitivityY = 1.0f;
        public bool updateZoomSensitivity = true;
        public bool invertMoveX = false;
        public bool invertMoveY = false;
        void UpdatePanNew()
        {
            if (updateZoomSensitivity)
            {
                moveSensitivityX = Camera.main.orthographicSize / 5.0f;
                moveSensitivityY = Camera.main.orthographicSize / 5.0f;
            }

            Touch[] touches = Input.touches;
            if (touches.Length < 1)
            {
                //if the camera is currently scrolling
                if (scrollVelocity != 0.0f)
                {
                    //slow down over time
                    float t = (Time.time - timeTouchPhaseEnded) / inertiaDuration;
                    float frameVelocity = Mathf.Lerp(scrollVelocity, 0.0f, t);
                    thisT.position += -scrollDirection.normalized * (frameVelocity * 0.05f) * Time.deltaTime;

                    if (t >= 1.0f)
                        scrollVelocity = 0.0f;
                }
            }
            if (touches.Length == 1)
            {
                if (touches[0].phase == TouchPhase.Began)
                {
                    scrollVelocity = 0.0f;
                }
                else if (touches[0].phase == TouchPhase.Moved)
                {
                    Vector2 delta = touches[0].deltaPosition;

                    float positionX = delta.x * moveSensitivityX * Time.deltaTime;
                    positionX = invertMoveX ? positionX : positionX * -1;

                    float positionY = delta.y * moveSensitivityY * Time.deltaTime;
                    positionY = invertMoveY ? positionY : positionY * -1;

                    thisT.position += new Vector3(positionX, 0, positionY);

                    scrollDirection = new Vector3(touches[0].deltaPosition.normalized.x, 0, touches[0].deltaPosition.normalized.y);
                    scrollVelocity = touches[0].deltaPosition.magnitude / touches[0].deltaTime;


                    if (scrollVelocity <= 100)
                        scrollVelocity = 0;
                }
                else if (touches[0].phase == TouchPhase.Ended)
                {
                    timeTouchPhaseEnded = Time.time;
                }
            }


        }
        public float orthoZoomSpeed = 0.05f;
        void UpdateZoomNew()
        {
            Touch[] touches = Input.touches;
            if (touches.Length == 2)
            {
               // Vector2 cameraViewsize = new Vector2(Screen.width, Screen.height);

                Touch touchOne = touches[0];
                Touch touchTwo = touches[1];

                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
                Vector2 touchTwoPrevPos = touchTwo.position - touchTwo.deltaPosition;

                float prevTouchDeltaMag = (touchOnePrevPos - touchTwoPrevPos).magnitude;
                float touchDeltaMag = (touchOne.position - touchTwo.position).magnitude;

                float deltaMagDiff = prevTouchDeltaMag - touchDeltaMag;

                //  thisT.position += thisT.TransformDirection((touchOnePrevPos + touchTwoPrevPos - cameraViewsize) * Camera.main.orthographicSize / cameraViewsize.y);

                Camera.main.orthographicSize += deltaMagDiff * orthoZoomSpeed;
                Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, minZoomDistance, maxZoomDistance);
                //currentZoom = Camera.main.orthographicSize; 
                //  thisT.position -= thisT.TransformDirection((touchOne.position + touchTwo.position - cameraViewsize) * Camera.main.orthographicSize / cameraViewsize.y);


            }
        }

        // Update is called once per frame
        void Update()
        {
            if (Time.timeScale == 1) deltaT = Time.deltaTime;
            else if (Time.timeScale > 1) deltaT = Time.deltaTime / Time.timeScale;
            else deltaT = 0.015f;
#if UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY
            if (enableTouchPan)
            {
                UpdatePanMY();
                //   UpdatePanNew();
            }
            if (enableTouchZoom)
            {
                UpdateTouchZoom();
                //  UpdateZoomNew();
            }
#endif
#if  UNITY_WEBGL || UNITY_WEBPLAYER // || UNITY_EDITOR

            //mouse and keyboard
            if (enableMouseRotate)
            {
                if (Input.GetMouseButtonDown(1))
                {
                    initialMousePosX = Input.mousePosition.x;
                    initialMousePosY = Input.mousePosition.y;
                    initialRotX = thisT.eulerAngles.y;
                    initialRotY = thisT.eulerAngles.x;
                }

                if (Input.GetMouseButton(1))
                {
                    float deltaX = Input.mousePosition.x - initialMousePosX;
                    float deltaRotX = (.1f * (initialRotX / Screen.width));
                    float rotX = deltaX + deltaRotX;

                    float deltaY = initialMousePosY - Input.mousePosition.y;
                    float deltaRotY = -(.1f * (initialRotY / Screen.height));
                    float rotY = deltaY + deltaRotY;
                    float y = rotY + initialRotY;

                    //limit the rotation
                    if (y > maxRotateAngle)
                    {
                        initialRotY -= (rotY + initialRotY) - maxRotateAngle;
                        y = maxRotateAngle;
                    }
                    else if (y < minRotateAngle)
                    {
                        initialRotY += minRotateAngle - (rotY + initialRotY);
                        y = minRotateAngle;
                    }

                    thisT.rotation = Quaternion.Euler(y, rotX + initialRotX, 0);
                }
            }

            Quaternion direction = Quaternion.Euler(0, thisT.eulerAngles.y, 0);


            if (enableKeyPanning)
            {
                if (Input.GetButton("Horizontal"))
                {
                    Vector3 dir = transform.InverseTransformDirection(direction * Vector3.right);
                    thisT.Translate(dir * panSpeed * deltaT * Input.GetAxisRaw("Horizontal"));
                }

                if (Input.GetButton("Vertical"))
                {
                    Vector3 dir = transform.InverseTransformDirection(direction * Vector3.forward);
                    thisT.Translate(dir * panSpeed * deltaT * Input.GetAxisRaw("Vertical"));
                }
            }
            if (enableMousePanning)
            {
                Vector3 mousePos = Input.mousePosition;
                Vector3 dirHor = transform.InverseTransformDirection(direction * Vector3.right);
                if (mousePos.x <= 0) thisT.Translate(dirHor * panSpeed * deltaT * -3);
                else if (mousePos.x <= mousePanningZoneWidth) thisT.Translate(dirHor * panSpeed * deltaT * -1);
                else if (mousePos.x >= Screen.width) thisT.Translate(dirHor * panSpeed * deltaT * 3);
                else if (mousePos.x > Screen.width - mousePanningZoneWidth) thisT.Translate(dirHor * panSpeed * deltaT * 1);

                Vector3 dirVer = transform.InverseTransformDirection(direction * Vector3.forward);
                if (mousePos.y <= 0) thisT.Translate(dirVer * panSpeed * deltaT * -3);
                else if (mousePos.y <= mousePanningZoneWidth) thisT.Translate(dirVer * panSpeed * deltaT * -1);
                else if (mousePos.y >= Screen.height) thisT.Translate(dirVer * panSpeed * deltaT * 3);
                else if (mousePos.y > Screen.height - mousePanningZoneWidth) thisT.Translate(dirVer * panSpeed * deltaT * 1);
            }


            if (enableMouseZoom)
            {
                float zoomInput = Input.GetAxis("Mouse ScrollWheel");
                if (zoomInput != 0)
                {
                    currentZoom += zoomSpeed * zoomInput;
                    currentZoom = Mathf.Clamp(currentZoom, -maxZoomDistance, -minZoomDistance);
                }
            }
#endif
            UpdateMapBorders();
            float x = Mathf.Clamp(thisT.position.x, minPosX, maxPosX);
            float z = Mathf.Clamp(thisT.position.z, minPosZ, maxPosZ);

            thisT.position = new Vector3(x, thisT.position.y, z);
         }


        public bool avoidClipping = false;
        //private bool obstacle = false;
        public bool showGizmo = true;
        void OnDrawGizmos()
        {
            if (showGizmo)
            {
                Vector3 p1 = new Vector3(minPosX, transform.position.y, maxPosZ);
                Vector3 p2 = new Vector3(maxPosX, transform.position.y, maxPosZ);
                Vector3 p3 = new Vector3(maxPosX, transform.position.y, minPosZ);
                Vector3 p4 = new Vector3(minPosX, transform.position.y, minPosZ);

                Gizmos.color = Color.green;
                Gizmos.DrawLine(p1, p2);
                Gizmos.DrawLine(p2, p3);
                Gizmos.DrawLine(p3, p4);
                Gizmos.DrawLine(p4, p1);
            }
        }

    }

}