﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK {

	public class UIPauseMenu : MonoBehaviour {

		public GameObject panel;
		private static UIPauseMenu instance;
		
		public Slider sliderMusicVolume;
		public Slider sliderSFXVolume;
		
		public GameObject pauseMenuObj;
		public GameObject optionMenuObj;
		
		
		void Awake(){
			instance=this;
						
			sliderMusicVolume.value=AudioManager.GetMusicVolume()*100;
			sliderSFXVolume.value=AudioManager.GetSFXVolume()*100;
		}
		
		// Use this for initialization
		void Start () {
			OnOptionBackButton();
			Hide();
		}
		
		
		
		public void OnResumeButton(){
			Hide();
			GameControl.ResumeGame();
		}
		
		public void OnOptionButton(){
			pauseMenuObj.SetActive(false);
			optionMenuObj.SetActive(true);
		}
		
		public void OnMainMenuButton()
        {
            UIWaitPanel.Show();
            TimeScaleManager.RefreshTimeScale();
            PlayerManager.DM.getPlayerData();
            CardAndLevelSelectPanel.isReturnLevel = true;
            PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;
        }
		
		
		public static bool isOn=true;
		public static void Show(){ instance._Show(); }
		public void _Show(){
			isOn=true;
            panel.SetActive(isOn);
		}
		public static void Hide(){ instance._Hide(); }
		public void _Hide(){
			isOn=false;
            panel.SetActive(isOn);
		}
		
		
		public void OnMusicVolumeSlider(){
			if(Time.timeSinceLevelLoad>0.5f)
				AudioManager.SetMusicVolume(sliderMusicVolume.value/100);
		}
		public void OnSFXVolumeSlider(){
			if(Time.timeSinceLevelLoad>0.5f)
				AudioManager.SetSFXVolume(sliderSFXVolume.value/100);
		}
		
		public void OnOptionBackButton(){
			optionMenuObj.SetActive(false);
			pauseMenuObj.SetActive(true);
		}
        public void OnRestartButton()
        {
            UIWaitPanel.Show();
            TimeScaleManager.SetTimeScale(1f);
            PlayerManager.DM.getPlayerData();
            CardAndLevelSelectPanel.isRefreshData = true;
            PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;
        }

        private void PlayerManager_InitializationСompleted()
        {
            UIWaitPanel.Hide();
            PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;
            GameControl.LoadMainMenu();
        }
    }


}