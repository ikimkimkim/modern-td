﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

using TDTK;
using Translation;
namespace TDTK {

	public class UIHUD : MonoBehaviour {

		public Text txtLife;
		public Text txtWave;
		public Text txtScore;
		public GameObject scoreObj;
		
		public Text txtTimer;
		public UnityButton buttonSpawn;
        public UnityButton buttonSpawnNextWeve;



        public delegate void DelegateOnSpawnButton();
        public static event DelegateOnSpawnButton ClickOnSpawnButton;
		
		public List<UnityButton> rscObjList=new List<UnityButton>();

        public static GameObject WaitResponseObj { get { if (instance == null) return null; return instance.waitResponseObj; } }
        public GameObject waitResponseObj;

      //  public static GameObject LoadingObj { get { if (instance == null) return null; return instance.loadingObj; } }
      //  public GameObject loadingObj;

		private static UIHUD instance;

		
		// Use this for initialization
		void Start () {
			instance=this;
			
			buttonSpawn.Init();
            buttonSpawnNextWeve.Init();

            txtTimer.text="";
			
			scoreObj.SetActive(false);
			
			Sprite[] rscList=ResourceManager.GetResourceIconArray();
			for(int i=0; i<rscList.Length; i++)
            {
				if(i<2)
                    rscObjList[i].Init();
				else
                    rscObjList.Add(rscObjList[0].Clone("RscObj"+i, new Vector3(i*90, 0, 0)));
				rscObjList[i].imageIcon.sprite=rscList[i];
			}

            List<Card> abilit = PlayerManager.getAbilityCards;
            bool ActiveMana = false;
            for (int i = 0; i < abilit.Count; i++)
            {
                if(abilit[i].disableInBuildManager==false)
                {
                    ActiveMana = true;
                    break;
                }
            }
            rscObjList[1].rootObj.SetActive(ActiveMana);

            OnLife(0);
			OnNewWave(1);
			OnResourceChanged(new List<float>());


            buttonSpawnNextWeve.rootObj.SetActive(false);
            if (SpawnManager.AutoStart()){
				buttonSpawn.rootObj.SetActive(false);
				//StartCoroutine(AutoStartTimer());
				OnSpawnTimer(SpawnManager.GetAutoStartDelay());
			}
		}

        
        
		
		void OnEnable(){
			GameControl.onLifeE += OnLife;
			
			SpawnManager.onNewWaveE += OnNewWave;
			SpawnManager.onEndWaveSpawnE += OnEnableSpawn;
			SpawnManager.onSpawnTimerE += OnSpawnTimer;
			
			ResourceManager.onRscChangedE += OnResourceChanged;
            ResourceManager.onRscIncomeChangedE += onRscIncomeChanged;
        }
		void OnDisable(){
			GameControl.onLifeE -= OnLife;
			
			SpawnManager.onNewWaveE -= OnNewWave;
			SpawnManager.onEndWaveSpawnE -= OnEnableSpawn;
			SpawnManager.onSpawnTimerE -= OnSpawnTimer;
			
			ResourceManager.onRscChangedE -= OnResourceChanged;
            ResourceManager.onRscIncomeChangedE -= onRscIncomeChanged;
        }
		
		void OnLife(float changedvalue){
			int cap=GameControl.GetPlayerLifeCap();
			string text=(cap>0) ? "/"+ GameControl.GetPlayerLifeCap().ToString("#.#") : "" ;
			txtLife.text=GameControl.GetPlayerLife().ToString("#.#") + text;
		}
		
		void OnNewWave(int waveID){
            Translator trans = TranslationEngine.Instance.Trans;
           
			int totalWaveCount=SpawnManager.GetTotalWaveCount();
			string text=totalWaveCount>0 ? "/"+totalWaveCount : "";
			txtWave.text=trans["Game.Wave"]+" " + waveID+text;
			if(GameControl.IsGameStarted()) buttonSpawn.rootObj.SetActive(false);
		}

        void onRscIncomeChanged(List<float> valueChangedList)
        {
            //List<Rsc> rscList = ResourceManager.GetResourceArray();
            if (valueChangedList[0] == 0)//нету монет
            {
                UpdateInfo();
                return;
            }

            for (int i = 0; i < rscObjList.Count; i++)
            {
                if (rscObjList[i].animator != null)
                {
                    rscObjList[i].animator.SetTrigger("Show");
                    rscObjList[i].Income.text = "+"+ (Mathf.Round(valueChangedList[i]*10f)/10f).ToString();
                }
            }
            Invoke("UpdateInfo", IncomeRscAnimationText.AllTime /*0.8f*/);
        }

        void OnResourceChanged(List<float> valueChangedList)
        {
            UpdateInfo();
        }
        public static void SetActiveButtonSpawnNextWave(bool active)
        {
            if (instance != null && instance.buttonSpawnNextWeve.rootObj.activeSelf != active)
            {
                instance.buttonSpawnNextWeve.rootObj.SetActive(active);
            }
        }

        public void UpdateInfo()
        {
            float[] rscList = ResourceManager.GetResourceArray();
            for (int i = 0; i < rscObjList.Count; i++)
            {
                rscObjList[i].label.text = (System.Math.Floor(rscList[i] * 10) / 10f).ToString();
                if (ResourceManager.instance.rscMaxRateList.Count > i && ResourceManager.instance.rscMaxRateList[i] > 0)
                {
                    rscObjList[i].label.text += "/" + ResourceManager.instance.rscMaxRateList[i];
                }
            }
        }

        public void OnSpawnNextWave()
        {
            SpawnManager.SpawnNextWave();
            SetActiveButtonSpawnNextWave(false);
        }
		
		public void OnSpawnButton()
        {
            //if(FPSControl.IsOn()) return;

            //timerDuration=0;

            SpawnManager.StartSpawn();
            GameControl.StartGame();
			buttonSpawn.rootObj.SetActive(false);
            if (ClickOnSpawnButton != null)
                ClickOnSpawnButton();

        }
		
		void OnEnableSpawn(){
			//buttonSpawnNextWeve.rootObj.SetActive(true);
		}
		
		//private float timerDuration=0;
        void OnSpawnTimer(float duration)
        { 
            //timerDuration = duration;
        }
		/*void FixedUpdate(){
			if(timerDuration>0){
				if(timerDuration<60) txtTimer.text="Новая через "+(Mathf.Ceil(timerDuration)).ToString("f0")+"сек";
				else txtTimer.text="Новая через "+(Mathf.Floor(timerDuration/60)).ToString("f0")+"мин";
				timerDuration-=Time.fixedDeltaTime;
			}
			else if(txtTimer.text!="") txtTimer.text="";
		}*/
		
		
		
		public Toggle toggleFastForward;
		public void ToggleFF(){
			if(FPSControl.IsOn()) return;
			
			if(toggleFastForward.isOn)
                TimeScaleManager.SetTimeScale(UI.GetFFTime());
            else
                TimeScaleManager.SetTimeScale(0.5f); 
		}
		public static void NormalTime(){ if(instance!=null) instance.toggleFastForward.isOn=false; }

        float logWidth, logHeight, logWeightedAverage, scaleFactor;
        void Update()
        {
            if (buttonSpawn.rootObj.activeSelf == true)
            {
                logWidth = Mathf.Log(Screen.width / 990f, 2);
                logHeight = Mathf.Log(Screen.height / 560f, 2);
                logWeightedAverage = Mathf.Lerp(logWidth, logHeight, 0);
                scaleFactor = Mathf.Pow(2, logWeightedAverage);
                buttonSpawn.rootObj.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
            }
#if UNITY_ANDROID || UNITY_IOS
            if (Input.GetKeyDown(KeyCode.Escape)) 
            {
                OnPauseButton();
            }
#endif
        }
		public void OnPauseButton(){
			if(FPSControl.IsOn()) return;
			
			_GameState gameState=GameControl.GetGameState();
			if(gameState==_GameState.Over) return;
			
			if(toggleFastForward.isOn) toggleFastForward.isOn=false;
			
			if(gameState==_GameState.Pause){
				GameControl.ResumeGame();
				UIPauseMenu.Hide();
			}
			else{
				GameControl.PauseGame();
				UIPauseMenu.Show();
			}
		}

        public void OnBoostButton()
        {
            TimeScaleManager.BoostTime();
            UI.ClearSelectedTower();
            UIBuildButton.Hide();
        }
	}

}