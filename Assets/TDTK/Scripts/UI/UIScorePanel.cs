﻿using UnityEngine;
using System.Collections;
using TDTK;
using UnityEngine.UI;
using System;

public class UIScorePanel : MonoBehaviour {

    public static UIScorePanel instance { get; private set; }

    public static event Action OnChangeScore;

    private static FloatSecurity Score;
    public static float GetScore {
        get
        {
            return  Mathf.Ceil(Score.Value);
        }
    }

    [System.Serializable]
    public struct ScoreParam
    {
        public int EndScore;
        public float Multiplier;
    }

    public ScoreParam[] ScoreSetting;

    public GameObject Panel;
    public Text textScore;

    [SerializeField]
    private bool bConsiders = false;

    public float GetMultiplier { get
        {
            if (ScoreSetting.Length == 0)
                return 1;

            for (int i = 0; i < ScoreSetting.Length; i++)
            {
                if(ScoreSetting[i].EndScore >= GetScore)
                {
                    return ScoreSetting[i].Multiplier;
                }
            }
            return ScoreSetting[ScoreSetting.Length-1].Multiplier;
        }
    }

    void Start ()
    {
        MyLog.Log("UISP -> Link unit damag");
        instance = this;
        if(CardAndLevelSelectPanel.Survival)// SpawnManager.GetSpawnLimit == _SpawnLimit.Infinite)
        {
            bConsiders = true;
            Unit.onDamagedE += OnDamag;
            Score = new FloatSecurity();
            Score.Value = 0;
            AddScore(0);
        }
        Panel.SetActive(bConsiders);
	}


    public void OnDisable()
    {
        MyLog.Log("UISP -> Delink unit damag");
        if(bConsiders)
            Unit.onDamagedE -= OnDamag;
    }

    public void OnDamag(Unit unit,float damage)
    {
        if(damage<0)
        {
            Debug.LogError("UIScore, incom damage less 0! for unit " + unit.prefabID);
            return;
        }

        if(unit.IsCreep && GameControl.GetGameState() == _GameState.Play)
        {
            //Debug.Log("Score:M "+GetMultiplier);
            AddScore(damage / GetMultiplier);
        }
    }
	
    public void AddScore(float score)
    {
        if(float.IsNaN(score))
        {
            MyLog.LogError("Add score is NaN", MyLog.Type.build);
            return;
        }
        if(float.IsInfinity(score))
        {
            MyLog.LogError("Add score is Infinity", MyLog.Type.build);
            return;
        }

        if (score < 0)
        {
            Debug.LogError("UIScore, incom score less 0!");
            return;
        }

        Score.Value += score;
        UpdateText();
        if (OnChangeScore != null)
            OnChangeScore();
    }

    public void UpdateText()
    {
        if (textScore == null)
            return;
        textScore.text = Mathf.Ceil(Score.Value).ToString();
    }
    
}
