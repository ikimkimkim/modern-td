﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITimerSpawnManager : MonoBehaviour {

    private static UITimerSpawnManager _instance;
    [SerializeField] private GameObject _panel;
    [SerializeField] private Text _textTimer;

	void Start ()
    {
        _instance = this;
	}
	
    public static void Show(string timeStart)
    {
        if (_instance == null)
            return;
        _instance._Show(timeStart);
    }

    private void _Show(string timeStart)
    {
        _SetTime(timeStart);
        if (_panel.activeSelf == false)
            _panel.SetActive(true);
    }

    public static void Hide()
    {
        if (_instance == null)
            return;
        _instance._Hide();
    }

    private void _Hide()
    {
        if (_panel.activeSelf)
            _panel.SetActive(false);
    }

    public static void SetTime(string time)
    {
        if (_instance == null)
            return;
        _instance._SetTime(time);
    }

    private void _SetTime(string time)
    {
        _textTimer.text = time;// string.Format("До следующий волны: {0}", time);
    }
}
