﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK
{

    public class UI : MonoBehaviour
    {
        private bool oldStatusFullScreen;
        private Resolution oldResolutionScreen;
        public delegate void dFullScreen();
        public static dFullScreen onChangeFullScreen;

        public delegate void dSelectTower(UnitTower tower);
        public static dSelectTower onSelectTower;

        public delegate void dClearSelectTower();
        public static dClearSelectTower onClearSelectTower;

        public float scaleFactor = 1;
        public static float GetScaleFactor() { return instance.scaleFactor; }

        public enum _BuildMode { PointNBuild, DragNDrop };
        public _BuildMode buildMode = _BuildMode.PointNBuild;
        public static bool UseDragNDrop() { return instance.buildMode == _BuildMode.PointNBuild ? false : true; }

        private UnitTower selectedTower;

        public float fastForwardTimeScale = 4;
        public static float GetFFTime() { return instance.fastForwardTimeScale; }


        public bool disableTextOverlay = false;
        public static bool DisableTextOverlay() { return instance.disableTextOverlay; }


        public bool pauseGameInPerkMenu = true;
        public static bool PauseGameInPerkMenu() { return instance.pauseGameInPerkMenu; }


        [SerializeField] private bool _isLookBuild;

        private static UI instance;
        void Awake()
        {
            instance = this;
        }

        // Use this for initialization
        void Start()
        {
            // scaleFactor = Mathf.Min(Screen.width/ 800f, Screen.height/ 610f);
            //#if UNITY_ANDROID || UNITY_IOS
            oldStatusFullScreen = Screen.fullScreen;
            oldResolutionScreen = Screen.currentResolution;
            /*float logWidth = Mathf.Log(Screen.width / 990f, 2);
            float logHeight = Mathf.Log(Screen.height / 560f, 2);
            float logWeightedAverage = Mathf.Lerp(logWidth, logHeight, 0);*/
            //scaleFactor = Mathf.Pow(2, logWeightedAverage);
//#endif
        }

        void OnEnable()
        {
            GameControl.onGameOverE += OnGameOver;

            Unit.onDestroyedE += OnUnitDestroyed;

            AbilityManager.onTargetSelectModeE += OnAbilityTargetSelectMode;

            FPSControl.onFPSModeE += OnFPSMode;
            //FPSControl.onFPSCameraE += OnFPSCameraActive;

            //UnitTower.onUpgradedE += SelectTower;	//called when tower is upgraded, require for upgrade which the current towerObj is destroyed so select UI can be cleared properly 

            BuildManager.onAddNewTowerE += OnNewTower;	//add new tower via perk
            AbilityManager.onAddNewAbilityE += OnNewAbility;	//add new tower via perk
        }
        void OnDisable()
        {
            GameControl.onGameOverE -= OnGameOver;

            Unit.onDestroyedE -= OnUnitDestroyed;

            AbilityManager.onTargetSelectModeE -= OnAbilityTargetSelectMode;

            FPSControl.onFPSModeE -= OnFPSMode;
            //FPSControl.onFPSCameraE -= OnFPSCameraActive;

            //UnitTower.onUpgradedE -= SelectTower;

            BuildManager.onAddNewTowerE -= OnNewTower;
            AbilityManager.onAddNewAbilityE -= OnNewAbility;
        }

        void OnGameOver(int stars, ResponseData data)
        {
            //StartCoroutine(_OnGameOver(stars, data));
            UIBuildButton.Hide();

            //yield return new WaitForSeconds(1.0f);
            UIGameOverMenu.Show(stars, data);
        }
        IEnumerator _OnGameOver(int stars, ResponseData data)
        {
            UIBuildButton.Hide();

            yield return new WaitForSeconds(1.0f);
            UIGameOverMenu.Show(stars, data);
        }
        void OnUnitDestroyed(Unit unit)
        {
            if (!unit.IsTower)
                return;

            if (selectedTower == unit.GetUnitTower)
                ClearSelectedTower();
        }

        private bool abilityTargetSelecting = false;
        void OnAbilityTargetSelectMode(bool flag) { StartCoroutine(_OnAbilityTargetSelectMode(flag)); }
        IEnumerator _OnAbilityTargetSelectMode(bool flag)
        {
            yield return null;
            abilityTargetSelecting = flag;
        }


        // Update is called once per frame
        void Update()
        {
            if ((Screen.fullScreen != oldStatusFullScreen || Screen.currentResolution.width != oldResolutionScreen.width ||
                Screen.currentResolution.height != oldResolutionScreen.height) && onChangeFullScreen != null)
            {
                //MyLog.Log("UI: OnChengeScreen!"+Screen.fullScreen +" : "+ oldResolutionScreen, MyLog.Type.build);
                onChangeFullScreen();
                oldStatusFullScreen = Screen.fullScreen;
                oldResolutionScreen = Screen.currentResolution;
            }

            if (GameControl.GetGameState() == _GameState.Over) return;

            if (FPSControl.IsOn()) return;

            if (abilityTargetSelecting) return;

#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8
            if (Input.touchCount == 1)
            {
                Touch touch = Input.touches[0];

                if (UIUtilities.IsCursorOnUI(0)) return;

                if (!UseDragNDrop() && !UIBuildButton.isOn) BuildManager.SetIndicator(touch.position);

                if (touch.phase == TouchPhase.Began) OnTouchCursorDown(touch.position);
            }
            else UpdateMouse();
#else
				UpdateMouse();
#endif

           
        }

        Vector3 lastMaousePos;
        void UpdateMouse()
        {
            //if (Mathf.Abs(lastMaousePos.x - Input.mousePosition.x) < 0.1f && Mathf.Abs(lastMaousePos.z - Input.mousePosition.z) < 0.1f)
            //    return;
            //lastMaousePos = Input.mousePosition;

            if (UIUtilities.IsCursorOnUI()) return;

            if (!UseDragNDrop() && !UIBuildButton.isOn)
                BuildManager.SetIndicator(Input.mousePosition);

            if (Input.GetMouseButtonDown(0) && _isLookBuild == false)
                OnTouchCursorDown(Input.mousePosition);

        }
        public static void SetLookBuild(bool canBuild)
        {
            Debug.Log("Set look " + canBuild);
            instance._SetLookBuild(canBuild);
        }

        private void _SetLookBuild(bool canBuild)
        {
            _isLookBuild = canBuild == false;
            if (_isLookBuild)
            {
                ClearSelectedTower();
                UIBuildButton.Hide();
            }
        }

        void OnTouchCursorDown(Vector3 cursorPos)
        {
            UnitTower tower = GameControl.Select(cursorPos);

            TimeScaleManager.RefreshTimeScale();
            if (tower != null)
            {
                SelectTower(tower);
                UIBuildButton.Hide();
                TimeScaleManager.SetTimeScale(0.5f);
            }
            else
            {
                if (selectedTower != null)
                {
                    ClearSelectedTower();
                    //	return; Из-за этого если выбрана башня и происходит нажатие на свободную площадку нужно 2 клика
                }

                if (!UseDragNDrop())
                {
                    if (BuildManager.CheckBuildPoint(cursorPos) == _TileStatus.Available)
                    {
                        UIBuildButton.HideTowerInfoAndSmapleTower();
                        UIBuildButton.Show();
                        TimeScaleManager.SetTimeScale(0.5f);
                    }
                    else
                    {
                        UIBuildButton.Hide();
                    }
                }
            }

        }


        void SelectTower(UnitTower tower)
        {
            selectedTower = tower;
            //TimeScaleManager.SetTimeScale(0.5f);

            //TowerInfoSmall.Show(selectedTower);
            UITowerInfo.ShowButtons(selectedTower);
            //Vector3 screenPos=Camera.main.WorldToScreenPoint(selectedTower.thisT.position);
            //UITowerInfo.SetScreenPos(screenPos);

            //UITowerInfo.Show(selectedTower, true);
            if (onSelectTower != null)
                onSelectTower(tower);
        }

        public static void ClearSelectedTower()
        {
            TimeScaleManager.RefreshTimeScale();
            if (instance != null)
            {
                if (instance.selectedTower == null)
                    return;

                instance.selectedTower = null;

                TowerInfoSmall.Hide();
                UITowerInfo.HideButtons();
                UITowerInfo.HideInfoPanel();//UITowerInfo.Hide();
                GameControl.ClearSelectedTower();
            }

            if (onClearSelectTower != null)
                onClearSelectTower();
        }

        public static UnitTower GetSelectedTower() { return instance.selectedTower; }



        void OnFPSMode(bool flag)
        {
            //FPSModeCrosshairObj.SetActive(flag);

#if UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY
            UIGameMessage.DisplayMessage("FPS mode is not supported in mobile");
#endif

            if (flag)
            {
                UIBuildButton.Hide();
                UIAbilityButton.Hide();
                UIPerkMenu.Hide();
                UIFPSHUD.Show();
            }
            else
            {
                if (UseDragNDrop()) UIBuildButton.Show();
                if (AbilityManager.IsOn()) UIAbilityButton.Show();
                if (PerkManager.IsOn()) UIPerkMenu.Show();
                UIFPSHUD.Hide();
            }
        }
        void OnFPSCameraActive(bool flag)
        {
            //Debug.Log(flag);
            //FPSModeCrosshairObj.SetActive(flag);
        }

        //public GameObject FPSModeCrosshairObj;
        public void OnFPSModeButton()
        {
            if (selectedTower == null) return;

            Vector3 pos = selectedTower.thisT.position + new Vector3(0, 7, 0);

            FPSControl.SetAnchorTower(selectedTower);
            ClearSelectedTower();

            FPSControl.Show(pos);
            //FPSModeCrosshairObj.SetActive(true);

        }






        void OnNewTower(UnitTower newTower)
        {
            UIBuildButton.AddNewTower(newTower);
        }
        void OnNewAbility(Ability newAbility)
        {
            Debug.Log("new abiility");
            UIAbilityButton.AddNewAbility(newAbility);
        }
    }

}