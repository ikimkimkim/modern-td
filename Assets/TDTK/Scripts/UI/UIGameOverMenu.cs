﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

using TDTK;
using System;

namespace TDTK
{

    public class UIGameOverMenu : MonoBehaviour
    {
        [System.Serializable]
        public struct StarIcon
        {
            public GameObject StarOn, StarOff;
        }


        [SerializeField]
        private GameObject thisObj;
        private static UIGameOverMenu instance;


        public GameObject ErrorObj;
        public Text ErrorText;

        public GameObject WinMenu;
        [SerializeField]
        private GameObject StarsPanel,StarsRedPanel;
        [SerializeField]
        private StarIcon[] Stars;
        [SerializeField]
        private StarIcon[] StarsRed;

        //public Image[] StarEarned = new Image[3];
       // public Image[] StarsDontEarned = new Image[3];
        public GameObject PanelInfoWM;
        public Text GoldWM, CristalWM;
        public GameObject GoldObj, CristalObj;
        public Image ChestIconWM;
        public Text NameChestWM, deltaTimeChestWM;
        public GameObject ObjNameChestWM, ObjDeltaTimeWM, ObjSlotLimitWM;


        public GameObject PortalWinMenu;
        public Text GoldPWM, CristalPWM, TimePWM, ScorePWM;
        public GameObject PanelInfoPWM;
        public GameObject GoldPObj, CristalPObj;
        public GameObject ObjTimePWM,ObjScorePWM;
        public Image ChestIconPWM;
        public Text NameChestPWM, deltaTimeChestPWM, deltaTimeCloseChestPWM;
        public GameObject ObjNameChestPWM, ObjDeltaTimeChestPWM, ObjDeltaTimeCloseChestPWM, ObjSlotLimitPWM;

        [Header("PanelPrizes2")]
        [SerializeField]
        private GameObject panelCardObj;
        [SerializeField]
        private CardInfoUI cardInfoPrize;
        [SerializeField]
        private Text cardNameProze;

        [Space()]
        public GameObject LoseMenu;
        public Image[] LoseStarsDontEarned = new Image[3];



        public float StarShowDelay;

        private TimeSpan _time;

        void Awake()
        {
            instance = this;
        }
        
        void Start()
        {
            Hide();
        }



        public void OnContinueButton()
        {
            TimeScaleManager.RefreshTimeScale();
            GameControl.LoadNextScene();
        }

        public void OnRestartButton()
        {
            GameControl.RestartLevel();
        }

        public void OnMainMenuButton()
        {
            MyLog.Log("OnMainMenuButton count:" + PlayerManager._instance.GetLevelsCountComplele()+" level:"+ GameControl.GetLevelID());
            /*if(PlayerManager._instance.GetLevelsCount() == 12 && GameControl.GetLevelID() == 12)
            {
                HistoryPanel.StartShow(loadMenu, new int[] { 22, 23, 24 });
            }
            else if (PlayerManager._instance.GetLevelsCount() == 24 && GameControl.GetLevelID() == 212)
            {
                HistoryPanel.StartShow(loadMenu, new int[] { 28, 29, 30 });
            }
            else if (PlayerManager._instance.GetLevelsCount() == 36 && GameControl.GetLevelID() == 312)
            {
                HistoryPanel.StartShow(loadMenu, new int[] { 34, 35, 36 });
            }
            else*/
            {
                loadMenu();
            }        
        }

        private void loadMenu()
        {
            TimeScaleManager.SetTimeScale(1f);
            GameControl.LoadMainMenu();
        }


        public static bool isOn = true;
        public static void Show(int stars,ResponseData data) { instance._Show(stars, data); }
        private int _stars;
        public void _Show(int stars, ResponseData data)
        {
            _stars = stars;
            if (data.error!=null && data.error.code!=null && data.error.code!="")
            {
                ErrorObj.SetActive(true);
                ErrorText.text =  data.error.code + ": " + data.error.text;
            }
            ChestManager.TypeNewChest = data.prizes.chest-1;
            ChestManagerV2.CountStars = data.new_stars;
            getResult(data.prizes.chest, data.prizes.gold, data.prizes.crystals, data.prizes2);
        }

        public void getResult(int chest,int Gold,int Cristal, ResponseData.PrizesTower prizesCard)
        {
            TimeScaleManager.SetTimeScale(0f);
            //GameControl.PauseGame();

            isOn = true;
            thisObj.SetActive(isOn);
            

            if (_stars != -1)
            {
                if (CardAndLevelSelectPanel.IDLevel < 0)
                {
                    PopapPortalWin(chest, Gold, Cristal);
                }
                else
                {
                    PopapWin(chest, Gold, Cristal);
                }
                MyLog.Log("Stars:" + _stars);
                if (_stars < 4)
                {
                    StartCoroutine(ShowStars(_stars));
                }
                else
                {
                    StartCoroutine(ShowStarsRed(_stars-3));
                }
                ShowPrizes2(prizesCard);
            }
            else
            {
                StarsPanel.SetActive(false);
                StarsRedPanel.SetActive(false);
                PopapLose();               
            }           
        }

        private void PopapLose()
        {
            LoseMenu.SetActive(true);
            if (CardAndLevelSelectPanel.IDLevel < 0)
            {
                for (int i = 0; i < 3; i++)
                {
                    LoseStarsDontEarned[i].enabled = false;
                }
            }
        }

        private void PopapPortalWin(int chest, int Gold, int Cristal)
        {
            PortalWinMenu.SetActive(true);
            ObjDeltaTimeChestPWM.SetActive(false);
            ObjDeltaTimeCloseChestPWM.SetActive(false);
            ObjNameChestPWM.SetActive(false);
            ObjSlotLimitPWM.SetActive(false);


            if (Gold > 0)
            {
                GoldPObj.SetActive(true);
                GoldPWM.text = Gold.ToString();
            }
            else
                GoldPObj.SetActive(false);

            if(Cristal>0)
            {
                CristalPObj.SetActive(true);
                CristalPWM.text = Cristal.ToString();
            }
            else
                CristalPObj.SetActive(false);

            PanelInfoPWM.SetActive(Gold > 0 || Cristal > 0);
            
            ObjTimePWM.SetActive(CardAndLevelSelectPanel.BigPortal);
            ObjScorePWM.SetActive(!CardAndLevelSelectPanel.BigPortal);
            _time = new TimeSpan(0, 0, Mathf.CeilToInt(GameControl.GetTimeGame));
            TimePWM.text = _time.ToString();
            ScorePWM.text = UIScorePanel.GetScore.ToString();
            if (_time.Minutes > 10 || (_time.Minutes == 10 && _time.Seconds > 0))
            {
                TimePWM.color = Color.red;
            }
            else
            {
                TimePWM.color = Color.green;
            }

            if (chest > 0)
            {

                ObjNameChestPWM.SetActive(true);
                NameChestPWM.text = Chest.getNameChest((Chest.TypeChest)chest - 1);
                StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsChest[chest - 1], value => { ChestIconPWM.sprite = value; }));

                // Debug.LogWarning("Нету расчетов для отображение сокращения времени открытия сундука");
                if (CardAndLevelSelectPanel.BigPortal)
                {
                    if (_time.Minutes > 10 || (_time.Minutes == 10 && _time.Seconds > 0))
                    {
                        if (/*PanelBigPortal.chestProchentOpen != null &&*/ PanelBigPortal.chestProchentOpen != 0)
                        {
                            ObjDeltaTimeCloseChestPWM.SetActive(true);
                            deltaTimeCloseChestPWM.text = "Время открытия уменьшено до " + PanelBigPortal.chestProchentOpen + "%";
                        }
                    }
                    else
                    {
                        if (/*PanelBigPortal.chestProchentOpen != null &&*/ PanelBigPortal.chestProchentOpen != 0)
                        {
                            ObjDeltaTimeChestPWM.SetActive(true);
                            deltaTimeChestPWM.text = "Время открытия уменьшено до " + PanelBigPortal.chestProchentOpen + "%";
                        }
                    }
                }
            }
            else
            {
                ObjSlotLimitPWM.SetActive(PlayerManager.ChestList.Count > 3);
                NameChestPWM.text = "";
                StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsEmptyChest, value => { ChestIconPWM.sprite = value; }));
            }
        }

        private void ShowPrizes2(ResponseData.PrizesTower prizesCard)
        {
            panelCardObj.SetActive(prizesCard != null);
            if (panelCardObj.activeSelf)
            {

                Card card = PlayerManager.CardsList.Find(i => i.IDTowerData == prizesCard.type-1 && (int)i.rareness == prizesCard.rarity - 1);
                if (card != null)
                {
                    cardInfoPrize.SetInfoTower(card);
                    if (card.Type == _CardType.Upgrade)
                        cardNameProze.text = card.UpgradeData.generalDesp;
                    else
                        cardNameProze.text = card.unitName;
                }
                else
                {
                    Debug.LogError("UI game over, show prizes 2. can't find card!");
                    panelCardObj.SetActive(false);
                }
            }
        }

        private void PopapWin(int chest, int Gold, int Cristal)
        {
            WinMenu.SetActive(true);
            ObjDeltaTimeWM.SetActive(false);
            ObjNameChestWM.SetActive(false);
            ObjSlotLimitWM.SetActive(false);



            if (Gold > 0)
            {
                GoldObj.SetActive(true);
                GoldWM.text = Gold.ToString();
            }
            else
                GoldObj.SetActive(false);

            if (Cristal > 0)
            {
                CristalObj.SetActive(true);
                CristalWM.text = Cristal.ToString();
            }
            else
                CristalObj.SetActive(false);

            PanelInfoWM.SetActive(Cristal > 0 || Gold > 0);

            if (chest!=0)
            {

                    ObjNameChestWM.SetActive(true);
                    NameChestWM.text = Chest.getNameChest((Chest.TypeChest)chest - 1);
                    StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsChest[chest - 1], value => { ChestIconWM.sprite = value; }));

                    //Debug.LogWarning("Нету расчетов для отображение сокращения времени открытия сундука");
                    /*if (chest.Time_decrease_pcnt!=null && chest.Time_decrease_pcnt!="0")
                    {
                        ObjDeltaTimeWM.SetActive(true);
                        deltaTimeChestWM.text = "Время открытия сокращено на " + chest.Time_decrease_pcnt + "%";
                    }*/
                
            }
            else
            {
                Debug.Log("GameOver WM Chest is 0");
                ObjSlotLimitWM.SetActive(PlayerManager.ChestList.Count > 3);
                StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsEmptyChest, value => { ChestIconWM.sprite = value; }));
            }

        }

        IEnumerator ShowStars(int star)
        {
            StarsPanel.SetActive(true);
            for (int i = 0; i < Stars.Length; i++)
            {
                Stars[i].StarOn.SetActive(true);
                Stars[i].StarOff.SetActive(true);
            }
            for (int i = 0; i < star; i++)
            {
                yield return new WaitForSecondsRealtime(StarShowDelay);
                Stars[i].StarOff.SetActive(false);
            }
            yield break;
        }
        IEnumerator ShowStarsRed(int star)
        {
            StarsRedPanel.SetActive(true);
            for (int i = 0; i < StarsRed.Length; i++)
            {
                StarsRed[i].StarOn.SetActive(true);
                StarsRed[i].StarOff.SetActive(true);
            }
            for (int i = 0; i < star; i++)
            {
                yield return new WaitForSecondsRealtime(StarShowDelay);
                StarsRed[i].StarOff.SetActive(false);
            }
            yield break;
        }

       /* IEnumerator StarShower(int star)
        {
            float timestart = Time.realtimeSinceStartup;
            int i = 0;


            if(star>3)
            {
                StarsDontEarned[0].gameObject.SetActive(false);
                StarsDontEarned[2].gameObject.SetActive(false);
                StarEarned[0].gameObject.SetActive(false);
                StarEarned[1].gameObject.SetActive(false);
                StarEarned[2].gameObject.SetActive(false);
                StarEarned[3].gameObject.SetActive(true);

                yield return new WaitForSecondsRealtime(StarShowDelay);
                StarsDontEarned[1].gameObject.SetActive(false);
            }
            else
            {
                StarEarned[3].gameObject.SetActive(false);
                while (true)
                {
                    if (timestart + StarShowDelay < Time.realtimeSinceStartup)
                    {
                        if (i >= star)
                            break;
                        StarEarned[i].gameObject.SetActive(true);
                        StarsDontEarned[i].gameObject.SetActive(false);
                        i++;
                        
                        timestart = Time.realtimeSinceStartup;
                    }
                    yield return null;
                }
            }

            
            yield break;
        }*/
        public static void Hide() { instance._Hide(); }
        public void _Hide()
        {
            isOn = false;
            ErrorObj.SetActive(false);
            WinMenu.SetActive(isOn);
            PortalWinMenu.SetActive(isOn);
            LoseMenu.SetActive(isOn);
            thisObj.SetActive(isOn);
        }

    }


}