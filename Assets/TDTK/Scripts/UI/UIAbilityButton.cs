﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using System.Collections;
using System.Collections.Generic;
using Translation;
using TDTK;

namespace TDTK
{

    public class UIAbilityButton : MonoBehaviour
    {
        public List<UnityButton> buttonList = new List<UnityButton>();
        public Color ColorBlink;

        public RectTransform energyRect;
        public Text txtEnergy;
        public Sprite[] CardLevelFons;

        public GameObject tooltipObj;
        public GameObject Arrow;
        public Text txtTooltipName;
        public Text txtTooltipDesp;
        public Text txtTooltipPropertis;


        public GameObject TooltipRcsObj;
        public Text txtTooltipRcsUpgrade;
        public GameObject TooltipNotNextLevel;
        public GameObject TooltipInfo;

        public static UIAbilityButton instance { get; private set; }
        private GameObject thisObj;

        private List<Card> abList;

        public float TooltipLeftMargin;
        public float TooltipRightMargin;
        public RectTransform MainCanvas;
        public RectTransform TooltipCanvas;

        public GameObject CrystalsUI;
        public GameObject ManaUI, CoinUI;

        private bool bHoverUpgradeButton = false;

        void Awake()
        {
            instance = this;
            thisObj = gameObject;
           // ResourceManager.onRscChangedE += OnResourceChanged;
        }


        public void TutorialShowUpgradeCurcor(int minLevel)
        {
            for (int i = 0; i < buttonList.Count; i++)
            {
                if(abList[i].LevelCard>=minLevel)
                {
                    buttonList[i].TutorialCursorUpgrade.SetActive(true);
                    break;
                }
            }
        }
        public void TutorialHide()
        {
            for (int i = 0; i < buttonList.Count; i++)
            {
                if (buttonList[i].TutorialCursorUpgrade.activeSelf==true)
                {
                    buttonList[i].TutorialCursorUpgrade.SetActive(false);
                    break;
                }
            }
        }

        // Use this for initialization
        public void StartUI(List<Card> cardsAbilityActive)
        {
            MyLog.Log("UIAB start");
            tooltipObj.SetActive(false);

            if (cardsAbilityActive == null)
                throw new System.ArgumentNullException("cardsAbility");


            if (!AbilityManager.IsOn())
            {
                Hide();
                return;
            }

            abList = cardsAbilityActive;
            if (abList.Count == 0)
            {
                Hide();
                return;
            }

            UpdateDataAbility();

                //transform.localPosition += new Vector3(0, -100, 0); // возможно нужно умножать на скейл

        }


        public void UpdateDataAbility()
        {
            //EventTrigger.Entry entryRequireTargetSelect = SetupTriggerEntry(true);
           // EventTrigger.Entry entryDontRequireTargetSelect = SetupTriggerEntry(false);

            for (int i = 0; i < abList.Count; i++)
            {
                if (i >= buttonList.Count)
                    buttonList.Add(buttonList[0].Clone("AbilityButton" + (i + 2), new Vector3(i * 65, 0, 0)));

                buttonList[i].Init();
                buttonList[i].imageIcon.sprite = abList[i].iconSprite;
                buttonList[i].levelCard.text = abList[i].LevelCard.ToString();
                float price = abList[i].abilityObj.GetCost();
                price = Mathf.Ceil(price * 10f) / 10f;
                buttonList[i].price.text = price.ToString();//.stats[0].cost[1].ToString();                
                buttonList[i].label.text = "";
                buttonList[i].imageFonLevelCard.sprite = CardLevelFons[(int)abList[i].rareness];
                buttonList[i].textIndexButtonAlpha.text = (i + 1).ToString();
                if (/*abList[i].LevelCard > abList[i].LevelTower */abList[i].NextLevelTower.Count > 0)
                {
                    buttonList[i].buttonUpgrade.GetComponent<Button>().interactable = true;
                    //buttonList[i].priceUpgrade.text = abList[i].NextLevelTower[0].GetCost()[0].ToString();
                }
                else
                    buttonList[i].buttonUpgrade.GetComponent<Button>().interactable = false;

               /* EventTrigger trigger = buttonList[i].rootObj.GetComponent<EventTrigger>();
                Debug.LogWarning(trigger.triggers.Count);
                
                if (abList[i].abilityObj.requireTargetSelection)
                    trigger.triggers.Add(entryRequireTargetSelect);
                else
                    trigger.triggers.Add(entryDontRequireTargetSelect);*/
            }
        }

        private EventTrigger.Entry SetupTriggerEntry(bool requireTargetSelection)
        {
            UnityEngine.Events.UnityAction<BaseEventData> call = new UnityEngine.Events.UnityAction<BaseEventData>(OnAbilityButton);
            EventTriggerType eventID = EventTriggerType.PointerClick;
#if UNITY_IOS|| UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY
            if (requireTargetSelection) eventID = EventTriggerType.PointerDown;
#endif

            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = eventID;
            entry.callback = new EventTrigger.TriggerEvent();
            entry.callback.AddListener(call);

            return entry;
        }


        void OnEnable()
        {
            AbilityManager.onAbilityActivatedE += OnAbilityActivated;
            GameControl.onGameStarted += GameControl_onGameStarted;
        }

        void GameControl_onGameStarted(int levelID)
        {
            //StartCoroutine(MoveUp(0.5f));
        }
        IEnumerator MoveUp(float moveDuration)
        {
            Vector3 startPosition = transform.localPosition;
            Vector3 endPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);
            float timer = 0;
            while (timer / moveDuration <= 1)
            {
                transform.localPosition = Vector3.Lerp(startPosition, endPosition, Mathf.SmoothStep(0f, 1f, timer / moveDuration));
                timer += Time.deltaTime;
                yield return null;
            }
            yield break;
        }
        void OnDisable()
        {
            AbilityManager.onAbilityActivatedE -= OnAbilityActivated;
            GameControl.onGameStarted -= GameControl_onGameStarted;
        }


        public static void AddNewAbility(Ability newAbility) { instance._AddNewAbility(newAbility); }
        public void _AddNewAbility(Ability newAbility)
        {
            float offset = 0.5f * (buttonList.Count - 1) * 55;
            for (int i = 0; i < buttonList.Count; i++)
            {
                buttonList[i].rootT.localPosition += new Vector3(offset, 0, 0);
            }

            int count = buttonList.Count;
            buttonList.Add(buttonList[count - 1].Clone("button" + (count), new Vector3(60, 0, 0)));
            buttonList[count].imageIcon.sprite = newAbility.icon;

           /* EventTrigger trigger = buttonList[count].rootObj.GetComponent<EventTrigger>();
            if (newAbility.requireTargetSelection)
            {
                EventTrigger.Entry entryRequireTargetSelect = SetupTriggerEntry(true);
                trigger.triggers.Add(entryRequireTargetSelect);
            }
            else
            {
                EventTrigger.Entry entryDontRequireTargetSelect = SetupTriggerEntry(false);
                trigger.triggers.Add(entryDontRequireTargetSelect);
            }*/

            offset = 0.5f * (buttonList.Count - 1) * 55;
            for (int i = 0; i < buttonList.Count; i++)
            {
                buttonList[i].rootT.localPosition -= new Vector3(offset, 0, 0);
            }
        }

        public void OnShowAllInfoButton(GameObject butObj)
        {
            int id = GetButtonID(butObj);

            Card c = abList[id];

            CardUIAllInfo.instance.Show(c, CardUIAllInfo.Mode.gameUpgrade);
        }


        public string OnUpgradeButton(Card c)
        {
            for (int i = 0; i < buttonList.Count; i++)
            {
                if (abList[i]==c)
                {
                    return UpdateAbility(c, i);
                }
            }
            return "Not Find Ability";
        }

        public void OnUpgradeButton(GameObject butObj)
        {
            int id = GetButtonID(butObj);
            Card c = abList[id];
            /*string s = */UpdateAbility(c, id);
            _showTooltip(butObj);
        }

        private int IdCache;
        private Card CardCache;
        private string UpdateAbility(Card c,int id)
        {
            if (/*c.LevelTower < c.LevelCard*/c.NextLevelTower.Count>0)
            {
                if (c.NextLevelTower[0].GetCost()[0] <= ResourceManager.GetResourceArray()[0])
                {
                    IdCache = id;
                    CardCache = c;
                    TowerSelectProperties.initShow(c.NextLevelTower[0], StartUpdate);

                    return "";
                }
                else
                {
                    RedBlinking.StartBlinking(TooltipRcsObj);
                    MyLog.Log("UIAB Can't upgrade card id:" + c.ID + " Rsc Coin<Price " + ResourceManager.GetResourceArray()[0] + "<" + c.NextLevelTower[0].GetCost()[0]);
                    return "Low Cost";
                }
            }
            else
            {
                MyLog.Log("UIAB Can't upgrade card id:" + c.ID + " lvlT<lvlC " + c.LevelTower + "<" + c.LevelCard);
                return "Low Level";
            }
        }
        public void StartUpdate(bool isUpgrade)
        {
            if (isUpgrade)
            {
                ResourceManager.SpendResource(new List<float> { CardCache.NextLevelTower[0].GetCost()[0], 0 });
                abList[IdCache] = CardCache.NextLevelTower[0];

                TooltipRcsObj.SetActive(false);
                MyLog.LogWarning("UpAb:count:" + abList[IdCache].NextLevelTower.Count);
                if (abList[IdCache].NextLevelTower.Count>0)
                {
                    buttonList[IdCache].buttonUpgrade.GetComponent<Button>().interactable = true;
                    //txtTooltipRcsUpgrade.text = abList[id].NextLevelTower[0].GetCost()[0].ToString();
                }
                else
                    buttonList[IdCache].buttonUpgrade.GetComponent<Button>().interactable = false;

                AbilityManager.UpdateAbility(IdCache, abList[IdCache]);

                buttonList[IdCache].Star[abList[IdCache].LevelTower - 2].SetActive(true);
            }
        }

        public void OnAbilityButton(UnityEngine.EventSystems.BaseEventData baseEvent)
        {
            OnAbilityButton(baseEvent.selectedObject);
        }
        public void OnAbilityButton(GameObject butObj)
        {
            //if(FPSControl.IsOn()) return;
            
            UIHeroSpawn.instance.ClearSelected();

            //in drag and drop mode, player could be hitting the button while having an active tower selected
            //if that's the case, clear the selectedTower first. and we can show the tooltip properly
            if (UI.UseDragNDrop() && GameControl.GetSelectedTower() != null)
            {
                UI.ClearSelectedTower();
                return;
            }

            UI.ClearSelectedTower();
            UIBuildButton.Hide();
            int ID = GetButtonID(butObj);
            MyLog.Log("UIAB id:" + ID);

            if (AbilityManager.GetSelectedAbilityID() == AbilityManager.GetAbilityCardList()[ID].abilityObj.ID)
            {
                #if UNITY_ANDROID || UNITY_IOS
                HideTooltip();
                #endif
                AbilityManager.ClearSelectedAbility();
                MyLog.Log("UIAB clear:");
                return;
            }
            else
            {
                int n = AbilityManager.GetSelectedAbilityID();
                if (n != -1)
                    HideCancelIcon(n);
                MyLog.Log("UIAB hide:" + AbilityManager.GetSelectedAbilityID() + "|" + AbilityManager.GetAbilityCardList()[ID].abilityObj.ID);
            }



            MyLog.Log("UIAB requireTargetSelection:" + AbilityManager.GetAbilityCardList()[ID].abilityObj.requireTargetSelection);
            if (!AbilityManager.GetAbilityCardList()[ID].abilityObj.requireTargetSelection)
            {
#if UNITY_ANDROID || UNITY_IOS
                HideTooltip();
#endif
                int n = AbilityManager.GetSelectedAbilityID();
                if (n != -1)
                {
                    AbilityManager.ClearSelectedAbility();
                    return;
                }
            }
            int lastID = AbilityManager.GetSelectedAbilityID();

            MyLog.Log("UIAB SelectAbility");
            string exception = AbilityManager.SelectAbility(ID);
            MyLog.Log("UIAB exception:"+exception);
            if (exception != "")
            {
                if (exception == "Ability is on cooldown")
                {
                    RedBlinking.StartBlinking(butObj);
                }
                else
                {
                    //Если все еще маргает текст прекратить
                    RedBlinking.StopBlinking(butObj);
                }
                if (exception == "Insufficient Energy")
                {
                    //Кристаллов не хватает
                    RedBlinking.StartBlinking(CrystalsUI);
                    CrystalsUI.GetComponentInChildren<ButtonInstantiate>().InstantiateOnCanvas();
                }
                if (exception == "Insufficient Mana")
                {
                    RedBlinking.StartBlinking(ManaUI);
                    RedBlinking.StartBlinking(buttonList[ID].price.gameObject);
                }

                    if (lastID != -1)
                {
                    ShowCancelIcon(lastID);
                }
            }
            else
            {
                if (AbilityManager.GetAbilityCardList()[ID].abilityObj.requireTargetSelection)
                    ShowCancelIcon(ID);

                #if UNITY_ANDROID || UNITY_IOS
                _showTooltip(butObj);
                #endif
            }

        }

        void ShowCancelIcon(int id)
        {
            buttonList[id].imageIcon.color = new Color(.250f, .250f, .250f, 1);
            buttonList[id].cancelIcon.gameObject.SetActive(true);
        }
        public void HideCancelIcon(int id)
        {
            if (buttonList[id].label.text == "")
                buttonList[id].imageIcon.color = new Color(1f, 1f, 1f, 1);
            buttonList[id].cancelIcon.gameObject.SetActive(false);
        }
        public void OnHoverAbilityButton(GameObject butObj)
        {
            //if(FPSControl.IsOn()) return;

            UIHeroSpawn.instance.bShotTooltip = true;

#if (UNITY_WEBGL || UNITY_WEBPLAYER)
            _showTooltip(butObj);
#endif
        }

        public void OnHoverAbilityInfoButton(GameObject butObj)
        {
            TooltipInfo.SetActive(true);
            float xPos = butObj.transform.position.x;
            TooltipInfo.transform.position = new Vector3(xPos+39, TooltipInfo.transform.position.y, TooltipInfo.transform.position.z);
        }

        public void OnHoverAbilityUpgradeButton(GameObject butObj)
        {
            bHoverUpgradeButton = true;
            int ID = GetButtonID(butObj);
            Card cardAb = AbilityManager.GetAbilityCardList()[ID];
            float xPos = butObj.transform.position.x;
            if(cardAb.NextLevelTower.Count>0 /*cardAb.LevelCard>cardAb.LevelTower*/)
            {
                TooltipRcsObj.SetActive(true);
                txtTooltipRcsUpgrade.text = cardAb.NextLevelTower[0].GetCost()[0].ToString();
                TooltipRcsObj.transform.position = new Vector3(xPos + 38, TooltipRcsObj.transform.position.y, TooltipRcsObj.transform.position.z);
                SetInfoMainTooltip(ID, cardAb.NextLevelTower[0]);
            }
            else
            {

                TooltipNotNextLevel.SetActive(true);
                TooltipNotNextLevel.transform.position = new Vector3(xPos+135, TooltipNotNextLevel.transform.position.y, TooltipNotNextLevel.transform.position.z);
            }
            
        }

        public void OnExitAbilityUpgradeButton(GameObject butObj)
        {
            bHoverUpgradeButton = false;
            TooltipRcsObj.SetActive(false);
            TooltipNotNextLevel.SetActive(false);
            TooltipInfo.SetActive(false);

            int ID = GetButtonID(butObj);
            if (ID == -1)
                return;
            List<Card> cards = AbilityManager.GetAbilityCardList();
            if(cards.Count>0)
            {
                MyLog.LogError("UIAbilityButtin->OnExitAbilityUpgradeButton-> AbilityCardList count is 0");
                return;
            }
            Card cardAb = cards[ID];
            SetInfoMainTooltip(ID, cardAb);
        }

        public float TooltipOffsetY;
        private void _showTooltip(GameObject butObj)
        {

            if (GameControl.GetSelectedTower() != null) return;


            int ID = GetButtonID(butObj);
            float xPos = butObj.transform.position.x;
            if (xPos < TooltipLeftMargin * UI.GetScaleFactor())
            {
                xPos = TooltipLeftMargin * UI.GetScaleFactor();
            }
            if (xPos > TooltipCanvas.sizeDelta.x * UI.GetScaleFactor() - TooltipRightMargin * UI.GetScaleFactor())
            {
                xPos = TooltipCanvas.sizeDelta.x * UI.GetScaleFactor() - TooltipRightMargin * UI.GetScaleFactor();
            }
            tooltipObj.transform.position = new Vector3(xPos, TooltipOffsetY * MainCanvas.localScale.x /* tooltipObj.transform.position.y*/, tooltipObj.transform.position.z);
            Arrow.transform.position = new Vector3(butObj.transform.position.x, Arrow.transform.position.y, Arrow.transform.position.z);

            Card cardAb = AbilityManager.GetAbilityCardList()[ID];

            if(bHoverUpgradeButton && cardAb.LevelCard > cardAb.LevelTower && cardAb.NextLevelTower.Count>0)
                SetInfoMainTooltip(ID, cardAb.NextLevelTower[0]);
            else 
                SetInfoMainTooltip(ID, cardAb);

            Arrow.SetActive(true);
            tooltipObj.SetActive(true);
        }

        private void SetInfoMainTooltip(int ID, Card cardAb)
        {
            Ability ability = cardAb.abilityObj;
            Translator trans = TranslationEngine.Instance.Trans;
            txtTooltipName.text = trans[ability.name];

            txtTooltipDesp.text = trans[ability.GetDesp()];
            txtTooltipPropertis.text = "";

            var CurrentStat = CraftLevelData.GetStatGrade(cardAb);

            if (CurrentStat.damageMax > 0)
                txtTooltipPropertis.text += CurrentStat.damageMin + "-" + CurrentStat.damageMax + " ед. ур.";

            List<CardProperties> activeProper = new List<CardProperties>();
            bool apply, addActive;
            foreach (CardProperties proper in cardAb.Properties)
            {
                apply = true;
                for (int i = 0; i < proper.Requirements.Count; i++)
                {
                    if (proper.Requirements[i].CanActiveProperties(cardAb) == false)
                    {
                        apply = false;
                        break;
                    }
                }
                if (apply)
                {
                    addActive = false;
                    for (int j = 0; j < activeProper.Count; j++)
                    {
                        if (activeProper[j].ID == proper.ID)
                        {
                            addActive = true;
                            activeProper[j] = proper.Clone();
                            break;
                        }
                    }
                    if (addActive == false)
                        activeProper.Add(proper.Clone());
                }
            }

            for (int i = 0; i < activeProper.Count; i++)
            {
                if (txtTooltipPropertis.text.Length > 0)
                    txtTooltipPropertis.text += "\r\n";
                txtTooltipPropertis.text += activeProper[i].getInfoText();
            }
            activeProper.Clear();
        }

        public void OnExitHoverAbilityButton(GameObject butObj)
        {
            UIHeroSpawn.instance.bShotTooltip = false;
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
            HideTooltip();
#endif
        }

        public void HideTooltip()
        {
            tooltipObj.SetActive(false);
            Arrow.SetActive(false);
        }


        int GetButtonID(GameObject butObj)
        {
            for (int i = 0; i < buttonList.Count; i++)
            {
                if (buttonList[i].rootObj == butObj)
                    return i;
            }
            MyLog.LogError("UIAbilityButtin->GetButtonID-> not find id");
            return -1;
        }

        Card GetButtonCard(GameObject butObj)
        {
           return abList[GetButtonID(butObj)];
        }

        void OnAbilityActivated(Ability ab)
        {
            StartCoroutine(ButtonCDROutine(ab));
#if UNITY_ANDROID || UNITY_IOS
            HideTooltip();
#endif
        }
        IEnumerator ButtonCDROutine(Ability ab)
        {
            int ID = ab.ID;
            buttonList[ID].imageIcon.color = new Color(.125f, .125f, .125f, 1);

            if (ab.usedCount == ab.maxUseCount)
            {
                buttonList[ID].label.text = "Used";
                yield break;
            }


            while (true)
            {
                string text = "";
                float duration = ab.currentCD;
                if (duration <= 0) break;

                if (duration > 60) text = Mathf.Floor(duration / 60).ToString("F0") + "м";
                else text = (Mathf.Ceil(duration)).ToString("F0") + "с";
                buttonList[ID].label.text = text;
                yield return new WaitForSeconds(0.1f);
            }
            buttonList[ID].imageIcon.color = new Color(1, 1, 1, 1);

            buttonList[ID].label.text = "";

        }


        // Update is called once per frame
        void Update()
        {
            for (int i = 0; i < buttonList.Count; i++)
            {
               
                if (Input.GetKeyDown((KeyCode)(49+i)))
                {
                    Debug.Log("Douwn button " + i);
                    OnAbilityButton(buttonList[i].rootObj);
                    return;
                }

            }

           // txtEnergy.text = AbilityManager.GetEnergy().ToString("f0") + "/" + AbilityManager.GetEnergyFull().ToString("f0");
           // float valueX = Mathf.Clamp(AbilityManager.GetEnergy() / AbilityManager.GetEnergyFull() * 200, 4, 200);
           // float valueY = Mathf.Min(valueX, 8);
          //  energyRect.sizeDelta = new Vector2(valueX, valueY);
        }



        public static bool isOn = true;
        public static void Show() { instance._Show(); }
        public void _Show()
        {
            isOn = true;
            thisObj.SetActive(isOn);
        }
        public static void Hide() { instance._Hide(); }
        public void _Hide()
        {
            MyLog.Log("UIAB hide");
            isOn = false;
            thisObj.SetActive(isOn);
        }




    }

}