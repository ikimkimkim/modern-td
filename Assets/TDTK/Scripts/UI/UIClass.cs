﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;
using System;

namespace TDTK
{


    [System.Serializable]
    public class UIObject
    {
        public GameObject rootObj;
        public Transform rootT;
    }


    [System.Serializable]
    public class UIItem
    {
        public Text label;
        public Image image;
    }

    [System.Serializable]
    public class UnityButton : UIObject
    {
        public Button button;
        public Text label;
        public Text price;// priceUpgrade;
        public Text levelCard;
        public Text Income;
        public Image imageFonLevelCard;
        public Image imageBG;
        public Image imageIcon;
        public Image cancelIcon;
        public Animator animator;
        public GameObject buttonInfo;// tooltipRscUpgrade;
        public GameObject buttonUpgrade, Plus;
        public GameObject[] Star;
        public Text textIndexButtonAlpha;

        public GameObject TutorialCursorUpgrade;

        public void Init()
        {
            rootT = rootObj.transform;
            animator = rootObj.GetComponent<Animator>();
            button = rootObj.GetComponent<Button>();
            imageBG = rootObj.GetComponent<Image>();
            Star = new GameObject[4];


            foreach (Transform child in rootT)
            {
                if (child.name == "Mask")
                {
                    foreach (Transform child2 in child)
                    {
                        if (child2.name == "Image")
                        {
                            imageIcon = child2.GetComponent<Image>();
                        }
                        else if (child2.name == "Text")
                        {
                            label = child2.GetComponent<Text>();
                        }
                        else if (child2.name == "Cancel")
                        {
                            cancelIcon = child2.GetComponent<Image>();
                            cancelIcon.gameObject.SetActive(false);
                        }
                    }
                }
                else if (child.name == "Image")
                {
                    imageIcon = child.GetComponent<Image>();
                }
                else if (child.name == "Text")
                {
                    label = child.GetComponent<Text>();
                }
                else if(child.name == "PricePanel")
                {
                    foreach (Transform child2 in child)
                    {
                        if (child2.name == "Price")
                        {
                            price = child2.GetComponent<Text>();
                        }
                    }
                }
                else if (child.name == "ButtonInfo")
                {
                    buttonInfo = child.gameObject;
                }
                else if (child.name == "ButtonUpgrade")
                {
                    buttonUpgrade = child.gameObject;
                }
                else if (child.name == "CursorUpgrade")
                {
                    TutorialCursorUpgrade = child.gameObject;
                }
                else if(child.name == "LevelImage")
                {
                    foreach (Transform child2 in child)
                    {
                        if (child2.name == "Level")
                        {
                            levelCard = child2.GetComponent<Text>();
                        }
                    }
                    imageFonLevelCard = child.GetComponent<Image>();
                }
                else if (child.name == "buttonAlpha")
                {
                    foreach (Transform child2 in child)
                    {
                        if (child2.name == "Text")
                        {
                            textIndexButtonAlpha = child2.GetComponent<Text>();
                        }
                    }
                }
                else if (child.name == "IncomeRsc")
                {
                    foreach (Transform child2 in child)
                    {
                        if (child2.name == "IncomeText")
                        {
                            Income = child2.GetComponent<Text>();
                        }
                    }
                }
                /*else if (child.name == "RscTooltip")
                {
                    foreach (Transform child2 in child)
                    {
                        if (child2.name == "PriceUpgradeText")
                        {
                            priceUpgrade = child2.GetComponent<Text>();
                        }
                    }
                    tooltipRscUpgrade = child.gameObject;
                }*/
                else if (child.name == "levelStar")
                {
                    foreach (Transform child2 in child)
                    {
                        if (child2.name == "Star1")
                        {
                            Star[0] = child2.gameObject;
                        }
                        else if (child2.name == "Star2")
                        {
                            Star[1] = child2.gameObject;
                        }
                        else if (child2.name == "Star3")
                        {
                            Star[2] = child2.gameObject;
                        }
                        else if (child2.name == "Star4")
                        {
                            Star[3] = child2.gameObject;
                        }
                    }
                }
            }
        }

        public UnityButton Clone(string name, Vector3 posOffset)
        {
            UnityButton newBut = new UnityButton();
            newBut.rootObj = (GameObject)MonoBehaviour.Instantiate(rootObj);
            newBut.rootObj.name = name;//=="" ? srcObj.name+"(Clone)" : name;
            newBut.Init();
            newBut.rootT.SetParent(rootT.parent);
            newBut.rootT.localPosition = rootT.localPosition + posOffset;
            newBut.rootT.localScale = rootT.localScale;
            newBut.rootT.localRotation = rootT.localRotation;

            return newBut;
        }
    }



    [System.Serializable]
    public class UnitOverlay : UIObject
    {
        public GameObject objBar;
        public Slider barHP;
        public Slider barShield;
        public Image imageLevelHero;
        public GameObject objLevelHero;
        public GameObject EffectNeedUpdate;
        public Image[] imageEffects;
        public GameObject Fire, Stun, Slow, Poison, Bleeding, Shock, MagicShock, Confuse, Weakness, Vulnerability, Doom, Frostbite, 
            DownDmgDebuff, AddCostDebaff, AddManaDebuff, AddSpeedBuff, CooldownBuff, UpDmgBuff;


        public void Init()
        {
            rootT = rootObj.transform;

            foreach (Transform child in rootT)
            {
                if (child.name == "Bar")
                {
                    objBar = child.gameObject;
                    foreach (Transform child2 in child)
                    {
                        if (child2.name == "ShieldBar")
                        {
                            barShield = child2.GetComponent<Slider>();
                        }
                        else if (child2.name == "HPBar")
                        {
                            barHP = child2.GetComponent<Slider>();
                        }
                    }
                }
                else if (child.name == "EffectUpdateImage")
                {
                    EffectNeedUpdate = child.gameObject;
                    EffectNeedUpdate.SetActive(false);
                }
                else if (child.name == "ImgLvlHero")
                {
                    imageLevelHero = child.GetComponent<Image>();
                    objLevelHero = child.gameObject;
                    imageLevelHero.enabled = false;
                }
                else if (child.name == "PanelEffect")
                {
                    imageEffects = new Image[child.childCount];
                    int index = 0;
                    foreach (Transform child2 in child)
                    {
                        imageEffects[index] = child2.GetComponent<Image>();
                        index++;

                        if (child2.name == "Fire")
                        {
                            Fire = child2.gameObject;
                        }
                        else if (child2.name == "Stun")
                        {
                            Stun = child2.gameObject;
                        }
                        else if (child2.name == "Slow")
                        {
                            Slow = child2.gameObject;
                        }
                        else if (child2.name == "Poison")
                        {
                            Poison = child2.gameObject;
                        }
                        else if (child2.name == "Bleeding")
                        {
                            Bleeding = child2.gameObject;
                        }
                        else if (child2.name == "Shock")
                        {
                            Shock = child2.gameObject;
                        }
                        else if (child2.name == "MagicShock")
                        {
                            MagicShock = child2.gameObject;
                        }
                        else if (child2.name == "Confuse")
                        {
                            Confuse = child2.gameObject;
                        }
                        else if (child2.name == "Weakness")
                        {
                            Weakness = child2.gameObject;
                        }
                        else if (child2.name == "Vulnerability")
                        {
                            Vulnerability = child2.gameObject;
                        }
                        else if (child2.name == "Doom")
                        {
                            Doom = child2.gameObject;
                        }
                        else if (child2.name == "Frostbite")
                        {
                            Frostbite = child2.gameObject;
                        }
                        else if (child2.name == "DownDmgDebuff")
                        {
                            DownDmgDebuff = child2.gameObject;
                        }
                        else if (child2.name == "AddCostDebuff")
                        {
                            AddCostDebaff = child2.gameObject;
                        }
                        else if (child2.name == "AddManaDebuff")
                        {
                            AddManaDebuff = child2.gameObject;
                        }
                        else if (child2.name == "AddSpeedBuff")
                        {
                            AddSpeedBuff = child2.gameObject;
                        }
                        else if (child2.name == "CooldownBuff")
                        {
                            CooldownBuff = child2.gameObject;
                        }
                        else if (child2.name == "UpDmgBuff")
                        {
                            UpDmgBuff = child2.gameObject;
                        }
                    }
                }
                    
            }
        }

        public void ClearEffectImages()
        {
            for (int i = 0; i < imageEffects.Length; i++)
            {
                if(imageEffects[i].gameObject.activeSelf)
                    imageEffects[i].gameObject.SetActive(false);
            }
        }

        public UnitOverlay Clone(string name = "")
        {
            UnitOverlay newOverlay = new UnitOverlay();
            newOverlay.rootObj = (GameObject)MonoBehaviour.Instantiate(rootObj);
            newOverlay.rootObj.name = name == "" ? rootObj.name + "(Clone)" : name;
            newOverlay.Init();

            newOverlay.rootT.SetParent(rootT.parent);
            newOverlay.rootT.localScale = rootT.localScale;

            return newOverlay;
        }
    }



    //~ public class Tween : MonoBehaviour{
    //~ public static void Pos(GameObject obj, float duration, Vector3 targetPos){

    //~ }
    //~ }

}