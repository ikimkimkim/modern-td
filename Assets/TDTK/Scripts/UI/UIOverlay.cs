﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

using TDTK;
using SystemUpdate;

namespace TDTK
{


    public class TextOverlay
    {
        public delegate void TextOverlayHandler(TextOverlay textO);
        public static event TextOverlayHandler onTextOverlayE;

        public Vector3 pos;
        public string msg;
        public Color color;
        public bool useColor = false;
        public bool ShowCry = false;
        public TextOverlay(Vector3 p, string m)
        {
            float rand = .25f;
            Vector3 posR = new Vector3(Random.Range(-rand, rand), Random.Range(-rand, rand), Random.Range(-rand, rand));
            pos = p + posR;
            msg = m;
            if (onTextOverlayE != null)
                onTextOverlayE(this); //ДОЛЖНО БЫТЬ АКТИВНО
        }
        public TextOverlay(Vector3 p, string m, Color col, bool showCry)
        {
            //float rand=.25f;
            //	Vector3 posR=new Vector3(Random.Range(-rand, rand), Random.Range(-rand, rand), Random.Range(-rand, rand));
            pos = p; //+posR;
            msg = m;
            color = col;
            useColor = true;
            ShowCry = showCry;
            if (onTextOverlayE != null)
                onTextOverlayE(this);//ДОЛЖНО БЫТЬ АКТИВНО
        }
    }



    public class UIOverlay : MonoBehaviour, IItemUpdate
    {

        private Camera mainCam;

        public Sprite iconBuff, iconDebuff;

        public void OnRefreshMainCamera() { mainCam = Camera.main; }

        public List<Slider> buildingBarList = new List<Slider>();
        public Sprite[] spriteLevelHero;
        public List<UnitOverlay> overlayList = new List<UnitOverlay>();
        public List<Text> textOverlayList = new List<Text>();

        public List<Unit> overlayedUnitList = new List<Unit>();

        [SerializeField] private List<UIUnitHealthProgressBar> healthProgressBars = new List<UIUnitHealthProgressBar>();

        public static void NewUnit(Unit unit,float damag=0)
        {
            if (unit.isShowHPBar)
                instance.StartCoroutine(instance._UnitOverlay(unit));
        }

        public Dictionary<Unit, UnitOverlay> overlaysUnit = new Dictionary<Unit, UnitOverlay>();

        IEnumerator _UnitOverlay(Unit unit)
        {
            //if (overlayedUnitList.Contains(unit)) yield break;

           // overlayedUnitList.Add(unit);

            if (overlaysUnit.ContainsKey(unit))
                yield break;

            UnitOverlay overlay = GetUnusedOverlay();
            overlay.rootObj.SetActive(true);

            overlaysUnit.Add(unit, overlay);

            if (overlay.imageLevelHero.enabled)
                overlay.imageLevelHero.enabled = false;
            if (overlay.EffectNeedUpdate.activeSelf)
                overlay.EffectNeedUpdate.SetActive(false);


            Vector3 screenPos;
            bool action, deff = false;
            float timeLastEffectUpdate = 0;
            float timeEffectUpdate = 0.2f;
            float shield;


            while (unit != null && !unit.dead && unit.thisObj.activeInHierarchy)
            {
                overlay.barHP.value = unit.HP / unit.fullHP;
                action = unit.IsHero || 
                    overlay.barHP.value != 1 ||
                    unit.shield > 0 ||
                    deff || 
                    (unit.EffectsManager!=null && unit.EffectsManager.isActiveEffect());
                if(action!= overlay.objBar.activeSelf)
                    overlay.objBar.SetActive(action);



                if (action == false)
                {
                    yield return new WaitForSeconds(0.2f);
                    continue;
                }
                else if(unit.IsTower==false)
                {
                    deff = true;
                }

                if(unit.isShowShieldBar)
                {
                    shield = unit.shield;
                    overlay.barShield.gameObject.SetActive(shield > 0);
                    if (unit.shield > 0)
                        overlay.barShield.value = shield / unit.fullShield;
                }
                else if(overlay.barShield.gameObject.activeSelf)
                {
                    overlay.barShield.gameObject.SetActive(false);
                }



                screenPos = mainCam.WorldToScreenPoint(unit.thisT.position);
                overlay.rootT.localPosition = (screenPos + Vector3.up*30) / UI.GetScaleFactor();


                if(Time.time - timeLastEffectUpdate<timeEffectUpdate)
                {
                    yield return null;
                    continue;
                }
                timeLastEffectUpdate = Time.time;


                if (unit.IsHero)
                {
                    action = unit.currentActiveStat > 0;
                    if (action != overlay.imageLevelHero.enabled)
                        overlay.imageLevelHero.enabled = action;
                    if (action)
                    {
                        overlay.imageLevelHero.sprite = spriteLevelHero[unit.GetUnitHero.currentActiveStat - 1];
                    }

                    if (unit.GetUnitHero.isNeedUpdate != overlay.EffectNeedUpdate.activeSelf)
                        overlay.EffectNeedUpdate.SetActive(unit.GetUnitHero.isNeedUpdate);
                }

                yield return null;
            }

            overlay.rootObj.SetActive(false);
            overlaysUnit[unit].ClearEffectImages();
            overlaysUnit.Remove(unit);

            //overlayedUnitList.Remove(unit);
        }



        public static UIOverlay instance;

        void Awake()
        {
            instance = this;

            for (int i = 0; i < 3; i++)
            {
                if (i > 0)
                {
                    GameObject obj = (GameObject)Instantiate(buildingBarList[0].gameObject);
                    buildingBarList.Add(obj.GetComponent<Slider>());
                    buildingBarList[i].transform.SetParent(buildingBarList[0].transform.parent);
                    obj.transform.localScale = buildingBarList[0].transform.localScale;
                }
                buildingBarList[i].gameObject.SetActive(false);
            }

            for (int i = 0; i < 10; i++)
            {
                if (i > 0)
                {
                    GameObject obj = (GameObject)Instantiate(textOverlayList[0].gameObject);
                    textOverlayList.Add(obj.GetComponent<Text>());
                    textOverlayList[i].transform.SetParent(textOverlayList[0].transform.parent);
                }
                textOverlayList[i].text = "";
                textOverlayList[i].gameObject.SetActive(false);
            }

            for (int i = 0; i < 15; i++)
            {
                if (i == 0) overlayList[i].Init();
                else overlayList.Add(overlayList[0].Clone());
                overlayList[i].rootObj.SetActive(false);
            }

            mainCam = Camera.main;

            if (textOverlaysShow == null)
                textOverlaysShow = new List<Text>();

            ClearHealthBar();
        }


        void OnEnable()
        {
            MainSystem.AddItem(this);

            //Unit.onDamagedE += NewUnit;
            UnitTower.onConstructionStartE += Building;
            TextOverlay.onTextOverlayE += OnTextOverlay;
            FPSControl.onFPSCameraE += OnRefreshMainCamera;
            Unit.onInitE += Unit_onInitE;
            //UIHeroSpawn.onSpawnHero += UIHeroSpawn_onSpawnHero;
           // EffectManager.addEffectE += AddEffect;
           // EffectManager.removeEffectE += RemoveEffect;
           // SpawnManager.onSpawnUnitE += SpawnManager_onSpawnUnitE;
        }

        private void Unit_onInitE(Unit unit)
        {
            AddNewUnitBar(unit);
        }

        private void UIHeroSpawn_onSpawnHero(UnitHero unit)
        {
            AddNewUnitBar(unit);
        }

        private void SpawnManager_onSpawnUnitE(Unit unit)
        {
            AddNewUnitBar(unit);
        }

        private void AddNewUnitBar(Unit unit)
        {
            if (unit.isShowHPBar == false)
                return;
            var freeBar = GetFreeHealthProgressBar();
            freeBar.AttachUnit(unit, mainCam);

        }

        private UIUnitHealthProgressBar GetFreeHealthProgressBar()
        {
            for (int i = 0; i < healthProgressBars.Count; i++)
            {
                if (healthProgressBars[i].isFree)
                    return healthProgressBars[i];
            }
            GameObject obj = Instantiate(healthProgressBars[0].gameObject, healthProgressBars[0].transform.parent);
            var newBar = obj.GetComponent<UIUnitHealthProgressBar>();
            healthProgressBars.Add(newBar);
            return newBar;
        }

        private void ClearHealthBar()
        {
            for (int i = 0; i < healthProgressBars.Count; i++)
            {
                healthProgressBars[i].DetachUnit();
            }
        }

        private void RemoveEffect(Unit unit, IDEffect typeEffect)
        {
            if(overlaysUnit.ContainsKey(unit))
            {
                //Debug.LogWarning("RemoveEffect:" + typeEffect);
                int index = (int)typeEffect;
                overlaysUnit[unit].imageEffects[index].gameObject.SetActive(false);
            }
            /*else
               Debug.LogError("RemoveEffect:" + typeEffect);*/
        }

        private void AddEffect(Unit unit, IDEffect typeEffect)
        {
            if (overlaysUnit.ContainsKey(unit))
            {
               // Debug.LogWarning("AddEffect:" + typeEffect);
                int index = (int)typeEffect;
                overlaysUnit[unit].imageEffects[index].gameObject.SetActive(true);

                if (unit.EffectsManager.Stacks[index].isBuff)
                    overlaysUnit[unit].imageEffects[index].sprite = iconBuff;
                else
                    overlaysUnit[unit].imageEffects[index].sprite = iconDebuff;

                overlaysUnit[unit].imageEffects[index].color = unit.EffectsManager.Stacks[index].getColor;
            }
            else
            {
                //Debug.LogError("AddEffect:" + typeEffect);

                if (unit.isShowHPBar)
                {
                    StartCoroutine(_UnitOverlay(unit));
                    AddEffect(unit, typeEffect);
                }
            }
        }

        void OnDisable()
        {
            MainSystem.RemoveItem(this);

            Unit.onInitE -= Unit_onInitE;
            // Unit.onDamagedE -= NewUnit;

            UnitTower.onConstructionStartE -= Building;

            TextOverlay.onTextOverlayE -= OnTextOverlay;

            FPSControl.onFPSCameraE -= OnRefreshMainCamera;

            //EffectManager.addEffectE -= AddEffect;

           // EffectManager.removeEffectE -= RemoveEffect;

           // SpawnManager.onSpawnUnitE -= SpawnManager_onSpawnUnitE;
           // UIHeroSpawn.onSpawnHero -= UIHeroSpawn_onSpawnHero;

            overlaysUnit.Clear();
        }

        List<Text> textOverlaysShow;
        public bool isTextUpdateSystem = false;

        void OnTextOverlay(TextOverlay overlayInstance)
        {
            if (UI.DisableTextOverlay()) return;

            Text txt = GetUnusedTextOverlay();

            textOverlaysShow.Add(txt);

            txt.text = overlayInstance.msg;
            if (overlayInstance.useColor)
                txt.color = overlayInstance.color;
            else txt.color = new Color(1f, 150 / 255f, 0, 1f);

            Vector3 screenPos = mainCam.WorldToScreenPoint(overlayInstance.pos);
            if (overlayInstance.ShowCry)
            {
                screenPos += new Vector3(25f, 0, 0);
            }
            txt.transform.localPosition = screenPos / UI.GetScaleFactor();
            txt.transform.localScale = new Vector3(1.3f, 1.3f);
            txt.gameObject.SetActive(true);

          /*  if (txt.GetComponentInChildren<Image>() != null)
            {
                txt.GetComponentInChildren<Image>().enabled = overlayInstance.ShowCry;
            }
            else
            {
                Debug.Log("OVERLAY WITHOUT IMAGE FOUND!");
            }*/
           
            if(isTextUpdateSystem==false)
                StartCoroutine(TextOverlayRoutine(txt));
        }

        private bool UpdateText(Text txt)
        {
            txt.transform.localPosition += new Vector3(0, 30 * Time.deltaTime, 0);
            Color color = txt.color;
            color.a -=  0.3f * Time.deltaTime;
            txt.color = color;
            if (color.a <= 0)
            {
                textOverlaysShow.Remove(txt);
                txt.text = "";
                txt.gameObject.SetActive(false);
                return true;
            }
            else
                return false;
        }

        IEnumerator TextOverlayRoutine(Text txt)
        {
            Transform txtT = txt.transform;

            Image crystal = txt.GetComponentInChildren<Image>();
            if ( crystal != null && !crystal.gameObject.activeInHierarchy)
            {
                crystal = null;
            }
            float duration = 0;
            while (duration < 2f)
            {
                txtT.localPosition += new Vector3(0, 30 * Time.deltaTime, 0);
                Color color = txt.color;
                color.a = 1f - 0.2f * duration;
                txt.color = color;

                if (crystal != null)
                {
                    Color imageColor = crystal.color;
                    imageColor.a = 1f - 0.2f * duration;
                    crystal.color = imageColor;
                }
                duration += Time.deltaTime * 1.5f;
                yield return null;
            }
            txt.text="";
           
            txt.gameObject.SetActive(false);
            
        }



        public static void Building(UnitTower tower) { instance.StartCoroutine(instance._Building(tower)); }
        IEnumerator _Building(UnitTower tower)
        {
            Slider bar = GetUnusedBuildingBar();
            Transform barT = bar.transform;
            while (tower != null && tower.IsInConstruction())
            {
                bar.value = tower.GetBuildProgress();

                if (mainCam == null)
                {
                    mainCam = Camera.main;
                    continue;
                }

                Vector3 screenPos = mainCam.WorldToScreenPoint(tower.thisT.position + new Vector3(0, 0, 0));
                barT.localPosition = (screenPos + new Vector3(0, -20, 0)) / UI.GetScaleFactor();
                bar.gameObject.SetActive(true);

                yield return null;
            }
            bar.gameObject.SetActive(false);
        }

        Slider GetUnusedBuildingBar()
        {
            //    Debug.Log("GetUnusedBuildingBar");
            for (int i = 0; i < buildingBarList.Count; i++)
            {
                if (!buildingBarList[i].gameObject.activeInHierarchy) return buildingBarList[i];
            }
            GameObject obj = (GameObject)Instantiate(buildingBarList[0].gameObject);
            obj.transform.parent = buildingBarList[0].transform.parent;
            obj.transform.localScale = buildingBarList[0].transform.localScale;
            Slider slider = obj.GetComponent<Slider>();
            buildingBarList.Add(slider);
            return slider;
        }

        UnitOverlay GetUnusedOverlay()
        {
            for (int i = 0; i < overlayList.Count; i++)
            {
                if (!overlayList[i].rootObj.activeInHierarchy)
                    return overlayList[i];
            }
            UnitOverlay overlay = overlayList[0].Clone();
            overlayList.Add(overlay);
            return overlay;
        }

        Text GetUnusedTextOverlay()
        {
            for (int i = 0; i < textOverlayList.Count; i++)
            {
                if (textOverlayList[i].text == "") return textOverlayList[i];
            }

            GameObject obj = (GameObject)Instantiate(textOverlayList[0].gameObject);
            obj.transform.SetParent(textOverlayList[0].transform.parent);
            obj.transform.localScale = textOverlayList[0].transform.localScale;
            Text txt = obj.GetComponent<Text>();
            textOverlayList.Add(txt);
            return txt;
        }

        public void SystemUpdate()
        {
            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                for (int i = 0; i < 500; i++)
                {
                    new TextOverlay(Vector3.left * i-Vector3.left*50, i.ToString(), new Color(1f, 1f, 1f, 1f), false);
                }
            }
            if (isTextUpdateSystem)
            {
                foreach (var item in textOverlaysShow)
                {
                    if (UpdateText(item))
                        break;
                }
            }
        }


        // Update is called once per frame
        //~ void Update () {
        //~ if(Input.GetMouseButtonDown(0)){
        //~ Vector3 pos=new Vector3(Random.Range(-5, 5), Random.Range(-5, 5), Random.Range(-5, 5));
        //~ new TextOverlay(pos, Random.Range(1, 999).ToString(), Color.white);
        //~ }
        //~ }


    }

}