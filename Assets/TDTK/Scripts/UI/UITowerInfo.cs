﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;
using Translation;
using TDTK;
using System;

namespace TDTK
{

    public class UITowerInfo : MonoBehaviour
    {

        public UnitTower currentTower;

        public static UITowerInfo instance;
        private GameObject thisObj;

        public Transform anchorLeft;
        public Transform anchorRight;
        public Transform frameT;

        public GameObject objTypeTargetBuild, objTypeTarget;
        public Text txtName;
        public Text InfoDesp, Desp;
        public Text InfoDamage, Damage;
        public Text AttackSpeed;
        public GameObject objDamage;
        public GameObject objDMtype;
        //private GameObject rscObj;
        public List<UnityButton> rscObjList = new List<UnityButton>();

        public GameObject floatingButtons;
        public GameObject butUpgrade;

        public Button butRepair;

        public GameObject TooltipNextLevel;
        public Text TextTooltipNextLevel;

        public GameObject butSell;

        public GameObject butUpgradeAlt1;
        public GameObject butUpgradeAlt2;
        public Image imageUpgrade;
        public Image imageUpgradeAlt;
        public Image imageSell;
        private int upgradeOption = 1;	//a number recording the upgrade option available for the tower, update everytime tower.ReadyToBeUpgrade() is called
        //0 - not upgradable
        //1 - can be upgraded
        //2 - 2 upgrade path, shows 2 upgrade buttons

        public GameObject rscTooltipObj;
        public List<UnityButton> rscTooltipObjList = new List<UnityButton>();

        public GameObject controlObj;
        public GameObject priorityObj;
        public Text txtTargetPriority;

        public GameObject directionObj;
        public Slider sliderDirection;

        public GameObject fpsButtonObj;

        public GameObject AncherBottomLeft;


        public Sprite ApplyIcon;
        public Sprite SellIcon;
        public GameObject Selector;

        public GameObject ArrowUp;
        public GameObject ArrowDown;
        public GameObject ArrowLeft;
        public GameObject ArrowRight;
        Vector3 _arrowUpStartPos;
        Vector3 _arrowDownStartPos;
        Vector3 _arrowLeftStartPos;
        Vector3 _arrowRightStartPos;

        //bool UpgareButtonWasPressed = false;
        //bool UpgareButtonAltWasPressed = false;
        //bool SellButtonWasPressed = false;
        public GameObject RscObj;
        float Scale;

        public GameObject DpsSmallObj;
        public Text InfoTypeDamageSmall, txtTypeDamageSmall, InfoDpsSmall, txtDps;

        public static Action OnShow;
        public static Action OnHide;

        [Header("Tutorial")]
        public GameObject UpgradeCurcor;

        void Start()
        {
            instance = this;
            thisObj = gameObject;

            //for build cost
            Sprite[] rscList = ResourceManager.GetResourceIconArray();
            for (int i = 0; i < rscList.Length; i++)
            {
                if (i == 0)
                {
                    rscObjList[i].Init();
                    rscObjList[i].imageIcon.sprite = rscList[i];
                }
                else
                {
                    MyLog.LogWarning("UITowerInfo Rsc count>1");
                    //rscObjList.Add(rscObjList[0].Clone("RscObj" + i, new Vector3(i * 80, 0, 0)));
                }
            }
            //rscObj = rscObjList[0].rootT.parent.gameObject;


            //for upgrade/selling cost
            for (int i = 0; i < rscList.Length; i++)
            {
                if (i == 0 || i==1)
                {
                    rscTooltipObjList[i].Init();
                    rscTooltipObjList[i].imageIcon.sprite = rscList[i];
                }
                else
                {
                    MyLog.LogWarning("UITowerInfo Rsc count>1");
                    //rscTooltipObjList.Add(rscTooltipObjList[0].Clone("RscObj" + i, new Vector3(i * 50, 0, 0)));
                }
            }
            //float offset = 0.5f * (rscList.Count - 1) * 50;
            //for (int i = 0; i < rscList.Count; i++) rscTooltipObjList[i].rootT.localPosition += new Vector3(-offset, 0, 0);
            rscTooltipObj.GetComponent<RectTransform>().sizeDelta += new Vector2((rscList.Length - 1) * 50, 0);

            rscTooltipObj.SetActive(false);

            // Hide();
            HideInfoPanel();
            HideButtons();
            InitArrows();
        }

        void OnEnable()
        {
            GameControl.onGameOverE += OnGameOver;
            UnitTower.onConstructionCompleteE += OnConstructionComplete;

            Unit.onDestroyedE += OnUnitDestroyed;
        }
        void OnDisable()
        {
            GameControl.onGameOverE -= OnGameOver;
            UnitTower.onConstructionCompleteE -= OnConstructionComplete;

            Unit.onDestroyedE -= OnUnitDestroyed;
        }

        void OnGameOver(int stars, ResponseData data) { Hide(); }

        //при заканчивании апгрейда нужно сменить информацию о таувере на актуальную в нижней панеле 
        void OnTowerUpgraded(UnitTower tower)
        {

            //Show(tower, true);
            //TowerInfoSmall.Show(tower);
            ShowButtons(tower);
            HideSelector();
        }

        void OnConstructionComplete(UnitTower tower)
        {
            
            if (tower != currentTower) return;

            //upgradeOption=tower.ReadyToBeUpgrade();
            //if(upgradeOption>0) butUpgrade.SetActive(true);
            //else butUpgrade.SetActive(false);

            //TimeScaleManager.SetTimeScale(0.5f);

            upgradeOption = currentTower.ReadyToBeUpgrade();

            if (upgradeOption > 0)
            {
                butUpgrade.SetActive(true);
                butUpgrade.GetComponent<Button>().interactable = true;
                if (upgradeOption > 1) butUpgradeAlt1.SetActive(true);
                else butUpgradeAlt1.SetActive(false);
            }
            else
            {
                butUpgrade.SetActive(true);
                butUpgrade.GetComponent<Button>().interactable = false;
                butUpgradeAlt1.SetActive(false);
            }
            HideSelector();
            //butUpgradeAlt1.SetActive(false);
            //butUpgradeAlt2.SetActive(false);
        }

        void OnUnitDestroyed(Unit unit)
        {
            if (currentTower == null) return;
            if (unit.IsTower == false || unit.GetUnitTower != currentTower)
                return;
            //Hide();
            HideButtons();
            HideInfoPanel();

        }

        // Update is called once per frame
        void Update()
        {
            if (!isOn) return;

            Vector3 screenPos = Camera.main.WorldToScreenPoint(currentTower.thisT.position);
            floatingButtons.transform.localPosition = screenPos / UI.GetScaleFactor();

            //force the frame to go left when the tower is off screen (specifically for dragNdrop button hover)
            if (currentTower.IsSampleTower)
            {
                if (screenPos.x < 0 || screenPos.x > Screen.width || screenPos.y < 0 || screenPos.y > Screen.height)
                {
                    screenPos.x = Screen.width;
                    //currentX = 0;
                }
            }

            ////Смена позиции информации при движении экрана или при первом нажатии
            //if (currentX < Screen.width / 2 && screenPos.x > Screen.width / 2) _SetScreenPos(screenPos);
            //else if (currentX > Screen.width / 2 && screenPos.x < Screen.width / 2) _SetScreenPos(screenPos);

        }



        public void OnUpgradeButton()
        {
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
            UpgradeTower(); 
#endif
#if UNITY_IOS || UNITY_ANDROID
            if (UpgareButtonWasPressed)
            {
                UpgradeTower();
            }
            else
            {
                RedBlinking.StopBlinking(gameObject);
                if (UpgareButtonAltWasPressed)
                {
                    UpgareButtonAltWasPressed = false;
                    SetNormalSpriteAndResetSize(imageUpgradeAlt, currentTower.nextLevelTowerList[1].iconSprite);
                }
                if (SellButtonWasPressed)
                {
                    SellButtonWasPressed = false;
                    SetNormalSpriteAndResetSizeForSell(imageSell);
                    rscTooltipObj.SetActive(false);
                }
                UpgareButtonWasPressed = true;
                SetApplaySprite(imageUpgrade);
                ShowSelector(butUpgrade);
                if (currentTower.nextLevelTowerList != null && currentTower.nextLevelTowerList[0] != null)
                {
                    _showInfoPanel(BuildManager.GetSampleTower(currentTower.nextLevelTowerList[0]), true, butUpgrade);
                    sampleTowerID = currentTower.nextLevelTowerList[0].prefabID;
                  //  Debug.Log(sampleTowerID);
                    ShowSampleTower(currentTower.nextLevelTowerList[0].prefabID, currentTower);
                }
            }
#endif

        }
        public void OnUpgradeButtonAlt()
        {
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
            UpgradeTower(1); 
#endif
#if UNITY_IOS || UNITY_ANDROID
            if (UpgareButtonAltWasPressed)
            {
                UpgradeTower(1);
            }
            else
            {
                RedBlinking.StopBlinking(gameObject);
                if (UpgareButtonWasPressed)
                {
                    UpgareButtonWasPressed = false;
                    SetNormalSpriteAndResetSize(imageUpgrade, currentTower.nextLevelTowerList[0].iconSprite);
                }
                if (SellButtonWasPressed)
                {
                    SellButtonWasPressed = false;
                    SetNormalSpriteAndResetSizeForSell(imageSell);
                    rscTooltipObj.SetActive(false);
                }
                UpgareButtonAltWasPressed = true;
                SetApplaySprite(imageUpgradeAlt);
                ShowSelector(butUpgradeAlt1);

                if (currentTower.nextLevelTowerList != null && currentTower.nextLevelTowerList.Count > 1 && currentTower.nextLevelTowerList[1] != null)
                {
                    _showInfoPanel(BuildManager.GetSampleTower(currentTower.nextLevelTowerList[1]), true, butUpgradeAlt1);
                    sampleTowerID = currentTower.nextLevelTowerList[1].prefabID;
                    Debug.Log(sampleTowerID);
                    ShowSampleTower(currentTower.nextLevelTowerList[1].prefabID, currentTower);
                }
            }
#endif
        }
        public string UpgradeTower(int upgradePath = 0)
        {
            if (sampleTowerID == -1)
                Debug.Log("SampleID = -1 smt wrong!");// TODO проверить
            
            HideSampleTower();
            if(currentTower.MyCard.NextLevelTower==null)
            {
                Debug.LogWarning("Card not have next level data!");
                return "Not have level data!";
            }
            Card nextLevelCard = currentTower.MyCard.NextLevelTower[Mathf.Clamp(0, 0, currentTower.nextLevelTowerList.Count)];

            List<float> cost = nextLevelCard.GetCost();
            int suffCost = ResourceManager.HasSufficientResource(cost);

            if (suffCost == -1)
            {
                TowerSelectProperties.initShow(nextLevelCard, StartUpdateTower);
                return "";
            }
            else
            {
                RedBlinking.StartBlinking(RscObj);
                RedBlinking.StartBlinking(gameObject);
                return "Insufficient Resource";
            }

            /*

                string exception = currentTower.Upgrade(upgradePath);
            if (exception == "")
            {
#if UNITY_ANDROID || UNITY_IOS
                HideInfoPanel();
                if (upgradePath == 1)
                {
                    UpgareButtonAltWasPressed = false;
                    if (currentTower.nextLevelTowerList != null && currentTower.nextLevelTowerList.Count > 1 && currentTower.nextLevelTowerList[1] != null)
                        SetNormalSpriteAndResetSize(imageUpgradeAlt, currentTower.nextLevelTowerList[1].iconSprite);
                }
                else
                {
                    UpgareButtonWasPressed = false;
                    if (currentTower.nextLevelTowerList != null && currentTower.nextLevelTowerList[0] != null)
                        SetNormalSpriteAndResetSize(imageUpgrade, currentTower.nextLevelTowerList[0].iconSprite);
                }
                HideSelector();
#endif
                upgradeOption = 0;
                floatingButtons.SetActive(false);
                HideInfoPanel();
                UI.ClearSelectedTower();
            }
            else
            {

                RedBlinking.StartBlinking(RscObj);
                RedBlinking.StartBlinking(gameObject);
            }
            return exception;*/
        }

        public void StartUpdateTower(bool isUpgrade)
        {
            if (isUpgrade)
            {
                currentTower.StartUpgradeTower();
                upgradeOption = 0;
                floatingButtons.SetActive(false);
                HideInfoPanel();
                UI.ClearSelectedTower();
            }
        }

#if (UNITY_WEBGL || UNITY_WEBPLAYER)
        public void OnHoverUpgradeButton()
        {
            //Debug.Log("OnHoverButton cTower:" + currentTower.name);
            if (butUpgrade.GetComponent<Button>().interactable == false)
            {
                TooltipNextLevel.SetActive(true);
                if(currentTower.GetLevel()<Card.MAX_LEVEL)
                {
                    TextTooltipNextLevel.text = "Достигнуть макс. уровень башни \r\n (необходимо улучшать карточку башни)";
                }
                else
                {
                    TextTooltipNextLevel.text = "Достигнут максимальный уровень башни";
                }
                return;

            }
            ShowSelector(butUpgrade);
            if (currentTower.MyCard.NextLevelTower != null && currentTower.MyCard.NextLevelTower[0] != null)
            {
                //Debug.Log("OnHoverUpgradeButton ЗАКОМИЧЕНО!");
                ShowSampleTower(currentTower.MyCard.NextLevelTower[0].prefabID, currentTower.MyCard.NextLevelTower[0], currentTower);
                UnitTower t = GameControl.GetSelectedTower();
                float min = t.stats[0].damageMin;
                float max = t.stats[0].damageMax;
                //min = min * 1f / currentTower.MyCard.NextLevelTower[0].stats[0].cooldown;
                //max = max * 1f / currentTower.MyCard.NextLevelTower[0].stats[0].cooldown;
                min = Mathf.Round(min * 10) / 10;
                max = Mathf.Round(max*10)/10;

                InfoDpsSmall.text = "За выстрел";
                txtDps.text = min + "-" + max;//(Mathf.Round((min + max) * 5) / 10).ToString();

                UnitTower sample = BuildManager.GetSampleTower(t.MyCard);
                List<CardProperties> p = sample.Properties.FindAll(i => i.ID == 14);
                if (p.Count > 0)
                {
                    InfoTypeDamageSmall.text = "";
                    txtTypeDamageSmall.text = "";
                    DpsSmallObj.SetActive(false);

                    for (int i = 0; i < p.Count; i++)
                    {
                        if (p[i].CanActive())
                        {
                            ResourceRegen rr = (ResourceRegen)p[i].properties;
                            InfoTypeDamageSmall.text = rr.Count + " ману за";
                            txtTypeDamageSmall.text = rr.Speed + "c";
                        }
                    }
                }

                _showInfoPanel(sample, true, butUpgrade);
                sampleTowerID = t.MyCard.prefabID;



            }

        }
        public void OnHoverUpgradeButtonAlt()
        {
            ShowSelector(butUpgradeAlt1);
            if (currentTower.MyCard.NextLevelTower != null && currentTower.MyCard.NextLevelTower.Count > 1 && currentTower.MyCard.NextLevelTower[1] != null)
            {
                //Debug.Log("OnHoverUpgradeButton ЗАКОМИЧЕНО!");
                _showInfoPanel(BuildManager.GetSampleTower(currentTower.MyCard.NextLevelTower[1]),true, butUpgradeAlt1);
                sampleTowerID = currentTower.MyCard.NextLevelTower[1].prefabID;
                ShowSampleTower(currentTower.MyCard.NextLevelTower[1].prefabID, currentTower.MyCard.NextLevelTower[1], currentTower);
            }

        }
#endif
        int sampleTowerID = -1;
        void ShowSampleTower(int id,Card card , UnitTower tower)
        {
            //Debug.Log("ShowSampleTower");
            BuildManager.ShowSampleTowerImmediately(id,card,tower);
            ChangeEnabledOfCurrentTowerRenderers(false);
        }
        void ChangeEnabledOfCurrentTowerRenderers(bool enabled)
        {
            var renderers = GameControl.GetSelectedTower().gameObject.GetComponentsInChildren<MeshRenderer>();
            for (int i = 0; i < renderers.Length; i++)
            {
                if (renderers[i].gameObject.name != "Quad")
                {
                    renderers[i].enabled = enabled;
                }
            }
        }

        void HideSampleTower()
        {
            if (sampleTowerID != -1)
            {
                //Debug.Log("HideSampleTower");
                BuildManager.HideSampleTowerImmediately(sampleTowerID);
                ChangeEnabledOfCurrentTowerRenderers(true);
                sampleTowerID = -1;

                GameControl.SelectTower(currentTower);
            }
            else
            {

            }
        }

        public void OnRepairButton()
        {
            float p =1- currentTower.HP / currentTower.fullHP;
            List<float> cost = new List<float>();  
            for (int i = 0; i < currentTower.stats[0].cost.Count; i++)
            {
                cost.Add(currentTower.stats[0].cost[i] * p);
            }
            
            int suffCost = ResourceManager.HasSufficientResource(cost);
            if(suffCost==-1)
            {
                ResourceManager.SpendResource(cost);
                currentTower.AddHeal(currentTower.fullHP - currentTower.HP);
                UI.ClearSelectedTower();
            }
            else
            {
                RedBlinking.StartBlinking(RscObj);
                RedBlinking.StartBlinking(gameObject);
            }

        }

        public void OnSellButton()
        {
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
            currentTower.Sell();
            UI.ClearSelectedTower();
#endif
#if UNITY_IOS || UNITY_ANDROID
            if (SellButtonWasPressed)
            {
                currentTower.Sell();
                UI.ClearSelectedTower();
                SellButtonWasPressed = false;
                SetNormalSpriteAndResetSizeForSell(imageSell);
                HideSelector();
            }
            else
            {
                RedBlinking.StopBlinking(gameObject);
                if (UpgareButtonWasPressed)
                {
                    UpgareButtonWasPressed = false;
                    SetNormalSpriteAndResetSize(imageUpgrade, currentTower.nextLevelTowerList[0].iconSprite);
                    _hideInfoPanel();
                }
                if (UpgareButtonAltWasPressed)
                {
                    UpgareButtonAltWasPressed = false;
                    SetNormalSpriteAndResetSize(imageUpgradeAlt, currentTower.nextLevelTowerList[1].iconSprite);
                    _hideInfoPanel();
                }
                HideSampleTower();
                SellButtonWasPressed = true;
                SetApplaySprite(imageSell);
                ShowSelector(butSell);
                OnHoverUpgradeSellButton(currentTower.GetValue());
            }
#endif

        }
        void SetApplaySprite(Image image)
        {
            image.sprite = ApplyIcon;
            image.rectTransform.sizeDelta = new Vector3(35, 35);
            image.rectTransform.localPosition = Vector3.zero;
        }
        void SetNormalSpriteAndResetSize(Image image, Sprite normalSprite, bool CanUpgrage)
        {
            image.sprite = normalSprite;
            image.rectTransform.sizeDelta = new Vector3(65, 65);
            image.rectTransform.localPosition = new Vector3(0, 8, 0);
            if (CanUpgrage)
                image.color = new Color(1f, 1f, 1f, 1f);
            else
                image.color = new Color(184f / 255f, 184f / 255f, 184f / 255f, 0.75f);
        }
        void SetNormalSpriteAndResetSizeForSell(Image image)
        {
            image.sprite = SellIcon;
            image.rectTransform.sizeDelta = new Vector3(45, 45);
        }

        public void OnHoverRepairButton()
        {
            if (butRepair.interactable == false)
                return;
            ShowSelector(butRepair.gameObject);
            OnHoverUpgradeRepairButton(currentTower.stats[0].cost);
            rscTooltipObj.transform.position = butRepair.transform.position - Vector3.up * 30f;
        }
        public void OnHoverUpgradeRepairButton(List<float> cost)
        {
            if (cost.Count != rscTooltipObjList.Count) return;
            for (int i = 0; i < rscTooltipObjList.Count; i++)
            {
                rscTooltipObjList[i].label.text = (System.Math.Ceiling(cost[i]*(1-currentTower.HP/currentTower.fullHP)*10f)/10f).ToString();
            }
            rscTooltipObj.SetActive(true);
        }

        public void OnHoverSellButton()
        {
            ShowSelector(butSell);
            OnHoverUpgradeSellButton(currentTower.GetValue());
            rscTooltipObj.transform.position = butSell.transform.position - Vector3.up * 30f;
        }

        public void OnHoverUpgradeSellButton(List<float> cost)
        {
            if (cost.Count != rscTooltipObjList.Count) return;
            for (int i = 0; i < rscTooltipObjList.Count; i++)
            {
                rscTooltipObjList[i].label.text = (System.Math.Ceiling(cost[i]*10)/10f).ToString();
            }
            rscTooltipObj.SetActive(true);
        }
        //при убирании курсора с кнопки апгрейдов убирать информацию о таувере  
        public void OnExitUpgradeSellButton()
        {

            updateTextDPS(currentTower);
            TooltipNextLevel.SetActive(false);
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
            RedBlinking.StopBlinking(gameObject);
            HideSelector();
            rscTooltipObj.SetActive(false);
            _hideInfoPanel();
            HideSampleTower();
#endif
        }
        void ShowSelector(GameObject button)
        {
            Selector.SetActive(true);
            Selector.transform.localPosition = button.transform.localPosition;
        }
        void HideSelector()
        {
            Selector.SetActive(false);
        }

        public void OnTargetPrioritySwitch()
        {
            currentTower.SwitchToNextTargetPriority();
            txtTargetPriority.text = currentTower.targetPriority.ToString();
        }
        public void OnSliderDirectionChanged()
        {
            if (currentTower != null) currentTower.ChangeScanAngle((int)sliderDirection.value);
        }


        //private float currentX = 0;
        public static void SetScreenPos(Vector3 pos) { instance._SetScreenPos(pos); }
        public void _SetScreenPos(Vector3 pos)
        {
            HideArrows();

            float shiftFromPos = 50f * Scale;
            float borderMarign = 20f * Scale;
            float vertSize = 129f * Scale;
            float horSize = 162f * Scale;
            //позиция центра панели если поставить сверху, снизу, справа, слева
            var tempUp = pos + new Vector3(0, shiftFromPos + vertSize / 2, 0);
            var tempDown = pos - new Vector3(0, shiftFromPos + vertSize / 2, 0);
            var tempRight = pos + new Vector3(shiftFromPos + horSize / 2, 0, 0);
            var tempLeft = pos - new Vector3(shiftFromPos + horSize / 2, 0, 0);
            Vector3 tempPosition = Vector3.zero;
            //расстояние до границы при установке панели в соответствующюю позицию
            float distUp = Screen.height - tempUp.y - vertSize / 2;
            float distDown = tempDown.y - vertSize / 2;
            float distRight = Screen.width - tempRight.x - horSize / 2;
            float distLeft = tempLeft.x - horSize / 2;

            //вначале пытаемся поставить сверху 
            if (distUp > borderMarign)
            {
                tempPosition = tempUp;

                float shift = 0;
                distRight = Screen.width - tempPosition.x - horSize / 2;
                //   Debug.Log("distRight " + distRight);
                if (distRight < borderMarign)
                {
                    shift = tempPosition.x - (Screen.width - borderMarign - horSize / 2);
                    tempPosition = new Vector3(Screen.width - borderMarign - horSize / 2, tempPosition.y, tempPosition.z);
                }

                distLeft = tempPosition.x - horSize / 2;
                //   Debug.Log("distLeft " + distLeft);
                if (distLeft < borderMarign)
                {
                    shift = tempPosition.x - (borderMarign + horSize / 2);
                    tempPosition = new Vector3(borderMarign + horSize / 2, tempPosition.y, tempPosition.z);
                }

                // Debug.Log(shift);
                frameT.position = pos;
                ArrowDown.transform.localPosition += new Vector3(Mathf.Sign(shift) * Mathf.Min(Mathf.Abs(shift), horSize / 2 - 16), 0, 0);
                //ArrowDown.SetActive(true);

            }
            else
            {
                //если не получилось сверху, то ставим со стороны противоположной стороне с минимальным расстоянием до границы
                List<float> dist = new List<float>();
                dist.Add(distUp);
                dist.Add(distRight);
                dist.Add(distLeft);
                float min = distDown;
                for (int i = 0; i < dist.Count; i++)
                {
                    if (dist[i] <= min)
                    {
                        min = dist[i];
                    }
                }
                if (distUp == min)
                {
                    tempPosition = tempDown;
                    frameT.position = pos;
                    //ArrowUp.SetActive(true);

                }
                if (distLeft == min)
                {
                    tempPosition = tempRight;
                    float shift = 0;
                    distUp = Screen.height - tempPosition.y - vertSize / 2;

                    //  Debug.Log("distUp " + distUp);
                    if (distUp < borderMarign)
                    {
                        shift = tempPosition.y - (Screen.height - borderMarign - vertSize / 2);
                        tempPosition = new Vector3(tempPosition.x, Screen.height - borderMarign - vertSize / 2, tempPosition.z);
                    }

                    distDown = tempPosition.y - vertSize / 2;
                    // Debug.Log("distDown " + distDown);
                    if (distDown < borderMarign)
                    {
                        shift = tempPosition.y - (borderMarign + vertSize / 2);
                        tempPosition = new Vector3(tempPosition.x, borderMarign + vertSize / 2, tempPosition.z);
                    }

                    //     Debug.Log(shift);

                    frameT.position = pos;
                    ArrowLeft.transform.localPosition += new Vector3(0, Mathf.Sign(shift) * Mathf.Min(Mathf.Abs(shift), vertSize / 2 - 16), 0);

                    //ArrowLeft.SetActive(true);

                }
                if (distRight == min)
                {
                    tempPosition = tempLeft;
                    float shift = 0;
                    distUp = Screen.height - tempPosition.y - vertSize / 2;

                    //  Debug.Log("distUp " + distUp);
                    if (distUp < borderMarign)
                    {
                        shift = tempPosition.y - (Screen.height - borderMarign - vertSize / 2);
                        tempPosition = new Vector3(tempPosition.x, Screen.height - borderMarign - vertSize / 2, tempPosition.z);
                    }

                    distDown = tempPosition.y - vertSize / 2;
                    //  Debug.Log("distDown " + distDown);
                    if (distDown < borderMarign)
                    {
                        shift = tempPosition.y - (borderMarign + vertSize / 2);
                        tempPosition = new Vector3(tempPosition.x, borderMarign + vertSize / 2, tempPosition.z);
                    }

                    //Debug.Log(shift);

                    frameT.position = pos;
                    ArrowRight.transform.localPosition += new Vector3(0, Mathf.Sign(shift) * Mathf.Min(Mathf.Abs(shift), vertSize / 2 - 16), 0);
                    //ArrowRight.SetActive(true);

                }

            }
        }

        void HideArrows()
        {
            ArrowDown.SetActive(false);
            ArrowUp.SetActive(false);
            ArrowLeft.SetActive(false);
            ArrowRight.SetActive(false);

            ArrowDown.transform.localPosition = _arrowDownStartPos;
            ArrowUp.transform.localPosition = _arrowUpStartPos;
            ArrowLeft.transform.localPosition = _arrowLeftStartPos;
            ArrowRight.transform.localPosition = _arrowRightStartPos;
        }
        void InitArrows()
        {
            _arrowDownStartPos = ArrowDown.transform.localPosition;
            _arrowUpStartPos = ArrowUp.transform.localPosition;
            _arrowLeftStartPos = ArrowLeft.transform.localPosition;
            _arrowRightStartPos = ArrowRight.transform.localPosition;
        }
        IEnumerator WaitForConstruction()
        {
            while (currentTower != null && currentTower.IsInConstruction()) yield return null;
            if (currentTower != null)
            {
                Update();
                floatingButtons.SetActive(true);
            }
        }

        public static void ShowInfoPanel(UnitTower tower, bool upgradeTower, GameObject butObj)
        {
            instance._showInfoPanel(tower, upgradeTower, butObj);
        }
        void _showInfoPanel(UnitTower tower, bool upgradeTower, GameObject butObj)
        {
            //Debug.Log("_showInfoPanel");
            Translator trans = TranslationEngine.Instance.Trans;
            if (!upgradeTower)
            {
                currentTower = tower;
            }
            isOn = true;
            anchorLeft.gameObject.SetActive(true);
            anchorRight.gameObject.SetActive(true);
            string[] name = tower.unitName.Split(';');
            if (name.Length > 1)
                txtName.text = trans[name[0]] + " " + name[1];
            else
                txtName.text = trans[name[0]];
            AttackSpeed.text = (Round(tower.GetCooldown(), 1)).ToString() + " " + trans["Game.Seconds"]; 
            string DamageMin = (tower.GetDamageMin() > 10) ? ((int)tower.GetDamageMin()).ToString() : (Round(tower.GetDamageMin(), 1)).ToString();
            string DamageMax = (tower.GetDamageMax() > 10) ? ((int)tower.GetDamageMax()).ToString() : (Round(tower.GetDamageMax(), 1)).ToString();

            if (tower.GetDamageMin() != tower.GetDamageMax())
            {
                Damage.text = DamageMin + "-" + DamageMax;
            }
            else
            {
                Damage.text = DamageMin;
            }



            objDamage.SetActive(upgradeTower == false);
            objDMtype.SetActive(upgradeTower == false);

            if (upgradeTower == false)
            {
                float min = tower.stats[0].damageMin;
                float max = tower.stats[0].damageMax;
                if (max > 0)
                {

                    //min = min * 1f / tower.stats[0].cooldown;
                    //max = max * 1f / tower.stats[0].cooldown;
                    min = Mathf.Round(min * 10) / 10;
                    max = Mathf.Round(max * 10) / 10;
                    InfoDamage.text = "За выстрел";
                    Damage.text = min + "-" + max; //(Mathf.Round((min + max) * 5) / 10).ToString();
                    InfoDesp.text = "Тип урона";
                    Desp.text = DamageTable.GetAllDamageType()[tower.MyCard.damageType].name;

                }
                else
                {
                    objDamage.SetActive(false);
                    Desp.text = "";
                    InfoDesp.text = "";
                    List<CardProperties> p = tower.Properties.FindAll(i => i.ID == 14);
                    for (int i = 0; i < p.Count; i++)
                    {
                        if (p[i].CanActive())
                        {
                            ResourceRegen rr = (ResourceRegen)p[i].properties;
                            InfoDesp.text = rr.Count + " ману за";
                            Desp.text = rr.Speed + "c";
                        }
                    }

                }
            }

            


            objTypeTarget.SetActive(currentTower.targetMode == _TargetMode.Ground && upgradeTower == false);


            List<float> cost = tower.MyCard.GetCost();
            rscObjList[0].label.text = (System.Math.Ceiling(cost[0]*10)/10f).ToString();
            Vector3 screenPos = Camera.main.WorldToScreenPoint(currentTower.thisT.position);
            Scale = GameObject.FindObjectOfType<Canvas>().scaleFactor;
            SetScreenPos(screenPos);
            Update();
        }
        public static float Round(float value, int digits)
        {
            float mult = Mathf.Pow(10.0f, (float)digits);
            return Mathf.Round(value * mult) / mult;
        }
        public static void HideInfoPanel()
        {
            instance._hideInfoPanel();
        }
        void _hideInfoPanel()
        {
            anchorRight.gameObject.SetActive(false);
            anchorLeft.gameObject.SetActive(false);
            isOn = false;
        }
        public static void ShowButtons(UnitTower tower)
        {
            instance._showButtons(tower);
        }

        public void OnShowInfo()
        {
            if (CardUIAllInfo.instance != null)
                CardUIAllInfo.instance.Show(currentTower.MyCard, CardUIAllInfo.Mode.gameUpgrade);
        }

        void updateTextDPS(UnitTower tower)
        {
           
            float min = tower.stats[0].damageMin;
            float max = tower.stats[0].damageMax;
            if (max > 0)
            {
                DpsSmallObj.SetActive(true);
                //min = min * 1f / tower.stats[0].cooldown;
                //max = max * 1f / tower.stats[0].cooldown;
                min = Mathf.Round(min * 10) / 10;
                max = Mathf.Round(max * 10) / 10;
                txtDps.text =  min + "-" + max;// (Mathf.Round((min + max) * 5)/10).ToString();

                InfoDpsSmall.text = "За выстрел";

                txtTypeDamageSmall.text = DamageTable.GetAllDamageType()[tower.DamageType()].name;
                InfoTypeDamageSmall.text = "Тип урона";
            }
            else
            {
                InfoTypeDamageSmall.text = "";
                txtTypeDamageSmall.text = "";
                DpsSmallObj.SetActive(false);

                List<CardProperties> p = tower.Properties.FindAll(i => i.ID == 14);
                for (int i = 0; i < p.Count; i++)
                {
                    if (p[i].CanActive())
                    {
                        ResourceRegen rr = (ResourceRegen)p[i].properties;
                        InfoTypeDamageSmall.text = rr.Count + " ману за";
                        txtTypeDamageSmall.text = rr.Speed + "c";
                    }
                }

            }
            objTypeTargetBuild.SetActive(tower.targetMode == _TargetMode.Ground);
        }

        void _showButtons(UnitTower tower)
        {
            MyLog.Log("_showButtons: " + tower.unitName);
            isOn = true;

            currentTower = tower;
            updateTextDPS(currentTower);
#if UNITY_ANDROID || UNITY_IOS
            HideSampleTower();
#endif
            if (UpgradeCurcor.activeSelf == true)
                UpgradeCurcor.SetActive(false);

            AncherBottomLeft.gameObject.SetActive(true);
            floatingButtons.SetActive(true);
            if (currentTower.IsInConstruction())
            {
                StartCoroutine(WaitForConstruction());
                floatingButtons.SetActive(false);
            }

            upgradeOption = currentTower.ReadyToBeUpgrade();
            if (upgradeOption > 0)
            {
                //float cost = ResourceManager.GetResourceArray()[0].value;
                //List<float> costlist = currentTower.MyCard.NextLevelTower[0].GetCost();
                butUpgrade.SetActive(true);
                butUpgrade.GetComponent<Button>().interactable = true;
                SetNormalSpriteAndResetSize(imageUpgrade, currentTower.MyCard.NextLevelTower[0].iconSprite, true);

                if (upgradeOption > 1)
                {
                    butUpgradeAlt1.SetActive(true);
                    //costlist = currentTower.MyCard.NextLevelTower[1].GetCost(0);
                    SetNormalSpriteAndResetSize(imageUpgradeAlt, currentTower.MyCard.NextLevelTower[1].iconSprite, true);
                }
                else
                {
                    butUpgradeAlt1.SetActive(false);
                }
            }
            else
            {
                butUpgrade.SetActive(true);
                butUpgrade.GetComponent<Button>().interactable = false;
                butUpgradeAlt1.SetActive(false);
            }

            if (tower.fullHP > tower.HP)
                butRepair.interactable = true;
            else
                butRepair.interactable = false;

            HideSelector();
            rscTooltipObj.SetActive(false);
            Update();
            SetNormalSpriteAndResetSizeForSell(imageSell);

            if (OnShow != null)
            {
                OnShow();
            }
#if UNITY_ANDROID || UNITY_IOS
            _hideInfoPanel();
            HideSelector();
            imageSell.sprite = SellIcon;
            UpgareButtonWasPressed = false;
            UpgareButtonAltWasPressed = false;
            SellButtonWasPressed = false;
            rscTooltipObj.SetActive(false);
#endif
        }
        public static void HideButtons()
        {
            MyLog.Log("UITowerInfo HideButtons");
            if (instance!=null)
            instance._hideButtons();
        }
        void _hideButtons()
        {
            AncherBottomLeft.SetActive(false);
            isOn = false;

            HideSampleTower();

            if (OnHide != null)
                OnHide();
        }
        public static bool isOn = true;
        public static void Hide()
        {
            if (instance == null)
                return;
            instance._Hide();
        }
        public void _Hide()
        {
            //currentTower=null;
            isOn = false;
            thisObj.SetActive(isOn);
            
        }

        public static void ShowUpgradeCursor()
        {
            instance._ShowUpgradeCursor();
        }

        public void _ShowUpgradeCursor()
        {
            if (UpgradeCurcor.activeSelf == false)
                UpgradeCurcor.SetActive(true);
        }
    }

}