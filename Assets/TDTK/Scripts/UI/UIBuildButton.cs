﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine.SceneManagement;

namespace TDTK
{

    public class UIBuildButton : MonoBehaviour
    {

        public List<UnityButton> buttonList = new List<UnityButton>();	//all button list
        private List<UnityButton> activeButtonList = new List<UnityButton>();	//current active button list, subject to tower availability on platform

        public delegate void ShowEventHandler();
        public static event ShowEventHandler ShowEvent;

        public delegate void HideEventHandler();
        public static event HideEventHandler HideEvent;

        public delegate void BuildEventHandler();
        public static event BuildEventHandler BuildEvent;

        private BuildInfo buildInfo;

        public Sprite NotCard;
        public Sprite[] SpritePlaceLevel;
        public Sprite[] MaskPlace;

        public static UIBuildButton instance;
        private GameObject thisObj;

        public Sprite ApplyIcon;
        public GameObject PressedButton = null;

        public GameObject LinesQuad;
        public GameObject Selector;

        public GameObject RscObj;
        public GameObject UnavailableTooltip;
        

        public List<bool> ButtonAvailably;
        int idFirstLevel = -100;
        int _id;
        void Awake()
        {
            instance = this;
            thisObj = gameObject;

            isOn = false;
        }

        // Use this for initialization
        void Start()
        {

            List<Card> towerList = BuildManager.GetTowerList();

            EventTrigger.Entry entry = SetupTriggerEntry();


            for (int i = 0; i < 6; i++)
            {
                if (i == 0)
                {
                    buttonList[0].Init();
                    //нужно чтобы можно быловыбирать кнопки
#if UNITY_ANDROID || UNITY_IOS
                    buttonList[0].button.navigation = Navigation.defaultNavigation;
#endif
                }
                else if (i > 0)
                {
                    buttonList.Add(buttonList[0].Clone("button" + (i + 1), new Vector3(i * 55, 0, 0)));
                }

                if (towerList.Count > i)
                {
                    buttonList[i].imageIcon.sprite = towerList[i].iconSprite;
                    //buttonList[i].imageBG.color = towerList[i].getColorRareness();
                    buttonList[i].imageBG.sprite = MaskPlace[(int)towerList[i].rareness];
                    if (buttonList[i].imageFonLevelCard != null)
                        buttonList[i].imageFonLevelCard.sprite = SpritePlaceLevel[(int)towerList[i].rareness];
                    if (buttonList[i].levelCard!=null)
                        buttonList[i].levelCard.text = towerList[i].LevelCard.ToString();
                }
                else
                {
                    buttonList[i].imageBG.sprite = NotCard;
                    buttonList[i].imageBG.color = Color.white;
                    buttonList[i].imageIcon.gameObject.SetActive(false);
                    buttonList[i].buttonInfo.SetActive(false);
                    if (buttonList[i].imageFonLevelCard != null)
                        buttonList[i].imageFonLevelCard.gameObject.SetActive(false);
                }
                    

                EventTrigger trigger = buttonList[i].rootObj.GetComponent<EventTrigger>();
                trigger.triggers.Add(entry);
            }
            Selector.transform.SetSiblingIndex(6); // TODO в WEBGL было 5 проверить 
            if (UnavailableTooltip != null)
                UnavailableTooltip.transform.SetSiblingIndex(6);
            Hide();
        }

        private EventTrigger.Entry SetupTriggerEntry()
        {
            UnityEngine.Events.UnityAction<BaseEventData> call = new UnityEngine.Events.UnityAction<BaseEventData>(OnTowerButton);
            EventTriggerType eventID = EventTriggerType.PointerClick;
#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY
            if (UI.UseDragNDrop()) eventID = EventTriggerType.PointerDown;
#endif

            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = eventID;
            entry.callback = new EventTrigger.TriggerEvent();
            entry.callback.AddListener(call);

            return entry;
        }



        public static void AddNewTower(UnitTower newTower) { instance._AddNewTower(newTower); }
        public void _AddNewTower(UnitTower newTower)
        {
            int count = buttonList.Count;
            buttonList.Add(buttonList[count - 1].Clone("button" + (count), new Vector3(55, 0, 0)));
            buttonList[count].imageIcon.sprite = newTower.iconSprite;

            EventTrigger trigger = buttonList[count].rootObj.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = SetupTriggerEntry();
            trigger.triggers.Add(entry);
        }



        public void OnTowerButton(UnityEngine.EventSystems.BaseEventData baseEvent)
        {
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
            if (!ButtonAvailably[_id])
                return;
            OnTowerButton(baseEvent.selectedObject);
#endif
#if UNITY_ANDROID || UNITY_IOS
            if (!ButtonAvailably[GetButtonID(baseEvent.selectedObject)])
            {
                ResetPressedButton();
                HideTowerInfoAndSmapleTower();
                HideSelector();
                ShowUnavailableTooltip(buttonList[GetButtonID(baseEvent.selectedObject)].button.gameObject);
                RedBlinking.StopBlinking(UITowerInfo.instance.gameObject);
                return;
            }
            else
            {
                HideUnavailableTooltip();
            }

            if (PressedButton == null)
            {
                SetPressedButtonAndApplayIcon(baseEvent);
                ShowTowerInfoAndSmapleTower(PressedButton);
                ShowSelector(PressedButton);
            }
            else
            {
                if (PressedButton != baseEvent.selectedObject)
                {
                    ResetPressedButton();
                    HideTowerInfoAndSmapleTower();
                    HideSelector();
                    RedBlinking.StopBlinking(UITowerInfo.instance.gameObject);
                    SetPressedButtonAndApplayIcon(baseEvent);
                    ShowTowerInfoAndSmapleTower(PressedButton);
                    ShowSelector(PressedButton);
                }
                else
                {
                    OnTowerButton(baseEvent.selectedObject);
                }
            }
#endif
        }

        private void SetPressedButtonAndApplayIcon(UnityEngine.EventSystems.BaseEventData baseEvent)
        {
            PressedButton = baseEvent.selectedObject;
            int ID = GetButtonID(PressedButton);
            buttonList[ID].imageIcon.sprite = ApplyIcon;
            //35 на 35 и в центре 
            buttonList[ID].imageIcon.rectTransform.localPosition = Vector3.zero;
            buttonList[ID].imageIcon.rectTransform.sizeDelta = new Vector3(35, 35);
        }

        private void ResetPressedButton()
        {
            if (PressedButton == null)
            {
                return;
            }
            List<Card> towerList = BuildManager.GetTowerList();
            int ID = GetButtonID(PressedButton);
            //65 на 65 и y =8
            buttonList[ID].imageIcon.rectTransform.localPosition = new Vector3(0, 8, 0);
            buttonList[ID].imageIcon.rectTransform.sizeDelta = new Vector3(65, 65);

            buttonList[ID].imageIcon.sprite = towerList[ID].iconSprite;
            PressedButton = null;
        }

        public void OnShowInfo(GameObject butObj)
        {
            if (CardUIAllInfo.instance != null)
            {
                if (UI.UseDragNDrop() && GameControl.GetSelectedTower() != null)
                {
                    UI.ClearSelectedTower();
                    return;
                }
                int ID = _id;// GetButtonID(butObj);
                List<Card> towerList = BuildManager.GetTowerList();
                if (towerList.Count <= ID)
                {
                    Debug.Log("OnTowerButton not select Card");
                    return;
                }

                CardUIAllInfo.instance.Show(towerList[ID], CardUIAllInfo.Mode.gameBild);
            }
        }

        public void OnTowerButton(GameObject butObj)
        {
            //in drag and drop mode, player could be hitting the button while having an active tower selected
            //if that's the case, clear the selectedTower first. and we can show the tooltip properly
            if (UI.UseDragNDrop() && GameControl.GetSelectedTower() != null)
            {
                UI.ClearSelectedTower();
                return;
            }


            int ID = _id;// GetButtonID(butObj);
            List<Card> towerList = BuildManager.GetTowerList();
            if(towerList.Count<=ID)
            {
                Debug.Log("OnTowerButton not select Card");
                return;
            }

            string exception = "";
            if (!UI.UseDragNDrop())
            {
                /*if (CardUIAllInfo.instance != null)
                {
                    CardUIAllInfo.instance.Show(towerList[ID], CardUIAllInfo.Mode.gameBild);
                }*/
                    
                exception = BuildManager.BuildTower(towerList[ID]);
            }
            else
                exception = BuildManager.BuildTowerDragNDrop(towerList[ID]);

            if (exception == "Insufficient Resource")
            {
                RedBlinking.StartBlinking(RscObj);
                RedBlinking.StartBlinking(UITowerInfo.instance.gameObject);
            }
            else if (exception != "")
            {
                Debug.LogError("Build exception:" + exception);
            }
            else
            {
#if UNITY_ANDROID || UNITY_IOS
                ResetPressedButton();
                HideTowerInfoAndSmapleTower();
                HideSelector();
#endif
                if (BuildEvent != null)
                    BuildEvent();

                HideSelector();
                if (!UI.UseDragNDrop())
                    OnExitHoverButton(butObj);
                
                Hide();
            }
            
        }


        public void OnHoverTowerButton(GameObject butObj)
        {
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
            
            ShowTowerInfoAndSmapleTower(butObj);
            ShowSelector(butObj);

            if (!ButtonAvailably[GetButtonID(butObj)])
            {
                _id = GetButtonID(butObj);
                ShowUnavailableTooltip(butObj);
                return;
            }
            
#endif
        }
        void ShowUnavailableTooltip(GameObject butObj)
        {
            UnavailableTooltip.transform.position = butObj.transform.position + new Vector3(0, 53f, 0);
            UnavailableTooltip.SetActive(true);
        }
        void HideUnavailableTooltip()
        {
            if (UnavailableTooltip != null)
                UnavailableTooltip.SetActive(false);
        }

        public void OnExitHoverButton(GameObject butObj)
        {
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
            RedBlinking.StopBlinking(UITowerInfo.instance.gameObject);
            HideUnavailableTooltip();

            HideTowerInfoAndSmapleTower();
            HideSelector();
#endif

        }
        private void ShowTowerInfoAndSmapleTower(GameObject butObj)
        {
            if (GameControl.GetSelectedTower() != null)
                return;

          

            _id = GetButtonID(butObj);
            UnitTower tower = BuildManager.GetSampleTower(_id /** 5*/);
            if(tower == null)
            {
                return;
            }
            //Debug.Log("ShowTowerInfoAndSmapleTower id:" + _id+ " Name card:" + tower.MyCard.unitName);

            Card c = PlayerManager.CardsList.Find(i => i.ID == tower.MyCard.ID);
            tower.WriteValueCardInTower(c, false);

            if (!UI.UseDragNDrop())
            {
                BuildManager.ShowSampleTower(_id);
            } //clear sample

            //show tooltip
            //UITowerInfo.Show(tower);
            if (SceneManager.GetActiveScene().buildIndex != /*Application.loadedLevel !=*/ idFirstLevel || BuildManager.GetTowerCount() > 1 || ButtonAvailably[_id])
            {
                UITowerInfo.ShowInfoPanel(tower, false, butObj);
            }
            
        }
        public static void HideTowerInfoAndSmapleTower()
        {
            if (GameControl.GetSelectedTower() != null && !GameControl.GetSelectedTower().IsSampleTower)
                return;

            BuildManager.ClearSampleTower();

            //clear tooltip
            //UITowerInfo.Hide();
            UITowerInfo.HideInfoPanel();
        }
        public void ShowSelector(GameObject butObj)
        {
            Selector.transform.position = butObj.transform.position;
            Selector.SetActive(true);
        }
        public void HideSelector()
        {
            Selector.SetActive(false);
        }
        int GetButtonID(GameObject butObj)
        {

            for (int i = 0; i < buttonList.Count; i++)
            {
                if (buttonList[i].rootObj == butObj)
                {
                    //Debug.Log("UIBuildButton GetButtonID " + i);
                    return i;
                }
            }
            return 0;
        }


        // Update is called once per frame
        float cutoff=0;
        Vector3 screenPos;
        void Update()
        {
            if (!UI.UseDragNDrop())
            {
                screenPos = Camera.main.WorldToScreenPoint(buildInfo.position);
                List<Vector3> pos = GetPieMenuPos(activeButtonList.Count, screenPos, cutoff, 40);

                LinesQuad.transform.localPosition = (screenPos + new Vector3(0, 20, 0) * UI.GetScaleFactor()) / UI.GetScaleFactor();

                for (int i = 0; i < activeButtonList.Count; i++)
                {
                    if (i < pos.Count)
                    {
                        activeButtonList[i].rootT.localPosition = pos[i];
                    }
                    else
                    {
                        activeButtonList[i].rootT.localPosition = new Vector3(0, 9999, 0);
                    }
                }
            }
        }


        void UpdateActiveBuildButtonList()
        {
            if (buildInfo == null)
                return;
            MyLog.Log("UpdateActiveBuildButtonList "+ buildInfo.availableTowerIDList.Count);

            activeButtonList = new List<UnityButton>();
            for (int i = 0; i <6 /*buildInfo.availableTowerIDList.Count*/; i++)
            {

                if (buildInfo.availableTowerIDList.Count > i)
                {
                    //Debug.Log(buildInfo.availableTowerIDList[i]);
                    activeButtonList.Add(buttonList[buildInfo.availableTowerIDList[i]]);
                }
                else
                    activeButtonList.Add(buttonList[i]);
            }

            for (int i = 0; i < buttonList.Count; i++)
            {
                buttonList[i].rootObj.SetActive(true);
                /*if (activeButtonList.Contains(buttonList[i]))
                    buttonList[i].rootObj.SetActive(true);
                else
                    buttonList[i].rootObj.SetActive(false);*/
            }
        }


        public static bool isOn = true;
        public static void Show()
        {            
            instance._Show();
        }
        public void _Show()
        {
            MyLog.Log("UIBuildManager _Show");

            //TimeScaleManager.SetTimeScale(0.5f);
            buildInfo = BuildManager.GetBuildInfo();
            UpdateActiveBuildButtonList();
            Update();
            ResetPressedButton();
            isOn = true;
            thisObj.SetActive(isOn);
            HideSelector(); //TODO В WEBGL небыло проверить
            HideUnavailableTooltip();

            if (ShowEvent != null)
                ShowEvent();

        }
        public static void Hide()
        {
            if (instance == null)
            {
                //Debug.LogError("UIBuildButton instance is null");
                return;
            }
            instance._Hide();
        }
        public void _Hide()
        {

            if (UI.UseDragNDrop() && !FPSControl.IsOn())
                return;


            BuildManager.ClearBuildPoint();
#if UNITY_ANDROID || UNITY_IOS
            HideTowerInfoAndSmapleTower();
            HideSelector();
            HideUnavailableTooltip();
#endif
            isOn = false;

            TimeScaleManager.RefreshTimeScale();

            thisObj.SetActive(isOn);

            if (HideEvent != null)
                HideEvent();
        }



        public static Transform piePosDummyT;
        public static List<Vector3> GetPieMenuPos(float num, Vector3 screenPos, float cutoff, int size)
        {
            List<Vector3> points = new List<Vector3>();
            TutorialBuildTowerManager.SetPosition(screenPos);
            if (num == 1)
            {
                points.Add(screenPos + new Vector3(0, 50, 0));
                return points;
            }

            //if there's only two button to be displayed, then normal calculation doesnt apply
            if (num <= 2)
            {
                points.Add(screenPos + new Vector3(50, 10, 0));
                points.Add(screenPos + new Vector3(-50, 10, 0));
                return points;
            }
            if (num == 4)
            {
                int length = 50;
                var shift = new Vector3(0, 50, 0);
                points.Add((screenPos + shift * UI.GetScaleFactor() + new Vector3(length, length / 2, 0) * UI.GetScaleFactor()) / UI.GetScaleFactor());
                points.Add((screenPos + shift * UI.GetScaleFactor() + new Vector3(-length, length / 2, 0) * UI.GetScaleFactor()) / UI.GetScaleFactor());
                points.Add((screenPos + shift * UI.GetScaleFactor() + new Vector3(length * 1.8f, -length * 0.75f, 0) * UI.GetScaleFactor()) / UI.GetScaleFactor());
                points.Add((screenPos + shift * UI.GetScaleFactor() + new Vector3(-length * 1.8f, -length * 0.75f, 0) * UI.GetScaleFactor()) / UI.GetScaleFactor());

                return points;
            }
            if (num == 6)
            {
               // int length = 50;
                points.Add((screenPos  + new Vector3(104, 56, 0) ));
                points.Add((screenPos  + new Vector3(82, 0, 0) ));
                points.Add((screenPos  + new Vector3(62, -56, 0) ));

                points.Add((screenPos  + new Vector3(-104, 56, 0) ));
                points.Add((screenPos  + new Vector3(-82, 0, 0) ));
                points.Add((screenPos  + new Vector3(-62, -56, 0) ));

                return points;
            }

            //create a dummy transform which we will use to do the calculation
            if (piePosDummyT == null) piePosDummyT = new GameObject().transform;

            int cutoffOffset = cutoff > 0 ? 1 : 0;

            //calculate the spacing of angle and distance of button from center
            float spacing = (float)((360f - cutoff) / (num - cutoffOffset));
            //float dist=Mathf.Max((num+1)*10, 50);
            float dist = 0.35f * num * size * UI.GetScaleFactor();

            piePosDummyT.rotation = Quaternion.Euler(0, 0, cutoff / 2);
            piePosDummyT.position = screenPos;//Vector3.zero;

            //rotate the dummy transform using the spacing interval, then sample the end point
            //these end point will be our button position
            for (int i = 0; i < num; i++)
            {
                Vector3 pos = piePosDummyT.TransformPoint(new Vector3(0, -dist, 0));
                points.Add(pos / UI.GetScaleFactor());
                piePosDummyT.Rotate(Vector3.forward * spacing);
            }

            //clear the dummy object
            //Destroy(piePosDummyT.gameObject);

            return points;
        }
    }

}