﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using Translation;

namespace TDTK
{
    public class UIHeroSpawn : MonoBehaviour
    {
        public static event Action<UnitHero> onSpawnHero; 

        [Serializable]
        public struct HeroDead
        {
            public HeroDead(UnityButton b, UnitHero h)
            {
                button = b;
                hero = h;
            }
            public UnityButton button;
            public UnitHero hero;
        }

        public List<HeroDead> ListButtonHero;
        public UnityButton button;

        public float TooltipLeftMargin;
        public float TooltipRightMargin;
        public RectTransform MainCanvas;
        public RectTransform TooltipCanvas;
        public GameObject tooltipObj;
        public GameObject Arrow;
        public Text txtTooltipName;
        public Text txtTooltipDesp;
        public Text txtTooltipPropertis;

        public static UIHeroSpawn instance { get; private set; }

        private bool Limited = true;

        public bool bShotTooltip = false;

        private void Awake()
        {
            ListButtonHero = new List<HeroDead>();
            button.Init();
            button.rootObj.SetActive(false);
            instance = this;

            currentIndicator = Instantiate(defaultIndicator);
            currentIndicator.parent = transform;

        }

        private void Start()
        {
            List<Card> heroCards = PlayerManager.CardsList.FindAll(i => i.disableInBuildManager == false && i.Type == _CardType.Hero);
            if (heroCards.Count < 1)
                return;

            SetCardHero(heroCards);
        }

        private void OnEnable()
        {
            UnitHero.unitHeroDead += OnDestroyUnit;
        }

        private void OnDisable()
        {
            UnitHero.unitHeroDead -= OnDestroyUnit;
        }

        private void OnDestroyUnit(object sender, EventArgs a)
        {
            UnitHero hero = (UnitHero)sender;
            OnDeadHero(hero);

        }

        private void OnDeadHero(UnitHero hero)
        {
            MyLog.LogWarning("OnDeadHero");
            if (hero.IsImmortal)
                return;

            if (Limited == false)
                SetButtonHero(hero);
            else
                DeadHero(hero);
        }



        public void SetButtonHero(UnitHero hero)
        {
            HeroDead hd = new HeroDead(button.Clone("buttonSpawn_" + hero.name, Vector3.zero), hero);
            ListButtonHero.Add(hd);
            hd.button.rootObj.SetActive(true);
            hd.button.imageIcon.sprite = hero.iconSprite;
            hd.button.label.text = "";
            float p = hero.GetCost(hero.currentActiveStat)[0];
            hd.button.price.text = (Mathf.Ceil(p * 10f) / 10f).ToString("0.#");
        }


        public void OnSpawnHero(GameObject butObj)
        {
            if (Limited == false)
                SpawnHeroLimited(butObj);
            else
                StartSpawnHeroNotLimited(butObj);
        }

        private void SpawnHeroLimited(GameObject butObj)
        {
            int index = GetButtonID(butObj);
            Debug.Log("Hero Spawn " + index);
            UnitHero h = ListButtonHero[index].hero;

            if (h.GetCost(h.currentActiveStat)[0] > ResourceManager.GetResourceArray()[0])
            {
                RedBlinking.StartBlinking(ListButtonHero[index].button.price.gameObject);
                return;
            }
            else
            {
                ListButtonHero[index].button.cancelIcon.gameObject.SetActive(true);
                selectedHeroID = index;
                StartCoroutine(SelectPointSpawn());
            }


        }
        private void EndSpawnHeroLimited(Vector3 point)
        {
            UnitHero h = ListButtonHero[selectedHeroID].hero;
            ResourceManager.SpendResource(h.GetCost(h.currentActiveStat));
            hideTooltip();
            h.thisT.position = point;
            h.thisObj.SetActive(true);
            h.InitHero(h.instanceID, h.MyCard, h.currentActiveStat, true);
            Destroy(ListButtonHero[selectedHeroID].button.rootObj);
            ListButtonHero[selectedHeroID].button.cancelIcon.gameObject.SetActive(false);
            ListButtonHero.RemoveAt(selectedHeroID);
        }

        int GetButtonID(GameObject butObj)
        {
            for (int i = 0; i < ListButtonHero.Count; i++)
            {
                if (ListButtonHero[i].button.rootObj == butObj) return i;
            }
            return 0;
        }

        public void hideTooltip()
        {
            bShotTooltip = false;
            tooltipObj.SetActive(false);
            Arrow.SetActive(false);
        }

        public void OnShowAllInfoButton(GameObject butObj)
        {
            int id = GetButtonID(butObj);

            Card c;
            if (Limited == false)
                c = ListButtonHero[id].hero.MyCard;
            else
                c = HeroCards[id];
            CardUIAllInfo.instance.Show(c, CardUIAllInfo.Mode.onlyCancel);
        }

        public float TooltipOffsetY;
        public void showTooltip(GameObject butObj)
        {
            bShotTooltip = true;
            if (GameControl.GetSelectedTower() != null) return;


            int ID = GetButtonID(butObj);
            float xPos = butObj.transform.position.x;
            if (xPos < TooltipLeftMargin * UI.GetScaleFactor())
            {
                xPos = TooltipLeftMargin * UI.GetScaleFactor();
            }
            if (xPos > TooltipCanvas.sizeDelta.x * UI.GetScaleFactor() - TooltipRightMargin * UI.GetScaleFactor())
            {
                xPos = TooltipCanvas.sizeDelta.x * UI.GetScaleFactor() - TooltipRightMargin * UI.GetScaleFactor();
            }
            tooltipObj.transform.position = new Vector3(xPos, TooltipOffsetY * MainCanvas.localScale.x /* tooltipObj.transform.position.y*/, tooltipObj.transform.position.z);
            Arrow.transform.position = new Vector3(butObj.transform.position.x, Arrow.transform.position.y, Arrow.transform.position.z);

            Card cardH = Limited ? HeroCards[ID] : ListButtonHero[ID].hero.MyCard;


            SetInfoMainTooltip(ID, cardH);

            Arrow.SetActive(true);
            tooltipObj.SetActive(true);
        }

        private void SetInfoMainTooltip(int ID, Card card)
        {
            Translator trans = TranslationEngine.Instance.Trans;
            txtTooltipName.text = card.unitName;
            if (card.heroUnit == null)
                Debug.LogError("cardAb.heroUnit null");

            var CurrentStat = CraftLevelData.GetStatGrade(card);

            txtTooltipDesp.text = trans[card.heroUnit.desp];
            txtTooltipPropertis.text = "";

            if (CurrentStat.damageMax > 0)
                txtTooltipPropertis.text += CurrentStat.damageMin + "-" + CurrentStat.damageMax + " ед. ур.";

            List<CardProperties> activeProper = new List<CardProperties>();
            bool apply, addActive;
            foreach (CardProperties proper in card.Properties)
            {
                apply = true;
                for (int i = 0; i < proper.Requirements.Count; i++)
                {
                    if (proper.Requirements[i].CanActiveProperties(card) == false)
                    {
                        apply = false;
                        break;
                    }
                }
                if (apply)
                {
                    addActive = false;
                    for (int j = 0; j < activeProper.Count; j++)
                    {
                        if (activeProper[j].ID == proper.ID)
                        {
                            addActive = true;
                            activeProper[j] = proper.Clone();
                            break;
                        }
                    }
                    if (addActive == false)
                        activeProper.Add(proper.Clone());
                }
            }

            for (int i = 0; i < activeProper.Count; i++)
            {
                if (txtTooltipPropertis.text.Length > 0)
                    txtTooltipPropertis.text += "\r\n";
                txtTooltipPropertis.text += activeProper[i].getInfoText();
            }
            activeProper.Clear();
        }

        public Transform defaultIndicator;
        public Transform currentIndicator;
        private int selectedHeroID;
        private IEnumerator SelectPointSpawn()
        {
            if (selectedHeroID < 0) yield break;



            //only cast on terrain and platform
            LayerMask mask = 1 << LayerManager.LayerTerrain() | 1 << LayerManager.LayerPlatform();


            Camera mainCam = Camera.main;
            if (mainCam != null)
            {
                yield return null;
                currentIndicator.gameObject.SetActive(true);
                Ray ray;
                RaycastHit hit;
                while (selectedHeroID > -1)
                {
                    ray = mainCam.ScreenPointToRay(Input.mousePosition);
                    if (Physics.Raycast(ray, out hit, 100f, mask))
                    {
                        currentIndicator.position = hit.point + new Vector3(0, 10f, 0);

                        if (Input.GetMouseButtonDown(0) && bShotTooltip == false)
                        {
                            if (Limited)
                                EndSpawnHeroNotLimited(hit.point);
                            else
                                EndSpawnHeroLimited(hit.point);
                            ClearSelected();
                        }
                    }

                    if (Input.GetMouseButtonDown(1))
                    {
                        ClearSelected();

                    }
                    if (Input.GetKey(KeyCode.Escape))
                    {
                        ClearSelected();
                    }
                    yield return null;
                }

            }





            yield break;
        }

        public void ClearSelected()
        {

            if (ListButtonHero.Count > selectedHeroID && selectedHeroID > -1)
                ListButtonHero[selectedHeroID].button.cancelIcon.gameObject.SetActive(false);
            selectedHeroID = -1;
            currentIndicator.gameObject.SetActive(false);
        }

        private List<float> GetCost(Card c)
        {
            List<float> cost = new List<float>();

            cost.Add(GetCostMoney(c));
            cost.Add(0);

            return cost;
        }

        private float GetCostMoney(Card c)
        {
            int index = c.Properties.FindIndex(i => i.ID == 191);
            float p ;
            if (index >= 0)
            {
                p = (float)c.Properties[index].SetParam[0] / 100f;
            }
            else
                p = 0;

            int count = countTypeActiveHero(c);
            return c.GetCost()[0] * Mathf.Pow(1.5f-p,count);
        }

        #region Spawn not limited hero

        public static List<UnitHero> getActiveHeroList {
            get
            {
                if (instance != null)
                    return instance.ActiveHeroList;
                else
                    return null;
            }
        }

        private List<Card> HeroCards;
        public List<UnitHero> ActiveHeroList;
        public void SetCardHero(List<Card> heroCards)
        {
            if (HeroCards != null)
                HeroCards.Clear();
            HeroCards = heroCards;

            ActiveHeroList = new List<UnitHero>();


            foreach (Card hc in HeroCards)
            {
                HeroDead hd = new HeroDead(button.Clone("buttonSpawn_" + hc.unitName, Vector3.zero), null);
                ListButtonHero.Add(hd);
                hd.button.rootObj.SetActive(true);
                hd.button.imageIcon.sprite = hc.iconSprite;
                hd.button.label.text = "";
                float p = hc.GetCost()[0];
                hd.button.price.text = (Mathf.Ceil(p * 10f) / 10f).ToString("0.#");
            }
        }


        private void StartSpawnHeroNotLimited(GameObject butObj)
        {


            int index = GetButtonID(butObj);
            MyLog.Log("Hero Spawn NotLimited " + index);

            Card card = HeroCards[index];

            if (card == null)
            {
                MyLog.LogError("SpawnHero Card in null!", MyLog.Type.build);
                return;
            }

            if (ListButtonHero[index].button.cancelIcon.gameObject.activeSelf == false)
            {
                if (selectedHeroID != -1)
                    ClearSelected();

                if (GetCostMoney(card) > ResourceManager.GetResourceArray()[0])
                {
                    RedBlinking.StartBlinking(ListButtonHero[index].button.price.gameObject);
                    return;
                }
                else
                {
                    ListButtonHero[index].button.cancelIcon.gameObject.SetActive(true);
                    selectedHeroID = index;
                    StartCoroutine(SelectPointSpawn());
                }
            }
            else
            {
                ClearSelected();
            }


            /* GameObject heroObj = ObjectPoolManager.Spawn(card.heroObj, SpawnManager.instance.GetPointSpawnHero(), Quaternion.identity);
             UnitHero hero = heroObj.GetComponent<UnitHero>();
             ActiveHeroList.Add(hero);
             hero.InitHero(ActiveHeroList.Count, card,0,true);

             p = card.GetCost()[0] * Mathf.Pow(1.5f, countTypeActiveHero(card));
             ListButtonHero[index].button.price.text = p.ToString();*/

        }

        private void EndSpawnHeroNotLimited(Vector3 pointSpawn)
        {
            Card card = HeroCards[selectedHeroID];
            ResourceManager.SpendResource(GetCost(card));
            GameObject heroObj = ObjectPoolManager.Spawn(card.heroObj, pointSpawn, Quaternion.identity);
            UnitHero hero = heroObj.GetComponent<UnitHero>();
            ActiveHeroList.Add(hero);
            hero.InitHero(ActiveHeroList.Count, card, 0, true);

            float p = GetCostMoney(card);//.GetCost()[0] * Mathf.Pow(1.5f, countTypeActiveHero(card));
            ListButtonHero[selectedHeroID].button.price.text = (Mathf.Ceil(p * 10f) / 10f).ToString("0.#");
            ListButtonHero[selectedHeroID].button.cancelIcon.gameObject.SetActive(false);

            if (onSpawnHero != null)
                onSpawnHero(hero);
        }

        public static UnitHero SpawnHero(int id, Card card, Vector3 spawnPoint, int startLevel = 0)
        {
            GameObject heroObj = ObjectPoolManager.Spawn(card.heroObj, spawnPoint, Quaternion.identity);
            UnitHero hero = heroObj.GetComponent<UnitHero>();
            hero.InitHero(id, card, startLevel, true);

            instance.UpdatePrice();

            return hero;
        }


        public int countTypeActiveHero(Card card)
        {
            return ActiveHeroList.FindAll(i => i.MyCard.ID == card.ID).Count;
        }


        public void UpdatePrice()
        {
            for (int i = 0; i < HeroCards.Count; i++)
            {
                float p = HeroCards[i].GetCost()[0] * Mathf.Pow(1.5f, countTypeActiveHero(HeroCards[i]));
                ListButtonHero[i].button.price.text = (Mathf.Ceil(p * 10f) / 10f).ToString("0.#");
            }
        }

        private void DeadHero(UnitHero hero)
        {
            MyLog.LogWarning("DeadHero"+HeroCards.Count);
            ActiveHeroList.Remove(hero);
            //ObjectPoolManager.Unspawn(hero.thisObj);
            UpdatePrice();
        }

        #endregion

    }


}