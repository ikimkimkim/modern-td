using UnityEngine;
using System.Collections;

namespace TDTK {

	public class LayerManager {

        private static int layerOnlyArtifact = 10;
        private static int layerHybridCreep = 9;
        private static int layerHybridTower = 8;
        private static int layerOnlyAir = 12;
        private static int layerOnlyGround = 11;
        private static int layerOnlyHero = 13;


        private static int layerCreep=31;
		private static int layerCreepF=30;
		private static int layerTower=29;
		private static int layerShootObj=28;
        private static int layerArtifact = 25;

		private static int layerPlatform=27;
		private static int layerTerrain=26;

        private static int layerHero = 24;
		
		
		
		
		public static int LayerCreep(){ return layerCreep; }
		public static int LayerCreepF(){ return layerCreepF; }
		public static int LayerTower(){ return layerTower; }
		public static int LayerShootObject(){ return layerShootObj; }
		public static int LayerPlatform(){ return layerPlatform; }
		public static int LayerArtifact() { return layerArtifact; }
        public static int LayerHero() { return layerHero; }
        public static int LayerOnlyArtifact() { return layerOnlyArtifact; }
        public static int LayerHybridCreep() { return layerHybridCreep; }
        public static int LayerHybridTower() { return layerHybridTower; }
        public static int LayerOnlyAir() { return layerOnlyAir; }
        public static int LayerOnlyHero() { return layerOnlyHero; }
        public static int LayerOnlyGround() { return layerOnlyGround; }


        public static int LayerTerrain(){ return layerTerrain; }
		public static int LayerUI(){ return 5; }	//layer5 is named UI by Unity's default
		
	}

}
