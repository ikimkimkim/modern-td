﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK
{

    [System.Serializable]
    public class HeroDB : MonoBehaviour
    {


        public List<UnitHero> HeroList = new List<UnitHero>();

        public static HeroDB LoadDB()
        {
            GameObject obj = Resources.Load("DB_TDTK/HeroDB", typeof(GameObject)) as GameObject;

#if UNITY_EDITOR
            if (obj == null) obj = CreatePrefab();
#endif

            return obj.GetComponent<HeroDB>();
        }

        public static List<UnitHero> Load()
        {
            GameObject obj = Resources.Load("DB_TDTK/HeroDB", typeof(GameObject)) as GameObject;

#if UNITY_EDITOR
            if (obj == null) obj = CreatePrefab();
#endif

            HeroDB instance = obj.GetComponent<HeroDB>();
            return instance.HeroList;
        }

        /*public static List<UnitHero> LoadClone()
        {
            GameObject obj = Resources.Load("DB_TDTK/AbilityDB", typeof(GameObject)) as GameObject;
            HeroDB instance = obj.GetComponent<HeroDB>();

            List<UnitHero> newList = new List<UnitHero>();

            if (instance != null)
            {
                for (int i = 0; i < instance.HeroList.Count; i++)
                {
                    newList.Add(instance.HeroList[i].Clone());
                }
            }

            return newList;
        }*/

#if UNITY_EDITOR
        private static GameObject CreatePrefab()
        {
            GameObject obj = new GameObject();
            obj.AddComponent<HeroDB>();
            GameObject prefab = PrefabUtility.CreatePrefab("Assets/TDTK/Resources/DB_TDTK/HeroDB.prefab", obj, ReplacePrefabOptions.ConnectToPrefab);
            DestroyImmediate(obj);
            AssetDatabase.Refresh();
            return prefab;
        }
#endif

    }
}
