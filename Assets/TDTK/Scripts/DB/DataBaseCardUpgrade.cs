﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New database card upgrade", menuName = "Database/CardUpgrade")]
public class DataBaseCardUpgrade : DataBase<CardUpgradeData>
{

}
