﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TextureDB : MonoBehaviour
{
    public List<SpriteData> badDBSprite = new List<SpriteData>();
    public List<TextureData> badDBTexture = new List<TextureData>();

    public Dictionary<string, Sprite> DBSprite = new Dictionary<string, Sprite>();
    public Dictionary<string, Texture2D> DBTextures = new Dictionary<string, Texture2D>();

    private List<string> loadingUrl;

    private Sprite NotTexture;

    public static TextureDB instance
    {
        get
        {
                if (_Instance == null)
                    Init();
                return _Instance;
            
        }
        private set { _Instance = value; }
    }

    private static TextureDB _Instance;
    private static void Init()
    {
        if (_Instance == null)
        {
            _Instance = (Resources.Load("DB_TDTK/Textur2D_DB", typeof(GameObject)) as GameObject).GetComponent<TextureDB>();
            _Instance.loadingUrl = new List<string>();
        }
    }


    //sprite

    public IEnumerator load(string url, Action<Sprite> result)
    {

        if (DBSprite.ContainsKey(url))
        {
            //Debug.Log("Sprite get:" + url);
            result(DBSprite[url]);// Sprite.Create(DBTextures[url], new Rect(0, 0, DBTextures[url].width, DBTextures[url].height), new Vector2(0.5f, 0.5f)));
        }
        else
        {
            for (int i = 0; i < badDBSprite.Count; i++)
            {
                if (badDBSprite[i].url == url)
                {
                    //Debug.Log("Sprite bad:" + url);
                    result(badDBSprite[i].sprite);
                    break;
                }
            }

            

            if (loadingUrl.Contains(url))
            {
                //Debug.Log("Sprite loading:" + url);
                int CountSecond = 0;
                while(DBSprite.ContainsKey(url)==false)
                {
                    yield return new WaitForSeconds(0.5f);
                    lock (loadingUrl)
                    {
                        if (loadingUrl.Contains(url) == false && CountSecond > 120)
                        {
                            Debug.LogError("Load Texture " + url + " to long, i break wait!");
                            break;
                        }
                    }
                    CountSecond++;
                }
                if(DBSprite.ContainsKey(url))
                {
                    result(DBSprite[url]);
                }
                else
                {
                    Debug.LogWarning("I break wait load texture, and hi not loading!");
                }
            }
            else
            {
                lock (loadingUrl)
                {
                    loadingUrl.Add(url);
                }
                PlayerManager._instance.StartCoroutine(ServerManager.DownLoadTextur(url, false, value =>
                {
#if !UNITY_EDITOR
                    if (value.width<9 && value.height<9)
                    {
                        Debug.LogWarning("Not sprite:" + url + "\r\n Use old sprite");
                        return;
                    }
#endif
                    Sprite s = Sprite.Create(value, new Rect(0, 0, value.width, value.height), new Vector2(0.5f, 0.5f));
                    s.name = value.name;
                   
                    lock (DBSprite)
                    {
                        if (DBSprite.ContainsKey(url) == false)
                            DBSprite.Add(url, s);
                    }
                    lock (loadingUrl)
                    {
                        loadingUrl.Remove(url);
                    }
                    result(s);

                } ));
            }

           
        }
        yield break;
    }

    public IEnumerator load(string url, Action<Texture2D> result)
    {

        if (DBTextures.ContainsKey(url))
        {
            //Debug.Log("Sprite get:" + url);
            result(DBTextures[url]);// Sprite.Create(DBTextures[url], new Rect(0, 0, DBTextures[url].width, DBTextures[url].height), new Vector2(0.5f, 0.5f)));
        }
        else
        {
            for (int i = 0; i < badDBTexture.Count; i++)
            {
                if (badDBTexture[i].url == url)
                {
                    //Debug.Log("Sprite bad:" + url);
                    result(badDBTexture[i].texture);
                    break;
                }
            }



            if (loadingUrl.Contains(url))
            {
                //Debug.Log("Sprite loading:" + url);
                int CountSecond = 0;
                while (DBTextures.ContainsKey(url) == false)
                {
                    yield return new WaitForSeconds(1);
                    lock (loadingUrl)
                    {
                        if (loadingUrl.Contains(url) == false && CountSecond > 120)
                        {
                            Debug.LogError("Load Texture " + url + " to long, i break wait!");
                            break;
                        }
                    }
                    CountSecond++;
                }
                if (DBTextures.ContainsKey(url))
                {
                    result(DBTextures[url]);
                }
                yield break;
            }
            else
            {
                lock (loadingUrl)
                {
                    loadingUrl.Add(url);
                }
                PlayerManager._instance.StartCoroutine(ServerManager.DownLoadTextur(url, false, value =>
                {
#if !UNITY_EDITOR
                    if (value.width<9 && value.height<9)
                    {
                        Debug.LogWarning("Not textures:" + url + "\r\n Use old sprite");
                        return;
                    }
#endif
                   
                    lock (DBTextures)
                    {
                        if (DBTextures.ContainsKey(url) == false)
                            DBTextures.Add(url, value);
                    }
                    lock (loadingUrl)
                    {
                        loadingUrl.Remove(url);
                    }
                    result(value);

                }));
            }


        }
        yield break;
    }



    [System.Serializable]
    public class SpriteData
    {
        public Sprite sprite;
        public string url;
    }

    [System.Serializable]
    public class TextureData
    {
        public Texture2D texture;
        public string url;
    }
}


