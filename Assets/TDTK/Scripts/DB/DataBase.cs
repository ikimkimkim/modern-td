﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataBase<T> : ScriptableObject
{
    private const string Directoty = "DB_TDTK";

    public enum NameBase { Ability, SpecialEffects, ShootObject, Blood, CardUpgrade, Resources, SplashObject };

    public List<T> data = new List<T>();

    public static Dictionary<NameBase, DataBase<T>> listDatabases;

    public static DataBase<T> LoadDB(NameBase nameBase)
    {
        checkedDatabase(nameBase);

        return listDatabases[nameBase];
    }

    public static List<T> Load(NameBase nameBase)
    {
        checkedDatabase(nameBase);

        return listDatabases[nameBase].data;
    }

    public static T LoadIndex(NameBase nameBase, int index)
    {
        checkedDatabase(nameBase);
        if(listDatabases[nameBase].data.Count>index)
        {
            return listDatabases[nameBase].data[index];
        }
        return default(T);
    }

    private static void checkedDatabase(NameBase nameBase)
    {
        if (listDatabases == null)
            listDatabases = new Dictionary<NameBase, DataBase<T>>();

        if (listDatabases.ContainsKey(nameBase) == false)
        {
            DataBase<T> DB = Resources.Load(Directoty + "/" + nameBase.ToString(), typeof(DataBase<T>)) as DataBase<T>;
            if (DB == null)
            {
                Debug.LogError(string.Format("Not find database {0} {1}",typeof(T).ToString(),nameBase.ToString()));
                return;
            }
            listDatabases.Add(nameBase, DB);
        }
    }
}