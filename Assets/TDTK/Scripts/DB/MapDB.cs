﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapDB : MonoBehaviour {

    [System.Serializable]
    public struct MapData
    {
        public bool ShowDataEditor;
        public string Name;
        public int LevelID;
        //public int ScaneID;
        public string NameBundle;
        //public string urlIcon;
        public Sprite Icon;

        public string getUrlIcon
        {
        get {
                if(Icon==null)
                {
                    Debug.LogError("ERROR get url icon, icon level is null");
                    return string.Format("ERROR get url icon, icon level is null");
                }

                return string.Format("LevelIcon/{0}.png", Icon.name );
            }
        }

    }


    public List<MapData> MapList = new List<MapData>();

    public static MapDB LoadDB()
    {
        GameObject obj = Resources.Load("DB_TDTK/MapDB", typeof(GameObject)) as GameObject;


        return obj.GetComponent<MapDB>();
    }

    public static List<MapData> Load()
    {
        GameObject obj = Resources.Load("DB_TDTK/MapDB", typeof(GameObject)) as GameObject;

        MapDB instance = obj.GetComponent<MapDB>();
        return instance.MapList;
    }




}
