﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

[CreateAssetMenu(fileName = "New database ability", menuName = "Database/Ability")]
public class DataBaseAbility : DataBase<Ability>
{
    public static List<Ability> Load()
    {
        return Load(NameBase.Ability);
    }
}
