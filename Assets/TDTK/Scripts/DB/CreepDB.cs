﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK {

	public class CreepDB : MonoBehaviour {

		public List<UnitCreep> creepList=new List<UnitCreep>();
        public static CreepDB creepDB;
	
		public static CreepDB LoadDB(){
            if (creepDB == null)
            {
                GameObject obj = Resources.Load("DB_TDTK/CreepDB", typeof(GameObject)) as GameObject;

#if UNITY_EDITOR
                if (obj == null) obj = CreatePrefab();
#endif
                creepDB = obj.GetComponent<CreepDB>();
            }

            return creepDB;
		}
		
		public static List<UnitCreep> Load(){
            if (creepDB == null)
            {
                GameObject obj = Resources.Load("DB_TDTK/CreepDB", typeof(GameObject)) as GameObject;

#if UNITY_EDITOR
                if (obj == null) obj = CreatePrefab();
#endif

                creepDB = obj.GetComponent<CreepDB>();
            }

            return creepDB.creepList;
		}
		
		#if UNITY_EDITOR
			private static GameObject CreatePrefab(){
				GameObject obj=new GameObject();
				obj.AddComponent<CreepDB>();
				GameObject prefab=PrefabUtility.CreatePrefab("Assets/TDTK/Resources/DB_TDTK/CreepDB.prefab", obj, ReplacePrefabOptions.ConnectToPrefab);
				DestroyImmediate(obj);
				AssetDatabase.Refresh ();
				return prefab;
			}
		#endif
		
		
		//for filling up  empty unit of spawnManager
		public static UnitCreep GetFirstPrefab(){
			GameObject obj=Resources.Load("DB_TDTK/CreepDB", typeof(GameObject)) as GameObject;
			
			#if UNITY_EDITOR
				if(obj==null) obj=CreatePrefab();
			#endif
			
			CreepDB instance=obj.GetComponent<CreepDB>();
			return instance.creepList.Count==0 ? null : instance.creepList[0];
		}

	}
	
}
