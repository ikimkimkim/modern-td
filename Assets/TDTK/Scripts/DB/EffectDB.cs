﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class EffectDB : MonoBehaviour {



    public List<GameObject> HeroList = new List<GameObject>(); // список эффектов
    public static EffectDB effectDB;

    public static EffectDB LoadDB()
    {
        if (effectDB == null)
        {
            GameObject objDB = Resources.Load("DB_TDTK/EffectDB", typeof(GameObject)) as GameObject;

#if UNITY_EDITOR
            if (objDB == null) objDB = CreatePrefab();
#endif
            effectDB = objDB.GetComponent<EffectDB>();
        }

        return effectDB;
    }

    public static List<GameObject> Load()
    {
        if (effectDB == null)
        {
            GameObject objDB = Resources.Load("DB_TDTK/EffectDB", typeof(GameObject)) as GameObject;

#if UNITY_EDITOR
            if (objDB == null) objDB = CreatePrefab();
#endif

            effectDB = objDB.GetComponent<EffectDB>();
        }

        return effectDB.HeroList;
    }


#if UNITY_EDITOR
    private static GameObject CreatePrefab()
    {
        GameObject obj = new GameObject();
        obj.AddComponent<EffectDB>();
        GameObject prefab = PrefabUtility.CreatePrefab("Assets/TDTK/Resources/DB_TDTK/EffectDB.prefab", obj, ReplacePrefabOptions.ConnectToPrefab);
        DestroyImmediate(obj);
        AssetDatabase.Refresh();
        return prefab;
    }
#endif

}
