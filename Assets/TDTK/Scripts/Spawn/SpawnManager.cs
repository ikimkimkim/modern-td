﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;

namespace TDTK {

    //Условия для начала спавна след волны
    public enum _SpawnMode
    {
        WaveCleared, //Ожидания уничтожения все мобов предыдущей волны
        Continuous, //Не дожидаясь уничтожения предыдущей волны
        ClearedAndTime, //Не дожидаясь уничтожения предыдущей волны и ожидания
    }

    public enum _SpawnDestinationMode
    {
        Destroy,
        Loop
    }

    //Определяет кол волн
    public enum _SpawnLimit {
        Finite, //Конечное число волн
        Infinite, //Бесконечно число волн
        Off, //Никого не спавнить
    }


	public class SpawnManager : MonoBehaviour {
		
		public delegate void NewWaveHandler(int waveID);
		public static event NewWaveHandler onNewWaveE;
		
		public delegate void WaveSpawnedHandler(int time);
		public static event WaveSpawnedHandler onWaveSpawnedE;	//listen by TDTK
		
		public delegate void WaveClearedHandler(int waveID);
		public static event WaveClearedHandler onWaveClearedE;			//listen by TDTK
		
		public delegate void EnableSpawnHandler();
		public static event EnableSpawnHandler onEndWaveSpawnE;	//call to indicate it's ready to spawn next wave (when no longer in the process of actively spawning a wave)
		
		public delegate void SpawnTimerHandler(float time);
		public static event SpawnTimerHandler onSpawnTimerE;		//call to indicate timer refresh for continous spawn

        public delegate void SpawnUnitHandler(Unit unit);
        public static event SpawnUnitHandler onSpawnUnitE;

        public delegate void SpawnEndBossHandler();
        public static event SpawnEndBossHandler onSpawnEndBossE;


		public _SpawnMode SpawnMode = _SpawnMode.Continuous;		
		public _SpawnLimit SpawnLimit = _SpawnLimit.Finite;
        public _SpawnDestinationMode DestinationMode = _SpawnDestinationMode.Destroy;

        public static _SpawnDestinationMode GetDestinationMode { get { return instance.DestinationMode; } }
        public static _SpawnLimit GetSpawnLimit { get { return instance.SpawnLimit; } }

        public bool RandomCreepHP = true;
        public static bool getRandomCreepHP { get { return instance == null ? true : instance.RandomCreepHP; } }

		public bool _isAllowSkip = false;
        public static bool isAllowSkip { get { return instance._isAllowSkip; } }

        public bool autoStart=false;
		public float autoStartDelay=5;
        public float TimeCheckedUnitDeadOrNull = 60;
        public static bool AutoStart(){ return instance==null ? false : instance.autoStart; }
		public static float GetAutoStartDelay(){ return instance.autoStartDelay; }
		
		public bool procedurallyGenerateWave=false;	//used in finite mode, when enabled, all wave is generate procedurally
		
		public PathTD defaultPath;
		private int currentWaveID=-1;			//start at -1, switch to 0 as soon as first wave start, always indicate latest spawned wave's ID
		public bool spawning=false;
		
		public int ActiveUnitCount { get { return ActiveCreeper.Count; } }	//for wave-cleared mode checking
        public static int GetActiveUnitCount { get { return instance.ActiveUnitCount; } }

        [SerializeField] private int _totalSpawnCount=0;	//for creep instanceID
		[SerializeField] private int _waveClearedCount=0;  //for quick checking how many wave has been cleared


        public List<Wave> waveList=new List<Wave>();	//in endless mode, this is use to store temporary wave
		
		public WaveGenerator waveGenerator;
		
		public static SpawnManager instance { get; private set; }
        public List<UnitCreep> ActiveCreeper = new List<UnitCreep>();

        private bool _isBossSpawn = false;
        public static bool isBossSpawn { get { return instance._isBossSpawn; } }
        public List<SubWave> PortalBossList = new List<SubWave>();

        [SerializeField] private bool _overrideCanTargerTower;

        void Awake()
        {
			instance=this;
        }

        void Start()
        {

            if (ActiveCreeper == null)
                ActiveCreeper = new List<UnitCreep>();
            else
                ActiveCreeper.Clear();

            if (defaultPath == null)
            {
                //Debug.Log("DefaultPath on SpawnManager not assigned, auto search for one");
                defaultPath = (PathTD)FindObjectOfType(typeof(PathTD));
            }

            if (procedurallyGenerateWave)
            {
                waveGenerator.CheckPathList();
                if (defaultPath != null && waveGenerator.pathList.Count == 0)
                    waveGenerator.pathList.Add(defaultPath);
            }

            switch (SpawnLimit)
            {
                case _SpawnLimit.Finite:
                    for (int i = 0; i < waveList.Count; i++)
                    {
                        if (procedurallyGenerateWave)
                            waveList[i] = waveGenerator.Generate(i);

                        waveList[i].waveID = i;
                    }
                    break;

                case _SpawnLimit.Infinite:
                    waveList = new List<Wave>();
                    Wave wave = waveGenerator.Generate(0);
                    wave.waveID = 0;
                    waveList.Add(wave);

                    break;
                case _SpawnLimit.Off:
                    return;
            }

            UICreeperWaveInfoManager.ShowInfoWave(waveList[0], true);

            if (autoStart)
                StartCoroutine(AutoStartRoutine());

        }

        void Update()
        {
          /*  if (GameControl.IsGamePlay && Time.time > AttackInstance.TimeLastShot + TimeCheckedUnitDeadOrNull)
            {
                CheckedDeadOrNullUnit();
                AttackInstance.TimeLastShot = Time.time;
            }*/
        }


        IEnumerator AutoStartRoutine()
        {
			yield return new WaitForSeconds(autoStartDelay);
			_StartSpawn();
		}		
		
		void OnEnable()
        {
			Unit.onDestroyedE += OnUnitDestroyed;
			UnitCreep.onDestinationE += OnUnitReachDestination;
		}
		void OnDisable()
        {
			Unit.onDestroyedE -= OnUnitDestroyed;
			UnitCreep.onDestinationE -= OnUnitReachDestination;
		}
		
		
		private void OnUnitDestroyed(Unit unit)
        {
			if(unit.IsCreep == false)
                return;
            if (unit.dead == false)
            {
                Debug.LogError("UnitDestroyed, " + unit.name+" is not dead");
                return;
            }

            if (unit as FakeUnitCreep)
                return;

            MyLog.Log("UnitDestroyed "+unit.name);
			OnUnitCleared(unit as UnitCreep);
		}

        private void OnUnitReachDestination(UnitCreep creep)
        {
            //only execute if creep is dead 
            //when using path-looping the creep would be still active and wouldnt set it's dead flag to true
            AttackInstance.TimeLastShot = Time.time;


            //MyLog.Log("UnitDestination " + creep.name);
            if (DestinationMode == _SpawnDestinationMode.Destroy)
                OnUnitCleared(creep);
        }

		private void OnUnitCleared(UnitCreep creep)
        {
			int waveID = creep.waveID;
			Wave wave=null;

            ActiveCreeper.Remove(creep);

            switch (SpawnLimit)
            {
                case _SpawnLimit.Finite:
                    wave = waveList[waveID];
                    break;

                case _SpawnLimit.Infinite:
                    for (int i = 0; i < waveList.Count; i++)
                    {
                        if (waveList[i].waveID == waveID)
                        {
                            wave = waveList[i];
                            break;
                        }
                    }
                    if (wave == null)
                    {
                        Debug.LogError(creep.name + "|" + creep.unitName + "|" + creep.waveID + " WaveID not find! Current:" + currentWaveID + " and count:" + waveList.Count);

                        wave = waveList.Find(i => i.waveID == creep.waveID);
                        if (wave == null)
                        {
                            Debug.LogError("Not find wave!");
                            return;
                        }
                    }
                    break;
                case _SpawnLimit.Off:
                    return;
            }
		
			wave.activeUnitCount-=1;
            if (wave.isEndSpawn && wave.activeUnitCount == 0)
            {
                wave.isCleared = true;
                _waveClearedCount += 1;
                MyLog.Log("wave " + (waveID) + " is cleared, count " + wave.activeUnitCount);

                ResourceManager.GainResource(wave.rscGainList, PerkManager.GetRscWaveKilled());
                GameControl.GainLife(wave.lifeGain + PerkManager.GetLifeWaveClearedModifier());
                AbilityManager.GainEnergy(wave.energyGain + (int)PerkManager.GetEnergyWaveClearedModifier());

                if (wave.waveID == currentWaveID)
                {
                    switch (SpawnMode)
                    {
                        case _SpawnMode.WaveCleared:
                            if (IsAllWaveCleared() == false)
                                SpawnWave();
                            break;
                        case _SpawnMode.ClearedAndTime:
                            StartCoroutine(WaitNextSpawnWave());
                            break;
                    }
                }

                if (SpawnLimit == _SpawnLimit.Infinite)
                {
                    waveList.Remove(wave);
                    AddCostInfiniteWaveEnd();
                }

                if (onWaveClearedE != null)
                    onWaveClearedE(waveID);
            }
            //else
            //    MyLog.Log("wave" + (waveID) + " is not cleared," + wave.activeUnitCount + " " + wave.isEndSpawn);



        }

        private float _timerSpawnNextWave;
        private bool _isWaitTimer = false;
        private IEnumerator WaitNextSpawnWave()
        {
            if (GameControl.IsGameOver)
                yield break;

            _isWaitTimer = true;
            _timerSpawnNextWave = 30;
            UI.SetLookBuild(true);
            UITimerSpawnManager.Show(_timerSpawnNextWave.ToString());
            UIHUD.SetActiveButtonSpawnNextWave(true);
            float timeStart = Time.unscaledTime;
            while (Time.unscaledTime - timeStart < _timerSpawnNextWave)
            {
                UITimerSpawnManager.SetTime(Mathf.Ceil(_timerSpawnNextWave - (Time.unscaledTime - timeStart)).ToString());
                yield return 0;
            }

            if (_isWaitTimer)
            {
                UIHUD.SetActiveButtonSpawnNextWave(false);
                _isWaitTimer = false;
                if (IsAllWaveCleared() == false)
                    SpawnWave();
            }
            UI.SetLookBuild(false);
            UITimerSpawnManager.Hide();

            yield break;
        }

       /* public void CheckedDeadOrNullUnit()
        {
            bool findCreep = false;
            for (int i = 0; i < ActiveCreeper.Count; i++)
            {
                if (ActiveCreeper[i] == null || ActiveCreeper[i].dead)
                {
                    Debug.LogError(ActiveCreeper[i] != null ? "Found active creep and hi is dead:" + ActiveCreeper[i].dead + " " + ActiveCreeper[i].name + ":" + i
                        : "Found active creep and hi is null");
                    findCreep = true;
                    OnUnitCleared(ActiveCreeper[i]);
                    i--;
                }
                else if (ActiveCreeper[i].dead == false)
                {
                    Debug.LogError("Found active creep not dead." + ActiveCreeper[i].name);
                    ActiveCreeper[i].Dead();
                }
            }
            if (activeUnitCount != ActiveCreeper.Count)
            {
                Debug.LogError("Active unit counter is not equal to list unit count!" + activeUnitCount + ":" + ActiveCreeper.Count);
                activeUnitCount = ActiveCreeper.Count;
            }

            if (findCreep == false)
            {
                if (IsAllWaveCleared() && SpawnMode != _SpawnMode.Portal)
                {
                    if (GameControl.instance.gameState != _GameState.Over && GameControl.instance.gameState != _GameState.Training)
                    {
                        Debug.Log("CheckedDeadOrNullUnit GameWon");
                        GameControl.GameWon();
                    }
                }
                else
                {
                    if (SpawnMode == _SpawnMode.Round && onEnableSpawnE != null) onEnableSpawnE();
                }
            }

            if (!IsAllWaveCleared() && activeUnitCount <= 0 && !spawning)
            {
                if (SpawnMode == _SpawnMode.WaveCleared || SpawnMode == _SpawnMode.Portal || SpawnMode == _SpawnMode.DefenseBase)
                    SpawnWaveFinite();
            }
            else
            {
                Debug.LogWarningFormat("Not Spawn next wave:{0}{1}{2}", !IsAllWaveCleared(), activeUnitCount, spawning);
            }
        }*/


        public void AddCostInfiniteWaveEnd()
        {
            if (BankStarPerk.Active)
                return;
            float[] res = ResourceManager.GetResourceArray();
            float coin = res[0] * 0.1f;
            MyLog.Log("res:" + res[0] + " coin:" + coin);
            ResourceManager.GainIncomeResource(new List<float>() { coin, 0 });
            IncomeRscAnimationText.StartAnim(0.1f);
        }


        public static int AddDestroyedSpawn(UnitCreep unit){ return instance._AddDestroyedSpawn(unit); }
        public int _AddDestroyedSpawn(UnitCreep unit)
        {
            // Debug.LogWarning("AddSpawn " + unit.name);


            if (unit as FakeUnitCreep)
                return -1;

            ActiveCreeper.Add(unit);

            switch(SpawnLimit)
            {
                case _SpawnLimit.Finite:
                    waveList[unit.waveID].activeUnitCount += 1;
                    break;
                case _SpawnLimit.Infinite:
                    for (int i = 0; i < waveList.Count; i++)
                    {
                        if (waveList[i].waveID == unit.waveID)
                        {
                            waveList[i].activeUnitCount += 1;
                            break;
                        }
                    }
                    break;
            }

            return _totalSpawnCount += 1;
        }

		public static void StartSpawn() { instance._StartSpawn(); }
		private void _StartSpawn()
        {
            Debug.Log("StartSpawn");
			if(GameControl.IsGameTrainig==false)
                return;

            // AttackInstance.TimeLastShot = Time.time;
            if (SpawnLimit == _SpawnLimit.Off)
                return;

            switch (SpawnMode)
            {
                case _SpawnMode.Continuous:
                    StartCoroutine(ContinousSpawnRoutine());
                    break;
                case _SpawnMode.WaveCleared:
                    SpawnWave();
                    break;
                case _SpawnMode.ClearedAndTime:
                    UI.SetLookBuild(false);
                    SpawnWave();
                    break;
                default:
                    Debug.LogError("SM Start spawn, mode spawn not find!");
                    break;
            }
		}

        public static void SpawnNextWave() { instance._SpawnNextWave(); }
        private void _SpawnNextWave()
        {
            Debug.Log("SpawnNextWave");
            if (GameControl.IsGamePlay == false)
                return;

            switch (SpawnMode)
            {
                case _SpawnMode.WaveCleared:
                    if (IsAllWaveCleared() == false)
                        SpawnWave();
                    break;
                case _SpawnMode.ClearedAndTime:
                    if (_isWaitTimer)
                        _timerSpawnNextWave = 0;
                    else if (IsAllWaveCleared() == false)
                        SpawnWave();
                    break;
                case _SpawnMode.Continuous:
                    spawnCD = 0;
                    break;
            }

        }


        private float spawnCD=0;
		IEnumerator ContinousSpawnRoutine()
        {
			while(true)
            {
				if(GameControl.IsGameOver)
                    yield break;

                spawnCD = SpawnWave();

				if(SpawnLimit == _SpawnLimit.Finite && currentWaveID >= waveList.Count)
                    break;

                while(spawnCD>0)
                {
                    spawnCD -= Time.deltaTime;
                    yield return null;
                }
                //yield return new WaitForSeconds(spawnCD);
			}
            yield break;
		}
		
		
		private float SpawnWave()
        {
            _isWaitTimer = false;
			if(spawning && _isAllowSkip==false)
                return 0;

            if (_isBossSpawn)
            {
                MyLog.Log("SpawnWaveFinite not spawn, portal boss life");
                return 0;
            }

            Debug.Log("SpawnWaveFinite");

            spawning =true;
			currentWaveID+=1;

            Wave wave = null;
            switch (SpawnLimit)
            {
                case _SpawnLimit.Finite:

                    if (currentWaveID >= waveList.Count)
                    {
                        Debug.LogWarning("SpawnWaveFinite end wave!");
                        return 0;
                    }
                    wave = waveList[currentWaveID];


                    if (waveList.Count > currentWaveID + 1)
                        UICreeperWaveInfoManager.ShowInfoWave(waveList[currentWaveID + 1], false);
                    else
                        UICreeperWaveInfoManager.ShowInfoWave(null, false);

                    break;
                case _SpawnLimit.Infinite:

                    wave = waveGenerator.Generate(currentWaveID + 1);
                    wave.waveID = currentWaveID + 1;
                    waveList.Add(wave);
                    wave = waveList.Find(i => i.waveID == currentWaveID);

                    if (CardAndLevelSelectPanel.Survival)
                        SaveProgressLevel.SaveProgressUnlimited();

                    UICreeperWaveInfoManager.ShowInfoWave(waveList.Find(i => i.waveID == currentWaveID + 1), false);
                    break;
            }

            MyLog.Log("spawning wave"+(currentWaveID+1), MyLog.Type.build);

            if (onNewWaveE!=null)
                onNewWaveE(currentWaveID+1);

            for (int i=0; i<wave.subWaveList.Count; i++)
            {
                StartCoroutine(SpawnSubWave(i,wave.subWaveList[i], wave));
			}

            MyLog.Log("end SpawnSubWave wave-duration:"+wave.duration);

			return wave.duration;
		}
		IEnumerator SpawnSubWave(int index,SubWave subWave, Wave parentWave){

            MyLog.LogWarning("start SpawnSubWave "+ index + " delay:" + subWave.delay);

            yield return new WaitForSeconds(subWave.delay);

            PathTD path=defaultPath;
			if(subWave.path!=null) path=subWave.path;
				
			Vector3 pos=path.GetSpawnPoint().position;
			Quaternion rot=path.GetSpawnPoint().rotation;
			
			int spawnCount=0;

            while (spawnCount < subWave.count)
            {
                //Debug.Log("SpawnSubWave");

                /*if (SpawnMode == _SpawnMode.Portal && scoreDeadUnitCount >= PortalScoreDeadUnityLimit && scoreDeadUnitCount > 0)
                {
                    SpawnBossPortal(true);
                    break;
                }

                if (SpawnMode == _SpawnMode.Portal && isBossSpawn)
                {
                    Debug.Log("SpawnSubWave stop spawn, portal boss life");
                    break;
                }*/

                if (_isBossSpawn)
                    break;

                GameObject obj = ObjectPoolManager.Spawn(subWave.unit, pos, rot);
                UnitCreep unit = obj.GetComponent<UnitCreep>();


                if (subWave.overrideHP > 0) unit.defaultHP = subWave.overrideHP;
                if (subWave.overrideShield > 0) unit.defaultShield = subWave.overrideShield;
                if (subWave.overrideMoveSpd > 0) unit.stats[unit.currentActiveStat].moveSpeed = subWave.overrideMoveSpd;
                if (subWave.overrideSlowIncrement >= 0) unit.slowIncrement = subWave.overrideSlowIncrement;
                if (subWave.overrideSlowTimeIncrement >= 0) unit.slowTimeIncrement = subWave.overrideSlowTimeIncrement;
                if (subWave.overrideStunTimeIncrement >= 0) unit.stunTimeIncrement = subWave.overrideStunTimeIncrement;
                if (unit.CanTargetTower != _overrideCanTargerTower) unit.CanTargetTower = _overrideCanTargerTower;

                if (CardAndLevelSelectPanel.BigPortal)
                    unit.defaultHP = unit.defaultHP * GameControl.instance.HPCreepMultiplier * Mathf.Pow(1.1f, (float)CardAndLevelSelectPanel.LevelDifficultySelect);
                else
                    unit.defaultHP = unit.defaultHP * GameControl.instance.HPCreepMultiplier * Mathf.Pow(1.3f, (float)CardAndLevelSelectPanel.LevelDifficultySelect);

                if (unit.stats != null && unit.stats[0] != null)
                {
                    unit.stats[0].damageMax *= Mathf.Clamp(Mathf.Pow(GameControl.instance.DamageCreepMultiplier, currentWaveID), 1f, 3f);
                    unit.stats[0].damageMin *= Mathf.Clamp(Mathf.Pow(GameControl.instance.DamageCreepMultiplier, currentWaveID), 1f, 3f);
                    //MyLog.Log("New damage:" + unit.stats[0].damageMax);
                }

                unit.Init(path, _totalSpawnCount, parentWave.waveID);

                LevelModification.ApplyModificationUnit(unit);
                
                ActiveCreeper.Add(unit);

                _totalSpawnCount += 1;

                parentWave.activeUnitCount += 1;

                if (onSpawnUnitE != null)
                    onSpawnUnitE(unit);
                spawnCount += 1;
                if (spawnCount == subWave.count)
                    break;

                yield return new WaitForSeconds(subWave.interval/* / TimeScaleManager.BoostIncrement*/);
            }
			
			parentWave.subWaveSpawnedCount+=1;
			if(parentWave.subWaveSpawnedCount>=parentWave.subWaveList.Count)
            {
				parentWave.isEndSpawn=true;
				spawning=false;
				MyLog.Log("wave "+(parentWave.waveID+1)+" has done spawning");
				
                if (onEndWaveSpawnE != null)                
                    onEndWaveSpawnE();
                
			}

            //MyLog.Log("Spawn p:" + parentWave.subWaveSpawnedCount + " count:" + parentWave.subWaveList.Count);
		}

        public static void SpawnBossPortal(bool isEndBoss)
        {
            if (instance == null)
                return;

            instance._SpawnBossPortal(isEndBoss);
        }

        private void _SpawnBossPortal(bool isEndBoss)
        {
            if (_isBossSpawn)
                return;
            if (isEndBoss)
            {
                _isBossSpawn = true;
            }

            if (PortalBossList == null && PortalBossList.Count == 0)
            {
                Debug.LogError("I can't spawn boss, list is null!");
                return;
            }

            if (isEndBoss)
            {
                //Debug.Log("SpawnBossPortal!");
                UICreeperWaveInfoManager.Hide();

                Debug.Log("Kill creep wait boss!");
                if (onSpawnEndBossE != null)
                    onSpawnEndBossE();
                //Если события появление босса не всех убило
                for (int i = 0; i < ActiveCreeper.Count; i++)
                {
                    if(ActiveCreeper[i].isBossPortal==false && ActiveCreeper[i].dead==false)
                    {
                        ActiveCreeper[i].Dead(false);
                    }
                }

                for (int i = 0; i < waveList.Count; i++)
                {
                    waveList[i].activeUnitCount = 0;
                }
            }
            else
                Debug.Log("SpawnBossWave!");

            int index = Random.Range(0, PortalBossList.Count);
            SubWave subWave = PortalBossList[index];
            int count = 0;
            while(subWave.unit==null && count<100)
            {
                Debug.LogWarning("Boss obj is null! Get new boss.");
                index = Random.Range(0, PortalBossList.Count);
                subWave = PortalBossList[index];
                count++;
            }
            if(count>100 && subWave.unit==null)
            {
                Debug.LogError("Not find boss!");
                TooltipMessageServer.Show("Что то пошло не так, не найден босс портала.",float.MaxValue);
                return;
            }

            Debug.Log("Spawn index Boss:" + index);

            PathTD path = defaultPath;
            if (subWave.path != null)
                path = subWave.path;
            Vector3 pos = path.GetSpawnPoint().position;
            Quaternion rot = path.GetSpawnPoint().rotation;

            
            GameObject obj = ObjectPoolManager.Spawn(subWave.unit, pos, rot);
            UnitCreep unit = obj.GetComponent<UnitCreep>();
            unit.isBoss = true;
            unit.isBossPortal = isEndBoss;


            if (subWave.overrideHP > 0) unit.defaultHP = subWave.overrideHP;
            if (subWave.overrideShield > 0) unit.defaultShield = subWave.overrideShield;
            if (subWave.overrideMoveSpd > 0) unit.stats[unit.currentActiveStat].moveSpeed = subWave.overrideMoveSpd;
            if (CardAndLevelSelectPanel.BigPortal)
            {
                if (subWave.overrideLifeCostBigPortal > 0) unit.lifeCost = subWave.overrideLifeCostBigPortal;
            }
            else
            {
                if (subWave.overrideLifeCost > 0) unit.lifeCost = subWave.overrideLifeCost;
            }
            if (subWave.overrideSlowIncrement >= 0) unit.slowIncrement = subWave.overrideSlowIncrement;
            if (subWave.overrideSlowTimeIncrement >= 0) unit.slowTimeIncrement = subWave.overrideSlowTimeIncrement;
            if (subWave.overrideStunTimeIncrement >= 0) unit.stunTimeIncrement = subWave.overrideStunTimeIncrement;


            if (CardAndLevelSelectPanel.BigPortal)
                unit.defaultHP = unit.defaultHP * GameControl.instance.HPCreepMultiplier * Mathf.Pow(1.1f, (float)CardAndLevelSelectPanel.LevelDifficultySelect);
            else
                unit.defaultHP = unit.defaultHP * GameControl.instance.HPCreepMultiplier * Mathf.Pow(1.3f, (float)CardAndLevelSelectPanel.LevelDifficultySelect);

            unit.waveID = currentWaveID;
            int id = AddDestroyedSpawn(unit);
            unit.Init(path,id,currentWaveID);

            LevelModification.ApplyModificationUnit(unit);

            if (onSpawnUnitE != null)
                onSpawnUnitE(unit);
        }
		
		
        public static bool IsAllWaveCleared()
        {
            if (instance == null)
                return true;
            return instance._IsAllWaveCleared();
        }
        private bool _IsAllWaveCleared()
        {
            if (SpawnLimit == _SpawnLimit.Infinite)
                return false;
            else if (SpawnLimit == _SpawnLimit.Off)
                return true;
            //Debug.Log("check all wave cleared   "+instance.waveClearedCount+"   "+instance.waveList.Count);
            return _waveClearedCount >= waveList.Count;
		}
		
		public static int GetTotalWaveCount()
        {
			if(instance==null || instance.SpawnLimit!=_SpawnLimit.Finite)
                return -1;
			return instance.waveList.Count;
		}
		
		public static int GetCurrentWaveID(){ return instance==null ? 0 : instance.currentWaveID; }
	}

}