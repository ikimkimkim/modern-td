﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace TDTK
{

    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(AudioSource))]
    public class UnitHero : Unit, ISelectedUnit
    {
        public static event EventHandler unitHeroDead;

        public event EventHandler unitUpdateXP;
        public event EventHandler unitUpdateLevel;

        [Header("Hero")]
        public bool CanMoveAndShoot;
        public bool bSelected;
        public GameObject SpriteSelectedObj;
        public GameObject PointMoveObj, PointTargetObj;
        public GameObject prefabPointMoveEffect;
        public GameObject prefabEffectLevelUp, prefabEffectSpawn, prefabEffectCastSpall;
        public Transform rangeIndicator;

        private GameObject rangeIndicatorObj;
        private Transform PointMoveT, PointTargetT;
        //public Animator _Animator { get; private set; }
        private AudioSource _Audio;
        public AudioClip clipRespawn,clipDead;
        private int indexSelected;
        public float TimeDeadAnim = 1f;
        
        public float AnimShotTime = 0;
        private int CountPropertiesNeedSelected;
        public bool isNeedUpdate { get { return CountPropertiesNeedSelected > 0; } }

        private CapsuleCollider capsule;

        Projector projectorRange;

        public float XP;
        public float[][] MaxXP = new float[][]
        {
            new float[]
            {
                227,
                341,
                512,
                768,
                1152
            },
            new float[]
            {
                387,
                580,
                870,
                1305,
                1958
            },
            new float[]
            {
                657,
                986,
                1479,
                2219,
                3328
            },
            new float[]
            {
                1118,
                1676,
                2515,
                3772,
                5658
            }
        };

        private LevelHeroUI LevelUI;


        private bool _isImmortal = false;
        public bool IsImmortal { get { return _isImmortal; } }
        private float _timeWaitRespawn;



        public override void Awake()
        {
            gameObject.layer = LayerManager.LayerHero();

            base.Awake();
            //_Animator = GetComponent<Animator>();
            _Audio = GetComponent<AudioSource>();
            _Audio.loop = false;
            _Audio.playOnAwake = false;
            if(PointMoveObj)
                PointMoveT = PointMoveObj.transform;
            if (PointTargetObj)
                PointTargetT = PointTargetObj.transform;

            if (rangeIndicator == null)
            {
                rangeIndicator = (Transform)Instantiate(GameControl.instance.rangeIndicator);
            }
            else
            {
                rangeIndicator = (Transform)Instantiate(rangeIndicator);
            }
            rangeIndicator.parent = thisT;
            rangeIndicatorObj = rangeIndicator.gameObject;
            rangeIndicatorObj.SetActive(false);
            projectorRange = rangeIndicator.GetComponentInChildren<Projector>();
            capsule = GetComponent<CapsuleCollider>();
        }

        public override void OnEnable()
        {
            UnitsVisionSystem.AddUnit(this);
            _Agent.enabled = true;
            base.OnEnable();
        }

       

        protected override void EffectsManager_removeMyEffectE(Unit unit, IDEffect typeEffect)
        {
            if (bSelected)
                UpdateRangePanel();
        }

        protected override void EffectsManager_addMyEffectE(BaseEffect effect)
        {
            if (bSelected)
                UpdateRangePanel();
        }

        public override void OnDisable()
        {
            UnitsVisionSystem.RemoveUnit(this);
            _Agent.enabled = false;
            base.OnDisable();
        }

        public override float GetSpeedMoveAnimation
        {
            get
            {
                if (stunned || isMoveEnd())
                    return 0;
                else
                    return _Agent.velocity.sqrMagnitude;
            }
        }

        LayerMask maskControl;

        ShootObject shell = null;
        Transform startPos = null;

        List<GameObject> DBEffects;
        public void InitHero(int ID, Card card, int levelActive = 0, bool respawn = false)
        {
            instanceID = ID;


            if (stats != null)
            {
                shell = stats[0].shootObject;
                startPos = stats[0].shootObjectT;
                stats.Clear();
            }



            MyCard = card.Clone();
            setStat(MyCard);
            currentActiveStat = levelActive;
            MyCard.LevelTower = currentActiveStat + 1;
            unitName = MyCard.unitName;
            prefabID = MyCard.prefabID;

            TowerSelectProperties.OnReselectProperties(MyCard);

            Init();
            int rscCount = ResourceManager.GetResourceCount();
            for (int i = 0; i < stats.Count; i++)
            {
                UnitStat stat = stats[i];
                if (stat.rscGain.Count != rscCount)
                {
                    while (stat.rscGain.Count < rscCount) stat.rscGain.Add(0);
                    while (stat.rscGain.Count > rscCount) stat.rscGain.RemoveAt(stat.rscGain.Count - 1);
                }
            }

            _Agent.speed = stats[currentActiveStat].moveSpeed;
            defaultHP = stats[currentActiveStat].maxHP;
            fullHP = stats[currentActiveStat].maxHP;
            HP = fullHP;
            XP = 0;

            StartCoroutine(ScanForTargetRoutine());
            StartCoroutine(AtackHero());
            maskControl = 1 << LayerManager.LayerTerrain() | 1 << LayerManager.LayerCreep() | 1 << LayerManager.LayerCreepF();

            //LevelUI = LevelHeroManager.getLevelUI();

            PropertiesInit();

            DBEffects = EffectDB.Load();
            prefabPointMoveEffect = DBEffects[4];
            if (prefabEffectLevelUp == null)
            {
                prefabEffectLevelUp = DBEffects[8];
            }

            if (prefabEffectCastSpall == null)
            {
                prefabEffectCastSpall = DBEffects[10];
            }

            if (respawn)
            {
                if (prefabEffectSpawn == null)
                {
                    prefabEffectSpawn = DBEffects[9];
                }

                GameObject go = GameObject.Instantiate(prefabEffectSpawn);
                go.transform.SetParent(thisT);
                go.transform.position = thisT.position;
                GameObject.Destroy(go, 10);

                if (PlayerManager.isSoundOn())
                {
                    _Audio.clip = clipRespawn;
                    _Audio.Play();
                }
            }

            if (capsule != null)
                SizeMyCollider = capsule.radius * thisT.localScale.x;



            //UIOverlay.NewUnit(this);
        }

        public List<CardProperties> Properties { get { return MyCard.Properties; } }
        private void PropertiesInit()
        {
            CountPropertiesNeedSelected = 0;
            /*if (Properties == null)
            {
                Properties = new List<CardProperties>();
            }

            Properties.Clear();
            foreach (CardProperties prop in MyCard.Properties)
            {
                Properties.Add(prop.Clone());
            }*/

            foreach (CardProperties cp in Properties)
            {
                cp.Apply(this);
                if (cp.ID == CardProperties.IDCoomplect)
                    PropertiesHelp.listCoomplectPropertiesBild.Add(cp);
            }

            PropertiesHelp.ActivePorioerties(Properties.FindAll(i => i.ID != CardProperties.IDCoomplect));
            PropertiesHelp.RefreshAllCoomplectProperties();
        }

        private void setStat(Card card)
        {
            stats.Add(CraftLevelData.GetStatGrade(card));
            stats[stats.Count - 1].shootObject = shell;
            stats[stats.Count - 1].shootObjectT = startPos;
            if (card.NextLevelTower.Count > 0)
            {
                setStat(card.NextLevelTower[0]);
            }
        }

        private void updateCurrentStat()
        {
            float oldMaxHP = stats[currentActiveStat].maxHP;
            if (currentActiveStat > 0)
                oldMaxHP = stats[currentActiveStat - 1].maxHP;
            _Agent.speed = stats[currentActiveStat].moveSpeed;
            defaultHP = stats[currentActiveStat].maxHP;
            fullHP = stats[currentActiveStat].maxHP;
            HP = Mathf.Clamp(HP + fullHP - oldMaxHP, HP, fullHP);
            for (int i = 0; i < Properties.Count; i++)
            {
                Properties[i].Refresh(true);
            }
        }



        public bool ControlLock = false;
        public override void SystemUpdate()
        {
            if (dead) return;
            base.SystemUpdate();

            if (bSelected)
            {
                if(ControlLock==false)
                    Control();

                if (priorityTarget != null && PointTargetT != null)
                {
                    PointTargetT.position = priorityTarget.thisT.position;
                }
            }

            CheckedTargetMe();
        }

        public bool bGrenadeEventAnim = false;
        public void GrenadeEventAnim()
        {
            bGrenadeEventAnim = true;
        }

        Ray r;
        RaycastHit hit;
        Vector3 PointMove;
        private void Control()
        {
            if (Input.GetMouseButtonDown(1))
            {
                MyLog.Log("Click Hero Point");
                r = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(r, out hit, 1000, maskControl))
                {
                    if (hit.collider.gameObject.layer == LayerManager.LayerTerrain())
                    {
                        MyLog.Log("Move Point:" + hit.point);
                        PointMoveObj.SetActive(true);


                        if(UISelected.CountSelectUnit>1)
                        {
                            float angl = 360f * Mathf.Rad2Deg * indexSelected / UISelected.CountSelectUnit;
                            float range =  1.2f;
                            PointMove = hit.point + new Vector3(range * Mathf.Cos(angl), 0, range * Mathf.Sin(angl));
                            //Debug.Log(UISelected.CountSelectUnit + " angl " +angl + " index:"+ indexSelected);
                        }
                        else
                        {
                            PointMove = hit.point;
                        }

                        _Agent.isStopped = false;
                        _Agent.SetDestination(PointMove);
                        priorityTarget = null;

                        GameObject go = GameObject.Instantiate(prefabPointMoveEffect);
                        go.transform.position = PointMove + Vector3.up * 0.1f;
                        GameObject.Destroy(go, 5);
                    }
                    else
                    {
                        MyLog.Log("Set target Point:" + hit.transform.name);
                        priorityTarget = hit.transform.GetComponent<Unit>();
                        if (priorityTarget != null)
                            PointMoveObj.SetActive(false);
                    }
                    PointTargetObj.SetActive(priorityTarget != null);
                }
            }

            MoveToTarget();


            PointMoveT.position = _Agent.pathEndPosition;
        }

        float distTarget;
        private void MoveToTarget()
        {
            if (priorityTarget != null)
            {
                if (priorityTarget.dead == true)
                {
                    priorityTarget = null;
                    PointTargetObj.SetActive(false);
                    _Agent.isStopped = false;
                    PointMove = thisT.position;
                    _Agent.SetDestination(PointMove);
                    return;
                }
                distTarget = getDistanceFromTarget(priorityTarget, thisT.position);// Vector3.Distance(priorityTarget.thisT.position, thisT.position);
                if (distTarget > GetRange())
                {
                    _Agent.isStopped = false;
                    _Agent.SetDestination(priorityTarget.thisT.position);
                }
                else
                {
                    _Agent.SetDestination(thisT.position);
                }
            }
        }

        public override void Dead(bool getRsc = false)
        {
            if (dead)
            {
                MyLog.LogWarning("UnitHero " + name + " dead is now dead!");
                return;
            }
            dead = true;

            MyLog.LogWarning("UnitHero " + name + " dead rsc!" + getRsc);

            if (thisObj.activeSelf)
                _Agent.isStopped = true;

            if (PlayerManager.isSoundOn())
            {
                _Audio.clip = clipDead;
                _Audio.Play();
            }


            StartDestroyedInvoke();

            animationUnit.PlayDead();

            if (_isImmortal)
            {
                StartCoroutine(ImmortalRespawn());
            }



            if (unitHeroDead != null)
                unitHeroDead(this, null);
        }

        public override void DeadEndAnimEvent()
        {
            if (_isImmortal)
            {
                return;
            }



            if (target != null && target.dead == false && target.IsHero)
                target.TargetMe.Remove(this);

            ClearProperties();


            base.DeadEndAnimEvent();
        }

        private IEnumerator ImmortalRespawn()
        {
            Debug.Log("Start wait respawn");
            yield return new WaitForSeconds(_timeWaitRespawn);
            if(dead)
                Recovery();
            yield break;
        }

        private void Recovery()
        {
            Debug.Log("Start Recovery");
            animationUnit.EndDead();
            AddHeal(fullHP);
            dead = false;
        }

        public void Selected()
        {
            if (dead)
                return;

            bSelected = true;
            SpriteSelectedObj.SetActive(bSelected);
            PointMoveObj.SetActive(bSelected);
            PointTargetObj.SetActive(priorityTarget != null);
            PointMoveT.position = _Agent.pathEndPosition;
            indexSelected = UISelected.CountSelectUnit;
            UISelected.CountSelectUnit++;
            SupportUI.SelectedHero(this);
            UpdateRangePanel();
            rangeIndicatorObj.SetActive(true);
        }

        private void UpdateRangePanel()
        {
            float range = GetRange();
            rangeIndicator.position = thisT.position;
            rangeIndicator.localScale = new Vector3(2 * range, 1, 2 * range);
            if (projectorRange != null)
            {
                projectorRange.orthographicSize = range+0.1f;
            }
        }

        public void Deselected()
        {
            bSelected = false;
            SpriteSelectedObj.SetActive(bSelected);
            PointMoveObj.SetActive(bSelected);
            PointTargetObj.SetActive(priorityTarget != null);
            UISelected.CountSelectUnit--;
            indexSelected = 0;
            SupportUI.DeSelectedHero(this);
            rangeIndicatorObj.SetActive(false);
        }

        public void CastSpell(int idDBEffect)
        {
            CastSpell(DBEffects[idDBEffect]);
        }
        public void CastSpell()
        {
            CastSpell(prefabEffectCastSpall);
        }
        public void CastSpell(GameObject prefab)
        {
            GameObject go = GameObject.Instantiate(prefab);
            go.transform.SetParent(thisT);
            go.transform.position = thisT.position;
            GameObject.Destroy(go, 10);
        }

        public bool isAnimShotDrop = false;
        AttackInstance attInstance;
        public IEnumerator AtackHero()
        {
            for (int i = 0; i < shootPoints.Count; i++)
            {
                if (shootPoints[i] == null) { shootPoints.RemoveAt(i); i -= 1; }
            }

            if (shootPoints.Count == 0)
            {
                Debug.LogWarning("ShootPoint not assigned for unit - " + unitName + ", auto assigned", this);
                shootPoints.Add(thisT);
            }

            for (int i = 0; i < stats.Count; i++)
            {
                if (stats[i].shootObjectT != null) ObjectPoolManager.New(stats[i].shootObjectT.gameObject, 3);
            }

            yield return null;
            float cd = 0;
            float dist;

            while (true)
            {
                if (dead)
                {
                    animationUnit.EndShootMecanim();
                    //_Animator.SetBool("Shoting", false);
                    yield return null;
                    continue;
                }

                if (PropertiesAttakLock || target == null || stunned || IsInConstruction() 
                    || !targetInLOS || GameControl.GetGameState() != _GameState.Play || (CanMoveAndShoot==false && isMoveEnd()==false))
                {
                    //MyLog.Log(name + " stunned: " + stunned + " IsInConstruction:" + IsInConstruction() + " !targetInLOS:" + !targetInLOS + " move:"+isMoveEnd());
                    if (ListTarget.Count > 0 && (CanMoveAndShoot || isMoveEnd()) && PropertiesAttakLock == false)
                    {
                        //Debug.Log("Scan!");
                        ScanForTarget();
                    }
                    else
                    {
                        //Debug.Log("EndShoot!");
                        animationUnit.EndShootMecanim();
                    }
                    //_Animator.SetBool("Shoting", false);
                    yield return null;
                }
                else
                {
                    dist = getDistanceFromTarget();
                    if (target.dead || dist > GetRange())
                    {

                        //MyLog.Log(name + " continue dist: " + dist+">"+GetRange());
                        ScanForTarget();
                        animationUnit.EndShootMecanim();
                        //_Animator.SetBool("Shoting", false);
                        yield return null;
                        continue;
                    }

                    turretOnCooldown = true;
                    float animationDelay = 0;



                    thisT.LookAt(target.getPosition);

                    WaitShootEvent = true;
                    animationUnit.StartShootMecanim();
                    //_Animator.SetBool("Shoting", true);
                    for (int g = 0; g < stats[0].CountAttakcs; g++)
                    {
                        Unit currentTarget = target;



                        attInstance = new AttackInstance();
                        attInstance.srcUnit = this;
                        attInstance.tgtUnit = currentTarget;




                        for (int i = 0; i < shootPoints.Count; i++)
                        {
                            animationUnit.PlayShoot();
                            //_Animator.SetTrigger("Shot");
                            while (WaitShootEvent && currentTarget != null)
                            {
                                if (dead == true)
                                {
                                    //Debug.LogWarning("Lock dead " + name);
                                    yield break;
                                }
                                if (CanMoveAndShoot || isMoveEnd())
                                {
                                    //Debug.Log("Wait Shoot, lookAt...");
                                    thisT.LookAt(currentTarget.getPosition);
                                }
                                yield return null;
                            }
                            Transform sp = shootPoints[i];
                            Transform objT = ObjectPoolManager.Spawn(GetShootObjectT(), sp.position, sp.rotation);  // (Transform)Instantiate(GetShootObjectT(), sp.position, sp.rotation);
                            ShootObject shootObj = objT.GetComponent<ShootObject>();
                            shootObj.Shoot(attInstance, sp);
                            if (onShotTurretE != null)
                                onShotTurretE(currentTarget);

                            if (delayBetweenShootPoint > 0) yield return new WaitForSeconds(delayBetweenShootPoint);
                            WaitShootEvent = true;

                        }
                    }

                    cd = (GetCooldown() - animationDelay - shootPoints.Count * delayBetweenShootPoint);

                    yield return new WaitForSeconds(AnimShotTime);

                    if(isAnimShotDrop || PropertiesAttakLock)
                    {
                        animationUnit.EndShootMecanim();
                        //_Animator.SetBool("Shoting", false);
                        yield return new WaitForSeconds(cd - AnimShotTime - Time.deltaTime);
                    }
                    else
                    {
                        float nowcd = cd - AnimShotTime - Time.deltaTime;
                        while(nowcd>0)
                        {                      
                            yield return null;
                            nowcd -= Time.deltaTime;
                            if (ListTarget.Count == 0 || (ListTarget.Count == 1 && ListTarget[0].dead == true))
                                animationUnit.EndShootMecanim();
                            //_Animator.SetBool("Shoting", false);
                        }
                    }
                    turretOnCooldown = false;

                    ScanForTarget();
                }
            }
        }

        public GameObject GrenadesEffect;

        public Transform GrenadesShootPoint;
        public float GrenadesDistance = 1;

        public void StartActiveProperties()
        {
            if (CountPropertiesNeedSelected <= 0)
                return;

            MyCard.LevelTower++;
            TowerSelectProperties.initShow(MyCard, UpLevelHero);
        }

        public void UpLevelHero(bool selectedProperties)
        {
            if (selectedProperties == false)
            {
                MyCard.LevelTower--;
                return;
            }

            updateCurrentStat();
            CountPropertiesNeedSelected--;
            if (CountPropertiesNeedSelected > 0)
            {
                StartActiveProperties();
            }
            else
            {
                if (unitUpdateXP != null)
                    unitUpdateXP(this, null);
            }
        }

        public void addXP(float add)
        {
            XP += add * XPIncrement;
            if (XP >= MaxXP[MyCard.getIntRareness][currentActiveStat])
            {
                if (ReadyToBeUpgrade())
                {
                    XP -= MaxXP[MyCard.getIntRareness][currentActiveStat];
                    currentActiveStat++;

                    if (TowerSelectProperties.HaveSelectedProperties(MyCard, currentActiveStat + 1) || CountPropertiesNeedSelected>0)
                        CountPropertiesNeedSelected++;
                    else if(CountPropertiesNeedSelected<=0)//для старой лоогики, потом можно удалить
                    {
                        MyCard.LevelTower= currentActiveStat + 1;
                    }

                    updateCurrentStat();

                    GameObject go = GameObject.Instantiate(prefabEffectLevelUp, thisT);
                    go.transform.position = thisT.position;
                    GameObject.Destroy(go, 10);

                    UpdateRangePanel();

                    

                    if (unitUpdateLevel != null)
                        unitUpdateLevel(this, null);
                }
                else
                {
                    //Debug.Log("Hero Max Level!");
                }
            }

            if (unitUpdateXP != null)
                unitUpdateXP(this, null);
        }

        public bool ReadyToBeUpgrade()
        {
            if (currentActiveStat < stats.Count - 1) return true;

            if (MyCard == null)
            {
                Debug.Log("Hero ReadyToBeUpgrade MyCard is null");

            }
            return false;
        }

        public bool healing = false;
        public bool castAbility = false;
        public float getWeightHealing { get { return WeightHealing; } }
        private float WeightHealing = 0;
        private bool healingDown = false;
        private bool CastDown = false;

        private Vector3 addVectorRHealing = new Vector3(0.7f,1.8f,0);
        private Vector3 addVectorRCast = new Vector3(0.7f, 1.8f, 0);
        private Vector3 addVectorLCast = new Vector3(-0.7f, 1.8f, 0);
        private void OnAnimatorIK(int layerIndex)
        {
            if (healing || castAbility)
            {
                WeightHealing = Mathf.Clamp(WeightHealing + Time.deltaTime * 2, 0, 1);
            }
            else
            {
                WeightHealing = Mathf.Clamp(WeightHealing - Time.deltaTime * 2, 0, 1);
            }

            animationUnit.anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, WeightHealing);
            animationUnit.anim.SetIKPositionWeight(AvatarIKGoal.RightHand, WeightHealing);

            if (healing || healingDown)
            {
                healingDown = healing? true : WeightHealing>0;
                animationUnit.anim.SetIKPosition(AvatarIKGoal.RightHand, thisT.position + thisT.right * addVectorRHealing.x + Vector3.up * addVectorRHealing.y);
            }

            if (castAbility || CastDown)
            {
                CastDown = castAbility ? true : WeightHealing > 0;
                animationUnit.anim.SetIKPosition(AvatarIKGoal.RightHand, thisT.position + thisT.right * addVectorRCast.x + Vector3.up * addVectorRCast.y);
                animationUnit.anim.SetIKPosition(AvatarIKGoal.LeftHand, thisT.position + thisT.right * addVectorLCast.x + Vector3.up * addVectorLCast.y);
            }

        }




        public List<float> GetCost(int ID = 0)
        {
            int index = Properties.FindIndex(i => i.ID == 16);

            List<float> cost = new List<float>();
            for (int i = 0; i < stats[ID].cost.Count; i++)
            {
                if (index >= 0)
                    cost.Add(stats[ID].cost[i] - stats[ID].cost[i] * (float)Properties[index].SetParam[0] / 100f);
                else
                    cost.Add(stats[ID].cost[i]);
            }

            return cost;
        }


        public void ImmortalityOn(float timeOff)
        {
            _isImmortal = true;
            _timeWaitRespawn = timeOff;
        }

        public void ImmortalityOff()
        {
            _isImmortal = false;
        }

    }
}