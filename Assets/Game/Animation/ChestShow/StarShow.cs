﻿using UnityEngine;
using System.Collections;

public class StarShow : MonoBehaviour {

    public ChestManagerV2 chestManagetV2;

    public void AddStar()
    {
        chestManagetV2.AddStarAnim();
    }

    public void End()
    {
        chestManagetV2.EndAnim();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
