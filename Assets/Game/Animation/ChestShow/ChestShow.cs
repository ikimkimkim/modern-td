﻿using UnityEngine;
using System.Collections;

public class ChestShow : MonoBehaviour {

    public ChestManager CM;

    public void OnChestShow()
    {
        CM.SetLastChest();
        gameObject.SetActive(false);
    }

}
