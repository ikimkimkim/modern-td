﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TopPanel : MonoBehaviour {

    //public GameObject TablePanel;
    public ToggleGroup toggleGroup;
    public Toggle toggleSurvive, togglePortal;

    public GameObject Loading;
    public Dropdown dropdownMap;
    public Image iconMap;
    public Scrollbar scroll;

    public Transform Table;
    public GameObject prefabUserRow;

    public Transform TableButton;
    public Sprite[] Button;
    //public Color TextPageDef, TextPageSelect;
    public GameObject prefabButton;

    public GameObject objBestMyTop;
    public Text NowMyTop, BestMyTopPlace, BestMyTopData;

    public Color colorRow;

    public GameObject topPanelSurvave, topPanelPassage;

    public TopData data;
    
    private List<GameObject> Rows = new List<GameObject>();

    public MapType IDType=MapType.Portal;
    public int IDMap = 1;

    public Toggle[] listToggle;
	

	void OnEnable()
    {
        MyLog.Log("TopPanel");

        if(CardAndLevelSelectPanel.IDMap>0)
        IDMap = CardAndLevelSelectPanel.IDMap;

        toggleGroup.RegisterToggle(togglePortal);
        toggleGroup.RegisterToggle(toggleSurvive);

        if (CardAndLevelSelectPanel.BigPortal)
        {
            togglePortal.isOn = false;
            togglePortal.isOn = true;
            toggleGroup.NotifyToggleOn(togglePortal);
        }
        else
        {
            toggleSurvive.isOn = false;
            toggleSurvive.isOn = true;
            toggleGroup.NotifyToggleOn(toggleSurvive);
        }

        MyLog.Log("Set drop:"+IDMap);

        dropdownMap.value = IDMap-1;
        dropdownMap.RefreshShownValue();

    }

    public void UpdateChecked(bool value)
    {
        MyLog.Log("UC:" + value);
        if (!value)
            return;
        for (int i = 0; i < listToggle.Length; i++)
        {
            if(listToggle[i].isOn)
            {
                IDType = (MapType) i -2;
            }
        }
        UpdateData();

        dropdownMap.ClearOptions();
        List<string> list = new List<string>();
        list.AddRange(TopManager.getNamesMap(IDType));

        dropdownMap.AddOptions(list);
    }

    public void UpdateMap(int id)
    {
        MyLog.Log("Update dropdown:"+id);
        IDMap = id+1;
        UpdateData();
    }

    public void UpdateData(int page=0)
    {
        MyLog.Log("UpdateData type:" + IDType+" map:"+IDMap+" page:"+page);
        Loading.SetActive(true);
        PlayerManager.DM.GreatAdvTop((int)IDType, IDMap, page, GetData);
    }

    public void GetData(TopData topData)
    {
        Loading.SetActive(false);
        scroll.value = 1;
        for (int i = 0; i < Rows.Count; i++)
        {
            GameObject.Destroy(Rows[i]);
        }
        Rows.Clear();

        data = topData;
        ITopUserRow tur;
        GameObject go;
        System.DateTime dt = new System.DateTime();

        topPanelPassage.SetActive(IDType == MapType.Portal);
        topPanelSurvave.SetActive(IDType == MapType.Survival);

        iconMap.sprite = TopManager.getIconMap(IDMap, IDType);
        StartCoroutine(TextureDB.instance.load(TopManager.getUrlIconMap(IDMap, IDType), (Sprite value) => { iconMap.sprite = value; }));


        for (int i = 0; i < topData.result.top_data.top.Length; i++)
        {
            go = GameObject.Instantiate(prefabUserRow, Table);
            go.transform.localScale = new Vector3(1, 1, 1);
            tur = go.GetComponent<ITopUserRow>();

            if(i%2!=0)
            {
                tur.SetFonColor(colorRow);
            }

            tur.SetData(topData.result.top_data.top[i], IDType);

            
            Rows.Add(go);
        }

        Text PageText;
        for (int i = 0; i < topData.result.top_data.pages.Length; i++)
        {
            go = GameObject.Instantiate(prefabButton, TableButton); 
            go.transform.localScale = new Vector3(1, 1, 1);
            if (i==0)
            {
                go.GetComponent<Image>().sprite =/* i == PageIndex ? Button[3] :*/ Button[0];
            }
            else if(i== topData.result.top_data.pages.Length-1)
            {
                go.GetComponent<Image>().sprite = Button[2];
            }
            PageText = go.GetComponentInChildren<Text>();
            PageText.text = topData.result.top_data.pages[i];
            int index = i;
            go.GetComponent<Button>().interactable = i!=PageIndex;
            go.GetComponent<Button>().onClick.AddListener(()=> {  LoadPage(index); });

            Rows.Add(go);
        }

        if (PlayerManager._instance.GetLevelsCountComplele() > 9)
        {
            if (/*topData.result.user_data.now != null && */topData.result.user_data.now != 0)
            {
                NowMyTop.text = "Ваше текущее место - " + topData.result.user_data.now;
            }
            else
            {
                NowMyTop.text = "Сейчас вы не входите в ТОП-100";
            }

            if (topData.result.user_data.best != null)
            {
                objBestMyTop.SetActive(true);
                dt = System.DateTime.Parse(topData.result.user_data.best.datetime);
                BestMyTopPlace.text = topData.result.user_data.best.place;
                BestMyTopData.text = dt.ToString("HH:mm dd.MM.yyyy");
            }
            else
            {
                objBestMyTop.SetActive(false);
            }
        }
        else
        {
            NowMyTop.text = "Перед вами список лучших в испытании";
            objBestMyTop.SetActive(false);
        }

        
    }



    private int PageIndex;

    public void LoadPage(int index)
    {
        PageIndex = index;
        UpdateData(index);
    }

}
