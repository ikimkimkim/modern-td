﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



[System.Serializable]
public class TopData
{
    public TopDataResult result;
}

[System.Serializable]
public class TopDataResult
{

    public MainTopData top_data;
    public MyTopData user_data;
}

[System.Serializable]
public class MyTopData
{
    public BestTopUsersInfoData best;
    public int now;
}

[System.Serializable]
public class MainTopData
{
    public infoTopData info;
    public string[] pages;
    public TopUsersInfoData[] top;
}

public class infoTopData
{
    public string start;
    public string end;
    public string start_text;
    public string end_text;
    public string short_text;
    public int map_id;
}


[System.Serializable]
public class TopUsersInfoData
{
    //"diff":5,"time":896,"hp_left":2,"date":"2018-02-21 07:15:46","place":1
    public string user_id;
    public string datetime;
    public string date;
    public string scores;
    public string map_id;
    public string seconds;  //сколько было потрачено на прохождение
    public string level; //уровень сложности начиная с 1
    public string hp_left;// сколько ХП осталось
    public int place;
    public TopVKUserData vk_user;
}

[System.Serializable]
public class BestTopUsersInfoData
{
    public string user_id;
    public string datetime;
    public string seconds;  //сколько было потрачено на прохождение
    public string level; //уровень сложности начиная с 1
    public string hp_left;// сколько ХП осталось
    public string place;
    public TopVKUserData vk_user;
}

public class TopVKUserData
{
    public string id;
    public string name;
    public string ava;

}
