﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public enum MapType
{
    Test = -3,
    Portal = -2,
    Survival =-1,
    Company=0
}


public class TopManager : UIEntity {
    

    private static MapDB.MapData[] MapUnlimitedData;
    private static MapDB.MapData[] MapPortalData;

    public static string[] getNamesMap(MapType type)
    {
        string[] names;
        switch (type)
        {
            case MapType.Portal:
                names = new string[MapPortalData.Length];
                for (int i = 0; i < MapPortalData.Length; i++)
                {
                    names[i] = MapPortalData[i].Name;
                }
                return names;
            case MapType.Survival:
                names = new string[MapUnlimitedData.Length];
                for (int i = 0; i < MapUnlimitedData.Length; i++)
                {
                    names[i] = MapUnlimitedData[i].Name;
                }
                return names;
            default:
                return null;
        }
    }

    public static string getNameMap(int id, MapType type)
    {
        switch (type)
        {
            case MapType.Portal:
                if (id < 1 || id > MapPortalData.Length)
                    return "Not name id:" + id;
                else
                    return MapPortalData[id - 1].Name;
            case MapType.Survival:
                if (id < 1 || id > MapUnlimitedData.Length)
                    return "Not name id:" + id;
                else
                    return MapUnlimitedData[id - 1].Name;
            default:
                return "Not name type" + type;
        }
    }

    public static Sprite getIconMap(int id, MapType type)
    {
        switch (type)
        {
            case MapType.Portal:
                if (id < 1 || id > MapPortalData.Length)
                    return null;
                else
                    return MapPortalData[id - 1].Icon;
            case MapType.Survival:
                if (id < 1 || id > MapUnlimitedData.Length)
                    return null;
                else
                    return MapUnlimitedData[id - 1].Icon;
            default:
                return null;
        }
    }

    public static string getUrlIconMap(int id, MapType type)
    {
        switch (type)
        {
            case MapType.Portal:
                if (id < 1 || id > MapPortalData.Length)
                    return "";
                else
                    return MapPortalData[id - 1].getUrlIcon;
            case MapType.Survival:
                if (id < 1 || id > MapUnlimitedData.Length)
                    return "";
                else
                    return MapUnlimitedData[id - 1].getUrlIcon;
            default:
                return "";
        }
    }

    public static string getBundleMap(int id,MapType type)
    {
        switch (type)
        {
            case MapType.Portal:
                if (id < 1 || id > MapPortalData.Length)
                    throw new ArgumentOutOfRangeException("getBundleScane", "id");
                else
                    return MapPortalData[id-1].NameBundle;
            case MapType.Survival:
                if (id < 1 || id > MapUnlimitedData.Length)
                    throw new ArgumentOutOfRangeException("getBundleScane", "id");
                else
                    return MapUnlimitedData[id-1].NameBundle;
            default:
                Debug.LogError("TM -> Not find type:" + type);
                throw new ArgumentException("getBundleRandomScane", "MapType");
        }
    }

    [SerializeField] private MapDB.MapData[] MapUnlimited;
    [SerializeField] private MapDB.MapData[] MapPortal;

    public static TopManager instance { get; private set; }

    public override int entityID { get { return 2; } }

    public override int priorityShow { get { return 300; } }

    public GameObject  Fon, Promo, Top, TopDay, Portal,TopDayPrize;
    public Button ButtonPortal, ButtonTop;

    bool bShowPromo;

    public static bool great_new_place;

    public override void Awake()
    {
        instance = this;
        base.Awake();
        MapUnlimitedData = MapUnlimited;
        MapPortalData = MapPortal;
        bShowPromo = true;
    }

    void Start()
    {
        ButtonPortal.interactable = PlayerManager._instance.GetLevelsCountComplele() > 8;
    }

    public override bool NeedShow()
    {
        if (great_new_place)
        {
            great_new_place = false;
            ShowTop();
            return true;
        }

        foreach (var top in PlayerManager.topDaysData.last_top_info)
        {
            //Debug.Log(top.Key + ":" + top.Value.prize_geted);
            if (top.Value.prize_geted != 1 && top.Value.user_res.place > 0)
            {
                ShowTopDayPrize();
                return true;
            }
        }

        if (bShowPromo && PlayerManager._instance.GetLevelsCountComplele() == 9 && CardAndLevelSelectPanel.isRefreshData == false &&
                (PlayerManager._instance.LastIdScenes == 0 || PlayerManager._instance.LastIdScenes == 8))
        {
            ShowPromo();
            bShowPromo = false;
            return true;
        }

        return false;
    }


    public void Hide()
    {
        if (Fon.activeSelf)
            Fon.SetActive(false);
        if (Promo.activeSelf)
            Promo.SetActive(false);
        if (Top.activeSelf)
            Top.SetActive(false);
        if (Portal.activeSelf)
            Portal.SetActive(false);
        if (TopDay.activeSelf)
            TopDay.SetActive(false);
        if (TopDayPrize.activeSelf)
            TopDayPrize.SetActive(false);
        TopInfoPlayerPanel.instance.Hide();
        HideUIEvent();
    }

    public void ShowPromo()
    {
        if (Top.activeSelf)
            Top.SetActive(false);
        if (Portal.activeSelf)
            Portal.SetActive(false);
        if (TopDay.activeSelf)
            TopDay.SetActive(false);
        if (TopDayPrize.activeSelf)
            TopDayPrize.SetActive(false);
        if (Promo.activeSelf == false)
            Promo.SetActive(true);
        if(Fon.activeSelf==false)
            Fon.SetActive(true);
        ShowUIEvent();
    }

    public void ShowTop()
    {
        if (Promo.activeSelf)
            Promo.SetActive(false);
        if (Portal.activeSelf)
            Portal.SetActive(false);
        if (TopDay.activeSelf)
            TopDay.SetActive(false);
        if (TopDayPrize.activeSelf)
            TopDayPrize.SetActive(false);
        if (Top.activeSelf == false)
            Top.SetActive(true);
        if (Fon.activeSelf == false)
            Fon.SetActive(true);
        ShowUIEvent();
    }

    public void ShowTopDayDeff()
    {
        UITopDayPanel.ShowTop = false;
        if(DateTime.Parse(PlayerManager.topDaysData.now_info["-1"].dates.start) < DateTime.Parse(PlayerManager.topDaysData.now_info["-2"].dates.start))
            UITopDayPanel.TypeShow = -1;
        else
            UITopDayPanel.TypeShow = -2;

        ShowTopDay();
    }
    public void ShowTopDay()
    {
        if (Promo.activeSelf)
            Promo.SetActive(false);
        if (Top.activeSelf)
            Top.SetActive(false);
        if (Portal.activeSelf)
            Portal.SetActive(false);
        if (TopDayPrize.activeSelf)
            TopDayPrize.SetActive(false);
        if (TopDay.activeSelf == false)
            TopDay.SetActive(true);
        if (Fon.activeSelf == false)
            Fon.SetActive(true);
        ShowUIEvent();
    }

    public void ShowPortal()
    {
        if (Promo.activeSelf)
            Promo.SetActive(false);
        if (Top.activeSelf)
            Top.SetActive(false);
        if (TopDay.activeSelf)
            TopDay.SetActive(false);
        if (TopDayPrize.activeSelf)
            TopDayPrize.SetActive(false);
        if (Portal.activeSelf == false)
            Portal.SetActive(true);
        if (Fon.activeSelf == false)
            Fon.SetActive(true);
        ShowUIEvent();
    }

    public void ShowTopDayPrize()
    {
        if (Promo.activeSelf)
            Promo.SetActive(false);
        if (Top.activeSelf)
            Top.SetActive(false);
        if (Portal.activeSelf)
            Portal.SetActive(false);
        if (TopDay.activeSelf)
            TopDay.SetActive(false);
        if (TopDayPrize.activeSelf == false)
            TopDayPrize.SetActive(true);
        if (Fon.activeSelf == false)
            Fon.SetActive(true);
        ShowUIEvent();
    }

    void Update()
    {
        if (CardUIAllInfo.isShow == false && Fon.activeSelf == true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Hide();
            }
        }
    }

    
}
