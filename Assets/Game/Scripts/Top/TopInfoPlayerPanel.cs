﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TopInfoPlayerPanel : MonoBehaviour {

    public static TopInfoPlayerPanel instance;

    public GameObject panel;
    private RectTransform TPanel;
    public FrameFriend Ava;

    public GameObject ScoreObj;
    public GameObject TimeObj, LevelObj, HPObj;
    public Text UserName, Place, Score, Level, Left_HP, Time;

    void Awake()
    {
        instance = this;
        TPanel = GetComponent<RectTransform>();
    }

    public void Show(TopUsersInfoData InfoData,MapType type)
    {
        panel.SetActive(true);
        /* var screenPoint = Vector3(Input.mousePosition);
         screenPoint.z = 10.0f; 
         transform.position = Camera.main.ScreenToWorldPoint(screenPoint);*/
        Vector3 locPos = Input.mousePosition;
        locPos.z = 100;
        locPos = Camera.main.ScreenToWorldPoint(locPos);
        TPanel.position = locPos;

        locPos = TPanel.localPosition;
        locPos.z = 0;
        TPanel.localPosition = locPos;


        Ava.SetFriendAva(InfoData.vk_user);

        ScoreObj.SetActive(type == MapType.Survival);
        TimeObj.SetActive(type == MapType.Portal);
        LevelObj.SetActive(type == MapType.Portal);
        HPObj.SetActive(type == MapType.Portal);


        switch (type)
        {
            case MapType.Portal:
                System.TimeSpan ts;
                if (InfoData.seconds != "" && InfoData.seconds != string.Empty && InfoData.seconds != null)
                    ts = new System.TimeSpan(0, 0, int.Parse(InfoData.seconds));
                else
                    ts = new System.TimeSpan(0, 59, 59);
                

                float m = ts.Minutes % 10f;
                if (m == 1 && ts.Minutes != 11 && ts.Minutes != 12 && ts.Minutes != 13 && ts.Minutes != 14)
                {
                    Time.text = ts.Minutes + " минута ";
                }
                else if (m > 1 && m < 5 && ts.Minutes != 11 && ts.Minutes != 12 && ts.Minutes != 13 && ts.Minutes != 14)
                {
                    Time.text = ts.Minutes + " минуты ";
                }
                else
                {
                    Time.text = ts.Minutes + " минут ";
                }

                float s = ts.Seconds % 10f;
                if (s == 1 && ts.Seconds != 11 && ts.Seconds != 12 && ts.Seconds != 13 && ts.Seconds != 14)
                {
                    Time.text += ts.Seconds + " секунда";
                }
                else if (s > 1 && s < 5 && ts.Seconds != 11 && ts.Seconds != 12 && ts.Seconds != 13 && ts.Seconds != 14)
                {
                    Time.text += ts.Seconds + " секунды";
                }
                else
                {
                    Time.text += ts.Seconds + " секунд";
                }
                break;
            case MapType.Survival:
                Score.text = InfoData.scores;
                break;
        }

        UserName.text = InfoData.vk_user.name;
        Left_HP.text = InfoData.hp_left;
        Place.text = InfoData.place.ToString();
        Level.text = InfoData.level;

    }

    public void Hide()
    {
        panel.SetActive(false);
    }
    
}
