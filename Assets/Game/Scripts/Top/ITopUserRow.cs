﻿using UnityEngine;
using System.Collections;



public interface ITopUserRow  {


    void SetData(TopUsersInfoData infoData, MapType type);

    void SetFonColor(Color color);
}
