﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class TopDaysData
{
    public Dictionary<string, LastTopInfo> last_top_info;
    public Dictionary<string, NowInfo> now_info;
    public PrizesInfo[] prizes;
    [System.Serializable]
    public class LastTopInfo
    {
        public Dates dates;
        public int map_id;
        public UserData user_res;
        public LastTopPrize[] prizes;
        public int prize_geted;
    }
    [System.Serializable]
    public class LastTopPrize
    {
        public string type;
        public int data;
    }
    [System.Serializable]
    public class NowInfo
    {
        public Dates dates;
        public int map_id;
        public UserData user_res;
    }
    [System.Serializable]
    public class Dates
    {
        public string start;
        public string end;
        public string start_text;
        public string end_text;
        public string short_text;
    }
    [System.Serializable]
    public class UserData
    {
        public int e;
        public int place;
        public PlaceInfo place_info;
    }
    [System.Serializable]
    public class PlaceInfo
    {
        public string level;
        public string seconds;
        public string hp_left;
        public string scores;
        public string date;
    }
    [System.Serializable]
    public class PrizesInfo
    {
        public int from;
        public int to;
        public PrizesChestInfo prizes;
    }
    [System.Serializable]
    public class PrizesChestInfo
    {
        public int chest;
        public int crystals;
        public int gold;
    }
}
