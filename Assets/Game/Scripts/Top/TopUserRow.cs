﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class TopUserRow : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ITopUserRow
{

    public TopUsersInfoData InfoData;

    public Text Place, Name, Level, Time, Data,Score;
    public Image Fon;

    public MapType _type;

    public void SetData(TopUsersInfoData infoData, MapType type)
    {
        InfoData = infoData;
        _type = type;
        TimeSpan ts = new TimeSpan();
        DateTime dt = new DateTime();

        Place.text = infoData.place.ToString();

        int maxName = type == MapType.Portal ? 22 : 25;

        if (infoData.vk_user.name.Length > maxName)
        {
            Name.text = infoData.vk_user.name.Remove(maxName - 3) + "...";
        }
        else
        {
            Name.text = infoData.vk_user.name;
        }

        Score.text = "";
        Level.text = "";
        Time.text = "";
        switch (type)
        {
            case MapType.Portal:
                Level.text = infoData.level;

                if (infoData.seconds != "" && infoData.seconds != string.Empty && infoData.seconds != null)
                    ts = new TimeSpan(0, 0, int.Parse(infoData.seconds));
                else
                    ts = new TimeSpan(0, 59, 59);

                Time.text = ts.Minutes + " мин. ";
                /*
                float m = ts.Minutes % 10f;
                if (m == 1 && ts.Minutes != 11 && ts.Minutes != 12 && ts.Minutes != 13 && ts.Minutes != 14)
                {
                    Time.text = ts.Minutes + " минута ";
                }
                else if (m > 1 && m < 5 && ts.Minutes != 11 && ts.Minutes != 12 && ts.Minutes != 13 && ts.Minutes != 14)
                {
                    Time.text = ts.Minutes + " минуты ";
                }
                else
                {
                    Time.text = ts.Minutes + " минут ";
                }*/

                Time.text += ts.Seconds + " сек. ";
               /* float s = ts.Seconds % 10f;
                if (s == 1 && ts.Seconds != 11 && ts.Seconds != 12 && ts.Seconds != 13 && ts.Seconds != 14)
                {
                    Time.text += ts.Seconds + " секунда";
                }
                else if (s > 1 && s < 5 && ts.Seconds != 11 && ts.Seconds != 12 && ts.Seconds != 13 && ts.Seconds != 14)
                {
                    Time.text += ts.Seconds + " секунды";
                }
                else
                {
                    Time.text += ts.Seconds + " секунд";
                }*/
                break;
            case MapType.Survival:
                Score.text = infoData.scores;
                break;
            default:
                Debug.LogError("TUR -> SetData set type:"+type);
                break;
        }


        MyLog.Log("Pars date:" + infoData.datetime+" | "+infoData.date);
        if(infoData.datetime!=null && infoData.datetime != string.Empty && infoData.datetime!="")
            dt = System.DateTime.Parse(infoData.datetime);
        else
            dt = System.DateTime.Parse(infoData.date);
        Data.text = dt.ToString("HH:mm dd.MM.yyyy");
    }

    public void SetFonColor(Color color)
    {
        Fon.color = color;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (TopInfoPlayerPanel.instance != null)
            TopInfoPlayerPanel.instance.Show(InfoData, _type);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (TopInfoPlayerPanel.instance != null)
            TopInfoPlayerPanel.instance.Hide();
    }
}
