﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IncomeRscAnimationText : MonoBehaviour {

    public Transform Tobj;
    public Text Text;
    public Transform target;
    public Vector3 offsetTarget;
    public float TimeScale;
    public float TimeWait;
    public float TimeMove;

    public float TimeChangeColor;
    public Color color1, color2;

    public bool isAnim = false;

    private static IncomeRscAnimationText instance;

    public Coroutine CorAnim, CorColor;

    public void Awake()
    {
        instance = this;
        Tobj.gameObject.SetActive(false);
    }

    public static void StartAnim(float procent)
    {
        if (instance == null)
            return;
        instance._StartAnim(procent);
    }

    public void _StartAnim(float procent)
    {
        Text.text = string.Format("Доход {0}%",procent*100);

        if (CorAnim != null)
            StopCoroutine(CorAnim);

        if (CorColor != null)
            StopCoroutine(CorColor);

        isAnim = true;
        CorAnim = StartCoroutine(Anim());
        CorColor = StartCoroutine(ChangeColor());
    }

    public static float AllTime {
        get
        {
            if (instance == null)
                return 0;
            return instance._AllTime();
        }
    }

    public float _AllTime()
    {
        return TimeScale + TimeWait + TimeMove;
    }

    public IEnumerator Anim()
    {
        Tobj.gameObject.SetActive(true);
        Tobj.localPosition = new Vector3(0, 0, 0);
        float scale = 0;
        while (scale < 1)
        {
            scale += Time.deltaTime * (1f/TimeScale);
            Tobj.localScale = new Vector3(scale, scale, scale);
            yield return null;
        }
        yield return new WaitForSeconds(TimeWait);

        float dist = Vector3.Distance(Tobj.position, target.position + offsetTarget);
        float StartDist = dist;
        Vector3 start = Tobj.position;
        float process = 0;
        while (dist > 0.1f)
        {
            process += Time.deltaTime * (1f/TimeMove);
            Tobj.position = Vector3.Lerp(start, target.position + offsetTarget, process);
            yield return null;
            dist = Vector3.Distance(Tobj.position, target.position + offsetTarget);

            scale = Mathf.Clamp(dist / StartDist+0.1f,0,1);
            Tobj.localScale = new Vector3(scale, scale, scale);
        }
        Tobj.gameObject.SetActive(false);

        isAnim = false;
        yield break;
    }

    public IEnumerator ChangeColor()
    {
        while(isAnim)
        {
            Text.color = color1;
            yield return new WaitForSeconds(TimeChangeColor);
            Text.color = color2;
            yield return new WaitForSeconds(TimeChangeColor);
        }
        yield break;
    }

}
