﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonTimeSpeed : MonoBehaviour {

    public GameObject ButtonSpeed,ButtonNorm;// SpeedImg_Norm, SpeedImg_Higth, SpeedImg_Down;


    void Start ()
    {
        TimeScaleManager.onChangeTimeScale += UpdateTime;
        UpdateTime();
    }
	
    public void UpdateTime()
    {
        if (TimeScaleManager.TimeScale == 1)
        {
            ButtonSpeed.SetActive(true);
            ButtonNorm.SetActive(false);
        }
        else if (TimeScaleManager.TimeScale == TimeScaleManager.GetTimeBoost)
        {
            ButtonSpeed.SetActive(false);
            ButtonNorm.SetActive(true);
        }
    }

    void OnDestroy()
    {
        TimeScaleManager.onChangeTimeScale -= UpdateTime;
    }


   /* void FixedUpdate ()
    {
        UpdateTime();
	}*/
}
