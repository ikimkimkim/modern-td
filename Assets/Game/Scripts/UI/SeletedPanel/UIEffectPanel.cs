﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public struct UIEffectData
{
    public string URLIcon;
    public int CountStack;
    public float Progress;
    public string textInfo;
    public Color Color;
    public bool isBuff;

    public UIEffectData(string UrlIcon, int count, float Procent, string Info, Color color,bool buff = false)
    {
        URLIcon = UrlIcon;
        CountStack = count;
        Progress = Procent;
        textInfo = Info;
        Color = color;
        isBuff = buff;
    }
}

public class UIEffectPanel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public delegate UIEffectData RequestDataDelegate();

    public RequestDataDelegate RequestData;

    public Sprite buffIndicator, debuffIndicator;
    public Image Indicator;

    public GameObject rootObj;
    public Image Icon;
    public Text StackCount;
    public Image ProgressTime;
    public GameObject TooltipObj;
    public Text TooltipInfo;

    UIEffectData data;
    Coroutine loadIcon;
	void Update ()
    {
        data = RequestData();
        ProgressTime.fillAmount = data.Progress;
        if (data.CountStack > 1)
            StackCount.text = data.CountStack.ToString();
        else
            StackCount.text = "";
    }

    public void Show(RequestDataDelegate requestData)
    {
        //if (rootObj.activeSelf==false)
        {
            RequestData = requestData;
            data = RequestData();
            TooltipInfo.text = data.textInfo; 
            rootObj.SetActive(true);

            Indicator.sprite = data.isBuff ? buffIndicator : debuffIndicator;
            Indicator.color = data.Color;

            if (loadIcon != null)
                StopCoroutine(loadIcon);
            if(string.IsNullOrEmpty(data.URLIcon))
            {
                Debug.LogError("UIEffectPanel URL Icon is null or Empty!");
            }
            else
                loadIcon = StartCoroutine(TextureDB.instance.load(data.URLIcon, value => { Icon.sprite = value; }));
        }
    }

    public void Hide()
    {
        if (rootObj.activeSelf)
        {
            RequestData = null;
            rootObj.SetActive(false);
            if(TooltipObj.activeSelf)
                TooltipObj.SetActive(false);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        TooltipObj.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        TooltipObj.SetActive(false);
    }
}
