﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;
using UnityEngine.UI;
using Translation;
using UnityEngine.EventSystems;
using System;

public class CreepSelectedInfoPanel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Animator animator;
    public Text NameTxt, SpeedTxt;

    public Image Icon;
    public Text SpeedText, Range;
    public Sprite[] listShildTypeIcon;
    public Image ShildTypeIcon;
    public Text textShildType;

    public ProgressBar ShildBar;
    public ProgressBar HPBar;
    public UIEffectPanelManager effectPanelManager;

    public Image iconCreepType;
    public Text textIconCreepType;
    public Sprite[] listCreepsIconRace; //string iconUrlCreepType;

    public Image iconCreepPropertise;
    public Text textIconCreepPropertise;

    public NewUnitPopup unitPopup;
    public Transform parentPopup;

    public List<Unit> units = new List<Unit>();

    void OnEnable()
    {
        SelectedUnitCompanent.SelectedE += SelectedUnitCompanent_SelectedE;
        SelectedUnitCompanent.DeselectedE += SelectedUnitCompanent_DeselectedE;
    }

    void OnDisable()
    {
        SelectedUnitCompanent.SelectedE -= SelectedUnitCompanent_SelectedE;
        SelectedUnitCompanent.DeselectedE -= SelectedUnitCompanent_DeselectedE;
        //units.Clear();
    }

    private void SelectedUnitCompanent_DeselectedE(Unit unit)
    {
        if (unit.IsCreep)
        {
            units.Remove(unit);
        }
        UpdateUnitsSelected();
    }

    private void SelectedUnitCompanent_SelectedE(Unit unit)
    {
        if (unit.IsCreep && units.Contains(unit)==false)
        {
            units.Add(unit);
        }
        UpdateUnitsSelected();
    }

    private Unit unitShow;
    public void UpdateUnitsSelected()
    {
        SerchDeadUnitInArray();
        if (units.Count == 1 && SelectedUnitCompanent.CountSelected == 1)
        {
            UIBuildButton.Hide();
            UIBuildButton.HideTowerInfoAndSmapleTower();       
            UI.ClearSelectedTower();

            unitShow = units[0];
            animator.SetBool("Show", true);
            effectPanelManager.Show(unitShow);
            NameTxt.text = unitShow.unitName;

            Translator trans = TranslationEngine.Instance.Trans;
            NameTxt.text = trans[unitShow.unitName];
            Icon.sprite = unitShow.iconSprite;
            //newPopUp.UnitDesc.text = trans[unitShow.desp];

            if(listShildTypeIcon.Length>unitShow.ArmorType())
                ShildTypeIcon.sprite = listShildTypeIcon[unitShow.ArmorType()];

            textShildType.text =string.Format("<color=#fba81d>{0} броня</color> \r\n Слаб против:<color=#fba81d> {1} урон</color>",
                  DamageTable.GetArmorTypeInfo(unitShow.ArmorType()).name ,
                  DamageTable.getNameDamageType(unitShow));// DamageTable.getNameDamageType(unitShow) ;

            int i = (int)unitShow.GetUnitCreep.creepClass - 1;
            if (i > -1)
                iconCreepType.sprite = listCreepsIconRace[i];
            else
                Debug.LogError("UnitCreep"+unitShow.name+" creepClass not find");

            //StartCoroutine(TextureDB.instance.load(iconUrlCreepType, value => { iconCreepType.sprite = value; }));
            textIconCreepType.text = trans[unitShow.GetUnitCreep.creepClass.ToString()].Replace("-r-n", Environment.NewLine);

            StartCoroutine(TextureDB.instance.load(unitShow.GetUnitCreep.UrlIconPropertiesInfo, value => { iconCreepPropertise.sprite = value; }));
            textIconCreepPropertise.text = unitShow.GetUnitCreep.textPropertiesInfo;

            UpdateInfo();
        }
        else
        {
            animator.SetBool("Show", false);
            UISelected.bLockSelected = false;
            effectPanelManager.Hide();
            unitShow = null;
        }
    }

    public void SerchDeadUnitInArray()
    {
        for (int i = 0; i < units.Count; i++)
        {
            if(units[i] == null || units[i].dead == true)
            {
                units.RemoveAt(i);
                i--;
            }
        }
    }

    public void UpdateInfo()
    {
        ShildBar.SetProgress(unitShow.shield / Mathf.Max(unitShow.fullShield,1));
        HPBar.SetProgress(unitShow.HP / unitShow.fullHP);
        SpeedText.text = System.Math.Round(unitShow.GetUnitCreep.GetMoveSpeed(),1).ToString();
        Range.text = unitShow.GetRange().ToString();
    }

    void Update()
    {
        if (unitShow != null)
        {
            UpdateInfo();
        }
    }

    public void OnShowUnitPopup()
    {
        unitPopup.Show(unitShow as UnitCreep, parentPopup);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        UISelected.bLockSelected = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        UISelected.bLockSelected = true;
    }
}
