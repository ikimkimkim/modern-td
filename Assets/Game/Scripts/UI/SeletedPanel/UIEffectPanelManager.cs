﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;

public class UIEffectPanelManager : MonoBehaviour {

    public bool isShow = false;
    Unit showUnit;


    public List<UIEffectPanel> effectPanels = new List<UIEffectPanel>();

    void Start ()
    {
        for (int i = 0; i < effectPanels.Count; i++)
        {
            effectPanels[i].Hide();
        }
    }
	
    public void Show(Unit unit)
    {
        if (isShow == true)
        {
            if(unit != showUnit)
            {
                Debug.LogError("UIEffectPanelManager Show new Unit and show old unit!");
            }
            return;
        }
        showUnit = unit;

        UpdateAllPanels();

        showUnit.EffectsManager.addMyEffectE += EffectsManager_addMyEffectE;
        showUnit.EffectsManager.removeMyEffectE += EffectsManager_removeMyEffectE;
        isShow = true;
    }

    private void EffectsManager_removeMyEffectE(Unit unit, IDEffect typeEffect)
    {
        UpdateAllPanels();
    }

    private void EffectsManager_addMyEffectE(BaseEffect effect)
    {
        UpdateAllPanels();
    }

    public void UpdateAllPanels()
    {
        int index = 0;
        foreach (var stack in showUnit.EffectsManager.Stacks)
        {
            effectPanels[index].Show(stack.Value.GetUIEffectData);
            index++;
            if (index >= effectPanels.Count)
                return;
        }

        for (int i = index; i < effectPanels.Count; i++)
        {
            effectPanels[i].Hide();
        }
    }

    public void Hide()
    {
        if (isShow == false)
            return;


        showUnit.EffectsManager.addMyEffectE -= EffectsManager_addMyEffectE;
        showUnit.EffectsManager.removeMyEffectE -= EffectsManager_removeMyEffectE;
        showUnit = null;
        for (int i = 0; i < effectPanels.Count; i++)
        {
            effectPanels[i].Hide();
        }
        isShow = false;
    }

	/*void Update ()
    {
	    if(isShow)
        {
            UpdateInfo();
        }
	}

    public void UpdateInfo()
    {
        if (showUnit == null)
            return;

        
    }*/
}
