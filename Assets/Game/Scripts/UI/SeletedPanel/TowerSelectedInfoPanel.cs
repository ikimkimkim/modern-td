﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;
using UnityEngine.UI;
using Translation;
using UnityEngine.EventSystems;

public class TowerSelectedInfoPanel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [System.Serializable]
    public struct PropertiesPanel
    {
        public Image icon;
        public Text textInfo;
        public CardProperties linkProperties;
    }


    public Animator animator;
    public Text NameTxt, DmgTxt, ColdownTxt, CountKillTxt, AllDmgTxt;

    public Image Icon;

    public ProgressBar ShieldBar;
    public ProgressBar HPBar;
    public UIEffectPanelManager effectPanelManager;

    public Image iconTypeDmg;
    public Text tooltipTypeDmg;

    public PropertiesPanel[] propertiesPanels;
    public Sprite IconNotProperties;
    public Sprite IconNotSelectProperties;

    public Unit unitShow;

    void OnEnable()
    {
        UITowerInfo.OnShow += ShowInfo;
        UITowerInfo.OnHide += HideInfo;
        //SelectedUnitCompanent.SelectedE += SelectedUnitCompanent_SelectedE;
        //SelectedUnitCompanent.DeselectedE += SelectedUnitCompanent_DeselectedE;
    }

    void OnDisable()
    {
        UITowerInfo.OnShow -= ShowInfo;
        UITowerInfo.OnHide -= HideInfo;
        //SelectedUnitCompanent.SelectedE -= SelectedUnitCompanent_SelectedE;
        //SelectedUnitCompanent.DeselectedE -= SelectedUnitCompanent_DeselectedE;
        //units.Clear();
    }

   /* private void SelectedUnitCompanent_DeselectedE(Unit unit)
    {
        if (unit.IsTower)
        {
            units.Remove(unit);
        }
        UpdateUnitsSelected();
    }

    private void SelectedUnitCompanent_SelectedE(Unit unit)
    {
        if (unit.IsTower)
        {
            units.Add(unit);
        }
        UpdateUnitsSelected();
    }*/

    public void ShowInfo()
    {
        if(unitShow!=null)
        {
            effectPanelManager.Hide();
        }
        unitShow = UITowerInfo.instance.currentTower;
        UpdateUnitsSelected();
    }

    public void HideInfo()
    {
        unitShow = null;
        UpdateUnitsSelected();
    }

    public void UpdateUnitsSelected()
    {
        if(unitShow!=null)
        { 
            
            animator.SetBool("Show", true);
            effectPanelManager.Show(unitShow);
            NameTxt.text = unitShow.MyCard.unitName;

            //Translator trans = TranslationEngine.Instance.Trans;
            //NameTxt.text = trans[unitShow.unitName];
            Icon.sprite = unitShow.iconSprite;
            //newPopUp.UnitDesc.text = trans[unitShow.desp];      

            StartCoroutine(TextureDB.instance.load(unitShow.UrlIconDamageType, (Sprite value) => { iconTypeDmg.sprite = value; }));
            tooltipTypeDmg.text = "<color=#fba81d>"+ DamageTable.GetDamageTypeInfo(unitShow.DamageType()).name + " урон</color>";

            List<CardProperties> prop = unitShow.MyCard.Properties.FindAll(p => p.Requirements.Find(i => i.ID == 5) != null);

            for (int i = 0; i < propertiesPanels.Length; i++)
            {
                /*if (i + 2 > SkillPanelInCardAllInfo.LevelRelease)
                {
                    propertiesPanels[i].icon.sprite = IconNotProperties;
                    propertiesPanels[i].textInfo.text = "В будущих обновлениях";
                    continue;
                }*/

                if (OpenPropertiesStarPerk.isOpenLevel(_CardType.Tower, i + 2) == false)
                {
                    propertiesPanels[i].icon.sprite = IconNotProperties;
                    propertiesPanels[i].textInfo.text = "Не изучено";
                    continue;
                }

                if (prop.Count / 2 >= i + 1)
                {
                    propertiesPanels[i].icon.sprite = IconNotSelectProperties;
                    propertiesPanels[i].textInfo.text = "Не выбрано";
                }
                else
                {
                    propertiesPanels[i].icon.sprite = IconNotProperties;
                    propertiesPanels[i].textInfo.text = "У более редких башнях";
                }
            }

            RequirementsProperties rp;
            for (int i = 0; i < prop.Count; i++)
            {
                
                if (prop[i].CanActive())
                {
                    rp = prop[i].Requirements.Find(f => f.ID == 5);
                    if (rp != null)
                    {
                        int levelProp = (int)rp.Param[0];
                        StartCoroutine(TextureDB.instance.load(prop[i].UrlIcon, (Sprite value) => { propertiesPanels[levelProp-2].icon.sprite = value; }));
                        propertiesPanels[levelProp - 2].textInfo.text = prop[i].getFullInfoText();
                        propertiesPanels[levelProp - 2].linkProperties = prop[i];
                    }
                }   
            }
            

            UpdateInfo();
        }
        else
        {
            animator.SetBool("Show", false);
            UISelected.bLockSelected = false;
            effectPanelManager.Hide();
            unitShow = null;
            for (int i = 0; i < propertiesPanels.Length; i++)
                propertiesPanels[i].linkProperties = null;
        }
    }


    public void UpdateInfo()
    {
        if(ShieldBar!=null)
            ShieldBar.SetProgress(unitShow.shield / Mathf.Max(unitShow.fullShield, 1));
        HPBar.SetProgress(unitShow.HP / unitShow.fullHP);
        DmgTxt.text = string.Format("{0}-{1}", 
            System.Math.Round(unitShow.stats[unitShow.currentActiveStat].damageMin,1),
            System.Math.Round(unitShow.stats[unitShow.currentActiveStat].damageMax,1));
        ColdownTxt.text = System.Math.Round(1f/unitShow.stats[unitShow.currentActiveStat].cooldown,1).ToString();
        CountKillTxt.text = unitShow.countKillUnitCurrentGame.ToString();
        AllDmgTxt.text = unitShow.allDmgCurrentGame.ToStringDmg();
    }

    void Update()
    {
        if (unitShow != null)
        {
            UpdateInfo();
        }
    }

    public void OnUpgrade()
    {
        UITowerInfo.instance.UpgradeTower();
    }

    public void OnSale()
    {
        UITowerInfo.instance.OnSellButton();
    }

    public void OnShowAllInfo()
    {
        CardUIAllInfo.instance.Show(unitShow.MyCard,CardUIAllInfo.Mode.gameUpgrade);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        UISelected.bLockSelected = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        UISelected.bLockSelected = true;
    }

    public void OnHoverPropertiesPanel(int index)
    {
        if (index < 0 || index >= propertiesPanels.Length)
            return;

        if (propertiesPanels[index].linkProperties!=null && propertiesPanels[index].linkProperties.getRangeActive() > -1)
            IndicatorRange.Show(unitShow.thisT, propertiesPanels[index].linkProperties.getRangeActive());
    }
    public void OnExitPropertiesPanel()
    {
        IndicatorRange.Hide();
    }
}
