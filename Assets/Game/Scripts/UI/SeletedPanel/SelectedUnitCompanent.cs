﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public interface ISelectedUnit
{
    void Selected();
    void Deselected();
}


public class SelectedUnitCompanent : MonoBehaviour
{

    public delegate void EventHandler(Unit unit);
    public static event EventHandler SelectedE;
    public static event EventHandler DeselectedE;

    public static int CountSelected { get; private set; }

    public Unit rootUnit;
    private ISelectedUnit SelectedRootUnit;

    public bool isSelected { get; private set; }

    public GameObject SelectedObj;


    public void Start()
    {
        terrain = null;
        if (rootUnit == null)
            rootUnit = GetComponent<Unit>();

        rootUnit.onMyStartDestroyedE += RootUnit_onMyStartDestroyedE;


        if (rootUnit is ISelectedUnit)
            SelectedRootUnit = rootUnit as ISelectedUnit;
        else
            SelectedRootUnit = null;
    }

    void OnEnable()
    {
        UnitCreep.onDestinationE += UnitCreep_onDestinationE;
    }

    void OnDisable()
    {
        UnitCreep.onDestinationE -= UnitCreep_onDestinationE;
        if(isSelected && rootUnit.dead)
        {
            Deselected();
        }
    }


    private void UnitCreep_onDestinationE(UnitCreep unit)
    {
        if(isSelected && unit == rootUnit)
        {
            Deselected();
        }
    }

    private void RootUnit_onMyStartDestroyedE(Unit unit)
    {
        if (isSelected)
            Deselected();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "SelectedBox")
        {
            if (UISelected.isNowSelected)
            {
                Selected();
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "SelectedBox")
        {
            if (UISelected.isNowSelected)
            {
                Deselected();
            }
        }
    }


    public void Selected()
    {
        if (rootUnit.dead || isSelected)
            return;
        

        isSelected = true;
        CountSelected++;
        SelectedObj.SetActive(isSelected);

        if (SelectedRootUnit != null)
            SelectedRootUnit.Selected();

        if (SelectedE != null)
        {
            SelectedE(rootUnit);
        }
    }

    public void Deselected()
    {
        if (isSelected == false)
            return;

        isSelected = false;
        CountSelected--;
        SelectedObj.SetActive(isSelected);

        if (SelectedRootUnit != null)
            SelectedRootUnit.Deselected();

        if (DeselectedE != null)
        {
            DeselectedE(rootUnit);
        }
    }

    public void Update()
    {
        if (rootUnit.dead)
            return;

        if (isSelected)
        {
            MoveSelectedObj();
            if (Input.GetMouseButtonDown(0))
            {
                if (UISelected.isNowSelected == false && UISelected.bLockSelected==false)
                {
                    Deselected();
                }
            }
        }

    }

    private Terrain terrain;
    protected float HeightTerrainPoint;
    Vector3 pos;
    private void MoveSelectedObj()
    {
        if (rootUnit.IsCreep == false || rootUnit.GetUnitCreep.flying == false)
            return;
        if (terrain == null)
            terrain = Terrain.activeTerrain;

        HeightTerrainPoint = terrain.SampleHeight(rootUnit.thisT.position);
        pos = Vector3.zero;
        pos.y = terrain.transform.position.y + HeightTerrainPoint - rootUnit.thisT.position.y+0.05f;
        SelectedObj.transform.localPosition = pos;
    }
}
