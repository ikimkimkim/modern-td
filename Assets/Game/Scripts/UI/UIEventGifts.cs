﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEventGifts : UIEntity
{
    public static UIEventGifts instance { get; private set; }

    public override int entityID
    {
        get
        {
            return 1;
        }
    }

    public override int priorityShow
    {
        get
        {
            return 200;
        }
    }

    public GameObject panel;
    public Text caption, description;
    public Image fon;
    public Image chestImage;

    //0 Новый год
    public InfoTypeFon[] fonType;

    [System.Serializable]
    public struct InfoTypeFon
    {
        public string caption, description;
        public string urlFonOrigin;
        public Sprite spriteFon;
    }
    

    public ChestOpenPanel openChestPanel;

    private int typeEvent;
    private Chest.TypeChest typeChest;

	public override void Awake ()
    {
        Debug.Log("UIEventGifts awake");
        instance = this;
        base.Awake();
    }


    public void SetData(EventGiftsData data)
    {
        typeEvent = data.type;
        typeChest = (Chest.TypeChest)(data.chest - 1);
        Show();
    }
	
    private void Show()
    {
        if (!panel.activeSelf)
            panel.SetActive(true);

        caption.text = fonType[typeEvent - 1].caption;
        description.text = fonType[typeEvent - 1].description;
        fon.sprite = fonType[typeEvent - 1].spriteFon;
        StartCoroutine(TextureDB.instance.load(fonType[typeEvent - 1].urlFonOrigin, (Sprite value) => { fon.sprite = value; }));


        StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsChest[(int)typeChest], (Sprite value) => { chestImage.sprite = value; }));

        ShowUIEvent();
    }

    private void Hide()
    {
        if (panel.activeSelf)
            panel.SetActive(false);

        HideUIEvent();
    }

    public void OnGetGift()
    {
        UIWaitPanel.Show();
        PlayerManager.DM.GetEventGift(typeEvent, ResponceGetGift);
    }


    private void ResponceGetGift(ResponseDataTopDayPrize rp)
    {
        UIWaitPanel.Hide();
        if (rp.ok == 0)
        {
            Debug.LogError("Get event gifts Error: " + rp.error.code + " -> " + rp.error.text);
            TooltipMessageServer.Show(rp.error.text);
            PlayerManager.SetPlayerData(rp.user);
        }
        else
        {
            PlayerManager.SetPlayerData(rp.user);

            openChestPanel.SetResponceDataAndShow(-3, typeChest, rp.prizes);
        }
        Hide();
    }

    public override bool NeedShow()
    {
        var data = PlayerManager.getGiftsData;
        if (data != null && data.Count > 0)
        {
            foreach (var d in data)
            {
                SetData(d.Value);
                return true;
            }
        }
        return false;
    }
}
