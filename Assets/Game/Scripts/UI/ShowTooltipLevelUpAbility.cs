﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowTooltipLevelUpAbility : MonoBehaviour {

    public GameObject Tooltip1,Tooltip2;
    public Button Button;


    public void OnShow()
    {
        if (Button.interactable == true)
            Tooltip1.SetActive(true);
        else
            Tooltip2.SetActive(true);
    }


    public void OnHide()
    {
        Tooltip1.SetActive(false);
        Tooltip2.SetActive(false);
    }
}
