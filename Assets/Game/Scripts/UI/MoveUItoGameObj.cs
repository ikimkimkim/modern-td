﻿using UnityEngine;
using System.Collections;
using TDTK;

public class MoveUItoGameObj : MonoBehaviour {

    public Transform target;
    public Vector3 Offset=Vector3.zero;

    public bool bScale=false;

	// Use this for initialization
	void Start () {
	
	}


    private float logWidth, logHeight, logWeightedAverage, scaleFactor;

    // Update is called once per frame
    void Update () {
        if(target!=null)
        {
            transform.position = Camera.main.WorldToScreenPoint(target.position)+ Offset;


        }
	
        if(bScale)
        {
            logWidth = Mathf.Log(Screen.width / 990f, 2);
            logHeight = Mathf.Log(Screen.height / 560f, 2);
            logWeightedAverage = Mathf.Lerp(logWidth, logHeight, 0);
            scaleFactor = Mathf.Pow(2, logWeightedAverage);
            transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
        }
	}
}
