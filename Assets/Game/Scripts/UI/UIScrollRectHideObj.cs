﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScrollRectHideObj : MonoBehaviour {

    public RectTransform viewport, content;
    public Scrollbar verticalScrollbar;

    public float offsetHideTop, offsetHideButtom;

    struct UIObj
    {
        public RectTransform t;
        public GameObject obj;

        public UIObj(RectTransform T,GameObject Obj)
        {
            t = T;
            obj = Obj;
        }
    }


	void Awake ()
    {
        listObj = new List<UIObj>();
	}
	


    private List<UIObj> listObj;
    public void ChangeCountObj(bool onlyActiveObj)
    {
        verticalScrollbar.value = 1;
        listObj.Clear();
        for (int i = 0; i < content.childCount; i++)
        {
            var t = content.GetChild(i);
            if(onlyActiveObj)
            {
                if (t.gameObject.activeSelf)
                    listObj.Add(new UIObj(t.GetComponent<RectTransform>(), t.gameObject));
            }
            else
                listObj.Add(new UIObj(t.GetComponent<RectTransform>(), t.gameObject));


        }

        ChangeScroll();
    }

    public void ChangeScroll()
    {
        float offset = (1f-verticalScrollbar.value) * (content.sizeDelta.y - viewport.sizeDelta.y) - offsetHideTop;
        float endOffset = (1f - verticalScrollbar.value) * (content.sizeDelta.y - viewport.sizeDelta.y) + viewport.sizeDelta.y + offsetHideButtom;
        for (int i = 0; i < listObj.Count; i++)
        {
            var t = listObj[i].t;
            if (t.localPosition.y < -offset && t.localPosition.y > -endOffset)
            {
                //Debug.Log(-offset + " " + -endOffset + " " + t.localPosition);
                listObj[i].obj.SetActive(true);
            }
            else
                listObj[i].obj.SetActive(false);


        }

    }
}
