﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIMiniMenu : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    [SerializeField]
    private Animator animator;
    private Camera mainCamera;
    [SerializeField]
    private GameObject ButtonInfo, ButtonSale;

    private Card linkCard;

    private static UIMiniMenu instance;

    private bool isHide = false;
	void Start ()
    {
        instance = this;
        mainCamera = Camera.main;
	}

    private void LateUpdate()
    {
        if (Input.GetMouseButtonUp(0) && isEnterPanel == false)
        {
            Hide();
        }

       /* if (Input.GetMouseButtonUp(1))
        {
            Debug.Log(1);
            Show(null);
        }*/
    }

    public void ShowAllInfo()
    {
        if (CardUIAllInfo.instance != null && linkCard != null)
        {
            CardUIAllInfo.instance.Show(linkCard, CardUIAllInfo.Mode.info);
            Hide();
        }
    }

    public void SaleCard()
    {
        if (linkCard == null)
            return;
        CardSalePanel.instance.Show(linkCard);
        Hide();
    }

    public static void Show(Card card)
    {
        if(instance==null)
        {
            Debug.LogError("UIMiniMenu is null!");
            return;
        }
        instance._Show(card);
    }

    public void _Show(Card card)
    {
        if (isHide == false)
        {
            Hide();
            return;
        }

        if (ButtonInfo.activeSelf==false)
            ButtonInfo.SetActive(true);

        if (ButtonSale.activeSelf==false)
            ButtonSale.SetActive(true);

        switch (card.Type)
        {
            case _CardType.Upgrade:
                if (ButtonSale.activeSelf)
                    ButtonSale.SetActive(false);
                break;
        }
        linkCard = card;
        Vector3 vector = Input.mousePosition;
        vector.z = 100;
        vector = mainCamera.ScreenToWorldPoint(vector);
        transform.position = vector;
        vector = transform.localPosition;
        vector.z = 0;
        transform.localPosition = vector;


        animator.SetTrigger("Show");
        isHide = false;
    }

    private bool startHide = false;
    private void Hide()
    {
        if (isHide == false && startHide == false)
        {
            startHide = true;
            linkCard = null;
            animator.SetTrigger("Hide");
        }
    }

    private void AnimEventHide()
    {
        isHide = true;
        startHide = false;
    }


    private bool isEnterPanel = false;
    public void OnPointerExit(PointerEventData eventData)
    {
        isEnterPanel = false;
        Hide();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isEnterPanel = true;
    }
}
