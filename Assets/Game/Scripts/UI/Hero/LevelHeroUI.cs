﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelHeroUI : MonoBehaviour {

    public GameObject[] imageLevel;
    public bool Active = false;
    public Transform thisT;

    void Awake ()
    {
        Active = false;
        thisT = transform;
        for (int i = 0; i < imageLevel.Length; i++)
        {
            imageLevel[i].SetActive(false);
        }
	}

    public void setLevel(int level)
    {
        for (int i = 0; i < imageLevel.Length; i++)
            imageLevel[i].SetActive(false);

        if(level>1)
        {
            imageLevel[level-1].SetActive(true);
        }        
    }
	
}
