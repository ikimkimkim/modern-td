﻿using UnityEngine;
using System.Collections;
using TDTK;

public class LevelHeroManager : MonoBehaviour {

    public GameObject PrefabUIlevelHero;
    public static LevelHeroManager instance { get; private set; }

	void Awake ()
    {
        instance = this;
    }
    
    public static LevelHeroUI getLevelUI()
    {
        return instance._getLevelUI();
    }
    public LevelHeroUI _getLevelUI()
    {
        GameObject go = GameObject.Instantiate(PrefabUIlevelHero);
        go.transform.SetParent(transform);
        LevelHeroUI ui = go.GetComponent<LevelHeroUI>();
        return ui;
    }
}
