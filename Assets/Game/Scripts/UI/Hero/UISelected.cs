﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TDTK;

public class UISelected : MonoBehaviour
{
    private Vector3 UIStartPos, UIEndPos,WorldStartPos,WorldEndPos;
    private Vector2 UIsize;
    public Image SelectImage;
    public LayerMask maskTerrain,maskHero;

    public static int CountSelectUnit;
    private static UISelected instance;
    public static bool isNowSelected { get { return instance.nowSelected; } }
    public bool nowSelected;

    public Transform ColliderT;
    public BoxCollider Collider;

    public bool LockSelected;
    public static bool bLockSelected { get { return instance.LockSelected; } set { instance.LockSelected = value; } }

    private void Awake()
    {
        instance = this;
        CountSelectUnit = 0;
        Collider.enabled = false;
        UITowerShow = false;
    }

    void Start () {
        Collider.center = new Vector3(0, 2, 0);
    }

    void OnEnable()
    {
        UIBuildButton.ShowEvent += SelectBuild;
        UIBuildButton.HideEvent += ClearUIBuild;

        UITowerInfo.OnShow += SelectTower;
        UITowerInfo.OnHide += ClearUITower;
    }

    void OnDisable()
    {
        UITowerInfo.OnShow -= SelectTower;
        UIBuildButton.ShowEvent -= SelectBuild;
        UIBuildButton.HideEvent -= ClearUIBuild;
        UITowerInfo.OnHide -= ClearUITower;
    }

    Ray r;
    RaycastHit hit;
    float lastClick;
    void Update()
    {
        if (LockSelected && nowSelected==false || TowerSelectProperties.isShow || GameControl.IsGameOver || GameControl.IsGamePause)
        {
            return;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if(CountSelectUnit==0)
            {
                r = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(r, out hit, 1000, maskHero))
                {
                    if (Time.time - lastClick < 0.3f)
                    {
                        var hero = hit.transform.GetComponent<UnitHero>();
                        if (hero != null)
                        {
                            foreach (var h in UIHeroSpawn.getActiveHeroList.FindAll(i => i.prefabID == hero.prefabID))
                            {
                                h.GetComponent<SelectedUnitCompanent>().Selected();

                            }
                            UIBuildButton.HideTowerInfoAndSmapleTower();
                            UIBuildButton.Hide();
                            UI.ClearSelectedTower();
                        }
                        else
                            SelectUnit(hit.transform);
                    }
                    else
                        SelectUnit(hit.transform);
                }
            }


            lastClick = Time.time;
        }

        if (Input.GetMouseButtonDown(0))
        {
            SelectImage.gameObject.SetActive(true);
            UIStartPos = Input.mousePosition;
            r = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(r, out hit, 1000, maskTerrain))
            {
                WorldStartPos = hit.point;
            }

        }

        if (Input.GetMouseButton(0))
        {
            UIEndPos = Input.mousePosition;
            UpdateImage();
            r = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(r, out hit, 1000, maskTerrain))
            {
                WorldEndPos = hit.point;
            }
            UpdateCollider();
        }
        else if(SelectImage.gameObject.activeSelf)
        {
            SelectImage.gameObject.SetActive(false);
        }


        if(Input.GetMouseButtonUp(0))
        {
            nowSelected = false;
            Collider.enabled = false;
        }
    }

    private void SelectUnit(Transform unitT)
    {
        SelectedUnitCompanent comp = unitT.GetComponent<SelectedUnitCompanent>();
        if (comp != null && comp.isSelected == false)
        {
            comp.Selected();
            UIBuildButton.HideTowerInfoAndSmapleTower();
            UIBuildButton.Hide();
            UI.ClearSelectedTower();


        }
    }
    
    private bool UITowerShow;
    private bool UIBuldeShow;
    public void SelectTower()
    {
        UITowerShow = true;
    }
    public void ClearUITower()
    {
        UITowerShow = false;
    }
    public void SelectBuild()
    {
        UIBuldeShow = true;
    }
    public void ClearUIBuild()
    {
        UIBuldeShow = false;
    }



    void UpdateImage()
    {
        UIsize = new Vector2(Mathf.Abs(UIEndPos.x - UIStartPos.x), Mathf.Abs(UIEndPos.y - UIStartPos.y));
        if (UIsize.x > 1 || UIsize.y > 1)
        {
            if (UIBuldeShow && CountSelectUnit>0)
            {
                UIBuildButton.Hide();
                UIBuildButton.HideTowerInfoAndSmapleTower(); 
            }
            if (UITowerShow && CountSelectUnit > 0)
            {
                UI.ClearSelectedTower();
            }
            Collider.enabled = true;
            nowSelected = true;
            SelectImage.rectTransform.localPosition = new Vector3(Mathf.Min(UIEndPos.x, UIStartPos.x), Mathf.Min(UIEndPos.y, UIStartPos.y), 0);
            SelectImage.rectTransform.sizeDelta = UIsize;
        }
        else
        {
            nowSelected = false;
            SelectImage.rectTransform.sizeDelta = Vector2.zero;
        }
    }




    void UpdateCollider()
    {
        Vector3 pos = WorldStartPos + new Vector3(WorldEndPos.x - WorldStartPos.x, 
            WorldEndPos.y - WorldStartPos.y, WorldEndPos.z - WorldStartPos.z)/2f;

        ColliderT.position = pos;

        Collider.size = new Vector3(Mathf.Abs(WorldEndPos.x - WorldStartPos.x),
            10,Mathf.Abs(WorldEndPos.z - WorldStartPos.z));
    }
}
