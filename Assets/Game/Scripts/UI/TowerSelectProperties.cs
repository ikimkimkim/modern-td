﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using TDTK;

public class TowerSelectProperties : MonoBehaviour {

    public delegate void OnShowHandler();
    public delegate void OnHideHandler(bool isUpgrade);
    public static event OnShowHandler onShowE;
    public static event OnHideHandler onHideE;


    public List<InfoUpgrade> DBUpgrade;
    [Serializable]
    public struct InfoUpgrade
    {
        public int type;
        public int damageType;
        public int levelTower;
        public int indexProperties;

        public InfoUpgrade(int Type, int damage, int level, int index)
        {
            type = Type;
            damageType = damage;
            levelTower = level;
            indexProperties = index;
        }
    }


    [Serializable]
    public struct SkillInfo
    {
        public Toggle toggle;
        public Image icon;
        public Text iconText;
        public Image iconLevel;
        public Text text;
    }
    public GameObject panel;
    public Text Toltip;
    public Sprite[] iconsLevel;

    [Header("SelectPanel")]
    public GameObject SelectPanel;
    public SkillInfo[] ListProperties;

    [Header("InfoPanel")]
    public GameObject InfoPanel;
    public Text PropertiesTextInfo;
    public Text IndexPropertiesInfo;
    public Image IconInfo, IconInfoLevel;
    [Space]
    public Text levelText, infoText;
    public Button buttonOk;

    public string[] DamageTypeName;

    private List<CardProperties> SelectProperties;
    private Action<bool> end;

    public static Card ShowCard { get { if (instance == null) return null; return instance.showCard; } }
    public Card showCard { get; private set; }
    private int SelectedIndex;

    private static TowerSelectProperties instance;

    [Header("Tutorial")]
    public GameObject[] TutorialArrows;

    public static bool isShow { get { return instance != null ? instance.panel.activeSelf : false; } }

    public void Start()
    {
        instance = this;
        DBUpgrade = new List<InfoUpgrade>();
        if (panel.activeSelf)
            panel.SetActive(false);

        //UnitTower.onDestroyedE += TowerDestroy;
        //UnitTower.onSoldE += TowerDestroy;
    }

    public void Destroy()
    {
        //UnitTower.onDestroyedE -= TowerDestroy;
        //UnitTower.onSoldE -= TowerDestroy;
    }

    public static void initShow(Card card, Action<bool> EndSelect)
    {
        if (instance != null)
        {
            instance._initShow(card, EndSelect);
            if (onShowE != null)
                onShowE();
        }
        else Debug.LogError("SelectProperties instance is null!");
    }

    /*public void TowerDestroy(Unit tower)
    {
        if (tower.MyCard.LevelTower < 2)
            return;
        int index = DBUpgrade.FindIndex(i => i.card == tower.MyCard);
        Debug.Log("index:"+index);
        if (index > 0)
        {
            DBUpgrade.RemoveAt(index);
        }
    }*/

    public static bool HaveSelectedProperties(Card card, int level)
    {
        RequirementsProperties rp;
        for (int i = 0; i < card.Properties.Count; i++)
        {
            rp = card.Properties[i].Requirements.Find(f => f.ID == 5);
            if (rp != null)
            {
                if ((int)rp.Param[0] == level)
                {
                    return true;
                }
            }
        }
        return false;
    }

    private void _initShow(Card card, Action<bool> EndSelect)
    {
        MyLog.Log("TSP init " + card.LevelTower);
        end = EndSelect;
        showCard = card;
        InitSelectProperties();
        if (SelectProperties.Count == 0)
        {
            Debug.Log("TSP Card not propeties");
            end(true);
        }
        else if(OpenPropertiesStarPerk.isOpenLevel(card) == false)
        {
            Debug.Log("TSP Card not open propeties");
            end(true);
        }
        else
            Show();
    }

    private void InitSelectProperties()
    {
        RequirementsProperties rp;
        SelectProperties = new List<CardProperties>();
        for (int i = 0; i < showCard.Properties.Count; i++)
        {
            rp = showCard.Properties[i].Requirements.Find(f => f.ID == 5);
            if (rp != null)
            {
                if ((int)rp.Param[0] == showCard.LevelTower)
                {
                    SelectProperties.Add(showCard.Properties[i]);
                }
            }
        }
    }

    private int GetIndexInDB(Card card)
    {
        if (card.IgnorePropertiesDB)
            return -1;

        int index = -1;
        switch(card.Type)
        {
            case _CardType.Tower:
                index = DBUpgrade.FindIndex(i => i.type == (int)card.Type && i.damageType == card.damageType && i.levelTower == card.LevelTower);
                break;
            case _CardType.Hero:
                index = DBUpgrade.FindIndex(i => i.type == (int)card.Type + card.heroUnit.prefabID * 100 && i.damageType == card.damageType && i.levelTower == card.LevelTower);
                break;
            case _CardType.Ability:
                index = DBUpgrade.FindIndex(i => i.type == -card.abilityObj.ID-1 && i.damageType == card.damageType && i.levelTower == card.LevelTower);
                break;
        }
            

        return index;
    }

	private void Show()
    {
        MyLog.Log("TSP show => " + SelectProperties.Count);

        

        int index = GetIndexInDB(showCard);
        //Debug.Log("index:" + index);
        if (index >= 0)
        {
            if (bShowAlreadySelectedProperties)
            {
                InfoPropertiesPanel(DBUpgrade[index].indexProperties);
                TimeScaleManager.SetTimeScale(0);
            }
            else
                NotShowInfoPropertiesPanel(DBUpgrade[index].indexProperties);
        }
        else
        {
            SelectPropertiesPanel();
            TimeScaleManager.SetTimeScale(0);
        }      

    }

    private void InfoPropertiesPanel(int indexSelect)
    {
        panel.SetActive(true);
        lockAddDB = true;
        if (toggleAlreadySelected != null)
            toggleAlreadySelected.isOn = bShowAlreadySelectedProperties;
        if (InfoPanel.activeSelf == false)
            InfoPanel.SetActive(true);
        if (SelectPanel.activeSelf)
            SelectPanel.SetActive(false);

        Toltip.text = "НОВОЕ СВОЙСТВО";
        infoText.text = "";
        buttonOk.interactable = true;

        IndexPropertiesInfo.text = ((showCard.LevelTower - 2) * 2 + indexSelect + 1).ToString();
        PropertiesTextInfo.text = SelectProperties[indexSelect].getFullInfoText();

        IconInfoLevel.gameObject.SetActive(SelectProperties[indexSelect].Level > 0);
        IconInfoLevel.sprite = iconsLevel[SelectProperties[indexSelect].Level];
        StartCoroutine(TextureDB.instance.load(SelectProperties[indexSelect].UrlIcon, (Sprite value) => { IconInfo.sprite = value; }));

        SelectedIndex = indexSelect;
    }

    private void NotShowInfoPropertiesPanel(int indexSelect)
    {
        SelectedIndex = indexSelect;
        Hide(true);
    }

    private void SelectPropertiesPanel()
    {
        panel.SetActive(true);
        lockAddDB = false;

        if (toggleAlreadySelected != null)
            toggleAlreadySelected.isOn = bShowAlreadySelectedProperties;
        if (SelectPanel.activeSelf == false)
            SelectPanel.SetActive(true);
        if (InfoPanel.activeSelf)
            InfoPanel.SetActive(false);

        Toltip.text = "ВЫБЕРИТЕ СВОЙСТВО";
        levelText.text = "Уровень " + showCard.LevelTower;
        switch(showCard.Type)
        {
            case _CardType.Tower:
                infoText.text = "Выбранное свойство будет применено ко всем <color=#ffcc00>" + DamageTypeName[showCard.damageType] + "</color> башням на уровне <color=#ffcc00>" + showCard.LevelTower + "</color>";
                break;
            case _CardType.Ability:
                infoText.text = "Выбранное свойство будет применено ко всем заклинанием таго же типа на уровне <color=#ffcc00>" + showCard.LevelTower + "</color>";
                break;
            case _CardType.Hero:
                infoText.text = "Выбранное свойство будет применено ко всей поддержки таго же типа на уровне <color=#ffcc00>" + showCard.LevelTower + "</color>";
                break;
        }
        buttonOk.interactable = false;
        

        for (int i = 0; i < ListProperties.Length; i++)
        {
            if (i < SelectProperties.Count)
            {
                ListProperties[i].text.text = SelectProperties[i].getFullInfoText();
                ListProperties[i].iconLevel.gameObject.SetActive(SelectProperties[i].Level > 0);
                ListProperties[i].iconLevel.sprite = iconsLevel[SelectProperties[i].Level];
                ListProperties[i].iconText.text = "";// ((showCard.LevelTower - 2) * 2 + i + 1).ToString();
                int index = i;
                StartCoroutine(TextureDB.instance.load(SelectProperties[i].UrlIcon, (Sprite value) => { ListProperties[index].icon.sprite = value; }));
            }
        }
    }

    public void OnSelected()
    {
        buttonOk.interactable = true;

        UpdateSelectedIndex();
    }

    private void UpdateSelectedIndex()
    {
        SelectedIndex = -1;
        for (int i = 0; i < ListProperties.Length; i++)
        {
            if (ListProperties[i].toggle.isOn)
            {
                SelectedIndex = i;
                break;
            }
        }
    }

    private bool lockAddDB;
    private void EndSelectProperties()
    {
        if (lockAddDB == false && showCard.IgnorePropertiesDB == false)
        {
            switch (showCard.Type)
            {
                case _CardType.Tower:
                    DBUpgrade.Add(new InfoUpgrade((int)showCard.Type, showCard.damageType, showCard.LevelTower, SelectedIndex));
                    break;
                case _CardType.Hero:
                    DBUpgrade.Add(new InfoUpgrade((int)showCard.Type + showCard.heroUnit.prefabID * 100, showCard.damageType, showCard.LevelTower, SelectedIndex));
                    break;
                case _CardType.Ability:
                    DBUpgrade.Add(new InfoUpgrade(-showCard.abilityObj.ID - 1, showCard.damageType, showCard.LevelTower, SelectedIndex));
                    break;
            }
            MyLog.Log("TSP save selected properties");
        }
        RequirementsProperties rp= SelectProperties[SelectedIndex].Requirements.Find(f => f.ID == 5);
        RTowerUpgradeLevel rtul = (RTowerUpgradeLevel) rp.requirements;
        rtul.isActive = true;

        MyLog.Log("TSP select => " + SelectedIndex + " : " + SelectProperties[SelectedIndex].getInfoText());
    }


    public static void OnReselectProperties(Card card)
    {
        if (instance != null)
            instance._OnReselectProperties(card);
    }
    public void _OnReselectProperties(Card card)
    {
        int levelTower = card.LevelTower;
        showCard = card;
        lockAddDB = true;
        for (int level = 0; level < levelTower; level++)
        {
            ShowCard.LevelTower = level + 1;
            InitSelectProperties();
            if (SelectProperties.Count <= 0)
                continue;

            int index = GetIndexInDB(showCard);
            if (index > -1)
            {
                index = DBUpgrade[index].indexProperties;
                RequirementsProperties rp = SelectProperties[index].Requirements.Find(f => f.ID == 5);
                RTowerUpgradeLevel rtul = (RTowerUpgradeLevel)rp.requirements;
                if (rtul.LevelActive <= showCard.LevelTower)
                    rtul.isActive = true;
            }
        }

        showCard = null;
    }


    public void Hide(bool isUpgrade)
    {
        TimeScaleManager.RefreshTimeScale();

        if(panel.activeSelf)
            panel.SetActive(false);
        if (isUpgrade)
            EndSelectProperties();

        for (int i = 0; i < ListProperties.Length; i++)
        {
            ListProperties[i].toggle.isOn = false;            
        }


        if (onHideE != null)
            onHideE(isUpgrade);

        if (end!=null)
            end(isUpgrade);

        //showCard = null;


       
    }

    public static bool bShowAlreadySelectedProperties = true;
    public Toggle toggleAlreadySelected;
    public void SetToggleShowPopapWithAlreadySelectedProperties(bool show)
    {
        bShowAlreadySelectedProperties = show;
    }

    public static void TutorialArrow(bool active) { if(instance!=null) instance._TutorialArrow(active); }
    public void _TutorialArrow(bool active)
    {
        for (int i = 0; i < TutorialArrows.Length; i++)
        {
            if (TutorialArrows[i].activeSelf != active)
                TutorialArrows[i].SetActive(active);
        }
    }


}
