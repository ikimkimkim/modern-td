﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowTooltipLevelUpTower : MonoBehaviour {

    public GameObject Tooltip;
    public Button Button;


    public void OnShow()
    {
        if(Button.interactable == false)
            Tooltip.SetActive(true);
    }     
            

    public void OnHide()
    {
        Tooltip.SetActive(false);
    }
}
