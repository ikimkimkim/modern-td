﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class UIEntityManager : Singleton<UIEntityManager>
{
    public List<UIEntity> UIEntitys;

    private bool anyShow;

    void Awake ()
    {
        Debug.Log("UIEntityManager");
        instance = this;
        UpdateShowUI();
        //PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }

    private void SceneManager_sceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if(scene.buildIndex==1)
        {
            UpdateShowUI();
        }

    }

    protected override void OnDestoroy()
    {
        base.OnDestoroy();
        //PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
    }

    private void PlayerManager_InitializationСompleted()
    {
        UpdateShowUI();
    }

    protected override void InitializationSingleton()
    {
        UIEntitys = new List<UIEntity>();
    }
	
    public static void AddEntity(UIEntity entity) { instance._AddEntity(entity); }
    private void _AddEntity(UIEntity entity)
    {
        UIEntitys.Add(entity);
        entity.ShowE.AddListener(ShowEntity);
        entity.HideE.AddListener(HideEntity);
    }

    private void ShowEntity()
    {
        anyShow = true;
    }


    private void HideEntity()
    {
        anyShow = false;
        UpdateShowUI();
    }

    private void UpdateShowUI()
    {
        if (anyShow)
            return;
        foreach (var ui in UIEntitys.OrderBy(i => i.priorityShow))
        {
            if (ui.NeedShow())
                return;
        }
    }

    public static void RemoveEntity(UIEntity entity) { instance._RemoveEntity(entity); }
    private void _RemoveEntity(UIEntity entity)
    {
        entity.ShowE.RemoveListener(ShowEntity);
        entity.HideE.AddListener(HideEntity);
        UIEntitys.Remove(entity);
    }

}
