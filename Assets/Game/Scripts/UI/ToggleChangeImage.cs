﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ToggleChangeImage : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string textNotinteractable;
    public Transform shotPointText;
    public Toggle toggle;
    public Image ImageFon;
    public string UrlImageOn, UrlImageOff;

    private Sprite _ImageOn, _ImageOff;

    void OnEnable()
    {
        StartCoroutine(TextureDB.instance.load(UrlImageOn, SetSprite));
        StartCoroutine(TextureDB.instance.load(UrlImageOff, SetSprite));
    }

    public void eventValueChanged()
    {
       /* if (toggle.isOn && _ImageOn == null)
        {
            StartCoroutine(TextureDB.instance.load(UrlImageOn, SetSprite));
        }
        else */if (toggle.isOn && _ImageOn != null)
        {
            ImageFon.sprite = _ImageOn;
        }
        /*else if (toggle.isOn == false && _ImageOff == null)
        {
            StartCoroutine(TextureDB.instance.load(UrlImageOff, SetSprite));
        }*/
        else if (toggle.isOn == false && _ImageOff != null)
        {
            ImageFon.sprite = _ImageOff;
        }
    }

    public void SetSprite(Sprite value)
    {
        if (value.name == UrlImageOn)
            _ImageOn = value;

        if (value.name == UrlImageOff)
            _ImageOff = value;

        if (value.name==UrlImageOn && toggle.isOn==true)
            ImageFon.sprite = value;
        else if(value.name == UrlImageOff && toggle.isOn==false)
            ImageFon.sprite = value;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (toggle.interactable == false)
        {
            if(textNotinteractable.Length>0)
                StayInSameWorldPos.setPos(shotPointText.position, textNotinteractable);
        }
        //else
        //  StayInSameWorldPos.setPos(transform.position + transform.up * 14f, perk.name);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        StayInSameWorldPos.Hide();
    }
}
