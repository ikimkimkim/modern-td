﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;

public class TowerLevelInfoManager : MonoBehaviour
{
    public GameObject objInfoPrefab;
    public bool UITower, UIBuild;
    public List<TowerLevelInfo> listLevelInfo;

    public static TowerLevelInfoManager instance;
    
	void Start ()
    {
        instance = this;
        listLevelInfo = new List<TowerLevelInfo>();
        UIBuildButton.ShowEvent += ShowUIBuild;
        UITowerInfo.OnShow += ShowUITower;
        UIBuildButton.HideEvent += HideUIBuild;
        UITowerInfo.OnHide += HideUITower;
        UnitTower.onConstructionCompleteE += ConstructionComplete;
    }

    void OnDestroy()
    {
        UIBuildButton.ShowEvent -= ShowUIBuild;
        UITowerInfo.OnShow -= ShowUITower;
        UIBuildButton.HideEvent -= HideUIBuild;
        UITowerInfo.OnHide -= HideUITower;
        UnitTower.onConstructionCompleteE -= ConstructionComplete;
    }

    public void DestroyTower(TowerLevelInfo tli)
    {
       // Debug.Log("TLIM -> DestroyTower " + tli.tower.name);
        listLevelInfo.Remove(tli);
        if(tli!=null)
            GameObject.Destroy(tli.gameObject);
    }

    public void ConstructionComplete(UnitTower t)
    {
        //if(t.GetLevel()==1)
        {
            CreadNewInfo(t);
        }
        ChangeUI();
    }

    public void CreadNewInfo(UnitTower t)
    {
        //Debug.Log("TLIM -> CreadNewInfo " + t.name);
        GameObject go = GameObject.Instantiate(objInfoPrefab);
        go.transform.SetParent(this.transform);
        TowerLevelInfo tli = go.GetComponent<TowerLevelInfo>();
        tli.SetTower(t);
        listLevelInfo.Add(tli);
    }
    

    public void ShowUITower()
    {
        UITower = true;
        ChangeUI();
    }

    public void HideUITower()
    {
        UITower = false;
        ChangeUI();
    }

    public void ShowUIBuild()
    {
        UIBuild = true;
        ChangeUI();
    }

    public void HideUIBuild()
    {
        UIBuild = false;
        ChangeUI();
    }
        

    public void ChangeUI()
    {
        //Debug.Log("TLIM -> ChangeUI " + (UITower == false && UIBuild == false));
        for (int i = 0; i < listLevelInfo.Count; i++)
        {
            listLevelInfo[i].Active(UITower==false && UIBuild==false);
        }
    }

   /* private void HideAllLevelInfo()
    {
        
    }

    public void ShowAllLevelInfo()
    {
        for (int i = 0; i < listLevelInfo.Count; i++)
        {
            listLevelInfo[i].Active(true);
        }
    }*/

    



}
