﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScaleCanvasClimp : MonoBehaviour {

    [SerializeField]
    private CanvasScaler canvesScaler;

    public Vector2 ScreenSizeDef;

    public Vector2 ClampScale;

    void Awake()
    {
        if (canvesScaler == null)
            canvesScaler = GetComponent<CanvasScaler>();

        FullSceenButton.AddListener(UpdateScaler);
    }

	void OnEnable ()
    {
        UpdateScaler();
    }

    private float oldW;
    public void Update()
    {
        if (oldW != Screen.width)
        {
            oldW = Screen.width;
            UpdateScaler();
        }
    }

    private void UpdateScaler()
    {
        
        float scale = Screen.width / ScreenSizeDef.x;
        scale = Mathf.Clamp(scale, ClampScale.x, ClampScale.y);
        canvesScaler.scaleFactor = scale;
    }
	

}
