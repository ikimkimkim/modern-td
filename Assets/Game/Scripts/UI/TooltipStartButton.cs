﻿using UnityEngine;
using System.Collections;

public class TooltipStartButton : MonoBehaviour {

    public int delay = 10;
    public GameObject TooltipObj;
    private Coroutine coroutineShow;

    IEnumerator ShowStartBattleDelayed(float delay)
    {
        yield return new WaitForSeconds(delay);
        TooltipObj.SetActive(true);
        yield break;
    }

    private void OnEnable()
    {
        coroutineShow = StartCoroutine(ShowStartBattleDelayed(delay));
    }

    private void OnDisable()
    {
        if(coroutineShow!=null)
        {
            StopCoroutine(coroutineShow);
        }
        TooltipObj.SetActive(false);
    }
  
}
