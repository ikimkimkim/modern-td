﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SystemUpdate;
using TDTK;
using UnityEngine.UI;

public class UIUnitHealthProgressBar : MonoBehaviour,IItemUpdate
{
    public bool isFree { get { return _isFree; } }    
    private bool _isFree, _isHero;

    [SerializeField] private Unit _unit;
    private UnitHero _hero;
    private Camera _mainCamera;
    [SerializeField] private Slider _sliderHP, _sliderShield;
    [SerializeField] private GameObject _effectUpgradeHero;
    [SerializeField] private Image _levelHero;
    [SerializeField] private Sprite[] _iconLevelHero;

    private List<IDEffect> _effectsType = new List<IDEffect>();
    [SerializeField] private Sprite spriteBuff, spriteDebuff;
    [SerializeField] private Image[] effectIcons;


    public void AttachUnit(Unit unit, Camera camera)
    {
        Debug.LogWarning("Attach unit: " + unit.name);
        _mainCamera = camera;
        _isFree = false;
        _unit = unit;


        _unit.onMyDamagedE += _unit_onMyDamagedE;
        _unit.onMyStartDestroyedE += _unit_onMyDestroyedE;
        _unit.EffectsManager.addMyEffectE += EffectsManager_addMyEffectE;
        _unit.EffectsManager.removeMyEffectE += EffectsManager_removeMyEffectE;
        _isHero = _unit.IsHero;
        if (_isHero)
        {
            _hero = _unit.GetUnitHero;
            _hero.unitUpdateLevel += GetUnitHero_unitUpdateLevel;
            _hero.unitUpdateXP += _hero_unitUpdateXP;

            if (_effectUpgradeHero.activeSelf == false)
                _effectUpgradeHero.SetActive(true);

            if (_hero.isNeedUpdate != _effectUpgradeHero.activeSelf)
                _effectUpgradeHero.SetActive(_hero.isNeedUpdate);

            if (_hero.currentActiveStat > 0)
            {
                if (_levelHero.enabled == false)
                    _levelHero.enabled = true;
                _levelHero.sprite = _iconLevelHero[_hero.currentActiveStat - 1];
            }
            else
                _levelHero.enabled = false;

            gameObject.SetActive(true);
        }
        else
        {
            if (_effectUpgradeHero.activeSelf)
                _effectUpgradeHero.SetActive(false);
            if (_levelHero.enabled)
                _levelHero.enabled = false;
        }

        _effectsType.Clear();
        foreach (var key in _unit.EffectsManager.Stacks.Keys)
        {
            _effectsType.Add((IDEffect)key);
        }
        UpdateEffects();

        _sliderHP.maxValue = _unit.fullHP;
        UpdateBarHP();
        _sliderShield.maxValue = _unit.fullShield;
        UpdateBarShield();

        if (_unit.HP < _unit.fullHP)
        {
            if (gameObject.activeSelf == false)
                gameObject.SetActive(true);
        }
        else
        {
            if (gameObject.activeSelf)
                gameObject.SetActive(false);
        }

        MainSystem.AddItem(this);
    }

    private void _hero_unitUpdateXP(object sender, System.EventArgs e)
    {
        if (_hero.isNeedUpdate != _effectUpgradeHero.activeSelf)
            _effectUpgradeHero.SetActive(_hero.isNeedUpdate);
    }

    private void GetUnitHero_unitUpdateLevel(object sender, System.EventArgs e)
    {
        _sliderHP.maxValue = _unit.fullHP;
        UpdateBarHP();
        _sliderShield.maxValue = _unit.fullShield;
        UpdateBarShield();
        if (_hero.currentActiveStat > 0)
        {
            if (_levelHero.enabled == false)
                _levelHero.enabled = true;
            _levelHero.sprite = _iconLevelHero[_hero.currentActiveStat - 1];
        }
        else
            _levelHero.enabled = false;
    }

    private void EffectsManager_removeMyEffectE(Unit unit, IDEffect typeEffect)
    {
        if (_effectsType.Contains(typeEffect))
        {
            if (_unit.EffectsManager.getCounterEffect(typeEffect) <= 0)
            {
                _effectsType.Remove(typeEffect);
                UpdateEffects();
            }
        }
    }

    private void EffectsManager_addMyEffectE(BaseEffect effect)
    {
        if (gameObject.activeSelf == false)
            gameObject.SetActive(true);
        if (_effectsType.Contains(effect.IDStack))
            return;
        _effectsType.Add(effect.IDStack);
        UpdateEffects();
    }

    private void _unit_onMyDestroyedE(Unit unit)
    {
        DetachUnit();
    }

    private void _unit_onMyDamagedE(float damag = 0)
    {
        if (gameObject.activeSelf == false)
            gameObject.SetActive(true);
        UpdateBarHP();
        UpdateBarShield();
    }

    public void DetachUnit()
    {
        if (_unit != null)
        {
            Debug.LogWarning("Detach unit: " + _unit.name);
            _unit.onMyDamagedE -= _unit_onMyDamagedE;
            _unit.onMyStartDestroyedE -= _unit_onMyDestroyedE;
            _unit.EffectsManager.addMyEffectE -= EffectsManager_addMyEffectE;
            _unit.EffectsManager.removeMyEffectE -= EffectsManager_removeMyEffectE;
        }
        else
            Debug.LogError("DetachUnit");

        if (_isHero)
        {
            _hero.unitUpdateLevel -= GetUnitHero_unitUpdateLevel;
        }
        OffNoUseIconEffect();
        _effectsType.Clear();
        _isHero = false;
        _unit = null;
        _hero = null;
        _isFree = true;
        MainSystem.RemoveItem(this);
        gameObject.SetActive(false);
    }


    Vector3 screenPos;
    public void SystemUpdate()
    {
        if(_unit == null)
        {
            DetachUnit();
            return;
        }
        screenPos = _mainCamera.WorldToScreenPoint(_unit.thisT.position);
        transform.localPosition = (screenPos + Vector3.up * 30) / UI.GetScaleFactor();         
    }

    private void UpdateBarHP()
    {
        _sliderHP.value = _unit.HP;
    }

    private void UpdateBarShield()
    {
        _sliderShield.value = _unit.shield;
    }

    private void UpdateEffects()
    {
        for (int i = 0; i < _effectsType.Count; i++)
        {            
            IStackEffect effects = _unit.EffectsManager.Stacks[(int)_effectsType[i]];
            effectIcons[i].enabled = true;
            effectIcons[i].color = effects.getColor;
            effectIcons[i].sprite = effects.isBuff ? spriteBuff : spriteDebuff;
        }
        OffNoUseIconEffect();
    }

    private void OffNoUseIconEffect()
    {
        for (int i = _effectsType.Count; i < effectIcons.Length; i++)
        {
            if (effectIcons[i].enabled)
                effectIcons[i].enabled = false;
            else
                break;
        }

    }

}
