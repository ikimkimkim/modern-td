﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGroupGameObject : MonoBehaviour {

    public Vector2 CellSize;
    [System.Serializable]
    public struct PaddingSctruct
    {
        public float Top, Bottom, Left, Right;
    }
    public PaddingSctruct padding;
    public int Column;

    private RectTransform Content;

    private void Awake()
    {
        Content = GetComponent<RectTransform>();
        activeObj = new List<Transform>();
    }

    List<Transform> activeObj;
    public void UpdateGroup(bool onluActive)
    {
        activeObj.Clear();
        for (int i = 0; i < Content.childCount; i++)
        {
            var c = Content.GetChild(i);
            if (onluActive)
            {
                if (c.gameObject.activeSelf)
                    activeObj.Add(c);
            }
            else
                activeObj.Add(c);
        }

        int row = Mathf.CeilToInt(activeObj.Count / (float) Column);

        Content.sizeDelta = new Vector2(Content.sizeDelta.x, padding.Top + row * (CellSize.y + padding.Bottom + padding.Top));

        int index = -1;
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < Column; j++)
            {
                index = i * Column + j;
                if (index >= activeObj.Count)
                    break;
                var T = activeObj[index];
                T.GetComponent<RectTransform>().localPosition = new Vector3(
                    j * (CellSize.x + padding.Right + padding.Left) + padding.Left + CellSize.x/2f,
                    -( i * (CellSize.y +padding.Bottom + padding.Top) + padding.Top + CellSize.y/2f));
            }
        }



    }


}
