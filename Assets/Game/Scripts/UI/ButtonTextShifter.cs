﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
/// <summary>
/// Предназначен для сдвига текста при нажатии на кнопку. Нужно добавлять к кнопке.
/// </summary>
public class ButtonTextShifter : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerEnterHandler
{
    public Sprite NormalImage;
    public Sprite HighlitedImage;
    public Sprite PressedImage;

    public string UrlNormalImage, UrlHighlitedImage, UrlPressedImage;

    public float YShift;
    public float YShiftDown;
    Button _button;
    Image _image;
    Transform _textTransform;
    Vector3 _startPosition;
    public bool _wasExit = false;
    void Start()
    {
        _button = GetComponent<Button>();
        _image = GetComponent<Image>();
        //_image.eventAlphaThreshold = 0.5f;
        _textTransform = GetComponentInChildren<Text>().transform;
        _startPosition = _textTransform.localPosition;

        StartCoroutine(TextureDB.instance.load(UrlNormalImage, value => { NormalImage = value; _image.sprite = value; }));
        StartCoroutine(TextureDB.instance.load(UrlHighlitedImage, value => { HighlitedImage = value; }));
        StartCoroutine(TextureDB.instance.load(UrlPressedImage, value => { PressedImage = value; }));
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (_button.interactable)
        {
            _textTransform.localPosition = _startPosition - new Vector3(0, YShiftDown, 0);
            _image.sprite = PressedImage;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (_button.interactable)
        {
            if (!_wasExit)
            {
                _textTransform.localPosition = _startPosition + new Vector3(0, YShift, 0);
                _image.sprite = HighlitedImage;
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (_button.interactable)
        {
            _textTransform.localPosition = _startPosition;
            _image.sprite = NormalImage;
            _wasExit = true;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_button.interactable)
        {
            _textTransform.localPosition = _startPosition + new Vector3(0, YShift, 0);
            _image.sprite = HighlitedImage;
            _wasExit = false;
        }
    }
}
