﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ChangeCurToHand : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public void OnPointerEnter(PointerEventData eventData)
    {
#if (UNITY_WEBGL) 
        //Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
        //Cursor.visible = false;
#endif
    }

    public void OnPointerExit(PointerEventData eventData)
    {
#if (UNITY_WEBGL) 
        //Cursor.SetCursor(null, Vector2.zero, cursorMode);
        //Cursor.visible = true;
#endif
    }

    void OnDisable()
    {
#if (UNITY_WEBGL) 
        //Cursor.SetCursor(null, Vector2.zero, cursorMode);
        //Cursor.visible = true;
#endif
    }

}
