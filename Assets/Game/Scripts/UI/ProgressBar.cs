﻿using UnityEngine;
using System.Collections;

public class ProgressBar : MonoBehaviour {

    public RectTransform tProgressBar,  thisRect;
 
    public void SetProgress(float P)
    {
        tProgressBar.sizeDelta = new Vector2(thisRect.sizeDelta.x * Mathf.Clamp(P, 0, 1f), tProgressBar.sizeDelta.y);
        
    }
}
