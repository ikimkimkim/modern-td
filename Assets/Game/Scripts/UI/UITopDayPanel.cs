﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Globalization;

public class UITopDayPanel : MonoBehaviour
{

    public static int TypeShow=-2;
    public static bool ShowTop=false;

    public Toggle[] listToggle;
    public ToggleGroup MainToggleGroup;

    private MapType IDType = MapType.Portal;

    public Text CaptionNameMap1;

    public Toggle toogleInfo, toggleTop;
    public ToggleGroup toggleGroup;

    [Header("PanelInfoMap")]
    public GameObject PanelInfo;
    public Text StartDate;
    public Text EndDate, Place, Score;
    public Text ScoreText;

    public Image MapIcon1;

    public Text textInfoPrize;

    [System.Serializable]
    public struct PanelPrize
    {
        public Text TextPlace;
        public Image imageChest;
        public Text TextNameChest;
        public GameObject panelGold, panelCristal;
        public Text Gold, Cristal;
    }

    [Header("PrizeInfo")]
    public PanelPrize prize1;
    public PanelPrize prize2;
    public PanelPrize prize3;


    [Header("PanelTop")]
    public GameObject PanelTop;
    public Dropdown DateDropdown;
    public GameObject TextOffTopObj;
    public Text CaptionNameMap2;
    public Image MapIcon2;
    public Text PlaceTop, ScoreTop, TextScoreTop;
    public Transform ponelListTop;
    public GameObject prefabRowTop;
    public Sprite[] spriteButton;
    public GameObject Loading;
    public Scrollbar scroll;
    private List<GameObject> Rows = new List<GameObject>();
    //public GameObject topPanelSurvave, topPanelPassage;
    public GameObject prefabButton;
    public Transform TableButton;
    public Color RowColor;

    public void UpdateChecked(bool value)
    {
        if (!value)
            return;
        for (int i = 0; i < listToggle.Length; i++)
        {
            if (listToggle[i].isOn)
            {
                IDType = (MapType) i - 2;
                MyLog.Log("Set idType:" + IDType);
            }
        }
        UpdateData();
    }



    public void OnEnable()
    {
        IDType = (MapType)TypeShow;
        MyLog.Log("Enable idType:"+IDType+" :"+TypeShow);

        MainToggleGroup.RegisterToggle(listToggle[0]);
        MainToggleGroup.RegisterToggle(listToggle[1]);

        if (IDType == MapType.Portal)
        {
            listToggle[1].isOn = false;
            listToggle[0].isOn = false;
            listToggle[0].isOn = true;
            MainToggleGroup.NotifyToggleOn(listToggle[0]);
        }
        else
        {
            listToggle[0].isOn = false;
            listToggle[1].isOn = false;
            listToggle[1].isOn = true;
            MainToggleGroup.NotifyToggleOn(listToggle[1]);
        }


        toggleGroup.RegisterToggle(toogleInfo);
        toggleGroup.RegisterToggle(toggleTop);

        if (ShowTop)
        {
            toggleTop.isOn = false;
            toggleTop.isOn = true;
            toggleGroup.NotifyToggleOn(toggleTop);
        }
        else
        {
            toogleInfo.isOn = false;
            toogleInfo.isOn = true;
            toggleGroup.NotifyToggleOn(toogleInfo);
        }

       // UpdateData();
    }

    public void UpdateData()
    {
        MyLog.Log("UpdateData idType:" + IDType);
        TopDaysData data = PlayerManager.topDaysData;

       
        SetMapInfo(data.now_info[((int)IDType).ToString()]);

        SetPrize(prize1, data.prizes[0]);
        SetPrize(prize2, data.prizes[1]);
        SetPrize(prize3, data.prizes[2]);

        InitTopPanel(data.now_info[((int)IDType).ToString()], data.last_top_info[((int)IDType).ToString()]);
    }

    public void SetMapInfo(TopDaysData.NowInfo nowInfo)
    {
        CaptionNameMap1.text = TopManager.getNameMap(nowInfo.map_id, IDType);
        MapIcon1.sprite = TopManager.getIconMap(nowInfo.map_id, IDType);
        //DateTime date = DateTime.Parse(nowInfo.dates.start);
        StartDate.text = nowInfo.dates.start_text; //date.ToString("dd.MM.yyyy HH:mm:ss");

        //date = DateTime.Parse(nowInfo.dates.end);
        EndDate.text = nowInfo.dates.end_text;// date.ToString("dd.MM.yyyy HH:mm:ss");

        Place.text = nowInfo.user_res.place.ToString();

        Debug.Log("Map info score:" + nowInfo.user_res.place_info.scores + "| level:" + nowInfo.user_res.place_info.level);

        switch (IDType)
        {
            case MapType.Portal:
                if (nowInfo.user_res.place_info.level != "")
                    Score.text = nowInfo.user_res.place_info.level;
                textInfoPrize.text = "Покажи лучший результат в испытаниях <color=#F1CA37>на прохождение</color> и получи призы!";
                break;
            case MapType.Survival:
                if (nowInfo.user_res.place_info.scores != "")
                    Score.text = nowInfo.user_res.place_info.scores;
                textInfoPrize.text = "Покажи лучший результат в испытаниях <color=#F1CA37>на выживание</color> и получи призы!";
                break;
        }
    }

    public void SetPrize(PanelPrize panel, TopDaysData.PrizesInfo prizesInfo)
    {
        if (prizesInfo.from != prizesInfo.to)
            panel.TextPlace.text = prizesInfo.from + "-" + prizesInfo.to + " место";
        else
            panel.TextPlace.text = prizesInfo.from + " место";

        panel.panelGold.SetActive(prizesInfo.prizes.gold > 0);
        panel.Gold.text = prizesInfo.prizes.gold.ToString();
        panel.panelCristal.SetActive(prizesInfo.prizes.crystals > 0);
        panel.Cristal.text = prizesInfo.prizes.crystals.ToString();

        panel.TextNameChest.text = Chest.getNameChest((Chest.TypeChest)prizesInfo.prizes.chest - 1);
        StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsChest[prizesInfo.prizes.chest - 1], value => { panel.imageChest.sprite = value; }));

    }

    private int page;

    public void InitTopPanel(TopDaysData.NowInfo nowInfo,TopDaysData.LastTopInfo lastInfo)
    {
        
        DateDropdown.ClearOptions();
       /* DateTime dates = DateTime.Parse(nowInfo.dates.start);
        DateTime datee = DateTime.Parse(nowInfo.dates.end);
        CultureInfo ci = new CultureInfo("ru-RU");
        string now = dates.ToString("dd MMMM ", ci);// + "-" + datee.ToString("dd.MM.yyyy");
        dates = dates.AddDays(-2);
        datee = datee.AddDays(-2);
        
        string last = dates.ToString("dd MMMM",ci);// + "-" + datee.ToString("dd.MM.yyyy");
*/

        DateDropdown.AddOptions(new List<string> { nowInfo.dates.short_text, lastInfo.dates.short_text });
        

        switch (IDType)
        {
            case MapType.Portal:
                if (nowInfo.user_res.place_info.level != "")
                    ScoreTop.text = nowInfo.user_res.place_info.level;
                ScoreText.text = "уровень";
                break;
            case MapType.Survival:
                if (nowInfo.user_res.place_info.scores != "")
                    ScoreTop.text = nowInfo.user_res.place_info.scores;
                ScoreText.text = "очков";
                break;
        }
        LoadPage(0);
    }

    public void GetData(TopData topData)
    {
        Loading.SetActive(false);
        if (scroll != null)
            scroll.value = 1;

        TextOffTopObj.SetActive(DateDropdown.value > 0);

        CaptionNameMap2.text = TopManager.getNameMap(topData.result.top_data.info.map_id, IDType);
        MapIcon2.sprite = TopManager.getIconMap(topData.result.top_data.info.map_id, IDType);

        if (DateDropdown.value == 0)
            PlaceTop.text = PlayerManager.topDaysData.now_info[((int)IDType).ToString()].user_res.place.ToString();
        else
        {
            PlaceTop.text = PlayerManager.topDaysData.last_top_info[((int)IDType).ToString()].user_res.place.ToString();
            
        }

        for (int i = 0; i < Rows.Count; i++)
        {
            GameObject.Destroy(Rows[i]);
        }

        ScoreTop.text = "0";
        ScoreText.text = "";
        switch (IDType)
        {
            case MapType.Portal:
                if (DateDropdown.value == 0)
                {
                    if (PlayerManager.topDaysData.now_info[((int)IDType).ToString()].user_res.place_info.level != "")
                        ScoreTop.text = PlayerManager.topDaysData.now_info[((int)IDType).ToString()].user_res.place_info.level;
                    TextScoreTop.text = "уровень";
                }
                else
                {
                    if (PlayerManager.topDaysData.last_top_info[((int)IDType).ToString()].user_res.place_info.level != "")
                        ScoreTop.text = PlayerManager.topDaysData.last_top_info[((int)IDType).ToString()].user_res.place_info.level;
                    TextScoreTop.text = "уровень";
                }
                break;
            case MapType.Survival:
                if (DateDropdown.value == 0)
                {
                    if (PlayerManager.topDaysData.now_info[((int)IDType).ToString()].user_res.place_info.scores != "")
                        ScoreTop.text = PlayerManager.topDaysData.now_info[((int)IDType).ToString()].user_res.place_info.scores;
                    TextScoreTop.text = "очков";
                }
                else
                {
                    if (PlayerManager.topDaysData.last_top_info[((int)IDType).ToString()].user_res.place_info.scores != "")
                        ScoreTop.text = PlayerManager.topDaysData.last_top_info[((int)IDType).ToString()].user_res.place_info.scores;
                    TextScoreTop.text = "очков";
                }
                break;
        }


        Rows.Clear();

        ITopUserRow tur;
        GameObject go;
        //System.DateTime dt = new System.DateTime();

        //topPanelPassage.SetActive(IDType == -2);
        //topPanelSurvave.SetActive(IDType == -1);


        for (int i = 0; i < topData.result.top_data.top.Length; i++)
        {
            go = GameObject.Instantiate(prefabRowTop, ponelListTop);
            go.transform.localScale = new Vector3(1, 1, 1);
            Rows.Add(go);
            tur = go.GetComponent<ITopUserRow>();

            if (i % 2 != 0)
            {
                tur.SetFonColor(RowColor);
            }

            tur.SetData(topData.result.top_data.top[i], IDType);
        }


        for (int i = 0; i < topData.result.top_data.pages.Length; i++)
        {
            go = GameObject.Instantiate(prefabButton, TableButton);
            go.transform.localScale = new Vector3(1, 1, 1);
            Rows.Add(go);
            if (i == 0)
            {
                go.GetComponent<Image>().sprite = spriteButton[0];
            }
            else if (i == topData.result.top_data.pages.Length - 1)
            {
                go.GetComponent<Image>().sprite = spriteButton[2];
            }
            else
                go.GetComponent<Image>().sprite = spriteButton[1];

            go.GetComponentInChildren<Text>().text = topData.result.top_data.pages[i];
            int index = i;
            go.GetComponent<Button>().interactable = i != PageIndex;
            go.GetComponent<Button>().onClick.AddListener(() => { LoadPage(index); });

        }

    }

    private int PageIndex;
    public void LoadPage(int index)
    {
        PageIndex = index;
        Loading.SetActive(true);
        PlayerManager.DM.GreatDayTop((int)IDType, DateDropdown.value, index, GetData);
    }




    TimeSpan ts;
    public void Update()
    {
        ts = DateTime.Parse(PlayerManager.topDaysData.now_info[((int)IDType).ToString()].dates.end) - DateTime.Now;
        if (ts.TotalSeconds > 0)
        {
            double H = Math.Floor(ts.TotalHours);
            EndDate.text = (H < 10 ? "0" + H : H.ToString()) + ":" + (ts.Minutes < 10 ? "0" + ts.Minutes : ts.Minutes.ToString()) + ":" + (ts.Seconds < 10 ? "0" + ts.Seconds : ts.Seconds.ToString());
        }
        else
        {
            EndDate.text = "00:00:00";
            TopManager.instance.Hide();
            //PlayerManager.InitializationСompleted += TopManager.instance.UpdateTopInfo;
            SocialManager.UpdateInfoPlayer();
        }
    }
}
