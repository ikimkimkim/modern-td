﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public abstract class UIEntity : MonoBehaviour
{
    public abstract int entityID { get; }
    public abstract int priorityShow { get; }
    //private bool isShow;

    [HideInInspector]
    public UnityEvent ShowE, HideE;

    public virtual void Awake()
    {
        //isShow = false;
        ShowE = new UnityEvent();
        HideE = new UnityEvent();
        UIEntityManager.AddEntity(this);
    }

    public virtual  void OnDestroy()
    {
        UIEntityManager.RemoveEntity(this);
    }

    protected void ShowUIEvent()
    {
        //Debug.LogWarning("Show " + entityID);
        ShowE.Invoke();
        //isShow = true;
    }

	protected void HideUIEvent()
    {
        //Debug.LogWarning("Hide " + entityID);
        HideE.Invoke();
        //isShow = false;
    }

    public abstract bool NeedShow();
}
