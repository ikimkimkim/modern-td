﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using TDTK;

public class UITopDayPanelPrize : MonoBehaviour {

    public Button buttonYes;
    public Text ButtonText;
    public Text caption, placeText;
    public Image imageChest;
    public Text chestName;
    public GameObject placeObjs;
    public GameObject goldObj, crystalObj, chestObj, TextNotPrizeObj,TextPrizeObj;
    public Text goldText, crystalText;

    public Sprite NotChest;

    [Header("Anim")]
    public GameObject animObj;
    public GameObject animGold, animCrystal;
    public Transform animTGold, animTCrystal;
    public Text animTextGold, animTextCrystal;
    public Transform animTargetGold, animTargetCrystal;
    private Coroutine corAnim;
    public Text AllGold, AllCrystal;

    private int idType;


    public void SetData(int place, TopDaysData.LastTopPrize[] listPrizes)
    {
        buttonYes.interactable = true;
        if (listPrizes.Length == 0)
        {
            TextNotPrizeObj.SetActive(true);
            TextPrizeObj.SetActive(false);
            ButtonText.text = "Закрыть";
            chestObj.SetActive(false);
        }
        else
        {
            TextPrizeObj.SetActive(true);
            TextNotPrizeObj.SetActive(false);
            ButtonText.text = "Забрать приз";
            chestObj.SetActive(true);

            for (int i = 0; i < listPrizes.Length; i++)
            {
                switch (listPrizes[i].type)
                {
                    case "chest":
                        SetChest(listPrizes[i].data);
                        break;
                    case "gold":
                        SetGold(listPrizes[i].data);
                        break;
                    case "crystals":
                        SetCrystal(listPrizes[i].data);
                        break;
                    default:
                        Debug.LogError("Top dat prize, new type:" + listPrizes[i].type);
                        break;
                }

            }
        }

        if (place > 0)
        {
            placeObjs.SetActive(true);
            placeText.text = place.ToString();
        }
        else
            placeObjs.SetActive(false);

        switch(idType)
        {
            case -1:
                caption.text = "НА ВЫЖИВАНИЕ";
                break;
            case -2:
                caption.text = "НА ПРОХОЖДЕНИЕ";
                break;
        }
    }

    public int ChestId;
    public void SetChest(int data)
    {
        ChestId = data - 1;
        chestName.text = Chest.getNameChest((Chest.TypeChest)data - 1);
        StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsChest[data - 1], value => { imageChest.sprite = value; }));
    }
    public void SetGold(int data)
    {
        goldObj.SetActive(true);
        goldText.text = data.ToString();
    }
    public void SetCrystal(int data)
    {
        crystalObj.SetActive(true);
        crystalText.text = data.ToString();
    }

    public void OnEnable ()
    {
        goldObj.SetActive(false);
        crystalObj.SetActive(false);
        animObj.SetActive(false);
        chestName.text = "";
        imageChest.sprite = NotChest;
        ChestId = 0;
        foreach (var top_info in PlayerManager.topDaysData.last_top_info)
        {
            if (top_info.Value.prize_geted != 1 && top_info.Value.user_res.place > 0)
            {
                idType = int.Parse(top_info.Key);
                SetData(top_info.Value.user_res.place, top_info.Value.prizes);
                break;
            }
        }
    }

    public void OnGetPrizeButton()
    {
        buttonYes.interactable = false;
        PlayerManager.DM.getDayTop(idType, ResultGetPrize);
    }

    private ResponseDataTopDayPrize ResponseData;
    public void ResultGetPrize(ResponseDataTopDayPrize rp)
    {
        Debug.Log("ResultGetPrize");

        if (rp.ok == 0)
        {
            Debug.LogError("Rusilt prize Error: " + rp.error.code + " -> " + rp.error.text);
            TooltipMessageServer.Show(rp.error.text);

            PlayerManager.SetPlayerData(rp.user);
            RemoveDataPrize();
        }
        else
        {
            if (rp.prizes.chest != null)
            {
                ResponseData = rp;
                animObj.SetActive(true);
                AnimData();
            }
            else
            {
                PlayerManager.SetPlayerData(rp.user);
                RemoveDataPrize();
            }
        }
    }


    public void AnimData()
    {
        SpeedAnim = 2;
        animTextGold.text = ResponseData.prizes.gold.ToString();      
        animTextCrystal.text = ResponseData.prizes.crystals.ToString();

        animGold.SetActive(/*ResponseData.prizes.gold != null &&*/ ResponseData.prizes.gold > 0);
        animCrystal.SetActive(false);
        StartCoroutine(StartAnim(animTGold, animTargetGold, () =>
        {
            AllGold.text = (PlayerManager._instance._playerData.Gold + ResponseData.prizes.gold).ToString();
            if (/*ResponseData.prizes.crystals != null &&*/ ResponseData.prizes.crystals > 0)
            {
                animCrystal.SetActive(/*ResponseData.prizes.crystals != null &&*/ ResponseData.prizes.crystals > 0);
                StartCoroutine(StartAnim(animTCrystal, animTargetCrystal, () => {
                    AllCrystal.text = (PlayerManager._instance._playerData.Crystals + ResponseData.prizes.crystals).ToString();
                    EndAnim();
                }));
            }
            else
                EndAnim();
        }));
    }

    public void EndAnim()
    {
        Debug.Log("EndAnim");
        PlayerManager.SetPlayerData(ResponseData.user);
        //List<UnitTower> bd = TowerDB.Load();
        List<Card> newCard = new List<Card>();

        foreach (ResponseDataTopDayPrize.PrizesTower PrizesTower in ResponseData.prizes.chest.towers)
        {
            for (int i = 0; i < PlayerManager.CardsList.Count; i++)
            {
                if (PlayerManager.CardsList[i].IDTowerData == PrizesTower.type - 1 && (int)PlayerManager.CardsList[i].rareness == PrizesTower.rarity - 1)
                {
                    newCard.Add(PlayerManager.CardsList[i]);
                    newCard[newCard.Count - 1].AddCount = PrizesTower.count;
                    newCard[newCard.Count - 1].count -= PrizesTower.count;
                    break;
                }
            }
        }
        Chest c = new Chest(-1, (Chest.TypeChest)ChestId, "", 0, 0, 0);
        ChestManager.instance.EndOpen(c, newCard, ResponseData.prizes.chest.gold, ResponseData.prizes.chest.crystals,ResponseData.prizes.chest.craft_resources);
        //ChestOpenPanel.OnHide += RemoveDataPrize;
        RemoveDataPrize();
        SpeedAnim = 2;
    }

    void Update()
    {
        if(Input.GetMouseButton(0))
            SpeedAnim = 6;
    }

    public void RemoveDataPrize()
    {
        //TopManager.instance.Hide();
        ChestOpenPanel.OnHide -= RemoveDataPrize;
        Debug.Log("Romove data prize type:"+idType);
        PlayerManager._instance._playerData.day_tops.last_top_info[idType.ToString()].prize_geted = 1;
        TopManager.instance.Hide();
    }

    public float SpeedAnim = 2;
    public Vector3 offsetTarget;

    public IEnumerator StartAnim(Transform Tobj,Transform target, System.Action action)
    {
        Tobj.localPosition = new Vector3(0,0,0);
        float scale = 0;
        while(scale<1)
        {
            scale += Time.deltaTime* SpeedAnim;
            Tobj.localScale = new Vector3(scale, scale, scale);
            yield return null;
        }
        yield return null;
        float dist = Vector3.Distance(Tobj.position, target.position+ offsetTarget);
        Vector3 start = Tobj.position;
        float process = 0;
        while (dist>0.1f)
        {
            process += Time.deltaTime * SpeedAnim;
            Tobj.position = Vector3.Lerp(start, target.position+ offsetTarget, process );
            yield return null;
            dist = Vector3.Distance(Tobj.position, target.position+ offsetTarget);
        }
        Tobj.gameObject.SetActive(false);

        yield return null;

        action();
        yield break;
    }

}



public class ResponseDataTopDayPrize
{
    public int ok;
    public Info info;
    public int great_new_place;
    public PlayerData user;
    public ErrorData error;
    public PrizesData prizes;
    //"stars_config":{"1":1,"2":2,"3":3}
    public class Info
    {
        public int ok;
        public PrizesData prizes;
    }

    public class ErrorData
    {
        public string code;
        public string text;
    }

    public class PrizesData
    {
        public int gold;
        public int crystals;
        public PrizesChest chest;
    }

    public class PrizesChest
    {
        public int gold;
        public int crystals;
        public PrizesTower[] towers;
        public Dictionary<string, int> craft_resources;
    }


    public class PrizesTower
    {
        public int rarity;
        public int type;
        public int count;
    }
}
