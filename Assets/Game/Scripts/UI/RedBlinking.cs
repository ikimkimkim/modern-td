﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Используется в игре когда не хватает маны или золота мигают цифры
/// </summary>
public class RedBlinking : MonoBehaviour
{

    public Text Target;
    public float BlinkTime;
    static float _maxBlinkTime = 1.5f;
    static float _changeTime = 0.25f;
    static Color _redColor = new Color(1f, 0f, 0f);
    float _timer = 0;
    bool _isBlinking = false;
    bool _RedBlink = false;
    Color _normalColor;
    GradientText _gradient = null;
    Outline _outline = null;
    void Awake()
    {
        _normalColor = Target.color;
        _gradient = Target.GetComponent<GradientText>();
        _outline = Target.GetComponent<Outline>();
    }
    public static void StartBlinking(GameObject targetOwner)
    {
        var blink = targetOwner.GetComponent<RedBlinking>();
        blink._isBlinking = true; 
        blink.BlinkTime = _maxBlinkTime;
    }
    public static void StopBlinking(GameObject targetOwner)
    {
        var blink = targetOwner.GetComponent<RedBlinking>();
        blink._isBlinking = false;
        blink._timer = 0;
        blink.BlinkTime = 0;
        blink._RedBlink = false;
        blink.MakeNormal();
    }
    void Update()
    {
        if (_isBlinking)
        {
            _timer += Time.deltaTime;
            if (_timer >= _changeTime)
            {
                BlinkTime -= _timer;
                _timer = 0;
                if (_RedBlink)
                {
                    _RedBlink = false;
                    MakeNormal();
                }
                else
                {
                    _RedBlink = true;
                    MakeRed();
                }
               
                if (BlinkTime <= 0)
                {
                    _isBlinking = false;
                    _RedBlink = false;
                    MakeNormal();
                }
            }
        }

    }
    void MakeNormal()
    {
        Target.color = _normalColor;
        if (_gradient != null)
        {
            _gradient.enabled = true;
        }
        if (_outline != null)
        {
            _outline.enabled = true;
        }
    }
    void MakeRed()
    {
        Target.color = _redColor;
        if (_gradient != null)
        {
            _gradient.enabled = false;
        }
        if (_outline != null)
        {
            _outline.enabled = false;
        }
    }
   
    void OnDisable()
    {
        _isBlinking = false;
        _timer = 0;
        BlinkTime = 0;
        _RedBlink = false;
        MakeNormal();
    }

}
