﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class TowerLevelInfo : MonoBehaviour {

    public GameObject[] Stars;
    public GameObject UpLevel;
    private Image UpLevelImage;
    public Color ColorBlink;
    public UnitTower tower;

    Coroutine CanUp;
    private bool isUpgraded;


    public void SetTower(UnitTower t)
    {
        //Debug.Log("TLI -> Set Tower" + t.name);
        tower = t;
        tower.onThisUpgradedE += OnDestroyThisTower;
        tower.onThisStartSoldE += OnDestroyThisTower;
        tower.onMyStartDestroyedE+= OnDestroyThisTower;
        Unit.onDestroyedE += OnDestroyTower;

        ResourceManager.onRscChangedE += OnResourceChanged;
        UI.onChangeFullScreen += OnChangeFullScreen;
        isUpgraded = false;
        UpLevelImage = UpLevel.GetComponent<Image>();

        OnChangeFullScreen();
        OnResourceChanged(null);
    }



    public void OnChangeFullScreen()
    {
        transform.position = Camera.main.WorldToScreenPoint(tower.thisT.position);
    }

    void OnDestroy()
    {
        //Debug.LogWarning("Destroy TowerLevelInfo");
        ResourceManager.onRscChangedE -= OnResourceChanged;
        UI.onChangeFullScreen -= OnChangeFullScreen;
        Unit.onDestroyedE -= OnDestroyTower;
        /*if (tower!=null)
        {
            tower.onThisUpgradedE -= ThisUpgraded;
            tower.onThisStartSoldE -= StartSold;
        }*/
        tower.onMyStartDestroyedE -= OnDestroyThisTower;
        tower.onThisUpgradedE -= OnDestroyThisTower;
        tower.onThisStartSoldE -= OnDestroyThisTower;
    }

   /* void Update()
    { 
        if(tower != null)
            transform.position = Camera.main.WorldToScreenPoint(tower.thisT.position);
    }*/

    /*public void ThisUpgraded(UnitTower t)
    {
        isUpgraded = true;
        UnitTower.onConstructionCompleteE += CompliteUpgraded;
        Active(false);
    }*/

    /*public void CompliteUpgraded(UnitTower t)
    {
        isUpgraded = false;
        UnitTower.onConstructionCompleteE -= CompliteUpgraded;
        SetTower(t);
        TowerLevelInfoManager.instance.ChangeUI();
    }*/

    public void OnResourceChanged(List<float> valueChangedList)
    {
        if (tower != null && tower.ReadyToBeUpgrade()>0 && 
            ResourceManager.GetResourceArray()[0] >= tower.MyCard.NextLevelTower[0].GetCost()[0])
        {
            if (CanUp == null)
                CanUp = StartCoroutine(onCanUp());
        }
        else if(CanUp!=null)
        {
            StopCoroutine(CanUp);
            UpLevelImage.color = Color.white;
            CanUp = null;
        }
    }

    private IEnumerator onCanUp()
    {
        while(true)
        {
            UpLevelImage.color = ColorBlink;
            yield return new WaitForSeconds(1f);
            UpLevelImage.color = Color.white;
            yield return new WaitForSeconds(1f);
        }
    }

    void OnDestroyTower(Unit t)
    {
        if (t.IsTower)
        {
            if (t.GetUnitTower == tower)
            {
                if (CanUp != null)
                    StopCoroutine(CanUp);
                TowerLevelInfoManager.instance.DestroyTower(this);
            }
        }
    }

    void OnDestroyThisTower(Unit t)
    {
        if (CanUp != null)
            StopCoroutine(CanUp);
        TowerLevelInfoManager.instance.DestroyTower(this);
    }

   /* public void StartSold(UnitTower t)
    {
        try
        {
            isUpgraded = true;
            Active(false);
        }
        catch(Exception ex)
        {
            Debug.LogError("TowerLevelInfo Start sold error -> "+ex.Message+"\r\n"+ex.StackTrace);
        }
    }
    */

    public void Active(bool active)
    {
        //Debug.Log("TLI -> Active " + active + " "+tower.name);
        if (UpLevel == null)
        {
            MyLog.LogWarning("TLI -> active uplevel is null!");
            return;
        }

        ActiveImage(active && isUpgraded ==false);
    }

    public void ActiveImage(bool priority)
    {
       // Debug.Log("TLI -> ActiveImage " + priority + " " + tower.name);

        UpLevel.SetActive(priority ? tower.ReadyToBeUpgrade() > 0 : false);
        for (int i = 0; i < Stars.Length; i++)
        {
            Stars[i].SetActive(priority ? tower.GetLevel() > i+1 : false);
        }
        //OnResourceChanged(null);
    }
    

    
}
