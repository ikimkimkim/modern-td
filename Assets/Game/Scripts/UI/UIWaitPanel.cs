﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWaitPanel : MonoBehaviour
{
    public static UIWaitPanel instance { get; private set; }

    public GameObject panelWait;

	void Awake ()
    {
        instance = this;
        if (panelWait.activeSelf)
            Hide();
	}
	

    public static void Show()
    {
        if(instance!=null)
        instance._Show();
    }

    private void _Show()
    {
        if(panelWait.activeSelf == false)
            panelWait.SetActive(true);
    }

    public static void Hide()
    {
        if (instance != null)
            instance._Hide();
    }

    private void _Hide()
    {
        if (panelWait.activeSelf)
            panelWait.SetActive(false);
    }
}
