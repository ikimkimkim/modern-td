﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MiniSelectedCardUpgradeUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private static List<Card> allSelectedCard;

    public Card selectedCard { get; private set; }
    List<CardUpgradeData> dbCardUpdate;

    [SerializeField]
    private List<int> IDCardSelected;
    [SerializeField]
    private Image Fon;
    [SerializeField]
    private Sprite[] FonButtonForRarenes;

    [SerializeField]
    private Image imageSelectedCard;
    [SerializeField]
    private Color colorSelected, colorNotSelected;

    [SerializeField]
    private GameObject  plus;

    [SerializeField]
    private Text textInfo;
    [SerializeField]
    private GameObject tooltip;

    static MiniSelectedCardUpgradeUI()
    {
        allSelectedCard = new List<Card>();
    }

    void Start()
    {
        dbCardUpdate = DataBase<CardUpgradeData>.Load(DataBase<CardUpgradeData>.NameBase.CardUpgrade);
        ClearSelectedCard();
    }

    public void OnSelected()
    {
        var cards = PlayerManager.CardsList.FindAll(i => i.Type == _CardType.Upgrade && IDCardSelected.Contains(i.IDTowerData)/* && 
            (allSelectedCard.Exists(c=>c.IDTowerData==i.IDTowerData)==false) || (selectedCard!=null && i.IDTowerData==selectedCard.IDTowerData)*/);
        if (rarenessFilter != _CardRareness.none)
            cards = cards.FindAll(i => i.rareness >= rarenessFilter);
        UIMiniPanelSelectedCard.Show(cards, SelectedCard);
    }


    private void SelectedCard(Card card)
    {
        if (selectedCard == card)
            return;

        if (selectedCard != null)
            allSelectedCard.Remove(selectedCard);

        if (card != null)
        {
            selectedCard = card;
            allSelectedCard.Add(selectedCard);
            UpdateCardData();
        }
        else
        {
            ClearSelectedCard();
        }
    }

    private _CardRareness rarenessFilter;
    public void SetRarenesFilter(_CardRareness rareness)
    {
        rarenessFilter = rareness;
    }

    private void UpdateCardData()
    {
        imageSelectedCard.color = colorSelected;
        plus.SetActive(false);
        Fon.sprite = FonButtonForRarenes[selectedCard.getIntRareness+1];
        textInfo.text = selectedCard.UpgradeData.GetShortDesp(selectedCard.rareness);
    }

    public void ClearSelectedCard()
    {
        if (selectedCard != null)
            allSelectedCard.Remove(selectedCard);

        selectedCard = null;
        plus.SetActive(true);
        imageSelectedCard.color = colorNotSelected;// iconNotSelected;
        Fon.sprite = FonButtonForRarenes[0];
        textInfo.text = string.Format("<color=#eacf22>Слот для карты усиления:</color>\r\n {0}", dbCardUpdate[IDCardSelected[0]- 500].generalDesp); //"Не выбранно";
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tooltip.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        tooltip.SetActive(true);
    }

    void OnEnable()
    {
        if(selectedCard!=null)
            allSelectedCard.Add(selectedCard);
        SetRarenesFilter(_CardRareness.none);

        PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;
    }

    void OnDisable()
    {
        if (selectedCard != null)
            allSelectedCard.Remove(selectedCard);


        PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;

    }

    private void PlayerManager_InitializationСompleted()
    {
        if (selectedCard == null)
            return;
        var card = PlayerManager.CardsList.Find(i => i.ID == selectedCard.ID);
        ClearSelectedCard();
        SelectedCard(card);
    }
}
