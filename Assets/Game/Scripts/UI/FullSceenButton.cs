﻿using UnityEngine;
using System.Collections;
using TDTK;
using UnityEngine.Events;

public class FullSceenButton : MonoBehaviour {

    private static UnityEvent ChangeScreen;
    public static void AddListener(UnityAction action)
    {
        if (ChangeScreen == null)
            ChangeScreen = new UnityEvent();
        ChangeScreen.AddListener(action);
    }
    public static void RemoveListener(UnityAction action)
    {
        if (ChangeScreen == null)
            ChangeScreen = new UnityEvent();
        ChangeScreen.RemoveListener(action);
    }

    public void Awake()
    {
        if (ChangeScreen == null)
            ChangeScreen = new UnityEvent();
    }

    public void OnFullScreen()
    {
        Screen.fullScreen = !Screen.fullScreen;
        ChangeScreenDelay(0.1f);
    }
    
	IEnumerator ChangeScreenDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        if (ChangeScreen != null)
            ChangeScreen.Invoke();
    }
}
