﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class UIEvents : MonoBehaviour
    , IPointerClickHandler // 2
     , IPointerEnterHandler
     , IPointerExitHandler
{
    public UnityEvent OnClick, OnMouseEnter, OnMouseExit;

    public void OnPointerClick(PointerEventData eventData)
    {
        OnClick.Invoke();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        OnMouseEnter.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        OnMouseExit.Invoke();
    }


}
