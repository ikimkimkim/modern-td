﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStarPerk : MonoBehaviour {

    public static event Action onDropCard, onDropAll;
    private static UIStarPerk instance;
    [SerializeField]
    private GameObject Panel;
    [SerializeField]
    private UIStarPerkProgressBar[] Bars;
    [SerializeField]
    private UIStarPerkCombinations Combinations;
    [SerializeField]
    private UIStarPerkSelectionPanel SelectionPanel;

    [SerializeField]
    private Text Stars_text, StarsRed_text;
    private int star_left, starRed_left;
    public static int[] StarsLeft { get { return new int[] { instance.star_left, instance.starRed_left }; } }

    [SerializeField]
    private Animator animator;
    [Header("ButtonInfo")]
    [SerializeField]
    private Button buttonInfoObj;

    [SerializeField]
    private GameObject buttonInfoPanel;
    [SerializeField]
    private GameObject buttonTimerPanel;
    [SerializeField]
    private ParticleSystem buttonInfoEffectPS;
    [SerializeField]
    private Color[] RarenesColorPS;
    [SerializeField]
    private Text buttonInfoPanelText, buttonInfoPanelTimer;
    [SerializeField]
    private Color colordef, color1, color2;

    [Header("Tutorial")]
    [SerializeField] private GameObject ArrowButtonOpenPanel;
    [SerializeField] private GameObject FrameShowCardSelected, FrameShowTry, FrameShowCombination;


    void Start()
    {
        instance = this;
        UpdateData();
    }

    public void Show()
    {
        Panel.SetActive(true);
        UpdateData();
        animator.SetTrigger("OpenPopap");
    }

    public void Hide()
    {
        if (isTutorial)
            return;
        Panel.SetActive(false);
    }
    void OnEnable()
    {
        PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;
    }

    void OnDisable()
    {
        PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;
    }

    private void PlayerManager_InitializationСompleted()
    {
        if (waitRefreshData == false)
            return;
        waitRefreshData = false;
        UpdateData();
        animator.SetTrigger("OpenPopap");
    }

    private void UpdateData()
    {
        for (int i = 0; i < Bars.Length; i++)
        {
            Bars[i].UpdateData();
        }
        Combinations.UpdateData();
        SelectionPanel.UpdateData();
        UpdateStarsCounter();
        UpdateButtonInfo();
        UpdateAnimator();
    }

    private void UpdateStarsCounter()
    {
        int star_total = 0;
        int starRed_total = 0;
        int count;
        for (int i = 0; i < PlayerManager._instance.GetLevelsCountComplele(); i++)
        {
            count = PlayerManager._instance._playerData.Levels[i].stars;
            if (count >= 3)
            {
                star_total += 3;
                starRed_total += count - 3;
            }
            else
            {
                star_total += count;
            }
        }

        star_left = star_total;
        starRed_left = starRed_total;
        int countTower = 0, countAbility = 0, countHero = 0, countEconomy = 0;
        foreach (int id in PlayerManager.GetPurchasedID)
        {
            switch(StarsPerk.GetGroupPerk(id))
            {
                case BaseStarPerk.Group.Tower:
                    star_left -= StarsPerk.GetPriceStar(id, countTower);
                    starRed_left -= StarsPerk.GetPriceRedStar(id, countTower);
                    if(StarsPerk.isPropertiesPerk(id))
                        countTower++;
                    break;
                case BaseStarPerk.Group.Ability:
                    star_left -= StarsPerk.GetPriceStar(id, countAbility);
                    starRed_left -= StarsPerk.GetPriceRedStar(id, countAbility);
                    if (StarsPerk.isPropertiesPerk(id))
                        countAbility++;
                    break;
                case BaseStarPerk.Group.Hero:
                    star_left -= StarsPerk.GetPriceStar(id, countHero);
                    starRed_left -= StarsPerk.GetPriceRedStar(id, countHero);
                    if (StarsPerk.isPropertiesPerk(id))
                        countHero++;
                    break;
                case BaseStarPerk.Group.Economy:
                    star_left -= StarsPerk.GetPriceStar(id, countEconomy);
                    starRed_left -= StarsPerk.GetPriceRedStar(id, countEconomy);
                    if (StarsPerk.isPropertiesPerk(id))
                        countEconomy++;
                    break;
            }
        }

        Stars_text.text = star_left + "/" + star_total;
        StarsRed_text.text = starRed_left + "/" + starRed_total;


    }

    private void UpdateAnimator()
    {
        animator.SetBool("Show_0", false);
        animator.SetBool("Show_1", false);
    }

    private int ID_buf, Index_buf;
    public void SelectedCard(int index)
    {
        Index_buf = index;
        ID_buf = SelectionPanel.GetIDCard(index);
        animator.SetBool("Show_" + Index_buf, true);
    }

    public void DeselectedCard()
    {
        if (Index_buf < 0 || ID_buf < 0)
            return;
        animator.SetBool("Show_" + Index_buf, false);
        Index_buf = -1;
        ID_buf = -1;
    }

    public void BuySelectedPerk()
    {
        if (Index_buf < 0 || ID_buf < 0)
            return;

        int priceStar = 0, priceRedStar = 0;
        priceStar = StarsPerk.GetPriceStar(ID_buf);
        priceRedStar = StarsPerk.GetPriceRedStar(ID_buf);
        if (star_left < priceStar)
        {
            RedBlinking.StartBlinking(Stars_text.gameObject);
            return;
        }
        if (starRed_left < priceRedStar)
        {
            RedBlinking.StartBlinking(StarsRed_text.gameObject);
            return;
        }
        PlayerManager.DM.BuyPerk(ID_buf, ResponseSelectedCard);
    }
       
    private void ResponseSelectedCard(ResponseData data)
    {
        PlayerManager.SetPlayerData(data.user);
        if(data.ok != 1)
        {
            Debug.LogErrorFormat("{0}:{1}", data.error.code, data.error.text);
            TooltipMessageServer.Show(data.error.text, 5);
            return;
        }

        if(StarsPerk.isPropertiesPerk(ID_buf))
            animator.SetTrigger("SelectedCardCombination_" + Index_buf);
        else
            animator.SetTrigger("SelectedCardBar_" + Index_buf);


    }

    public void DropCard()
    {
        StartWaitRefreshData();
        PlayerManager.DM.BuyForCrystals("stars_change", ResponseDropCard);
    }

    public void DropAll()
    {
        StartWaitRefreshData();
        PlayerManager.DM.BuyForCrystals("stars_drop", ResponseDropAll);
    }

    private void ResponseDropCard(ResponseData data)
    {
        PlayerManager.SetPlayerData(data.user);
        if (data.ok != 1)
        {
            Debug.LogErrorFormat("{0}:{1}", data.error.code, data.error.text);
            TooltipMessageServer.Show(data.error.text, 5);
            return;
        }
        if (onDropCard != null)
            onDropCard();
    }
    private void ResponseDropAll(ResponseData data)
    {
        PlayerManager.SetPlayerData(data.user);
        if (data.ok != 1)
        {
            Debug.LogErrorFormat("{0}:{1}", data.error.code, data.error.text);
            TooltipMessageServer.Show(data.error.text, 5);
            return;
        }
        if (onDropAll != null)
            onDropAll();
    }

    private void StartWaitRefreshData()
    {
        waitRefreshData = true;
        waitStartTime = Time.unscaledTime;
    }


    private bool waitRefreshData;
    private float waitStartTime;
    DateTime timer;
    void Update()
    {
        if (buttonInfoObj.interactable == false)
            return;
        if (waitRefreshData && Time.unscaledTime > waitStartTime + 60)
        {
            Debug.LogWarningFormat("StarPerk, wait update player data time out!");
            StartWaitRefreshData();
            PlayerManager.DM.getPlayerData();
        }

        if (waitRefreshData)
            return;

        timer = new DateTime((long)(PlayerManager.TimeStarsCard) * 10000000);

        if (PlayerManager.TimeStarsCard <= 0)
        {
            StartWaitRefreshData();
            PlayerManager.DM.getPlayerData();
        }

        buttonInfoPanelTimer.text = timer.ToString("mm:ss");
        SelectionPanel.SetTimer(buttonInfoPanelTimer.text);
    }


    private Coroutine coroutineButtonInfoColor;
    public void UpdateButtonInfo()
    {
        buttonInfoObj.interactable = PlayerManager._instance.GetLevelsCountComplele() >= 7;
        buttonTimerPanel.SetActive(buttonInfoObj.interactable);
        if (buttonInfoObj.interactable == false)
        {
            buttonInfoEffectPS.Stop();
            buttonInfoPanel.SetActive(false);
            return;
        }
        int count = star_left + starRed_left;
        buttonInfoPanel.SetActive(count > 0);
        buttonInfoPanelText.text = count.ToString();

        if (count > 5)
            StartChangeColor();
        else
            StopChangeColor();
        ParticleSystem.MainModule main;
        switch (SelectionPanel.GetMaxRarenes())
        {
            case _CardRareness.Usual:
                buttonInfoEffectPS.Stop();
                break;
            case _CardRareness.Magic:
                buttonInfoEffectPS.Play();
                main = buttonInfoEffectPS.main;
                main.startColor = RarenesColorPS[0];
                break;
            case _CardRareness.Rare:
                buttonInfoEffectPS.Play();
                main = buttonInfoEffectPS.main;
                main.startColor = RarenesColorPS[1];
                break;
            case _CardRareness.Legendary:
                buttonInfoEffectPS.Play();
                main = buttonInfoEffectPS.main;
                main.startColor = RarenesColorPS[2];
                break;
        }

    }


    private void StartChangeColor()
    {
        if (coroutineButtonInfoColor == null)
            coroutineButtonInfoColor = StartCoroutine(ChangeColor());
    }

    private void StopChangeColor()
    {
        if (coroutineButtonInfoColor != null)
            StopCoroutine(coroutineButtonInfoColor);
        buttonInfoPanelText.color = colordef;
    }

    private IEnumerator ChangeColor()
    {
        while (true)
        {
            buttonInfoPanelText.color = color1;
            yield return new WaitForSeconds(1);
            buttonInfoPanelText.color = color2;
            yield return new WaitForSeconds(1);
        }
    }

    public void ShowTooltip(int index)
    {
        animator.SetBool("ShowTooltip_"+index, true);
    }

    public void HideTooltip()
    {
        animator.SetBool("ShowTooltip_0", false);
        animator.SetBool("ShowTooltip_1", false);
    }

    public void EndAnimation()
    {
        if (Panel.activeSelf == false)
            return;
        if(_tutorialStatus == StatusTutorial.waitOpen || _tutorialStatus == StatusTutorial.ShowCardSelected)
            _AddStatusTutorial();
    }

    #region Tutor
    private enum StatusTutorial { waitOpen, ShowCardSelected, ShowTry, ShowCombination, end }
    [SerializeField] private StatusTutorial _tutorialStatus;
    private Action _endTutorialEvent;
    private bool isTutorial = false;


    public static void StartTutorial(Action endTutorial)
    {
        instance._StartTutorial(endTutorial);
    }

    private void _StartTutorial(Action endTutorial)
    {
        isTutorial = true;
        _endTutorialEvent = endTutorial;
        _tutorialStatus = StatusTutorial.waitOpen;
        _UpdateStatus();
    }

    private void _AddStatusTutorial()//StatusTutorial status)
    {
        if (isTutorial == false)
            return;
        //if (_tutorialStatus != status)
        //    return;
        Debug.Log("_AddStatusTutorial");
        _tutorialStatus++;
        _UpdateStatus();
    }


    private void _UpdateStatus()
    {
        switch (_tutorialStatus)
        {
            case StatusTutorial.waitOpen:
                ArrowButtonOpenPanel.SetActive(true);
                break;
            case StatusTutorial.ShowCardSelected:
                ArrowButtonOpenPanel.SetActive(false);
                FrameShowCardSelected.SetActive(true);
                GeneralPanel.StartShow(null, new GeneralState[]{
                    new GeneralState("Вам на выбор предоставляются две технологии. Они меняются автоматически каждый час или же за кристаллы.",
                    GeneralPos.Tactic, GeneralOrientation.right),
                    new GeneralState("Вам необходимо выбрать одну из технологий.",
                    GeneralPos.Teaches, GeneralOrientation.right)
                });

                break;
            case StatusTutorial.ShowTry:
                FrameShowCardSelected.SetActive(false);
                FrameShowTry.SetActive(true);
                GeneralPanel.StartShow(_AddStatusTutorial, new GeneralState[]{
                    new GeneralState("Справа вы наблюдаете дерево улучшений. Тут находятся четыре возможных пути развития.",
                    GeneralPos.Tactic),
                    new GeneralState("Улучшить все ветки невозможно, поэтому распределяйте очки с умом",
                    GeneralPos.Teaches)
                });
                break;
            case StatusTutorial.ShowCombination:
                FrameShowTry.SetActive(false);
                FrameShowCombination.SetActive(true);
                GeneralPanel.StartShow(_AddStatusTutorial, new GeneralState[]{
                    new GeneralState("В нижней части находится поле комбинаций. Карточки одного типа образуют связи, которые дают дополнительный бонус.",
                    GeneralPos.Teaches)
                });
                break;
            case StatusTutorial.end:
                _EndTutorial();
                break;
        }
    }

    private void _EndTutorial()
    {
        if (isTutorial == false)
            return;
        FrameShowCombination.SetActive(false);
        isTutorial = false;
        _endTutorialEvent();
    }



    #endregion
}
