﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStarPerkProgressBar : MonoBehaviour {



    [SerializeField]
    private enum Type { Tower, Hero, Ability, Economy }
    [SerializeField]
    private Type type;

    [SerializeField]
    private Image[] StagesBar;
    [SerializeField]
    private Color LockColor, UnlockColor;

    [System.Serializable]
    public struct BarPropertiesIcons
    {
        public Image icon;
        public Image lockIcon;
    }
    [SerializeField]
    private BarPropertiesIcons[] PropertiesBar;
    [SerializeField]
    private Color LockPropertiesColor, ShowColor, HideColor;

    [SerializeField]
    private Sprite Lock, Unlock;


    public void UpdateData()
    {
        InitPerk();
        InitPropreties();
    }


    private void InitPerk()
    {
        int index = 0;
        for (int i = 0; i < 3; i++)
        {
            int count = StarsPerk.CountPerk(i + 1 + (int)Mathf.Clamp((int)type * 10 - 1,0,float.MaxValue));
            for (int j = 0; j < count; j++)
            {
                StagesBar[index].color = UnlockColor;
                index++;
            }
        }
        for (int i = index; i < StagesBar.Length; i++)
        {
            StagesBar[i].color = LockColor;
        }       
    }

    private void InitPropreties()
    {
        int count = StarsPerk.CountPerk((int)Mathf.Clamp((int)type * 10 - 1, 0, float.MaxValue)+1)+
            StarsPerk.CountPerk((int)Mathf.Clamp((int)type * 10-1, 0, float.MaxValue) + 2)+
            StarsPerk.CountPerk((int)Mathf.Clamp((int)type * 10-1, 0, float.MaxValue) + 3);

        for (int i = 0; i < PropertiesBar.Length; i++)
        {
            PropertiesBar[i].icon.color = (i + 1) * 3 > count ? LockPropertiesColor : ShowColor;
            
            if (StarsPerk.CountPerk(i + 4 + (int)Mathf.Clamp((int)type * 10 - 1, 0, float.MaxValue)) > 0)
            {
                PropertiesBar[i].lockIcon.sprite = Unlock;
                PropertiesBar[i].lockIcon.color = ShowColor;
            }
            else
            {
                PropertiesBar[i].lockIcon.color = HideColor;
            }
        }
    }
}
