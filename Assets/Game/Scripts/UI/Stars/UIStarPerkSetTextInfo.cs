﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStarPerkSetTextInfo : MonoBehaviour
{
    [SerializeField]
    private int ID;

    [SerializeField]
    private Text TextInfo;
	void Start ()
    {
        TextInfo.text = StarsPerk.GetTextInfo(ID);
	}
}
