﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStarPerkSelectionPanel : MonoBehaviour
{

    [System.Serializable]
    public class CardInfo
    {
        public int ID;
        public _CardRareness Rareness;
        public Image Fon;
        public Image Icon;
        public Image[] IconGroup;
        public Text[] TooltipsType;
        public Text Tooltip;
        public GameObject Star, StarRed;
        public Text Star_price_text, StarRed_price_text;
        public ParticleSystem particleEffect;
    }
    [SerializeField]
    private Color currentPriceColor, errorPriceColor;
    [SerializeField] private Color[] _colorRareness;

    [SerializeField]
    private CardInfo[] cardsInfo;
    [SerializeField]
    private string[] UrlCards;
    [SerializeField]
    private List<int> GroupUsual, GroupMagic, GroupRare, GroupLegendary;
    [SerializeField]
    private Sprite[] CardIcons,GroupIcons;

    [SerializeField]
    private Text timeText;
    [SerializeField]
    private GameObject WaitResponseObj;
    private bool waitRefreshData;

    public _CardRareness GetMaxRarenes()
    {
        if (cardsInfo[0].Rareness > cardsInfo[1].Rareness)
            return cardsInfo[0].Rareness;
        else
            return cardsInfo[1].Rareness;
    }

    public int GetIDCard(int index)
    {
        return cardsInfo[index].ID;
    }

    public void UpdateData()
    {
        for (int i = 0; i < 2; i++)
        {
            SetDataCard(cardsInfo[i], PlayerManager.IDsStarsCard[i]);
        }
    }
    private void SetDataCard(CardInfo cardInfo, int id)
    {
        int index = 0;
        cardInfo.Rareness = _CardRareness.Usual;
        if (GroupMagic.Contains(id))
        {
            index = 1;
            cardInfo.Rareness = _CardRareness.Magic;

            var main = cardInfo.particleEffect.main;
            main.startColor = _colorRareness[index - 1];

            cardInfo.particleEffect.Play();
        }
        else if (GroupRare.Contains(id))
        {
            index = 2;
            cardInfo.Rareness = _CardRareness.Rare;

            var main = cardInfo.particleEffect.main;
            main.startColor = _colorRareness[index - 1];
            cardInfo.particleEffect.Play();
        }
        else if (GroupLegendary.Contains(id))
        {
            index = 3;
            cardInfo.Rareness = _CardRareness.Legendary;

            var main = cardInfo.particleEffect.main;
            main.startColor = _colorRareness[index - 1];
            cardInfo.particleEffect.Play();
        }
        else
        { 
            cardInfo.particleEffect.Stop();
        }

        PlayerManager._instance.StartCoroutine(TextureDB.instance.load(UrlCards[index], (Sprite value)=> { cardInfo.Fon.sprite = value; }));

        cardInfo.ID = id;
        cardInfo.Icon.sprite = CardIcons[id];
        for (int i = 0; i < cardInfo.IconGroup.Length; i++)
        {
            cardInfo.IconGroup[i].sprite = GroupIcons[Mathf.FloorToInt(id / 10f)];
        }        
        cardInfo.Tooltip.text = string.Format("<color=#eacf22>{0}</color>\r\n{1}", StarsPerk.GetNameGroup(id), StarsPerk.GetTextInfo(id));

        int[] leftStar = UIStarPerk.StarsLeft;
        Debug.Log("Show left star " + leftStar[0].ToString());

        int price = StarsPerk.GetPriceStar(id);
        cardInfo.Star.SetActive(price > 0);
        cardInfo.Star_price_text.text = price.ToString();
        cardInfo.Star_price_text.color = price > leftStar[0] ? errorPriceColor : currentPriceColor;

        for (int i = 0; i < cardInfo.TooltipsType.Length;i++)
        {
            cardInfo.TooltipsType[i].text = StarsPerk.GetNameGroup(id);
        }

        price = StarsPerk.GetPriceRedStar(id);
        cardInfo.StarRed.SetActive(price > 0);
        cardInfo.StarRed_price_text.text = price.ToString();
        cardInfo.StarRed_price_text.color = price > leftStar[1] ? errorPriceColor : currentPriceColor;
    }

     public void SetTimer(string text)
     {
         timeText.text = text;
     }
}
