﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStarPerkCombinations : MonoBehaviour
{
    private List<int> perkShow = new List<int>() { -1, 1, 2, 3, -10, 10, 11, 12, -20, 20, 21, 22, -30, 30, 31, 32 };

    private List<int> TowerGrop = new List<int>() { 1, 2, 3 };
    private List<int> HeroGrop = new List<int>() { 10, 11, 12 };
    private List<int> AbilityGrop = new List<int>() { 20, 21, 22 };
    private List<int> EconomyGrop = new List<int>() { 30, 31, 32 };

    [SerializeField]
    private UIStarPerkCombinationsItem[] items;

    [SerializeField]
    private Sprite[] iconType, fonType;
    [SerializeField]
    private Color[] colorType;
    [SerializeField]
    private Color colorLinkDeff;

    [System.Serializable]
    public struct PanelInfo
    {
        public GameObject obj;
        public Text textCountLink;
        public BaseStarPerk.Group group;
        public int idLinkShow;
    }

    [Header("Full indo")]
    [SerializeField]
    private PanelInfo[] panelFullInfoAndLink;
    [SerializeField]
    private Text textButtonFullInfo;
    [SerializeField]
    private GameObject panelFullInfo;
    [SerializeField]
    private Transform panelFullInfoContent;
    [SerializeField]
    private GameObject prefabPropertiesInfo;



    [Header("Tooltip")]
    [SerializeField]
    private GameObject Tooltip;
    [SerializeField]
    private Text TextTooltip;
    [SerializeField]
    private Vector3 offsetTooltip;

    public void UpdateData()
    {
        int index = 0;
        int step = 0;
        int id;
        List<int> IdCombination = PlayerManager.GetPurchasedID.FindAll(i => perkShow.Contains(i));
        for (int i = 0; i < items.Length; i++)
        {
            if (i == 20 || i == 40)
                step++;
            index = i * 3 - step * 60 + step;
            if (index < IdCombination.Count)
            {
                id = IdCombination[index];
                items[i].SetFon(fonType[Mathf.FloorToInt(id / 10f)]);
                items[i].SetIcon(iconType[Mathf.FloorToInt(id / 10f)]);
                items[i].SetText(StarsPerk.GetTextInfo(id));
                items[i].ID = id;
            }
            else
                items[i].SetDeff();
        }

        for (int i = 0; i < items.Length; i++)
        {
            if (items[i].ID == 0)
            {
                for (int j = 0; j < items[i].getNeighbors.Length; j++)
                    items[i].SetColorLinks(j, colorLinkDeff, "");
                continue;
            }

            for (int j = 0; j < items[i].getNeighbors.Length; j++)
            {
                if (TowerGrop.Contains(items[i].ID) && TowerGrop.Contains(items[i].getNeighbors[j].ID))
                {
                    items[i].SetColorLinks(j, colorType[0], StarsPerk.GetTextInfo(-1));
                }
                else if (HeroGrop.Contains(items[i].ID) && HeroGrop.Contains(items[i].getNeighbors[j].ID))
                {
                    items[i].SetColorLinks(j, colorType[1], StarsPerk.GetTextInfo(-10));
                }
                else if (AbilityGrop.Contains(items[i].ID) && AbilityGrop.Contains(items[i].getNeighbors[j].ID))
                {
                    items[i].SetColorLinks(j, colorType[2], StarsPerk.GetTextInfo(-20));
                }
                else if (EconomyGrop.Contains(items[i].ID) && EconomyGrop.Contains(items[i].getNeighbors[j].ID))
                {
                    items[i].SetColorLinks(j, colorType[3], StarsPerk.GetTextInfo(-30));
                }
                else
                {
                    items[i].SetColorLinks(j, colorLinkDeff, "");
                }

            }
        }

        InitLinkPanel();
    }

    public void ShowTooltipLink(Transform pos)
    {
        ShowTooltip(pos.position, "Количество связей");
    }

    public void ShowTooltip(Vector3 pos, string text)
    {
        Tooltip.SetActive(true);
        TextTooltip.text = text;
        Tooltip.transform.position = pos + offsetTooltip;
    }
    public void HideTooltip()
    {
        Tooltip.SetActive(false);
    }

    void OnDisable()
    {
        HideFullInfo();
    }

    bool showFullInfo;
    public void ShowFullInfo()
    {

        showFullInfo = !showFullInfo;
        panelFullInfo.SetActive(showFullInfo);
        textButtonFullInfo.text = showFullInfo ? "X" : "?";
    }

    public void HideFullInfo()
    {
        if (showFullInfo)
            ShowFullInfo();
    }



    private void InitLinkPanel()
    {
        for (int i = 0; i < panelFullInfoAndLink.Length; i++)
        {
            switch(panelFullInfoAndLink[i].group)
            {
                case BaseStarPerk.Group.Tower:
                    panelFullInfoAndLink[i].obj.SetActive(StarsPerk.GetCountInGroupTower > 0);
                    break;
                case BaseStarPerk.Group.Hero:
                    panelFullInfoAndLink[i].obj.SetActive(StarsPerk.GetCountInGroupHero > 0);
                    break;
                case BaseStarPerk.Group.Ability:
                    panelFullInfoAndLink[i].obj.SetActive(StarsPerk.GetCountInGroupAbility > 0);
                    break;
                case BaseStarPerk.Group.Economy:
                    panelFullInfoAndLink[i].obj.SetActive(StarsPerk.GetCountInGroupEconomy > 0);
                    break;
            }
            panelFullInfoAndLink[i].textCountLink.text = StarsPerk.CountPerk(panelFullInfoAndLink[i].idLinkShow).ToString();

        }
    }

    private List<UIStarPerkCombinationsPropertiesInfoItem> Propertiesitems;
    public void InitFullInfo(int offset)
    {
        if (Propertiesitems == null)
            Propertiesitems = new List<UIStarPerkCombinationsPropertiesInfoItem>();

        int count;
        int index = 0;
        int id;
        for (int i = offset*4; i < offset*4 + 4; i++)
        {
            id = perkShow[i];
            count = StarsPerk.CountPerk(id);
            if (count > 0)
            {
                UIStarPerkCombinationsPropertiesInfoItem item;
                if(index<Propertiesitems.Count)
                {
                    item = Propertiesitems[index];
                }
                else
                {
                    item = GameObject.Instantiate(prefabPropertiesInfo, panelFullInfoContent).GetComponent<UIStarPerkCombinationsPropertiesInfoItem>();
                    Propertiesitems.Add(item);
                }
                
                item.icon.sprite=iconType[Mathf.FloorToInt(Mathf.Abs(id) / 10f)];
                item.text.text = string.Format("<color=#eacf22>{0}:</color> {1}", StarsPerk.GetNameAndLevel(id),  StarsPerk.GetTextInfo(id,count));
                index++;
            }
        }
        int countItem = Propertiesitems.Count;
        for (int i = index; i < countItem; i++)
        {
            GameObject.Destroy(Propertiesitems[index].gameObject);
            Propertiesitems.RemoveAt(index);
        }
    }

}
