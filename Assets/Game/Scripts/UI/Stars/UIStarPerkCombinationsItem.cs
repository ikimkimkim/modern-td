﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIStarPerkCombinationsItem : MonoBehaviour
{

    public int ID { get; set; }

    [SerializeField]
    private Image icon, fon;
    [SerializeField]
    private Sprite deffIcon, deffFon;
    [SerializeField]
    private UIStarPerkCombinationsItem[] Neighbors;
    public UIStarPerkCombinationsItem[] getNeighbors { get { return Neighbors; } }

    [SerializeField]
    private UIStarPerkCombinationsLine[] Links;

    [SerializeField]
    private UIStarPerkCombinations linkCombination;

    private string TextTooltip;

    public void SetDeff()
    {
        ID = 0;
        SetFon(deffFon);
        SetIcon(deffIcon);
        SetText("");
    }

    public void SetFon(Sprite sprite)
    {
        fon.sprite = sprite;
    }

    public void SetIcon(Sprite sprite)
    {
        icon.sprite = sprite;
    }
    public void SetText(string text)
    {
        TextTooltip = text;
    }

    public void SetColorLinks(int index, Color color, string text)
    {
        Links[index].SetInfo(color, text);
    }

    public void ShowTooltip()
    {
        if(TextTooltip.Length>0)
            linkCombination.ShowTooltip(transform.position + Vector3.up * 2, TextTooltip);
    }

    public void HideTooltip()
    {
        linkCombination.HideTooltip();
    }
}
