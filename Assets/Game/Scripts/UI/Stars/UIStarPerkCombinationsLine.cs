﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIStarPerkCombinationsLine : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler
{
    [SerializeField]
    private Image line;

    [SerializeField]
    private GameObject EffectShowTooltip;

    [SerializeField]
    private UIStarPerkCombinations linkCombination;

    private string TextTooltip;
    private bool Show = true;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (Show)
        {
            linkCombination.ShowTooltip(transform.position, TextTooltip);
            //Tooltip.SetActive(true);
            EffectShowTooltip.SetActive(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (Show)
        {
            linkCombination.HideTooltip();
            //Tooltip.SetActive(false);
            EffectShowTooltip.SetActive(false);
        }
    }

    public void SetInfo(Color color, string text)
    {
        line.color = color;
        TextTooltip = text;
        Show = text.Length > 0;
        EffectShowTooltip.SetActive(false);
    }
}
