﻿using System;
using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct ResPanel
{
    public GameObject Panel;
    public Image Icon;
    public Text Name, Count;
}

public class UICardsInventory : Singleton<UICardsInventory>
{

    public GameObject Panel;

    public GameObject CardPrefabs;

    public static event Action onShow, onHide;


    public UIGroupGameObject groupCard;
    public UIScrollRectHideObj scrollRectHide;
    private Transform parentCards;
    public GameObject LoadingCardWait;

    public ToggleGroup groupRareness;
    public Toggle[] toggleRareness;

    public ToggleGroup groupType;
    public Toggle[] toggleType;




    [Header("Resources")]
    public ResPanel[] panelRes;


    [Header("SelectedMode")]
    public GameObject buttonCloseObj;
    public GameObject buttonSelectedObj;
    public Button buttonSelected;

    [Header("Tutorial")]
    [SerializeField] private GameObject TutorialArrowOpenInventory;
    [SerializeField] private GameObject TutorialUpgradeCard;
    [SerializeField] private GameObject TutorialSaveCard;

    private struct CardObj
    {
        public CardInfoUI infoUI;
        public GameObject obj;
        public CardObj(CardInfoUI InfoUI, GameObject Obj)
        {
            infoUI = InfoUI;
            obj = Obj;
        }
    }

    protected override void InitializationSingleton()
    {
    }

    // Use this for initialization
    void Start()
    {
        parentCards = groupCard.transform;
        cardsObj = new List<CardObj>();
        bLockHide = false;
    }

    public void Show()
    {
        Show(false, true, true, true, true);
    }

    private void PlayerManager_InitializationСompleted()
    {
        UpdateData();
    }

    private bool isSelectedMode, _showUpgrade, _showTower, _showAbility, _showHero;
    public void Show(bool SelectedMode, bool showUpgrade, bool showTower, bool showAbility, bool showHero)
    {
        Panel.SetActive(true);
        isSelectedMode = SelectedMode;
        _showUpgrade = showUpgrade;
        _showTower = showTower;
        _showAbility = showAbility;
        _showHero = showHero;
        buttonCloseObj.SetActive(true);
        buttonSelectedObj.SetActive(isSelectedMode);
        buttonSelected.interactable = false;

        UpdateData();

        PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;
        if (onShow != null)
            onShow();
    }

    private Coroutine coroutine;
    private void UpdateData()
    {
        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = StartCoroutine(LoadCard());

        _AddStatusTutorial();
        LoadResources();
    }

    private void LoadResources()
    {
        var resDB = DataBase<TDTKItem>.Load(DataBase<TDTKItem>.NameBase.Resources);
        for (int i = 1; i < 5; i++)
        {
            var item = resDB.Find(j => j.ID == i);
            panelRes[i - 1].Icon.sprite = item.icon;
            panelRes[i - 1].Count.text = PlayerManager.GetCraftResources(i).ToString();
            panelRes[i - 1].Name.text = item.getFullDesp;
        }
    }

    public void OnShowMarket()
    {
        MarketResources.Show();
    }

    public void OnSelectedCard()
    {
        Panel.SetActive(false);

        PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;

        if (onHide != null)
            onHide();

        if (lastSelectedCard != null)
        {
            lastSelectedCard.OnSelect(false);
            lastSelectedCard = null;
        }
    }

    public void Hide()
    {
        if (bLockHide)
            return;

        Panel.SetActive(false);
        if (lastSelectedCard != null)
        {
            lastSelectedCard.OnSelect(false);
            lastSelectedCard = null;
        }
        PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;
        if (onHide != null)
            onHide();
    }

    private void DeleteCardObj()
    {
        for (int i = 0; i < cardsObj.Count; i++)
        {
            GameObject.Destroy(cardsObj[i].obj);
        }
        cardsObj.Clear();
    }

    public void ChangeToggleRareness(bool active)
    {
        if (active)
        {
            for (int i = 0; i < toggleRareness.Length; i++)
            {
                if (toggleRareness[i].isOn)
                {
                    SelectRarenes(i);
                    break;
                }
            }
        }
        else
        {
            if (groupRareness.AnyTogglesOn() == false)
                SelectRarenes(-1);
        }
    }

    public void SelectType(int index)
    {
        if (!Panel.activeSelf)
            return;
        _CardType newT = (_CardType)index;
        if (newT != lastType)
            ChanceFilter(newT, lastRareness);
    }

    public void SelectRarenes(int index)
    {
        if (!Panel.activeSelf)
            return;
        _CardRareness newR = (_CardRareness)index;
        if (newR == _CardRareness.Complete)
            newR = _CardRareness.Legendary;

        if (newR != lastRareness)
            ChanceFilter(lastType, newR);
    }


    private _CardType lastType;
    private _CardRareness lastRareness;
    public void ChanceFilter(_CardType type, _CardRareness rareness)
    {
        //Debug.LogFormat("ChanceFilter {0} {1}", type, rareness);
        lastType = type;
        lastRareness = rareness;
        for (int i = 0; i < cardsObj.Count; i++)
        {
            if (cardsObj[i].infoUI.linkCard == null)
            {
                cardsObj[i].obj.SetActive(false);
                continue;
            }

            if ((rareness == _CardRareness.none || cardsObj[i].infoUI.linkCard.rareness == rareness) &&
                (type == _CardType.none || cardsObj[i].infoUI.linkCard.Type == type))
            {
                cardsObj[i].obj.SetActive(true);
            }
            else
                cardsObj[i].obj.SetActive(false);
        }
        groupCard.UpdateGroup(true);
        scrollRectHide.ChangeCountObj(true);
    }

    private List<CardObj> cardsObj;
    private IEnumerator LoadCard()
    {
        int count = 0, countWait = 30;
        LoadingCardWait.SetActive(true);
        var cards = PlayerManager.CardsList;
        bool tower = false, ability = false, hero = false, upgrade = false;
        for (int i = 0; i < cards.Count; i++)
        {
            switch (cards[i].Type)
            {
                case _CardType.Tower:
                    tower = true;
                    break;
                case _CardType.Ability:
                    ability = true;
                    break;
                case _CardType.Hero:
                    hero = true;
                    break;
                case _CardType.Upgrade:
                    upgrade = true;
                    break;
            }
            toggleType[0].gameObject.SetActive(tower && _showTower);
            toggleType[1].gameObject.SetActive(ability && _showAbility);
            toggleType[2].gameObject.SetActive(hero && _showHero);
            toggleType[3].gameObject.SetActive(upgrade && _showUpgrade);

            if (i >= cardsObj.Count)
            {
                GameObject go = Instantiate(CardPrefabs, parentCards);
                go.transform.position = Vector3.one * -1000;
                go.transform.localScale = Vector3.one;
                go.transform.name += cards[i].unitName;
                var info = go.GetComponent<CardInfoUI>();
                if (isSelectedMode)
                    info.SetInfoTower(cards[i], ClickCard);
                else
                    info.SetInfoTower(cards[i], null);
                cardsObj.Add(new CardObj(info, go));

            }
            else
            {
                if (cardsObj[i].obj.activeSelf == false)
                    cardsObj[i].obj.SetActive(true);
                if (isSelectedMode)
                    cardsObj[i].infoUI.SetInfoTower(cards[i], ClickCard);
                else
                    cardsObj[i].infoUI.SetInfoTower(cards[i], null);
            }
            count++;
            if (count > countWait)
            {
                yield return null;
                count = 0;
            }
        }

        for (int i = cards.Count; i < cardsObj.Count; i++)
        {
            cardsObj[i].infoUI.DeletCardInfo();
            if (cardsObj[i].obj.activeSelf == false)
                cardsObj[i].obj.SetActive(false);
        }

        LoadingCardWait.SetActive(false);
        // groupCard.UpdateGroup(true);
        // scrollRectHide.ChangeCountObj(true);


        lastType = _CardType.none;
        for (int i = 0; i < toggleType.Length; i++)
        {
            if (toggleType[i].isOn && toggleType[i].gameObject.activeSelf)
            {
                groupType.NotifyToggleOn(toggleType[i]);
                lastType = (_CardType)i;
            }
        }

        if (lastType == _CardType.none)
        {
            for (int i = 0; i < toggleType.Length; i++)
            {
                if (toggleType[i].gameObject.activeSelf)
                {
                    //toggleType[i].Select();
                    toggleType[i].isOn = true;
                    groupType.NotifyToggleOn(toggleType[i]);
                    lastType = (_CardType)i;
                }
            }
        }

        if (groupRareness.AnyTogglesOn())
        {
            groupRareness.SetAllTogglesOff();
        }
        ChanceFilter(lastType, _CardRareness.none);

        yield break;
    }

    public Card cardSelect { get { if (lastSelectedCard == null) return null; else return lastSelectedCard.linkCard; } }
    private CardInfoUI lastSelectedCard;
    public void ClickCard(CardInfoUI cardInfoUI)
    {
        if (lastSelectedCard != cardInfoUI)
        {
            if (lastSelectedCard != null)
                lastSelectedCard.OnSelect(false);
            cardInfoUI.OnSelect(true);
            lastSelectedCard = cardInfoUI;
            buttonSelected.interactable = true;
        }

    }

    private bool bLockHide;

    public enum TutorialType { saleCard, gradeCard }
    private TutorialType _tutorialType;
    private Action _endTutorialEvent;
    private bool isTutorial = false;


    public static void StartTutorial(TutorialType type, Action endTutorial)
    {
        Debug.Log("_StartTutorial1");
        instance._StartTutorial(type, endTutorial);
    }

    private void _StartTutorial(TutorialType type, Action endTutorial)
    {
        Debug.Log("_StartTutorial2");
        isTutorial = true;
        bLockHide = true;
        _endTutorialEvent = endTutorial;
        _tutorialType = type;
        for (int i = 0; i < toggleRareness.Length; i++)
        {
            toggleRareness[i].interactable = false;
        }
        for (int i = 0; i < toggleType.Length; i++)
        {
            toggleType[i].interactable = false;
        }

        switch (_tutorialType)
        {
            case TutorialType.saleCard:
                _StartTutorSaveCard();
                break;
            case TutorialType.gradeCard:

                if (PlayerManager.CardsList.Exists(i => i.count >= i.next_level_count))
                    _StartTutorGradeCard();
                else
                    EndAllTutorial();
                break;
        }

    }

    private void _AddStatusTutorial()
    {
        if (isTutorial == false)
            return;
        switch(_tutorialType)
        {
            case TutorialType.saleCard:
                _statusTutorSave++;
                _UpdateStatusTitorSaveCard();
                break;
            case TutorialType.gradeCard:
                _statusTutorGrade++;
                _UpdateStatusTitorGradeCard();
                break;
        }
    }

    #region TutorSaveCard

    private enum TutorialSaleCard { openPanel, openSalePanel, end }
    [SerializeField] private TutorialSaleCard _statusTutorSave;

    private void _StartTutorSaveCard()
    {
        Debug.Log("_StartTutorSaveCard");
        _statusTutorSave = TutorialSaleCard.openPanel;
        _UpdateStatusTitorSaveCard();

    }

    private void _UpdateStatusTitorSaveCard()
    {
        Debug.Log(_statusTutorSave);
        switch(_statusTutorSave)
        {
            case TutorialSaleCard.openPanel:
                TutorialArrowOpenInventory.SetActive(true);
                break;
            case TutorialSaleCard.openSalePanel:
                TutorialArrowOpenInventory.SetActive(false);
                ShowTutorialSaveCard();
                CardSalePanel.StartTutorial(_AddStatusTutorial);
                break;
            case TutorialSaleCard.end:
                EndAllTutorial();
                break;
        }
    }

    public void ShowTutorialSaveCard()
    {
        for (int i = 0; i < cardsObj.Count; i++)
        {
            if (cardsObj[i].obj.activeSelf && cardsObj[i].infoUI.linkCard.count >1 && cardsObj[i].infoUI.linkCard.IDTowerData == 0)
            {
                if (TutorialSaveCard.activeSelf == false)
                    TutorialSaveCard.SetActive(true);
                TutorialSaveCard.GetComponent<StayInSameWorldPos>().Target = cardsObj[i].obj;
                return;
            }
        }
    }
    #endregion

    #region TuroeGradeCard
    private enum TutorialGradeCard { openPanel, openAllInfo, end }
    [SerializeField] private TutorialGradeCard _statusTutorGrade;

    private void _StartTutorGradeCard()
    {
        Debug.Log("_StartTutorGradeCard");
        _statusTutorGrade = TutorialGradeCard.openPanel;
        _UpdateStatusTitorSaveCard();

    }

    private void _UpdateStatusTitorGradeCard()
    {
        Debug.Log(_statusTutorGrade);
        switch (_statusTutorGrade)
        {
            case TutorialGradeCard.openPanel:
                TutorialArrowOpenInventory.SetActive(true);
                break;
            case TutorialGradeCard.openAllInfo:
                TutorialArrowOpenInventory.SetActive(false);
                ShowTutorialUpgradeCard();
                CardUIAllInfo.StartTutorial(_AddStatusTutorial);
                break;
            case TutorialGradeCard.end:
                EndAllTutorial();
                break;
        }
    }

    public void ShowTutorialUpgradeCard()
    {
        for (int i = 0; i < cardsObj.Count; i++)
        {
            if (cardsObj[i].infoUI.linkCard.Type == _CardType.Tower && cardsObj[i].infoUI.linkCard.count >= cardsObj[i].infoUI.linkCard.next_level_count)
            {
                if (TutorialUpgradeCard.activeSelf == false)
                    TutorialUpgradeCard.SetActive(true);
                TutorialUpgradeCard.GetComponent<StayInSameWorldPos>().Target = cardsObj[i].obj;
                bLockHide = true;
                return;
            }
        }
    }

    #endregion

    private void EndAllTutorial()
    {
        isTutorial = false;
        bLockHide = false;
        for (int i = 0; i < toggleRareness.Length; i++)
        {
            toggleRareness[i].interactable = true;
        }
        for (int i = 0; i < toggleType.Length; i++)
        {
            toggleType[i].interactable = true;
        }
        TutorialArrowOpenInventory.SetActive(false);
        TutorialSaveCard.SetActive(false);
        TutorialUpgradeCard.SetActive(false);
        _endTutorialEvent();
    }


}
