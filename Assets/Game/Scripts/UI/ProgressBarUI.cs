﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProgressBarUI : MonoBehaviour
{
    public Slider slider;
    public Image BarIcon;
    public Sprite BlueBar, GreenBar;
    public Text BarText;

    public IEnumerator SetProgressAmin(int value, int valueMax, string text, int ColorID)
    {
        float NowP = 0;
        StartCoroutine(ColorManager.ChangeColor(BarText, Color.yellow, Color.white));
        BarText.text = text;
        BarIcon.sprite = ColorID == 0 ? BlueBar : GreenBar;


        slider.maxValue = valueMax;
        slider.value = NowP;

        float max = Mathf.Clamp(value, value, valueMax);

        while ((Mathf.Abs(NowP - max) > 0.05f))
        {

            yield return null;
            NowP += valueMax * Time.unscaledDeltaTime;
            if (NowP > max)
                NowP = max;
            slider.value = NowP;

        }
        slider.value = value;


    }

    public void SetProgress(int value, int valueMax, string text, int ColorID)
    {
        BarText.text = text;
        slider.maxValue = valueMax;
        slider.value = value;
        BarIcon.sprite = ColorID == 0 ? BlueBar : GreenBar;
    }
}
