﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;
using UnityEngine.UI;

public class MarketResources : MonoBehaviour
{
    [System.Serializable]
    public struct ResToggle
    {
        public GameObject obj;
        public Text desp;
        public Image icon;
        public Toggle toggle;
    }

    [SerializeField]
    private GameObject panel;
    [SerializeField]
    private Slider sliderCounter;
    [SerializeField]
    private Text textCounterSale, textCounterBuy;
    [SerializeField]
    private Button buttonSale;


    [SerializeField]
    private ToggleGroup toggleGroupSaleRes, toggleGroupBuyRes;
    [SerializeField]
    private ResToggle[] togglesSaleRes, togglesBuyRes;

    public static MarketResources instance { get; private set; }

    private void Awake()
    {
        instance = this;
    }

    public static void Show()
    {
        instance._Show();
    }

    private void _Show()
    {
        panel.SetActive(true);
        Init();
    }

    private void Init()
    {
        var resDB = DataBase<TDTKItem>.Load(DataBase<TDTKItem>.NameBase.Resources);
        for (int i = 0; i < togglesSaleRes.Length; i++)
        {
            var item = resDB.Find(j => j.ID == (i + 2));
            togglesSaleRes[i].icon.sprite = item.icon;
            togglesSaleRes[i].desp.text = item.getFullDesp;
        }
        for (int i = 0; i < togglesBuyRes.Length; i++)
        {
            var item = resDB.Find(j => j.ID == (i + 1));
            togglesBuyRes[i].icon.sprite = item.icon;
            togglesBuyRes[i].desp.text = item.getFullDesp;
        }


        buttonSale.interactable = false;

        IndexSaleRes = -1;
        togglesSaleRes[0].toggle.isOn = true;
        toggleGroupSaleRes.NotifyToggleOn(togglesSaleRes[0].toggle);
        SelectedSaleRes(2);

    }
    [SerializeField]
    private int IndexSaleRes, IndexBuyRes;
    public void SelectedSaleRes(int index)
    {
        if (IndexSaleRes == index)
            return;

        if (toggleGroupSaleRes.AnyTogglesOn() == false)
            index = -1;
        else if (togglesSaleRes[index - 2].toggle.isOn == false)
            return;

        IndexSaleRes = index;
        UpdateBuyToggles();
        UpdateSliderCounter();
    }


    private void UpdateBuyToggles()
    {
        togglesBuyRes[0].toggle.isOn = true;
        toggleGroupBuyRes.NotifyToggleOn(togglesBuyRes[0].toggle);
        if (IndexSaleRes <= 0)
        {
            togglesBuyRes[0].toggle.interactable = false;
            for (int i = 1; i < togglesBuyRes.Length; i++)
            {
                togglesBuyRes[i].obj.SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < togglesBuyRes.Length; i++)
            {
                togglesBuyRes[i].toggle.interactable = (i + 1) < IndexSaleRes;
                togglesBuyRes[i].obj.SetActive((i + 1) < IndexSaleRes);
            }
        }
    }

    public void SelectedBuyRes(int index)
    {
        if (toggleGroupBuyRes.AnyTogglesOn() == false)
            index = -1;
        else if (togglesBuyRes[index - 1].toggle.isOn == false)
            return;

        IndexBuyRes = index;
        UpdateButtonInteractable();
        ChangeValueSliderCounter();
    }

    private void UpdateButtonInteractable()
    {
        buttonSale.interactable = (IndexBuyRes > 0 && sliderCounter.value > 0);
    }


    private void UpdateSliderCounter()
    {
        if (IndexSaleRes < 2)
        {
            sliderCounter.value = 0;
            sliderCounter.interactable = false;
        }
        else
        {
            sliderCounter.interactable = true;
            sliderCounter.maxValue = PlayerManager.GetCraftResources(IndexSaleRes);
            sliderCounter.value = Mathf.Floor(sliderCounter.maxValue / 2f);
        }
    }

    public void AddValueSliderCounter(int value)
    {
        sliderCounter.value += value;
    }

    public void ChangeValueSliderCounter()
    {
        textCounterSale.text = sliderCounter.value.ToString();
        if (IndexBuyRes > 0 && IndexSaleRes > 0)
            textCounterBuy.text = (sliderCounter.value * Mathf.Pow(2, IndexSaleRes - IndexBuyRes)).ToString();
        else
            textCounterBuy.text = "-";

        UpdateButtonInteractable();
    }

    public void _Hide()
    {
        panel.SetActive(false);
    }

    public void OnSale()
    {
        PlayerManager.DM.SaleResources(IndexSaleRes, IndexBuyRes, Mathf.FloorToInt(sliderCounter.value), Result);
    }

    private void Result(ResponseData data)
    {
        if (data.ok == 0)
        {
            Debug.LogError("Sale Resources Error: " + data.error.code + " -> " + data.error.text);
            TooltipMessageServer.Show(data.error.text);
        }
        else
        {
            TooltipMessageServer.Show("<color=#19c62e>Успешно</color>", 1);            
        }
        PlayerManager.SetPlayerData(data.user);
        Init();
    }

}
