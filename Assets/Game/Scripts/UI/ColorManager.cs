﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColorManager : MonoBehaviour {

    public static IEnumerator ChangeColor(Text colorObj, Color color1, Color color2)
    {
        for (int i = 0; i < 3; i++)
        {
            colorObj.color = color1;
            yield return new WaitForSecondsRealtime(0.1f);// colorObj.StartCoroutine(TimeScaleManager.WaitForRealSeconds(0.1f));
            colorObj.color = color2;
            yield return new WaitForSecondsRealtime(0.1f);// colorObj.StartCoroutine(TimeScaleManager.WaitForRealSeconds(0.1f));
        }
    }

    public static IEnumerator ChangeColor(Color colorObj, Color color1, Color color2)
    {
        for (int i = 0; i < 3; i++)
        {
            colorObj = color1;
            yield return new WaitForSeconds(0.1f);
            colorObj = color2;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
