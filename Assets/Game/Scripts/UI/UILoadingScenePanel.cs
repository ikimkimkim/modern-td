﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILoadingScenePanel : MonoBehaviour {

    public GameObject panelObj;

    public string urlFon;
    public Image Fon, IconLevel;
    public Text textAdvice;
    public Text textProgress;
    public Slider PorgessBarSlider;

    public string[] ProgessInfo;
    public string[] Advices;

    private bool isLoading = false;

    float progress;
    private List<MapDB.MapData> mapData;

    void Start ()
    {
        mapData = MapDB.Load();
        BundleSceneLoading_completeLoading();
        BundleSceneLoading.startLoading += BundleSceneLoading_startLoading;
        //BundleSceneLoading.completeLoading += BundleSceneLoading_completeLoading;
        StartCoroutine(TextureDB.instance.load(urlFon, (Sprite value) => { Fon.sprite = value; }));
    }

    private void BundleSceneLoading_startLoading()
    {
        isLoading = true;
        panelObj.SetActive(true);

        textAdvice.text = Advices[Random.Range(0, Advices.Length)];
        int idLvl = CardAndLevelSelectPanel.IDLevel;
        if (idLvl < 0)
        {
            IconLevel.sprite = TopManager.getIconMap(CardAndLevelSelectPanel.IDMap, CardAndLevelSelectPanel.mapType);
            StartCoroutine(TextureDB.instance.load(TopManager.getUrlIconMap(CardAndLevelSelectPanel.IDMap, CardAndLevelSelectPanel.mapType), (Sprite value) => { IconLevel.sprite = value; }));
        }
        else
        {
            var map = mapData.Find(i => i.LevelID == idLvl);
            IconLevel.sprite = map.Icon;
            StartCoroutine(TextureDB.instance.load(map.getUrlIcon, (Sprite value) => { IconLevel.sprite = value; }));
        }
    }

    private void BundleSceneLoading_completeLoading()
    {
        isLoading = false;
        if(panelObj.activeSelf)
            panelObj.SetActive(false);
        if (textProgress != null)
            textProgress.text = "";
    }

    private void Update()
    {
        if(isLoading && textProgress!=null)
        {
            progress = Mathf.Round(BundleSceneLoading.Progress * 100f);
            if (progress<20f)
            {
                textProgress.text = ProgessInfo[0];
            }
            else if(progress<40f)
            {
                textProgress.text = ProgessInfo[1];
            }
            else if(progress<60f)
            {
                textProgress.text = ProgessInfo[2];
            }
            else if(progress<80f)
            {
                textProgress.text = ProgessInfo[3];
            }
            else
            {
                textProgress.text = ProgessInfo[4];
            }

            PorgessBarSlider.value = BundleSceneLoading.Progress;
        }
    }

    private void OnDestroy()
    {
        BundleSceneLoading.startLoading -= BundleSceneLoading_startLoading;
        //BundleSceneLoading.completeLoading -= BundleSceneLoading_completeLoading;
    }
}