﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMiniPanelSelectedCard : MonoBehaviour
{
    public delegate void EndSelectedHandler(Card card);
    public static UIMiniPanelSelectedCard instance { get; private set; }

    [SerializeField]
    private GameObject panel;
    [SerializeField]
    private Text textNotCard, textInfo;
    [SerializeField]
    private GameObject prefabCard;
    [SerializeField]
    private Transform placeCard;
    [SerializeField]
    private Button buttonSelected;
    [SerializeField]
    private List<CardInfoUI> cardsInfoUI;

    private CardInfoUI cardSelected;
    private EndSelectedHandler selectedHandler;


    public static void Show(List<Card> cards,  EndSelectedHandler endSelected)
    {
        if (instance != null)
            instance._Show(cards, endSelected);
        else
            Debug.LogError("ERROR: Show MiniPanelSelectedCard instance is null!");
    }

    private void _Show(List<Card> cards, EndSelectedHandler endSelected)
    {
        selectedHandler = endSelected;
        panel.SetActive(true);
        buttonSelected.interactable = false;
        textNotCard.text = "У вас нет подходящих карт.\r\nДля начала создайте их в Мастерской \"Сборка\".";
        if (cards == null || cards.Count == 0)
        {
            textInfo.text = "";
            textNotCard.gameObject.SetActive(true);
            for (int i = 0; i < cardsInfoUI.Count; i++)
            {
                cardsInfoUI[i].gameObject.SetActive(false);
            }
        }
        else
        {
            textInfo.text = "Выберите карту";
            textNotCard.gameObject.SetActive(false);
            for (int i = 0; i < cards.Count; i++)
            {
                if (cardsInfoUI.Count <= i)
                    CreadCard();
                if (cardsInfoUI[i].gameObject.activeSelf == false)
                    cardsInfoUI[i].gameObject.SetActive(true);
                cardsInfoUI[i].SetInfoTower(cards[i], Selected);
            }

            for (int i = cards.Count; i < cardsInfoUI.Count; i++)
            {
                cardsInfoUI[i].gameObject.SetActive(false);
            }
        }
    }

    private void Awake()
    {
        instance = this;
    }

    private void CreadCard()
    {
        GameObject go = Instantiate(prefabCard, placeCard);
        var cardInfo = go.GetComponent<CardInfoUI>();
        cardsInfoUI.Add(cardInfo);
    }

    private void Selected(CardInfoUI card)
    {
        if(cardSelected!=card)
        {
            if (cardSelected != null)
                cardSelected.OnSelect(false);
            cardSelected = card;
            cardSelected.OnSelect(true);
            textInfo.text = card.linkCard.ShortDesp;
            buttonSelected.interactable = true;
        }
    }

    public void EndSelected()
    {
        panel.SetActive(false);
        if (selectedHandler != null)
            selectedHandler(cardSelected.linkCard);

        if (cardSelected != null)
            cardSelected.OnSelect(false);
        cardSelected = null;
    }

    public void Hide()
    {
        panel.SetActive(false);
        if (selectedHandler != null)
            selectedHandler(null);

        if (cardSelected != null)
            cardSelected.OnSelect(false);
        cardSelected = null;
    }

    private void OnDestroy()
    {
        instance = null;
    }

}
