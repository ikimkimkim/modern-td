﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TDTK;
using System;

public class TimeGame : MonoBehaviour {

    [SerializeField] private Text TimeText;
    private TimeSpan timeSpanWaitBoss;

    private void Start ()
    {
        if (CardAndLevelSelectPanel.BigPortal)
        {
            timeSpanWaitBoss = new TimeSpan(0, 0, GameControl.GetTimeWaitBoss);
        }
        else
        {
            gameObject.SetActive(false);
            return;
        }
	
	}



    private void Update ()
    {
        if(CardAndLevelSelectPanel.BigPortal)
        {
            if(GameControl.GetTimeGame <= GameControl.GetTimeWaitBoss)
                TimeText.text = (timeSpanWaitBoss - new TimeSpan(0, 0, (int)GameControl.GetTimeGame)).ToString();
            else
                TimeText.text = (new TimeSpan(0, 0, (int)GameControl.GetTimeGame) - timeSpanWaitBoss).ToString();
        }
        else
        {
            TimeText.text = new TimeSpan(0, 0, (int)GameControl.GetTimeGame).ToString();
        }
	}
}
