﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SkillPanelInCardAllInfo : MonoBehaviour {

   // public const int LevelRelease = 5;

    public Sprite[] IconsLevel;

    public Color ReleaseColor, NotReleaseColor;

    public Color IconColor, NotIconColor;

    [System.Serializable]
    public struct SkillPanel
    {
        public GameObject panel;
        public Text levelText;
        public Skill[] skills;
    }

    [System.Serializable]
    public struct Skill
    {
        public Text TextProperties;
        public Image Fon;
        public Image Icon;
        public Image IconLevel;
        public GameObject Check;
    }

    public List<SkillPanel> SkillPanels;

    public Sprite spriteActive, spriteDeactive, spriteLock;

    public void SetData(Card card, List<CardProperties> properties)
    {
        int[] counters = new int[5];
        for (int i = 1; i < 5; i++)
        {
            counters[i] = 0;
            if (SkillPanels.Count>i)
            {
                foreach (Skill s in SkillPanels[i].skills)
                {
                    s.Icon.sprite = spriteLock;
                    s.Icon.color = NotIconColor;

                    if (i - 1 > card.getIntRareness)
                        s.TextProperties.text = "Доступны у более редких карт";
                    else if (OpenPropertiesStarPerk.isOpenLevel(card.Type, i + 1) == false)
                        s.TextProperties.text = "Заблокировано.\r\nИзучите умение в разделе \"Технологии\"";
                    else
                        s.TextProperties.text = "error skill panel";

                    s.Check.SetActive(false);
                    s.IconLevel.gameObject.SetActive(false);
                    s.Fon.sprite = spriteDeactive;
                    s.Fon.color = i - 1 <= card.getIntRareness && OpenPropertiesStarPerk.isOpenLevel(card.Type, i + 1) ? 
                        ReleaseColor : NotReleaseColor;
                }
                //if(SkillPanels[i].panel!=null && SkillPanels[i].panel.activeSelf)
                //    SkillPanels[i].panel.SetActive(false);
            }
        }
        
        RequirementsProperties rp;
        int level;
        bool isActive = false;
        for (int i = 0; i < properties.Count; i++)
        {
            rp = properties[i].Requirements.Find(f => f.ID == 5);
            if (rp != null)
            {
                isActive = true;
                level = (int)rp.Param[0];
                //if (/*level <= LevelRelease &&*/ OpenPropertiesStarPerk.isOpenLevel(card.Type, level))
                {
                    SkillSet(card, level, counters[level - 1], SkillPanels[level - 1], properties[i]);
                    counters[level - 1]++;
                }
            }
        }

        gameObject.SetActive(isActive);
    }


    public void SkillSet(Card card, int level,int index, SkillPanel panel,CardProperties properties)
    {
        //Debug.Log(string.Format("Skill set:{0},{1} prop:{2}",level,index,properties.getInfoText()));
        if (index>=panel.skills.Length)
        {
            Debug.LogError("SPICAI -> Card show skill out of range skillpanel!");
            return;
        }

        if (panel.panel.activeSelf == false)
            panel.panel.SetActive(true);

        StartCoroutine(TextureDB.instance.load(properties.UrlIcon, (Sprite value) => 
        {
            panel.skills[index].Icon.color = /*level <= LevelRelease &&*/ OpenPropertiesStarPerk.isOpenLevel(card.Type, level) ? ReleaseColor : NotReleaseColor;
            panel.skills[index].Icon.sprite = value;
        }));

        panel.skills[index].TextProperties.text = OpenPropertiesStarPerk.isOpenLevel(card.Type, level) ? properties.getFullInfoText() :
            string.Format("<color=#d13232>{0}</color>\r\n{1}", "Заблокировано.\r\nИзучите умение в разделе \"Технологии\"", properties.getFullInfoText());


        panel.skills[index].IconLevel.gameObject.SetActive(properties.Level>0);
        panel.skills[index].IconLevel.sprite = IconsLevel[properties.Level];

        if (properties.CanActive(card))
        {
            panel.skills[index].Check.SetActive(true);
            panel.skills[index].Fon.sprite = spriteActive;
        }
        else
        {
            panel.skills[index].Check.SetActive(false);
            panel.skills[index].Fon.sprite = spriteDeactive;
        }

    }
}
