﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using TDTK;

public class PortalProgressBarManager : MonoBehaviour
{
    //private RectTransform thisRT;
    public RectTransform LineTimeProgressBar, LinePortalProgressBar;
    public RectTransform TimeProgressBar, PortalProgressBar;

    public Text textInfo;

    public GameObject ObjBonus, ObjWive;

    public float p1, p2;
    float ScoreDeadUnityLimit;
    float TimeWaitBoss;

    public RectTransform IconTime, IconCreep;

    void Start ()
    {
        if (GameControl.GetGameType != _GameType.TimeBoss)
        {
            gameObject.SetActive(false);
            return;
        }

        ObjWive.SetActive(false);

        /*if (CardAndLevelSelectPanel.BigPortal ==true)
        {*/
            textInfo.text = "Убейте всех нападающих";
            ObjBonus.SetActive(true);
        /*}
        else
        {
            textInfo.text = "Убейте всех нападающих";
            ObjBonus.SetActive(false);
        }*/

        //thisRT = GetComponent<RectTransform>();

        TimeWaitBoss = (float)new TimeSpan(0, 0, GameControl.GetTimeWaitBoss).TotalSeconds;
        ScoreDeadUnityLimit = GameControl.GetScoreDeadUnityLimit;
        /*if (CardAndLevelSelectPanel.BigPortal==false)
        {
            IconTime.gameObject.SetActive(false);
            TimeProgressBar.gameObject.SetActive(false);
        }*/
        UnitCreep_onDestroyedE(null);
        UnitCreep.onDestroyedE += UnitCreep_onDestroyedE;
    }

    private void UnitCreep_onDestroyedE(Unit unit)
    {
        if (IconCreep.gameObject.activeSelf)
        {
            p2 = GameControl.GetScoreDeadUnitCount / ScoreDeadUnityLimit;
            IconCreep.anchoredPosition = new Vector2(2.5f + PortalProgressBar.sizeDelta.x * p2, 28f);
            LinePortalProgressBar.sizeDelta = new Vector2(PortalProgressBar.sizeDelta.x * p2, 30);
            if (p2 > 1)            
                IconCreep.gameObject.SetActive(false);            
        }
    }

    private void UpdateTimeProgressBar()
    {
        if (IconTime.gameObject.activeSelf)
        {
            p1 = GameControl.GetTimeGame / TimeWaitBoss;
            IconTime.anchoredPosition = new Vector2(2.5f + TimeProgressBar.sizeDelta.x * p1, -16f);
            LineTimeProgressBar.sizeDelta = new Vector2(TimeProgressBar.sizeDelta.x * p1, 30);
            if (p1 > 1)
                IconTime.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTimeProgressBar();
    }

    void OnDestroy()
    {
        UnitCreep.onDestroyedE -= UnitCreep_onDestroyedE;
    }
}
