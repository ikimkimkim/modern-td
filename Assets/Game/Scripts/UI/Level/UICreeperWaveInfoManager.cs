﻿using System;
using System.Collections;
using System.Collections.Generic;
using TDTK;
using Translation;
using UnityEngine;
using UnityEngine.UI;

public class UICreeperWaveInfoManager : MonoBehaviour
{
    [Serializable]
    public struct ButtonCreep
    {
        public GameObject rootObj;
        public Image iconImage;
        public Text nameText;
    }

    public float timeHideInfo = 9f;
    public NewUnitPopup unitPopup;
    public Transform parentPopup;

    public static UICreeperWaveInfoManager instance { get; private set; }

    private static Coroutine _coroutineShow;
    
    private Translator trans;

    private List<int> IDCreepAdded;
    private List<Unit> creepList;//, bossList;

    public Text textInfo;
    public ButtonCreep[] imagesCreep;

    private void Awake()
    {
        instance = this;
        IDCreepAdded = new List<int>();
        creepList = new List<Unit>();
        //bossList = new List<Unit>();
    }

    public static void ShowInfoWave(Wave wave, bool startInfo=false)
    {
        if (instance != null)
        {
            if (_coroutineShow != null)
                instance.StopCoroutine(_coroutineShow);
            _coroutineShow = instance.StartCoroutine(instance._ShowInfoWave(wave, startInfo));
        }
        else
            Debug.Log("UICreeperWaveInfoManager is null!");
    }

    public IEnumerator _ShowInfoWave(Wave wave, bool startInfo)
    {
        UIHUD.SetActiveButtonSpawnNextWave(false);

        trans = TranslationEngine.Instance.Trans;

        if (startInfo == false)
        {
            textInfo.text = "В текущей волне:";
            yield return new WaitForSeconds(timeHideInfo);
            textInfo.text = "";
            for (int i = 0; i < imagesCreep.Length; i++)
            {
                if (imagesCreep[i].rootObj.activeSelf)
                    imagesCreep[i].rootObj.SetActive(false);
            }
            yield return new WaitForSeconds(1);
        }
        else
            textInfo.text = "";

        if (wave != null)
        {
            IDCreepAdded.Clear();
            creepList.Clear();
            UnitCreep creep;
            for (int i = 0; i < wave.subWaveList.Count; i++)
            {
                creep = wave.subWaveList[i].unit.GetComponent<UnitCreep>();
                if (IDCreepAdded.Contains(creep.prefabID) == false)
                {
                    IDCreepAdded.Add(creep.prefabID);
                    creepList.Add(creep);
                }
            }

            if(IDCreepAdded.Count>imagesCreep.Length)
            {
                Debug.LogError("UICWIM-> Unique creeps is more of icons!");
                yield break;
            }


            for (int i = 0; i < creepList.Count; i++)
            {
                if (imagesCreep[i].rootObj.activeSelf == false)
                    imagesCreep[i].rootObj.SetActive(true);
                imagesCreep[i].iconImage.sprite = creepList[i].iconSprite;
                imagesCreep[i].nameText.text = trans[creepList[i].unitName];
            }

            for (int i =creepList.Count; i < imagesCreep.Length; i++)
            {
                if (imagesCreep[i].rootObj.activeSelf)
                    imagesCreep[i].rootObj.SetActive(false);
            }

            textInfo.text = "В следующей волне:";

            if (startInfo == false && SpawnManager.isAllowSkip)
                UIHUD.SetActiveButtonSpawnNextWave(true);
        }
    }

    public void OnShowFullInfoCreep(int id)
    {
        unitPopup.Show(creepList[id] as UnitCreep, parentPopup);
    }

    public static void Hide()
    {
        if (instance == null)
            return;
        instance._Hide();
    }

    private void _Hide()
    {
        if (_coroutineShow != null)
            instance.StopCoroutine(_coroutineShow);

        UIHUD.SetActiveButtonSpawnNextWave(false);

        for (int i = 0; i < imagesCreep.Length; i++)
        {
            if (imagesCreep[i].rootObj.activeSelf)
                imagesCreep[i].rootObj.SetActive(false);
        }

        IDCreepAdded.Clear();
        creepList.Clear();
    }

    private void OnDestroy()
    {
        creepList.Clear();
    }
}
