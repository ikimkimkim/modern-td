﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class HelpResponse
{
    public int result;
    public PlayerData user;
    public HelpResponse()
    { }
}

public class HelpFriendsPanel : UIEntity
{
    ServerManager _server;
    public static HelpFriendsPanel instance { get; private set; }

    public override int entityID
    {
        get
        {
            return 4;
        }
    }

    public override int priorityShow
    {
        get
        {
            return 400;
        }
    }

    public GameObject Screen, Background;
    public ScrollFriends scrollFriends;
    public GameObject ContentPanelFriends;
    public GameObject PrefabFrameFriend;
    public Toggle CheckBoxAll;
    public GameObject TextNotFriend;
    public Button ButtonSendHelp;
    public List<FrameFriend> ListFrameFriend;
    


    public GameObject BackgroundGetHelp;
    public ScrollFriends scrollGetHelpFriends;
    public GameObject ContentPanelGetHelpFriends;
    public GameObject TextNotHelp;
    public GameObject TooltipError;
    public Text TooltipErrorText;
    public float TimeShowTootltipError;
    public GameObject PrefabRowFriend;
    public List<RowFriend> ListRowFriend;
    FriendsData HelpMeFriends;

   // public GameObject ButtonGetHelpFriends;
    public GameObject GOCountHelpMe;
    

    public static Action ActionHideSendPanel;

    public void ShowPanel()
    {
        Screen.SetActive(true);
        Background.SetActive(true);

        foreach (FrameFriend ff in ListFrameFriend)
        {
            GameObject.Destroy(ff.gameObject);
        }
        ListFrameFriend.Clear();

        int i = 0;
        //float ost = 0;

        GameObject f;
        if (PlayerManager._instance._friendsData.friends != null)
        {
            foreach (Friend friend in PlayerManager._instance._friendsData.friends)
            {
                //ost = Mathf.Floor(i / 2f);
                f = GameObject.Instantiate(PrefabFrameFriend, ContentPanelFriends.transform);
                /*if (i % 2 == 0)
                {
                    f.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(100 * ost, 50, 0);
                }
                else
                {
                    f.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(100 * ost, -25, 0);
                }*/
                f.GetComponent<FrameFriend>().SetFriendInfo(friend);
                ListFrameFriend.Add(f.GetComponent<FrameFriend>());
                i++;
            }
        }
        TextNotFriend.SetActive(i == 0);
        CheckBoxAll.gameObject.SetActive(i != 0);
        ButtonSendHelp.interactable = i != 0;

        //scrollFriends.SetSizeScroll(Mathf.CeilToInt(i /2f));
        CheckBoxAll.isOn = true;
        SelectAll();
    }

    public void ChangeSelect(bool b)
    {
        int i = 0;
        foreach (FrameFriend ff in ListFrameFriend)
        {
            if (ff.Checkbox.isOn)
                i++;
        }
        ButtonSendHelp.interactable = i > 0;
    }

    public void SelectAll()
    {
        foreach (FrameFriend ff in ListFrameFriend)
        {
           ff.Checkbox.isOn = CheckBoxAll.isOn;
        }
    }

    public void HidePanel(int status)
    {
        Screen.SetActive(false);
        Background.SetActive(false);

        if (ActionHideSendPanel != null)
            ActionHideSendPanel();

        if (status == 0)
        {
            Debug.Log("Send help 0");
            //List<string> ids = new List<string>();//1 Не где не используется дальше

            List<StringPair> extra = new List<StringPair>();
            //string json = StringSerializationAPI.Serialize(ids.GetType(), ids);//2 Не где не используется дальше
            extra.Add(new StringPair("ids", "[]"));
            StartCoroutine(_server.SaveLoadServer("sendHelp", extra, "", value => CallBackSendHelp(value)));
        }
    }

    public void HidePanelGetHelp()
    {
        Screen.SetActive(false);
        BackgroundGetHelp.SetActive(false);

        GOCountHelpMe.SetActive(HelpMeFriends.friends.Length > 0);
        GOCountHelpMe.GetComponentInChildren<Text>().text = HelpMeFriends.friends.Length.ToString();
        //StartCoroutine(_server.SaveLoadServer("helpedFriends", new List<StringPair>(), "", value => SetHelpMeFriends(value)));
    }

    void Start ()
    {
        ListFrameFriend = new List<FrameFriend>();
        ListRowFriend = new List<RowFriend>();

        var playerInfo = GameObject.FindGameObjectWithTag("PlayerInfo");
        _server = playerInfo.GetComponent<ServerManager>();        


        if(PlayerManager._instance._playerData.available_help_friends==1)
        {
            StartCoroutine(_server.SaveLoadServer("helpedFriends", new List<StringPair>(), "", value => SetHelpMeFriends(value, 0)));

            /*if (SocialManager.Instance != null)
            {
                SocialManager.Instance.actionLoadFriends += loadFriends;
                SocialManager.Instance.LoadFriendsPlayer();
            }*/
               
        }
        /*else if (PlayerManager._instance._playerData.daily_bonus == 0)
        {
            StartCoroutine(_server.SaveLoadServer("helpedFriends", new List<StringPair>(), "", value => SetHelpMeFriends(value, 1)));
        }*/
        else
            StartCoroutine(_server.SaveLoadServer("helpedFriends", new List<StringPair>(), "", value => SetHelpMeFriends(value, 0)));

        instance = this;
#if UNITY_EDITOR
        //Для тестирование.
        HelpMeFriends = new FriendsData();
        HelpMeFriends.friends = new Friend[15];
        for (int i = 0; i < 15; i++)
        {
            HelpMeFriends.friends[i] = new Friend();
            HelpMeFriends.friends[i].id = "1111" + i;
            HelpMeFriends.friends[i].name = "Name" + i;
            HelpMeFriends.friends[i].ava = @"http://cs10114.vk.me/u203654/e_8cf93725.jpg";
        }
#endif

    }

   /* public void ButtonShowPanel()
    {
        if (SocialManager.Instance != null)
            SocialManager.Instance.LoadFriendsPlayer();
        SocialManager.Instance.actionLoadFriends += loadFriends;
    }*/


    IEnumerator HideTooltipError()
    {
        yield return new WaitForSeconds(TimeShowTootltipError);
        TooltipError.SetActive(false);
    }

    public void ShowTooltipError(string message,RectTransform position)
    {
        TooltipErrorText.text = message;
        TooltipError.SetActive(true);
        TooltipError.GetComponent<RectTransform>().anchoredPosition = new Vector2(180,150+position.anchoredPosition.y);
        StartCoroutine(HideTooltipError());
    }
    
    public void loadFriends()
    {
        ShowPanel();
        SocialManager.Instance.actionLoadFriends -= loadFriends;
    }

    public void SetHelpMeFriendsDM(FriendsData HelpFriends, int status)
    {
        HelpMeFriends = HelpFriends;
        GOCountHelpMe.SetActive(HelpMeFriends.friends.Length > 0);
        GOCountHelpMe.GetComponentInChildren<Text>().text = HelpMeFriends.friends.Length.ToString();

        if (status == 1 && HelpMeFriends.can_take_help == 1 && HelpMeFriends.friends.Length > 0)
            ShowGetHelpPanel();

        if (status == 2)
            ShowGetHelpPanel();
    }

    public void SetHelpMeFriends(string value,int status)
    {
        Debug.Log("HelpMeFriends:" + value);
        HelpMeFriends =(FriendsData)StringSerializationAPI.Deserialize(typeof(FriendsData), value);
        GOCountHelpMe.SetActive(HelpMeFriends.friends.Length > 0);
        GOCountHelpMe.GetComponentInChildren<Text>().text = HelpMeFriends.friends.Length.ToString();

        //ButtonGetHelpFriends.SetActive(HelpMeFriends.can_take_help == 1);

        if (status == 1 && HelpMeFriends.can_take_help==1 && HelpMeFriends.friends.Length>0)
            ShowGetHelpPanel();

        if(status==2)
            ShowGetHelpPanel();
    }

    public void SendHelp()
    {
        List<string> ids = new List<string>();
        foreach (FrameFriend ff in ListFrameFriend)
        {
            if (ff.Checkbox.isOn)
                ids.Add(ff.friend.id);
        }
        List<StringPair> extra = new List<StringPair>();
        string json = StringSerializationAPI.Serialize(ids.GetType(), ids);
        Debug.Log("Send help 1:"+ json);
        extra.Add(new StringPair("ids", json));
        StartCoroutine(_server.SaveLoadServer("sendHelp", extra, "", value => CallBackSendHelp(value)));
    }

    void CallBackSendHelp(string result)
    {
        Debug.Log("HFP-> CBSH");
        HelpResponse hr = (HelpResponse)StringSerializationAPI.Deserialize(typeof(HelpResponse), result);
        Debug.Log("HFP-> CBSH result:"+hr.result);
        PlayerManager.SetPlayerData(hr.user);
        HidePanel(1);
    }

    public void ButtonShowGetHelpPanel()
    {
        PlayerManager.DM.HelpedFriends(SetHelpMeFriendsDM,2);
        //StartCoroutine(_server.SaveLoadServer("helpedFriends", new List<StringPair>(), "", value => SetHelpMeFriends(value, 2)));
    }

    private void ShowGetHelpPanel()
    {
        Screen.SetActive(true);
        BackgroundGetHelp.SetActive(true);
        foreach (RowFriend ff in ListRowFriend)
        {
            GameObject.Destroy(ff.gameObject);
        }
        ListRowFriend.Clear();

        int i = 0;
        GameObject f;
        foreach (Friend friend in HelpMeFriends.friends)
        {
            f = GameObject.Instantiate(PrefabRowFriend, ContentPanelGetHelpFriends.transform);
            f.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            f.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0,- 55 * i, 0);

            f.GetComponent<RowFriend>().SetFriendInfo(_server,friend,this);
            ListRowFriend.Add(f.GetComponent<RowFriend>());

            i++;
        }

        scrollGetHelpFriends.SetSizeScroll(i);
        TextNotHelp.SetActive(i == 0);
    }

    void Update()
    {
        if (CardUIAllInfo.isShow == false)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (Background.activeSelf == true)                    
                    HidePanel(0);
                if (BackgroundGetHelp.activeSelf == true)
                    HidePanelGetHelp();
            }
        }
    }

    public override bool NeedShow()
    {
        if(PlayerManager._instance._playerData.available_help_friends == 1)
        {
            ShowPanel();
            return true;
        }
        return false;
    }
}
