﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FrameFriend : MonoBehaviour {

    public Image Avatar;
    public Text Name;
    public GameObject PanelName;
    public Toggle Checkbox;
    public Friend friend;

    private Coroutine AvatarCor;

    public void SetFriendInfo(Friend f)
    {
        friend = f;
        if (Name != null)
            Name.text = f.name;

        Checkbox.onValueChanged.AddListener(HelpFriendsPanel.instance.ChangeSelect);
        if(friend!=null && friend.ava!=null)
        SocialManager.Instance.DownLoadImage(friend.ava, value => setAvatar(value));
    }

    public void SetFriendAva(TopVKUserData vk)
    {
        if (AvatarCor != null)
            StopCoroutine(AvatarCor);
        if(SocialManager.Instance!=null)
            AvatarCor = SocialManager.Instance.DownLoadImage(vk.ava, value => setAvatar(value));
    }

    public void OnFrameClick()
    {
        Checkbox.isOn = !Checkbox.isOn;
    }

    void setAvatar(Sprite ava)
    {
        Avatar.sprite = ava;
        Avatar.color = new Color(255, 255, 255, 255);
    }

    public void ShowPanel(bool Show)
    {
        PanelName.SetActive(Show);
    }


}
