﻿using UnityEngine;
using System.Collections;

public class ScrollFriends : MonoBehaviour
{
    public float WidthFrameFriend = 100;
    public float HeightFrameFriend = 50;

    public bool Horizontal, Vertical;

    public RectTransform ScrollContents;

    public void SetSizeScroll(int CountRow)
    {
        if(Horizontal)
            ScrollContents.sizeDelta = new Vector2(WidthFrameFriend * CountRow, 0);
        if(Vertical)
            ScrollContents.sizeDelta = new Vector2(0, HeightFrameFriend * CountRow);
    }

 
}
