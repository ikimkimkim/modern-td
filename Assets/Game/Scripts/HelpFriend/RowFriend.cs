﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class DataHelpFriend
{
    public int result;
    public string error;
    public int able_answer;
    public PlayerData user;
    public DataHelpFriend()
    { }
}

public class RowFriend : MonoBehaviour
{
    public static event Action onTakePrizeFriends, onSendPrizeFriends;

    public GameObject IconEnergy;
    public FrameFriend frameFriend;
    public Text RowText;
    public GameObject GetHelp,SendHelp, Done,Error;
    HelpFriendsPanel HelpFP;
    Friend _Friend;
    ServerManager _server;

    int status;

    public void SetFriendInfo(ServerManager server, Friend friend,HelpFriendsPanel hfp)
    {
        _server = server;
        _Friend = friend;
        HelpFP = hfp;
        frameFriend.SetFriendInfo(friend);
        //RowText.text = TranslationEngine.Instance.Trans["HelpFriend.YouFriendSendCristal.start"] + " "+ friend.name+" "+ TranslationEngine.Instance.Trans["HelpFriend.YouFriendSendCristal.end"];

        RowText.text = "Ваш друг " + friend.name + " прислал\r\nВам помощь +1";

        status = 0;
        GetHelp.SetActive(true);
        SendHelp.SetActive(false);
        Done.SetActive(false);

    }

    public void GetPrizes()
    {
        //string s = StringSerializationAPI.Serialize(_Friend.id.GetType(), _Friend.id);
        List<StringPair> extra = new List<StringPair>();
        extra.Add(new StringPair("id", _Friend.id.ToString()));
        StartCoroutine(_server.SaveLoadServer("takeHelpPrizes", extra, "", value => ResponceGetPrize(value)));

        GetHelp.GetComponent<Button>().interactable = false;

    }

    private void ResponceGetPrize(string strData)
    {
        DataHelpFriend data = (DataHelpFriend)StringSerializationAPI.Deserialize(typeof(DataHelpFriend), strData);
        CallBackSendHelp(data);
        if(data.result != 0)
        {
            if (onTakePrizeFriends != null)
                onTakePrizeFriends();
        }
    }

    public void SendHelpToFriend()
    {
        List<StringPair> extra = new List<StringPair>();
        extra.Add(new StringPair("id", _Friend.id.ToString())); //StringSerializationAPI.Serialize(_Friend.id.GetType(), _Friend.id)));
        StartCoroutine(_server.SaveLoadServer("sendAnswerHelp", extra, "", value => ResponceSendPrize(value)));


        SendHelp.GetComponent<Button>().interactable = false;
    }

    private void ResponceSendPrize(string strData)
    {
        DataHelpFriend data = (DataHelpFriend)StringSerializationAPI.Deserialize(typeof(DataHelpFriend), strData);
        CallBackSendHelp(data);
        if (data.result != 0)
        {
            if (onSendPrizeFriends != null)
                onSendPrizeFriends();
        }
    }


    void CallBackSendHelp(DataHelpFriend data)
    {
        Debug.Log("RF-> CBSH: " + data.result + ":" + data.able_answer);

        if (data.user != null)
            PlayerManager.SetPlayerData(data.user);

        if (data.error != null && data.error != "")
        {
            HelpFP.ShowTooltipError(data.error, GetComponent<RectTransform>());
        }

        GetHelp.SetActive(false);
        SendHelp.SetActive(false);
        Done.SetActive(false);

        if (status == 0)
        {
            if (data.able_answer == 1)
            {
                RowText.text = "Отправить помощь в ответ?";
                SendHelp.SetActive(true);
            }
            else
                DoneOrError(data.error != null && data.error != "");
            status++;
        }
        else
            DoneOrError(data.error != null && data.error != "");

        IconEnergy.SetActive(RowText.text != "Отправить помощь в ответ?");
    }


    public void DoneOrError(bool error)
    {
        Done.SetActive(!error);
        Error.SetActive(error);
    }

}
