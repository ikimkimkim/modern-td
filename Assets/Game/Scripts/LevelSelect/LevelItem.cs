﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LevelItem : MonoBehaviour
{
    public Text Level;
    public Image Star;
    public List<string> Stars;

    public Image Flag;
    public string FlagOff;
    public string FlagOn;
    public string FlagCurrent;

    public bool isTest = false;
    public string textTestFlag;
    bool _available = false;

    public int _stars { get; set; }

    public void Start()
    {
        if (isTest == false)
            return;
        Level.text = textTestFlag;
        StartCoroutine(TextureDB.instance.load(Stars[0], SetStars));
        StartCoroutine(TextureDB.instance.load(FlagOn, SetFlag));
        _available = true;
        Level.GetComponent<GradientText>().enabled = true;
        Level.GetComponent<Outline>().enabled = true;
        GetComponent<Button>().transition = Selectable.Transition.ColorTint;
    }

    public void SetStars(int stars, bool current)
    {
        if (isTest)
            return;
        //Debug.Log("SET STARS:" + transform.name);
        //Star.sprite = Stars[stars];
        _stars = stars;

        if (current)
        {
            StartCoroutine(TextureDB.instance.load(Stars[stars], SetStars));
            StartCoroutine(TextureDB.instance.load(FlagCurrent, SetFlag));
            _available = true;
            Level.GetComponent<GradientText>().enabled = true;
            Level.GetComponent<Outline>().enabled = true;
            GetComponent<Button>().transition = Selectable.Transition.ColorTint;
        }
        else if (stars >= 0)
        {
            StartCoroutine(TextureDB.instance.load(Stars[stars], SetStars));
            StartCoroutine(TextureDB.instance.load(FlagOn, SetFlag));
            _available = true;
            Level.GetComponent<GradientText>().enabled = true;
            Level.GetComponent<Outline>().enabled = true;
            GetComponent<Button>().transition = Selectable.Transition.ColorTint;
        }
        else
        {
            StartCoroutine(TextureDB.instance.load(FlagOff, SetFlag));
            StartCoroutine(TextureDB.instance.load(Stars[0], SetStars));
        }
    }

    public void SetStars(Sprite stars)
    {
        Star.sprite = stars;
    }

    public void SetFlag(Sprite flag)
    {
        Flag.sprite = flag;
    }

    public bool AvailableLevel()
    {
        return _available;
    }
}
