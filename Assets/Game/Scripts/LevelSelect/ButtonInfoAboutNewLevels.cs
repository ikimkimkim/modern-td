﻿using UnityEngine;
using System.Collections;

public class ButtonInfoAboutNewLevels : MonoBehaviour
{
    public InfoAboutNewLevels NewLevelsSoon;
    public BuyPackPanel BuyNextPack;
    public float delta;

    bool _isNextPackExist;

    public void Initialize(int CountPackOpen, bool isNextPackExist)
    {
        _isNextPackExist = isNextPackExist;
        GetComponent<RectTransform>().anchoredPosition3D = new Vector3(1032 * CountPackOpen + delta, 0,0);
    }

    public void Show()
    {
        if (_isNextPackExist)
        {
            BuyNextPack.Show();
        }
        else
        {
            NewLevelsSoon.gameObject.SetActive(true);
        }

    }
}
