﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Нужно чтобы при деактивации попапа магазина корутина не сдыхала 
/// </summary>
public class StoreHelper : MonoBehaviour
{

    public void CallServer()
    {
        var player = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
        var server = player.GetComponent<ServerManager>();
        StartCoroutine(server.SaveLoadServer("loadinfo", new List<StringPair>(), "", value => CallBack(value, player)));

    }

    void CallBack(string value, PlayerManager player)
    {
        Debug.Log("StoreHelper");
        player._SetPlayerData(((UserData)StringSerializationAPI.Deserialize(player.GetPlayerDataType(), value)).user);
        Destroy(gameObject);
    }
}
