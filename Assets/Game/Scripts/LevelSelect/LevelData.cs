﻿
[System.Serializable]
public class LevelData
{
    public int id;
    public int stars;
    public LevelData()
    {
    }
    public LevelData(int levelID, int starsEarned)
    {
        id = levelID;
        stars = starsEarned;
    }
   
}
