﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Используется для передаче информации о выбранной планете LevelSelectManager'у
/// </summary>
public class PackInfo : MonoBehaviour
{
    public int ID;
    public int StartIDLevelInPakc;
    public string Name;
    public string Desc;
    public string StoreID;
    public string ServerID;
    public int TrialLevelID;
    public Sprite Icon;
    public Sprite BoughtPicture;
    public bool isScaleLevelsFlagsPositions = false;
    public List<LevelItem> Levels;
    public List<Image> Lines;
    public Canvas MainCanvas;
    public bool isShow { get; private set; }

    private void Awake()
    {
        isShow = false;
    }

    void Start()
    {
        if (isScaleLevelsFlagsPositions)
        {
            ScaleLevelsFlagsPositions();
        }
    }
    public void Show(PlayerManager _playerInfo)
    {
        MyLog.Log("Pack " + ID + " Show");
        gameObject.SetActive(true);
        if (_playerInfo.GetLevelsCountComplele() >= StartIDLevelInPakc)
        /*{
            Levels[0].gameObject.SetActive(true);
            Levels[0].SetStars(_playerInfo.GetLevelStars(_playerInfo.GetLevelIDByIndexInPack(0, ID)), true);
            for (int i = 0; i < Lines.Count; i++)
            {
                Lines[i].color = new Color(74f / 255f, 59f / 255f, 45f / 255f);
            }
        }
        else if(_playerInfo.GetLevelsCount() > StartIDLevelInPakc)*/
        {
            InitializePack(_playerInfo);
        }
        isShow = true;
    }
    void ScaleLevelsFlagsPositions()
    {
        int referenceX = 800;
        int referenceY = 610;
        float aspectX = Screen.width / MainCanvas.scaleFactor / (float)referenceX;
        float aspectY = Screen.height / MainCanvas.scaleFactor / (float)referenceY;

        for (int i = 0; i < Levels.Count; i++)
        {
            Levels[i].transform.localPosition = new Vector3(Levels[i].transform.localPosition.x * aspectX, Levels[i].transform.localPosition.y * aspectY, Levels[i].transform.localPosition.z);
        }
        for (int i = 0; i < Lines.Count; i++)
        {
            Lines[i].transform.localPosition = new Vector3(Lines[i].transform.localPosition.x * aspectX, Lines[i].transform.localPosition.y * aspectY, Lines[i].transform.localPosition.z);
            Lines[i].rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Lines[i].rectTransform.sizeDelta.y * aspectY);
            Lines[i].rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Lines[i].rectTransform.sizeDelta.x * aspectX);
        }
    }

    private void InitializePack(PlayerManager _playerInfo)
    {


        int min = Mathf.Min(_playerInfo.GetLevelsCount(ID) + 1, Levels.Count); //_playerInfo.GetLevelsCount(ID) +1 нужно чтобы следующий уровень становился currnet для 11 уровней 12й например
        for (int i = 0; i < Levels.Count; i++)
        {
            if (i < min)
            {
                Levels[i].gameObject.SetActive(true);
                if (i == min - 1 && _playerInfo.GetLevelsCount(ID) != Levels.Count)
                {
                    Levels[i].SetStars(_playerInfo.GetLevelStars(_playerInfo.GetLevelIDByIndexInPack(i, ID)), true);
                }
                else
                {
                    Levels[i].SetStars(_playerInfo.GetLevelStars(_playerInfo.GetLevelIDByIndexInPack(i, ID)), false);

                }
            }
            else
            {
                Levels[i].SetStars(-1, false);
            }
            
        }

        

        //Чтобы закрасить последний отрезок линий при 12 пройденных уровней
        if (min == Levels.Count)
        {
            min++;
        }
        for (int i = 0; i < min - 1; i++)
        {
            Lines[i].color = new Color(252f / 255f, 251f / 255f, 251f / 255f);
        }
        for (int i = min - 1; i < Lines.Count; i++)
        {
            Lines[i].color = new Color(74f / 255f, 59f / 255f, 45f / 255f);
        }
    }

}
