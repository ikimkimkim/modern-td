﻿using UnityEngine;
using System.Collections;

public class InfoAboutNewLevels : MonoBehaviour {

    void OnEnable()
    {
        TimeScaleManager.SetTimeScale(0);
    }
    void OnDisable()
    {
        TimeScaleManager.RefreshTimeScale();
    }
    public void Notify()
    {
        //Debug.Log("Fake notify about new pack");
    }

    void Update()
    {
        if (CardUIAllInfo.isShow == false && gameObject.activeSelf == true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                gameObject.SetActive(false);
            }
        }
    }
}
