﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using Translation;
using System.Collections.Generic;

public class BuyResponse
{
    public int result;
    public PlayerData user;
    public BuyResponse()
    { }
}

public class BuyPackPanel : MonoBehaviour
{

    public GameObject Background;
    public GameObject BlackScreen;
    public GameObject BuyConfirmBackground;
    public GameObject SmallBackground;
    public static event Action<int> OnBuyPackPanelOpen;
   
    public Button ButtonFreeBuy;
    public Button ButtonPaidBuy;
    public Button ButtonPaidBuyBig;

    public LevelSelectManager Manager;

    public GameObject RecomendationTextSmallBackground;
    public GameObject RecomendationText;

    public Image Icon;

    public Text LocationName;
    public Text LocationNameSmallBackground;
    public Text LocationDesc;
    public Text LocationDescSmallBackground;
    public ButtonLoadLevelAsync TrialLevelButton;
    public ButtonLoadLevelAsync TrialLevelButtonSmallBackground;
    PlayerManager _playerInfo;
    //TODO сделать чтобы картинки и текст устанавливался в зависимости от покупаемого пака

    BillingManager _billingManager;
    int _buyPackID;
    LocationItemData _buyPackData;
    void Start()
    {
        _playerInfo = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
#if UNITY_ANDROID || UNITY_IOS
        _billingManager = GameObject.FindGameObjectWithTag("MobileBillingManager").GetComponent<BillingManager>();
#endif
    }

    public string[] TrySceneID;

    public void OnTryButton()
    {
        int id = Manager.GetCurrentPackID()-1;
        CardAndLevelSelectPanel.SetDataLevel("Пробный", 0, TrySceneID[id], 0);
        Hide();
    }

    public void FreePackBuy()
    {
        int nextIdPack = Manager.GetCurrentPackID() + 1;
        if (_playerInfo.CanBuyPack(nextIdPack, Manager.Packs))
        {
            Debug.Log("BuyResponse: Send buy location: tryGiveLocation"+ nextIdPack);
            //_playerInfo.BuyPack(Manager.GetCurrentPackID() + 1, false); //old
            var playerInfo = GameObject.FindGameObjectWithTag("PlayerInfo");
            ServerManager _server = playerInfo.GetComponent<ServerManager>();

            List<StringPair> param = new List<StringPair>();
            param.Add(new StringPair("id", nextIdPack.ToString()));
            StartCoroutine(_server.SaveLoadServer("tryGiveLocation", param, "", value =>
            {
                GetBuyResponse(value);
            }));

        }
    }

    public void GetBuyResponse(string brstr)
    {
        Debug.Log("BuyResponse:" + brstr);
        BuyResponse br =(BuyResponse)StringSerializationAPI.Deserialize(typeof(BuyResponse),brstr);
        Debug.Log("BuyResponse: result:" + br.result);
        if (br.result == 1)
        {
            _playerInfo._SetPlayerData(br.user);
            Manager.Start();            
        }
    }

    public void BuyPacksPaid()
    {
#if (UNITY_ANDROID || UNITY_IOS)
        _billingManager.BuyProduct(Manager.GetNextPackStoreID());
#endif
#if UNITY_WEBGL || UNITY_WEBPLAYER
        Application.ExternalCall("order", _buyPackData.item);
#endif
    }
    void OnEnable()
    {
        PlayerManager.OnPackBought += PlayerManager_OnPackBought;
    }
    void OnDisable()
    {
        PlayerManager.OnPackBought -= PlayerManager_OnPackBought;
    }
    void PlayerManager_OnPackBought()
    {
        Manager.OnRightMoverClick();
        Background.SetActive(false);
        SmallBackground.SetActive(false);
        BuyConfirmBackground.SetActive(true);
    }
    public void Hide()
    {
        BlackScreen.SetActive(false);
        Background.SetActive(false);
        SmallBackground.SetActive(false);
        BuyConfirmBackground.SetActive(false);
    }
#if UNITY_IOS || UNITY_ANDROID
    string GetPrice()
    {
        var items = _billingManager.GetItems();
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i].item == Manager.Packs[Manager.GetCurrentPackID()].StoreID)
            {
                return items[i].price;
            }
        }
        return "";
    }
#endif
    public void Show()
    {
        Translator trans = TranslationEngine.Instance.Trans;
        _buyPackID = Manager.GetCurrentPackID() + 1;
        BlackScreen.SetActive(true);
#if UNITY_IOS || UNITY_WEBGL || UNITY_WEBPLAYER
        Background.SetActive(true);
        if (!_playerInfo.CanBuyPack(Manager.GetCurrentPackID() + 1, Manager.Packs))
        {
            ButtonFreeBuy.GetComponent<Button>().interactable = false;
        }
        else
        {
            ButtonFreeBuy.GetComponent<Button>().interactable = true;
        }

        /*SmallBackground.SetActive(true);*/
        int trialID = Manager.Packs[_buyPackID - 1].TrialLevelID;
        if (trialID == 0)
        {
            TrialLevelButton.gameObject.SetActive(false);
        }
        else
        {
            TrialLevelButton.gameObject.SetActive(true);
            TrialLevelButton.GetComponent<Button>().GetComponentInChildren<Text>().text = trans["BuyPanel.TrialButton"].Replace("\\n", "\n");
            TrialLevelButton.Level = trialID;
        }
        LocationName.text = trans[Manager.Packs[_buyPackID - 1].Name];
        LocationDesc.text = trans[Manager.Packs[_buyPackID - 1].Desc].Replace("\\n","\n");
        Icon.sprite = Manager.Packs[_buyPackID - 1].Icon;


#if UNITY_IOS
        ButtonPaidBuyBig.GetComponentInChildren<Text>().text = trans["BuyPanel.Button.BuyPaid"] + " " + GetPrice();
#endif
        UpdateRectext(RecomendationTextSmallBackground);
#endif

#if UNITY_ANDROID
        Background.SetActive(true);
        if (!_playerInfo.CanBuyPack(Manager.GetCurrentPackID() + 1, Manager.Packs))
        {
            ButtonFreeBuy.GetComponent<Button>().interactable = false;
        }
        else
        {
            ButtonFreeBuy.GetComponent<Button>().interactable = true;
        }
          int trialID = Manager.Packs[_buyPackID - 1].TrialLevelID;
        if (trialID == 0)
        {
            TrialLevelButton.gameObject.SetActive(false);
        }
        else
        {
            TrialLevelButton.gameObject.SetActive(true);
            TrialLevelButton.GetComponent<Button>().GetComponentInChildren<Text>().text = trans["BuyPanel.TrialButton"].Replace("\\n", "\n");
            TrialLevelButton.Level = trialID;
        }
         LocationName.text = trans[Manager.Packs[_buyPackID - 1].Name];
        LocationDesc.text = trans[Manager.Packs[_buyPackID - 1].Desc].Replace("\\n","\n");
        ButtonPaidBuy.GetComponentInChildren<Text>().text = trans["BuyPanel.Button.BuyPaid"] + " " + GetPrice(); 
          UpdateRectext(RecomendationText);
#endif
#if UNITY_WEBGL || UNITY_WEBPLAYER
        _buyPackData = _playerInfo.BuyLocations[Manager.Packs[_buyPackID - 1].ServerID];
        ButtonPaidBuy.GetComponentInChildren<Text>().text = trans["BuyPanel.Button.BuyPaid"] + " "+ _buyPackData.price;
         UpdateRectext(RecomendationTextSmallBackground);
#endif
        if (OnBuyPackPanelOpen != null)
        {
            OnBuyPackPanelOpen(_buyPackID);
        }


    }

    private void UpdateRectext(GameObject text)
    {
        if (_playerInfo.GetLevelsCount(Manager.GetCurrentPackID()) == Manager.Packs[Manager.GetCurrentPackID() - 1].Levels.Count)
        {
            text.SetActive(false);
        }
        else
        {
            text.SetActive(true);
        }
    }

    void Update()
    {
        if (CardUIAllInfo.isShow == false && BlackScreen.activeSelf == true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Hide();
            }
        }
    }
}
