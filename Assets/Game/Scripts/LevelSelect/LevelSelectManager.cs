﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

/// <summary>
/// Главный класс LevelSelect сцены распологает картинки паков уровней и перемещает их влево вправо при нажатии
/// </summary>
public class LevelSelectManager : MonoBehaviour
{
    public Canvas MainCanvas;
    public List<PackInfo> Packs = new List<PackInfo>();
    PlayerManager _playerInfo;
    public Text StarsCount;
    public Text CrystalsCount;
    public Text GoldCount;
    public Text EnergiCount;
    public GameObject ButtonCardManager;

    public ButtonInfoAboutNewLevels ButtonInfoAboutNextPack;
    public GameObject ButtonFreeCrystals;

    public GameObject LeftMover;
    public GameObject RightMover;
    int _currentPackID = 0;
    int _lastPackID = 0;
    bool _isMoving = false;

    public static event Action OnMoveStarted;
    public static event Action OnMoveFinished;
   // static int _vkLastWatchedPack = -1;
    public void Start()
    {
        print("LevelSelectManager was start");
        _playerInfo = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
#if UNITY_WEBGL || UNITY_WEBPLAYER
            _currentPackID = _playerInfo.GetMaxPurchasedPackID();
       /* if (_vkLastWatchedPack == -1)
        {
            _vkLastWatchedPack = _currentPackID;
        }
        else
        {
            _currentPackID = _vkLastWatchedPack;
        }*/
#endif
#if UNITY_ANDROID || UNITY_IOS
        ScaleLeftRightMovers();
        _currentPackID = LoadLastWatchedPack();
        if (_currentPackID == -1)
        {
            _currentPackID = _playerInfo.GetMaxPurchasedPackID();
        }
#endif

        NewCloudManager(true);

        //инициализация доступных уровней
        //Подсчет звезд по пройденым уровням.
        float stars = 0;
        for (int i = 0; i < _playerInfo.GetMaxPurchasedPackID(); i++)
        {
            if (Packs.Count <= i)
                break;
            Packs[i].Show(_playerInfo);
            stars += _playerInfo.GetStarsInPack(i+1);
        }

        //PlacePacks(_currentPackID);
        //ShowHideMovers();
        ShowInfoAboutNextLevelsButton();

        //Добявлять теоретически доступные еще 3 звезды если след уровень доступен.
        double idpack = Math.Floor((_playerInfo.GetLevelsCountComplele()) / 12f);
        if(_playerInfo.isPackBought((int)idpack+1))
            StarsCount.text = stars + "/" + (_playerInfo.GetLevelsCountComplele() * 3 + 3).ToString();
        else
            StarsCount.text = stars + "/" + (_playerInfo.GetLevelsCountComplele() * 3).ToString();

        CrystalsCount.text = _playerInfo.GetCrystals().ToString();

        GoldCount.text = _playerInfo._playerData.Gold.ToString();

        if (_playerInfo.GetLevelsCountComplele() < 3 && ButtonFreeCrystals!=null)
        {
            ButtonFreeCrystals.SetActive(false);
        }
        ButtonCardManager.SetActive(_playerInfo.GetLevelsCountComplele() > 0);

        PlayerManager.InitializationСompleted += UpdateInfo;
    }

    public void UpdateInfo()
    {
        NewCloudManager(false);
        ShowInfoAboutNextLevelsButton();

        if(GoldCount!=null)
            GoldCount.text = _playerInfo._playerData.Gold.ToString();
        if(CrystalsCount!=null)
            CrystalsCount.text = _playerInfo.GetCrystals().ToString();
        if(EnergiCount!=null)
            EnergiCount.text = PlayerManager.CountEnergy.ToString() + "/10";
    }

    void OnDestroy()
    {
        print("LevelSelectManager was destroyed");
        PlayerManager.InitializationСompleted -= UpdateInfo;
    }

    public void NewCloudManager(bool bScrollilng)
    {
        //определения растояния скролла
        //определения позиции облаков по текущему левелу.
        double idpack = -1;
        if (CloudManager.instance != null)
        {
            //Debug.Log("1:"+ _playerInfo.GetMaxPurchasedPackID());

            CloudManager.instance.MoveScroll(_playerInfo.GetMaxPurchasedPackID());
            idpack = Mathf.Clamp(Mathf.Floor(_playerInfo.GetLevelsCountComplele() / 12f), 0, Packs.Count-1);
            Debug.Log("NewCloudManager:" + idpack +" : " + _playerInfo.GetLevelsCount((int)idpack + 1));
            int idlvl = Mathf.Clamp(_playerInfo.GetLevelsCount((int)idpack + 1), 0, 11);

            //Transform cur_f = GetCurrentLevelTransform();
            //Debug.Log("cur_f:" + cur_f.name + " " + cur_f.parent.name);
            CloudManager.instance.MoveClouds(Packs[(int)idpack].Levels[idlvl].GetComponent<RectTransform>().anchoredPosition3D.x, _playerInfo.GetMaxPurchasedPackID(), bScrollilng);

        }
        else
        {
            Debug.LogErrorFormat("LevelSelectManager-> CloudManager is null! Scroll:{0}", bScrollilng);
        }
    }

    void ScaleLeftRightMovers()
    {
        float scale = 1.3f;
        LeftMover.GetComponent<RectTransform>().localScale = new Vector3(scale, scale);
        RightMover.GetComponent<RectTransform>().localScale = new Vector3(scale, scale);
    }
    public int GetCurrentPackID()
    {
        return _currentPackID;
    }

    public string GetNextPackStoreID()
    {
        return Packs[_currentPackID].StoreID;
    }

    void PlacePacks(int showedPackID)
    {
        for (int i = 0; i < Packs.Count; i++)
        {
            Packs[i].transform.localPosition = new Vector3((i - showedPackID + 1) * Screen.width / MainCanvas.scaleFactor, 0, 0);
            Packs[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Screen.width / MainCanvas.scaleFactor);
            Packs[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Screen.height / MainCanvas.scaleFactor);
            //Packs[i].GetComponent<RectTransform>().localScale = new Vector3(1 / MainCanvas.scaleFactor, 1 / MainCanvas.scaleFactor, 1);
        }
    }
    /// <summary>
    /// Визуально двигаем все паки ВПРАВО
    /// </summary>
    public void OnLeftMoverClickNew()
    {
        _lastPackID = _currentPackID;
        _currentPackID--;
        CloudManager.instance.Scrolling(_currentPackID* 800);
    }
    public void OnLeftMoverClick()
    {
        if (_isMoving)
        {
            return;
        }
        _isMoving = true;
        _lastPackID = _currentPackID;
        _currentPackID--;
        Packs[_currentPackID - 1].Show(_playerInfo);
        //StartCoroutine(MovePacks(1, AfterMovePacks));
    }
    /// <summary>
    /// Визуально двигаем все паки ВЛЕВО
    /// </summary>
    public void OnRightMoverClickNew()
    {
        _lastPackID = _currentPackID;
        _currentPackID++;
        CloudManager.instance.Scrolling(_currentPackID * 800);
    }
    public void OnRightMoverClick()
    {
        if (_isMoving)
        {
            return;
        }
        //_isMoving = true;
        _lastPackID = _currentPackID;
        _currentPackID++;
        Packs[_currentPackID - 1].Show(_playerInfo);
        //StartCoroutine(MovePacks(-1, AfterMovePacks));
    }
    void AfterMovePacks()
    {
        Packs[_lastPackID - 1].gameObject.SetActive(false);
        ShowHideMovers();
        ShowInfoAboutNextLevelsButton();
        _isMoving = false;
        StarsCount.text = _playerInfo.GetStarsInPack(_currentPackID).ToString() + "/" + (Packs[_currentPackID - 1].Levels.Count * 3).ToString();
#if UNITY_ANDROID || UNITY_IOS
        SaveLastWatchedPack(_currentPackID);
#endif
        //_vkLastWatchedPack = _currentPackID;
    }

    public void ShowInfoAboutNextLevelsButton()
    {
        if(ButtonInfoAboutNextPack==null)
        {
            Debug.LogWarning("LSM -> ButtonInfoAboutNextPack is null");
            return;
        }
        if (_playerInfo.GetLevelsCount(_currentPackID) < 9 || _playerInfo.isPackBought(_currentPackID + 1))
        {
            ButtonInfoAboutNextPack.gameObject.SetActive(false);
        }
        else
        {
            ButtonInfoAboutNextPack.Initialize(Math.Min(Packs.Count, _playerInfo.GetMaxPurchasedPackID()), isPackExist(_currentPackID + 1));
            ButtonInfoAboutNextPack.gameObject.SetActive(true);
        }
    }
    /// <summary>
    /// Двигает все паки если direction == 1 вправо, если == -1 влево
    /// </summary>
    IEnumerator MovePacks(int direction, Action callback)
    {
        if (direction != 1 && direction != -1)
        {
            Debug.Log("Move packs direction = " + direction);
        }
        if (OnMoveStarted != null)
        {
            OnMoveStarted();
        }
        //Сохраним начальные позиции
        List<Vector3> startPositions = new List<Vector3>();
        for (int i = 0; i < Packs.Count; i++)
        {
            startPositions.Add(Packs[i].transform.localPosition);
        }

        float moveTime = 0.9f;
        float timer = 0;
        while (timer <= moveTime)
        {
            for (int i = 0; i < Packs.Count; i++)
            {
                Packs[i].transform.localPosition = Vector3.Lerp(startPositions[i], startPositions[i] 
                    + new Vector3(direction * Screen.width / MainCanvas.scaleFactor, 0, 0), Mathf.SmoothStep(0, 1, timer / moveTime));
            }
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        for (int i = 0; i < Packs.Count; i++)
        {
            Packs[i].transform.localPosition = startPositions[i] + new Vector3(direction * Screen.width / MainCanvas.scaleFactor, 0, 0);
        }
        if (callback != null)
        {
            callback();
        }
        if (OnMoveFinished != null)
        {
            OnMoveFinished();
        }
        yield break;
    }
    void ShowHideMovers()
    {
        if (_currentPackID == 1)
        {
            LeftMover.SetActive(false);
            RightMover.SetActive(_playerInfo.isPackBought(_currentPackID + 1));
        }
        else if (_currentPackID == Packs.Count)
        {
            LeftMover.SetActive(_playerInfo.isPackBought(_currentPackID - 1));
            RightMover.SetActive(false);
        }
        else
        {
            LeftMover.SetActive(_playerInfo.isPackBought(_currentPackID - 1));
            RightMover.SetActive(_playerInfo.isPackBought(_currentPackID + 1));
        }
    }
    void OnEnable()
    {
        PlayerManager.OnCrystalsChangedE += OnCrystalsChanged;
    }
    void OnDisable()
    {
        PlayerManager.OnCrystalsChangedE -= OnCrystalsChanged;
    }
    public void OnCrystalsChanged(int currentCrystals)
    {
        CrystalsCount.text = currentCrystals.ToString();
        //ShowHideMovers(); //поставим для теста
    }
    public bool isPackExist(int id)
    {
        for (int i = 0; i < Packs.Count; i++)
        {
            if (id == Packs[i].ID)
            {
                return true;
            }
        }
        return false;
    }
    int GetLevelsCountInAllPacks()
    {
        int count = 0;
        for (int i = 0; i < Packs.Count; i++)
        {
            count += Packs[i].Levels.Count;
        }
        return count;
    }
    int GetLevelsCountInShowPacks()
    {
        int count = 0;
        for (int i = 0; i < Packs.Count; i++)
        {
            if(Packs[i].isShow)
            count += Packs[i].Levels.Count;
        }
        return count;
    }
    public bool isAnyNotFinishedLevel()
    {
        //Debug.Log(_playerInfo.GetLevelsCount() + "!=" + GetLevelsCountInShowPacks());
        return _playerInfo.GetLevelsCountComplele() != GetLevelsCountInShowPacks();
    }
    public Vector3 GetCurrentLevelPosition()
    {
        Transform t = GetCurrentLevelTransform();
        if (t == null)
            return new Vector3(5000, 5000, 1);
        else
            return t.position;

    }
    public Transform GetCurrentLevelTransform()
    {
        int levels = _playerInfo.GetLevelsCountComplele();
        int idPack = 0;
        while(levels>=12)
        {
            levels -= 12;
            idPack++;
        }
        //Debug.Log("!!->" + _currentPackID);
        if (levels == 0 && idPack > _currentPackID - 1)
            return null;
        return Packs[idPack].Levels[levels].transform;
        /*
        if (levels >= 12)
        {
            levels -= 12;
            //Debug.LogError("Error level:" + levels);
            return Packs[1].Levels[levels].transform;
        }
        else
        {
            if (levels == Packs[_currentPackID - 1].Levels.Count)
                return null;
            else
                return Packs[0].Levels[levels].transform;
        }*/
    }
    /*void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (CrystalStore.activeInHierarchy)
            {
                CrystalStore.SetActive(false);
            }
            else
            {
                Application.LoadLevel(0);
            }

        }
    }*/

    void SaveLastWatchedPack(int packID)
    {
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        CryptoPlayerPrefs.Save("lastWatchedPack", packID.GetType(), packID);
#endif
#if UNITY_EDITOR
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/lastWatchedPack.d", FileMode.OpenOrCreate);
        //   Debug.Log(StringSerializationAPI.Serialize(typeof(int), packID));
        binaryFormatter.Serialize(file, packID);
        file.Close();
#endif
    }

    int LoadLastWatchedPack()
    {
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        if (CryptoPlayerPrefs.HasKey("lastWatchedPack"))
        {
            return (int)CryptoPlayerPrefs.Load("lastWatchedPack", typeof(int));
        }
#endif
#if UNITY_EDITOR
        if (File.Exists(Application.persistentDataPath + "/lastWatchedPack.d"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/lastWatchedPack.d", FileMode.Open);
            int data = (int)bf.Deserialize(file);
            // Debug.Log(StringSerializationAPI.Serialize(typeof(int), data));
            file.Close();
            return data;
        }
#endif
        return -1;
    }
}
