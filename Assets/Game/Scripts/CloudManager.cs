﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CloudManager : MonoBehaviour
{
    public float[] packEndPosX;
    public static CloudManager instance { get; private set; }
    private const float _StartWidth = 800;
    private const float _lvlSizeWidth = 800;
    public ScrollRect ScrollRectClouds;
    public RectTransform Clouds;
    public RectTransform ScrollContents;
    public float DeltaClouds,DeltaScroll;

    void Awake()
    {
        instance = this;
    }

    public void Scrolling(float PosX)
    {
        float f = (PosX - Screen.width / 2f + Screen.width / 3f + DeltaScroll) / (ScrollContents.sizeDelta.x - Screen.width/2f);
        f = Mathf.Clamp(f, 0f, 1f);
        ScrollRectClouds.horizontalNormalizedPosition =f; 
    }

    public void MoveScroll(float CountOpenPack)
    {
        ScrollContents.sizeDelta = new Vector2(_StartWidth + packEndPosX[(int)CountOpenPack - 1], 0);
    }

    public void MoveClouds(float PosXCurrentFlag, int CountOpenPack, bool bScrolling)
    {
        MyLog.Log("Move Cloud:"+PosXCurrentFlag + " : " + CountOpenPack);
        float newX = PosXCurrentFlag + DeltaClouds;
        if (newX > packEndPosX[CountOpenPack - 1])
        {
            newX = packEndPosX[CountOpenPack - 1];
        }
        Clouds.anchoredPosition3D = new Vector3(newX, 0,0);
        if(bScrolling)
            Scrolling(PosXCurrentFlag);
    }
    
}
