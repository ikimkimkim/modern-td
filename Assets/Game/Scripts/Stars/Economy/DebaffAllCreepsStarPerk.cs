﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class DebaffAllCreepsStarPerk : BasesSkillStarPerk
{

    //private float range = 2, procent = 0.5f;
    public override Group myGroup { get { return Group.Economy; } }

    public override string Name { get { return "Урон"; } }

    protected override void _Apply(List<Card> cards)
    {
        SpawnManager.onSpawnUnitE += SpawnManager_onSpawnUnitE;
    }

    private void SpawnManager_onSpawnUnitE(Unit unit)
    {
        if(Random.Range(0f,1f)<=0.5)
            unit.EffectsManager.AddEffect(null, new MalisonDebuffEffect(0.2f, 999999999));
        //unit.EffectsManager.AddEffect(null, new ExplosionAfterDeathDebaffUnlimitedEffect(range, procent));
    }

    protected override void _Cancel()
    {
        SpawnManager.onSpawnUnitE -= SpawnManager_onSpawnUnitE;
    }

    public override string GetTextInfo(int countActive)
    {
        return string.Format("Насылается \"Проклятье\" на некоторых монстров");
        //return string.Format("Монстры перед смертью взрываются нанося урон окружающим");        
    }
}
