﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpStartMoneyStarPerk : BasePropertiesStarPerk
{
    private const float procent = .02f;
    public override Group myGroup { get { return Group.Economy; } }

    public override string Name { get { return "За связи"; } }//Щедрость

    protected override void _Apply(List<Card> cards)
    {
        StarsPerk.ProcentUpMoney = count * procent;
    }

    protected override void _Cancel()
    {
        StarsPerk.ProcentUpMoney = 0;
    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("Стартовое количество монет увеличено на {0}%", procent*countActive*100);
        
    }
}

