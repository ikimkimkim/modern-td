﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpResourceCraftStarPerk : BasePropertiesStarPerk
{
    public static float RealProcent { get; private set; }

    private float Procent = 5f;
    public override Group myGroup { get { return Group.Economy; } }
    public override string Name { get { return "Щедрость"; } }

    public static string GetTextCountRes(int count)
    {
        if (RealProcent > 0)
        {
            var addResPerk = count - Mathf.CeilToInt(count / (1f + RealProcent/100f));
            if(addResPerk>0)
                return string.Format("{0}(+{1})", count - addResPerk, addResPerk);
            else
                return count.ToString();
        }
        else
            return count.ToString();
    }

    protected override void _Apply(List<Card> cards)
    {
        RealProcent = Procent * count;
    }

    protected override void _Cancel()
    {
        RealProcent = 0;
    }

    public override string GetTextInfo(int countActive)
    {
            return string.Format("Бонус {0}% ко всем получаемым ресурсам",Procent*countActive);
        
    }
}
