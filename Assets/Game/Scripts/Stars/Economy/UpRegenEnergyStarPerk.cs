﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpRegenEnergyStarPerk : BasePropertiesStarPerk
{
    private float Procent = 2f;
    public override Group myGroup { get { return Group.Economy; } }
    public override string Name { get { return "Энергоэффективность"; } }

    protected override void _Apply(List<Card> cards)
    {

    }

    protected override void _Cancel()
    {

    }

    public override string GetTextInfo(int countActive)
    {
        return string.Format("Время восстановления энергии уменьшено на {0} минуты", Procent*countActive);
    }
}
