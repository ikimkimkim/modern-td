﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TDTK;

public class BankStarPerk : BasesSkillStarPerk
{
    public static bool Active { get; private set; }
    public const float Procent = 0.3f;
    public override Group myGroup { get { return Group.Economy; } }

    protected override void _Apply(List<Card> cards)
    {
        //Debug.LogWarning("Active");
        SpawnManager.onWaveClearedE += SpawnManager_onWaveClearedE;
        Active = true;
    }

    private void SpawnManager_onWaveClearedE(int time)
    {
        //Debug.LogWarning("Add");
        float[] res = ResourceManager.GetResourceArray();
        ResourceManager.GainIncomeResource(new List<float>() { res[0] * Procent, 0 });
        IncomeRscAnimationText.StartAnim(Procent);
    }

    protected override void _Cancel()
    {
       // Debug.LogWarning("_Cancel");
        SpawnManager.onWaveClearedE -= SpawnManager_onWaveClearedE;
        Active = false;
    }

    public override string GetTextInfo(int countActive)
    {
            return string.Format("В конце каждой волны вам даются дополнительные монеты, {0}% от оставшихся денег", Procent*100);
        
    }
}