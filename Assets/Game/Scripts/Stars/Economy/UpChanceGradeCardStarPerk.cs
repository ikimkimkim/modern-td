﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpChanceGradeCardStarPerk : BasePropertiesStarPerk
{
    private float Procent = 5;
    public override Group myGroup { get { return Group.Economy; } }
    public override string Name { get { return "Удача"; } }

    protected override void _Apply(List<Card> cards)
    {

    }

    protected override void _Cancel()
    {

    }

    public override string GetTextInfo(int countActive)
    {
            return string.Format("Увеличение шанс успешного улучшения карты в Мастерской на {0}%", Procent * countActive);
        
    }

}
