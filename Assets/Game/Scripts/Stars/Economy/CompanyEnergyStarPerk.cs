﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompanyEnergyStarPerk : BasesSkillStarPerk
{
    private static bool active = false;
    public static bool isActive { get { return active; } }
    public override Group myGroup
    {
        get
        {
            return Group.Economy;
        }
    }

    protected override void _Apply(List<Card> cards)
    {
        active = true;
    }

    protected override void _Cancel()
    {
        active = false;
    }

    public override string GetTextInfo(int countActive)
    {
            return string.Format("Восстановление энергии за победы в кампании. Уровни на высокой сложности не требуют энергии.");
        
    }
}
