﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenCraftStarPerk : BasesSkillStarPerk
{
    public static int OpenLevel { get; protected set; }
    public override Group myGroup { get { return Group.Economy; } }


    static OpenCraftStarPerk()
    {
        OpenLevel = 1;
    }

    protected override void _Apply(List<Card> cards)
    {

    }

    protected override void _Cancel()
    {

    }
    public override string GetTextInfo(int countActive)
    {
        return string.Format("");

    }
}


public class OpenCraft1LvlStarPerk : OpenCraftStarPerk
{
    protected override void _Apply(List<Card> cards)
    {
        OpenLevel = 1;
    }

    protected override void _Cancel()
    {
        OpenLevel = 1;
    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("Возможность собирать обычные карты");
        
    }

}


public class OpenCraft2LvlStarPerk : OpenCraft1LvlStarPerk
{
    protected override void _Apply(List<Card> cards)
    {
        OpenLevel = 2;
    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("Возможность создавать в Мастерской магические карты ");
        
    }
}
public class OpenCraft3LvlStarPerk : OpenCraft1LvlStarPerk
{
    protected override void _Apply(List<Card> cards)
    {
        OpenLevel = 3;
    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("Возможность создавать в Мастерской редкие карты");
        
    }
}
public class OpenCraft4LvlStarPerk : OpenCraft1LvlStarPerk
{
    protected override void _Apply(List<Card> cards)
    {
        OpenLevel = 5;//5 так как 4 комплектные
    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("Возможность создавать в Мастерской легендарные карты");
        
    }
}