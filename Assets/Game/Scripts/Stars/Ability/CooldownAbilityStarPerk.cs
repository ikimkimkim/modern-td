﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CooldownAbilityStarPerk : BasePropertiesStarPerk
{
    public const float procent = 3;

    public override Group myGroup { get { return Group.Ability; } }
    public override string Name { get { return "За связи"; } }//Восстановление

    protected override void _Apply(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].Type == _CardType.Ability)
            {
                cards[i].Properties.Add(new CardProperties(11, new object[] { procent * count }, 0, new RequirementsProperties[0]));
            }
        }
    }

    protected override void _Cancel()
    {

    }

    public override string GetTextInfo(int countActive)
    {
            return string.Format("Уменьшение времени восстановления заклинаний на {0}%", procent*countActive);
        
    }
}

