﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownCostAbilityStarPerk : BasePropertiesStarPerk
{
    private const float procent = 1.5f;
    public override Group myGroup { get { return Group.Ability; } }
    public override string Name { get { return "Стоимость"; } }


    protected override void _Apply(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].Type == _CardType.Ability)
            {
                cards[i].Properties.Add(new CardProperties(193, new object[] { procent * count }, 0, new RequirementsProperties[0]));
            }
        }
    }

    protected override void _Cancel()
    {

    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("Снижение стоимости заклинания на {0}%", procent*countActive);
        
    }
}
