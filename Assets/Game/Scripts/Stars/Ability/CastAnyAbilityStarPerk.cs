﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastAnyAbilityStarPerk : BasesSkillStarPerk
{
    private const float procent = 25;
    public override Group myGroup { get { return Group.Ability; } }


    protected override void _Apply(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].Type == _CardType.Ability)
            {
                cards[i].Properties.Add(new CardProperties(195, new object[] { procent }, 0, new RequirementsProperties[0]));
            }
        }
    }

    protected override void _Cancel()
    {

    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("С шансом {0}% создаёт случайное заклинание из колоды в туже точку", procent);
        
    }

}