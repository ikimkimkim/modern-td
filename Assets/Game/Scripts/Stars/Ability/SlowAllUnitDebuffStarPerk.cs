﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowAllUnitDebuffStarPerk : BasesSkillStarPerk
{
    private const float procent = 3, duration = 8;
    public override Group myGroup { get { return Group.Ability; } }


    protected override void _Apply(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].Type == _CardType.Ability)
            {
                cards[i].Properties.Add(new CardProperties(194, new object[] { procent, duration }, 0, new RequirementsProperties[0]));
            }
        }
    }

    protected override void _Cancel()
    {

    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("Замедление передвижения монстров за каждое заклинание до {0}% на {1}c.", procent*10, duration);
        
    }
}
