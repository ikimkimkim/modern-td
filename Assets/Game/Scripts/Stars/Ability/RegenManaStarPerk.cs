﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegenManaStarPerk : BasePropertiesStarPerk
{
    private const float procent = 4.5f;
    public override Group myGroup { get { return Group.Ability; } }
    public override string Name { get { return "Генерация"; } }


    protected override void _Apply(List<Card> cards)
    {
        StarsPerk.ProcentRegenMana = count * procent;
    }

    protected override void _Cancel()
    {
        StarsPerk.ProcentRegenMana = 0;
    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("Восстановление маны увеличено на {0}%", procent*countActive);
        
    }
}
