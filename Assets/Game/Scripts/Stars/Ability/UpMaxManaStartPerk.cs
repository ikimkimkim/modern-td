﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpMaxManaStartPerk : BasePropertiesStarPerk
{
    private const float addCount = 2;
    public override Group myGroup { get { return Group.Ability; } }
    public override string Name { get { return "Расширение"; } }


    protected override void _Apply(List<Card> cards)
    {
        StarsPerk.AddManaMax = count * addCount;
    }

    protected override void _Cancel()
    {
        StarsPerk.AddManaMax = 0;
    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("Максимальное количество маны увеличено на {0}", addCount*countActive);
        
    }
}
