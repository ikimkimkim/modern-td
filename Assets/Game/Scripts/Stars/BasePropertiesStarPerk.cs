﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BasePropertiesStarPerk : BaseStarPerk
{

    public sealed override int getPriceStar(int countProgressBar)
    {
            int count = 0;
            switch (myGroup)
            {
                case Group.Tower:
                    count = StarsPerk.GetCountInGroupTower;
                    break;
                case Group.Ability:
                    count = StarsPerk.GetCountInGroupAbility;
                    break;
                case Group.Hero:
                    count = StarsPerk.GetCountInGroupHero;
                    break;
                case Group.Economy:
                    count = StarsPerk.GetCountInGroupEconomy;
                    break;
            }
            if (countProgressBar < 3)
                return 4;
            else if (countProgressBar < 6)
                return 4;
            else if (countProgressBar < 9)
                return 4;
            else if (countProgressBar < 12)
                return 4;
            else if (countProgressBar < 15)
                return 8;
            return 16;
        
    }
    public sealed override int getPriceRedStar(int countProgressBar)
    {
            int count = 0;
            switch (myGroup)
            {
                case Group.Tower:
                    count = StarsPerk.GetCountInGroupTower;
                    break;
                case Group.Ability:
                    count = StarsPerk.GetCountInGroupAbility;
                    break;
                case Group.Hero:
                    count = StarsPerk.GetCountInGroupHero;
                    break;
                case Group.Economy:
                    count = StarsPerk.GetCountInGroupEconomy;
                    break;
            }
            if (countProgressBar < 3)
                return 0;
            else if (countProgressBar < 6)
                return 0;
            else if (countProgressBar < 9)
                return 1;
            else if (countProgressBar < 12)
                return 2;
            else if (countProgressBar < 15)
                return 4;
            return 10;
        
    }

}
