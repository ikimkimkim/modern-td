﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChanceIgnoreArmorStarPerk : BasePropertiesStarPerk
{
    private const float chance = 10f;
    public override Group myGroup { get { return Group.Tower; } }
    public override string Name { get { return "Универсальность"; } }
    protected override void _Apply(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].Type == _CardType.Tower)
            {
                cards[i].Properties.Add(new CardProperties(186, new object[] { chance * count }, 0, new RequirementsProperties[0]));
            }
        }
    }

    protected override void _Cancel()
    {

    }


    public override string GetTextInfo(int countActive)
    {
            return string.Format("{0}% шанс нанести урон игнорируя тип брони", chance*countActive);
    }
}
