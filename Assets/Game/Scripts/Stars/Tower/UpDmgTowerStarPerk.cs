﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDmgTowerStarPerk : BasePropertiesStarPerk
{
    private const float dmgUp = 5.5f;
    public override Group myGroup { get { return Group.Tower; } }

    public override string Name { get { return "Урон"; } }

    protected override void _Apply(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].Type == _CardType.Tower)
            {
                cards[i].Properties.Add(new CardProperties(3, new object[] { dmgUp * count }, 0, new RequirementsProperties[0]));
            }
        }
    }
    protected override void _Cancel()
    {

    }


    public override string GetTextInfo(int countActive)
    {
            return string.Format("Увеличение урона башням на {0}%", dmgUp*countActive);
        
    }
}
