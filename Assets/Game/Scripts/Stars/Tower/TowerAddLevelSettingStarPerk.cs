﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAddLevelSettingStarPerk : BasesSkillStarPerk
{
    public override Group myGroup { get { return Group.Tower; } }

    protected override void _Apply(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if(cards[i].Type == _CardType.Tower)
            {
                cards[i].Properties.Add(new CardProperties(187, new object[] { 2 }, 0, new RequirementsProperties[0]));
            }
        }
    }

    protected override void _Cancel()
    {
    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("Максимальный уровень башни в бою увеличен на 2 уровня");        
    }
}
