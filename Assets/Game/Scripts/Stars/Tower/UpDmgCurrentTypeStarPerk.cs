﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDmgCurrentTypeStarPerk : BasePropertiesStarPerk
{
    private const float dmgUp = 8.5f;

    public override Group myGroup { get { return Group.Tower; } }

    public override string Name { get { return "Специализация"; } }

    protected override void _Apply(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if(cards[i].Type == _CardType.Tower)
            {
                cards[i].Properties.Add(new CardProperties(185, new object[] { dmgUp*count }, 0, new RequirementsProperties[0]));
            }
        }
    }
    protected override void _Cancel()
    {

    }

    public override string GetTextInfo(int countActive)
    {
        return string.Format("Увеличение урона на {0}% против монстров своего типа", dmgUp * countActive);
    }
}
