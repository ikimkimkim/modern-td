﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CostDropTowerStarPerk : BasePropertiesStarPerk
{
    private const float cost=2f;

    public override string Name { get { return "За связи"; } }//Стоимость
    public override Group myGroup
    {
        get
        {
            return Group.Tower;
        }
    }

    protected override void _Apply(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].Type == _CardType.Tower)
            {
                cards[i].Properties.Add(new CardProperties(16, new object[] { cost * count }, 0, new RequirementsProperties[0]));
            }
        }
    }

    protected override void _Cancel()
    {

    }

    public override string GetTextInfo(int countActive)
    {
            return "Снижение стоимости строительства и улучшения на " + cost*countActive + "%";
    }
}
