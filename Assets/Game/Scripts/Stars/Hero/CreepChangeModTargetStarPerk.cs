﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TDTK;

public class CreepChangeModTargetStarPerk : BasesSkillStarPerk
{
    public override Group myGroup { get { return Group.Hero; } }

    protected override void _Apply(List<Card> cards)
    {
        SpawnManager.onSpawnUnitE += SpawnManager_onSpawnUnitE;
    }

    private void SpawnManager_onSpawnUnitE(Unit unit)
    {
        unit.targetMode = _TargetMode.Hybrid;
    }

    protected override void _Cancel()
    {
        SpawnManager.onSpawnUnitE -= SpawnManager_onSpawnUnitE;
    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("Быстрые монстры перестают игнорировать поддержку и нападают");
        
    }
}
