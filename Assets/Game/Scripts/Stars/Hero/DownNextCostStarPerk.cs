﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownNextCostStarPerk : BasePropertiesStarPerk
{
    private const float procent = 5f;
    public override Group myGroup { get { return Group.Hero; } }

    public override string Name { get { return "Стоимость"; } }

    protected override void _Apply(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].Type == _CardType.Hero)
            {
                cards[i].Properties.Add(new CardProperties(191, new object[] { procent * count }, 0, new RequirementsProperties[0]));
            }
        }
    }
    protected override void _Cancel()
    {

    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("Уменьшение стоимости последующих вызовов на {0}%", procent*countActive);
        
    }
}
