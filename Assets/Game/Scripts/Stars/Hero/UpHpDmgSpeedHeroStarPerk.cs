﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpHpDmgSpeedHeroStarPerk : BasePropertiesStarPerk
{
    public const float  procent = 3f;

    public override Group myGroup { get { return Group.Hero; } }
    public override string Name { get { return "За связи"; } } //Усиление

    protected override void _Apply(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].Type == _CardType.Hero)
            {
                cards[i].Properties.Add(new CardProperties(190, new object[] { procent * count, procent * count, procent * count }, 0, new RequirementsProperties[0]));
            }
        }
    }
    protected override void _Cancel()
    {

    }

    public override string GetTextInfo(int countActive)
    {
            return string.Format("Увеличение здоровья, урона и скорости бега на {0}%", procent*countActive);
        
    }
}
