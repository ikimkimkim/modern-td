﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnHeroStarPerk : BasesSkillStarPerk
{
    public override Group myGroup { get { return Group.Hero; } }

    protected override void _Apply(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].Type == _CardType.Hero)
            {
                cards[i].Properties.Add(new CardProperties(192, new object[] { 60f }, 0, new RequirementsProperties[0]));
            }
        }
    }

    protected override void _Cancel()
    {

    }

    public override string GetTextInfo(int countActive)
    {
        return string.Format("Поддержка при смертельном ранении воскресает через минуту");        
    }
}
