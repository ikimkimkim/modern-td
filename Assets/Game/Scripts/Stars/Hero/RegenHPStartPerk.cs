﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegenHPStartPerk : BasePropertiesStarPerk
{
    private const float procent = 4.2f;
    public override Group myGroup { get { return Group.Hero; } }

    public override string Name { get { return "Восстановление"; } }

    protected override void _Apply(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].Type == _CardType.Hero)
            {
                cards[i].Properties.Add(new CardProperties(189, new object[] { procent * count }, 0, new RequirementsProperties[0]));
            }
        }
    }

    protected override void _Cancel()
    {

    }
    public override string GetTextInfo(int countActive)
    {
            return string.Format("Восстановление {0}% здоровья в минуту", procent*countActive);
        
    }
}
