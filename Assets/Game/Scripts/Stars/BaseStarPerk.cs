﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseStarPerk
{

    public enum Group { Tower, Ability, Hero, Economy }
    public abstract Group myGroup { get; }



    public int count = 0;
    private bool isApply = false;


    public abstract int getPriceStar(int countProgressBar);
    public abstract int getPriceRedStar(int countProgressBar);

    public void Apply(List<Card> cards)
    {
        if (count > 0)
        {
            _Apply(cards);
            isApply = true;
        }
    }

    protected abstract void _Apply(List<Card> cards);

    public void Cancel()
    {
        if (isApply)
        {
            _Cancel();
            isApply = false;
        }
    }
    protected abstract void _Cancel();

    public virtual string GetTextInfo()
    {
        return GetTextInfo(1);
    }

    public abstract string GetTextInfo(int countActive);

    public virtual string Name { get { return "No name"; } }
}
