﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class StarsPerk
{
    private static Dictionary<int, BaseStarPerk> db;
    private static List<int> PerkIDs;
    public static int[] GetPerkIDs { get { return PerkIDs.ToArray(); } }

    private static List<int> TowerGrop, AbilityGrop, HeroGrop, EconomyGrop;
    public static string GetNameGroup(int ID)
    {
        switch (GetGroupPerk(ID))
        {
            case BaseStarPerk.Group.Tower:
                return "Башни";
            case BaseStarPerk.Group.Ability:
                return "Заклинания";
            case BaseStarPerk.Group.Hero:
                return "Поддержка";
            case BaseStarPerk.Group.Economy:
                return "Экономика";
        }
        return "Error nof find group";

    }

    public static float AddManaMax { get; set; }
    public static float ProcentUpMoney { get; set; }
    public static float ProcentRegenMana { get; set; }

    public static int GetCountInGroupTower {
        get
        {
            return PerkIDs.FindAll(i => TowerGrop.Contains(i)).Count;
        }
    }
    public static int GetCountInGroupAbility
    {
        get
        {
            return PerkIDs.FindAll(i => AbilityGrop.Contains(i)).Count;
        }
    }
    public static int GetCountInGroupHero
    {
        get
        {
            return PerkIDs.FindAll(i => HeroGrop.Contains(i)).Count;
        }
    }
    public static int GetCountInGroupEconomy
    {
        get
        {
            return PerkIDs.FindAll(i => EconomyGrop.Contains(i)).Count;
        }
    }

    public static int CountPerk(int ID)
    {
        if (db.ContainsKey(ID))
            return db[ID].count;
        return 0;
    }

    public static string GetNameAndLevel(int ID)
    {
        if (db.ContainsKey(ID))
            return string.Format("{0} ({1})", db[ID].Name, db[ID].count);
        return "";
    }

    public static string GetTextInfo(int ID,int countActive)
    {
        if (db.ContainsKey(ID))
            return db[ID].GetTextInfo(countActive);
        return "";
    }
    public static string GetTextInfo(int ID)
    {
        return GetTextInfo(ID, 1);
    }

    public static BaseStarPerk.Group GetGroupPerk(int ID)
    {
        if (db.ContainsKey(ID))
            return db[ID].myGroup;
        return 0;
    }

    public static int GetPriceStar(int ID)
    {
        switch (GetGroupPerk(ID))
        {
            case BaseStarPerk.Group.Tower:
                return GetPriceStar(ID, PerkIDs.FindAll(i => TowerGrop.Contains(i)).Count);
            case BaseStarPerk.Group.Ability:
                return GetPriceStar(ID, PerkIDs.FindAll(i => AbilityGrop.Contains(i)).Count);
            case BaseStarPerk.Group.Hero:
                return GetPriceStar(ID, PerkIDs.FindAll(i => HeroGrop.Contains(i)).Count);
            case BaseStarPerk.Group.Economy:
                return GetPriceStar(ID, PerkIDs.FindAll(i => EconomyGrop.Contains(i)).Count);
        }
        return 0;
    }  
    public static int GetPriceStar(int ID,int countProgressBar)
    {
        if (db.ContainsKey(ID))
            return db[ID].getPriceStar(countProgressBar);
        return 0;
    }
    public static int GetPriceRedStar(int ID)
    {
        switch (GetGroupPerk(ID))
        {
            case BaseStarPerk.Group.Tower:
                return GetPriceRedStar(ID, PerkIDs.FindAll(i => TowerGrop.Contains(i)).Count);
            case BaseStarPerk.Group.Ability:
                return GetPriceRedStar(ID, PerkIDs.FindAll(i => AbilityGrop.Contains(i)).Count);
            case BaseStarPerk.Group.Hero:
                return GetPriceRedStar(ID, PerkIDs.FindAll(i => HeroGrop.Contains(i)).Count);
            case BaseStarPerk.Group.Economy:
                return GetPriceRedStar(ID, PerkIDs.FindAll(i => EconomyGrop.Contains(i)).Count);
        }
        return 0;
    }
    public static int GetPriceRedStar(int ID, int countProgressBar)
    {
        if (db.ContainsKey(ID))
            return db[ID].getPriceRedStar(countProgressBar);
        return 0;
    }

    public static bool isPropertiesPerk(int ID)
    {
        return TowerGrop.Contains(ID) || HeroGrop.Contains(ID) || AbilityGrop.Contains(ID) || EconomyGrop.Contains(ID);
    }

    static StarsPerk()
    {
        TowerGrop = new List<int>() { 1, 2, 3 };
        HeroGrop = new List<int>() { 10, 11, 12 };
        AbilityGrop = new List<int>() { 20, 21, 22 };
        EconomyGrop = new List<int>() { 30, 31, 32 };



        db = new Dictionary<int, BaseStarPerk>();

        //башни
        db.Add(1, new UpDmgCurrentTypeStarPerk());
        db.Add(2, new UpDmgTowerStarPerk());
        db.Add(3, new ChanceIgnoreArmorStarPerk());
        db.Add(-1, new CostDropTowerStarPerk()); //для связей

        db.Add(4, new TowerOpenProperties2LevelStarPerk());
        db.Add(5, new TowerOpenProperties3LevelStarPerk());
        db.Add(6, new TowerOpenProperties4LevelStarPerk());
        db.Add(7, new TowerOpenProperties5LevelStarPerk());

        db.Add(8, new TowerAddLevelSettingStarPerk());
        db.Add(9, new SelectedAnyPropertiesStarPerk());

        //hero
        db.Add(10, new DownNextCostStarPerk());
        db.Add(11, new RegenHPStartPerk());
        db.Add(12, new UpXPStarPerk());
        db.Add(-10, new UpHpDmgSpeedHeroStarPerk());

        db.Add(13, new HeroOpenProperties2LevelStarPerk());
        db.Add(14, new HeroOpenProperties3LevelStarPerk());
        db.Add(15, new HeroOpenProperties4LevelStarPerk());
        db.Add(16, new HeroOpenProperties5LevelStarPerk());

        db.Add(17, new CreepChangeModTargetStarPerk());
        db.Add(18, new RespawnHeroStarPerk());

        //ability
        db.Add(20, new UpMaxManaStartPerk());
        db.Add(21, new RegenManaStarPerk());
        db.Add(22, new DownCostAbilityStarPerk());
        db.Add(-20, new CooldownAbilityStarPerk());

        db.Add(23, new AbilityOpenProperties2LevelStarPerk());
        db.Add(24, new AbilityOpenProperties3LevelStarPerk());
        db.Add(25, new AbilityOpenProperties4LevelStarPerk());
        db.Add(26, new AbilityOpenProperties5LevelStarPerk());

        db.Add(27, new SlowAllUnitDebuffStarPerk());
        db.Add(28, new CastAnyAbilityStarPerk());

        //экономика
        db.Add(-30, new UpStartMoneyStarPerk());
        db.Add(31, new UpRegenEnergyStarPerk());
        db.Add(32, new UpResourceCraftStarPerk());
        db.Add(30, new UpChanceGradeCardStarPerk());


        db.Add(33, new CompanyEnergyStarPerk());
        db.Add(34, new OpenCraft2LvlStarPerk());
        db.Add(35, new OpenCraft3LvlStarPerk());
        db.Add(36, new OpenCraft4LvlStarPerk());

        db.Add(37, new DebaffAllCreepsStarPerk());
        db.Add(38, new BankStarPerk());
    }


    public static void SetData(List<int> perks)
    {
        PerkIDs = perks;
        ClearPerk();
        for (int i = 0; i < perks.Count; i++)
        {
            if(db.ContainsKey(perks[i]))
            {
                db[perks[i]].count++;
            }
            else
            {
                Debug.LogError("Perk id not find:" + perks[i]);
            }
        }
        CalcContactPerk(perks);
        ApplyAllPerk();
    }

    private const int Colums = 3, Rows = 20;
    private static void CalcContactPerk(List<int> perks)
    {
        int curColum = 0;
        int curRow = 0;
        perks = perks.FindAll(i => TowerGrop.Contains(i) || HeroGrop.Contains(i) || AbilityGrop.Contains(i) || EconomyGrop.Contains(i));
        //Debug.LogWarning(StringSerializationAPI.Serialize(perks.GetType(), perks));
        for (int i = 0; i < perks.Count; i++)
        {
            if(TowerGrop.Contains(perks[i]))
            {
                ContactPerkGroup(perks, TowerGrop, -1, curColum, curRow);
            }
            else if (HeroGrop.Contains(perks[i]))
            {
                ContactPerkGroup(perks, HeroGrop, -10, curColum, curRow);
            }
            else if (AbilityGrop.Contains(perks[i]))
            {
                ContactPerkGroup(perks, AbilityGrop, -20, curColum, curRow);
            }
            else if (EconomyGrop.Contains(perks[i]))
            {
                ContactPerkGroup(perks, EconomyGrop, -30, curColum, curRow);
            }
            else
            {
                continue;
            }

            curColum++;
            if(curColum >= Colums)
            {
                curRow++;
                curColum = 0;
            }
        }
    }

    private static void ContactPerkGroup(List<int> perks, List<int> grop, int idPerk, int currentColum, int currerntRow)
    {
        int bufCol = currentColum + 1;
        int bufRow = currerntRow;
        int index = bufRow * Colums + bufCol;
        if (index<perks.Count && bufCol < Colums && grop.Contains(perks[index]))
        {
           // Debug.LogWarningFormat("AddC {0} {3}:{4} => {1}:{2} | {5}=>{6}", idPerk, bufCol, bufRow,currentColum,currerntRow, perks[currerntRow * Colums + currentColum], perks[index]);
            db[idPerk].count++;
        }

        bufCol = currentColum;
        bufRow = currerntRow + 1;
        index = bufRow * Colums + bufCol;
        if (index < perks.Count && bufRow < Rows && grop.Contains(perks[index]))
        {
           // Debug.LogWarningFormat("AddR {0} {3}:{4} => {1}:{2} | {5}=>{6}", idPerk, bufCol, bufRow, currentColum, currerntRow, perks[currerntRow * Colums + currentColum], perks[index]);
            db[idPerk].count++;
        }
    }

    private static void ApplyAllPerk()
    {
        foreach (var key in db.Keys)
        {
            db[key].Apply(PlayerManager.CardsList);
        }
    }

    private static void ClearPerk()
    {
        foreach (var key in db.Keys)
        {
            db[key].Cancel();
            db[key].count = 0;
        }
    }

}
