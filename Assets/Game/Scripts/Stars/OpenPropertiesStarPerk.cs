﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenPropertiesStarPerk : BasesSkillStarPerk
{
    public static int OpenTowerLevel { get; protected set; }
    public static int OpenHeroLevel { get; protected set; }
    public static int OpenAbilityLevel { get; protected set; }
    public override Group myGroup { get { return Group.Tower; } }


    static OpenPropertiesStarPerk()
    {
        OpenTowerLevel = 0;
        OpenHeroLevel = 0;
        OpenAbilityLevel = 0;
    }

    public static bool isOpenLevel(_CardType type, int level)
    {

        switch (type)
        {
            case _CardType.Tower:
                return OpenTowerLevel >= level;
            case _CardType.Ability:
                return OpenAbilityLevel >= level;
            case _CardType.Hero:
                return OpenHeroLevel >= level;
        }

        return false;
    }

    public static bool isOpenLevel(Card card)
    {
        return isOpenLevel(card.Type, card.LevelTower);
    }

    
    protected override void _Apply(List<Card> cards)
    {
    }


    protected override void _Cancel()
    {

    }
    public override string GetTextInfo(int countActive)
    {
        return "";

    }
}

#region tower
public class TowerOpenProperties2LevelStarPerk : OpenPropertiesStarPerk
{
    protected override void _Apply(List<Card> cards)
    {
        OpenTowerLevel = 2;
    }

    protected override void _Cancel()
    {
        OpenTowerLevel = 0;
    }
    public override string GetTextInfo(int countActive)
    {
            return "Умения 2 уровня";
        
    }
}

public class TowerOpenProperties3LevelStarPerk : TowerOpenProperties2LevelStarPerk
{
    protected override void _Apply(List<Card> cards)
    {
        OpenTowerLevel = 3;
    }
    public override string GetTextInfo(int countActive)
    {
            return "Умения 3 уровня";
        
    }

}
public class TowerOpenProperties4LevelStarPerk : TowerOpenProperties2LevelStarPerk
{
    protected override void _Apply(List<Card> cards)
    {
        OpenTowerLevel = 4;
    }
    public override string GetTextInfo(int countActive)
    {
            return "Умения 4 уровня";
        
    }

}
public class TowerOpenProperties5LevelStarPerk : TowerOpenProperties2LevelStarPerk
{
    protected override void _Apply(List<Card> cards)
    {
        OpenTowerLevel = 5;
    }

    public override string GetTextInfo(int countActive)
    {
            return "Умения 5 уровня";
        
    }
}
#endregion


#region Hero
public class HeroOpenProperties2LevelStarPerk : OpenPropertiesStarPerk
{
    public override Group myGroup { get { return Group.Hero; } }
    protected override void _Apply(List<Card> cards)
    {
        OpenHeroLevel = 2;
    }

    protected override void _Cancel()
    {
        OpenHeroLevel = 0;
    }
    public override string GetTextInfo(int countActive)
    {
            return "Умения 2 уровня";
        
    }

}

public class HeroOpenProperties3LevelStarPerk : HeroOpenProperties2LevelStarPerk
{
    public override Group myGroup { get { return Group.Hero; } }
    protected override void _Apply(List<Card> cards)
    {
        OpenHeroLevel = 3;
    }

    public override string GetTextInfo(int countActive)
    {
            return "Умения 3 уровня";
        
    }
}
public class HeroOpenProperties4LevelStarPerk : HeroOpenProperties2LevelStarPerk
{
    public override Group myGroup { get { return Group.Hero; } }
    protected override void _Apply(List<Card> cards)
    {
        OpenHeroLevel = 4;
    }

    public override string GetTextInfo(int countActive)
    {
            return "Умения 4 уровня";
        
    }
}
public class HeroOpenProperties5LevelStarPerk : HeroOpenProperties2LevelStarPerk
{
    public override Group myGroup { get { return Group.Hero; } }
    protected override void _Apply(List<Card> cards)
    {
        OpenHeroLevel = 5;
    }

    public override string GetTextInfo(int countActive)
    {
            return "Умения 5 уровня";
        
    }
}
#endregion


#region Ability
public class AbilityOpenProperties2LevelStarPerk : OpenPropertiesStarPerk
{
    public override Group myGroup { get { return Group.Ability; } }
    protected override void _Apply(List<Card> cards)
    {
        OpenAbilityLevel = 2;
    }

    protected override void _Cancel()
    {
        OpenAbilityLevel = 0;
    }

    public override string GetTextInfo(int countActive)
    {
            return "Умения 2 уровня";
        
    }
}

public class AbilityOpenProperties3LevelStarPerk : AbilityOpenProperties2LevelStarPerk
{
    public override Group myGroup { get { return Group.Ability; } }
    protected override void _Apply(List<Card> cards)
    {
        OpenAbilityLevel = 3;
    }

    public override string GetTextInfo(int countActive)
    {
            return "Умения 3 уровня";
        
    }
}
public class AbilityOpenProperties4LevelStarPerk : AbilityOpenProperties2LevelStarPerk
{
    public override Group myGroup { get { return Group.Ability; } }
    protected override void _Apply(List<Card> cards)
    {
        OpenAbilityLevel = 4;
    }

    public override string GetTextInfo(int countActive)
    {
            return "Умения 4 уровня";
        
    }
}
public class AbilityOpenProperties5LevelStarPerk : AbilityOpenProperties2LevelStarPerk
{
    public override Group myGroup { get { return Group.Ability; } }
    protected override void _Apply(List<Card> cards)
    {
        OpenAbilityLevel = 5;
    }

    public override string GetTextInfo(int countActive)
    {
            return "Умения 5 уровня";
        
    }
}
#endregion
