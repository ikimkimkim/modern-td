﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadFirtsScenes : MonoBehaviour
{

    public ButtonLoadLevelAsync LoadLevel;

    private int DeffFirstLevel = 0;
    private bool startLoad;

    public int IDLevel = 1, IDFirstScene = 3, IDMapScene = 1;

    void Awake()
    {
        startLoad = false;
        PlayerManager.InitializationСompleted += LoadScen;
    }

    void Start()
    {
        if(startLoad==false)
           StartCoroutine(WaitData());
    }

    IEnumerator WaitData()
    {
        Debug.Log("LoadFirtsScenes wait data.");
        do
        {
            yield return new WaitForSeconds(1f);
        }
        while (PlayerManager.CardsList == null);

        if (startLoad==false)
            LoadScen();
    }

    public void LoadScen()
    {
        if (startLoad == true)
            return;
        Debug.Log("LoadFirtsScenes load scen.");
        startLoad = true;
        PlayerManager.InitializationСompleted -= LoadScen;

        if (PlayerManager._instance.GetLevelsCountComplele() > 0)
        {
            LoadLevelSelect();
        }
        else
        {
            LoadFirstScane();
        }

    }

    private void LoadLevelSelect()
    {
        LoadLevel.Level = IDMapScene;
        LoadLevel.LoadLevel();
        Debug.Log("Load level:" + LoadLevel.Level);
    }

    private void LoadFirstScane()
    {
        int count = 0;
        List<int[]> idTowers = new List<int[]>();
        foreach (Card card in PlayerManager.CardsList)// AllCards)
        {
            if (count > 5)
                card.disableInBuildManager = true;
            else
            {
                card.disableInBuildManager = false;
                idTowers.Add(new int[] { card.IDTowerData + 1, (int)card.rareness + 1 });
                count++;
            }
        }

        Debug.Log(count + "Cards count:" + PlayerManager.CardsList.Count);
        PlayerManager.DM.StartLevel(IDLevel, DeffFirstLevel+1, idTowers,new List<int[]>(), resultStart);

    }

    public void resultStart(ResponseData response)
    {
        if (response.ok == 1)
        {
            CardAndLevelSelectPanel.RND = response.rnd;
            CardAndLevelSelectPanel.IDLevel = IDLevel;
            CardAndLevelSelectPanel.SetLevelDifficulty(DeffFirstLevel);

            Debug.Log("Play Data Level " + IDLevel + ":" + IDFirstScene);

            string bundle = MapDB.Load().Find(i => i.LevelID == IDLevel).NameBundle;
            BundleManager.StartLoadingScene(bundle);
            //LoadLevel.Level = IDFirstScene;
            //LoadLevel.LoadLevel();
        }
        else
        {
            Debug.LogError("Start first level error:"+response.error.code+", "+response.error.text);
            TooltipMessageServer.Show(response.error.text);
            LoadLevelSelect();
        }
    }
}
