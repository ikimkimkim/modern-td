﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShowOnlyAdmin : MonoBehaviour {
    
    private static readonly int[] listID = {
    89098799, //Михаил
    203654, //Алексей
    80164260 //Андрей
    };

    public static bool isAdmin
    {
        get
        {
#if (UNITY_WEBGL || UNITY_WEBPLAYER) && !UNITY_EDITOR
            return listID.Contains(SocialManager.Instance.getVKUserID);
#else
            return true;
#endif
        }
    }

    // Use this for initialization
    void Start()
    {
#if (UNITY_WEBGL || UNITY_WEBPLAYER) && !UNITY_EDITOR
        try
        {
            this.gameObject.SetActive(listID.Contains(SocialManager.Instance.getVKUserID));
        }
        catch
        {
            Debug.LogError("TestFlagActiveted hidden.");
            this.gameObject.SetActive(false);
        }
#endif
    }
}
