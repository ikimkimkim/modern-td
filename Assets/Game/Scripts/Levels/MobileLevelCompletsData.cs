﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class MobileLevelCompletsData
{
    /// <summary>
    /// ID уровня, количество прохождений этого уровня
    /// </summary>
    public Dictionary<int, int> LevelComplets = new Dictionary<int, int>();

}
