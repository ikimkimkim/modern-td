﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine.UI;

public class CreeperWaveInfoManager : MonoBehaviour {

    public float TimeHideInfo = 9f;

    public Text textinfo;
    private string lastTextInfo;

    private static Coroutine _CorSetInfo;

    public static CreeperWaveInfoManager instance { get; private set; }

    private List<string> boss = new List<string>();
    private List<string> creep = new List<string>();
    private List<string> NameCreepAddInfo = new List<string>();
    // Use this for initialization
    void Awake ()
    {
        instance = this;
	}

    public static void SetWaveInfo(Wave wave, bool startInfo)
    {
        if (instance != null)
        {
            if (_CorSetInfo != null)
                instance.StopCoroutine(_CorSetInfo);
            _CorSetInfo = instance.StartCoroutine(instance.setWaveInfo(wave, startInfo));
        }
        else
            Debug.Log("CreeperWaveInfoManager is null!");
    }

    UnitCreep uc;
    public IEnumerator setWaveInfo(Wave wave,bool startInfo)
    {
        UIHUD.SetActiveButtonSpawnNextWave(false);
        textinfo.text = "В текущей волне: " + lastTextInfo;

        if (startInfo==false)
        {
            yield return new WaitForSeconds(TimeHideInfo);
            textinfo.text = "";
            yield return new WaitForSeconds(1);
        }
        textinfo.text = "";

        if (wave != null)
        {
            boss.Clear();
            creep.Clear();
            NameCreepAddInfo.Clear();
            for (int i = 0; i < wave.subWaveList.Count; i++)
            {
                uc = wave.subWaveList[i].unit.GetComponent<UnitCreep>();
                if (NameCreepAddInfo.Contains(uc.unitName) == false)
                {
                    NameCreepAddInfo.Add(uc.unitName);
                    if (uc.isBoss)
                        boss.Add(uc.UnitInfoText);
                    else
                        creep.Add(uc.UnitInfoText);
                }
            }

            for (int i = 0; i < boss.Count; i++)
            {
                textinfo.text += boss[i] + ", ";
            }

            for (int i = 0; i < creep.Count; i++)
            {
                if (i + 1 == creep.Count - 1)
                    textinfo.text += creep[i] + " и ";
                else
                    textinfo.text += creep[i] + ", ";
            }

            textinfo.text = textinfo.text.Remove(textinfo.text.Length - 2);

            //if (creep.Count > 0)
           //     textinfo.text += " противники";

            lastTextInfo = textinfo.text;

            //if(startInfo==false)
                textinfo.text = "В следующей волне: "+ textinfo.text;
            /*else
                textinfo.text = "В текущей волне: " + textinfo.text;*/

            if(startInfo==false && SpawnManager.instance._isAllowSkip)
                UIHUD.SetActiveButtonSpawnNextWave(true);
        }
    }
}
