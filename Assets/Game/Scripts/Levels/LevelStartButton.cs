﻿using UnityEngine;
using System.Collections;

public class LevelStartButton : MonoBehaviour {

    public int Level_ID;
    //public int Scene_ID;
    [SerializeField] private string _nameBundleSceneOnlyTest;
    public string NameLevel;

    private LevelItem level;

    public void Start()
    {
        level = GetComponent<LevelItem>();
    }

    public void OnStartLevel()
    {
        if (NameLevel == "")
            NameLevel = "ID:" + Level_ID;

        if (level.AvailableLevel())
        {
            CardAndLevelSelectPanel.SetDataLevel(NameLevel, Level_ID, _nameBundleSceneOnlyTest, level._stars);
            //GetComponent<ButtonLoadLevelAsync>().LoadLevel();
        }
    }

}
