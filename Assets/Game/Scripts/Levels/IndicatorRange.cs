﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorRange : Singleton<IndicatorRange> {

    private string pathPrefab = "Indicators/RangeIndicatorAddition";


    [SerializeField]
    private GameObject prefabProjector;

    private Transform targetShow;
    private GameObject projectorObj;
    private Projector projector;

    protected override void InitializationSingleton()
    {
        if(prefabProjector == null)
        {
            prefabProjector = Resources.Load<GameObject>(pathPrefab);
        }
        projectorObj = Instantiate(prefabProjector,transform);
        projectorObj.SetActive(false);
        projector = projectorObj.GetComponentInChildren<Projector>();
    }

    public static void Show(Transform target, float range)
    {
        instance._show(target, range);
    }

    private void _show(Transform target, float range)
    {
        targetShow = target;
        projectorObj.SetActive(true);
        projector.orthographicSize = range + 0.2f;
    }

    private void Update()
    {
        if(projectorObj.activeSelf && targetShow!=null)
        {
            projectorObj.transform.position = targetShow.position;
        }
    }

    public static void Hide()
    {
        instance._hide();
    }

    private void _hide()
    {
        if (projectorObj.activeSelf)
        {
            projectorObj.SetActive(false);
            targetShow = null;
        }
    }

}
