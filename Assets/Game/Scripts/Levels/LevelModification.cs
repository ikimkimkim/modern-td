﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public static class LevelModification
{
    private static List<Card> cardsModification;

    private static float UpMoneyProcent, ChanceDownHpProcent, DownHpProcent=0.2f, ChanceDownSpeedProcent, DownSpeedProcent=0.2f;

    static LevelModification()
    {
        cardsModification = new List<Card>();
    }

    public static void AddCard(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            AddCard(cards[i]);
        }
    }

    public static void AddCard(Card cards)
    {
        if (cards.Type == _CardType.Upgrade)
        {
            cardsModification.Add(cards);
            InitParam(cards);
        }
        else
            Debug.LogErrorFormat("LevelMod -> AddCards {0}", cards.Type);
    }

    private static void InitParam(Card card)
    {
        switch(card.IDTowerData)
        {
            case 507: //+Money
                switch(card.rareness)
                {
                    case _CardRareness.Usual:
                        UpMoneyProcent = 0.1f;
                        break;
                    case _CardRareness.Magic:
                        UpMoneyProcent = 0.2f;
                        break;
                    case _CardRareness.Rare:
                        UpMoneyProcent = 0.4f;
                        break;
                    case _CardRareness.Legendary:
                        UpMoneyProcent = 0.6f;
                        break;
                }
                break;
            case 508: //-hp
                switch (card.rareness)
                {
                    case _CardRareness.Usual:
                        ChanceDownHpProcent = 0.1f;
                        break;
                    case _CardRareness.Magic:
                        ChanceDownHpProcent = 0.2f;
                        break;
                    case _CardRareness.Rare:
                        ChanceDownHpProcent = 0.3f;
                        break;
                    case _CardRareness.Legendary:
                        ChanceDownHpProcent = 0.5f;
                        break;
                }
                break;
            case 509: //-speed
                switch (card.rareness)
                {
                    case _CardRareness.Usual:
                        ChanceDownSpeedProcent = 0.1f;
                        break;
                    case _CardRareness.Magic:
                        ChanceDownSpeedProcent = 0.2f;
                        break;
                    case _CardRareness.Rare:
                        ChanceDownSpeedProcent = 0.3f;
                        break;
                    case _CardRareness.Legendary:
                        ChanceDownSpeedProcent = 0.5f;
                        break;
                }
                break;
        }
    }

    public static void Clear()
    {
        cardsModification.Clear();
        UpMoneyProcent = 0;
        ChanceDownHpProcent = 0;
        ChanceDownSpeedProcent = 0;
    }

    public static float procentUpMoney
    {
        get
        {
            return UpMoneyProcent;
        }
    }

    public static void ApplyModificationUnit(Unit unit)
    {
        ApplyHPDown(unit);
        ApplySpeedDown(unit);
    }


    private static void ApplyHPDown(Unit unit)
    {
        if (ChanceDownHpProcent <= 0)
            return;

        if (Random.Range(0f, 1f) > ChanceDownHpProcent)
            return;

        unit.ApplyDamageHP(null, unit.fullHP * DownHpProcent);
    }

    private static void ApplySpeedDown(Unit unit)
    {
        if (ChanceDownSpeedProcent <= 0)
            return;

        if (Random.Range(0f, 1f) > ChanceDownSpeedProcent)
            return;

        unit.EffectsManager.AddEffect(null, new SlowControlUnlimitedEffect(DownSpeedProcent));
    }

}
