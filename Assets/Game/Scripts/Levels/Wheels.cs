﻿using UnityEngine;
using System.Collections;

public class Wheels : MonoBehaviour
{

    public float RotationSpeed;
    Transform _transform;
    Vector3 _rotationVector;
    void Awake()
    {
        _transform = GetComponent<Transform>();
        _rotationVector = new Vector3(RotationSpeed, 0, 0);
    }
    void Update()
    {
        _transform.Rotate(_rotationVector);
    }
}
