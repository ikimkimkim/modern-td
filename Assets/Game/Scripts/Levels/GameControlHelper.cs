﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Указываем какие крипы на этом уровне новые и платформы для таториала 
/// </summary>
public class GameControlHelper : MonoBehaviour
{
    public List<GameObject> NewUnits = new List<GameObject>();

    public GameObject Platform1;
    public GameObject Platform2;

}
