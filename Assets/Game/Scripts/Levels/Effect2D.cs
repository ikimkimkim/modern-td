﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;

/// <summary>
/// Платные заклинания летящие сердечки и золото  по направлению к иконкам соответствуюшим
/// </summary>
public class Effect2D : MonoBehaviour
{

    public Transform Target;

    public GameObject SpawnedPrefab;
    public int AllAmount;
    public int AmountPerSpawn;
    public float SpawnPeriod;
    public float SpawnCircleRadius;
    public float MoveDuration;

    List<RectTransform> _spawned = new List<RectTransform>();
    List<float> _spawnedTime = new List<float>();
    List<Vector3> _spawnedPosition = new List<Vector3>();
    RectTransform _transform;
    Vector3 _target;
    int _allAmountSaver;
    void Start()
    {
        _transform = GetComponent<RectTransform>();
        _allAmountSaver = AllAmount;
    }
    void Update()
    {
        for (int i = 0; i < _spawned.Count; )
        {
            _spawned[i].position = Vector3.Lerp(_spawnedPosition[i], Target.position, Mathf.SmoothStep(0f, 1f, Mathf.SmoothStep(0f, 1f, (1 / MoveDuration) * (Time.time - _spawnedTime[i]))));

            if ((1 / MoveDuration) * (Time.time - _spawnedTime[i]) >= 1f)
            {
                Destroy(_spawned[i].gameObject);
                _spawned.RemoveAt(i);
                _spawnedTime.RemoveAt(i);
                _spawnedPosition.RemoveAt(i);
            }
            else
            {
                i++;
            }

        }
    }
    public void Show()
    {
        StartCoroutine(Spawn());
    }
    IEnumerator Spawn()
    {
        while (AllAmount > 0)
        {
            if (AllAmount < AmountPerSpawn)
            {
                AmountPerSpawn = AllAmount;
            }
            AllAmount -= AmountPerSpawn;
            for (int i = 0; i < AmountPerSpawn; i++)
            {
                var newParticle = (Instantiate(SpawnedPrefab) as GameObject).GetComponent<RectTransform>();
                newParticle.localScale *= UI.GetScaleFactor();
                newParticle.SetParent(_transform);
                Vector2 randomPoint = Random.insideUnitCircle * SpawnCircleRadius;
                newParticle.localPosition = Vector3.zero + new Vector3(randomPoint.x, randomPoint.y, 0);
                _spawnedPosition.Add(newParticle.position);
                _spawned.Add(newParticle);
                _spawnedTime.Add(Time.time);
            }
            yield return new WaitForSeconds(SpawnPeriod);
        }
        AllAmount = _allAmountSaver;
        yield break;
    }
}
