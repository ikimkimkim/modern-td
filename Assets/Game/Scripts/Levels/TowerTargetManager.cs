﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using TDTK;
/// <summary>
/// Устанавливаем приоритет целей для каждого из тауверов 
/// </summary>
public class TowerTargetManager : MonoBehaviour
{
    public List<TargetPriorityList> TowersTargets = new List<TargetPriorityList>();
    static TowerTargetManager _instance;



    public static TowerTargetManager GetInstance()
    {
        return _instance;
    }
    void Awake()
    {
        _instance = this;
    }
    void Start()
    {
        for (int i = 0; i < TowersTargets.Count; i++)
        {
            TowersTargets[i].Initialize();
        }
    }
    TargetPriorityList GetApplayableTargetList(Unit tower)
    {
        for (int i = 0; i < TowersTargets.Count; i++)
        {
            if (TowersTargets[i].isApplyable(tower))
            {
                return TowersTargets[i];
            }
        }
        Debug.Log("TTM-> GetApplayableTargetList " + TowersTargets.Count);
        return null;
    }


    public Unit GetTarget(Unit tower,List<Unit> tgtList)
    {
       

        //Debug.Log("TTM->" + tower.unitName + ":"+tower.damageType +" unity:" + tgtList.Count);

       /* Unit newTarger = tgtList[0];

        foreach (Unit t in tgtList)
        {
            if(MatrixPrioritet[tower.damageType,t.armorType] > MatrixPrioritet[tower.damageType, newTarger.armorType])
            {
                newTarger = t;
            }
        }
        return newTarger;*/

        //old-----

        TargetPriorityList applyableList = GetApplayableTargetList(tower);

        if (applyableList == null)
        {
            Debug.LogError("TTM->Error!");
        }
        Unit target = null;
        float maxPriority = 0;
        //Выясняем какой среди юнитов максимальный приоритет
        for (int i = 0; i < tgtList.Count; i++)
        {
            float targetPriority = applyableList.GetPriority(tower,tgtList[i]);
            if (targetPriority > maxPriority)
            {
                maxPriority = targetPriority;
                /*if (maxPriority == 1)// потому что 1 это самые приоритетные цели
                {
                    break;
                }*/
            }
        }
        //делаем список юнитов с максимальным приоритетом
        List<Unit> priorityTargets = new List<Unit>();
        for (int i = 0; i < tgtList.Count; i++)
        {
            if (applyableList.GetPriority(tower, tgtList[i]) == maxPriority)
            {
                priorityTargets.Add(tgtList[i]);
            }
        }
        //выясняем у кого из приоритетных юнитов саммое маленькое расстояние 
        float lowest = Mathf.Infinity;
        for (int i = 0; i < priorityTargets.Count; i++)
        {
            if (priorityTargets[i].GetDistFromDestination() < lowest)
            {
                lowest = priorityTargets[i].GetDistFromDestination();
                target = priorityTargets[i];
            }
        }
        return target;
    }

}
[System.Serializable]
public class UnitPriorityPair
{
    public Unit TargetUnit;
    public int Priority;
}
