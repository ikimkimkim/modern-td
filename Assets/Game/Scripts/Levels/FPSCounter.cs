﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FPSCounter : MonoBehaviour
{
    [Header("Test limit frame rate")]
    public bool bTest = false;
    public int CountFrameRate = 15;

    
   

    [Header("Counter FPS")]
    public float updateInterval = 0.5F;
    public delegate void UpdateFPSCount(int fps);
    public static event UpdateFPSCount OnUpdateFPS;
    private float accum = 0; // FPS accumulated over the interval
    private int frames = 0; // Frames drawn over the interval
    private float timeleft; // Left time for current interval
    Text _text;

    public static float AvgFps { get { return Summ / Count; } }
    private static float Summ, Count;

    public bool bShow = true;

    private void Awake()
    {
#if UNITY_EDITOR
        if (bTest)
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = CountFrameRate;
        }
#endif
    }

    void Start()
    {
        _text = GetComponent<Text>();
        _text.text = "";
        timeleft = 0;
        Summ = 0;
        Count = 0;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                bShow = !bShow;
                _text.text = "";
            }
        }
        
        
        timeleft -= Time.deltaTime;
        accum += Time.timeScale / Time.deltaTime;
        ++frames;

        // Interval ended - update GUI text and start new interval
        if (timeleft <= 0.0)
        {
            // display two fractional digits (f2 format)
            float fps = accum / frames;
            if (OnUpdateFPS != null)
            {
                OnUpdateFPS((int)fps);
            }
            if (bShow == true)
            {
                string format = System.String.Format("{0:F2} FPS", fps);
                _text.text = format;

                if (fps < 30)
                    _text.color = Color.yellow;
                else
                    if (fps < 10)
                    _text.color = Color.red;
                else
                    _text.color = Color.green;
            }
            //	DebugConsole.Log(format,level);
            timeleft = updateInterval;
            accum = 0.0F;
            frames = 0;
            Count++;
            Summ += fps;
        }
    }
}