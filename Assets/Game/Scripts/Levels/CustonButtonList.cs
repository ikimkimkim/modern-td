﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine.SceneManagement;
/// <summary>
/// Используется в первом уровне чтобы сначала можно было построить только арту а потом только пулемет
/// </summary>
public class CustonButtonList : MonoBehaviour
{
    //Арта 4, пулемет 2й
    public List<UnityButton> buttonList;
    public List<bool> ButtonAvailably = new List<bool>();
    int idFirstLevel=-100;
    bool _phaseOne = false;
    void Start()
    {
         if (SceneManager.GetActiveScene().buildIndex != 3) // Application.loadedLevel != 3)
         {
             buttonList = GetComponent<UIBuildButton>().buttonList;
             for (int i = 0; i < buttonList.Count; i++)
             {
                 ButtonAvailably.Add(true);
             }
             GetComponent<UIBuildButton>().ButtonAvailably = ButtonAvailably;
             //enabled = false;
         }
         else
         {
             buttonList = GetComponent<UIBuildButton>().buttonList;

             for (int i = 0; i < buttonList.Count; i++)
             {

                 if (i == 3)
                 {
                     //все ОК!
                     ButtonAvailably.Add(true);
                 }
                 else
                 {
                     ButtonAvailably.Add(false);
                     buttonList[i].imageIcon.color = new Color(184f / 255f, 184f / 255f, 184f / 255f, 0.5f);
                 }
             }
             GetComponent<UIBuildButton>().ButtonAvailably = ButtonAvailably;
         }
    }
    void ActivateAllButtons()
    {
        for (int i = 0; i < buttonList.Count; i++)
        {
            ButtonAvailably[i] = true;
            buttonList[i].imageIcon.color = new Color(1f, 1f, 1f, 1f);
        }
    }

    void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == idFirstLevel)// Application.loadedLevel == idFirstLevel)
        {
            if (!_phaseOne && BuildManager.GetTowerCount() == 1)
            {
                _phaseOne = true;
                ActivateAllButtons();
                for (int i = 0; i < buttonList.Count; i++)
                {
                    if (i == 1) continue;
                    ButtonAvailably[i] = false;
                    buttonList[i].imageIcon.color = new Color(184f / 255f, 184f / 255f, 184f / 255f, 0.75f);
                }
                
            }
            else if(BuildManager.GetTowerCount() >= 2)
            {
                UpdateEnableBildTower();
            }
        }
        else
        {
            UpdateEnableBildTower();
        }

        
        /*if (BuildManager.GetTowerCount() == 2)
        {
            ActivateAllButtons();
            enabled = false;
        }*/
    }

    private void UpdateEnableBildTower()
    {
        ActivateAllButtons();
        /*float cost = ResourceManager.GetResourceArray()[0].value;

        for (int i = 0; i < buttonList.Count; i++)
        {
            if (BuildManager.GetSampleTower(i * 5).GetCost()[0] <= cost)
            {
                //ButtonAvailably[i] = true;
            }
            else
            {
                //ButtonAvailably[i] = false;
                buttonList[i].imageIcon.color = new Color(184f / 255f, 184f / 255f, 184f / 255f, 0.75f);
            }
        }*/
    }

}
