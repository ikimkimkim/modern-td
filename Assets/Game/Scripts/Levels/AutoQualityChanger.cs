﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AutoQualityChanger : MonoBehaviour
{

    int CollectTimes = 30;
    public int FPSLimit;

    PlayerManager _player;
    //ServerManager _server;
    double _FPSSumm;
    int _timer;

    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
        //_server = _player.GetComponent<ServerManager>();
        if (_player.isPlayerManuallySetQualityLevel() == 0)
        {
            LoadQualityLevel();
        }
    }
    void LoadQualityLevel()
    {
#if UNITY_ANDROID || UNITY_IOS
        //int current = PlayerPrefs.GetInt("Quality");
        //if (current != 0)
        //{
        //    QualitySettings.SetQualityLevel(current, true);
        //}
        //QualitySettings.SetQualityLevel(5, true);
#endif
#if UNITY_WEBGL || UNITY_WEBPLAYER
        MyLog.LogWarning("SetQualityLevel");
        //QualitySettings.SetQualityLevel(5, true);
#endif
    }
    void SaveQualityLevel()
    {

        if (_player.isPlayerManuallySetQualityLevel() == 0)
        {
#if UNITY_ANDROID || UNITY_IOS
            PlayerPrefs.SetInt("Quality", QualitySettings.GetQualityLevel());
#endif
#if UNITY_WEBGL || UNITY_WEBPLAYER
            //List<StringPair> extra = new List<StringPair>();
            //extra.Add(new StringPair("name", "quality"));
            //extra.Add(new StringPair("value", QualitySettings.GetQualityLevel().ToString()));
            //StartCoroutine(_server.SaveLoadServer("changeSettings", extra, "", value => _player.SetPlayerData(((UserData)StringSerializationAPI.Deserialize(_player.GetPlayerDataType(), value)).user)));
#endif

        }

    }
    void OnEnable()
    {
        FPSCounter.OnUpdateFPS += OnUpdateFPS;
    }
    void OnDisable()
    {
        FPSCounter.OnUpdateFPS -= OnUpdateFPS;
    }
    void OnUpdateFPS(int fps)
    {
        if (_player.isPlayerManuallySetQualityLevel() == 0)
        {
            _timer++;
            _FPSSumm += fps;

            if (_timer == CollectTimes)
            {
                _timer = 0;
                double aveFPS = _FPSSumm / CollectTimes;
                if (aveFPS <= FPSLimit)
                {
                    if (QualitySettings.GetQualityLevel() > 3)
                    {
                        //QualitySettings.DecreaseLevel(true);
                        //Debug.Log("Quality changed " + QualitySettings.GetQualityLevel());
                        SaveQualityLevel();
                    }
                }
            }
        }

    }

}
