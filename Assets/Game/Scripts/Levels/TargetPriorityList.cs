﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;

[System.Serializable]
public class TargetPriorityList
{
    public List<Unit> Tower = new List<Unit>();
    public List<UnitPriorityPair> Targets = new List<UnitPriorityPair>();
    public Dictionary<string, int> UnitNamePriority = new Dictionary<string, int>();


    //i-урона j-броня
    private float[,] MatrixPrioritet =
    {
        {125,75,50,100 },
        {50,125,100,75 },
        {75,100,125,50 },
        {75,75,100,100 },
    };

    public void Initialize()
    {
        List<ArmorType> armorT = DamageTable.GetAllArmorType();

        MatrixPrioritet = new float[armorT.Count, armorT[0].modifiers.Count];
        for (int i = 0; i < armorT.Count; i++)
        {
            for (int j = 0; j < armorT[i].modifiers.Count; j++)
            {
                //Debug.Log(i+"\\"+j+" -> "+armorT[i]+":"+armorT[i].modifiers[j]);
                MatrixPrioritet[i, j] = armorT[i].modifiers[j];
            }
        }

        MyLog.Log("TargetPriorityList Initialize");
        /*for (int i = 0; i < Targets.Count; i++)
        {
            UnitNamePriority.Add(Targets[i].TargetUnit.unitName, Targets[i].Priority);
        }*/
    }
    public bool isApplyable(Unit tower)
    {
        //Debug.Log("Исправить определение приоритета");
        return true;
        /*for (int i = 0; i < Tower.Count; i++)
        {
            Debug.Log("TPL->isApplyable " + Tower[i].unitName + " == " + tower.unitName);
            if (Tower[i].unitName == tower.unitName)
            {
                return true;
            }
        }
        return false;*/
    }
    public float GetPriority(Unit tower, Unit unit)
    {
        return MatrixPrioritet[ unit.ArmorType(), tower.DamageType()];

/*
        if (unit.armorType == tower.damageType)
        {
            return 2;
        }
        else
            return 1;

        return UnitNamePriority[unit.unitName];*/
    }
}
