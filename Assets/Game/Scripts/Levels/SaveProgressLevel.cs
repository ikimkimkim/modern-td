﻿using System;
using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
    public struct SaveProgressData
    {
        public int lvl_id;
        public int diff;
        public int time;
        public int rnd;
        public int map_id;
        public float hp;
        public float score;
        public int[] stars_config;
    }

public class SaveProgressLevel : UIEntity
{


    public override int entityID { get { return 5; } }

    public override int priorityShow { get { return 250; } }

    public static void SaveProgressUnlimited()
    {
        Debug.Log("SaveProgressUnlimited");
        SaveProgressData data = new SaveProgressData();
        data.lvl_id = CardAndLevelSelectPanel.IDLevel;
        data.diff = CardAndLevelSelectPanel.LevelDifficultySelect + 1;
        data.time = Mathf.CeilToInt(GameControl.GetTimeGame);
        data.rnd = CardAndLevelSelectPanel.RND;
        data.map_id = CardAndLevelSelectPanel.IDMap;
        data.score = UIScorePanel.GetScore;
        data.stars_config = CardAndLevelSelectPanel.StarsConfig;
        data.hp = GameControl.GetPlayerLife();

        string json_data = StringSerializationAPI.Serialize(data.GetType(), data);

        PlayerManager.DM.SaveProgressUnlimited(CardAndLevelSelectPanel.RND, CardAndLevelSelectPanel.IDLevel,
            CardAndLevelSelectPanel.LevelDifficultySelect + 1, json_data, resultSaveProgress);
    }


    private static void resultSaveProgress(ResponseData rp)
    {
        if (rp.ok == 0)
        {
            if (rp.error != null)
            {
                Debug.LogErrorFormat("SaveProgress: {0}:{1}", rp.error.code, rp.error.text);
                TooltipMessageServer.Show(rp.error.text);
            }
            else
            {
                Debug.LogError("SaveProgress: error null!");
            }
        }
       
    }



    public GameObject ErrorObj;
    public Text ErrorText;

    public GameObject rootPanel;

    public GameObject StarsPanel;
    public Image[] StarEarned = new Image[3];
    public Image[] StarsDontEarned = new Image[3];
    public float StarShowDelay;
    [Header("Game")]
    public GameObject GameWinMenu;
    public GameObject PanelInfoWM;
    public Text GoldWM, CristalWM;
    public GameObject GoldObj, CristalObj;
    public Image ChestIconWM;
    public Text NameChestWM, deltaTimeChestWM;
    public GameObject ObjNameChestWM, ObjDeltaTimeWM, ObjSlotLimitWM;


    [Header("Portal")]
    public GameObject PortalWinMenu;
    public AudioSource AudioPWM;
    public Text GoldPWM, CristalPWM, TimePWM, ScorePWM;
    public GameObject PanelInfoPWM;
    public GameObject GoldPObj, CristalPObj;
    public GameObject ObjTimePWM, ObjScorePWM;
    public Image ChestIconPWM;
    public Text NameChestPWM, deltaTimeChestPWM, deltaTimeCloseChestPWM;
    public GameObject ObjNameChestPWM, ObjDeltaTimeChestPWM, ObjDeltaTimeCloseChestPWM, ObjSlotLimitPWM;

    private SaveProgressData data;
    private int Stars;
    void Initialization()
    {
        ShowUIEvent();
        CardAndLevelSelectPanel.IDLevel = data.lvl_id;
        CardAndLevelSelectPanel.LevelDifficultySelect = data.diff-1;
        CardAndLevelSelectPanel.RND = data.rnd;
        CardAndLevelSelectPanel.IDMap = data.map_id;
        //UIScorePanel.Score = data.score;

        if (data.stars_config != null && data.stars_config[0] < data.score)
        {
            if (data.stars_config[1] < data.score)
            {
                if (data.stars_config[2] < data.score)
                {
                    Stars = 3;
                }
                else
                    Stars = 2;
            }
            else
                Stars = 1;
        }
        else
            Stars = 0;

        PlayerManager.DM.EndLevel(CardAndLevelSelectPanel.IDLevel, CardAndLevelSelectPanel.LevelDifficultySelect + 1, Stars, 
            CardAndLevelSelectPanel.RND, 0, CardAndLevelSelectPanel.IDMap, data.score, data.hp, data.time, CBonGameOver);

    }


    private ResponseData.PrizesData prize;
    private void CBonGameOver(ResponseData rp)
    {

        if(rp.ok==0)
        {
            if (rp.error != null)
            {
                Debug.LogErrorFormat("Save progress, end lvl error {0}:{1}", rp.error.code, rp.error.text);
                TooltipMessageServer.Show(rp.error.text);
            }
            HideUIEvent();
            return;
        }

        PlayerManager.SetPlayerData(rp.user);

        prize = rp.prizes;

        rootPanel.SetActive(true);


        ShowPortalUIGameOver();
        StartCoroutine(StarShower(Stars));
        // UIGameOverMenu.HideE.AddListener(HideUIGameOver);
        // UIGameOverMenu.Show(Stars, rp);
    }

    private void ShowPortalUIGameOver()
    {
        PortalWinMenu.SetActive(true);
        ObjDeltaTimeChestPWM.SetActive(false);
        ObjDeltaTimeCloseChestPWM.SetActive(false);
        ObjNameChestPWM.SetActive(false);
        ObjSlotLimitPWM.SetActive(false);

        if(PlayerManager.isSoundOn())
            AudioPWM.Play();

        if (prize.gold > 0)
        {
            GoldPObj.SetActive(true);
            GoldPWM.text = prize.gold.ToString();
        }
        else
            GoldPObj.SetActive(false);

        if (prize.crystals > 0)
        {
            CristalPObj.SetActive(true);
            CristalPWM.text = prize.crystals.ToString();
        }
        else
            CristalPObj.SetActive(false);

        PanelInfoPWM.SetActive(prize.gold> 0 || prize.crystals > 0);

        ObjTimePWM.SetActive(data.lvl_id==-2);
        ObjScorePWM.SetActive(data.lvl_id == -1);

        TimeSpan _time = new TimeSpan(0, 0, data.time);
        TimePWM.text = _time.ToString();
        ScorePWM.text = UIScorePanel.GetScore.ToString();
        if (_time.Minutes > 10 || (_time.Minutes == 10 && _time.Seconds > 0))
        {
            TimePWM.color = Color.red;
        }
        else
        {
            TimePWM.color = Color.green;
        }

        if (prize.chest > 0)
        {

            ObjNameChestPWM.SetActive(true);
            NameChestPWM.text = Chest.getNameChest((Chest.TypeChest)prize.chest - 1);
            StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsChest[prize.chest - 1], value => { ChestIconPWM.sprite = value; }));

            // Debug.LogWarning("Нету расчетов для отображение сокращения времени открытия сундука");
            if (CardAndLevelSelectPanel.BigPortal)
            {
                if (_time.Minutes > 10 || (_time.Minutes == 10 && _time.Seconds > 0))
                {
                    if (/*PanelBigPortal.chestProchentOpen != null &&*/ PanelBigPortal.chestProchentOpen != 0)
                    {
                        ObjDeltaTimeCloseChestPWM.SetActive(true);
                        deltaTimeCloseChestPWM.text = "Время открытия уменьшено до " + PanelBigPortal.chestProchentOpen + "%";
                    }
                }
                else
                {
                    if (/*PanelBigPortal.chestProchentOpen != null &&*/ PanelBigPortal.chestProchentOpen != 0)
                    {
                        ObjDeltaTimeChestPWM.SetActive(true);
                        deltaTimeChestPWM.text = "Время открытия уменьшено до " + PanelBigPortal.chestProchentOpen + "%";
                    }
                }
            }
        }
        else
        {
            ObjSlotLimitPWM.SetActive(PlayerManager.ChestList.Count > 3);
            NameChestPWM.text = "";
            StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsEmptyChest, value => { ChestIconPWM.sprite = value; }));
        }
    }

    public void HideUIGameOver()
    {
        rootPanel.SetActive(false);

        HideUIEvent();
    }

    public override bool NeedShow()
    {
        data = PlayerManager.DataSaveProgress;
        if (data.time > 0)
        {
            Debug.Log("ProgressSave need show");
            Initialization();
            return true;
        }
        return false;
    }

    IEnumerator StarShower(int star)
    {
        float timestart = Time.realtimeSinceStartup;
        int i = 0;

        StarsPanel.SetActive(true);

        if (star > 3)
        {
            StarsDontEarned[0].gameObject.SetActive(false);
            StarsDontEarned[2].gameObject.SetActive(false);
            StarEarned[0].gameObject.SetActive(false);
            StarEarned[1].gameObject.SetActive(false);
            StarEarned[2].gameObject.SetActive(false);
            StarEarned[3].gameObject.SetActive(true);

            yield return new WaitForSecondsRealtime(StarShowDelay);// StartCoroutine(TimeScaleManager.WaitForRealSeconds(StarShowDelay));
            StarsDontEarned[1].gameObject.SetActive(false);
        }
        else
        {
            StarEarned[3].gameObject.SetActive(false);
            while (true)
            {
                if (timestart + StarShowDelay < Time.realtimeSinceStartup)
                {
                    if (i >= star)
                        break;
                    StarEarned[i].gameObject.SetActive(true);
                    StarsDontEarned[i].gameObject.SetActive(false);
                    i++;

                    timestart = Time.realtimeSinceStartup;
                }
                yield return null;
            }
        }


        yield break;
    }
}
