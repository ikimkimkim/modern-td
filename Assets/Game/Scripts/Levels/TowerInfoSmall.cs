﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TDTK;
using Translation;
/// <summary>
/// Маленькая информация о башне которая показыается когда она выбрана 
/// </summary>
public class TowerInfoSmall : MonoBehaviour
{
    public Text Name;
    public Text Damage;
    public Text AttackSpeed;
    static TowerInfoSmall _instance;
    Transform _transform;
    Vector3 _shift = new Vector3(0, -90, 0);
    void Start()
    {
        _instance = this;
        _transform = this.transform;
        _hide();
    }
    void OnDestroy() { _instance = null; }
    public static void Show(UnitTower tower)
    {
        _instance._show(tower);
    }
    void _show(UnitTower tower)
    {
        Translator trans = TranslationEngine.Instance.Trans;

        var pos = Camera.main.WorldToScreenPoint(tower.transform.position);
        _transform.position = pos + _shift * GameObject.FindObjectOfType<Canvas>().scaleFactor;
        _instance.gameObject.SetActive(true);

        string[] name = tower.unitName.Split(';');
        if (name.Length > 1)
            _instance.Name.text = trans[name[0]] + " " + name[1];
        else
            _instance.Name.text = trans[name[0]];
        //  Debug.Log(tower.GetDamageMin());
        string DamageMin = (tower.GetDamageMin() > 10) ? ((int)tower.GetDamageMin()).ToString() : (Round(tower.GetDamageMin(), 1)).ToString();
        string DamageMax = (tower.GetDamageMax() > 10) ? ((int)tower.GetDamageMax()).ToString() : (Round(tower.GetDamageMax(), 1)).ToString();


        if (tower.GetDamageMin() != tower.GetDamageMax())
        {
            _instance.Damage.text = DamageMin + "-" + DamageMax;
        }
        else
        {
            _instance.Damage.text = DamageMin;
        }
        _instance.AttackSpeed.text = (Round(tower.GetCooldown(), 1)).ToString() + " " + trans["Game.Seconds"];

    }
    public static float Round(float value, int digits)
    {
        float mult = Mathf.Pow(10.0f, (float)digits);
        return Mathf.Round(value * mult) / mult;
    }
    public static void Hide()
    {
        _instance._hide();
    }
    void _hide()
    {
        _instance.gameObject.SetActive(false);
    }
}
