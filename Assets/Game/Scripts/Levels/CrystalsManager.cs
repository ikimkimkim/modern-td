﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

/// <summary>
/// Раздает кристаллы крипам при старте уровня 
/// </summary>
public class CrystalsManager : MonoBehaviour
{
    public int MaxCrystalGain;
    public int MinCrystalGain;
    public int CrystalGained = 0;
    public int MobileMaxCrystalsInLevel = 100;
    int _targetGain;
    int _unitAllCount;
    int _currentUnit = 0;
    int[] _unitsAllCrystals;
    ServerManager _server;
    PlayerManager _player;
    List<int> _crystalsGainedByLevel;
    public int WinBounty;

    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
#if UNITY_ANDROID || UNITY_IOS  
        WinBounty = GetMobileWinBounty();
        _crystalsGainedByLevel = _player.GetCrystalsGained();
        if (_crystalsGainedByLevel.Count > GameControl.GetLevelID() - 1)
        {
            Initialize((int)((MobileMaxCrystalsInLevel - _crystalsGainedByLevel[GameControl.GetLevelID() - 1]) * Random.Range(0.3f, 0.5f)), WinBounty, 0);
        }
        else
        {
            Initialize((int)(MobileMaxCrystalsInLevel * Random.Range(0.3f, 0.5f)), WinBounty, 0);
        }
#endif

#if( UNITY_WEBGL || UNITY_WEBPLAYER ) 
       /* _server = _player.GetComponent<ServerManager>();
        List<StringPair> extra = new List<StringPair>();
        extra.Add(new StringPair("level_id", GameControl.instance.levelID.ToString()));
        Crystals gainCrystals = new Crystals();
        StartCoroutine(_server.SaveLoadServer("onLevelStarted", extra, "", value => Initialize(((Crystals)StringSerializationAPI.Deserialize(gainCrystals.GetType(), value)))));*/
#endif
    }
    int GetMobileWinBounty()
    {
        int bounty = 0;
        MobileLevelCompletsData data = LoadMobileLevelCompletsData();
        if (data != null)
        {
            if (data.LevelComplets.ContainsKey(GameControl.GetLevelID()))
            {
                if (data.LevelComplets[GameControl.GetLevelID()] < 5)
                {
                    bounty = 10;
                }
                else
                {
                    bounty = 0;
                }
            }
            else
            {
                bounty = (100 + (GameControl.GetLevelID() % 100) * 10);
            }
        }
        else
        {
            bounty = (100 + (GameControl.GetLevelID() % 100) * 10);
        }

        return bounty;
    }

    void Initialize(int max, int win, int time)
    {
        InitializeDropCrystals(max);
        WinBounty = win;
        _player.Times = time;
    }
    void InitializeDropCrystals(int max)
    {
        _targetGain = max;
        //Debug.Log("Target Gain = " + _targetGain.ToString());
        _unitAllCount = CountAllUnits();
        _unitsAllCrystals = new int[_unitAllCount];
        int unitsWithCrystals = Mathf.Max(Random.Range((int)(_unitAllCount * 0.1f), (int)(_unitAllCount * 0.2f)), 4); // 4 - минимальное количество юнитов на которое распределяются кристаллы

        int[] randoms = new int[unitsWithCrystals];
        int summ = 0;
        for (int i = 0; i < randoms.Length; i++)
        {
            randoms[i] = Random.Range(1, 10);
            summ += randoms[i];
        }
        for (int i = 0; i < unitsWithCrystals; i++)
        {
            float crystals = _targetGain * (randoms[i] / (float)summ);
            _unitsAllCrystals[Random.Range(0, _unitAllCount)] += (int)crystals;
        }
    }
    int CountAllUnits()
    {
        int count = 0;
        var waves = SpawnManager.instance.waveList;
        for (int i = 0; i < waves.Count; i++)
        {
            for (int q = 0; q < waves[i].subWaveList.Count; q++)
            {
                count += waves[i].subWaveList[q].count;
            }
        }
        // Debug.Log("CountAllUnits  " + count);
        return count;
    }
    void OnEnable()
    {
        Unit.onDestroyedE += OnUnitDestroy;
        SpawnManager.onSpawnUnitE += OnUnitSpawn;
        GameControl.onGameOverE += OnGameOver;
    }
    void OnDisable()
    {
        Unit.onDestroyedE -= OnUnitDestroy;
        SpawnManager.onSpawnUnitE -= OnUnitSpawn;
        GameControl.onGameOverE -= OnGameOver;
    }
    void OnUnitDestroy(Unit unit)
    {
        if (unit.Crystals > 0)
        {
            CrystalGained += unit.Crystals;
            _player.AddCrystals(unit.Crystals, GameControl.instance.levelID);
            new TextOverlay(unit.thisT.position, unit.Crystals.ToString(), new Color(0.04f, 0.56f, 0.92f, 1f), true);
            unit.Crystals = 0;

        }
    }
    void OnUnitSpawn(Unit unit)
    {
        if (_currentUnit < _unitAllCount)
        {
            unit.Crystals += _unitsAllCrystals[_currentUnit];
            _currentUnit++;
        }
    }

    void SaveMobileLevelCompletsData(MobileLevelCompletsData data)
    {
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        CryptoPlayerPrefs.Save("levelComplets", data.GetType(), data);
#endif
#if UNITY_EDITOR
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/levelComplets.d", FileMode.OpenOrCreate);
        Debug.Log(StringSerializationAPI.Serialize(typeof(MobileLevelCompletsData), data));
        binaryFormatter.Serialize(file, data);
        file.Close();
#endif
    }

    MobileLevelCompletsData LoadMobileLevelCompletsData()
    {
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        if (CryptoPlayerPrefs.HasKey("levelComplets"))
        {
            return (MobileLevelCompletsData)CryptoPlayerPrefs.Load("levelComplets", typeof(MobileLevelCompletsData));
        }
#endif
#if UNITY_EDITOR
        if (File.Exists(Application.persistentDataPath + "/levelComplets.d"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/levelComplets.d", FileMode.Open);
            MobileLevelCompletsData data = (MobileLevelCompletsData)bf.Deserialize(file);
            file.Close();
            return data;
        }
#endif
        return null;
    }

    void OnGameOver(int starts,ResponseData prizes)
    {
#if UNITY_ANDROID || UNITY_IOS
        if (starts >= 0)
        {
            _player.AddCrystals(WinBounty, 0); //тут ноль чтобы не засчитывалось в список выданных кристаллов// GameControl.instance.levelID);
            MobileLevelCompletsData data = LoadMobileLevelCompletsData();
            if (data != null)
            {
                if (!data.LevelComplets.ContainsKey(GameControl.GetLevelID()))
                {
                    data.LevelComplets.Add(GameControl.GetLevelID(), 1);
                }
                else
                {
                    data.LevelComplets[GameControl.GetLevelID()]++;
                }
                SaveMobileLevelCompletsData(data);
            }
            else
            {
                data = new MobileLevelCompletsData();
                data.LevelComplets.Add(GameControl.GetLevelID(), 1);
                SaveMobileLevelCompletsData(data);
            }
        }
#endif
    }
}
