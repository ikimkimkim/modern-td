﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class CrystalsUI : MonoBehaviour
{
   
    public RectTransform BuyButton;
    PlayerManager _playerManger;
    Text _text;
    void Start()
    {
#if UNITY_ANDROID || UNITY_IOS
        BuyButton.localScale *= 1.2f;
        GetComponent<RectTransform>().localPosition += new Vector3(-15,0,0);
#endif
        _playerManger = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
        _text = GetComponentInChildren<Text>();
        _text.text = _playerManger.GetCrystals().ToString();
        if (SceneManager.GetActiveScene().buildIndex == 3) // Application.loadedLevel == 3)
        {
            gameObject.SetActive(false);
        }
    }
    void OnEnable()
    {
        PlayerManager.OnCrystalsChangedE += OnCrystalsChanged;
    }
    void OnDisable()
    {
        PlayerManager.OnCrystalsChangedE -= OnCrystalsChanged;
    }
    void OnCrystalsChanged(int currentCrystals)
    {
        _text.text = currentCrystals.ToString();
    }
}
