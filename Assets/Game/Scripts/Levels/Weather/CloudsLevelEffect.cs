﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;
using UnityEngine.UI;

public class CloudsLevelEffect : MonoBehaviour {

    [System.Serializable]
    public struct CloudUnit
    {
        public Sprite image;
        [Range(0f,1f)]
        public float chanceSelect;        
    }

    public GameObject prefabImageCound;

    public float Frequency;
    public Vector2 TransparencyLimits;
    public Vector2 SpeedLimits;
    private Vector2 Direction;

    public CloudUnit[] Clouds;

    private List<Transform> CloudsObjs;
    private float lastTimeInstance;

    public List<int> LevelIDActive;

    void Start ()
    {
        if(LevelIDActive.Contains(GameControl.GetLevelID())==false)
        {
            gameObject.SetActive(false);
            return;
        }

        CloudsObjs = new List<Transform>();
        lastTimeInstance = Time.time;
        Direction.x = Random.Range(-1f, 1f);
        Direction.y = Random.Range(-1f, 1f);
        Direction = Direction.normalized;

	}
	

	void Update ()
    {
        InstanceCound();
	}

    

    private void InstanceCound()
    {
        if (Time.time - lastTimeInstance > Frequency)
        {
            CloudUnit unit = getCloudUnit();
            GameObject unitObj = getCloudObj();

            Image img = unitObj.GetComponent<Image>();
            img.sprite = unit.image;
            img.color = new Color(1f, 1f, 1f, Random.Range(TransparencyLimits.x, TransparencyLimits.y));

            Vector2 offset = new Vector2( (0.5f - Direction.x*0.8f) * Screen.width ,
                (0.5f - Direction.y*0.8f) * Screen.height);

            offset += new Vector2(-Direction.y,Direction.x) * Random.Range(-Screen.width/2f, Screen.width/2f);



            unitObj.GetComponent<RectTransform>().sizeDelta = new Vector2(unit.image.rect.width, unit.image.rect.height);

            unitObj.transform.position = new Vector3(offset.x, offset.y, 0);
            unitObj.transform.rotation = new Quaternion(0, Random.value > 0.5f ? 0 : 1, 0, 0);
            StartCoroutine(UpdateCloud(unitObj, unitObj.transform, Random.Range(SpeedLimits.x, SpeedLimits.y)));
            lastTimeInstance = Time.time;
        }
    }

    private CloudUnit getCloudUnit()
    {
        float rnd = Random.Range(0f,100f)/100f;
        float counter = 0;
        for (int i = 0; i < Clouds.Length; i++)
        {
            counter += Clouds[i].chanceSelect;
            if (rnd < counter)
                return Clouds[i];
        }
        return new CloudUnit();
    }

    private GameObject getCloudObj()
    {
        for (int i = 0; i < CloudsObjs.Count; i++)
        {
            if (CloudsObjs[i].gameObject.activeSelf == false)
                return CloudsObjs[i].gameObject;
        }

        GameObject cloud = Instantiate(prefabImageCound, transform);
        cloud.SetActive(false);
        CloudsObjs.Add(cloud.transform);
        return cloud;
    }


    private IEnumerator UpdateCloud(GameObject cloudObj,Transform cloudT,float speed)
    {
        cloudObj.SetActive(true);
        float distance = 0;
        Vector2 speedCloud;
        while (cloudObj.activeSelf)
        {
            speedCloud = new Vector2(Direction.x * speed * Screen.width, Direction.y * speed * Screen.height);
            distance += speedCloud.magnitude * Time.deltaTime;
            cloudT.Translate(speedCloud * Time.deltaTime, Space.World);

            if (distance> Screen.width+ Screen.height)
            {
                cloudObj.SetActive(false);
            }
            yield return null;
        }
        yield break;
    }

}
