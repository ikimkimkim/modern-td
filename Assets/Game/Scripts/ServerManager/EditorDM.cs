﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using TDTK;

public class EditorDM : DataManager
{

    private const float delayResponce = 1.5f;
    
    public override void Initialization()
    {
        MyLog.Log("EditorDM Initialization complite");
    }

    public override void getPlayerData()
    {
        UIWaitPanel.Show();
        PlayerManager._instance.StartCoroutine(DelayPlayerData(PlayerManager._instance._playerData));
    }

    private IEnumerator DelayPlayerData(PlayerData data)
    {
        if (delayResponce > 0)
            yield return new WaitForSecondsRealtime(delayResponce);


        PlayerManager.SetPlayerData(data);

        UIWaitPanel.Hide();
        yield break;
    }

    public override void LoadFriendsPlayer()
    {
        throw new NotImplementedException();
    }

    public override void HelpedFriends(Action<FriendsData, int> result, int status)
    {
        FriendsData fd = new FriendsData(new int[] { 1, 2, 3, 4,5,6,7,8,9,10,11,12,13 });

        fd.can_take_help = 1;

        result(fd, status);
    }


    public override void ChangeSettings(string name, string value, Action<ResponseData> result)
    {
        Debug.LogFormat("ChangeSettings {0}:{1}",name,value);
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void StartLevel(int level_id, int diff, List<int[]> towers, List<int[]> bonus, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        string json = StringSerializationAPI.Serialize(towers.GetType(), towers);
        string json2 = StringSerializationAPI.Serialize(bonus.GetType(), bonus);
        Debug.LogFormat("StartLevel tower:{0} bonus:{1} level_id:{2}",json,json2,level_id );
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        PlayerData pd= PlayerManager._instance._playerData;
        foreach (var t in pd.towers)
        {
            for (int i = 0; i < towers.Count; i++)
            {
                if (t.Value.type == towers[i][0] && t.Value.rarity == towers[i][1])
                {
                    t.Value.selected = 1;
                    break;
                }
                else
                    t.Value.selected = 0;
            }
            
        }
        rp.stars_config = new Dictionary<string, int>();
        rp.stars_config.Add("1", 5);
        rp.stars_config.Add("2", 10);
        rp.stars_config.Add("3", 15);
        rp.user = pd;
        rp.error = new ErrorData();
        rp.error.code = "123";
        rp.error.text = "Error new text";
        //result(rp);
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void EndLevel(int level_id, int diff, int stars, int rnd, float avgfps, int map_id, float scores, float hp_left, int time, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.Log(String.Format("EndLevel -> id {0}, diff {1}, stars {2}, rnd {3}, time {4}, hp {5}, user_id {6}, avgfps {7}, map_id {8}, scores {9}",
            level_id, diff, stars, rnd, time.ToString(), hp_left.ToString(),
            0, avgfps, map_id, scores));

        ResponseData rp = new ResponseData();
        rp.user = PlayerManager._instance._playerData;
        rp.user.has_not_ended_session = new SaveProgressData();
        rp.ok = 1;
        if(stars!=-1)
        {
            rp.prizes = new ResponseData.PrizesData();
            rp.prizes.chest = 1;
            rp.prizes.gold = 2;
            rp.prizes.crystals = 3;
        }
        else
        {
            rp.prizes = new ResponseData.PrizesData();
            rp.prizes.chest = 0;
            rp.prizes.gold = 0;
            rp.prizes.crystals = 0;
        }

        rp.prizes2 = new ResponseData.PrizesTower();
        rp.prizes2.type = 504;
        rp.prizes2.rarity = 2;
        rp.prizes2.count = 1;
        //result(rp);
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void StartOpenChest(int id_chest, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.Log("Start Open Chest!"+id_chest);
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        rp.user.chests.Find(i => i.id == id_chest.ToString()).status = 1;
        //rp.user.chests.Find(i => i.id == id_chest.ToString()).time_to_open = 20;
        //result(rp);
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void FastOpenChest(int id_chest, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        rp.user.chests.RemoveAll(i => i.id == id_chest.ToString());

        rp.prizes = new ResponseData.PrizesData();
        rp.prizes.gold = 1;
        rp.prizes.crystals = 2;
        rp.prizes.towers = new ResponseData.PrizesTower[3];
        rp.prizes.towers[0] = new ResponseData.PrizesTower();
        rp.prizes.towers[0].count = 2;
        rp.prizes.towers[0].rarity = 1;
        rp.prizes.towers[0].type = 1;


        rp.prizes.towers[1] = new ResponseData.PrizesTower();
        rp.prizes.towers[1].count = 1;
        rp.prizes.towers[1].rarity = 1;
        rp.prizes.towers[1].type = 4;

        rp.prizes.towers[2] = new ResponseData.PrizesTower();
        rp.prizes.towers[2].count = 3;
        rp.prizes.towers[2].rarity = 1;
        rp.prizes.towers[2].type = 9;

        rp.prizes.craft_resources = new Dictionary<string, int>();
        rp.prizes.craft_resources.Add("1", UnityEngine.Random.Range(50, 100));
        rp.prizes.craft_resources.Add("2", UnityEngine.Random.Range(25, 50));
        rp.prizes.craft_resources.Add("3", UnityEngine.Random.Range(5, 25));
        rp.prizes.craft_resources.Add("4", UnityEngine.Random.Range(0, 5));

        //result(rp);
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void TakeChestPrizes(int id_chest, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        rp.user.chests.RemoveAll(i => i.id == id_chest.ToString());
        rp.prizes = new ResponseData.PrizesData();
        rp.prizes.gold = 1;
        rp.prizes.crystals = 2;
        rp.prizes.towers = new ResponseData.PrizesTower[4];
        rp.prizes.towers[0] = new ResponseData.PrizesTower();
        rp.prizes.towers[0].count = 2;
        rp.prizes.towers[0].rarity = 2;
        rp.prizes.towers[0].type = 1;

        rp.prizes.towers[1] = new ResponseData.PrizesTower();
        rp.prizes.towers[1].count = 1;
        rp.prizes.towers[1].rarity = 1;
        rp.prizes.towers[1].type = 4;

        

        for (int i = 2; i < 4; i++)
        {
            rp.prizes.towers[i] = new ResponseData.PrizesTower();
            rp.prizes.towers[i].count = 3;
            rp.prizes.towers[i].rarity = 1;
            rp.prizes.towers[i].type = 9;
        }
        rp.prizes.craft_resources = new Dictionary<string, int>();
        rp.prizes.craft_resources.Add("1", 10);

        //result(rp);
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void UpgradeTower(int type, int rarity, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.error = new ErrorData();
        rp.error.code = " no_gold ";
        rp.user = PlayerManager._instance._playerData;

        rp.user.towers[(rarity) +"_"+(type)].card_level++;
        rp.user.towers[(rarity) + "_" + (type)].count -= 1;
        //result(rp);
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void SellTower(int type, int rarity, int count, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        rp.user.towers[(rarity + "_"+ type)].count -= count;
        rp.user.Gold += 10;
        rp.prizes = new ResponseData.PrizesData();
        rp.prizes.craft = new Dictionary<string, int>();

        for (int i = 1; i < 5; i++)
        {
            rp.prizes.craft.Add(i.ToString(), UnityEngine.Random.Range(0, 100));
        }

        //result(rp);
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void BuyForCrystals(string item_id, Action<ResponseData> result) 
    {
        UIWaitPanel.Show();
        Debug.LogFormat("BuyForCrystals, item_id {0}", item_id);
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.info = new ResponseData.Info();
        rp.info.ok = 1;
        rp.error = new ErrorData();
        rp.error.code = "error";
        rp.error.text = "Какая то ошибка";
        rp.user = PlayerManager._instance._playerData;


        //result(rp);
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void GreatAdvTop(int type, int map_id, int page, Action<TopData> result)
    {
      string data = "{ \"result\":{ \"top_data\":{ \"pages\":[\"1 - 20\",\"21 - 40\",\"41 - 60\",\"61 - 80\",\"81 - 100\"],\"top\":[{\"user_id\":\"243045\",\"datetime\":\"2017-05-24 14:39:40\",\"seconds\":\"666\",\"level\":\"25\",\"hp_left\":\"2\",\"place\":1,\"vk_user\":{\"id\":\"243045\",\"name\":\"Владимир Прибитковский\",\"ava\":\"http://cs625325.vk.me/v625325045/43efb/OkwoHZru3i0.jpg\"}},{\"user_id\":\"2514414\",\"datetime\":\"2017-05-24 14:40:54\",\"seconds\":\"726\",\"level\":\"25\",\"hp_left\":\"3\",\"place\":2,\"vk_user\":{\"id\":\"2514414\",\"name\":\"Ирина Блинова\",\"ava\":\"http://cs621727.vk.me/v621727414/736d/G-ZVzuiJ_XQ.jpg\"}},{\"user_id\":\"2231014\",\"datetime\":\"2017-05-24 14:40:54\",\"seconds\":\"840\",\"level\":\"25\",\"hp_left\":\"2\",\"place\":3,\"vk_user\":{\"id\":\"2231014\",\"name\":\"Дмитрий Крендясов\",\"ava\":\"http://cs630720.vk.me/v630720014/18789/CL3l4dlRVqY.jpg\"}},{\"user_id\":\"3320271\",\"datetime\":\"2017-05-24 14:40:54\",\"seconds\":\"555\",\"level\":\"25\",\"hp_left\":\"2\",\"place\":4,\"vk_user\":{\"id\":\"3320271\",\"name\":\"Михаил Архангельский\",\"ava\":\"https://pp.vk.me/c633323/v633323271/bc8d/1Q04DvkEtgM.jpg\"}}]},\"user_data\":{}}}";
        TopData rd = (TopData)StringSerializationAPI.Deserialize(typeof(TopData), data);
        result(rd);
    }

    public override void GreatDayTop(int type, int is_last, int page, Action<TopData> result)
    {
        string data = "{\"result\":{\"top_data\":{ \"info\":{\"start\":\"2018 - 02 - 22 00:00:00\",\"end\":\"2018 - 02 - 23 23:59:59\",\"map_id\":1},\"pages\":[\"1 - 20\",\"21 - 40\",\"41 - 60\",\"61 - 80\"],\"top\":[{\"scores\":\"99\",\"date\":\"2018 - 02 - 23 15:04:59\",\"map_id\":\"1\",\"place\":1,\"vk_user\":{\"id\":\"963609\",\"name\":\"Анна Суворова\",\"ava\":\"http://cs630822.vk.me/v630822609/1d2c0/NxoFLiozugY.jpg\"}},{\"scores\":\"97\",\"date\":\"2018 - 02 - 23 15:04:59\",\"map_id\":\"1\",\"place\":2,\"vk_user\":{\"id\":\"2563134\",\"name\":\"Евгения Мельникова\",\"ava\":\"http://cs630719.vk.me/v630719134/f749/GDymFRXMotE.jpg\"}},{\"scores\":\"97\",\"date\":\"2018 - 02 - 23 15:04:59\",\"map_id\":\"1\",\"place\":3,\"vk_user\":{\"id\":\"2231014\",\"name\":\"Дмитрий Крендясов\",\"ava\":\"http://cs630720.vk.me/v630720014/18789/CL3l4dlRVqY.jpg\"}},{\"scores\":\"96\",\"date\":\"2018 - 02 - 23 11:40:20\",\"map_id\":\"1\",\"place\":4,\"vk_user\":{\"id\":\"199979\",\"name\":\"Евгений Петров\",\"ava\":\"http://cs627917.vk.me/v627917979/338dd/TbH38WtwlyI.jpg\"}},{\"scores\":\"94\",\"date\":\"2018 - 02 - 23 15:04:59\",\"map_id\":\"1\",\"place\":5,\"vk_user\":{\"id\":\"2345825\",\"name\":\"Антон Бурко\",\"ava\":\"http://cs627224.vk.me/v627224825/41f30/NW9OT6lw8Wc.jpg\"}},{\"scores\":\"92\",\"date\":\"2018 - 02 - 23 14:58:23\",\"map_id\":\"1\",\"place\":6,\"vk_user\":{\"id\":\"1252025\",\"name\":\"Константин Шамаев\",\"ava\":\"http://cs616827.vk.me/v616827025/16c5e/SB5hlovF2D8.jpg\"}},{\"scores\":\"92\",\"date\":\"2018 - 02 - 23 15:04:59\",\"map_id\":\"1\",\"place\":7,\"vk_user\":{\"id\":\"1341569\",\"name\":\"Petr Kolesnik\",\"ava\":\"http://cs627225.vk.me/v627225569/13d52/I2gVuTvb9Ck.jpg\"}},{\"scores\":\"90\",\"date\":\"2018 - 02 - 23 14:58:23\",\"map_id\":\"1\",\"place\":8,\"vk_user\":{\"id\":\"299493\",\"name\":\"Виктор Загорский\",\"ava\":\"https://pp.vk.me/c623120/v623120493/3f87c/_kZfNIXxG10.jpg\"}},{\"scores\":\"89\",\"date\":\"2018 - 02 - 23 15:04:59\",\"map_id\":\"1\",\"place\":9,\"vk_user\":{\"id\":\"467347\",\"name\":\"Илья Донкин\",\"ava\":\"http://cs633530.vk.me/v633530347/126a1/a3M5vomcVCw.jpg\"}},{\"scores\":\"88\",\"date\":\"2018 - 02 - 23 14:58:23\",\"map_id\":\"1\",\"place\":10,\"vk_user\":{\"id\":\"915820\",\"name\":\"Анфиса Сузи\",\"ava\":\"http://cs7001.vk.me/v7001624/a105/l_kuN0nKKG4.jpg\"}},{\"scores\":\"87\",\"date\":\"2018 - 02 - 23 15:04:59\",\"map_id\":\"1\",\"place\":11,\"vk_user\":{\"id\":\"1188431\",\"name\":\"Александр Дейч\",\"ava\":\"http://cs629508.vk.me/v629508431/2de14/-J8llJ4HEg8.jpg\"}},{\"scores\":\"86\",\"date\":\"2018 - 02 - 23 14:58:23\",\"map_id\":\"1\",\"place\":12,\"vk_user\":{\"id\":\"2477485\",\"name\":\"Марина Сапрыкина\",\"ava\":\"http://cs622929.vk.me/v622929485/12866/fZsjlJdRcEo.jpg\"}},{\"scores\":\"84\",\"date\":\"2018 - 02 - 23 15:04:59\",\"map_id\":\"1\",\"place\":13,\"vk_user\":{\"id\":\"1289613\",\"name\":\"Кирилла Травокуров\",\"ava\":\"https://pp.userapi.com/c618/u1289613/d_0435ad68.jpg\"}},{\"scores\":\"83\",\"date\":\"2018 - 02 - 23 14:58:23\",\"map_id\":\"1\",\"place\":14,\"vk_user\":{\"id\":\"1586380\",\"name\":\"Anatoly Kurchavy\",\"ava\":\"http://cs323221.vk.me/v323221380/945c/GnnqjzcSuU0.jpg\"}},{\"scores\":\"80\",\"date\":\"2018 - 02 - 23 11:42:25\",\"map_id\":\"1\",\"place\":15,\"vk_user\":{\"id\":\"1110311\",\"name\":\"Даниил Безносов\",\"ava\":\"http://cs4485.vk.me/u1110311/d_1f3992fc.jpg\"}},{\"scores\":\"80\",\"date\":\"2018 - 02 - 23 14:58:23\",\"map_id\":\"1\",\"place\":16,\"vk_user\":{\"id\":\"2253984\",\"name\":\"Сергей Мишанин\",\"ava\":\"http://cs604519.vk.me/v604519984/f36/cqXqqT6KCoA.jpg\"}},{\"scores\":\"78\",\"date\":\"2018 - 02 - 23 14:58:23\",\"map_id\":\"1\",\"place\":17,\"vk_user\":{\"id\":\"2514414\",\"name\":\"Ирина Блинова\",\"ava\":\"http://cs621727.vk.me/v621727414/736d/G - ZVzuiJ_XQ.jpg\"}},{\"scores\":\"78\",\"date\":\"2018 - 02 - 23 14:58:23\",\"map_id\":\"1\",\"place\":18,\"vk_user\":{\"id\":\"216413\",\"name\":\"Виктор Огарёв\",\"ava\":\"http://cs623122.vk.me/v623122413/9b17/mKvGIG5I0BI.jpg\"}},{\"scores\":\"78\",\"date\":\"2018 - 02 - 23 14:58:23\",\"map_id\":\"1\",\"place\":19,\"vk_user\":{\"id\":\"1931204\",\"name\":\"Алексей Снетков\",\"ava\":\"http://cs5946.vk.me/v5946204/2a0/-5CZ5UsdJ50.jpg\"}},{\"scores\":\"78\",\"date\":\"2018 - 02 - 23 14:58:23\",\"map_id\":\"1\",\"place\":20,\"vk_user\":{\"id\":\"1147997\",\"name\":\"Иван Донских\",\"ava\":\"http://cs616830.vk.me/v616830997/13034/h4A9xqNF9Mc.jpg\"}}]},\"user_data\":{\"e\":0,\"place\":61,\"place_info\":{\"scores\":\"4\",\"date\":\"2018 - 02 - 22 23:47:32\"}}}}";
        // "{ \"result\":{ \"top_data\":{ \"pages\":[\"1 - 20\",\"21 - 40\",\"41 - 60\",\"61 - 80\",\"81 - 100\"],\"top\":[{\"user_id\":\"243045\",\"datetime\":\"2017-05-24 14:39:40\",\"seconds\":\"666\",\"level\":\"25\",\"hp_left\":\"2\",\"place\":1,\"vk_user\":{\"id\":\"243045\",\"name\":\"Владимир Прибитковский\",\"ava\":\"http://cs625325.vk.me/v625325045/43efb/OkwoHZru3i0.jpg\"}},{\"user_id\":\"2514414\",\"datetime\":\"2017-05-24 14:40:54\",\"seconds\":\"726\",\"level\":\"25\",\"hp_left\":\"3\",\"place\":2,\"vk_user\":{\"id\":\"2514414\",\"name\":\"Ирина Блинова\",\"ava\":\"http://cs621727.vk.me/v621727414/736d/G-ZVzuiJ_XQ.jpg\"}},{\"user_id\":\"2231014\",\"datetime\":\"2017-05-24 14:40:54\",\"seconds\":\"840\",\"level\":\"25\",\"hp_left\":\"2\",\"place\":3,\"vk_user\":{\"id\":\"2231014\",\"name\":\"Дмитрий Крендясов\",\"ava\":\"http://cs630720.vk.me/v630720014/18789/CL3l4dlRVqY.jpg\"}},{\"user_id\":\"3320271\",\"datetime\":\"2017-05-24 14:40:54\",\"seconds\":\"555\",\"level\":\"25\",\"hp_left\":\"2\",\"place\":4,\"vk_user\":{\"id\":\"3320271\",\"name\":\"Михаил Архангельский\",\"ava\":\"https://pp.vk.me/c633323/v633323271/bc8d/1Q04DvkEtgM.jpg\"}}]},\"user_data\":{}}}";
        TopData rd = (TopData)StringSerializationAPI.Deserialize(typeof(TopData), data);
        result(rd);
    }


    public override void getDayTop(int type, Action<ResponseDataTopDayPrize> result)
    {
        UIWaitPanel.Show();
        Debug.Log(string.Format("get day top prize, type {0} ", type));
        ResponseDataTopDayPrize rp = new ResponseDataTopDayPrize();
        rp.ok = 0;

        rp.error = new ResponseDataTopDayPrize.ErrorData();
        rp.error.code = "";
        rp.error.text = "";

        rp.user = PlayerManager._instance._playerData;
        //rp.user.chests.RemoveAll(i => i.id == id_chest.ToString());
        rp.prizes = new ResponseDataTopDayPrize.PrizesData();
        rp.prizes.gold = 1;
        rp.prizes.crystals = 2;
        rp.prizes.chest = new ResponseDataTopDayPrize.PrizesChest();
        rp.prizes.chest.towers = new ResponseDataTopDayPrize.PrizesTower[4];
        rp.prizes.chest.towers[0] = new ResponseDataTopDayPrize.PrizesTower();
        rp.prizes.chest.towers[0].count = 2;
        rp.prizes.chest.towers[0].rarity = 2;
        rp.prizes.chest.towers[0].type = 1;

        rp.prizes.chest.towers[1] = new ResponseDataTopDayPrize.PrizesTower();
        rp.prizes.chest.towers[1].count = 1;
        rp.prizes.chest.towers[1].rarity = 1;
        rp.prizes.chest.towers[1].type = 4;



        for (int i = 2; i < 4; i++)
        {
            rp.prizes.chest.towers[i] = new ResponseDataTopDayPrize.PrizesTower();
            rp.prizes.chest.towers[i].count = 3;
            rp.prizes.chest.towers[i].rarity = 1;
            rp.prizes.chest.towers[i].type = 9;
        }

        //result(rp);
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void BuyPerk(int item_id, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.info = new ResponseData.Info();
        rp.info.ok = 1;
        rp.error = new ErrorData();
        rp.error.code = "error";
        rp.error.text = "Какая то ошибка";
        PlayerManager._instance._playerData.PurchasedUpgradesIDs.Add(item_id);
        rp.user = PlayerManager._instance._playerData;

        int[] ids = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 30, 31, 32, 33, 34, 35, 36, 37, 38 };
        rp.user.stars_grade_choice.choice = new int[] { ids[UnityEngine.Random.Range(0, ids.Length)], ids[UnityEngine.Random.Range(0, ids.Length)] };

        //result(rp);
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void CancelPerk(int[] item_ids, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.info = new ResponseData.Info();
        rp.info.ok = 1;
        rp.error = new ErrorData();
        rp.error.code = "error";
        rp.error.text = "Какая то ошибка";
        for (int i = 0; i < item_ids.Length; i++)
        {
            PlayerManager._instance._playerData.PurchasedUpgradesIDs.Remove(item_ids[i]);
        }

        rp.user = PlayerManager._instance._playerData;
        //result(rp);
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void RefrechPerk(Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.info = new ResponseData.Info();
        rp.info.ok = 1;
        rp.error = new ErrorData();
        rp.error.code = "error";
        rp.error.text = "Какая то ошибка";
        PlayerManager._instance._playerData.PurchasedUpgradesIDs.Clear();
        rp.user = PlayerManager._instance._playerData;
        //result(rp);
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void ReducingOpenTimeChest(int id_chest, float timeleft, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        //result(rp);
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void GetEventGift(int type, Action<ResponseDataTopDayPrize> result)
    {
        UIWaitPanel.Show();
        ResponseDataTopDayPrize rp = new ResponseDataTopDayPrize();
        rp.ok = 1;
        PlayerManager._instance._playerData.event_gifts.Remove(type.ToString());
        rp.user = PlayerManager._instance._playerData;





        rp.prizes = new ResponseDataTopDayPrize.PrizesData();
        rp.prizes.gold = 10;
        rp.prizes.crystals = 20;
        rp.prizes.chest = new ResponseDataTopDayPrize.PrizesChest();
        rp.prizes.chest.towers = new ResponseDataTopDayPrize.PrizesTower[4];
        rp.prizes.chest.towers[0] = new ResponseDataTopDayPrize.PrizesTower();
        rp.prizes.chest.towers[0].count = 2;
        rp.prizes.chest.towers[0].rarity = 1;
        rp.prizes.chest.towers[0].type = 4;

        rp.prizes.chest.towers[1] = new ResponseDataTopDayPrize.PrizesTower();
        rp.prizes.chest.towers[1].count = 1;
        rp.prizes.chest.towers[1].rarity = 5;
        rp.prizes.chest.towers[1].type = 1;


        rp.prizes.chest.towers[2] = new ResponseDataTopDayPrize.PrizesTower();
        rp.prizes.chest.towers[2].count = 3;
        rp.prizes.chest.towers[2].rarity = 5;
        rp.prizes.chest.towers[2].type = 2;


        rp.prizes.chest.towers[3] = new ResponseDataTopDayPrize.PrizesTower();
        rp.prizes.chest.towers[3].count = 4;
        rp.prizes.chest.towers[3].rarity = 5;
        rp.prizes.chest.towers[3].type = 6;

        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
        //result(rp);
    }


    public override void CreateUpgradeCard(int type, int rarity, int count, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("CreateUpgradeCard, type {0}, rarity {1}, count {2}", type, rarity, count);
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        if(rp.user.towers.ContainsKey(rarity + "_" + type))
            rp.user.towers[rarity + "_" + type].count += count;
        else
        {
            TowersData towers = new TowersData();
            towers.name = "Craft card";
            towers.count = count;
            towers.type = type;
            towers.rarity = rarity;
            rp.user.towers.Add(rarity + "_" + type, towers);
        }
        rp.user.craft_resources["1"] -= 10;
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }



    public override void ApplyUpgradeCard(int type, int rarity, int craft_type, int craft_rarity, int param_id, List<int[]> bonus, Action<ResponseData> result)
    {
        var json = StringSerializationAPI.Serialize(bonus.GetType(), bonus);
        Debug.Log("StartLevel bonus json:" + json);

        Debug.LogFormat("CreateUpgradeCard, type {0}, rarity {1}, craft_type {2}, craft_rarity {3}, param_id {4}, bonus {5} ", type, rarity, craft_type, craft_rarity, param_id, json);

        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void SaveProgressUnlimited(int rnd, int level_id, int diff, string json_data, Action<ResponseData> result)
    {
        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }


    public override void DestroyChest(int id_chest, int card_rarity, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        ResponseData rp = new ResponseData();

        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        rp.prizes = new ResponseData.PrizesData();
        rp.prizes.craft = new Dictionary<string, int>();
        rp.prizes.craft.Add("1", UnityEngine.Random.Range(50, 100));
        rp.prizes.craft.Add("2", UnityEngine.Random.Range(25, 50));
        rp.prizes.craft.Add("3", UnityEngine.Random.Range(5, 25));
        rp.prizes.craft.Add("4", UnityEngine.Random.Range(0, 5));

        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }


    public override void SaleResources(int id_sale_res, int id_buy_res, int count_buy, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("SaleResources, id_sale_res {0}, id_buy_res {1}, count {2}", id_sale_res, id_buy_res, count_buy);
        ResponseData rp = new ResponseData();

        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        int add = Mathf.RoundToInt(Mathf.Pow(2, id_sale_res - id_buy_res) * count_buy);
        rp.user.craft_resources[id_sale_res.ToString()] -= count_buy;

        rp.user.craft_resources[id_buy_res.ToString()] += add;

        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }


    public override void EnergyRecovery(int card_rarity, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("EnergyRecovery, card_rarity {0}, ", card_rarity);
        ResponseData rp = new ResponseData();

        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        rp.user.Energy[0] += 2;

        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void OpenChest(int id_chest, int card_rarity, Action<ResponseData> result)
    {

        UIWaitPanel.Show();
        Debug.LogFormat("OpenChest, id_chest {0}, rarity {1}", id_chest, card_rarity);

        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        rp.user.chests.RemoveAll(i => i.id == id_chest.ToString());

        rp.prizes = new ResponseData.PrizesData();
        rp.prizes.gold = 1;
        rp.prizes.crystals = 2;
        rp.prizes.towers = new ResponseData.PrizesTower[3];
        rp.prizes.towers[0] = new ResponseData.PrizesTower();
        rp.prizes.towers[0].count = 2;
        rp.prizes.towers[0].rarity = 1;
        rp.prizes.towers[0].type = 1;


        rp.prizes.towers[1] = new ResponseData.PrizesTower();
        rp.prizes.towers[1].count = 1;
        rp.prizes.towers[1].rarity = 1;
        rp.prizes.towers[1].type = 4;

        rp.prizes.towers[2] = new ResponseData.PrizesTower();
        rp.prizes.towers[2].count = 3;
        rp.prizes.towers[2].rarity = 1;
        rp.prizes.towers[2].type = 9;


        rp.prizes.craft_resources = new Dictionary<string, int>();
        rp.prizes.craft_resources.Add("1", UnityEngine.Random.Range(50, 100));
        rp.prizes.craft_resources.Add("2", UnityEngine.Random.Range(25, 50));
        rp.prizes.craft_resources.Add("3", UnityEngine.Random.Range(5, 25));
        rp.prizes.craft_resources.Add("4", UnityEngine.Random.Range(0, 5));

        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void DecreaseOpenTimeChest(int id_chest, int card_rarity, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("DecreaseOpenTimeChest, id {0}, rarity {1}", id_chest, card_rarity);

        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;

        var c = rp.user.chests.Find(i => i.id == id_chest.ToString());
        c.time_to_open = Mathf.RoundToInt(c.time_to_open * 0.8f);

        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    public override void UpgradeRarityChest(int id_chest, int card_rarity, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("UpgradeRarityChest,  id {0}, rarity {1}", id_chest, card_rarity);

        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;

        var c = rp.user.chests.Find(i => i.id == id_chest.ToString());
        c.type_id = (int.Parse(c.type_id) + 1).ToString();

        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }


    public override void StarsGradeVsCards(Action<ResponseData> result)
    {
        int[] ids = new int[] { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,22,23,24,25,26,27,28,30,31,32,33,34,35,36,37,38 }; 
        Debug.LogFormat("StarsGradeVsCards");

        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        rp.cards = new int[] { ids[UnityEngine.Random.Range(0,ids.Length)], ids[UnityEngine.Random.Range(0, ids.Length)] };
        rp.time = 10;

        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }


    public override void DropStarsGradeVsCards(Action<ResponseData> result)
    {
        int[] ids = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 30, 31, 32, 33, 34, 35, 36, 37, 38 };
        Debug.LogFormat("DropStarsGradeVsCards");

        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;
        rp.cards = new int[] { ids[UnityEngine.Random.Range(0, ids.Length)], ids[UnityEngine.Random.Range(0, ids.Length)] };
        rp.time = 10;

        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }


    private Dictionary<string, string> userData;
    private string data1 = "{\"6\":-1,\"7\":-1,\"8\":0,\"9\":-1,\"10\":-1,\"11\":0,\"12\":0,\"13\":0,\"14\":0,\"15\":0,\"5\":2,\"4\":2,\"3\":2,\"2\":2,\"1\":2}";
    private string data2 = "{\"12\":0}";
    private string data3 = "";
    private string data4 = "Done";
    
    private string data5_day = "{\"score\":30,\"totalScore\":30,\"current\":\"10\\/30\\/2019\",\"end\":\"11\\/06\\/2019\",\"quest\":{\"16\":\"-2\",\"17\":\"0\",\"3\":\"0\",\"1\":\"0\",\"5\":\"0\",\"31\":\"0\",\"6\":\"-2\",\"14\":\"0\",\"22\":\"0\",\"19\":\"23\",\"21\":\"{\\\"ids\\\":[1,3,4,6,7,8,9,10,12,13,14,15,16,17,18,19,20,21]}\",\"23\":\"0\",\"4\":\"0\",\"26\":\"0\",\"27\":\"0\",\"28\":\"0\",\"29\":\"-2\",\"30\":\"0\"},\"prizeGetD\":[0],\"prizeGetW\":[]}";
    public override void LoadUserData(Action<StupidServerData> result)
    {
        Debug.LogFormat("LoadUserData");
        StupidServerData data = new StupidServerData();
        if (userData == null)
        {
            userData = new Dictionary<string, string>();
            userData.Add("1", data4);
            userData.Add("2", data3);
        }

        data.data = userData;

        PlayerManager._instance.StartCoroutine(DelayResponses(data, result));
    }

    public override void SaveUserData(string ID, string data, Action<StupidServerData> result)
    {
        Debug.LogFormat("SaveUserData {0} = {1}",ID,data);
        StupidServerData rp = new StupidServerData();
        userData[ID] = data;
        rp.data = userData;
        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }


    public override void GiveGoods(PrizeType type, string value, Action<ResponseData> result)
    {
        UIWaitPanel.Show();

        string sig = ServerManager.Md5Sum(PlayerManager.sig_key + "-" + 000000 + "-asda(234nkl45fd-" + value);
        Debug.LogFormat("GiveGoods,  type {0}, value {1} sig {2}", type, value,sig);


        ResponseData rp = new ResponseData();
        rp.ok = 1;
        rp.user = PlayerManager._instance._playerData;

        if (type == PrizeType.Chest)
        {
            rp.prizes = new ResponseData.PrizesData();
            rp.prizes.gold = 1;
            rp.prizes.crystals = 2;
            rp.prizes.towers = new ResponseData.PrizesTower[3];
            rp.prizes.towers[0] = new ResponseData.PrizesTower();
            rp.prizes.towers[0].count = 2;
            rp.prizes.towers[0].rarity = 1;
            rp.prizes.towers[0].type = 1;


            rp.prizes.towers[1] = new ResponseData.PrizesTower();
            rp.prizes.towers[1].count = 1;
            rp.prizes.towers[1].rarity = 1;
            rp.prizes.towers[1].type = 4;

            rp.prizes.towers[2] = new ResponseData.PrizesTower();
            rp.prizes.towers[2].count = 3;
            rp.prizes.towers[2].rarity = 1;
            rp.prizes.towers[2].type = 9;


            rp.prizes.craft_resources = new Dictionary<string, int>();
            rp.prizes.craft_resources.Add("1", UnityEngine.Random.Range(50, 100));
            rp.prizes.craft_resources.Add("2", UnityEngine.Random.Range(25, 50));
            rp.prizes.craft_resources.Add("3", UnityEngine.Random.Range(5, 25));
            rp.prizes.craft_resources.Add("4", UnityEngine.Random.Range(0, 5));
        }

        PlayerManager._instance.StartCoroutine(DelayResponses(rp, result));
    }

    private IEnumerator DelayResponses(ResponseData data, Action<ResponseData> result)
    {
        if(delayResponce>0)
            yield return new WaitForSecondsRealtime(delayResponce);

        UIWaitPanel.Hide();
        result(data);

        yield break;
    }

    private IEnumerator DelayResponses(ResponseDataTopDayPrize data, Action<ResponseDataTopDayPrize> result)
    {
        if (delayResponce > 0)
            yield return new WaitForSecondsRealtime(delayResponce);

        result(data);

        UIWaitPanel.Hide();
        yield break;
    }
    private IEnumerator DelayResponses(StupidServerData data, Action<StupidServerData> result)
    {
        if (delayResponce > 0)
            yield return new WaitForSecondsRealtime(delayResponce);

        result(data);

        UIWaitPanel.Hide();
        yield break;
    }

}
