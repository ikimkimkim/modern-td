﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using TDTK;

public abstract class DataManager 
{
    //Для определения необходимых параметров
    public abstract void Initialization();

    public abstract void LoadFriendsPlayer();

    public abstract void HelpedFriends(Action<FriendsData,int> result, int status);

    //Для получения свежих данных о пользователе (UserData) 
    public abstract void getPlayerData();

    //
    //public abstract void getListCards(Action<List<Card>> result);

    //public abstract void UpgradeCards(Card up, List<Card> resurse, Action result);
    public abstract void ChangeSettings(string name, string value, Action<ResponseData> result);

    public abstract void GreatAdvTop(int type, int map_id, int page, Action<TopData> result);

    public abstract void GreatDayTop(int type, int is_last, int page, Action<TopData> result);

    public abstract void getDayTop(int type,  Action<ResponseDataTopDayPrize> result);

    //StartLevel
    public abstract void StartLevel(int level_id, int diff, List<int[]>  towers, List<int[]> bonus, Action<ResponseData> result);

    //EndLevel
    public abstract void EndLevel(int level_id, int diff,int stars, int rnd, float avgfps,int map_id,float scores, float hp_left, int time, Action<ResponseData> result);

    public abstract void StartOpenChest(int id_chest, Action<ResponseData> result);

    public abstract void FastOpenChest(int id_chest, Action<ResponseData> result);

    public abstract void TakeChestPrizes(int id_chest, Action<ResponseData> result);


    public abstract void UpgradeTower(int type, int rarity, Action<ResponseData> result);

    public abstract void SellTower(int type, int rarity , int count, Action<ResponseData> result);

    public abstract void BuyForCrystals(string item_id, Action<ResponseData> result);

    public abstract void ReducingOpenTimeChest(int id_chest,float timeLeft, Action<ResponseData> result);

    public abstract void BuyPerk(int item_id, Action<ResponseData> result);
    public abstract void CancelPerk(int[] item_ids, Action<ResponseData> result);
    public abstract void RefrechPerk(Action<ResponseData> result);

    public abstract void SaveProgressUnlimited(int rnd, int level_id, int diff, string json_data, Action<ResponseData> result);

    public abstract void GetEventGift(int type, Action<ResponseDataTopDayPrize> result);

    public abstract void CreateUpgradeCard(int type, int rarity, int count, Action<ResponseData> result);


    public abstract void ApplyUpgradeCard(int type, int rarity, int craft_type, int craft_rarity, int param_id, List<int[]> bonus, Action<ResponseData> result);

    public abstract void DestroyChest(int id_chest, int card_rarity, Action<ResponseData> result);
    public abstract void OpenChest(int id_chest, int card_rarity, Action<ResponseData> result);
    public abstract void DecreaseOpenTimeChest(int id_chest, int card_rarity, Action<ResponseData> result);
    public abstract void UpgradeRarityChest(int id_chest, int card_rarity, Action<ResponseData> result);


    public abstract void SaleResources(int id_sale_res, int id_buy_res, int count_buy, Action<ResponseData> result);


    public abstract void EnergyRecovery(int card_rarity, Action<ResponseData> result);

    public abstract void LoadUserData(Action<StupidServerData> result);
    public abstract void SaveUserData(string ID, string data, Action<StupidServerData> result);

    public abstract void StarsGradeVsCards(Action<ResponseData> result);
    public abstract void DropStarsGradeVsCards(Action<ResponseData> result);

    public abstract void GiveGoods(PrizeType type, string value, Action<ResponseData> result);

    #region Api vk


    #endregion


}

public class UserData
{
    public PlayerData user = new PlayerData();
    public UserData()
    {

    }
}
