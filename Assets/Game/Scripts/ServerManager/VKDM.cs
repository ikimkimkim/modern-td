﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using TDTK;

public class VKDM : DataManager
{
    private ServerManager _ServerM;
    private PlayerManager _PlayerM;

    public override void Initialization()
    {
        WaitData = false;
        var playerInfo = GameObject.FindGameObjectWithTag("PlayerInfo");
        _ServerM = playerInfo.GetComponent<ServerManager>();
        _PlayerM = playerInfo.GetComponent<PlayerManager>();
        Debug.Log("VKDM Initialization complite");
    }

    public override void HelpedFriends(Action<FriendsData,int> result, int status)
    {
        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("helpedFriends", new List<StringPair>(), "", value => CBHelpedFriends(value, result, status)));
    }

    public override void LoadFriendsPlayer()
    {
        //_PlayerM.StartCoroutine(_ServerM.SaveLoadServer("friends", new List<StringPair>(), "", value => CBLoadFriendsPlayer(value, _PlayerM)));

    }

    public bool WaitData;
    public override void getPlayerData()
    {
        if (WaitData == false)
        {
            UIWaitPanel.Show();
            WaitData = true;
            _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("loadinfo", new List<StringPair>(), "", value =>
            {
                WaitData = false;
                Debug.Log("VKDM-> loadinfo");
                _PlayerM._SetPlayerData(((UserData)StringSerializationAPI.Deserialize(typeof(UserData), value)).user);
                UIWaitPanel.Hide();
            }));
        }
        else
        {
            MyLog.LogWarning("VKDM -> ladInfo WaitData!",MyLog.Type.build);
        }
    }


    public override void ChangeSettings(string name, string value, Action<ResponseData> result)
    {
        Debug.LogFormat("ChangeSettings {0}:{1}", name, value);

        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("name", name));
        param.Add(new StringPair("value", value));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("changeSettings", param, "", v => Response("changeSettings", v, result)));
    }


    public override void StartLevel(int level_id, int diff, List<int[]> towers, List<int[]> bonus, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("StartLevel -> id {0}, diff {1}", level_id, diff);

        if (level_id == 0)
        {
            Debug.Log("Trial level");
            ResponseData rd = new ResponseData();
            rd.ok = 1;
            rd.new_stars = 0;
            rd.prizes = new ResponseData.PrizesData();
            rd.prizes.chest = 0;
            rd.prizes.gold = 0;
            rd.prizes.crystals = 0;
            result(rd);
            return;
        }



        List<StringPair> param = new List<StringPair>();


        string json; //= StringSerializationAPI.Serialize(level_id.GetType(), level_id);
        param.Add(new StringPair("level_id", level_id.ToString()));

        // json = StringSerializationAPI.Serialize(diff.GetType(), diff);
        param.Add(new StringPair("diff", diff.ToString()));
        
        json = StringSerializationAPI.Serialize(towers.GetType(), towers);
        param.Add(new StringPair("towers", json));
        Debug.Log("StartLevel tower json:" + json);

        json = StringSerializationAPI.Serialize(bonus.GetType(), bonus);
        param.Add(new StringPair("bonus", json));
        Debug.Log("StartLevel bonus json:" + json);



        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("start", param, "", value => Response("StartLevel", value, result)));
    }

    public override void EndLevel(int level_id, int diff, int stars, int rnd, float avgfps, int map_id, float scores, float hp_left, int time, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("EndLevel -> id {0}, diff {1}, stars {2}, rnd {3}, time {4}, hp {5}=>{10}, user_id {6}, avgfps {7}, map_id {8}, scores {9}",
            level_id, diff, stars, rnd, time.ToString(), hp_left.ToString(), 
            SocialManager.Instance.getVKUserID, avgfps,map_id,scores, Mathf.CeilToInt(hp_left));


        if (level_id == 0)
        {
            Debug.Log("Trial level");
            ResponseData rd = new ResponseData();
            rd.ok = 1;
            rd.new_stars = 0;
            rd.prizes = new ResponseData.PrizesData();
            rd.prizes.chest = 0;
            rd.prizes.gold = 0;
            rd.prizes.crystals = 0;
            result(rd);
            return;
        }

        List<StringPair> param = new List<StringPair>();
       // string json = StringSerializationAPI.Serialize(level_id.GetType(), level_id);
        param.Add(new StringPair("level_id", level_id.ToString()));

       // json = StringSerializationAPI.Serialize(diff.GetType(), diff);
        param.Add(new StringPair("diff", diff.ToString()));

       // json = StringSerializationAPI.Serialize(stars.GetType(), stars);
        param.Add(new StringPair("stars", stars.ToString()));

        param.Add(new StringPair("time", time.ToString()));

        // json = StringSerializationAPI.Serialize(rnd.GetType(), rnd);
        param.Add(new StringPair("rnd", rnd.ToString()));

        param.Add(new StringPair("hp_left", Mathf.CeilToInt(hp_left).ToString()));

        param.Add(new StringPair("avgfps", avgfps.ToString()));

        //(userId + '-' + diff + '-' + time + '-' + hp_left + '-sakldjaskldASL:i7dalsd7alsdqqzxc')
        if (level_id < 0)
        {
            param.Add(new StringPair("map_id", map_id.ToString()));
            param.Add(new StringPair("scores", scores.ToString()));

            if(level_id==-2)
            {
                param.Add(new StringPair("sign", ServerManager.Md5Sum(SocialManager.Instance.getVKUserID + "-" + diff + "-" + time.ToString() +
                    "-" + GameControl.GetPlayerLife() + "-" + map_id + "-sakldjaskldASL:i7dalsd7alsdqqzxc")));
            }
            else if(level_id==-1)
            {
                //'89098799-41-1-sakldjaskldASL:i7dalsd7alsdqqzxc' < 'c648e2a39c04033fdb59436cec05f5b8'
                param.Add(new StringPair("sign", ServerManager.Md5Sum(SocialManager.Instance.getVKUserID + "-" + scores+"-"+map_id + "-sakldjaskldASL:i7dalsd7alsdqqzxc")));
            }
            else
            {
                Debug.LogError("End level send, level id error:"+level_id);
            }
        }
        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("endLevel", param, "", value => Response("EndLevel", value, result)));
    }

    public override void StartOpenChest(int id_chest, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("id", id_chest.ToString()));
        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("startOpenChest", param, "", value => Response("StartOpenChest", value, result)));
    }

    public override void FastOpenChest(int id_chest, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("id", id_chest.ToString()));
        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("fastOpenChest", param, "", value => Response("FastOpenChest", value, result)));
    }

    public override void TakeChestPrizes(int id_chest, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        List<StringPair> param = new List<StringPair>();
        if(id_chest>=0)
        {
            param.Add(new StringPair("id", id_chest.ToString()));
            _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("takeChestPrizes", param, "", value => Response("TakeChestPrizes", value, result)));
        }
        else if(id_chest == -1)
        {
            _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("collectDailyBonus", param, "", value => Response("TakeChestPrizes", value, result)));
        }
        else if (id_chest == -2)
        {
            _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("collectStarsChest", param, "", value => Response("TakeChestPrizes", value, result)));
        }
    }

    public override void UpgradeTower(int type, int rarity, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("type", type.ToString()));
        param.Add(new StringPair("rarity", rarity.ToString()));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("upgradeTower", param, "", value => Response("UpgradeTower", value, result)));
    }


    public override void SellTower(int type, int rarity, int count, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("type", type.ToString()));
        param.Add(new StringPair("rarity", rarity.ToString()));
        param.Add(new StringPair("count", count.ToString()));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("sellTower", param, "", value => Response("SellTower", value, result)));

    }

    public override void BuyForCrystals(string item_id, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("item_id",item_id.ToString()));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("buyForCrystals", param, "", value => Response("SellTower", value, result),true));
    }

    public override void GreatAdvTop(int type, int map_id, int page, Action<TopData> result)
    {
        Debug.Log(string.Format("Top type {0} map {1} page {2}",type,map_id,page));
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("page", page.ToString()));
        param.Add(new StringPair("map_id", map_id.ToString()));
        param.Add(new StringPair("type", type.ToString()));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("greatAdvTop", param, "", value => CBTop("GreatAdvTop", value, result)));
    }

    public override void GreatDayTop(int type, int is_last, int page, Action<TopData> result)
    {
        Debug.Log(string.Format("Day top type {0} last {1} page {2}", type, is_last, page));
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("page", page.ToString()));
        param.Add(new StringPair("is_last", is_last.ToString()));
        param.Add(new StringPair("type", type.ToString()));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("getDayTop", param, "", value => CBTop("GreatDayTop", value, result)));
    }

    public override void getDayTop(int type,Action<ResponseDataTopDayPrize> result)
    {
        UIWaitPanel.Show();
        Debug.Log(string.Format("get day top prize, type {0} ", type));
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("type", type.ToString()));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("collectDayTopPrize", param, "", value => Response("DayTopPrize", value, result)));
    }

    public override void BuyPerk(int item_id, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("id", item_id.ToString()));
        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("buyupgrade", param, "", value => Response("Response", value, result)));
    }

    public override void CancelPerk(int[] item_id, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("ids", StringSerializationAPI.Serialize(item_id.GetType(), item_id)));
        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("sellupgrades", param, "", value => Response("Response", value, result)));
    }

    public override void RefrechPerk(Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("dropUpgrades", null, "", value => Response("RefrechPerk", value, result)));
    }

    public override void ReducingOpenTimeChest(int id_chest,float timeLeft,  Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        timeLeft = Mathf.Round(timeLeft);
        Debug.Log(string.Format("ReducingOpenTimeChest, id_chest {0} , time {1} ", id_chest, timeLeft));
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("chest_id", id_chest.ToString()));

        param.Add(new StringPair("time_left", timeLeft.ToString()));

        param.Add(new StringPair("sig", ServerManager.Md5Sum(SocialManager.Instance.getVKUserID + "-" + id_chest + "-" + timeLeft + "-" + "asdjlkasdj0983ujlsa0_")));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("rewardedVideo", param, "", value => Response("rewardedVideo", value, result)));
    }


    public override void GetEventGift(int type, Action<ResponseDataTopDayPrize> result)
    {
        UIWaitPanel.Show();
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("type", type.ToString()));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("getEventGift", param, "", value => Response("getEventGift", value, result)));

    }

    public override void SaveProgressUnlimited(int rnd, int level_id, int diff, string json_data, Action<ResponseData> result)
    {
        string sig = ServerManager.Md5Sum(SocialManager.Instance.getVKUserID + "-" + level_id + "-" + diff + "-"
            + json_data + "-" + rnd + "-" + "ASDmlkqweap[s");
        Debug.Log(string.Format("SaveProgress, rnd {0}, level_id {1}, diff {2}, data {3}, sig {4} ", rnd, level_id, diff, json_data, sig));

        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("rnd", rnd.ToString()));
        param.Add(new StringPair("level_id", level_id.ToString()));
        param.Add(new StringPair("diff", diff.ToString()));
        param.Add(new StringPair("data", json_data.ToString()));
        param.Add(new StringPair("sig", sig));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("levelProgress", param, "", value => Response("levelProgress", value, result)));
    }

    public override void CreateUpgradeCard(int type, int rarity, int count, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("CreateUpgradeCard, type {0}, rarity {1}, count {2}", type, rarity, count);
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("type", type.ToString()));
        param.Add(new StringPair("rarity", rarity.ToString()));
        param.Add(new StringPair("count", count.ToString()));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("createUpgradeCard", param, "", value => Response("createUpgradeCard", value, result)));
    }


    public override void ApplyUpgradeCard(int type, int rarity, int craft_type, int craft_rarity, int param_id, List<int[]> bonus, Action<ResponseData> result)
    {
        //UIWaitPanel.Show();
        var json = StringSerializationAPI.Serialize(bonus.GetType(), bonus);
        Debug.LogFormat("CreateUpgradeCard, type {0}, rarity {1}, craft_type {2}, craft_rarity {3}, param_id {4}, bonus {5} ", type, rarity, craft_type, craft_rarity, param_id,json);
        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("type", type.ToString()));
        param.Add(new StringPair("rarity", rarity.ToString()));
        param.Add(new StringPair("craft_type", craft_type.ToString()));
        param.Add(new StringPair("craft_rarity", craft_rarity.ToString()));
        param.Add(new StringPair("param_id", param_id.ToString()));
        param.Add(new StringPair("bonus", json));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("applyUpgradeCard", param, "", value => Response("applyUpgradeCard", value, result)));
    }

    public override void DestroyChest(int id_chest, int card_rarity, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("DestroyChest, id {0}, rarity {1}", id_chest, card_rarity);

        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("id", id_chest.ToString()));
        param.Add(new StringPair("rarity", card_rarity.ToString()));
        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("destroyChest", param, "", value => Response("destroyChest", value, result)));
    }

    public override void OpenChest(int id_chest, int card_rarity, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("OpenChest, id {0}, rarity {1}", id_chest, card_rarity);

        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("id", id_chest.ToString()));
        param.Add(new StringPair("rarity", card_rarity.ToString()));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("openChest", param, "", value => Response("openChest", value, result)));
    }

    public override void DecreaseOpenTimeChest(int id_chest, int card_rarity, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("DecreaseOpenTimeChest, id {0}, rarity {1}", id_chest, card_rarity);

        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("id", id_chest.ToString()));
        param.Add(new StringPair("rarity", card_rarity.ToString()));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("decreaseOpenTimeChest", param, "", value => Response("decreaseOpenTimeChest", value, result)));
    }

    public override void UpgradeRarityChest(int id_chest, int card_rarity, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("UpgradeRarityChest, id {0}, rarity {1}", id_chest, card_rarity);

        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("id", id_chest.ToString()));
        param.Add(new StringPair("rarity", card_rarity.ToString()));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("upgradeRarityChest", param, "", value => Response("upgradeRarityChest", value, result)));
    }

    
    public override void SaleResources(int id_sale_res, int id_buy_res, int count_buy, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("SaleResources, id_sale_res {0}, id_buy_res {1}, count {2}", id_sale_res, id_buy_res, count_buy);

        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("id_sale", id_sale_res.ToString()));
        param.Add(new StringPair("id_buy", id_buy_res.ToString()));
        param.Add(new StringPair("count", count_buy.ToString()));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("saleResources", param, "", value => Response("saleResources", value, result)));
    }

    public override void EnergyRecovery(int card_rarity, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("EnergyRecovery, card_rarity {0}, ", card_rarity);

        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("rarity", card_rarity.ToString()));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("energyRecovery", param, "", value => Response("energyRecovery", value, result)));
    }



    public override void StarsGradeVsCards(Action<ResponseData> result)
    {
        Debug.LogFormat("StarsGradeVsCards");

        List<StringPair> param = new List<StringPair>();

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("Stars_grade_vs_cards", param, "", value => Response("StarsGradeVsCards", value, result)));
    }

    public override void DropStarsGradeVsCards(Action<ResponseData> result)
    {
        Debug.LogFormat("DropStarsGradeVsCards");

        List<StringPair> param = new List<StringPair>();

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("Stars_grade_vs_cards_drop", param, "", value => Response("DropStarsGradeVsCards", value, result)));
    }

    public override void LoadUserData(Action<StupidServerData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("LoadUserData");
        List<StringPair> param = new List<StringPair>();
        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("getAllFrontEndData", param, "", value => Response("LoadFullQuest", value, result)));
    }

    public override void SaveUserData(string ID, string data, Action<StupidServerData> result)
    {
        //UIWaitPanel.Show();
        //TimeScaleManager.SetTimeScale(0);
        Debug.LogFormat("saveFrontEndData {0}|{1}  {2},{3}",ID,data, PlayerManager.sig_key, SocialManager.Instance.getVKUserID);

        string sig = ServerManager.Md5Sum(PlayerManager.sig_key + "-" + SocialManager.Instance.getVKUserID + "-asda(234nkl45fd");

        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("data_id", ID.ToString()));
        param.Add(new StringPair("data", data.ToString()));
        param.Add(new StringPair("sig", sig));
        param.Add(new StringPair("sig_key", PlayerManager.sig_key));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer(
            string.Format("saveFrontEndData/?data_id={0}&data={1}",ID,data),
            param, "", value => Response("saveFrontEndData", value, result)));
    }


    public override void GiveGoods(PrizeType type, string value, Action<ResponseData> result)
    {
        UIWaitPanel.Show();
        Debug.LogFormat("GiveGoods,  type {0}, value {1}", type, value);

        string sig = ServerManager.Md5Sum(PlayerManager.sig_key + "-" + SocialManager.Instance.getVKUserID + "-asda(234nkl45fd-"+value);

        List<StringPair> param = new List<StringPair>();
        param.Add(new StringPair("type", ((int)type).ToString()));
        param.Add(new StringPair("value", value));
        param.Add(new StringPair("sig", sig));
        param.Add(new StringPair("sig_key", PlayerManager.sig_key));

        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("giveGoods", param, "", v => Response("GiveGoods", v, result)));
    }


    #region CallBacks

    public void Response(string nameMetod, string data, Action<ResponseData> result)
    {
        Debug.Log(nameMetod + " -> " + data);
        UIWaitPanel.Hide();
        ResponseData rp = null;
        try
        {
            rp = (ResponseData)StringSerializationAPI.Deserialize(typeof(ResponseData), data);
        }
        catch
        {
            Debug.LogError("Parsing response data fail! "+ nameMetod);
            ErrorParsing(result);
        }
        if (rp != null)
            result(rp);
    }
    public void Response(string nameMetod, string data, Action<ResponseDataTopDayPrize> result)
    {
        Debug.Log(nameMetod + " -> " + data);
        UIWaitPanel.Hide();
        try
        {
            ResponseDataTopDayPrize rp = (ResponseDataTopDayPrize)StringSerializationAPI.Deserialize(typeof(ResponseDataTopDayPrize), data);
            result(rp);
        }
        catch
        {
            MyLog.LogError("Parsing response data fail!", MyLog.Type.build);
            ErrorParsing(result);
        }
    }
    public void Response(string nameMetod, string data, Action<StupidServerData> result)
    {
        Debug.Log(nameMetod + " -> " + data);
        //TimeScaleManager.RefreshTimeScale();
        UIWaitPanel.Hide();
        
        try
        {
            StupidServerData rp = (StupidServerData)StringSerializationAPI.Deserialize(typeof(StupidServerData), data);
            result(rp);
        }
        catch
        {
            Debug.LogError("Parsing response data fail! " + nameMetod);
            ErrorParsing(result);
        }

    }

    public void CBHelpedFriends(string data, Action<FriendsData,int> result, int status)
    {
        Debug.Log("CBHelpedFriends -> " + data);
        UIWaitPanel.Hide();
        result((FriendsData)StringSerializationAPI.Deserialize(typeof(FriendsData), data),status);
    }


    public void CBTop(string MetodName, string data, Action<TopData> result)
    {
        Debug.Log(MetodName+" -> " + data);
        UIWaitPanel.Hide();
        TopData td = (TopData)StringSerializationAPI.Deserialize(typeof(TopData), data);
        result(td);
    }



    private void ErrorParsing(Action<ResponseData> result)
    {
        ResponseData rp = new ResponseData();
        rp.ok = 0;
        rp.error = new ErrorData();
        rp.error.code = "p0";
        rp.error.text = "Parsing response data fail";
        result(rp);
    }
    private void ErrorParsing(Action<ResponseDataTopDayPrize> result)
    {
        ResponseDataTopDayPrize rp = new ResponseDataTopDayPrize();
        rp.ok = 0;
        rp.error = new ResponseDataTopDayPrize.ErrorData();
        rp.error.code = "p0";
        rp.error.text = "Parsing response data fail";
        result(rp);
    }
    private void ErrorParsing(Action<StupidServerData> result)
    {
        StupidServerData rp = new StupidServerData();
        rp.data = new Dictionary<string, string>();
        result(rp);
    }



    #endregion
}
