﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GETPOSTTEST : MonoBehaviour
{

    private ServerManager _ServerM;
    private PlayerManager _PlayerM;

    public void Start()
    {
        var playerInfo = GameObject.FindGameObjectWithTag("PlayerInfo");
        _ServerM = playerInfo.GetComponent<ServerManager>();
        _PlayerM = playerInfo.GetComponent<PlayerManager>();
    }

    public void GET()
    {
        _PlayerM.StartCoroutine(_ServerM.SaveLoadServerTestGET("loadinfo", value =>
        {
            Debug.Log("VKDM-> loadinfo");
            _PlayerM._SetPlayerData(((UserData)StringSerializationAPI.Deserialize(typeof(UserData), value)).user);
        }));
    }

    public void POST()
    {
        _PlayerM.StartCoroutine(_ServerM.SaveLoadServer("loadinfo", new List<StringPair>(), "", value =>
        {
            Debug.Log("VKDM-> loadinfo");
            _PlayerM._SetPlayerData(((UserData)StringSerializationAPI.Deserialize(typeof(UserData), value)).user);
        }));
    }
}
