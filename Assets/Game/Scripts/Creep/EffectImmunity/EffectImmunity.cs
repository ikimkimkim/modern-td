﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;


//[CreateAssetMenu(fileName = "ImmunitySetting", menuName = "EffectManager/ImmunitySetting")]
public class ImmunitySetting
{
    public enum Type { Multiplier, Pow }
    public Type type;
    public float current { get; private set; }
    public float max = 1;
    public float min = 0;
    private int _count = 0;
    public int count
    {
        get
        {
            return _count;
        }
        set
        {
            _count = value;
            switch (type)
            {
                case Type.Multiplier:
                    current = Mathf.Clamp(_count * multiplier, min, max);
                    break;
                case Type.Pow:
                    current = Mathf.Clamp(Mathf.Pow(multiplier, _count), min, max);
                    break;
            }
        }
    }
    public float multiplier = 1;
}

public class EffectImmunity : MonoBehaviour
{
    
    public Unit rootUnit;
    public Dictionary<int, ImmunitySetting> setting = new Dictionary<int, ImmunitySetting>();

    void Awake()
    {
        if (rootUnit == null)
            rootUnit = GetComponent<Unit>();
        rootUnit.onMyInitE += RootUnit_onMyInitE;
        rootUnit.onMyDestroyedE += RootUnit_onMyDestroyedE;
    }

    private void RootUnit_onMyInitE(Unit unit)
    {
        InitSetting();
        rootUnit.EffectsManager.addMyEffectE += AddEffect;
    }

    int index;
    private void AddEffect(BaseEffect effect)
    {
        index = (int)effect.IDStack;
        if (setting.ContainsKey(index))
        {
            setting[index].count++;
        }     
    }

    private void RootUnit_onMyDestroyedE(Unit unit)
    {
        rootUnit.EffectsManager.addMyEffectE -= AddEffect;
    }

    void OnDestroy()
    {
        rootUnit.onMyInitE -= RootUnit_onMyInitE;
        rootUnit.onMyDestroyedE -= RootUnit_onMyDestroyedE;
    }

    void InitSetting()
    {
        foreach (var s in setting)
        {
            s.Value.count = 0;
        }
    }
}
