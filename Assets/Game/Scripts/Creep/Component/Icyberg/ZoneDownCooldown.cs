﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;

[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(Rigidbody))]
public class ZoneDownCooldown : MonoBehaviour {

    public Unit root;

    public List<UnitTower> towers;
    SphereCollider cols;

    [Range(0f, 1f)]
    public float Procent;

    void Start()
    {
        towers = new List<UnitTower>();
        cols = GetComponent<SphereCollider>();
        if (root.IsCreep && root.GetUnitCreep.flying)
        {
            transform.localPosition = new Vector3(0, -root.GetUnitCreep.flyingHeightOffset / root.thisT.localScale.x, 0);
        }
        else
        {
            transform.localPosition = new Vector3(0, 0, 0);
        }

    }

    void OnEnable()
    {
        if (towers != null)
            towers.Clear();
    }

    void OnDisable()
    {
        if (towers == null)
            return;
        for (int i = 0; i < towers.Count; i++)
        {
            towers[i].EffectsManager.removeMyEffectE -= RemoveEffect;
        }
        towers.Clear();
    }

    UnitTower tower;
    void OnTriggerEnter(Collider other)
    {
        if (root == null || root.dead)
            return;

        tower = other.transform.GetComponent<UnitTower>();
        if (tower == null)
            return;

        if (towers.Contains(tower) == false)
        {
            towers.Add(tower);
            tower.EffectsManager.removeMyEffectE += RemoveEffect;
        }

        if (tower.EffectsManager.isActiveEffect(IDEffect.UpCooldownDebufUnique))
            return;

        towerAddEffect(tower);
    }

    void OnTriggerExit(Collider other)
    {
        if (root == null || root.dead)
            return;

        tower = other.transform.GetComponent<UnitTower>();
        if (tower == null)
            return;

        if (towers.Contains(tower))
        {
            towers.Remove(tower);
            tower.EffectsManager.removeMyEffectE -= RemoveEffect;
        }

    }

    private void towerAddEffect(UnitTower tower)
    {
        UpCooldownDebuffEffect effect = new UpCooldownDebuffEffect(Procent,1);
        tower.EffectsManager.AddEffect(root, effect);
        effect.workСonditionEffect = new EffectInRangeWC(root, tower, cols.radius * root.thisT.localScale.z);
    }

    private void RemoveEffect(Unit unit, IDEffect typeEffect)
    {
        if (root.dead)
            return;
        if (unit is UnitTower && typeEffect == IDEffect.UpCooldownDebufUnique && unit.EffectsManager.isActiveEffect(IDEffect.UpCooldownDebufUnique) == false)
        {
            tower = unit as UnitTower;
            towerAddEffect(tower);
        }
    }
}
