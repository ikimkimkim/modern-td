﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(Rigidbody))]
public class ZoneRandomDebuff : MonoBehaviour
{
    public Unit root;

    public List<Unit> units;
    SphereCollider cols;
    public SkinnedMeshRenderer rootMeshRender;

    [Header("Effect 1")]
    [Range(0f, 1f)]
    public float ProcentDownInDamage;
    [TextArea]
    public string TextInfoEffect1;
    public Material flag1;

    [Header("Effect 2")]
    [Range(0f, 1f)]
    public float ProcentRegenHPForSec;
    [TextArea]
    public string TextInfoEffect2;
    public Material flag2;

    [Header("Effect 3")]
    [Range(0f, 1f)]
    public float ProcentUpMove;
    [TextArea]
    public string TextInfoEffect3;
    public Material flag3;

    private IDEffect currentEffect;

    public void InitRandomEffecct()
    {
        int i = Random.Range(0, 3);
        switch(i)
        {
            case 0:
                currentEffect = IDEffect.DownInDmgBuff;
                root.GetUnitCreep.textPropertiesInfo = TextInfoEffect1;
                rootMeshRender.material = flag1;
                break;
            case 1:
                currentEffect = IDEffect.RegenHPBuff;
                root.GetUnitCreep.textPropertiesInfo = TextInfoEffect2;
                rootMeshRender.material = flag2;
                break;
            case 2:
                currentEffect = IDEffect.UpMoveBuff;
                root.GetUnitCreep.textPropertiesInfo = TextInfoEffect3;
                rootMeshRender.material = flag3;
                break;
        }
    }

    private BaseEffect getCurrentNewEffect()
    {
        switch (currentEffect)
        {
            case IDEffect.DownInDmgBuff:
                return new DownInDmgBuffEffect(ProcentDownInDamage);
            case IDEffect.RegenHPBuff:
                return new RegenHPBuffEffect(ProcentRegenHPForSec);
            case IDEffect.UpMoveBuff:
                return new UpMoveBuffEffect(ProcentUpMove, 1);
            default:
                Debug.LogError("Random effect not init!");
                return null;
        }
    }

    private bool isNewTarget(Unit unit)
    {
        if (unit == null)
            return false;
        switch (currentEffect)
        {
            case IDEffect.DownInDmgBuff:
            case IDEffect.RegenHPBuff:
            case IDEffect.UpMoveBuff:
                return unit.IsCreep;
            default:
                Debug.LogError("Random effect not init!");
                return false;
        }
    }


    void Start()
    {
        units = new List<Unit>();
        cols = GetComponent<SphereCollider>();
        if (root.IsCreep && root.GetUnitCreep.flying)
        {
            transform.localPosition = new Vector3(0, -root.GetUnitCreep.flyingHeightOffset / root.thisT.localScale.x, 0);
        }
        else
        {
            transform.localPosition = new Vector3(0, 0, 0);
        }
    }

    void OnEnable()
    {
        if (units != null)
            units.Clear();

        InitRandomEffecct();
    }

    void OnDisable()
    {
        if (unit == null)
            return;
        for (int i = 0; i < units.Count; i++)
        {
            unit.EffectsManager.removeMyEffectE -= RemoveEffect;
        }
        units.Clear();
    }

    Unit unit;
    void OnTriggerEnter(Collider other)
    {
        if (root == null || root.dead)
            return;

        unit = other.transform.GetComponent<Unit>();
        if (isNewTarget(unit)==false)
            return;

        if (units.Contains(unit) == false)
        {
            units.Add(unit);
            unit.EffectsManager.removeMyEffectE += RemoveEffect;
        }

        if (unit.EffectsManager.isActiveEffect(currentEffect))
            return;

        unitAddEffect(unit);
    }

    void OnTriggerExit(Collider other)
    {
        if (root == null || root.dead)
            return;

        unit = other.transform.GetComponent<Unit>();
        if (unit == null)
            return;

        if (units.Contains(unit))
        {
            units.Remove(unit);
            unit.EffectsManager.removeMyEffectE -= RemoveEffect;
        }

    }

    private void unitAddEffect(Unit unit)
    {
        BaseEffect effect = getCurrentNewEffect();
        unit.EffectsManager.AddEffect(root, effect);
        effect.workСonditionEffect = new EffectInRangeWC(root, unit, cols.radius * root.thisT.localScale.z);
    }

    private void RemoveEffect(Unit unit, IDEffect typeEffect)
    {
        if (root.dead)
            return;
        if (unit is Unit && typeEffect == currentEffect && unit.EffectsManager.isActiveEffect(currentEffect) == false)
        {
            unit = unit as Unit;
            unitAddEffect(unit);
        }
    }
}
