﻿using UnityEngine;
using System.Collections;
using TDTK;

public class UpMoveOfDmgCreepCompanent : MonoBehaviour {

    private const int IDMoveInc = -1;

    public Unit root;
    public int dmgType;
    [Range(0f, 1f)]
    public float moveIncProcent = 0.01f;
    [Range(0f, 1f)]
    public float maxMoveProcent;
    private float currentProcentMove;

    void OnEnable()
    {
        root.onMyInitE += Root_onMyInitE;
        root.onMyDestroyedE += Root_onMyDestroyedE;
    }


    private void Root_onMyInitE(Unit unit)
    {
        currentProcentMove = 0f;
        root.SetMoveMultiplier(IDMoveInc, 1f + currentProcentMove);
        root.myInAttakInstanceLastE += InDmg;
    }

    private void InDmg(AttackInstance attackInstance)
    {
        if(attackInstance.getDamageType == dmgType)
        {
            currentProcentMove = Mathf.Clamp(currentProcentMove + moveIncProcent,0f,maxMoveProcent);
            root.SetMoveMultiplier(IDMoveInc, 1f + currentProcentMove);
        }
    }

    private void Root_onMyDestroyedE(Unit unit)
    {
        root.myInAttakInstanceLastE -= InDmg;
        root.RemoveMoveMultiplier(IDMoveInc);
    }

    void OnDisable()
    {
        root.onMyInitE -= Root_onMyInitE;
        root.onMyDestroyedE -= Root_onMyDestroyedE;
    }

}
