﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class StunMaxDmgTowerComponent : MonoBehaviour {


    public UnitCreep Root;

    [Range(0f,1f)]
    public float DmgHPProcent;

    public Dictionary<Unit,float> towers;

    public ShootObject shootObjPrefab;
    public float TimeStun=1f;
    private PropertiesAttak attack;

    // Use this for initialization
    void Start ()
    {
        attack = new PropertiesAttak();
        attack.stats[0].damageMin = 0;
        attack.stats[0].damageMax = 0;
    }
	
    public void OnEnable()
    {
        if(towers==null)
            towers = new Dictionary<Unit, float>();
        else
            towers.Clear();
        Root.myApplyAttackInstance += Root_myApplyAttackInstance;
    }

    public void OnDisable()
    {
        towers.Clear();
        Root.myApplyAttackInstance -= Root_myApplyAttackInstance;
    }

    private void Root_myApplyAttackInstance(AttackInstance instance)
    {        
        if(instance.srcUnit != null && instance.srcUnit is UnitTower)
        {
            if (towers.ContainsKey(instance.srcUnit as UnitTower) == false)
                towers.Add(instance.srcUnit as UnitTower, instance.damage);
            else
                towers[instance.srcUnit as UnitTower] += instance.damage;

            //Debug.Log(name + "Dmg tower "+ instance.srcUnit.name+":" + towers[instance.srcUnit]+" limit "+ (Root.fullHP * DmgHPProcent));

            if(towers[instance.srcUnit as UnitTower] >=Root.fullHP*DmgHPProcent)
            {
                StartCoroutine(StartAttack(instance.srcUnit as UnitTower));
                towers.Remove(instance.srcUnit as UnitTower);
            }
        }
    }

    IEnumerator StartAttack(Unit target)
    {
        

        while((Root.WaitShootEvent || Root.PropertiesAttakLock) && Root.dead == false || Root.stunned)
        {
            yield return null;
        }
        if (Root.dead)
            yield break;

        Root.PropertiesAttakLock = true;
        Root.WaitShootEvent = true;
        Root.lockMovePach = true;
        Root._Agent.isStopped = true;
        Root.animationUnit.PlayShoot();
        Root.thisT.LookAt(target.thisT);

        while (Root.WaitShootEvent && Root.dead == false)
        {
            yield return null;
        }

        if (Root.dead)
            yield break;


        AttackInstance attackInstance = new AttackInstance();
        attackInstance.tgtUnit = target;
        attackInstance.srcPropretiesAttak = attack;
        attackInstance.listEffects.Add(new StunControlEffect(TimeStun, false));

        GameObject obj = ObjectPoolManager.Spawn(shootObjPrefab.gameObject, Root.shootPoints[0].position, Root.shootPoints[0].rotation); 

        ShootObject shootObj = obj.GetComponent<ShootObject>();
        shootObj.Shoot(attackInstance, Root.shootPoints[0]);


        Root.PropertiesAttakLock = false;
        Root.lockMovePach = false;

        if (Root.stunned == false)
            Root._Agent.isStopped = false;

        yield break;
    }


}
