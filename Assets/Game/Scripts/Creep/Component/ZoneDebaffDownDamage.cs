﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(Rigidbody))]
public class ZoneDebaffDownDamage : MonoBehaviour
{
    public Unit root;

    public List<UnitHero> heroes;
    SphereCollider cols;

    [Range(0f,1f)]
    public float ProcentDamage;

    void Start()
    {
        heroes = new List<UnitHero>();
        cols = GetComponent<SphereCollider>();
        if (root.IsCreep && root.GetUnitCreep.flying)
        {
            transform.localPosition = new Vector3(0, -root.GetUnitCreep.flyingHeightOffset / root.thisT.localScale.x, 0);
        }
        else
        {
            transform.localPosition = new Vector3(0, 0, 0);
        }

    }

    void OnEnable()
    {
        if (heroes != null)
            heroes.Clear();
    }

    void OnDisable()
    {
        if (hero == null)
            return;
        for (int i = 0; i < heroes.Count; i++)
        {
            heroes[i].EffectsManager.removeMyEffectE -= RemoveEffect;
        }
        heroes.Clear();
    }

    UnitHero hero;
    void OnTriggerEnter(Collider other)
    {
        if (root == null || root.dead)
            return;

        hero = other.transform.GetComponent<UnitHero>();
        if (hero == null)
            return;

        if (heroes.Contains(hero) == false)
        {
            heroes.Add(hero);
            hero.EffectsManager.removeMyEffectE += RemoveEffect;
        }

        if (hero.EffectsManager.isActiveEffect(IDEffect.DownDmgUnique))
            return;

        HeroAddEffect(hero);
    }

    void OnTriggerExit(Collider other)
    {
        if (root == null || root.dead)
            return;

        hero = other.transform.GetComponent<UnitHero>();
        if (hero == null)
            return;

        if (heroes.Contains(hero))
        {
            heroes.Remove(hero);
            hero.EffectsManager.removeMyEffectE -= RemoveEffect;
        }

    }

    private void HeroAddEffect(UnitHero hero)
    {
        DownDmgUniqueEffect effect = new DownDmgUniqueEffect(ProcentDamage);
        hero.EffectsManager.AddEffect(root, effect);
        effect.workСonditionEffect = new EffectInRangeWC(root, hero, cols.radius * root.thisT.localScale.z);
    }

    private void RemoveEffect(Unit unit, IDEffect typeEffect)
    {
        if (root.dead)
            return;
        if(unit is UnitHero && typeEffect == IDEffect.DownDmgUnique && unit.EffectsManager.isActiveEffect(IDEffect.DownDmgUnique)==false)
        {
            hero = unit as UnitHero;
            HeroAddEffect(hero);
        }
    }
}
