﻿using UnityEngine;
using System.Collections;
using TDTK;

public class RatProperties : MonoBehaviour{

    private const int IDMoveM = -4;
    [SerializeField] private UnitCreep rats;
    public float moveInc;
    [SerializeField] private float maxInc = 3;
    private int counter;

    private void OnEnable()
    {
        UnitCreep.onDestroyedE += UnitCreep_onDestroyedE;
    }

    private void OnDisable()
    {
        UnitCreep.onDestroyedE -= UnitCreep_onDestroyedE;
    }

    private void UnitCreep_onDestroyedE(Unit unit)
    {
        if(unit.prefabID == 14)
        {
            counter++;
            rats.SetMoveMultiplier(IDMoveM, 1f + Mathf.Clamp(counter * moveInc,0,maxInc));
           // rats.stats[rats.currentActiveStat].moveSpeed = Mathf.Clamp(rats.stats[rats.currentActiveStat].moveSpeed * moveInc, 0, maxMove);
        }
    }
}
