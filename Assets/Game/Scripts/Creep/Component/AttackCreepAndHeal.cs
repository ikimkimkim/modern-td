﻿using UnityEngine;
using System.Collections;
using TDTK;

public class AttackCreepAndHeal : MonoBehaviour {




    public UnitCreep rootUnitCreep;


    public _CreepClass TargetClass;
    public LayerMask mask;
    public float Radius;
    public float Cooldown;
    private float LastAttack = -1;
    [Range(0f,1f)]
    public float ProcentLimitHP=0.9f;

    [SerializeField]
    private UnitCreep Target;
    private UnitCreepAnimation anim;

    private enum Status { Normal, SearchTarget, Attack }
    [SerializeField]
    private Status status;

    void Start()
    {
        anim = rootUnitCreep.GetComponent<UnitCreepAnimation>();
    }

    void OnEnable()
    {
        LastAttack = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (rootUnitCreep == null || rootUnitCreep.dead)
            return;

        switch (status)
        {
            case Status.Normal:
                if (Time.time > LastAttack + Cooldown && rootUnitCreep.HP/rootUnitCreep.fullHP< ProcentLimitHP)
                    status = Status.SearchTarget;

                break;

            case Status.SearchTarget:
                if(rootUnitCreep.stunned==false)
                    SearchTarget();
                break;
            case Status.Attack:
                if (Target == null || Target.dead || Vector3.Distance(Target.getPosition, rootUnitCreep.getPosition)-Target.SizeMyCollider > Radius)
                {
                    ReturnSearch();
                    return;
                }
                rootUnitCreep.thisT.LookAt(Target.getPosition);
                break;
        }

    }

    private void ReturnSearch()
    {
        MyLog.Log("ReturnSearch:" + (Vector3.Distance(Target.getPosition, rootUnitCreep.getPosition) - Target.SizeMyCollider));
        status = Status.SearchTarget;
        rootUnitCreep.lockMovePach = false;
        rootUnitCreep._Agent.isStopped = false;
    }

    private Collider[] cols;
    private void SearchTarget()
    {
        var units = SpawnManager.instance.ActiveCreeper;
        for (int i = 0; i < units.Count; i++)
        {
            if (units[i].creepClass == TargetClass && !units[i].isBoss &&
                StaticCommon.DistanceLimit(units[i].getPosition, rootUnitCreep.getPosition, Radius))
            {
                Target = units[i];
                rootUnitCreep.thisT.LookAt(Target.getPosition);
                anim.PlayShoot();
                rootUnitCreep.lockMovePach = true;
                rootUnitCreep._Agent.isStopped = true;
                status = Status.Attack;
                return;
            }
        }
        Target = null;

      /*  cols = Physics.OverlapSphere(rootUnitCreep.getPosition, Radius, mask);
        if (cols == null || cols.Length <= 0)
            return;
        for (int i = 0; i < cols.Length; i++)
        {
            Target = cols[i].GetComponent<UnitCreep>();
            if (Target.creepClass == TargetClass && Target.isBoss==false)
            {
                rootUnitCreep.thisT.LookAt(Target.getPosition);
                anim.PlayShoot();
                rootUnitCreep.lockMovePach = true;
                rootUnitCreep._Agent.isStopped = true;
                status = Status.Attack;
                return;
            }
        }
        Target = null;*/

    }

    UnitCreep unit;
    //Вызывается аниматором при анимации атаки (когда нужно/можно выпустить снаряд)
    public void AttackAnimEvent()
    {
        if (status != Status.Attack || Target==null)
            return;


        rootUnitCreep.AddHeal(Target.HP);

        Target.Dead(false);

        Target = null;
        rootUnitCreep.lockMovePach = false;
        rootUnitCreep._Agent.isStopped = false;
        LastAttack = Time.time;
        status = Status.Normal;
    }


}
