﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class ZoneSwarmDebuff : MonoBehaviour {

    public Unit root;

    public List<UnitCreep> creeps;
    SphereCollider cols;

    [Range(0f, 1f)]
    public float ProcentSlow;

    [Range(0f, 1f)]
    public float ProcentMiss;

    void Start()
    {
        creeps = new List<UnitCreep>();
        cols = GetComponent<SphereCollider>();
        if (root.IsCreep && root.GetUnitCreep.flying)
        {
            transform.localPosition = new Vector3(0, -root.GetUnitCreep.flyingHeightOffset / root.thisT.localScale.x, 0);
        }
        else
        {
            transform.localPosition = new Vector3(0, 0, 0);
        }

    }

    void OnEnable()
    {
        if (creeps != null)
            creeps.Clear();
    }

    void OnDisable()
    {
        if (unit == null)
            return;
        for (int i = 0; i < creeps.Count; i++)
        {
            creeps[i].EffectsManager.removeMyEffectE -= RemoveEffect;
        }
        creeps.Clear();
    }

    UnitCreep unit;
    void OnTriggerEnter(Collider other)
    {
        if (root == null || root.dead)
            return;

        unit = other.transform.GetComponent<UnitCreep>();
        if (unit == null)
            return;

        if (unit.prefabID == root.prefabID)
            return;

        if (creeps.Contains(unit) == false)
        {
            creeps.Add(unit);
            unit.EffectsManager.removeMyEffectE += RemoveEffect;
        }

        if (unit.EffectsManager.isActiveEffect(IDEffect.SwarmEffect))
            return;

        unitAddEffect(unit);
    }

    void OnTriggerExit(Collider other)
    {
        if (root == null || root.dead)
            return;

        unit = other.transform.GetComponent<UnitCreep>();
        if (unit == null)
            return;

        if (creeps.Contains(unit))
        {
            creeps.Remove(unit);
            unit.EffectsManager.removeMyEffectE -= RemoveEffect;
        }

    }

    private void unitAddEffect(UnitCreep unit)
    {
        SwarmEffect effect = new SwarmEffect(ProcentSlow,ProcentMiss);
        unit.EffectsManager.AddEffect(root, effect);
        effect.workСonditionEffect = new EffectInRangeWC(root, unit, cols.radius * root.thisT.localScale.z);
    }

    private void RemoveEffect(Unit unit, IDEffect typeEffect)
    {
        if (root.dead)
            return;
        if (unit is UnitCreep && typeEffect == IDEffect.SwarmEffect && unit.EffectsManager.isActiveEffect(IDEffect.SwarmEffect) == false)
        {
            unitAddEffect(unit as UnitCreep);
        }
    }
}
