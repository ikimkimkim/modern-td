﻿using UnityEngine;
using System.Collections;
using TDTK;

public class ShieldUnitCompanent : MonoBehaviour {

    public const int IDShiled = 2;

    public UnitCreep root;

    public ShieldData data;

    [Range(0f,1f)]
    public float Procent=0.5f;

    public int ArmorType;

    void OnEnable()
    {
        if (root == null)
            root = GetComponent<UnitCreep>();

        root.onMyInitE += Root_onMyInitE;
    }

    private void Root_onMyInitE(Unit unit)
    {
        data = new ShieldData(root.fullHP * Procent, ArmorType);
        root.AddShield(IDShiled, data);
    }

    void OnDisable()
    {
        root.onMyInitE -= Root_onMyInitE;
    }
}
