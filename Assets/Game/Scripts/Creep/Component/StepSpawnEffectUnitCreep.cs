﻿using UnityEngine;
using System.Collections;
using TDTK;

public class StepSpawnEffectUnitCreep : MonoBehaviour {

    public UnitCreep rootCreep;

    public GameObject prefab;
    public float StepSpawn=1;

    private Vector3 lastPosition;	

	void Update ()
    {
        if (rootCreep.dead)
            return;

        if(Vector3.Distance(rootCreep.thisT.position,lastPosition)>=StepSpawn)
        {
            Spawn();
        }
	}

    public void Spawn()
    {
        ObjectPoolManager.Spawn(prefab,rootCreep.thisT.position,rootCreep.thisT.rotation);
        lastPosition = rootCreep.thisT.position;
    }
}
