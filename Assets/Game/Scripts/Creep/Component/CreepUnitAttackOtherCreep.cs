﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;
//AttackCreepAndSpawnChild
public class CreepUnitAttackOtherCreep : MonoBehaviour {

   

    public UnitCreep rootUnitCreep;
    public GameObject prefabChild;
    [SerializeField]
    private Vector2 RandomOffset;
    [Range(0f,1f)]
    public float DegradationProcent=1;
    [Range(0f,1f)]
    public float ScaleProcent = 1;
    [Range(0f, 1f)]
    public float RscProcent = 1;
    public float minScaleProcent = 0.5f;
    public _CreepClass TargetClass;
    public LayerMask mask;
    public float Radius;
    public float Cooldown;
    private float LastAttack = -1;
    public int LimitGeneration = -1;
    public int LimitAttack = -1;
    [SerializeField]
    private int CountAttack = 0;

    [SerializeField]
    private UnitCreep Target;
    private UnitCreepAnimation anim;

    private enum Status { Normal, SearchTarget, Attack }
    [SerializeField]
    private Status status;

	void Start ()
    {
        anim = rootUnitCreep.GetComponent<UnitCreepAnimation>();
    }
	
    void OnEnable()
    {
        LastAttack = Time.time;
        CountAttack = 0;
        status = Status.Normal;
    }

    private float lastUpdate;
	void Update () {
        if (rootUnitCreep == null || rootUnitCreep.dead || (LimitGeneration <= rootUnitCreep.Generation && LimitGeneration > -1) || 
            (CountAttack >= LimitAttack && LimitAttack > -1))
            return;

        if(Time.time> lastUpdate+0.5f)
            lastUpdate = Time.time;

        switch(status)
        {
            case Status.Normal:
                if(Time.time > LastAttack + Cooldown)
                    status = Status.SearchTarget;

                break;

            case Status.SearchTarget:
                if(rootUnitCreep.stunned==false)
                    SearchTarget();
                break;
            case Status.Attack:
                if (Target==null || Target.dead || Vector3.Distance(Target.getPosition,rootUnitCreep.getPosition)-Target.SizeMyCollider>Radius)
                {
                    ReturnSearch();
                    return;
                }
                rootUnitCreep.thisT.LookAt(Target.getPosition);
                break;
        }

	}

    private void ReturnSearch()
    {
        status = Status.SearchTarget;
        rootUnitCreep.lockMovePach = false;
        rootUnitCreep._Agent.isStopped = false;
    }

    private Collider[] cols;
    private void SearchTarget()
    {
        if (status != Status.SearchTarget)
            return;

        var units = SpawnManager.instance.ActiveCreeper;
        for (int i = 0; i < units.Count; i++)
        {
            if(units[i].creepClass == TargetClass && !units[i].isBoss &&
            StaticCommon.DistanceLimit(units[i].getPosition, rootUnitCreep.getPosition, Radius))
            {
                Target = units[i];
                status = Status.Attack;
                rootUnitCreep.thisT.LookAt(Target.getPosition);
                anim.PlayShoot();
                rootUnitCreep.lockMovePach = true;
                rootUnitCreep._Agent.isStopped = true;
                return;
            }
        }
        Target = null;

        /*cols = Physics.OverlapSphere(rootUnitCreep.getPosition, Radius, mask);
        if (cols == null || cols.Length <= 0)
            return;
        for (int i = 0; i < cols.Length; i++)
        {
            Target = cols[i].GetComponent<UnitCreep>();
            if (Target.creepClass == TargetClass && Target.isBoss==false)
            {
                status = Status.Attack;
                rootUnitCreep.thisT.LookAt(Target.getPosition);
                anim.PlayShoot();
                rootUnitCreep.lockMovePach = true;
                rootUnitCreep._Agent.isStopped = true;
                return;
            }
        }
        Target = null;*/

    }


    UnitCreep unit;
    //Вызывается аниматором при анимации атаки (когда нужно/можно выпустить снаряд)
    public void AttackAnimEvent()
    {
        if (status != Status.Attack)
            return;

        Target.Dead(false);
        Target.DeadEndAnimEvent();
        Vector3 offset = getRandom;
        unit = ObjectPoolManager.Spawn(prefabChild, rootUnitCreep.getPosition + offset, rootUnitCreep.thisT.rotation).GetComponent<UnitCreep>();
        unit.waveID = rootUnitCreep.waveID;
        int ID = SpawnManager.AddDestroyedSpawn(unit);
        unit.Init(rootUnitCreep.path, ID, rootUnitCreep.waveID, rootUnitCreep);

       

        unit.fullHP = rootUnitCreep.fullHP * DegradationProcent;
        //unit.fullShield = rootUnitCreep.fullShield * DegradationProcent;
        unit.HP = unit.fullHP;
        MyLog.LogWarning("Не унаследует щит");
        //unit.shield = unit.fullShield;

        unit.valueRscMin = new List<float>();
        for (int i = 0; i < rootUnitCreep.valueRscMin.Count; i++)
        {
            unit.valueRscMin.Add(rootUnitCreep.valueRscMin[i] * RscProcent);
        }
        unit.valueRscMax = new List<float>();
        for (int i = 0; i < rootUnitCreep.valueRscMax.Count; i++)
        {
            unit.valueRscMax.Add(rootUnitCreep.valueRscMax[i] * RscProcent);
        }

        unit.thisT.localScale = rootUnitCreep.thisT.localScale * ScaleProcent;
        if (unit.thisT.localScale.x < unit.deffScale.x * minScaleProcent)
        {
            unit.thisT.localScale = unit.deffScale * minScaleProcent;
        }

        unit.Generation = rootUnitCreep.Generation + 1;

       
        unit.pathOffset = rootUnitCreep.pathOffset + offset / 2;

        status = Status.Normal;
        Target = null;
        rootUnitCreep.lockMovePach = false;
        rootUnitCreep._Agent.isStopped = false;
        LastAttack = Time.time;
        CountAttack++;
    }

    private Vector3 getRandom
    {
        get
        {
            return  new Vector3(Random.value>0.5 ? -RandomOffset.x: RandomOffset.x, 0, Random.value > 0.5 ? -RandomOffset.y : RandomOffset.y);
        }
    }
}
