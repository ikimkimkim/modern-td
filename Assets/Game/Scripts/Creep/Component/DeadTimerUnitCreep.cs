﻿using UnityEngine;
using System.Collections;
using TDTK;

public class DeadTimerUnitCreep : MonoBehaviour {

    public UnitCreep RootUnit;

    public float TimeLife;
    private float timeStart;

	// Use this for initialization
	void OnEnable () {
        timeStart = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if (RootUnit.dead)
            return;
	    if(Time.time>timeStart + TimeLife)
        {
            RootUnit.Dead();
        }
	}
}
