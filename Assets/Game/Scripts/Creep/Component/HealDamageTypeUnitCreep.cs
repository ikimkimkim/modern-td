﻿using UnityEngine;
using System.Collections;
using TDTK;

public class HealDamageTypeUnitCreep : MonoBehaviour {

    public UnitCreep rootCreep;

    public int typeInDmg;
    [Range(0f,1f)]
    public float ProcentDmgHeal=1;

    public float ProcentMinScale;
    [SerializeField]
    private float CurrentProcentScale;
    public float ProcentMaxScale;

	void Start ()
    {
        rootCreep.myApplyAttackInstance += ApplyAttack;
    }

    void OnEnable()
    {
        CurrentProcentScale = ProcentMinScale;
        transform.localScale = CurrentProcentScale * rootCreep.deffScale;
    }

    public void ApplyAttack(AttackInstance attackInstance)
    {
        if(attackInstance.getDamageType == typeInDmg)
        {
            rootCreep.AddHeal(attackInstance.damage * ProcentDmgHeal);
            CurrentProcentScale = Mathf.Clamp(CurrentProcentScale + attackInstance.damage / rootCreep.fullHP,ProcentMinScale,ProcentMaxScale);
            transform.localScale = CurrentProcentScale * rootCreep.deffScale;

            attackInstance.damage = 0;
            attackInstance.damageShield = 0;
            attackInstance.destroy = false;
        }
    }
	

    void OnDestory()
    {
        rootCreep.myApplyAttackInstance -= ApplyAttack;
    }
}
