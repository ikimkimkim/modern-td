﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(Rigidbody))]
public class ZoneProvocation : MonoBehaviour
{
    public Unit root;

    public List<Unit> units;
    //SphereCollider cols;


    void Start()
    {
        units = new List<Unit>();
        //cols = GetComponent<SphereCollider>();
        if (root.IsCreep && root.GetUnitCreep.flying)
        {
            transform.localPosition = new Vector3(0, -root.GetUnitCreep.flyingHeightOffset / root.thisT.localScale.x, 0);
        }
        else
        {
            transform.localPosition = new Vector3(0, 0, 0);
        }

    }

    void OnEnable()
    {
        if (units != null)
            units.Clear();
    }

    void OnDisable()
    {
        if (unit == null)
            return;
        for (int i = 0; i < units.Count; i++)
        {
            if (units[i].priorityTarget == root)
                units[i].priorityTarget = null;
        }
        units.Clear();
    }

    Unit unit;
    void OnTriggerEnter(Collider other)
    {
        if (root == null || root.dead)
            return;

        unit = other.transform.GetComponent<Unit>();
        if (unit == null)
            return;

        if (units.Contains(unit) == false)
        {
            units.Add(unit);
        }

        unit.priorityTarget = root;
    }

    void OnTriggerExit(Collider other)
    {
        if (root == null || root.dead)
            return;

        unit = other.transform.GetComponent<Unit>();
        if (unit == null)
            return;

        if (units.Contains(unit))
        {
            units.Remove(unit);
        }

        if (unit.priorityTarget == root)
            unit.priorityTarget = null;
    }

}
