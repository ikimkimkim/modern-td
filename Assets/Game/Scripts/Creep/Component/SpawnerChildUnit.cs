﻿using UnityEngine;
using System.Collections;
using TDTK;

public class SpawnerChildUnit : MonoBehaviour {

    public UnitCreep rootUnit;
    public GameObject prefab;

    [SerializeField]
    [Range(0f, 1f)]
    private float DegradationProcent;
    [SerializeField]
    [Range(0f, 1f)]
    private float ScaleProcent=1;

    public Vector3 Offset;
    [SerializeField]
    private Vector2 RandomOffset;

    public float Interval = 1;
    //private float lastTimeSpawn;

    private Vector3 lastPos;

    public void OnEnable()
    {
        //lastTimeSpawn = Time.time;
        lastPos = rootUnit.getPosition;
    }

	void Update ()
    {
        if (rootUnit.dead)
            return;


        if(Vector3.Distance(lastPos,rootUnit.getPosition)>Interval)
        //if(Time.time-lastTimeSpawn>Interval)
        {
            Spawn();
            //lastTimeSpawn = Time.time;
            lastPos = rootUnit.getPosition;
        }
    }

    UnitCreep unit;
    private void Spawn()
    {
        Vector3 offset = getRandom;
        unit = ObjectPoolManager.Spawn(prefab, rootUnit.getPosition + offset, rootUnit.thisT.rotation).GetComponent<UnitCreep>();
        unit.waveID = rootUnit.waveID;
        int ID = SpawnManager.AddDestroyedSpawn(unit);
        unit.Init(rootUnit.path, ID, rootUnit.waveID, rootUnit);

        unit.fullHP = rootUnit.fullHP * DegradationProcent;
        //unit.fullShield = rootUnit.fullShield * DegradationProcent;
        unit.HP = unit.fullHP;
       // unit.shield = unit.fullShield;

        unit.pathOffset = offset;

        unit.thisT.localScale = unit.deffScale * ScaleProcent;
    }

    private Vector3 getRandom
    {
        get
        {
            return rootUnit.thisT.forward * Offset.x + new Vector3(Random.Range(-RandomOffset.x, RandomOffset.x), 0, Random.Range(-RandomOffset.y, RandomOffset.y));
        }
    }
}
