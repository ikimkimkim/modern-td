﻿using UnityEngine;
using System.Collections;
using TDTK;

public class StunImmunity : MonoBehaviour {

    public Unit rootUnit;

    [SerializeField]
    private float Multiplier = 0.8f;
    [SerializeField]
    private int Counter;

    private float realMultiplier;

    void Awake()
    {
        if (rootUnit == null)
        {
            rootUnit = GetComponent<Unit>();
            if (rootUnit == null)
                throw new System.ArgumentNullException("RootUnit is null");
        }
        rootUnit.onMyDestroyedE += DestroyedE;
        rootUnit.onMyInitE += InitE;
    }

    void OnDestroy()
    {
        rootUnit.onMyDestroyedE -= DestroyedE;
        rootUnit.onMyInitE -= InitE;
    }

    //EffectsManager объявляется только в инициализации
    private void InitE(Unit unit)
    {
        Counter = 0;
        realMultiplier = 1;
        rootUnit.EffectsManager.addMyEffectE += addEffectE;
        rootUnit.myInAttakInstanceInitE += RootUnit_myInAttakInstanceInitE;
    }


    private void DestroyedE(Unit unit)
    {
        rootUnit.EffectsManager.addMyEffectE -= addEffectE;
        rootUnit.myInAttakInstanceInitE -= RootUnit_myInAttakInstanceInitE;
    }

    private void RootUnit_myInAttakInstanceInitE(AttackInstance attackInstance)
    {
        attackInstance.MultiplierChanceStun *= realMultiplier;
    }

    private void addEffectE(BaseEffect effect)
    {
        if(effect.IDStack == IDEffect.StunControl)
        {
            Counter++;
            realMultiplier = Mathf.Pow(Multiplier, Counter);
        }
    }
}