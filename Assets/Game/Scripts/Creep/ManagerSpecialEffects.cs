﻿using UnityEngine;
using System.Collections;
using TDTK;

public class ManagerSpecialEffects : MonoBehaviour
{
    

    public Unit linkUnit;
    public GameObject prefabDeadEffect;
    public Transform pointDeadEffect;

    public GameObject prefabHealEffect;
    public float HealCooldownSpawn=2.5f;
    private float LastTimeSpawnHeal;

    public void Start()
    {
        if (linkUnit == null)
        {
            linkUnit = GetComponent<Unit>();
            if (linkUnit == null)
            {
                Debug.LogError("ManagerSpecialEffect not find unit conpanent!");
            }
        }
        if(pointDeadEffect==null)
            pointDeadEffect = linkUnit.thisT;

        if (prefabHealEffect == null)
        {
            var bdeffect = EffectDB.Load();
            prefabHealEffect = bdeffect[2];
        }
    }

    void OnEnable()
    {
        linkUnit.HealEvent += HealEffect;
    }

    void OnDisable()
    {
        linkUnit.HealEvent -= HealEffect;
    }

    public void SpawnDeadEffectAnimEvent()
    {
        //Debug.LogWarning("SpawnDeadEffectAnimEvent");
        IDeadUnitEffect effect = ObjectPoolManager.Spawn(prefabDeadEffect, pointDeadEffect.position, pointDeadEffect.rotation).GetComponent<IDeadUnitEffect>();
        if (effect != null)
            effect.InitDeadUnit(linkUnit);
    }

    public void HealEffect(float count)
    {
        if (LastTimeSpawnHeal + HealCooldownSpawn <= Time.time)
        {
            GameObject go = GameObject.Instantiate(prefabHealEffect);
            go.transform.position = linkUnit.thisT.position;
            go.transform.SetParent(linkUnit.thisT);
            GameObject.Destroy(go, 5);
            LastTimeSpawnHeal = Time.time;
        }
    }
}
