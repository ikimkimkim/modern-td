﻿using UnityEngine;
using System.Collections;
using TDTK;

[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(Rigidbody))]
public class NormalDeadUnit : MonoBehaviour, IDeadUnitEffect
{
    public Unit root;
    private float Damage;

    [Range(0f,1f)]
    public float Procent = 0.05f;

    public int countUnitDamage=1;
    private int counterUnit;

    public float TimeDot;
    public float ItervetDot;
    float countTik;
    [Range(0f, 1f)]
    public float ProcentDot;


    public void InitDeadUnit(Unit DeadUnit)
    {
        counterUnit = 0;
        root = DeadUnit;
        Damage = DeadUnit.fullHP * Procent;
        countTik = TimeDot / ItervetDot;
    }

    Unit unit;
    void OnTriggerEnter(Collider other)
    {
        unit = other.transform.GetComponent<Unit>();
        if (unit != null && root!=unit && unit.prefabID!=10 && unit.prefabID!=11 &&
            unit.EffectsManager.isActiveEffect(IDEffect.PoisonDot)==false)
        {
            if((unit.IsCreep && unit.GetUnitCreep.flying==false) || unit.IsCreep == false)
                ApplyEffect(unit);
        }
    }

    private void ApplyEffect(Unit unit)
    {
        unit.ApplyDamage(root, Damage,root.DamageType(),false);
        if(unit.dead == false)
            unit.EffectsManager.AddEffect(root, new PoisonDotEffect(countTik, ItervetDot, Damage * ProcentDot / countTik, root.damageType));
        
        counterUnit++;
        if (counterUnit >= countUnitDamage)
            ObjectPoolManager.Unspawn(gameObject);
    }
}
