﻿using UnityEngine;
using System.Collections;
using TDTK;

public interface IDeadUnitEffect
{
    void InitDeadUnit(Unit DeadUnit);

}
