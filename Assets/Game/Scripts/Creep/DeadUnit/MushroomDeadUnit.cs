﻿using UnityEngine;
using System.Collections;
using TDTK;

[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(Rigidbody))]
public class MushroomDeadUnit : MonoBehaviour, IDeadUnitEffect
{
    public Unit root;
    [Range(0f, 1f)]
    public float healProcent;

    public int CountTarget;


    public void InitDeadUnit(Unit DeadUnit)
    {
        root = DeadUnit;
        countHeal = 0;
    }

    Unit unit;
    void OnTriggerEnter(Collider other)
    {
        unit = other.transform.GetComponent<Unit>();
        if (unit != null && root != unit && unit.prefabID != 4 && unit.prefabID != 5)
        {
            if (unit.IsCreep && unit.GetUnitCreep.flying == false)
                ApplyEffect(unit);
        }
    }

    private int countHeal;
    private void ApplyEffect(Unit unit)
    {
        unit.AddHeal(healProcent*unit.fullHP);
        countHeal++;

        if (countHeal>=CountTarget)
            ObjectPoolManager.Unspawn(gameObject);
    }
}
