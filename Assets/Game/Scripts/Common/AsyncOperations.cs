﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AsyncOperations  {

    public delegate void ChangesProgressHandler(float progress, string nameProgress);

    public static IEnumerator ShowAsyncOperation(AsyncOperation async, string nameOperation = "", ChangesProgressHandler changesProgress = null)
    {
        if (async == null)
        {
            Debug.LogError(string.Format("AsyncOperation {0} is null",nameOperation));
            yield break;
        }
        MyLog.Log(string.Format("AsyncOperation {0} -> Progress start",nameOperation));
        while (!async.isDone)
        {
            MyLog.Log(string.Format("AsyncOperation {0} -> Progress {1}", nameOperation,async.progress * 100f));
            if(changesProgress != null)
            {
                changesProgress(async.progress, nameOperation);
            }
            if (async.progress >= 0.9f)
            {
                break;
            }

            yield return null;
        }

        MyLog.Log(string.Format("AsyncOperation {0} -> Progress complete",nameOperation));

        yield return null;
        yield break;
    }
}
