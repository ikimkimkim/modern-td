﻿using UnityEngine;
using System.Collections;
using System;

public static class StaticCommon
{
    public static bool DistanceLimit(Vector3 vector, Vector3 vector2,float maxDistance)
    {
        if (Mathf.Abs(vector.z - vector2.z) > maxDistance)
            return false;

        if (Mathf.Abs(vector.x - vector2.x) > maxDistance)
            return false;

        if (Vector3.Distance(vector,vector2) > maxDistance)
            return false;

        return true;
    }

    public static float DistanceYZero(this Vector3 vector, Vector3 vector2)
    {
        vector.y = 0;
        vector2.y = 0;
        return Vector3.Distance(vector, vector2);
    }

    public static Vector3 Rotation(this Vector3 vector, float ang)
    {
        Vector3 v = new Vector3(0,vector.y,0);
        v.x = vector.x * Mathf.Cos(ang) - vector.z * Mathf.Sin(ang);
        v.z = vector.x * Mathf.Sin(ang) + vector.z * Mathf.Cos(ang);
        return v;
    }

    public static Vector3 RandomOffset(this Vector3 vector, float offsetXZ)
    {
        return vector.RandomOffset(offsetXZ, 0, offsetXZ);
    }

    public static Vector3 RandomOffset(this Vector3 vector, float offsetX,float offsetY,float offsetZ)
    {
        return vector + new Vector3(UnityEngine.Random.Range(-offsetX, offsetX), UnityEngine.Random.Range(-offsetY, offsetY), UnityEngine.Random.Range(-offsetZ, offsetZ));
    }

    public static float RoundDamage(this float dmg)
    {
        if (dmg < 30)
            return (float) Math.Round(dmg, 2);
        else if (dmg < 100)
            return (float) Math.Round(dmg, 1);
        else
            return Mathf.Round(dmg);
    }

    public static string ToStringDmg(this float dmg)
    {
        if(dmg<1000)
        {
            return string.Format("{0}", dmg.RoundDamage());
        }
        else if(dmg<1000000)
        {
            return string.Format("{0}Т", (dmg / 1000).RoundDamage());
        }
        else if (dmg < 1000000000)
        {
            return string.Format("{0}М", (dmg / 1000000).RoundDamage());
        }
        return string.Format("{0}B", (dmg / 1000000000).RoundDamage());
    }
}
