﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct FloatSecurity
{
    private float _value,_rnd;
	public float Value {
        get {
            return _value / _rnd;
        }
        set {
            _rnd = Random.Range(2f, 1000000f);
            _value = value * _rnd;
        }
    }

}
