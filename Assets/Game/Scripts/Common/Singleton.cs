﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T: MonoBehaviour
{


    private static T _instance;
    public static T instance
    {
        get
        {
            if(_instance==null)
            {
                _instance = FindObjectOfType<T>();
                if(_instance==null)
                {
                    _instance = new GameObject("Singleton:" + typeof(T)).AddComponent<T>();
                }
                DontDestroyOnLoad(_instance.gameObject);
                (_instance as Singleton<T>).InitializationSingleton();
            }
            return _instance;
        }
        protected set
        {
            if (_instance == null)
            {
                _instance = value;
                DontDestroyOnLoad(_instance.gameObject);
                (_instance as Singleton<T>).InitializationSingleton();
            }
            else if(_instance != value)
            {
                Debug.LogError(string.Format("Singleton {0} is exist, i destroy value gameObject.", typeof(T)));
                Destroy(value.gameObject);
            }
        }
    }

    //Вызов при назвачении уникального instance
    protected abstract void InitializationSingleton();

    protected virtual void OnDestoroy()
    {

    }
}
