﻿using UnityEngine;

public static class MyLog
{

    public enum Type { editor,build}

    public static void LogWarning(string text, Type type = Type.editor)
    {
        switch (type)
        {
            case Type.build:
                Debug.LogWarning(text);
                break;
            case Type.editor:
#if UNITY_EDITOR
                Debug.LogWarning(text);
#endif
                break;
        }
    }

    public static void LogError(string text, Type type = Type.editor)
    {
        switch (type)
        {
            case Type.build:
                Debug.LogError(text);
                break;
            case Type.editor:
#if UNITY_EDITOR
                Debug.LogError(text);
#endif
                break;
        }
    }


    public static void Log(string text, Type type = Type.editor)
    {
        switch (type)
        {
            case Type.build:
                Debug.Log(text);
                
                break;
            case Type.editor:
#if UNITY_EDITOR
                Debug.Log(text);
#endif
                break;
        }
    }

}
