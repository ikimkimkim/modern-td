﻿using UnityEngine;
using System.Collections;
using TDTK;

public class PortalRctManager : MonoBehaviour {

    public GameObject lifeObj;



    public void EndBossSpawn()
    {
        GameControl.GainLife(0);
        if (lifeObj != null)
            lifeObj.SetActive(true);
        else
            Debug.LogError("ProtalRctManager: lifeObj is null!");
        SpawnManager.onSpawnEndBossE -= EndBossSpawn;
    }

	void Start ()
    {
        if (GameControl.GetGameType != _GameType.Life)
        {
            lifeObj.SetActive(false);
            SpawnManager.onSpawnEndBossE += EndBossSpawn;
        }
	}
}
