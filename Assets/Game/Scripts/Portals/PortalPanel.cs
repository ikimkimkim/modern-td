﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PortalPanel : MonoBehaviour {

    [SerializeField] private GameObject _tooltip;
    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void PortalSelect()
    {
        CardAndLevelSelectPanel.SetDataLevel("На выживание", -1, string.Empty,0);
       // GetComponent<ButtonLoadLevelAsync>().LoadLevel();
    }

    public void BigPortalSelect()
    {
        CardAndLevelSelectPanel.SetDataLevel("На прохождение" , -2, string.Empty, 0);
       // GetComponent<ButtonLoadLevelAsync>().LoadLevel();
    }

    public void TooltipSetActive(bool active)
    {
        if(_tooltip.activeSelf != active && PlayerManager._instance.GetLevelsCountComplele() < 9)
            _tooltip.SetActive(active);
    }
   

}
