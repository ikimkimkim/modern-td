﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Translation;
using UnityEngine.SceneManagement;

public class InstructionsPopup : MonoBehaviour
{
    public GameObject[] States;
    public Text[] Captions;
    public Text[] SkipButtonText;
    public GameObject Screen;
    public float ShowDelay = 3f;
    int _state;
   
    void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == /*Application.loadedLevel ==*/ 3)
        {
            //StartCoroutine(ShowAfter(ShowDelay));
        }
        else if (SceneManager.GetActiveScene().buildIndex /*!= Application.loadedLevel*/ != 0)
        {
            Destroy(gameObject);
        }
    }

    IEnumerator ShowAfter(float time)
    {
        yield return new WaitForSeconds(time);
        _state = -1;
        for (int i = 0; i < States.Length; i++)
        {
            States[i].SetActive(false);
        }
        NextState();
        yield break;


    }
    void OnEnable()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0) // Application.loadedLevel == 0)
        {
            /*Translator trans = TranslationEngine.Instance.Trans;
            for (int i = 0; i < Captions.Length; i++)
            {
                Captions[i].text = trans["Instractions.Caption"];
            }
            for (int i = 0; i < SkipButtonText.Length; i++)
            {
                SkipButtonText[i].text = trans["Instartctions.Button.Close"];
            }
            _state = -1;
            for (int i = 0; i < States.Length; i++)
            {
                States[i].SetActive(false);
            }
            NextState();*/
        }
    }
    void OnDisable()
    {
        TimeScaleManager.RefreshTimeScale();
    }
    public void NextState()
    {
        TimeScaleManager.SetTimeScale(0);
        Screen.SetActive(true);
        _state++;
        if (_state != 0)
        {
            States[_state - 1].SetActive(false);
        }
        States[_state].SetActive(true);
    }
    public void Skip()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0) //Application.loadedLevel == 0)
        {
            _state = -1;
            for (int i = 0; i < States.Length; i++)
            {
                States[i].SetActive(false);
            }
            Screen.SetActive(false);
            gameObject.SetActive(false);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Close()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0) //Application.loadedLevel == 0)
        {
            GetComponent<ButtonQuickPlay>().CheckLevel();
            GetComponent<ButtonLoadLevelAsync>().LoadLevel();
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
