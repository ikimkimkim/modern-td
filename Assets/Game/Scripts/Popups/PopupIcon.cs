﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TDTK;
using Translation;

public class PopupIcon : MonoBehaviour
{


    public GameObject UnitPopupPrefab;
    public Text ButtonText;
    public Image Icon;
    [HideInInspector]
    public RectTransform Transform;
    float _lifeTime = 60f;

    Unit _newUnit;
    NewUnitPopupManager _popupManager;
    public void Initialize(Unit newUnit, NewUnitPopupManager popupManger)
    { Translator trans = TranslationEngine.Instance.Trans;
        _newUnit = newUnit;
        Icon.sprite = _newUnit.iconSprite;
        _popupManager = popupManger;
        ButtonText.text = trans["NewUnitIcon.Text"].Replace("\\n","\n");
        // Icon.sprite = _newUnit.iconSprite;
    }
    void Awake()
    {
        Transform = GetComponent<RectTransform>();
    }
    void Update()
    {
        if (!_popupManager.NeedToMove())
        {
            _lifeTime -= Time.deltaTime;
            if (_lifeTime <= 0)
            {
                _popupManager.RemoveIcon(this);
                Destroy(gameObject);
            }
        }
    }
    public void ShowInfo()
    {
        if (!_popupManager.NeedToMove())
        {
            Translator trans = TranslationEngine.Instance.Trans;
            NewUnitPopup newPopUp = (Instantiate(UnitPopupPrefab) as GameObject).GetComponent<NewUnitPopup>();
            RectTransform newPopUpTrasform = newPopUp.GetComponent<RectTransform>();
            // newPopUpTrasform.parent = Transform.parent.parent;
            newPopUpTrasform.SetParent(_popupManager.transform, false);
            newPopUpTrasform.position = new Vector3(Screen.width / 2, Screen.height / 2, 0);
            newPopUp.UnitName.text = trans[_newUnit.unitName];
            newPopUp.UnitIcon.sprite = _newUnit.iconSprite;
            newPopUp.UnitDesc.text = trans[_newUnit.desp];//.Replace("\\n","\n");

            //newPopUp.UnitCharacteristics.text = trans[_newUnit.characteristic].Replace("\\n", "\n");
            newPopUp.UnitCharacteristics.text = "Против него нужен:";
            ArmorType at= DamageTable.GetArmorTypeInfo(_newUnit.ArmorType());
            newPopUp.UnitArmor.text = at.name;
            DamageType[] dt = DamageTable.GetAllDamageType().ToArray();
            float max = 0;
            float now = 0;
            newPopUp.UnitVulnerable.text = "";
            for (int j = 0; j < dt.Length; j++)
            {
                now = DamageTable.GetModifier(_newUnit.ArmorType(), j);
                if (max < now)
                {
                    max = now;
                }
            }
            for (int j = 0; j < dt.Length; j++)
            {
                now = DamageTable.GetModifier(_newUnit.ArmorType(), j);
                if (max == now)
                {
                    newPopUp.UnitVulnerable.text += "" +dt[j].name+ " урон\n\r";
                }
            }
            

            GameControl.PauseGame();
            _popupManager.RemoveIcon(this);
            _popupManager.OnIconClick();
            Destroy(gameObject);
        }
    }
}
