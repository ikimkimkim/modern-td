﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TDTK;
using Translation;

public class NewUnitPopup : MonoBehaviour
{
    public Image UnitIcon;
    public Text UnitName;
    public Text UnitDesc;
    public Text UnitArmor;
    public Text UnitCharacteristics;
    public Text UnitVulnerable;
    void OnEnable()
    {
        TimeScaleManager.SetTimeScale(0);
    }
    void OnDisable()
    {
        TimeScaleManager.RefreshTimeScale();
    }
    public void Close()
    {
        Destroy(gameObject);
        GameControl.ResumeGame();
    }

    public void Show(UnitCreep creep, Transform parent)
    {
        Translator trans = TranslationEngine.Instance.Trans;
        NewUnitPopup newPopUp = (Instantiate(this.gameObject) as GameObject).GetComponent<NewUnitPopup>();
        RectTransform newPopUpTrasform = newPopUp.GetComponent<RectTransform>();
        // newPopUpTrasform.parent = Transform.parent.parent;
        newPopUpTrasform.SetParent(parent, false);
        newPopUpTrasform.position = new Vector3(Screen.width / 2, Screen.height / 2, 0);
        newPopUp.UnitName.text = trans[creep.unitName];
        newPopUp.UnitIcon.sprite = creep.iconSprite;
        newPopUp.UnitDesc.text = trans[creep.desp];//.Replace("\\n","\n");

        //newPopUp.UnitCharacteristics.text = trans[_newUnit.characteristic].Replace("\\n", "\n");
        newPopUp.UnitCharacteristics.text = "Против него нужен:";
        ArmorType at = DamageTable.GetArmorTypeInfo(creep.ArmorType());
        newPopUp.UnitArmor.text = at.name;
        DamageType[] dt = DamageTable.GetAllDamageType().ToArray();
        float max = 0;
        float now = 0;
        newPopUp.UnitVulnerable.text = "";
        for (int j = 0; j < dt.Length; j++)
        {
            now = DamageTable.GetModifier(creep.ArmorType(), j);
            if (max < now)
            {
                max = now;
            }
        }
        for (int j = 0; j < dt.Length; j++)
        {
            now = DamageTable.GetModifier(creep.ArmorType(), j);
            if (max == now)
            {
                newPopUp.UnitVulnerable.text += "" + dt[j].name + " урон\n\r";
            }
        }


        GameControl.PauseGame();
    }
}
