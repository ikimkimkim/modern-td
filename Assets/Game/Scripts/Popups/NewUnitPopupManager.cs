﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TDTK;
/// <summary>
/// Показывает иконку нового юнита при его появлении 
/// </summary>
public class NewUnitPopupManager : MonoBehaviour
{
    [HideInInspector]
    public List<GameObject> NewUnits = new List<GameObject>();
    public GameObject PopupIconPrefab;
    private float Delay=0;
    public float OffsetY;
    public float SpeedY;

    public delegate void IconShown();
    public static event IconShown onIconShownE;

    public delegate void IconClick();
    public static event IconClick onIconClickE;

    List<PopupIcon> _icons = new List<PopupIcon>();
    List<PopupIcon> _iconsToAdd = new List<PopupIcon>();
    bool _needToMove = false;
    void Start()
    {
       NewUnits = GameControl.instance.GetComponent<GameControlHelper>().NewUnits;
    }
    public bool NeedToMove()
    {
        return _needToMove;
    }
    void OnEnable()
    {
        SpawnManager.onSpawnUnitE += OnUnitSpawn;
    }
    void OnDisable()
    {
        SpawnManager.onSpawnUnitE -= OnUnitSpawn;
    }
    public void RemoveIcon(PopupIcon icon)
    {
        _icons.Remove(icon);
    }
    public void OnIconClick()
    {
        if (onIconClickE != null)
        onIconClickE();
    }
    public void OnUnitSpawn(Unit unit)
    {
        // Debug.Log("OnUnitSpawn");
        for (int i = 0; i < NewUnits.Count; i++)
        {
            //Debug.Log(NewUnits[i].name + " " + unit.name);
            if (NewUnits[i].GetComponent<Unit>().unitName.Equals(unit.unitName))
            {
                //  Debug.Log("OK!!");
                StartCoroutine(ShowIcon(NewUnits[i].GetComponent<Unit>()));
                NewUnits.RemoveAt(i);
                return;
            }
        }
    }

    IEnumerator ShowIcon(Unit unit)
    {
        yield return new WaitForSeconds(Delay);
        PopupIcon newIcon = (Instantiate(PopupIconPrefab) as GameObject).GetComponent<PopupIcon>();
        newIcon.Initialize(unit, this);
        newIcon.Transform.SetParent(GetComponent<RectTransform>(), false);
        newIcon.Transform.localPosition = Vector3.zero;
        if (_icons.Count > 0)
        {
            newIcon.gameObject.SetActive(false);
            _iconsToAdd.Add(newIcon);
            _needToMove = true;
        }
        else
        {
            _icons.Add(newIcon);
            if (onIconShownE != null)
                onIconShownE();
        }
        yield break;
    }
    void Update()
    {
        if (_needToMove)
        {
            bool allMoved = true;
            for (int i = 0; i < _icons.Count; i++)
            {
               // Debug.Log(_icons[i].Transform.localPosition.y - OffsetY * (_icons.Count - i));
                if (_icons[i].Transform.localPosition.y - OffsetY * (_icons.Count - i) > 0f)
                {
                    allMoved = false;
                    _icons[i].Transform.localPosition += new Vector3(0, SpeedY, 0) * Time.deltaTime;
                }
                else
                {
                    _icons[i].Transform.localPosition = new Vector3(_icons[i].Transform.localPosition.x, OffsetY * (_icons.Count - i), _icons[i].Transform.localPosition.z);
                }
            }
            if (allMoved)
            {
                var icon = _iconsToAdd[0];
                _iconsToAdd.RemoveAt(0);
                _icons.Add(icon);
                icon.gameObject.SetActive(true);
                if (onIconShownE != null)
                    onIconShownE();
                if (_iconsToAdd.Count != 0)
                {
                    _needToMove = true;
                }
                else
                {
                    _needToMove = false;
                }
            }
        }

    }

}
