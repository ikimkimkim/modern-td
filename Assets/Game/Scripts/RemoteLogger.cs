﻿using UnityEngine;
using System.Collections;
using System.IO;
/// <summary>
/// Крутой класс который скидывает все ошибки в гугл таблицу  гайд о том как настроить https://habrahabr.ru/post/262901/
/// </summary>
public class RemoteLogger : MonoBehaviour
{
#if UNITY_IOS || UNITY_ANDROID
    string url = "https://script.google.com/macros/s/AKfycbz0_YAmaA1c-CYBOPNd4HrnA-ey_-qEEYTMKSCDxIHjgcG1dD0/exec"; // Your URL copy here

    static GameObject _instance;
    void Awake()
    {
        if (_instance != null && _instance != gameObject)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            _instance = gameObject;
        }

        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        Debug.Log("Hello world!");
    }

    //get method here http://forum.antichat.ru/showthread.php?t=290347
    string UrlEncode(string instring)
    {
        StringReader strRdr = new StringReader(instring);
        StringWriter strWtr = new StringWriter();
        int charValue = strRdr.Read();
        while (charValue != -1)
        {
            if (((charValue >= 48) && (charValue <= 57)) // 0-9
            || ((charValue >= 65) && (charValue <= 90)) // A-Z
            || ((charValue >= 97) && (charValue <= 122))) // a-z
            {
                strWtr.Write((char)charValue);
            }
            else if (charValue == 32) // Space
            {
                strWtr.Write("+");
            }
            else
            {
                strWtr.Write("%{0:x2}", charValue);
            }
            charValue = strRdr.Read();
        }
        return strWtr.ToString();
    }

    void SendLog(string mes)
    {
        string t_url = url + "?p=" + UrlEncode(mes);
        WWW www = new WWW(t_url);
        StartCoroutine(WaitForRequest(www));
    }

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;
        // check for errors
        if (www.error == null)
        {
            //OK
        }
        else
        {
            //Error
        }
    }

    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        if (type == LogType.Error || type == LogType.Exception)
        {
            SendLog(logString + " STAK TRACE  " + stackTrace);
        }
    }
#endif
}