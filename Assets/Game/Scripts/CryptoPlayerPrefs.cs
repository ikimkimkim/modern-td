﻿using System;
using UnityEngine;
public static class CryptoPlayerPrefs
{
    static string _password = "r1$1D0@qHEcbTQgJ";
    public static void Save(string key, Type type, object value)
    {

        Debug.Log("CryptoPlayerPrefs save");
        PlayerPrefs.SetString(key, AESCoder.Encrypt(StringSerializationAPI.Serialize(type, value), _password));
    }
    public static bool HasKey(string key)
    {
        return PlayerPrefs.HasKey(key);
    }
    public static object Load(string key, Type type)
    {
        Debug.Log("CryptoPlayerPrefs load");
        return StringSerializationAPI.Deserialize(type, AESCoder.Decrypt(PlayerPrefs.GetString(key), _password));
    }
}
