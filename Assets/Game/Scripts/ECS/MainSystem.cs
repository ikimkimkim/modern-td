﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SystemUpdate
{
    public class MainSystem : Singleton<MainSystem>
    {

        [SerializeField] private List<IItemUpdate> itemUpdates;

        protected override void InitializationSingleton()
        {
            itemUpdates = new List<IItemUpdate>();
        }


        void Start()
        {
            instance = this;
        }


        void Update()
        {
            for (int i = 0; i < itemUpdates.Count; i++)
            {          
                itemUpdates[i].SystemUpdate();
            }
        }


        public static void AddItem(IItemUpdate item)
        {
            instance._AddItem(item);
        }
        private void _AddItem(IItemUpdate item)
        {
            //Debug.Log("Add item " + (item as MonoBehaviour).name );
            itemUpdates.Add(item);
        }

        public static void RemoveItem(IItemUpdate item)
        {
            //Debug.Log("Remove item " + (item as MonoBehaviour).name);
            instance._RemoveItem(item);
        }
        private void _RemoveItem(IItemUpdate item)
        {
            itemUpdates.Remove(item);
        }

        private void OnDestoy()
        {
            itemUpdates.Clear();
        }
    }
}
