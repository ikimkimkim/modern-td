﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SystemUpdate
{
    public interface IItemUpdate
    {
        void SystemUpdate();
    }
}
