﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class MobileData
{
    public PlayerData Data;
    public List<int> CrystalsGainedByLevel;
    public MobileData(PlayerData data, List<int> crystalGained)
    {
        Data = data;
        CrystalsGainedByLevel = crystalGained;
    }
}
