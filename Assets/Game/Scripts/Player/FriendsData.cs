﻿

public class FriendsData
{

    public Friend[] friends;
    public int can_take_help;

    public FriendsData()
    {

    }

    public FriendsData(int[] ids)
    {
        friends = new Friend[ids.Length];

        for (int i = 0; i < ids.Length; i++)
        {
            friends[i] = new Friend(); 
            friends[i].id = ids[i].ToString();
        }
    }
}

public class Friend
{
    public string id;
    public string name;
    public string ava;

    public Friend()
    {

    }
}
