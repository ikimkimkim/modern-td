﻿using System;
using System.Collections.Generic;

[System.Serializable]
public class PlayerData
{
    public struct freeChestData
    {
        public int count;
        public float time;
    }
    public struct starsChestData
    {
        public int count;
        public int need;
    }
    public struct starsChice
    {
        public int timer;
        public int[] choice;
    }

    public int Gold;
    public int[] Energy;
    public List<ChestsData> chests = new List<ChestsData>();
    public Dictionary<string,TowersData> towers = new Dictionary<string, TowersData>();
    public Dictionary<string, EventGiftsData> event_gifts = new Dictionary<string, EventGiftsData>();




    public List<LevelData> Levels = new List<LevelData>();
    public List<int> PurchasedUpgradesIDs = new List<int>();
    public int Crystals = 0;
    public Dictionary<string, int> settings = new Dictionary<string, int>(); // quality, quality_by_user, music_on, sound_on
    public List<int> locations = new List<int>();
    public int daily_bonus=-1;
    public int daily_bonus_ttl=-1;
    public int available_help_friends;
    public int max_great_adv_level;
    public int chest_subscription;

    public freeChestData free_chest;
    public starsChestData stars_chest;
    /*public int free_chest_count;
    public float free_chest_time;
    public int stars_chest_count;
    public int stars_chest_need;*/
    public TopDaysData day_tops;

    public DateTime timeSet;
    public Dictionary<string, int> craft_resources = new Dictionary<string, int>();
    public List<int[]> grade_levels = new List<int[]>();
    public starsChice stars_grade_choice;
    public SaveProgressData has_not_ended_session;
    public string sig_key;

    public int QualityLevel
    {
        get
        {
            if (settings.ContainsKey("quality"))
            {
                return settings["quality"];
            }
            return 5; //по умолчанию максимальное качество
        }
        set
        {
            if (settings.ContainsKey("quality"))
            {
                settings["quality"] = value;
            }
            else
            {
                settings.Add("quality", value);
            }
        }
    }
    public int PlayerManuallySetQualityLevel
    {
        get
        {
            if (settings.ContainsKey("quality_by_user"))
            {
                return settings["quality_by_user"];
            }
            return 0; //по умолчанию false
        }
        set
        {
            if (settings.ContainsKey("quality_by_user"))
            {
                settings["quality_by_user"] = value;
            }
            else
            {
                settings.Add("quality_by_user", value);
            }
        }
    }
    public int Music
    {
        get
        {
            if (settings.ContainsKey("music_on"))
            {
                return settings["music_on"];
            }
            return 1; //по умолчанию true
        }
        set
        {
            if (settings.ContainsKey("music_on"))
            {
                settings["music_on"] = value;
            }
            else
            {
                settings.Add("music_on", value);
            }
        }
    }
    public int Sound
    {
        get
        {
            if (settings.ContainsKey("sound_on"))
            {
                return settings["sound_on"];
            }
            return 1; //по умолчанию true
        }
        set
        {
            if (settings.ContainsKey("sound_on"))
            {
                settings["sound_on"] = value;
            }
            else
            {
                settings.Add("sound_on", value);
            }
        }
    }

    public PlayerData()
    {

    }
    public PlayerData Copy()
    {
        PlayerData CopyPlayerData = new PlayerData();
        for (int i = 0; i < Levels.Count; i++)
        {
            CopyPlayerData.Levels.Add(new LevelData(Levels[i].id, Levels[i].stars));
        }
        for (int i = 0; i < PurchasedUpgradesIDs.Count; i++)
        {
            CopyPlayerData.PurchasedUpgradesIDs.Add(PurchasedUpgradesIDs[i]);
        }
        CopyPlayerData.Crystals = Crystals;
        return CopyPlayerData;
    }
}
