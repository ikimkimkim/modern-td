﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Security.Cryptography;
using FullSerializer;
using TDTK;
using UnityEngine.SceneManagement;
/// <summary>
/// Содержит и управляет информацией об игроке 
/// </summary>



public class PlayerManager : MonoBehaviour
{
    public static bool isPremium
    {
        get { return _instance != null && _instance._playerData.chest_subscription == 1; }
    }

    public PlayerData _playerData { get; private set; }
    public static TopDaysData topDaysData { get { return _instance._playerData.day_tops; } }
    public static SaveProgressData DataSaveProgress { get { return _instance._playerData.has_not_ended_session; } }
    public static Dictionary<string, EventGiftsData> getGiftsData { get { return _instance._playerData.event_gifts; } }
    public FriendsData _friendsData = new FriendsData();
    ServerManager _serverManager;
    public delegate void OnCrystalsChanged(int currentCrysals);
    public static event OnCrystalsChanged OnCrystalsChangedE;

    public static event Action InitializationСompleted;

    public static event Action<int> OnUpgradeBuy;
    public static event Action<List<int>> OnUpgradeSell;

    public static event Action OnPackBought;


    List<int> _crystalsGainedByLevel = new List<int>();
    public static PlayerManager _instance;
    //public int crystals;
    public StoreItemData[] crystals;
    public Dictionary<string, LocationItemData> BuyLocations = new Dictionary<string, LocationItemData>() {
     { "2", new LocationItemData("levels2", "12 голосов") },
     { "3", new LocationItemData("levels3", "12 голосов") },
     { "4", new LocationItemData("levels4", "12 голосов") },
     { "5", new LocationItemData("levels5", "12 голосов") },
     { "6", new LocationItemData("levels6", "12 голосов") },
     { "7", new LocationItemData("levels7", "12 голосов") },
     { "8", new LocationItemData("levels8", "12 голосов") },
     { "9", new LocationItemData("levels9", "12 голосов") },
     { "10", new LocationItemData("levels10", "12 голосов") },
     { "11", new LocationItemData("levels11", "12 голосов") },
     { "12", new LocationItemData("levels12", "12 голосов") }
    };
    public int Times;
    [TextArea(1, 10)]
    public string DataFake = "";
    public int limitFrameRate = 100;

    public static DataManager DM { get; private set; }

    public static List<Card> getTowerCards
    {
        get
        {
            return CardsList.FindAll(i => i.Type == _CardType.Tower);
        }
    }

    public static List<Card> getAbilityCards
    {
        get
        {
            return CardsList.FindAll(i => i.Type == _CardType.Ability);
        }
    }
    public static List<Card> getHeroCards
    {
        get
        {
            return CardsList.FindAll(i => i.Type == _CardType.Hero);
        }
    }


    public static List<Card> CardsList
    {
        get; private set;
    }

    public static List<Chest> ChestList
    {
        get; private set;
    }

    public static int CountGolds { get { return _instance._playerData.Gold; } }
    public static int CountCrystals { get { return _instance._playerData.Crystals; } }

    public static int CountLevelBigPortal { get { return _instance._playerData.max_great_adv_level; } }
    public static int CountEnergy { get { return _instance._playerData.Energy[0]; } }
    public static int TimeAddEnergy { get { return _instance._playerData.Energy[1]; } }



    public static int TimeStarsCard { get { return _instance._playerData.stars_grade_choice.timer - (int)getTimeLeftForSetData.TotalSeconds; } }
    public static int[] IDsStarsCard { get { return _instance._playerData.stars_grade_choice.choice; } }
    public static string sig_key { get { return _instance._playerData.sig_key; } set { _instance._playerData.sig_key = value; } }

    void Awake()
    {
        if (_instance == null)
        {
#if UNITY_EDITOR
            Application.targetFrameRate = 1000;
#endif
            _instance = this;
            DontDestroyOnLoad(gameObject);
            InitializationDataManager();
            MyStart();
        }
        else
        {
            if (_instance != this)
            {
                Destroy(gameObject);
                return;
            }
        }

    }

    void OnDestroy()
    {
        if(_instance == this)
            StupidServer.CleatData();
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
    }

    public int LastIdScenes { get; private set; }
    public int NowIdScenes { get; private set; }

    private void SceneManager_sceneLoaded(Scene scene, LoadSceneMode mode)
    {
        LastIdScenes = NowIdScenes;
        NowIdScenes = scene.buildIndex;
    }

    public void MyStart()
    {
        MyLog.Log("MyStart PM");
        _playerData = new PlayerData();
        _serverManager = GetComponent<ServerManager>();

        StupidServer.Init();

#if UNITY_IOS || UNITY_ANDROID
        LoadMobile();
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
#endif
#if UNITY_EDITOR

        MyLog.Log("Applay data Fake");
        string data = DataFake;
        if (data.Length > 1)
        {
            var obj = GameObject.FindGameObjectWithTag("SocialManager");
            if (obj == null)
            {
                SocialManager.Instance.userData(data);
            }
            else
            {
                var sm = obj.GetComponent<SocialManager>();
                sm.userData(data);
            }
            /* RecievedData recieved = new RecievedData();
            recieved = (RecievedData)StringSerializationAPI.Deserialize(recieved.GetType(), data);
            ServerManager._Domain = recieved.request_data.domain;
            SocialManager.buyConfig = recieved.buy_conf;
            SocialManager.buyCrystalConfig = recieved.buy_crystals_conf;
            SocialManager.chest_info_conf = recieved.chest_info_conf;
            InitCraftCardConfig(recieved.craft_cards_conf);
            InitCardPriceSellConfig(recieved.sell_price_conf);


            SetPlayerData(recieved.user_info);*/


        }
        //DM.getListCards(CallBackLoadCards);
        //DM.getChest(getChests);
        //CountLevelBigPortal = 10;
        _playerData.Energy = new int[2];
        _playerData.Energy[0] = 8;
        _playerData.Energy[1] = 70;



        /*for (int i = 0; i < 11; i++)
             OnLevelEnd(i + 1,3);*/


        _instance._playerData.max_great_adv_level = 100; /**/

        /*for (int i = 200; i < 212; i++)
            OnLevelEnd(i + 1, 3);

        _playerData.locations.Add(2);*/

        /*_friendsData = new FriendsData();
        _friendsData.friends = new Friend[20];
        for (int i = 0; i < 20; i++)
        {
            _friendsData.friends[i] = new Friend();
            _friendsData.friends[i].id = "1111"+i;
            _friendsData.friends[i].name = "Naamee"+i+ " family name";
            _friendsData.friends[i].ava = @"http://cs10114.vk.me/u203654/e_8cf93725.jpg";
        }*/



#endif

    }
    public void getChests(List<Chest> Chests)
    {
        ChestList = Chests;
    }

    public void EnergiChange(int addEnergi)
    {
        _playerData.Energy[0] += addEnergi;
    }

    private void InitializationDataManager()
    {
#if (UNITY_WEBGL || UNITY_WEBPLAYER) && !UNITY_EDITOR
        DM = new VKDM();
#endif
#if UNITY_ANDROID || UNITY_IOS
        DM = new MobileDM();
#endif
#if UNITY_EDITOR
        DM = new EditorDM();
#endif

        DM.Initialization();
    }

    public static int GetCraftResources(int id)
    {
        return _instance._GetCraftResources(id);
    }

    private int _GetCraftResources(int id)
    {
        return _playerData.craft_resources[id.ToString()];
    }


    public List<CraftCardConfig> CardCraftConfArray;
    public void InitCraftCardConfig(Dictionary<string, Dictionary<string, Dictionary<string, float>>> craft_cards_conf)
    {

        CardCraftConfArray = new List<CraftCardConfig>();

        foreach (var id in craft_cards_conf.Keys)
        {
            foreach (var rareness in craft_cards_conf[id].Keys)
            {
                CraftCardConfig cc = new CraftCardConfig();
                cc.id = int.Parse(id);
                cc.rareness = (_CardRareness)int.Parse(rareness) - 1;
                cc.price = new float[4];
                foreach (var res in craft_cards_conf[id][rareness].Keys)
                {
                    cc.price[int.Parse(res) - 1] = craft_cards_conf[id][rareness][res];
                }
                CardCraftConfArray.Add(cc);
            }
        }
    }

    public List<CardSaleConfig> CardPriceSellConfigArray;
    public void InitCardPriceSellConfig(Dictionary<string, Dictionary<string, float[]>> sell_price_conf)
    {
        CardPriceSellConfigArray = new List<CardSaleConfig>();

        int index = -1;
        foreach (var rareness in sell_price_conf.Keys)
        {
            CardSaleConfig cs = new CardSaleConfig();
            cs.rareness = (_CardRareness)int.Parse(rareness) - 1;
            cs.resourseMin = new float[4];
            cs.resourseMax = new float[4];

            foreach (var res in sell_price_conf[rareness].Keys)
            {
                index = int.Parse(res) - 1;
                cs.resourseMin[index] = sell_price_conf[rareness][res][0];
                cs.resourseMax[index] = sell_price_conf[rareness][res][1];
            }
            CardPriceSellConfigArray.Add(cs);
        }
    }

    public void SetCards(List<Card> cards)
    {
        CardsList = cards;
        Debug.Log("SetCards " + CardsList[0].NextLevelTower[0]);
    }

    public void SetFiendsData(FriendsData data)
    {
        _friendsData = data;
    }

    private List<UnitTower> bdTower;
    private List<Ability> bdAbility;
    private List<UnitHero> bdHero;
    private List<CardUpgradeData> dbCardUpgrade;
    public static TimeSpan getTimeLeftForSetData
    {
        get { return _instance._getTimeLeftForSetData; }
    }
    public TimeSpan _getTimeLeftForSetData
    {
        get { return DateTime.Now - PlayerManager._instance._playerData.timeSet; }
    }
    public static void SetPlayerData(PlayerData data)
    {
        _instance._SetPlayerData(data);
    }
    public void _SetPlayerData(PlayerData data)
    {
        if (data == null)
        {
            MyLog.LogError("Set Player Data in null!", MyLog.Type.build);
            return;
        }
            
        if (_playerData.locations.Count < data.locations.Count)
        {
            if (OnPackBought != null)
            {
                //Debug.Log("OnPackBought called!");
                OnPackBought();
            }
        }

        _playerData = data;
        _playerData.timeSet = DateTime.Now;

        MyLog.Log("Player data set",MyLog.Type.build);
        MyLog.Log(StringSerializationAPI.Serialize(_playerData.GetType(), _playerData));

        if (CardsList == null)
            CardsList = new List<Card>();

        bool LevelPanelShow = CardAndLevelSelectPanel.instance != null && CardAndLevelSelectPanel.instance.isShow;
        //для сохранения последне выбраных карт, если проиходит апгрейд карты а выбраные карты изменены.
        if (LevelPanelShow)
        {
            foreach (var tower in _playerData.towers)
            {
                tower.Value.selected = 0;
                Card card = CardsList.Find(c => c.disableInBuildManager == false && c.ID == tower.Key.ToString());
                if (card != null)
                    tower.Value.selected = 1;
            }
        }

        CraftLevelData.Init(_playerData.grade_levels);

        CardsList.Clear();

        bdTower = TowerDB.Load();
        bdAbility = AbilityDB.Load();
        bdHero = HeroDB.Load();
        dbCardUpgrade = DataBase<CardUpgradeData>.Load(DataBase<CardUpgradeData>.NameBase.CardUpgrade);

        int i = 0;
        int indexTower = 0;
        
        foreach (var tower in _playerData.towers)
        {
            if (tower.Value.type == 0)
                Debug.LogError("PM-> type card is 0");

            //Если карт 0
            if (tower.Value.count == 0)
                continue;


            bool initNextLevel = true;
            if (tower.Value.type > 200)
            {
                if (tower.Value.type > 400)
                {
                    if (tower.Value.type > 500)
                    {
                        CardsList.Add(new Card(tower.Value, dbCardUpgrade[tower.Value.type - 501]));
                        initNextLevel = false;
                    }
                    else
                    {
                        CardsList.Add(new Card(tower.Value, bdHero[tower.Value.type - 401].gameObject, _CardType.Hero, tower.Value.type - 401));
                    }
                }
                else
                {
                    CardsList.Add(new Card(tower.Value, bdAbility[tower.Value.type - 201]));
                }
            }
            else
            {
                CardsList.Add(new Card(tower.Value, bdTower[tower.Value.type - 1].gameObject,_CardType.Tower,indexTower));
                indexTower++;
            }

            if (initNextLevel)
            {
                foreach (Card c in CardsList[i].NextLevelTower)
                {
                    CardGenerator.setPropertiesNextLevelCard(c, CardsList[i]);
                    CardGenerator.setTowerObjNextLevelCard(c, CardsList[i]);
                }
            }

            i++;
        }

        if (ChestList == null)
            ChestList = new List<Chest>();
        else
            ChestList.Clear();

        foreach (ChestsData chestData in _playerData.chests)
        {
            ChestList.Add(new Chest(chestData));
            //ChestList.Add(new Chest(int.Parse(chestData.id), (Chest.TypeChest)(int.Parse(chestData.type_id) - 1), chestData.time_decrease_pcnt, chestData.status, chestData.time_to_open, chestData.open_price));
        }

        /*if (QualitySettings.GetQualityLevel() != _playerData.QualityLevel)
        {
            QualitySettings.SetQualityLevel(_playerData.QualityLevel); // не факт что нужно 
        }*/
        //_playerData.PurchasedUpgradesIDs = new List<int> { 1/*, 1,30,30, 1, 1, 2,10,11,10, 2, 4, 5, 6, 3, 7, 8, 3, 3, 2, 1, 22, 22, 27, 28 */};
        if (_playerData.PurchasedUpgradesIDs == null)
            _playerData.PurchasedUpgradesIDs = new List<int>();

#if UNITY_EDITOR
        _playerData.stars_grade_choice.timer = 40;
        //_playerData.PurchasedUpgradesIDs.Clear();
        //_playerData.PurchasedUpgradesIDs.AddRange(new int[] { 37, 38 });
#endif

        StarsPerk.SetData(_playerData.PurchasedUpgradesIDs);


        if (OnCrystalsChangedE != null)
        {
            OnCrystalsChangedE(_playerData.Crystals);
        }

        if (InitializationСompleted != null)
            InitializationСompleted();

    }

    public static int GetLevelCountComplete(int countMinStars) { if (_instance != null) return _instance._GetLevelCountComplete(countMinStars); else return 0; }
    public int _GetLevelCountComplete(int countMinStars)
    {
        return _playerData.Levels.FindAll(i => i.stars >= countMinStars).Count;
    }
    public int GetLevelsCountComplele()
    {
        return _playerData.Levels.Count;
    }
    public int GetLevelsCount(int packID)
    {
        int count = 0;
        for (int i = 0; i < _playerData.Levels.Count; i++)
        {
            if (GetLevelPackID(_playerData.Levels[i].id) == packID)
            {
                count++;
            }
        }
        return count;
    }

    public void OnLevelEnd(int level, int stars)
    {
#if (UNITY_WEBGL || UNITY_WEBPLAYER) 
        /*if (stars >=0)
        {
            List<StringPair> extra = new List<StringPair>();
            extra.Add(new StringPair("level_id", level.ToString()));
            extra.Add(new StringPair("stars", stars.ToString()));
            extra.Add(new StringPair("time", Times.ToString()));
            StartCoroutine(_serverManager.SaveLoadServer("onLevelEnded", extra, "asdkasda8()sdu0a9ud0asd", value => SetPlayerData(((UserData)StringSerializationAPI.Deserialize(GetPlayerDataType(), value)).user)));
        }*/
#endif

#if UNITY_ANDROID || UNITY_IOS 
        if (stars >= 0)
        {
            int index = GetLevelIndex(level);
            if (index != -1)
            {
                if (_playerData.Levels[index].stars <= stars)
                {
                    _playerData.Levels[index] = new LevelData(level, stars);
                }
            }
            else
            {
                _playerData.Levels.Add(new LevelData(level, stars));

            }
            SaveMobile();
        }
#endif
        
    }

    public int GetLevelStars(int level)
    {
        int index = GetLevelIndex(level);
        if (index != -1)
        {
            return _playerData.Levels[index].stars;
        }
        return 0;
    }
    int GetLevelIndex(int level)
    {
        for (int i = 0; i < _playerData.Levels.Count; i++)
        {
            if (_playerData.Levels[i].id == level)
            {
                return i;
            }
        }
        return -1;
    }

    public float GetSpendStars()
    {
        var perks = PerkDB.LoadClone();
        float spendStars = 0;
        var purchased = GetPurchasedID;
        for (int i = 0; i < purchased.Count; i++)
        {
            for (int q = 0; q < perks.Count; q++)
            {
                if (perks[q].ID == purchased[i])
                {
                    spendStars += perks[q].cost[0];
                    break;
                }
            }
        }
        return spendStars;
    }
    public List<int> GetPurchasedPacks()
    {
        return _playerData.locations;
    }
    public int GetMaxPurchasedPackID()
    {
        int max = 1;
        for (int i = 0; i < _playerData.locations.Count; i++)
        {
            if (_playerData.locations[i] > max)
            {
                max = _playerData.locations[i];
            }
        }
        return max;
    }

    int GetLevelPackID(int level)
    {
        if (level < 100)
        {
            return 1;
        }
        else
        {
            return level / 100;
        }

    }
    public int GetLevelIDByIndexInPack(int index, int packID)
    {
        if (packID == 1)
        {
            return index + 1;
        }
        else
        {
            return packID * 100 + index + 1;
        }
    }

    public bool CanBuyPack(int id, List<PackInfo> packs)
    {
        if (id < 2)
        {
            Debug.Log("Can't buy pack id smaller then 2 id =" + id.ToString());
            return false;
        }
        int levelCount = 0;
        int starsSumm = 0;
        for (int i = 0; i < _playerData.Levels.Count; i++)
        {
            if (GetLevelPackID(_playerData.Levels[i].id) == id - 1)
            {
                starsSumm += Mathf.Clamp(_playerData.Levels[i].stars,0,3);
                levelCount++;
            }
        }
        return (levelCount == packs[id - 1].Levels.Count && starsSumm == 3 * levelCount);
    }
    public void BuyPack(int id, bool isPaidBuy)
    {
        _playerData.locations.Add(id);
        if (!isPaidBuy)
        {
            AddCrystals(-3000, 0);
        }
        if (OnPackBought != null)
        {
            OnPackBought();
        }

#if UNITY_IOS || UNITY_ANDROID
        SaveMobile();
#endif
    }
    public bool isPackBought(int id)
    {
        return _playerData.locations.Contains(id) || id == 1;
    }
    public float GetNotSpendStars(float spendStars)
    {
        float earnedStars = 0;
        for (int i = 0; i < _playerData.Levels.Count; i++)
        {
            earnedStars += _playerData.Levels[i].stars;
        }
        return earnedStars - spendStars;
    }
    public int GetStarsInPack(int packID)
    {
        int earnedStars = 0;
        for (int i = 0; i < _playerData.Levels.Count; i++)
        {
            if (GetLevelPackID(_playerData.Levels[i].id) == packID)
                earnedStars += _playerData.Levels[i].stars;
        }
        return earnedStars;
    }
    public int GetPerksCount()
    {
        return 0;
        //return _playerData.PurchasedUpgradesIDs.Count;
    }

    public bool isUpgradeBought(int index)
    {
        return false;
        //return _playerData.PurchasedUpgradesIDs.Contains(index);
    }


    public void BuyUpgrade(int index)
    {
        MyLog.Log("BuyUpgrade:"+index);

        if (OnUpgradeBuy != null)
        {
            OnUpgradeBuy(index);
        }
        
        //_playerData.PurchasedUpgradesIDs.Add(index);
        //SaveMobile();

        //StartCoroutine(FakeServerCall(index));
        List<StringPair> extra = new List<StringPair>();
        extra.Add(new StringPair("id", index.ToString()));
        StartCoroutine(_serverManager.SaveLoadServer("buyupgrade", extra, "", value => SetPlayerData(((UserData)StringSerializationAPI.Deserialize(GetPlayerDataType(), value)).user)));

    }
    IEnumerator FakeServerCall(int id)
    {
        yield return new WaitForSeconds(0.1f);
        PlayerData nPlayerData = _playerData.Copy();
        nPlayerData.PurchasedUpgradesIDs.Add(id);
        SetPlayerData(nPlayerData);
        yield break;
    }
    public void SellUpgrades(List<int> selledUpgrades)
    {
        MyLog.Log("SellUpgrades:" + selledUpgrades.ToArray().ToString());
        if (OnUpgradeSell != null)
        {
            OnUpgradeSell(selledUpgrades);
        }
        
        /*for (int i = 0; i < selledUpgrades.Count; i++)
        {
            _playerData.PurchasedUpgradesIDs.Remove(selledUpgrades[i]);
        }
        //Debug.Log( StringSerializationAPI.Serialize(selledUpgrades.GetType(),selledUpgrades));
        SaveMobile();*/

        List<StringPair> extra = new List<StringPair>();
        extra.Add(new StringPair("ids", StringSerializationAPI.Serialize(selledUpgrades.GetType(),selledUpgrades)));
        StartCoroutine(_serverManager.SaveLoadServer("sellupgrades", extra, "", value => SetPlayerData(((UserData)StringSerializationAPI.Deserialize(GetPlayerDataType(), value)).user)));

    }

    public void ResetUpgrades()
    {
        MyLog.Log("ResetUpgrades");
   
       //    StartCoroutine(_serverManager.SaveLoadServer("dropUpgrades", null, "", value => SetPlayerData(((UserData)StringSerializationAPI.Deserialize(GetPlayerDataType(), value)).user)));

       // _playerData.PurchasedUpgradesIDs.Clear();
        /*
        OnCrystalsChangedE(0);
        SaveMobile();*/
    }
    public static List<int> GetPurchasedID
    {
      get{ return _instance._GetPurchasedID; }
    }
    public List<int> _GetPurchasedID
    {
        get { return _playerData.PurchasedUpgradesIDs; }
    }

    public void AddCrystals(int crystals, int level)
    {
#if UNITY_ANDROID || UNITY_IOS ||UNITY_EDITOR
        if (level != 0)
        {
            if (_crystalsGainedByLevel.Count > level - 1 && crystals > 0)
            {
                _crystalsGainedByLevel[level - 1] += crystals;
            }
            else
            {
                _crystalsGainedByLevel.Add(crystals);
            }
        }
        _playerData.Crystals += crystals;
        SaveMobile();
        if (OnCrystalsChangedE != null)
        { OnCrystalsChangedE(_playerData.Crystals); }
#endif
#if (UNITY_WEBGL || UNITY_WEBPLAYER)  && !UNITY_EDITOR
        if (level < 0)
        {
            StartCoroutine(_serverManager.SaveLoadServer("collectDailyBouns", new List<StringPair>(), "", value => SetPlayerData(((UserData)StringSerializationAPI.Deserialize(GetPlayerDataType(), value)).user)));
        }
        else
        {
            if (crystals > 0)
            {
                List<StringPair> extra = new List<StringPair>();
                extra.Add(new StringPair("level_id", level.ToString()));
                extra.Add(new StringPair("count", crystals.ToString()));

                StartCoroutine(_serverManager.SaveLoadServer("onCrystalFinded", extra, "asdjkhasd78styd789123ydto01hlwd", value => SetPlayerData(((UserData)StringSerializationAPI.Deserialize(GetPlayerDataType(), value)).user)));
            }
        }
#endif
    }

    public int GetCrystals()
    {
        return _playerData.Crystals;
    }
    public int GetQuality()
    {
        return _playerData.QualityLevel;
    }
    public void SetQuality(int id)
    {
        _playerData.QualityLevel = id;
        _playerData.PlayerManuallySetQualityLevel = 1;
#if UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR
        SaveMobile();
#endif
#if (UNITY_WEBGL || UNITY_WEBPLAYER) && !UNITY_EDITOR
        List<StringPair> extra = new List<StringPair>();
        extra.Add(new StringPair("name", "quality"));
        extra.Add(new StringPair("value", id.ToString()));
        StartCoroutine(_serverManager.SaveLoadServer("changeSettings", extra, "", value => SetPlayerData(((UserData)StringSerializationAPI.Deserialize(GetPlayerDataType(), value)).user)));

        List<StringPair> extra2 = new List<StringPair>();
        extra2.Add(new StringPair("name", "quality_by_user"));
        extra2.Add(new StringPair("value", "1"));
        StartCoroutine(_serverManager.SaveLoadServer("changeSettings", extra2, "", value => SetPlayerData(((UserData)StringSerializationAPI.Deserialize(GetPlayerDataType(), value)).user)));

#endif
    }

    public List<int> GetCrystalsGained()
    {
        return _crystalsGainedByLevel;
    }
    public int isPlayerManuallySetQualityLevel()
    {
        return 0;// _playerData.PlayerManuallySetQualityLevel;
    }

    public bool isMusicOn()
    {
        return 1 == _playerData.Music;
    }
    public void SetMusicOn(bool on)
    {
        _playerData.Music = on ? 1 : 0;
        DM.ChangeSettings("music_on", _playerData.Music.ToString(), Response);
        /*
#if UNITY_ANDROID || UNITY_IOS 
        SaveMobile();
        if (OnCrystalsChangedE != null)
        { OnCrystalsChangedE(_playerData.Crystals); }
#endif
#if (UNITY_WEBGL || UNITY_WEBPLAYER) && !UNITY_EDITOR
        List<StringPair> extra = new List<StringPair>();
        extra.Add(new StringPair("name", "music_on"));
        extra.Add(new StringPair("value", _playerData.Music.ToString()));
        _playerData.Music.ToString();
        StartCoroutine(_serverManager.SaveLoadServer("changeSettings", extra, "", value => SetPlayerData(((UserData)StringSerializationAPI.Deserialize(GetPlayerDataType(), value)).user)));
#endif*/
    }

    public static bool isSoundOn()
    {
        if (_instance == null)
        {
            MyLog.LogWarning("PM > soundOn > instance is null", MyLog.Type.build);
            return false;
        }
        return _instance._isSoundOn();
    }

    public bool _isSoundOn()
    {
        return 1 == _playerData.Sound;
    }
    public void SetSoundOn(bool on)
    {
        _playerData.Sound = on ? 1 : 0;

        DM.ChangeSettings("sound_on", _playerData.Sound.ToString(), Response);
        /*
#if UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR
        SaveMobile();
        //if (OnCrystalsChangedE != null)
        //{ OnCrystalsChangedE(_playerData.Crystals); }
#endif
#if (UNITY_WEBGL || UNITY_WEBPLAYER) 
        List<StringPair> extra = new List<StringPair>();
        extra.Add(new StringPair("name", "sound_on"));
        extra.Add(new StringPair("value", _playerData.Sound.ToString()));
       // Debug.Log(_playerData.Sound.ToString());
        StatCoroutine(_serverManager.SaveLoadServer("changeSettings", extra, "", value => SetPlayerData(((UserData)StringSerializationAPI.Deserialize(GetPlayerDataType(), value)).user)));
#endif*/
    }

    private void Response(ResponseData data)
    {
        SetPlayerData(data.user);
    }


    public Type GetPlayerDataType()
    {
        return typeof(UserData);
    }
    void SaveMobile()
    {
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        MobileData data = new MobileData(_playerData, _crystalsGainedByLevel);
        CryptoPlayerPrefs.Save("savedGames", data.GetType(), data);
#endif
#if UNITY_EDITOR
        MyLog.LogWarning("Закомментировано. Только для редактора и не работало!");
        /*BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.OpenOrCreate);
        MobileData data = new MobileData(_playerData, _crystalsGainedByLevel);
        binaryFormatter.Serialize(file, data);
        file.Close();*/
#endif
    }
    void LoadMobile()
    {
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        if (CryptoPlayerPrefs.HasKey("savedGames"))
        {
            var data = (MobileData)CryptoPlayerPrefs.Load("savedGames", typeof(MobileData));
            _playerData = data.Data;
            _crystalsGainedByLevel = data.CrystalsGainedByLevel;
        }

#endif
#if UNITY_EDITOR
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
            MobileData data = (MobileData)bf.Deserialize(file);
            _playerData = data.Data;
            _crystalsGainedByLevel = data.CrystalsGainedByLevel;
            file.Close();
            //QualitySettings.SetQualityLevel(GetQuality());
        }
#endif
    }


}
