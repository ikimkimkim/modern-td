﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class EnergyLevelSelect : MonoBehaviour
{
    public Text TextEnergy;

    private bool SendUpdateData = false;
    void Start()
    {
        PlayerManager.InitializationСompleted += ChangeInfo;
        TextEnergy.text = PlayerManager.CountEnergy + "/10";
    }

    public static DateTime FaitTime { get; private set; }
    public static TimeSpan timeold { get; private set; }
    void FixedUpdate()
    {

        timeold = DateTime.Now - PlayerManager._instance._playerData.timeSet;

        if (PlayerManager.TimeAddEnergy < timeold.TotalSeconds)
        {
            if (SocialManager.Instance!= null && SendUpdateData == false)
            {
                SendUpdateData = true;
                SocialManager.UpdateInfoPlayer();
            }
        }
        else if(PlayerManager.CountEnergy < 10)
        {
            FaitTime = new DateTime((long)(PlayerManager.TimeAddEnergy - timeold.TotalSeconds) * 10000000);
        }
    }

    public void ChangeInfo()
    {
        TextEnergy.text = PlayerManager.CountEnergy + "/10";
        //Debug.Log("Time add energy:" + PlayerManager.TimeAddEnergy);
        if (PlayerManager.CountEnergy < 10)
            SendUpdateData = false;
    }

    void OnDestroy()
    {
        PlayerManager.InitializationСompleted -= ChangeInfo;
    }
}
