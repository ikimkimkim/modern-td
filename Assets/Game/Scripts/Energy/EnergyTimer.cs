﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class EnergyTimer : MonoBehaviour
{

    public string TextInfoAllEnergy,TextInfoAddEnergy;
    public Text TextPanel;

    public GameObject panel;
   
    // Use this for initialization
    void OnEnable()
    {
        PlayerManager.InitializationСompleted += RecountTime;
       
    }

    void Update()
    {
        if (PlayerManager.CountEnergy >= 10)
        {
            
            TextPanel.text = TextInfoAllEnergy;
        }
        else
        {
            TextPanel.text = TextInfoAddEnergy + EnergyLevelSelect.FaitTime.ToString("mm:ss");
        }
    }

    private void RecountTime()
    {
        panel.SetActive(PlayerManager.CountEnergy < 10);
    }

    void OnDisable()
    {
        PlayerManager.InitializationСompleted -= RecountTime;
    }
}
