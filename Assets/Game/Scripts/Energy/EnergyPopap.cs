﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnergyPopap : MonoBehaviour {

    public GameObject thisObj;
    public Text textInfo;

    public void Show(string t)
    {
        textInfo.text = t;
        thisObj.SetActive(true);
    }

    public void OnBuyEnergyVoiсe()
    {
        if (SocialManager.buyConfig != null && SocialManager.buyConfig.energy.Count > 0)
        {
            foreach (var e in SocialManager.buyConfig.energy)
            {
                if (e.Value.price == "1 голос")
                {
                    Application.ExternalCall("order", e.Value.item);
                }
            }
        }
    }

    void Update()
    {
        if (CardUIAllInfo.isShow == false && thisObj.activeSelf == true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                thisObj.SetActive(false);
            }
        }
    }

    public void ApplyCardEnergy()
    {
        var cards = PlayerManager.CardsList.FindAll(i => i.IDTowerData == 506);
        UIMiniPanelSelectedCard.Show(cards, EndSelectCard);
    }

    private void EndSelectCard(Card card)
    {
        if (card != null)
        {
            PlayerManager.DM.EnergyRecovery((int)card.rareness + 1, Result);
            Debug.Log("Select card " + card.ID);
        }
        else
            Debug.Log("Cancel selected card");
    }

    private void Result(ResponseData data)
    {
        if (data.ok == 0)
        {
            Debug.LogError("Energy Recovery Error: " + data.error.code + " -> " + data.error.text);
            TooltipMessageServer.Show(data.error.text);
        }
        else
        {
            TooltipMessageServer.Show("<color=#19c62e>Успешно</color>", 1);
            thisObj.SetActive(false);
        }
        PlayerManager.SetPlayerData(data.user);
    }
}
