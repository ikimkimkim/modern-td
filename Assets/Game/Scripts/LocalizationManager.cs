﻿using UnityEngine;
using System.Collections;

public class LocalizationManager : MonoBehaviour
{
    static GameObject _instance;
    void Awake()
    {
        if (_instance != null && _instance != gameObject)
        {
            DestroyImmediate(gameObject);//Простой destroy не сразу убивает объект и translation engine успевает загрузить csv что дофига долго
            return;
        }
        else
        {
            _instance = gameObject;
        }

        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
#if UNITY_WEBGL || UNITY_WEBPLAYER
        TranslationEngine.Instance.SelectLanguage("ru"); //Возможно не нужно чтобы 2жды язык не ставился, так как по умолчанию он и так русский
#endif
#if UNITY_ANDROID || UNITY_IOS
#if !UNITY_EDITOR
        if (Application.systemLanguage == SystemLanguage.Russian)
        {
            TranslationEngine.Instance.SelectLanguage("ru");
        }
        else
        {
            TranslationEngine.Instance.SelectLanguage("en-US");
        }
#endif
#if UNITY_EDITOR
        TranslationEngine.Instance.SelectLanguage("en-US");
#endif
#endif
    }

}
