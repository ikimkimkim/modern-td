﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AudioControll
{
    private struct audioData
    {
        public AudioSource audioSource;
        public float timePlay;
        public audioData(AudioSource source, float time)
        {
            audioSource = source;
            timePlay = time;
        }
    }

    //private const float playTimeLimit = 0.15f;
    private const float stopTimeLimit = 0.08f;
    private static Dictionary<string, audioData> PlayTime;

    static AudioControll()
    {
        PlayTime = new Dictionary<string, audioData>();
    }
    private static float delta;
    public static void Play(AudioSource audioSource)
    {
        if (PlayerManager.isSoundOn() == false || audioSource == null || audioSource.clip == null)
            return;

        if (PlayTime.ContainsKey(audioSource.clip.name))
        {
            delta = Time.unscaledTime - PlayTime[audioSource.clip.name].timePlay;
            if (delta <= audioSource.clip.length * stopTimeLimit)
            {
                //Debug.LogErrorFormat("Not play {0} delta {1} limit {2}", audioSource.clip.name, delta, audioSource.clip.length * stopTimeLimit);
                return;
            }
           /* Debug.LogFormat("Play {0} delta {1}", audioSource.clip.name, delta);
            if (delta <= audioSource.clip.length * playTimeLimit)
            {
                PlayerManager._instance.StartCoroutine(StopPlay(PlayTime[audioSource.clip.name].audioSource));
                //PlayTime[audioSource.clip.name].audioSource.Stop();
                Debug.LogWarningFormat("Stop {0} delta {1} limit {2}", audioSource.clip.name, delta, audioSource.clip.length * playTimeLimit);
            }*/
            audioSource.Play();
            PlayTime[audioSource.clip.name] = new audioData(audioSource, Time.unscaledTime);
        }
        else
        {
            audioSource.Play();
            PlayTime.Add(audioSource.clip.name, new audioData(audioSource, Time.unscaledTime));
        }
    }

    /* private static IEnumerator StopPlay(AudioSource source)
     {
         float startV = source.volume;
         while(source.volume>0)
         {
             source.volume -= Time.deltaTime*2f;
             Debug.LogFormat("S: {0} v:{1}",source.clip.name,source.volume);
             yield return null;
         }
         source.Stop();
         yield return null;
         source.volume = startV;
         yield break;
     }*/
}
