﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Вешается на кнопки, если кнопка деактивирует объект то нужно вызывать PlayOnDummy чтобы не пропал звук 
/// </summary>
public class SoundFXHelper : MonoBehaviour
{
    AudioSource _source;
    public bool BlockAwakeCheck = false;
    void Awake()
    {
        _source = GetComponent<AudioSource>();
        if (!BlockAwakeCheck)
            _source.mute = !PlayerManager.isSoundOn();
    }

    public void Play()
    {
        _source.mute = !PlayerManager.isSoundOn();
        AudioControll.Play(_source);//.Play();
    }
    public void PlayOnDummy()
    {
        GameObject dummy = new GameObject();
        dummy.name = _source.clip.name;
        var dummySound = dummy.AddComponent<AudioSource>();
        dummySound.clip = _source.clip;
        dummySound.volume = _source.volume;
        dummySound.mute = !PlayerManager.isSoundOn();
        AudioControll.Play(dummySound);//.Play();
        Destroy(dummy, 2f);
    }
    void OnEnable()
    {
        if (!BlockAwakeCheck)
        {
            _source.mute = !PlayerManager.isSoundOn();
            PlayerManager.OnCrystalsChangedE += OnPlayerDataChanged;
        }
    }
    void OnDisable()
    {
        if (!BlockAwakeCheck)
        {
            PlayerManager.OnCrystalsChangedE -= OnPlayerDataChanged;
        }
    }
    void OnPlayerDataChanged(int crystals)
    {
        _source.mute = !PlayerManager.isSoundOn();
    }
}
