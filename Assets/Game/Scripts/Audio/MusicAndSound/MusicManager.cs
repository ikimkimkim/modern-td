﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System;
using Object = UnityEngine.Object;
using SystemUpdate;
/// <summary>
/// Вебгл не поддерживает нормальную музыку поэтому этот класс управляет музыкой и при переходе с одного уровня на другой смешивает музыку 
/// </summary>
public class MusicManager : Singleton<MonoBehaviour>, IItemUpdate
{
    public const string FolderSounde = "/td/static/unity_sound/";

    [SerializeField] private string AudioMap = "Map.mp3";
    [SerializeField] private string AudioStory = "Story.mp3";
    [SerializeField] private string AudioBattleIntro = "BattleIntro.mp3";
    [SerializeField] private string[] AudioBattles = new string[] { "Battle_1.mp3", "Battle_2.mp3", "Battle_3.mp3", "Battle_4.mp3" };

    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private AudioClip clipMap, clipBattleIntro;
    //[SerializeField] private AudioMixerSnapshot mixerSnapshot;

    [SerializeField] private AudioSource _audioSource;

    private Coroutine _CorLoading, _CorSetClip;
    private int _randomBattlesMusic = 0;
    private PlayerManager _player;
    private bool isPlayBackground = false;

    protected override void InitializationSingleton()
    {
        _player = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
        PlayerManager.InitializationСompleted += PlayerDataUpdate;
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        GameControl.onGameStarted += BattleStart;
        HistoryPanel.OnStartShow += HistoryPanel_OnStartShow;
        HistoryPanel.OnEndShow += HistoryPanel_OnEndShow;
        SceneManager_sceneLoaded(SceneManager.GetActiveScene(), LoadSceneMode.Single);
        PlayerDataUpdate();
    }

    private void HistoryPanel_OnEndShow()
    {
        SceneManager_sceneLoaded(SceneManager.GetActiveScene(), LoadSceneMode.Single);
    }

    void Start()
    {
        Debug.LogWarning("MusicManager");
        MainSystem.AddItem(this);
        instance = this;
    }
    private void BattleStart(int levelID)
    {
        OnBattleMusic();
    }

    private void HistoryPanel_OnStartShow()
    {
        OnStoryMusic();
    }

    public void OnStoryMusic()
    {
        //PlayMusic(AudioStory);
        if (_player.isMusicOn())
        {
            isPlayBackground = true;
            Application.ExternalCall("playStoryMusic");
        }
        PlayMusic(null);
    }

    public void OnMapMusic()
    {
       // PlayMusic(AudioMap);
        PlayMusic(clipMap);
        Application.ExternalCall("stopBackgroundMusic");
        isPlayBackground = false;
    }

    public void OnBattleIntro()
    {
        //PlayMusic(AudioBattleIntro);
        PlayMusic(clipBattleIntro);
        Application.ExternalCall("stopBackgroundMusic");
        isPlayBackground = false;
    }

    public void OnBattleMusic()
    {
        _randomBattlesMusic = UnityEngine.Random.Range(0, AudioBattles.Length);
        if (_player.isMusicOn())
        {
            isPlayBackground = true;
            Application.ExternalCall("playBackgroundMusic", _randomBattlesMusic + 1);
        }
        PlayMusic(null);
        //PlayMusic(AudioBattles[_randomBattlesMusic]);
    }

    private void PlayMusic(AudioClip clip)
    {
        Debug.LogFormat("Play Music {0}", name);
        if (_CorSetClip != null)
            StopCoroutine(_CorSetClip);
        if (clip == null)
        {
            _audioSource.Stop();
        }
        else
            newClip = clip;// _CorSetClip = StartCoroutine(NewClipSet(clip));
    }

    /*private void PlayMusic(string name)
    {
        Debug.LogFormat("Play Music {0}", name);
        if (_CorLoading != null)
            StopCoroutine(_CorLoading);
        if (_CorSetClip != null)
            StopCoroutine(_CorSetClip);

        _CorLoading = StartCoroutine(LoadAudioFromServer(ServerManager._Domain + FolderSounde + name, AudioType.WAV,
            (AudioClip clip)=> {
                if (clip == null)
                {
                    Debug.LogErrorFormat("Clip load is null, {0}",name);
                    return;
                }
                clip.name = name;
                _CorSetClip = StartCoroutine(NewClipSet(clip));
            }));
    }*/

    IEnumerator NewClipSet(AudioClip clip)
    {
        while(_audioSource.volume>0)
        {
            _audioSource.volume -= Time.unscaledDeltaTime;
            yield return null;
        }
        _audioSource.Stop();
        if (clip == null)
            yield break;
        _audioSource.clip = clip;
        _audioSource.Play();
        while (_audioSource.volume < 1f)
        {
            _audioSource.volume += Time.unscaledDeltaTime;
            yield return null;
        }

        _audioSource.volume = 1f;
        yield break;
    }

    IEnumerator LoadAudioFromServer(string url, AudioType audioType, Action<AudioClip> response)
    {
        var request = UnityWebRequestMultimedia.GetAudioClip(url, audioType);
        
        yield return request.SendWebRequest();

        while(request.isDone == false)
        {
            Debug.Log("-> isDone false");
            yield return null;
        }

        while(request.uploadProgress < 1)
        {
            Debug.Log("-> uploadProgress " + request.uploadProgress);
            yield return null;
        }

        while (request.downloadProgress < 1)
        {
            Debug.Log("-> downloadProgress " + request.downloadProgress);
            yield return null;
        }
        

        if (!request.isHttpError && !request.isNetworkError)
        {
            AudioClip clip = DownloadHandlerAudioClip.GetContent(request);
            if(clip == null)
            {
                Debug.Log("Not find clip in "+url);
                response(null);
            }

            Debug.Log("-> load data "+ clip.LoadAudioData());

            if (clip.loadState == AudioDataLoadState.Loading)
            {
                Debug.Log("Wait load " + url);
                while (clip.loadState == AudioDataLoadState.Loading)
                {
                    yield return null;
                }
            }

            Debug.Log("-> status: " + clip.loadState);

            response(clip);
        }
        else
        {
            Debug.LogErrorFormat("error request [{0}, {1}]", url, request.error);

            response(null);
        }

        request.Dispose();
    }

    private void PlayerDataUpdate()
    {
        _audioSource.mute = !_player.isMusicOn();
        if (_audioSource.mute)
        {
            Application.ExternalCall("stopBackgroundMusic");
            isPlayBackground = false;
            return;
        }

        if (isPlayBackground == false && currentLevelIndex!=0 && currentLevelIndex!=1)
        {
            if (GameControl.IsGameTrainig == false && (GameControl.IsGamePause && GameControl.bufferStatePause == _GameState.Play))
            {
                Application.ExternalCall("playBackgroundMusic", _randomBattlesMusic + 1);
                isPlayBackground = true;
            }
        }
    }

    void OnDestroy()
    {
        MainSystem.RemoveItem(this);
       // PlayerManager.OnCrystalsChangedE -= OnPlayerDataChanged;
        PlayerManager.InitializationСompleted -= PlayerDataUpdate;
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
        GameControl.onGameStarted -= BattleStart;
        HistoryPanel.OnStartShow -= HistoryPanel_OnStartShow;
        HistoryPanel.OnEndShow -= HistoryPanel_OnEndShow;
    }

    private int currentLevelIndex;
    private void SceneManager_sceneLoaded(Scene scene, LoadSceneMode mode)
    {
        currentLevelIndex = scene.buildIndex;
        //Debug.Log("MUSIC On LEVEL WAS LOADED!"+level);
        if (currentLevelIndex == 0)//Главная 
        {

        }
        else if (currentLevelIndex == 1)//Карта, не возобновляем песню если перейти с апгрейдов
        {
            OnMapMusic();
        }
        else//Уровни
        {
            OnBattleIntro();
        }
    }

    private AudioClip newClip = null;
    public void SystemUpdate()
    {
        if(newClip!=null)
        { 
            if(_audioSource.clip == newClip)
            {
                if (_audioSource.isPlaying == false)
                {
                    _audioSource.volume = 0;
                    _audioSource.Play();
                }

                if (_audioSource.volume >= 1)
                    newClip = null;
                else
                    _audioSource.volume += Time.deltaTime;
            }
            else
            {
                if(_audioSource.isPlaying == false)
                {
                    _audioSource.clip = newClip;
                    _audioSource.volume = 0;
                    _audioSource.Play();
                    return;
                }

                if (_audioSource.volume > 0)
                    _audioSource.volume -= Time.deltaTime;
                else
                    _audioSource.clip = newClip;

            }
        }
    }
}
