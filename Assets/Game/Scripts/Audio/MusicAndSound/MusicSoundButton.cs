﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class MusicSoundButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerEnterHandler
{
    [Header("On Sprites")]
    public Sprite OnNormal;
    public Sprite OnHighlited;
    public Sprite OnPressed;
    [Space(1f)]
    [Header("Off Sprites")]
    public Sprite OffNormal;
    public Sprite OffHighlited;
    public Sprite OffPressed;
    [Space(1f)]
    public bool ChangeSound = false;
    public bool ChangeMusic = false;

    bool _OnState = true;
    bool _wasExit = false;
    Image _image;
    enum ButtonState
    {
        Normal, Highlited, Pressed
    }
    ButtonState _state = ButtonState.Normal;
    void Start()
    {
        _image = GetComponent<Image>();
        if (ChangeSound)
        {
            _OnState = PlayerManager.isSoundOn();
        }
        else if (ChangeMusic)
        {
            _OnState = PlayerManager._instance.isMusicOn();
        }
        SetProperSprite();
    }
    public void OnClick()
    {
        _OnState = !_OnState;

        if (ChangeSound)
        {
            PlayerManager._instance.SetSoundOn(_OnState);
        }
        else if (ChangeMusic)
        {
            PlayerManager._instance.SetMusicOn(_OnState);
        }

        if (!_wasExit)
        {
            if (_OnState)
            {
                _image.sprite = OnHighlited;
            }
            else
            {
                _image.sprite = OffHighlited;
            }
        }
    }

    void OnPlayerDataChanged(int crystals)
    {
        if (ChangeSound)
        {
            _OnState = PlayerManager.isSoundOn();
        }
        else if (ChangeMusic)
        {
            _OnState = PlayerManager._instance.isMusicOn();
        }

        SetProperSprite();

    }

    private void SetProperSprite()
    {
        if(_image==null)
        {
            _image = GetComponent<Image>();
        }

        switch (_state)
        {
            case ButtonState.Normal:
                {
                    _image.sprite = (_OnState) ? OnNormal : OffNormal;
                    break;
                }
            case ButtonState.Highlited:
                {
                    _image.sprite = (_OnState) ? OnHighlited : OffHighlited;
                    break;
                }
            case ButtonState.Pressed:
                {
                    _image.sprite = (_OnState) ? OnPressed : OffPressed;
                    break;
                }
        }
    }

    void OnEnable()
    {
        PlayerManager.OnCrystalsChangedE += OnPlayerDataChanged;
    }
    void OnDisable()
    {
        PlayerManager.OnCrystalsChangedE -= OnPlayerDataChanged;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (_OnState)
        {
            _image.sprite = OnPressed;
        }
        else
        {
            _image.sprite = OffPressed;
        }
        _state = ButtonState.Pressed;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!_wasExit)
        {
            if (_OnState)
            {
                _image.sprite = OnHighlited;
            }
            else
            {
                _image.sprite = OffHighlited;
            }
            _state = ButtonState.Highlited;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (_OnState)
        {
            _image.sprite = OnNormal;
        }
        else
        {
            _image.sprite = OffNormal;
        }
        _wasExit = true;
        _state = ButtonState.Normal;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_OnState)
        {
            _image.sprite = OnHighlited;
        }
        else
        {
            _image.sprite = OffHighlited;
        }
        _wasExit = false;
        _state = ButtonState.Highlited;
    }
}
