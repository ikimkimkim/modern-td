﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftCardInfoPanel : MonoBehaviour {

    public GameObject panel;

    public int ID;
    public _CardRareness Rareness;
    public CardUpgradeData cardData;
    public Image Icon, FonCard;
    public Text DespText;
    public Toggle toggle;

    public void Show(CardUpgradeData data, _CardRareness rareness,  string urlFonCard)
    {
        panel.SetActive(true);
        cardData = data;
        ID = data.id;
        Rareness = rareness;
        Icon.sprite = data.icon;
        DespText.text = data.generalDesp;

        //StartCoroutine(TextureDB.instance.load(urlFonCard, (Sprite s) => { FonCard.sprite = s; }));
    }

    public void Hide()
    {
        panel.SetActive(false);
    }
}
