﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public static class CraftLevelData
{
    private class LevelData
    {
        public string IDCard;
        public int Type;
        public _CardRareness Rareness;
        public int ID;
        public int Level;
    }

    private static Dictionary<int, float> GradeValueForLevel;
    private static List<LevelData> datas;

    public static int getCountGrades
    {
        get
        {
            if (datas != null)
                return datas.Count;
            else
                return 0;
        }
    }

    static CraftLevelData()
    {
        InitGradeValue();
    }

    private static void InitGradeValue()
    {
        GradeValueForLevel = new Dictionary<int, float>();
        GradeValueForLevel.Add(-1, 0.10f); //Урон
        GradeValueForLevel.Add(-2, 0.03f); //Радиус
        GradeValueForLevel.Add(-3, 0.05f); //Скорострельность
    }

    public static void Init(List<int[]> data)
    {
        datas = new List<LevelData>();
        for (int i = 0; i < data.Count; i++)
        {
            LevelData d = new LevelData();
            d.Type = data[i][0] - 1;
            d.Rareness = (_CardRareness)(data[i][1]-1);
            d.ID = data[i][2];
            d.Level = data[i][3];
            d.IDCard = (int)d.Rareness + "_" + d.Type;

            datas.Add(d);
           // Debug.LogFormat("init craft {0} r:{1} id:{2} lvl:{3}", d.Type, d.Rareness, d.ID,d.Level);
        }
    }
    public static int GetMyLevelGrade(Card card, int idGrade)
    {
        return GetMyLevelGrade(card.IDTowerData, card.rareness, idGrade);   
    }

    public static int GetMyLevelGrade(int type, _CardRareness rarety, int idGrade)
    {
       /* Debug.LogFormat("GetMyLevelGrade t:{0} r:{1} id:{2}", type, rarety, idGrade);
        for (int i = 0; i < datas.Count; i++)
        {

            Debug.LogFormat("Data t: {0} r:{1} id:{2}", datas[i].Type, datas[i].Rareness, datas[i].ID);
            if (datas[i].Type == type && datas[i].Rareness == rarety && datas[i].ID == idGrade)
            {
                return datas[i].Level;
            }
            
        }
        Debug.Log("Not find!");*/
        var data = datas.Find(i => i.Type == type && i.Rareness == rarety && i.ID == idGrade);
        if (data != null)
            return data.Level;
        return 0;
    }

    public static float ValueGrade(int idSetting)
    {
        if (GradeValueForLevel.ContainsKey(idSetting))
            return GradeValueForLevel[idSetting];
        else
            return 0;
    }



    public static UnitStat GetStatGrade(Card card)
    {       
        return GetStatGrade(card,card.stats[0]);
    }

    public static UnitStat GetStatGrade(Card card, UnitStat baseStat)
    {
        UnitStat stat = baseStat.Clone();
        int level = GetMyLevelGrade(card, -1);
        if (level > 0)
        {
            stat.damageMax *= (1f + ValueGrade(-1) * level);
            stat.damageMin *= (1f + ValueGrade(-1) * level);
        }
        level = GetMyLevelGrade(card, -2);
        if (level > 0)
        {
            stat.range *= (1f + ValueGrade(-2) * level);
        }
        level = GetMyLevelGrade(card, -3);
        if (level > 0)
        {
            stat.cooldown *= (1f - ValueGrade(-3) * level);
        }

        return stat;
    }
}
