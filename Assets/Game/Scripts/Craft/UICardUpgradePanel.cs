﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICardUpgradePanel : MonoBehaviour
{
    public static event Action OnShow, OnUpgradeCard, OnHide;
    private static UICardUpgradePanel instance;
    [SerializeField] private Button _buttonShow;
    public GameObject panel;

    public CardInfoUI cardInfoSelected;
    public CardInfoUI cardInfoUpgradeSelect;

    public GameObject panelSelectedUpgradeCard;
    public CardInfoUI[] cardsInfoUIUpgradeSelected;
    public GameObject panelSelectedUpgradeCardNotCard;
    public Text textSelectedUpgradeCard;
    public Button buttonSelectUpgradeCard;

    public Button buttonUpgrade;
    public AudioSource audioSource, audioSourceWait;
    public AudioClip audioGrade, audioFailed;
    private int CountNowGrade = 0;

    [SerializeField]
    private MiniSelectedCardUpgradeUI[] CardsBonusArray;

    [Header("Panel Selected Error")]
    public GameObject panelNotCardSelect;
    public Text textNotCardSelected;
    public GameObject Arrow1, Arrow2;

    [Header("Panel setting upgrade")]
    public GameObject panelSettingUpgrade;
    [System.Serializable]
    public struct SettingPanel
    {
        public GameObject obj, fon;
        public Toggle toggle;
        public Image icon;
        public Image iconLevel;
        public Text textName, textParam;
        public ParticleSystem particleWait, particleLuck, particleFail;
    }
    public SettingPanel[] settingsPanel;
    public ToggleGroup toggleGroupSetting;

    [Header("Panel properties upgrade")]
    public GameObject panelPropertiesUpgrade;
    public Sprite[] iconsLevel;
    [System.Serializable]
    public struct PropertiesPanel
    {
        public GameObject obj, fon;
        public Image icon;
        public Image iconLevel;
        public Toggle toggle;
        public Text textDesp;
        public CardProperties properties;
        public ParticleSystem particleWait, particleLuck, particleFail;
    }
    public PropertiesPanel[] propertiesPanel;
    public ToggleGroup toggleGroupProperties;

    [Header("Tutorial")]
    [SerializeField] private GameObject ArrowButtonOpen;

    public GameObject[] ArrowTutorial;


    private void Start()
    {
        _buttonShow.interactable = PlayerManager._instance.GetLevelsCountComplele() >= 14;
        instance = this;
    }

    public void Show()
    {
        Show(null, null);
    }


    public void Show(Card selectedCard, Card upgradeCard)
    {
        panel.SetActive(true);
        panelSelectedUpgradeCard.SetActive(false);
        cardInfoSelected.SetInfoTower(selectedCard, OnSelectCard);
        cardInfoUpgradeSelect.SetInfoTower(upgradeCard, ShowPanelSelectedUpgrade);
        PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;
        CountNowGrade = 0;
        PanelUpdate();
        if (OnShow != null)
            OnShow();
    }

    private void PlayerManager_InitializationСompleted()
    {
        if(cardInfoSelected.linkCard !=null)
        {
            var c = PlayerManager.CardsList.Find(i => i.ID == cardInfoSelected.linkCard.ID);
            cardInfoSelected.SetInfoTower(c, OnSelectCard);
        }
        PanelUpdate();
        if (cardInfoUpgradeSelect.linkCard != null)
        {
            var c = PlayerManager.CardsList.Find(i => i.ID == cardInfoUpgradeSelect.linkCard.ID);
            cardInfoUpgradeSelect.SetInfoTower(c, ShowPanelSelectedUpgrade);
        }
        if (cardInfoUpgradeSelect.linkCard == null)
            buttonUpgrade.interactable = false;
    }

    public void Hide()
    {
        if (isTutorial || waitForResponce)
            return;
        //HidePanelSelectedUpgrade();
        panel.SetActive(false);
        PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;
        if (OnHide != null)
            OnHide();
    }

    public void OnSelectCard(CardInfoUI cardInfoUI)
    {
        UICardsInventory.onHide += UICardsInventory_onHide;
        UICardsInventory.instance.Show(true,false,true,true,true);
    }

    private void UICardsInventory_onHide()
    {
        UICardsInventory.onHide -= UICardsInventory_onHide;
        if (UICardsInventory.instance.cardSelect == null)
            return;

        cardInfoSelected.SetInfoTower(UICardsInventory.instance.cardSelect);
        PanelUpdate();
        toggleGroupSetting.SetAllTogglesOff();
        toggleGroupProperties.SetAllTogglesOff();
        buttonUpgrade.interactable = false;

        for (int i = 0; i < CardsBonusArray.Length; i++)
        {
            CardsBonusArray[i].ClearSelectedCard();
            CardsBonusArray[i].SetRarenesFilter(cardInfoSelected.linkCard.rareness);
        }

        _AddStatusTutorial();
        //if (isTutorial && statusTutorial == Tutorial.selectCraft)
        //    addStatusTutorial();
    }



    public void ShowPanelSelectedUpgrade(CardInfoUI click)
    {
        var cardsUp = PlayerManager.CardsList.FindAll(i => i.Type == _CardType.Upgrade && 
                        (i.IDTowerData == 500 || i.IDTowerData == 501));

        UIMiniPanelSelectedCard.Show(cardsUp, EndSelectedPanelSelectedUpgrade);
    }

    public void EndSelectedPanelSelectedUpgrade(Card card)
    {
        //HidePanelSelectedUpgrade();
        cardInfoUpgradeSelect.SetInfoTower(card);
        PanelUpdate();
        toggleGroupSetting.SetAllTogglesOff();
        toggleGroupProperties.SetAllTogglesOff();
        buttonUpgrade.interactable = false;

        _AddStatusTutorial();
        //if (isTutorial && statusTutorial == Tutorial.selectUpgrade)
        //    addStatusTutorial();
    }



    private void PanelUpdate()
    {
        if (cardInfoSelected.linkCard==null || cardInfoUpgradeSelect.linkCard == null)
        {
            ShowInfoNotCardSelect();
        }
        else
        {
            switch (cardInfoUpgradeSelect.linkCard.IDTowerData)
            {
                case 500:
                    ShowSettingPanel();
                    break;
                case 501:
                    ShowPropertiesPanel();
                    break;
                default:
                    Debug.LogError("Select Card upgrade wrong id");
                    ShowInfoNotCardSelect();
                    break;
            }

        }

    }

    private void ShowInfoNotCardSelect(string info="")
    {
        panelNotCardSelect.SetActive(true);
        panelPropertiesUpgrade.SetActive(false);
        panelSettingUpgrade.SetActive(false);
        Arrow1.SetActive(false);
        Arrow2.SetActive(false);

        if (info.Length>0)
        {
            textNotCardSelected.text = info;
            return;
        }

        if (cardInfoSelected.linkCard == null)
        {
            Arrow1.SetActive(true);
            textNotCardSelected.text = "Выберите улучшаемую карту";
            return;
        }
        if (cardInfoUpgradeSelect.linkCard == null)
        {
            Arrow2.SetActive(true);
            textNotCardSelected.text = "Выберите улучшающую карту";
            return;
        }
    }


    public void ShowSettingPanel()
    {
        panelNotCardSelect.SetActive(false);
        panelPropertiesUpgrade.SetActive(false);
        panelSettingUpgrade.SetActive(true);

        Card card = cardInfoSelected.linkCard;
        switch (card.Type)
        {
            case _CardType.Tower:
            case _CardType.Hero:
                ShowTowerSetting();
                break;
            case _CardType.Ability:
                ShowAbilitySetting();
                break;
        }

        //toggleGroupSetting.SetAllTogglesOff();
    }

    public void SelectSettingOrPropertiesToggle()
    {
        if(cardInfoUpgradeSelect.linkCard!=null)
            buttonUpgrade.interactable = true;
    }

    public void ShowTowerSetting()
    {
        Card card = cardInfoSelected.linkCard;
        if (card.IDTowerData == 12)
        {
            ShowInfoNotCardSelect("Выбранная карта не имеет нужных свойств для улучшения");
            return;
            /*settingsPanel[0].textName.text = "Реген маны";
            settingsPanel[1].textName.text = "Максимальное количество маны";

            hideSettingPanel(2);*/
        }
        else
        {
            int level;
            float value/*, multiplier*/;

            level = CraftLevelData.GetMyLevelGrade(card, -1);
            value = CraftLevelData.ValueGrade(-1);
            if (level > 0)
                setDataInSettingPanel(0, level, "Урон", string.Format("{0}-{1} (<color=#19c62e>+{2}-{3}</color>)",
                     System.Math.Round(card.stats[0].damageMin, 2),
                     System.Math.Round(card.stats[0].damageMax, 2),
                     System.Math.Round(card.stats[0].damageMin * value * level, 2),
                     System.Math.Round(card.stats[0].damageMax * value * level, 2)
                    ));
            else
                setDataInSettingPanel(0, level, "Урон", string.Format("{0}-{1}", card.stats[0].damageMin, card.stats[0].damageMax));
            



            level = CraftLevelData.GetMyLevelGrade(card, -2);
            value = CraftLevelData.ValueGrade(-2);
            if (level>0)
                setDataInSettingPanel(1, level, "Радиус поражения", string.Format("{0} (<color=#19c62e>+{1}</color>)",
                    System.Math.Round(card.stats[0].range, 2),
                    System.Math.Round(card.stats[0].range * value * level, 2)
                    ));
            else
                setDataInSettingPanel(1, level, "Радиус поражения", string.Format("{0}", card.stats[0].range));



            level = CraftLevelData.GetMyLevelGrade(card, -3);
            value = CraftLevelData.ValueGrade(-3);
            if (level > 0)
                setDataInSettingPanel(2, level, "Атак в секунду", string.Format("{0} (<color=#19c62e>+{1}</color>)",
                    System.Math.Round(1f / card.stats[0].cooldown, 2),
                    System.Math.Round(1f / (card.stats[0].cooldown-card.stats[0].cooldown * value * level) - 1f / card.stats[0].cooldown, 2)
                    ));
            else
                setDataInSettingPanel(2, level, "Атак в секунду", string.Format("{0}", 1f / card.stats[0].cooldown));
        }
    }

    public void ShowAbilitySetting()
    {
        Card card = cardInfoSelected.linkCard;
        int level;
        float value/*, multiplier*/;

        if (card.stats[0].damageMin > 0)
        {
            level = CraftLevelData.GetMyLevelGrade(card, -1);
            value = CraftLevelData.ValueGrade(-1);
            if (level > 0)
                setDataInSettingPanel(0, level, "Урон", string.Format("{0}-{1} (<color=#19c62e>+{2}-{3}</color>)",
                     (card.stats[0].damageMin).RoundDamage(),
                     (card.stats[0].damageMax).RoundDamage(),
                     (card.stats[0].damageMin * value * level).RoundDamage(),
                     (card.stats[0].damageMax * value * level).RoundDamage()
                    ));
            else
                setDataInSettingPanel(0, level, "Урон", string.Format("{0}-{1}", card.stats[0].damageMin.RoundDamage(), card.stats[0].damageMax.RoundDamage()));
        }
        else
            hideSettingPanel(0);


        hideSettingPanel(1);

        if (card.stats[0].cooldown > 0)
        {
            level = CraftLevelData.GetMyLevelGrade(card, -3);
            value = CraftLevelData.ValueGrade(-3);
            //multiplier = 1f - value * level;
            if (level > 0)
                setDataInSettingPanel(2, level, "Таймаут", string.Format("{0} (<color=#19c62e>-{1}</color>)",
                     System.Math.Round(card.stats[0].cooldown, 2),
                     System.Math.Round(card.stats[0].cooldown * value * level, 2)
                     ));
            else
                setDataInSettingPanel(2, level, "Таймаут", string.Format("{0}", card.stats[0].cooldown));
        }
        else
            hideSettingPanel(2);
    }

    private void hideSettingPanel(int index)
    {
        if (settingsPanel[index].obj.activeSelf)
            settingsPanel[index].obj.SetActive(false);

        settingsPanel[index].toggle.isOn = false;
    }

    private void setDataInSettingPanel(int index, int level/*Sprite icon*/, string name, string param)
    {
        if (settingsPanel[index].obj.activeSelf == false)
            settingsPanel[index].obj.SetActive(true);

        if(level>0)
        {
            settingsPanel[index].iconLevel.sprite = iconsLevel[level];
            settingsPanel[index].iconLevel.gameObject.SetActive(true);

        }
        else
            settingsPanel[index].iconLevel.gameObject.SetActive(false);
        settingsPanel[index].textName.text = name;
        settingsPanel[index].textParam.text = param;
    }







    private void ShowPropertiesPanel()
    {
        Card card = cardInfoSelected.linkCard;
        Card cardUpgrade = cardInfoUpgradeSelect.linkCard;

        if (card.getIntRareness < cardUpgrade.getIntRareness)
        {
            ShowInfoNotCardSelect("Выбранная карта не имеет нужных свойств для улучшения");
            return;
        }

        panelNotCardSelect.SetActive(false);
        panelPropertiesUpgrade.SetActive(true);
        panelSettingUpgrade.SetActive(false);


        var properties = card.Properties;
        int level, index = 0;
        for (int i = 0; i < properties.Count; i++)
        {
            var rp = properties[i].Requirements.Find(f => f.ID == 5);
            if (rp != null)
            {
                level = (int)rp.Param[0];
                //Debug.LogFormat("Find Prop {2}: {0} {1}", level, cardUpgrade.getIntRareness + 2, properties[i].ID);
                if (level == cardUpgrade.getIntRareness + 2)
                {
                   //properties[i].SetLevel(card);
                    SetPropertiesInPanel(index, properties[i]);
                    index++;
                    if (index > 1)
                        break;
                }
            }
        }
        if(index == 0)
        {
            ShowInfoNotCardSelect("Выбранная карта не имеет нужных свойств для улучшения");
            return;
        }
        //toggleGroupProperties.SetAllTogglesOff();
        UpdetaPropretiesInPanel();
    }

    private void SetPropertiesInPanel(int index, CardProperties properties)
    {
        propertiesPanel[index].properties = properties;
        StartCoroutine(TextureDB.instance.load(properties.UrlIcon, (Sprite value) =>
        {
            propertiesPanel[index].icon.sprite = value;
        }));
    }

    public void UpdetaPropretiesInPanel()
    {
        for (int i = 0; i < propertiesPanel.Length; i++)
        {
            if (propertiesPanel[i].properties.Level > 0)
            {
                propertiesPanel[i].iconLevel.sprite = iconsLevel[propertiesPanel[i].properties.Level];
                propertiesPanel[i].iconLevel.gameObject.SetActive(true);
            }
            else
                propertiesPanel[i].iconLevel.gameObject.SetActive(false);

            propertiesPanel[i].textDesp.text = propertiesPanel[i].properties.getFullInfoText();
        }
    }

    private int id_param, id_panel, grade_card_id, grade_level;
    private bool waitForResponce = false;
    private float TimeSend;
    public void OnGradeCard()
    {
        buttonUpgrade.interactable = false;
        Card card = cardInfoSelected.linkCard;
        Card cardUpgrade = cardInfoUpgradeSelect.linkCard;
        id_param = 0;
        grade_card_id = cardUpgrade.IDTowerData;

        List<int[]> idCardBinus = new List<int[]>();
        for (int i = 0; i < CardsBonusArray.Length; i++)
        {
            if (CardsBonusArray[i].selectedCard != null)
                idCardBinus.Add(new int[] { CardsBonusArray[i].selectedCard.IDTowerData+1, (int)CardsBonusArray[i].selectedCard.rareness + 1 });
        }

        switch (cardUpgrade.IDTowerData)
        {
            case 500:
                for (int i = 0; i < settingsPanel.Length; i++)
                {
                    if (settingsPanel[i].toggle.isOn)
                    {
                        id_param = -i - 1;
                        id_panel = i;
                        grade_level = CraftLevelData.GetMyLevelGrade(cardInfoSelected.linkCard, id_param);
                        settingsPanel[i].particleWait.Play();
                    }
                }
                waitForResponce = true;
                TimeSend = Time.unscaledTime;
                if (PlayerManager.isSoundOn())
                    audioSourceWait.Play();
                PlayerManager.DM.ApplyUpgradeCard(card.IDTowerData+1, (int)card.rareness + 1, cardUpgrade.IDTowerData+1, (int)cardUpgrade.rareness + 1, id_param, idCardBinus, ResponseApplyUpgradeCard);

                break;
            case 501:
                for (int i = 0; i < propertiesPanel.Length; i++)
                {
                    if (propertiesPanel[i].toggle.isOn)
                    {
                        id_param = propertiesPanel[i].properties.ID;
                        id_panel = i;
                        grade_level = CraftLevelData.GetMyLevelGrade(cardInfoSelected.linkCard, id_param);
                        propertiesPanel[i].particleWait.Play();
                    }
                }

                waitForResponce = true;
                TimeSend = Time.unscaledTime;
                if (PlayerManager.isSoundOn())
                    audioSourceWait.Play();
                PlayerManager.DM.ApplyUpgradeCard(card.IDTowerData + 1, (int)card.rareness + 1, cardUpgrade.IDTowerData + 1, (int)cardUpgrade.rareness + 1, id_param, idCardBinus, ResponseApplyUpgradeCard);

                break;
            default:
                Debug.LogError("OnGradeCard: Select Card upgrade wrong id");
                ShowInfoNotCardSelect();
                break;
        }
    }


    private void ResponseApplyUpgradeCard(ResponseData responseData)
    {

        if (responseData.ok != 1)
        {
            PlayerManager.SetPlayerData(responseData.user);
            Debug.LogError("Response apply upgrade card error:" + responseData.error.code);
            TooltipMessageServer.Show(responseData.error.text, 5);
            audioSourceWait.Stop();
        }
        else
        {
            StartCoroutine(PlayResultResponce(responseData.user));
        }


    }

    private IEnumerator PlayResultResponce(PlayerData newData)
    {
        CountNowGrade++;
        if (CountNowGrade <= 3)
        {
            float delta = Mathf.Clamp(Time.unscaledTime - TimeSend, 0, 1.5f);
            yield return new WaitForSecondsRealtime(1.5f - delta);
        }
        else
            yield return null;
        audioSourceWait.Stop();
        PlayerManager.SetPlayerData(newData);

        bool isGrade = CraftLevelData.GetMyLevelGrade(cardInfoSelected.linkCard, id_param) > grade_level;
        waitForResponce = false;
        buttonUpgrade.interactable = true;
        switch (grade_card_id)
        {
            case 500:
                settingsPanel[id_panel].particleWait.Stop();

                break;
            case 501:
                propertiesPanel[id_panel].particleWait.Stop();
                break;
            default:
                Debug.LogErrorFormat("PlayResultParticle :" + grade_card_id);
                break;
        }
        yield return null;
        if (isGrade)
        {
            audioSource.clip = audioGrade;
            TooltipMessageServer.Show("<color=#19c62e>Успешно</color>", 1);
        }
        else
        {
            audioSource.clip = audioFailed;
            TooltipMessageServer.Show("<color=#cc2216>Провал</color>", 1);
        }

        switch (grade_card_id)
        {
            case 500:
                if (isGrade)
                    settingsPanel[id_panel].particleLuck.Play();
                else
                    settingsPanel[id_panel].particleFail.Play();

                break;
            case 501:
                if (isGrade)
                    propertiesPanel[id_panel].particleLuck.Play();
                else
                    propertiesPanel[id_panel].particleFail.Play();
                break;
        }


        if (PlayerManager.isSoundOn())
            audioSource.Play();

        _AddStatusTutorial();
        //if (isTutorial && statusTutorial == Tutorial.craft)
        //    addStatusTutorial();

        if (OnUpgradeCard != null)
            OnUpgradeCard();

        yield break;
    }



    #region Tutor
    private enum StatusTutorial { waitOpen, end }
    [SerializeField] private StatusTutorial _tutorialStatus;
    private Action _endTutorialEvent;
    private bool isTutorial = false;


    public static void StartTutorial(Action endTutorial)
    {
        instance._StartTutorial(endTutorial);
    }

    private void _StartTutorial(Action endTutorial)
    {
        isTutorial = true;
        _endTutorialEvent = endTutorial;
        _tutorialStatus = StatusTutorial.waitOpen;
        _UpdateStatus();
    }

    private void _AddStatusTutorial()//StatusTutorial status)
    {
        if (isTutorial == false)
            return;
        //if (_tutorialStatus != status)
        //    return;
        Debug.Log("_AddStatusTutorial");
        _tutorialStatus++;
        _UpdateStatus();
    }


    private void _UpdateStatus()
    {
        switch (_tutorialStatus)
        {
            case StatusTutorial.waitOpen:
                ArrowButtonOpen.SetActive(true);
                break;
           
            case StatusTutorial.end:
                _EndTutorial();
                break;
        }
    }

    private void _EndTutorial()
    {
        if (isTutorial == false)
            return;
        ArrowButtonOpen.SetActive(false);
        isTutorial = false;
        _endTutorialEvent();
    }



    #endregion

}
