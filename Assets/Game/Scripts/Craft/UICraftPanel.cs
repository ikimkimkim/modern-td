﻿using System;
using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public struct CraftCardConfig
{
    public int id;
    public _CardRareness rareness;
    public float[] price;
}

public class UICraftPanel : MonoBehaviour {

    public static event Action<int> OnCreatedCard;
    public static event Action OnShow,  OnHide;
    private static UICraftPanel instance;

    [SerializeField] public Button _buttonShow;
    public GameObject panel;
    public ToggleGroup toggleGroupCardInfo;
    public GameObject craftCartInfoPanelRefab;
    public Transform craftCardInfoPanelParent;
    public List<CraftCardInfoPanel> cardInfoPanels;
    public string[] UrlCardFon;
    private List<CraftCardConfig> craftCardConf;
    private List<CardUpgradeData> cardUpgradeDB;

    [SerializeField]
    private ToggleGroup toggleGroupFilter;
    [SerializeField]
    private Toggle[] toggleFilters;
    private enum FilterList { none, card, chest, energi, level, grade }
    private FilterList currentFilter;
    private Dictionary<FilterList, List<int>> FilterDB;


    public AudioSource audioSource;

    [Header("Resources")]
    public ResPanel[] panelRes;


    [Header("Craft")]
    public Image FonCard;
    public Image IconCard;
    public Slider countCardBar;
    public ToggleGroup toggleGroupRareness;
    public Toggle[] togglesRareness;
    public Text textCountCard;
    public Text textDesp;
    public Button buttonCraft;

    public Slider sliderCounterCraft;
    public Text textCounterInSlider;
    public ResPanel[] panelCraftRes;
    public Color colorHasRes, colorOverRes;



    public GameObject tooltipNotOpenCraft;

    [Header("Tutorial")]
    [SerializeField] private GameObject ArrowOpenButton;
    [SerializeField] private GameObject ArrowTutorial, FrameCardSelected, FrameRareness, FrameSelectedCount, ArrowButtonCraft;

    private void Start()
    {
        _buttonShow.interactable = PlayerManager._instance.GetLevelsCountComplele() >= 14;
        FilterDB = new Dictionary<FilterList, List<int>>();
        FilterDB.Add(FilterList.none, new List<int>() { 501, 502, 503, 505,507,509,510,512 });
        FilterDB.Add(FilterList.card, new List<int>() { 501, 502 });
        FilterDB.Add(FilterList.chest, new List<int>() { 503 });

        FilterDB.Add(FilterList.energi, new List<int>() { 507 });
        FilterDB.Add(FilterList.level, new List<int>() { 509,510 });
        FilterDB.Add(FilterList.grade, new List<int>() { 512 });

        instance = this;
    }

    public void Show()
    {
        panel.SetActive(true);
        Init();
        if(toggleGroupFilter.AnyTogglesOn())
            toggleGroupFilter.SetAllTogglesOff();
        else
            SetFilter(FilterList.none);

        PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;

        _AddStatusTutorial();
        if (OnShow != null)
            OnShow();
    }

    private void PlayerManager_InitializationСompleted()
    {
        LoadResources();
        UpdateCountCard();
        UpdateSliderCounter();
        ShowCraftCardPrice();
    }

    public void Init()
    {
        craftCardConf = PlayerManager._instance.CardCraftConfArray;
        cardUpgradeDB = DataBaseCardUpgrade.Load(DataBase<CardUpgradeData>.NameBase.CardUpgrade);
        LoadResources();
        initToggleFilter();
        initCraftCardListPanel();
    }

    public void SetFilter(int indexFilter)
    {
        if (toggleGroupFilter.AnyTogglesOn())
            SetFilter((FilterList)indexFilter);
        else
            SetFilter(FilterList.none);
    }

    private void SetFilter(FilterList filter)
    {
        if (filter == currentFilter)
            return;
        currentFilter = filter;
        initCraftCardListPanel();
    }

    private void initToggleFilter()
    {
        for (int i = 1; i < FilterDB.Count; i++)
        {
            toggleFilters[i-1].gameObject.SetActive(
                craftCardConf.Exists(j => FilterDB[(FilterList)i].Contains(j.id)));
        }
    }

    private void initCraftCardListPanel()
    {

        for (int i = 0; i < FilterDB[currentFilter].Count; i++)
        {
            if (!craftCardConf.Exists(j => j.id == FilterDB[currentFilter][i]))
            {
                Debug.LogWarningFormat("Card upgrade id {0} not find data", FilterDB[currentFilter][i]);
                continue;
            }

            var filterCraftCardConf = craftCardConf.Find(j => j.id == FilterDB[currentFilter][i]);     

            var cardUpgrade = cardUpgradeDB.Find(c => c.id == FilterDB[currentFilter][i]);
            if (cardUpgrade == null)
            {
                Debug.LogWarningFormat("Card upgrade id {0} not find in db", FilterDB[currentFilter][i]);
                continue;
            }
            if (cardInfoPanels.Count <= i)
            {
                GameObject go = Instantiate(craftCartInfoPanelRefab, craftCardInfoPanelParent);
                CraftCardInfoPanel ccp = go.GetComponent<CraftCardInfoPanel>();
                ccp.toggle.group = toggleGroupCardInfo;
                ccp.toggle.onValueChanged.AddListener((bool value) => { SelectedConfig(ccp); });
                cardInfoPanels.Add(ccp);
            }

            cardInfoPanels[i].Show(cardUpgrade, filterCraftCardConf.rareness, UrlCardFon[(int)_CardRareness.Usual]);
            if (cardInfoPanels[i].toggle.isOn || i == 0)
            {
                SelectedConfig(cardInfoPanels[i]);
                cardInfoPanels[i].toggle.isOn = true;
                toggleGroupCardInfo.NotifyToggleOn(cardInfoPanels[i].toggle);
            }
        }

        for (int i = FilterDB[currentFilter].Count; i < cardInfoPanels.Count; i++)
        {
            cardInfoPanels[i].Hide();
        }
    }

    private float[] countRes;
    private void LoadResources()
    {
        var resDB = DataBase<TDTKItem>.Load(DataBase<TDTKItem>.NameBase.Resources);
        countRes = new float[4];
        for (int i = 1; i < 5; i++)
        {
            var item = resDB.Find(j => j.ID == i);
            panelRes[i - 1].Icon.sprite = item.icon;
            countRes[i - 1] = PlayerManager.GetCraftResources(i);
            panelRes[i - 1].Count.text = countRes[i - 1].ToString();
            panelRes[i - 1].Name.text = item.getFullDesp;
        }
    }

    private List<CraftCardConfig> configsSelected;
    private CardUpgradeData cardUpgradeData;
    public void SelectedConfig(CraftCardInfoPanel panel)
    {
        configsSelected = craftCardConf.FindAll(i => i.id == panel.ID && !(i.id==503 &&i.rareness== _CardRareness.Usual));
        cardUpgradeData = panel.cardData;
        //Debug.Log("Selected: " + cardUpgradeData.generalDesp);

        IconCard.sprite = cardUpgradeData.icon;
        toggleGroupRareness.SetAllTogglesOff();
        for (int i = 0; i < togglesRareness.Length; i++)
        {
            togglesRareness[i].gameObject.SetActive(false);    
        }

        for (int i = 0; i < configsSelected.Count; i++)
        {
            int index = configsSelected[i].rareness == _CardRareness.Legendary?3: (int)configsSelected[i].rareness;
            togglesRareness[index].gameObject.SetActive(true);
            if(i == 0)
            {
                togglesRareness[index].isOn = true;
                toggleGroupRareness.NotifyToggleOn(togglesRareness[index]);
            }
        }
       
    }

    private CraftCardConfig configSeleted;
    public void ChangeRareness(int index)
    {
        if (!configsSelected.Exists(i => (int)i.rareness == index))
            return;
        configSeleted = configsSelected.Find(i => (int)i.rareness == index);
        /*Debug.Log(cardUpgradeData.generalDesp +":"+ configSeleted.rareness+":"+index);
        for (int i = 0; i < configsSelected.Count; i++)
        {
            Debug.Log(configsSelected[i].rareness);
        }*/
        textDesp.text = cardUpgradeData.data.First(i=>i.rareness == configSeleted.rareness).craftDesp;
        StartCoroutine(TextureDB.instance.load(UrlCardFon[(int)configSeleted.rareness], (Sprite s) => { FonCard.sprite = s; }));

        UpdateCountCard();
        UpdateSliderCounter();
        ShowCraftCardPrice();
    }

    private int lastCountCard;
    private void UpdateCountCard()
    {
        var cardHave = PlayerManager.CardsList.Find(i => i.IDTowerData == configSeleted.id - 1 && i.rareness == configSeleted.rareness);
        if (cardHave != null)
        {
            
            countCardBar.value = countCardBar.maxValue;
            textCountCard.text = cardHave.count.ToString();
            lastCountCard = cardHave.count;
        }
        else
        {
            lastCountCard = 0;
            countCardBar.value = 0;
            textCountCard.text = "0";
        }
    }

    private bool CanCraft;
    public void UpdateSliderCounter()
    {
        float min = 9999;
        for (int i = 0; i < 4; i++)
        {
            if(configSeleted.price[i]>0)
            {
                int count = Mathf.FloorToInt( countRes[i] / configSeleted.price[i]);
                if (min > count)
                    min = count;
            }
        }

        sliderCounterCraft.maxValue = min > 0 ? min : 1;
        CanCraft = min > 0;
        buttonCraft.interactable = CanCraft && (int)configSeleted.rareness < OpenCraftStarPerk.OpenLevel;
        sliderCounterCraft.minValue = 1;
        sliderCounterCraft.value = Mathf.FloorToInt(sliderCounterCraft.maxValue / 2f);
    }
    public void OnEnterButton()
    {
        tooltipNotOpenCraft.SetActive(CanCraft && buttonCraft.interactable == false);
    }
    public void OnExitButton()
    {
        tooltipNotOpenCraft.SetActive(false);
    }


    public void AddCountCraft()
    {
        if (sliderCounterCraft.value != sliderCounterCraft.maxValue)
            sliderCounterCraft.value++;
    }

    public void RemoveCountCraft()
    {
        if (sliderCounterCraft.value != sliderCounterCraft.minValue)
            sliderCounterCraft.value--;
    }

    public void ShowCraftCardPrice()
    {
        textCounterInSlider.text = string.Format("{0}/{1}", sliderCounterCraft.value, sliderCounterCraft.maxValue);
        float res;
        for (int i = 0; i < configSeleted.price.Length; i++)
        {
            if (configSeleted.price[i] == 0)
            {
                panelCraftRes[i].Panel.SetActive(false);
                continue;
            }
            else
                panelCraftRes[i].Panel.SetActive(true);

            res = configSeleted.price[i] * sliderCounterCraft.value;
            panelCraftRes[i].Count.text = res.ToString();
            if(res>0)
                panelCraftRes[i].Count.color = res <= countRes[i] ? colorHasRes : colorOverRes;
            else
                panelCraftRes[i].Count.color = colorHasRes;

        }

        for (int i = configSeleted.price.Length; i < 4; i++)
        {
            panelCraftRes[i].Panel.SetActive(false);
        }
    }

    public void Hide()
    {
        if (isTutorial)
            return;
        panel.SetActive(false);

        PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;

        if (OnHide != null)
            OnHide();
    }

    private int _countCreate;
    public void OnCreateCard()
    {
        buttonCraft.interactable = false;
        _countCreate = Mathf.FloorToInt(sliderCounterCraft.value);
        PlayerManager.DM.CreateUpgradeCard(configSeleted.id, (int)configSeleted.rareness + 1, _countCreate, responseCreateCard);
    }

    private void responseCreateCard(ResponseData responseData)
    {
        if (responseData.ok!=1)
        {
            Debug.LogError("Response create card error:" + responseData.error.code);
            TooltipMessageServer.Show(responseData.error.text, 5);
        }
        else
        {
            if(PlayerManager.isSoundOn())
                audioSource.Play();

            if (OnCreatedCard != null)
                OnCreatedCard(_countCreate);

            _AddStatusTutorial(); 
        }

        PlayerManager.SetPlayerData(responseData.user);
        //Init();
        LoadResources();
        //UpdateCountCard();
        var cardHave = PlayerManager.CardsList.Find(i => i.IDTowerData == configSeleted.id - 1 && i.rareness == configSeleted.rareness);
        if(cardHave!=null)
            StartCoroutine(AnimationText(textCountCard, countCardBar, lastCountCard, cardHave.count));
        UpdateSliderCounter();
        ShowCraftCardPrice();
    }



    IEnumerator AnimationText(Text text,Slider slider, float StartValue, float EndValue, string format = "{0}")
    {
        float currentValue = StartValue;
        text.text = string.Format(format, currentValue);

        float duration = 0.5f;
        float inc_for_sec = (EndValue - StartValue) / duration;
        slider.maxValue = EndValue;

        while (currentValue < EndValue)
        {
            currentValue += inc_for_sec*0.05f;
            slider.value = currentValue;
            text.text = string.Format(format, Mathf.Floor(currentValue));
            yield return new WaitForSecondsRealtime(0.05f);
        }

        text.text = string.Format(format, EndValue);
        UpdateCountCard();
        yield break;
    }




    #region Tutor
    private enum StatusTutorial { waitOpen, GeneralCard, GeneralRareness, GeneralCount, Craft, end }
    [SerializeField] private StatusTutorial _tutorialStatus;
    private Action _endTutorialEvent;
    private bool isTutorial = false;


    public static void StartTutorial(Action endTutorial)
    {
        instance._StartTutorial(endTutorial);
    }

    private void _StartTutorial(Action endTutorial)
    {
        isTutorial = true;
        _endTutorialEvent = endTutorial;
        _tutorialStatus = StatusTutorial.waitOpen;
        _UpdateStatus();
    }

    private void _AddStatusTutorial()//StatusTutorial status)
    {
        if (isTutorial == false)
            return;
        //if (_tutorialStatus != status)
        //    return;
        Debug.Log("_AddStatusTutorial");
        _tutorialStatus++;
        _UpdateStatus();
    }


    private void _UpdateStatus()
    {
        switch (_tutorialStatus)
        {
            case StatusTutorial.waitOpen:
                ArrowOpenButton.SetActive(true);
                break;
            case StatusTutorial.GeneralCard:
                ArrowOpenButton.SetActive(false);
                FrameCardSelected.SetActive(true);
                GeneralPanel.StartShow(_AddStatusTutorial, new GeneralState[]{
                    new GeneralState("Вам выдан доступ к разработкам наших инженеров.",
                    GeneralPos.Teaches, GeneralOrientation.right),
                    new GeneralState("Здесь доступные вам рецепты",
                    GeneralPos.Tactic, GeneralOrientation.right)
                });
                break;
            case StatusTutorial.GeneralRareness:
                FrameCardSelected.SetActive(false);
                FrameRareness.SetActive(true);
                GeneralPanel.StartShow(_AddStatusTutorial, new GeneralState[]{
                    new GeneralState("Далее вы можете выбрать редкость создаваемой вами карты",
                    GeneralPos.Tactic)
                });
                break;
            case StatusTutorial.GeneralCount:
                FrameRareness.SetActive(false);
                FrameSelectedCount.SetActive(true);
                GeneralPanel.StartShow(_AddStatusTutorial, new GeneralState[]{
                    new GeneralState("Далее выбираете количество, но советую не тратить сразу все ресурсы",
                    GeneralPos.Tactic),
                    new GeneralState("Соберите пару карт улучшающих базовые характеристики обычной редкости",
                    GeneralPos.Teaches)
                });
                break;
            case StatusTutorial.Craft:
                FrameSelectedCount.SetActive(false);
                ArrowButtonCraft.SetActive(true);
                break;
            case StatusTutorial.end:
                _EndTutorial();
                break;
        }
    }

    private void _EndTutorial()
    {
        if (isTutorial == false)
            return;
        ArrowButtonCraft.SetActive(false);
        isTutorial = false;
        _endTutorialEvent();
    }



    #endregion

}
