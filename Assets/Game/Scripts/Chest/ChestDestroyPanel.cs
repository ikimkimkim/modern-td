﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;
using UnityEngine.UI;

public class ChestDestroyPanel : MonoBehaviour
{
    public static ChestDestroyPanel instance { get; private set; }


    public ResPanel[] ResCraftPanel;
    [SerializeField]
    private Text textBonusRes;
    public AudioSource Audio;


    public void Show(Dictionary<string, int> resCraft)
    {
        instance = this;
        gameObject.SetActive(true);

        var resDB = DataBase<TDTKItem>.Load(DataBase<TDTKItem>.NameBase.Resources);
        for (int i = 0; i < ResCraftPanel.Length; i++)
        {
            ResCraftPanel[i].Panel.SetActive(false);
            ResCraftPanel[i].Name.text = resDB[i].getFullDesp;
            ResCraftPanel[i].Icon.sprite = resDB[i].icon;
        }

        foreach (var k in resCraft.Keys)
        {
            if (resCraft[k] > 0)
            {
                ResCraftPanel[int.Parse(k) - 1].Panel.SetActive(true);
                ResCraftPanel[int.Parse(k) - 1].Count.text = UpResourceCraftStarPerk.GetTextCountRes(resCraft[k]);
            }
        }

        textBonusRes.text = string.Format("(бонус за технологии: {0}%)", UpResourceCraftStarPerk.RealProcent);


        if (PlayerManager.isSoundOn())
            Audio.Play();

    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
