﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using TDTK;
using System.Collections.Generic;

public class ChestPanel : MonoBehaviour {

    public Text Name, deltaTime, Gold, Cristal, CardCount, CardInfo, textLevel;

    [SerializeField]
    private GameObject levelIcon;
    [SerializeField]
    private Text levelText;

    public GameObject PanelTime, PanelCristal;
    public Text StartTimeText, EndTimeText;

    public GameObject PanelInfo;
    public Text InfoText;

    public Button ButtonClose;
    public GameObject ButtonOpenNow, ButtonOpenNow2, ButtonGetPrize, ButtonStartOpen, ButtonShowVideo, ButtonUseCard;
    public Text TextPriceOpenNow, TextPriceOpenNow2;
    public Image IconChest;
    public string[] UrlChestIcon = new string[7];
    public ResPanel[] panelRes;
    
    [Header("Tutorial")]
    public GameObject TutorialStartOpenGO;
    public GameObject TutorialWaitGO;
    public GameObject TutorialGetPrizeGO;
    public GameObject TutorialSkipCritalGO;

    public static Action OnShow;
    public static Action OnHide;
    public static Action<int> ChangeStatusNowPanelChest;

    public static ChestPanel instance { get; private set; }

    public void Show(Chest chest)
    {
        Debug.Log("ChestPanel Show");
        instance = this;
        gameObject.SetActive(true);
        SetChest(chest);

        if (OnShow != null)
            OnShow();
    }

    public void Hide()
    {
        _EndTutorial();
        gameObject.SetActive(false);
        if (OnHide != null)
            OnHide();
    }

    public Chest chest { get { return _chest; } }
    private Chest _chest;

    private long ticks;

    public void SetChest(Chest chest)
    {
        if (_chest != null)
        {
            _chest.ChangeStatus -= OnChangeStatusChest;
            _chest = null;
        }

        _chest = chest;

        _chest.ChangeStatus += OnChangeStatusChest;

        ButtonClose.interactable = true;
        HideAllButton();

        switch (chest.Status)
        {
            case 0:
                if (ChestManager.ChestOpening)
                {
                    ButtonOpenNow.SetActive(true);
                    PanelInfo.SetActive(true);
                    InfoText.text = "Открывается другой сундук!";
                    TextPriceOpenNow.text = "Открыть сразу за " + chest.Open_price.ToString();
                }
                else
                {
                    ButtonStartOpen.SetActive(true);
                    PanelTime.SetActive(true);
                    StartTimeText.text = "Время ожидания:";

                    now = new DateTime(_chest.Time_to_open.Ticks);
                    EndTimeText.text = now.ToString("HH:mm:ss");

                }
                InitButtonUseCard();
                break;
            case 1:
                if (_chest.Type == Chest.TypeChest.Wood)
                {
                    ButtonOpenNow2.SetActive(true);
                    PanelTime.SetActive(true);
                }
                else
                {
                    //ButtonShowVideo.SetActive(true);
                    ButtonOpenNow.SetActive(true);
                    PanelTime.SetActive(true);
                    TextPriceOpenNow.text = "Открыть сразу за " + chest.Open_price.ToString();
                    InitButtonUseCard();
                }
                break;
            case 2:
                ButtonGetPrize.SetActive(true);
                PanelInfo.SetActive(true);
                InfoText.text = "СУНДУК ОТКРЫТ!";

                break;
            default:
                Debug.LogError("Chest Status is " + chest.Status);
                break;
        }


        if (chest.Time_decrease_pcnt != null && chest.Time_decrease_pcnt.Length > 0 && chest.Time_decrease_pcnt != "0")
            deltaTime.text = "Время открытия уменьшено на " + chest.Time_decrease_pcnt + "%";
        else
            deltaTime.text = "";

        levelIcon.SetActive(chest.Level != "0");
        levelText.text = chest.Level;

        //textLevel.text = "Уровень сундука: " + chest.Level;
        CardCount.text = chest.GetTextCardCount;// Chest.getCountCard(chest.Type);
        CardInfo.text = chest._data.available_prizes.cards.max_text;// Chest.getInfoCardInChest(chest.Type);
        Gold.text = chest.GetTextGoldCount; //  Chest.getCountGold(chest.Type);
        Cristal.text = chest.GetTextCrystalCount;// Chest.getCountCrystal(chest.Type);
        PanelCristal.SetActive(chest._data.available_prizes.crystals[1] > 0);
        Name.text = Chest.getNameChest(chest.Type);

        StartCoroutine(TextureDB.instance.load(UrlChestIcon[(int)chest.Type], SetIcon));

        LoadResources();

        waitVideoShow = false;
    }

    private void LoadResources()
    {
        var resDB = DataBase<TDTKItem>.Load(DataBase<TDTKItem>.NameBase.Resources);
        for (int i = 1; i < 5; i++)
        {
            string count = _chest.GetTextResourcesCount(i);
            if (count.Length > 0)
            {
                var item = resDB.Find(j => j.ID == i);
                panelRes[i - 1].Panel.SetActive(true);
                panelRes[i - 1].Icon.sprite = item.icon;
                panelRes[i - 1].Count.text = count;
                panelRes[i - 1].Name.text = item.getFullDesp;
            }
            else
            {
                panelRes[i - 1].Panel.SetActive(false);
            }
        }
    }

    private void HideAllButton()
    {
        ButtonOpenNow.SetActive(false);
        ButtonOpenNow.GetComponent<Button>().interactable = true;
        ButtonOpenNow2.SetActive(false);
        ButtonGetPrize.SetActive(false);
        ButtonShowVideo.SetActive(false);
        ButtonGetPrize.GetComponent<Button>().interactable = true;
        ButtonStartOpen.SetActive(false);
        ButtonStartOpen.GetComponent<Button>().interactable = true;
        PanelTime.SetActive(false);
        PanelInfo.SetActive(false);
        ButtonUseCard.SetActive(false);
    }

    private List<Card> cardsCanUse;
    private void InitButtonUseCard()
    {
        _CardRareness r = _CardRareness.Legendary;
        switch (_chest.Type)
        {
            case Chest.TypeChest.Wood:
                r = _CardRareness.Usual;
                break;
            case Chest.TypeChest.Silver:
                r = _CardRareness.Magic;
                break;
            case Chest.TypeChest.Gold:
                r = _CardRareness.Rare;
                break;

        }
        if(_chest.Status == 0)
            cardsCanUse = PlayerManager.CardsList.FindAll(i => (i.IDTowerData == 502 && i.rareness>=(r>=_CardRareness.Rare?_CardRareness.Legendary:r+1)) //Карта разрушения сундука на ресурсы
                    || (i.IDTowerData == 503 && i.rareness >= r )//Карта мгновенного открытия сундука
                    || (i.IDTowerData == 504 && i.rareness >= r )//Карта уменьшения времени открытия сундука 
                    || (i.IDTowerData == 505 && _chest.Type <= Chest.TypeChest.Huge && i.rareness >= r));//Карта улучшения редкости сундука
        else if (_chest.Status == 1)
            cardsCanUse = PlayerManager.CardsList.FindAll(i => (i.IDTowerData == 502 && i.rareness >= r)
                    || (i.IDTowerData == 503 && i.rareness >= r )
                    || (i.IDTowerData == 504 && i.rareness >= r ));

        ButtonUseCard.SetActive(true);
    }

    public void ShowSelectCardPanel()
    {
        UIMiniPanelSelectedCard.Show(cardsCanUse, ApplyCard);
    }

    public void ApplyCard(Card cardSelect)
    {
        if (cardSelect == null)
            return;

        switch(cardSelect.IDTowerData)
        {
            case 502:
                _chest.DestroyChest(cardSelect);
                break;
            case 503:
                _chest.OpenChest(cardSelect);
                break;
            case 504:
                _chest.DecreaseOpenTimeChest(cardSelect);
                break;
            case 505:
                _chest.UpgradeRarity(cardSelect);
                break;
            default:
                Debug.LogError("ApplyCard not use. id:"+cardSelect.ID);
                break;
        }
    }

    #region VideoShow

    bool waitVideoShow;
    public void OnShowVideo()
    {
        Debug.Log("Wait show video:"+waitVideoShow);
        if (waitVideoShow == false)
        {
            waitVideoShow = true;
            Application.ExternalCall("tryRewardedVideo");
        }
    }
    
    public void ResultShowVideo(bool show)
    {
        if (show)
        {
            TimeSpan t = new TimeSpan((_chest.Time_to_open.Ticks + _chest.timeSetData.Ticks) - DateTime.Now.Ticks);
            PlayerManager.DM.ReducingOpenTimeChest(_chest.ID, (float) t.TotalSeconds, ResultReducingOpenTime);
        }
        else
        {
            TooltipMessageServer.Show("К сожалению, для вас не нашлось рекламы.\r\nПопробуйте позже.", 5);
            waitVideoShow = false;
        }
    }

    public void ResultReducingOpenTime(ResponseData rp)
    {
        PlayerManager.SetPlayerData(rp.user);
        if (rp.ok==1)
        {
            TooltipMessageServer.Show("Вы ускорили открытие сундука", 5);
            foreach (Chest c in PlayerManager.ChestList)
            {
                if(c.ID == _chest.ID)
                {
                    SetChest(c);
                    break;
                }
            }
        }
        else
        {
            Debug.LogError("UpgradeTower Error: " + rp.error.code + " -> " + rp.error.text);
            TooltipMessageServer.Show("Error: " + rp.error.code + " -> " + rp.error.text, 5);

        }

        waitVideoShow = false;
    }

    #endregion

    public void OnChangeStatusChest()
    {
        if(this.gameObject.activeSelf)
            SetChest(_chest);

        _TutorialGlobalAddStatus();

        if (ChangeStatusNowPanelChest != null)
            ChangeStatusNowPanelChest(_chest.Status);
    }

    public void SetIcon(Sprite icon)
    {
        IconChest.sprite = icon;
    }

    public void StartOpenChest()
    {
        //Hide();
        _chest.StartOpen();
        ButtonStartOpen.GetComponent<Button>().interactable = false;
    }

    public void NowOpenChest()
    {
        _chest.FastOpen(cbNowOpenChest);
        ButtonOpenNow.GetComponent<Button>().interactable = false;
    }

    public void cbNowOpenChest(string error)
    {
        if(error== "no_crystals")
        {
            ButtonOpenNow.GetComponent<Button>().interactable = true;
            StartCoroutine(ErrorOpen());
        }
    }

    IEnumerator ErrorOpen()
    {
        for (int i = 0; i < 4; i++)
        {
            TextPriceOpenNow.color = new Color(1, 0.7f, 0.7f, 1f);
            yield return new WaitForSeconds(0.1f);
            TextPriceOpenNow.color = Color.white;
            yield return new WaitForSeconds(0.1f);

        }
    }

    public void OpenChest()
    {
        _chest.Open();
        ButtonGetPrize.GetComponent<Button>().interactable = false;
    }

    void Start ()
    {
	
	}

    DateTime now;

    void Update ()
    {
        if (_chest.Status == 1)
        {
            ticks = (_chest.Time_to_open.Ticks + _chest.timeSetData.Ticks) - DateTime.Now.Ticks;
            if (ticks >= 0)
            {
                now = new DateTime(ticks);
                EndTimeText.text = now.ToString("HH:mm:ss");
                TextPriceOpenNow2.text = now.ToString("HH:mm:ss");
            }
            else
            {
                _chest.Status = 2;
            }
        }


        if (TutorialOpenChest.instance == null && CardUIAllInfo.isShow==false && gameObject.activeSelf==true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Hide();
            }
        }
    }

    private Action _tutorialEndEvent;
    private bool isTutorial = false;
    public enum TutorialType { WaitTime, FastOpen}
    private TutorialType _tutorialType;

    public static void StartTutorial(TutorialType tutorialType, Action endEvent) { instance._StartTutorial(tutorialType, endEvent); }
    private void _StartTutorial(TutorialType tutorialType, Action endEvent)
    {
        isTutorial = true;
        _tutorialType = tutorialType;
        _tutorialEndEvent = endEvent;
        switch(_tutorialType)
        {
            case TutorialType.WaitTime:
                _StartTutorialOpenChestWaitTime();
                break;
            case TutorialType.FastOpen:
                _StartTutorialOpenChestFast();
                break;
        }
    }

    private void _EndTutorial()
    {
        Debug.LogWarning("End Tutorial Chest");
        if (isTutorial == false)
            return;

        isTutorial = false;
        _TutorialStartOpen(false);
        _TutorialWait(false);
        _TutorialGetPrize(false);
        _TutorialSkip(false);
        if (_tutorialEndEvent != null)
            _tutorialEndEvent();
    }

    private void _TutorialGlobalAddStatus()
    {
        if (isTutorial == false)
            return;

        switch (_tutorialType)
        {
            case TutorialType.WaitTime:
                _statusTutorialWoodChest++;
                _TutorialWaitTimeUpdateStatus();
                break;
            case TutorialType.FastOpen:
                _statusTutorialFastChest++;
                _TutorialFastOpenUpdateStatus();
                break;
        }
    }

    #region tutorialWoodChest

    private enum TutorialEnumWoodChest { StartOpen, WaitTime, Open, end }
    private TutorialEnumWoodChest _statusTutorialWoodChest;
    private void _StartTutorialOpenChestWaitTime()
    {
        _statusTutorialWoodChest = TutorialEnumWoodChest.StartOpen;
        _TutorialWaitTimeUpdateStatus();
    }


    private void _TutorialWaitTimeUpdateStatus()
    {
        switch(_statusTutorialWoodChest)
        {
            case TutorialEnumWoodChest.StartOpen:
                _TutorialStartOpen(true);
                break;
            case TutorialEnumWoodChest.WaitTime:
                _TutorialStartOpen(false);
                _TutorialWait(true);
                break;
            case TutorialEnumWoodChest.Open:
                _TutorialWait(false);
                _TutorialGetPrize(true);
                break;
            case TutorialEnumWoodChest.end:
                _TutorialGetPrize(false);
                _EndTutorial();
                break;
        }
    }
    #endregion

    #region tutorialFastChest
    private enum TutorialEnumFastChest { StartOpen, Skip, end }
    private TutorialEnumFastChest _statusTutorialFastChest;
    private void _StartTutorialOpenChestFast()
    {
        _statusTutorialFastChest = TutorialEnumFastChest.StartOpen;
        _TutorialFastOpenUpdateStatus();
    }
    private void _TutorialFastOpenUpdateStatus()
    {
        switch (_statusTutorialFastChest)
        {
            case TutorialEnumFastChest.StartOpen:
                _TutorialStartOpen(true);
                break;
            case TutorialEnumFastChest.Skip:
                _TutorialStartOpen(false);
                _TutorialSkip(true);
                break;
            case TutorialEnumFastChest.end:
                _TutorialSkip(false);
                _EndTutorial();
                break;
        }
    }

    #endregion




    private void _TutorialStartOpen(bool active)
    {
        if (TutorialStartOpenGO.activeSelf != active)
            TutorialStartOpenGO.SetActive(active);
    }

    private void _TutorialWait(bool active)
    {
        if (TutorialWaitGO.activeSelf != active)
            TutorialWaitGO.SetActive(active);
    }

    private void _TutorialGetPrize(bool active)
    {
        if (TutorialGetPrizeGO.activeSelf != active)
            TutorialGetPrizeGO.SetActive(active);
    }

    private void _TutorialSkip(bool active)
    {
        if (TutorialSkipCritalGO.activeSelf != active)
            TutorialSkipCritalGO.SetActive(active);
    }
}
