﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using TDTK;
using System;

public class ChestManagerV2 : MonoBehaviour
{

    private static int old_CountStarChest = 0, old_MaxCountStar = 10;
    private bool animPlay;

    public static int CountStars = 0;
    public Animator animatorStars;
    public ChestOpenPanel openPanel;
    public Color colorTextGold, colorTextWite;

    public Color colorActiveChest, colorDeactiveChest;

    [Header("Free")]
    public Image fonFree;
    public Text textNameFreeChest;
    public GameObject buttonFreeObj, panelTimeLeft;
    public Button buttonFreeFon;
    private Button buttonFree;
    public int countFreeChest;
    public float TimeLeft;
    public Sprite[] spritesFreeChestFon;
    public Image[] imageFreeChest;
    public Sprite spriteFreeChestNorm, spriteFreeChestPremium;
    public Text textTimeLeft;


    [Header("Stars")]
    public Image fonStars;
    public Text textNameStarChest;
    public GameObject buttonStarsObj, panelProgress;
    public Button buttonStarsFon;
    private Button buttonStars;
    public Sprite[] spriteStarChestFon;
    public Sprite spriteStarChestNorm, spriteStarChestPremium;
    public Image imageStarChest;
    public int CountStarChest = 3;
    public int MaxCountStar = 10;
    public RectTransform porgressBar;
    public RectTransform progressLine;
    public Text textCountStar;


    private void Awake()
    {
        buttonFree = buttonFreeObj.GetComponent<Button>();
        buttonStars = buttonStarsObj.GetComponent<Button>();
        animPlay = false;
    }

    private void Start()
    {
        animatorStars.SetInteger("CountStar", CountStars);

        if (CountStars > 0)
        {
            animPlay = true;
            setOldData();
        }
        else if (PlayerManager._instance != null)
            eventUpdateData();
    }

    void OnEnable()
    {
        PlayerManager.InitializationСompleted += eventUpdateData;
    }

    void OnDisable()
    {
        PlayerManager.InitializationСompleted -= eventUpdateData;
    }


    public void eventUpdateData()
    {
        if (animPlay)
            return;
        countFreeChest = PlayerManager._instance._playerData.free_chest.count;
        TimeLeft = PlayerManager._instance._playerData.free_chest.time;
        CountStarChest = PlayerManager._instance._playerData.stars_chest.count;
        old_CountStarChest = CountStarChest;
        MaxCountStar = PlayerManager._instance._playerData.stars_chest.need;
        old_MaxCountStar = MaxCountStar;
        UpdateData();
    }

    public void setOldData()
    {
        countFreeChest = PlayerManager._instance._playerData.free_chest.count;
        TimeLeft = PlayerManager._instance._playerData.free_chest.time;
        CountStarChest = old_CountStarChest;
        MaxCountStar = old_MaxCountStar;
        UpdateData();
    }

    public void UpdateData()
    {
        UpdateFreeChest();
        UpdateStarChest();
    }

    private void UpdateFreeChest()
    {
        if (countFreeChest > spritesFreeChestFon.Length-1)
            Debug.LogError("ChetManager: count free chest is over "+ (spritesFreeChestFon.Length-1));
        countFreeChest = Mathf.Min(countFreeChest, spritesFreeChestFon.Length-1);
        fonFree.sprite = spritesFreeChestFon[countFreeChest];

        for (int i = 0; i < imageFreeChest.Length; i++)
        {
            if (i < countFreeChest)
                imageFreeChest[i].color = colorActiveChest;
            else
                imageFreeChest[i].color = colorDeactiveChest;
            imageFreeChest[i].sprite = PlayerManager.isPremium ? spriteFreeChestPremium : spriteFreeChestNorm;
        }
        if (countFreeChest > 0)
        {
            buttonFreeObj.SetActive(true);
            buttonFreeFon.interactable = true;
            buttonFree.interactable = true;
            panelTimeLeft.SetActive(false);
            textTimeLeft.text = "";
            textNameFreeChest.color = colorTextGold;
        }
        else
        {
            buttonFreeObj.SetActive(false);
            buttonFreeFon.interactable = false;
            panelTimeLeft.SetActive(true);
            textNameFreeChest.color = colorTextWite;
            float time = (TimeLeft - (float)timeold.TotalSeconds);
            /*int min = Mathf.FloorToInt(time / 60f);
            int hous = Mathf.FloorToInt(min / 60);
            int sec = Mathf.FloorToInt(time - min * 60);
            min = min - hous * 60;*/
            DateTime d = new DateTime((long) time* 10000000);
            textTimeLeft.text = d.ToString("hh:mm:ss"); // string.Format("{0}:{1}:{2}", hous, min, sec);

            if (time <= 0.1f)
            {
                MyLog.Log("ChestFree wait timeout!", MyLog.Type.build);
                PlayerManager.DM.getPlayerData();
            }
        }
    }

    private void UpdateStarChest()
    {
        textCountStar.text = CountStarChest + "/" + MaxCountStar;
        bool starComlite = CountStarChest >= MaxCountStar;
        fonStars.sprite = starComlite ? spriteStarChestFon[1] : spriteStarChestFon[0];
        buttonStarsObj.SetActive(starComlite);
        buttonStars.interactable = starComlite;
        buttonStarsFon.interactable = starComlite;

        panelProgress.SetActive(!starComlite);
        imageStarChest.sprite = PlayerManager.isPremium ? spriteStarChestPremium : spriteStarChestNorm;
        if (starComlite)
        {
            textNameStarChest.color = colorTextWite;
            imageStarChest.color = colorActiveChest;
        }
        else
        {
            imageStarChest.color = colorDeactiveChest;
            textNameStarChest.color = colorTextGold;
            progressLine.sizeDelta = new Vector2(porgressBar.sizeDelta.x * CountStarChest / MaxCountStar, progressLine.sizeDelta.y);

        }
    }

    public TimeSpan timeold { get; private set; }
    private bool bFreeColor, bStarColor;
    public float mtpColor=0.1f;
    public float minColor = 0.9f;
    private ColorBlock cb;
    private Color c;
    private void Update()
    {
        timeold = DateTime.Now - PlayerManager._instance._playerData.timeSet;
        if (countFreeChest <= 0)
            UpdateFreeChest();

        if(buttonFreeFon.interactable)
        {
            cb = buttonFreeFon.colors;
            c = cb.normalColor;
            if (bFreeColor)
            {
                c.r += mtpColor * Time.deltaTime;
                c.g += mtpColor * Time.deltaTime;
                c.b += mtpColor * Time.deltaTime;
               // c = new Color(c.r + mtpColor * Time.deltaTime, c.g + mtpColor * Time.deltaTime, c.b + mtpColor * Time.deltaTime, c.a);
            }
            else
            {
                c.r -= mtpColor * Time.deltaTime;
                c.g -= mtpColor * Time.deltaTime;
                c.b -= mtpColor * Time.deltaTime;
                //c = new Color(c.r - mtpColor * Time.deltaTime, c.g - mtpColor * Time.deltaTime, c.b - mtpColor * Time.deltaTime, c.a);
            }
            cb.normalColor = c;
            if (c.r >= 1f)
                bFreeColor = false;
            else if (c.r <= minColor)
                bFreeColor = true;

            buttonFreeFon.colors = cb;
        }

        if (buttonStarsFon.interactable)
        {
            cb = buttonStarsFon.colors;
            c = cb.normalColor;
            if (bStarColor)
            {
                c.r += mtpColor * Time.deltaTime;
                c.g += mtpColor * Time.deltaTime;
                c.b += mtpColor * Time.deltaTime;
                //c = new Color(c.r + mtpColor * Time.deltaTime, c.g + mtpColor * Time.deltaTime, c.b + mtpColor * Time.deltaTime, c.a);
            }
            else
            {
                c.r -= mtpColor * Time.deltaTime;
                c.g -= mtpColor * Time.deltaTime;
                c.b -= mtpColor * Time.deltaTime;
                //c = new Color(c.r - mtpColor * Time.deltaTime, c.g - mtpColor * Time.deltaTime, c.b - mtpColor * Time.deltaTime, c.a);
            }
            cb.normalColor = c;
            if (c.r >= 1f)
                bStarColor = false;
            else if (c.r <= minColor)
                bStarColor = true;

            buttonStarsFon.colors = cb;
        }
    }


    public void OpenFreeChest()
    {
        buttonFree.interactable = false;
        idWaitChest = -1;
        typeWaitChest = PlayerManager.isPremium ? Chest.TypeChest.Silver : Chest.TypeChest.Wood;
        PlayerManager.DM.TakeChestPrizes(-1, CBOpen);

    }

    public void OpenStarsChest()
    {
        buttonStars.interactable = false;
        idWaitChest = -2;
        typeWaitChest = PlayerManager.isPremium ? Chest.TypeChest.Gold : Chest.TypeChest.Silver;
        PlayerManager.DM.TakeChestPrizes(-2, CBOpen);
    }

    private int idWaitChest;
    private Chest.TypeChest typeWaitChest;
    private void CBOpen(ResponseData rp)
    {
        if (rp.ok == 0)
        {
            Debug.LogError("Open Chest Error: " + rp.error.code + " -> " + rp.error.text);
            TooltipMessageServer.Show(rp.error.text);
            PlayerManager.SetPlayerData(rp.user);
        }
        else
        {
            PlayerManager.SetPlayerData(rp.user);

            openPanel.SetResponceDataAndShow(idWaitChest, typeWaitChest, rp.prizes);

            //List<UnitTower> bd = TowerDB.Load();
           /* List<Card> newCard = new List<Card>();

            foreach (ResponseData.PrizesTower PrizesTower in rp.prizes.towers)
            {
                for (int i = 0; i < PlayerManager.CardsList.Count; i++)
                {
                    if (PlayerManager.CardsList[i].IDTowerData == PrizesTower.type - 1 && (int)PlayerManager.CardsList[i].rareness == PrizesTower.rarity - 1)
                    {
                        newCard.Add(PlayerManager.CardsList[i]);
                        newCard[newCard.Count - 1].AddCount = PrizesTower.count;
                        newCard[newCard.Count - 1].count -= PrizesTower.count;
                        break;
                    }
                }
            }
            Chest c = new Chest(idWaitChest, typeWaitChest, "", 0, 0, 0);
            openPanel.Show(c, newCard, rp.prizes.gold, rp.prizes.crystals);*/
        }


        ChestManager.instance.UpdateInfo();
    }

    public void AddStarAnim()
    {
        old_CountStarChest++;
        old_CountStarChest = Mathf.Min(old_CountStarChest, old_MaxCountStar);
        setOldData();
    }

    public void EndAnim()
    {
        animPlay = false;
        CountStars = 0;
        eventUpdateData();
    }


}
