﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ChestUI : MonoBehaviour
{

    public Chest myChest;

    public Text TimeText;

    public GameObject Open, OpenNow, Close, Time, IconShining;

    public Image IconChest;
    public Button button;

    public Animator _Animator; 

    public static Action<int> ChangeStatus; 

    public void SetChest(Chest chest)
    {

        Debug.Log("ChestUI SetChest status:" + chest.Status);

        myChest = chest;

        Open.SetActive(false);
        OpenNow.SetActive(false);
        Close.SetActive(false);
        Time.SetActive(false);
        IconShining.SetActive(false);
        switch (chest.Status)
        {
            case 0:
                Close.SetActive(true);
                break;
            case 1:
                OpenNow.SetActive(true);
                Time.SetActive(true);
                break;
            case 2:
                Open.SetActive(true);
                IconShining.SetActive(true);
                break;
        }

        button.enabled = true;

        StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsChest[(int)chest.Type], setSpriteFonChest));

        _Animator.SetInteger("Status", myChest.Status);
        Debug.Log("ChestUI SetChest end");
    }
    public void setSpriteFonChest(Sprite sprite)
    {
        IconChest.sprite = sprite;
    }

    public void ShowPanel()
    {
        GetComponentInParent<ChestManager>().ShowPanel(myChest);
    }

    public void DestroyChest()
    {
        myChest = null;
        
        button.enabled = false;
        StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsEmptyChest, setSpriteFonChest));
        Open.SetActive(false);
        OpenNow.SetActive(false);
        Close.SetActive(false);
        Time.SetActive(false);
        IconShining.SetActive(false);
        _Animator.SetInteger("Status", 0);
    }

    void FixedUpdate()
    {
        if (myChest == null)
            return;

        if (myChest.Status == 1)
        {
            long ticks = (myChest.Time_to_open.Ticks + myChest.timeSetData.Ticks) - DateTime.Now.Ticks;

            if (ticks < 0)
            {
                OpenNow.SetActive(false);
                Time.SetActive(false);
                Open.SetActive(true);
                IconShining.SetActive(true);
                myChest.Status = 2;
                if (ChangeStatus != null)
                    ChangeStatus(2);
            }
            else
            {
                DateTime now = new DateTime(ticks);
                TimeText.text = now.ToString("HH:mm:ss");
            }
        }
        else if(myChest.Status == 2)
        {
            _Animator.SetInteger("Status", myChest.Status);
        }
        else if (myChest.Status == 3)
            Destroy(gameObject);
    }
}
