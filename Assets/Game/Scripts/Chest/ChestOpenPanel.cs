﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TDTK;
using System;

public class ChestOpenPanel : UIEntity {

    [Serializable]
    public struct ResPanel
    {
        public GameObject obj;
        public Text textCount;
    }   

    public static ChestOpenPanel instance { get; private set; }

    public Button OkButton;
    [SerializeField] private GameObject _cardPanel;
    public GameObject prefabCardInfo;
    public RectTransform panelCard;

    public Text _TextInfo;

    [SerializeField] private GameObject _moneyPanel;
    public ResPanel _Gold, _Cristal;
    [SerializeField] private GameObject _resCraftPanel;
    public ResPanel[] ResCraftPanel;
    [SerializeField]
    private Text textBonusRes;

    public Image IconOpenChest;
    public Sprite iconDestroyChest;
    [SerializeField] private string _iconPrizeUrl;
    public TooltipInfoCard tooltipInfo;
    public string[] UrlOpenChests = new string[7];
    private List<GameObject> _cards=new List<GameObject>();
    public AudioSource Audio;
    public static Action OnHide;

    private bool speedUpShow, skipShow;

    public bool isShow { get { return gameObject.activeSelf; } }
    public override int entityID { get { return 3; } }
    public override int priorityShow { get { return 100; } }

    public override bool NeedShow()
    {
        return isShow;
    }

    private List<Card> LastCards;
    public void Close()
    {
        gameObject.SetActive(false);
        for (int i = 0; i < _cards.Count; i++)
        {
            Destroy(_cards[i]);
        }
        _cards.Clear();
        if (OnHide != null)
            OnHide();

        HideUIEvent();
        _EndTutorial();
    }

    public void SetResponceDataAndShow(int idChest, Chest.TypeChest typeChest, ResponseDataTopDayPrize.PrizesData prizes)
    {
        List<Card> newCard = new List<Card>();
        foreach (ResponseDataTopDayPrize.PrizesTower PrizesTower in prizes.chest.towers)
        {
            for (int i = 0; i < PlayerManager.CardsList.Count; i++)
            {
                if (PlayerManager.CardsList[i].IDTowerData == PrizesTower.type - 1 && (int)PlayerManager.CardsList[i].rareness == PrizesTower.rarity - 1)
                {
                    newCard.Add(PlayerManager.CardsList[i]);
                    newCard[newCard.Count - 1].AddCount = PrizesTower.count;
                    newCard[newCard.Count - 1].count -= PrizesTower.count;
                    break;
                }
            }
        }
        Chest c = new Chest(idChest, typeChest, "", 0, 0, 0);

        Show(c, newCard, prizes.chest.gold, prizes.chest.gold, prizes.chest.craft_resources);
    }

    public void SetResponceDataAndShow(int idChest, Chest.TypeChest typeChest, ResponseData.PrizesData prizes)
    {
        List<Card> newCard = new List<Card>();

        foreach (ResponseData.PrizesTower PrizesTower in prizes.towers)
        {
            for (int i = 0; i < PlayerManager.CardsList.Count; i++)
            {
                if (PlayerManager.CardsList[i].IDTowerData == PrizesTower.type - 1 && (int)PlayerManager.CardsList[i].rareness == PrizesTower.rarity - 1)
                {
                    newCard.Add(PlayerManager.CardsList[i]);
                    newCard[newCard.Count - 1].AddCount = PrizesTower.count;
                    newCard[newCard.Count - 1].count -= PrizesTower.count;
                    break;
                }
            }
        }
        Chest c = new Chest(idChest, typeChest, "", 0, 0, 0);

        Show(c, newCard, prizes.gold, prizes.crystals, prizes.craft_resources);
    }

    public void SetResponceDataAndShow(ResponseData.PrizesData prizes)
    {
        List<Card> newCard = new List<Card>();

        foreach (ResponseData.PrizesTower PrizesTower in prizes.towers)
        {
            for (int i = 0; i < PlayerManager.CardsList.Count; i++)
            {
                if (PlayerManager.CardsList[i].IDTowerData == PrizesTower.type - 1 && (int)PlayerManager.CardsList[i].rareness == PrizesTower.rarity - 1)
                {
                    newCard.Add(PlayerManager.CardsList[i]);
                    newCard[newCard.Count - 1].AddCount = PrizesTower.count;
                    newCard[newCard.Count - 1].count -= PrizesTower.count;
                    break;
                }
            }
        }
        ShowPrize( newCard, prizes.gold, prizes.crystals, prizes.craft_resources);
    }

    public void ShowPrize(List<Card> cards, int Gold, int Crystal, Dictionary<string, int> resCraft)
    {
        instance = this;
        speedUpShow = false;
        skipShow = false;

        ShowMoney(Gold, Crystal);
        ShowCraftRes(resCraft);

        gameObject.SetActive(true);

        _cardPanel.SetActive(cards != null && cards.Count>0);
        if (cards != null && cards.Count > 0)
            StartCoroutine(CardShow(cards));

        StartCoroutine(TextureDB.instance.load(_iconPrizeUrl, SetIcon));

        if (PlayerManager.isSoundOn())
            Audio.Play();

        ShowUIEvent();
    }



    public void Show(Chest chest, List<Card> cards, int Gold, int Crystal, Dictionary<string, int> resCraft)
    {
        instance = this;
        speedUpShow = false;
        skipShow = false;

        ShowMoney(Gold, Crystal);
        ShowCraftRes(resCraft);

        gameObject.SetActive(true);

        _cardPanel.SetActive(cards != null && cards.Count > 0);
        if (cards!= null && cards.Count > 0)
            StartCoroutine(CardShow(cards));

        if (chest != null)
            StartCoroutine(TextureDB.instance.load(UrlOpenChests[(int)chest.Type], SetIcon));
        else
            SetIcon(iconDestroyChest);
        if(PlayerManager.isSoundOn())
            Audio.Play();

        ShowUIEvent();
    }


    private void ShowMoney(int gold, int crystal)
    {
        _Gold.obj.SetActive(gold > 0);
        _Gold.textCount.text = gold.ToString();

        _Cristal.obj.SetActive(crystal > 0);
        _Cristal.textCount.text = crystal.ToString();

        _moneyPanel.SetActive(gold > 0 || crystal > 0);
    }

    private void ShowCraftRes(Dictionary<string, int> resCraft)
    {

        if (resCraft != null)
        {
            for (int i = 0; i < ResCraftPanel.Length; i++)
            {
                ResCraftPanel[i].obj.SetActive(false);
            }

            bool isActive = false;
            foreach (var k in resCraft.Keys)
            {
                if (resCraft[k] > 0)
                {
                    ResCraftPanel[int.Parse(k) - 1].obj.SetActive(true);
                    ResCraftPanel[int.Parse(k) - 1].textCount.text = UpResourceCraftStarPerk.GetTextCountRes(resCraft[k]);
                    isActive = true;
                }
            }
            _resCraftPanel.SetActive(resCraft != null && isActive);

            textBonusRes.text = string.Format("(бонус за технологии: {0}%)", UpResourceCraftStarPerk.RealProcent);
        }
        else
            _resCraftPanel.SetActive(false);

    }

    int index = 0;

    public IEnumerator CardShow(List<Card> cards)
    {
        OkButton.interactable = false;
        MyLog.Log("Add card "+cards.Count);
        index = 0;
        int NowIndex = index;
        while (index < cards.Count)
        {
            GameObject go = GameObject.Instantiate(prefabCardInfo, panelCard);
            CardInfoUI card = go.GetComponent<CardInfoUI>();
            card.CanChecked = false;
            card.SetInfoTower(cards[NowIndex],false);
            go.transform.SetSiblingIndex(0);
            _cards.Add(go);

            if (skipShow)
            {
                index++;

                card.SetAddCount();
            }
            else
            {
                card.AnimatorCard.SetTrigger("AnimOpenChest");

                while (index == NowIndex && !skipShow)
                {
                    yield return null;
                }
                if (index == NowIndex && skipShow)
                    index++;
               else
                    yield return new WaitForSeconds(1f);
            }
            
            
            NowIndex = index;
        }
        MyLog.Log("End add card");
        OkButton.interactable = true;
        TimeScaleManager.RefreshTimeScale();
        speedUpShow = false;
        skipShow = false;
    }
    
    public void UpdateInfoCard()
    {
        Card c;
        CardInfoUI cInfo;
        for (int i = 0; i < _cards.Count; i++)
        {
            cInfo = _cards[i].GetComponent<CardInfoUI>();
            c = PlayerManager.CardsList.Find(find => find.ID == cInfo.linkCard.ID);
            if (c != null)
                cInfo.SetInfoTower(c);
            else
                MyLog.LogError("COP-> UpdateInfoCard card not find");
        }
    }

    public void AddCard(CardInfoUI card)
    {
        if (!skipShow)
            index++;
    }

    public void SetIcon(Sprite icon)
    {
        IconOpenChest.sprite = icon;
    }

    void Update()
    {
        if (CardUIAllInfo.isShow == false && gameObject.activeSelf == true)
        {
            if (OkButton.interactable != false)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    Close();
                }
            }
            else if(Input.GetMouseButtonDown(0))
            {
                if(speedUpShow)
                {
                    skipShow = true;
                    TimeScaleManager.RefreshTimeScale();
                }
                else
                {
                    TimeScaleManager.SetTimeScale(10f);
                    speedUpShow = true;
                }
            }
        }
    }


    private bool isTutorial = false;
    private Action _tutorialEvent;
    public static void  StartTutorial(Action endEvent)
    {
        if (instance == null  || instance.isShow==false)
        {
            endEvent();
            return;
        }
        instance._StartTutorial(endEvent);
    }
    private void _StartTutorial(Action endEvent)
    {
        isTutorial = true;
        _tutorialEvent = endEvent;
    }

    private void _EndTutorial()
    {
        if (isTutorial == false)
            return;
        if (_tutorialEvent != null)
            _tutorialEvent();
    }
    
}


