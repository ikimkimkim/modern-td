﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using TDTK;

public class Chest
{
    public static event Action<Chest> onOpenChest, onFastOpenChest, onCardOpenChest;
    public static event Action<int> onDestroyChest, onUpgradeChest;
    

    public const string DespAllChest = "Содержит различные карты и ресурсы";

    public enum TypeChest { Wood, Silver, Gold, Huge, Magic, SuperMagic, Legendary }
    private static string[] IconCloseChestUrls = new string[7]
    {
        "Chest/CloseWood.png",
        "Chest/CloseSilver.png",
        "Chest/CloseGold.png",
        "Chest/CloseHuge.png",
        "Chest/CloseMagic.png",
        "Chest/CloseSupermagic.png",
        "Chest/CloseLegendary.png"
    };
    public static string[] getIconUrlsChest
    {
        get { return IconCloseChestUrls; }
    }

    private static string[] NameChest = new string[7]
    {
        "Деревянный сундук",
        "Серебряный сундук",
        "Золотой сундук",
        "Огромный сундук",
        "Магический сундук",
        "Супер-магический сундук",
        "Легендарный сундук"
    };
    public static string getNameChest(TypeChest type)
    {
        return NameChest[(int)type];
    }

    private static string IconEmptyChestUrl = "Blackout/ChestEmpty.png";
    public static string getIconUrlsEmptyChest
    {
        get { return IconEmptyChestUrl; }
    }
    public string GetTextGoldCount { get { return _data.available_prizes.gold[0] + "-" + _data.available_prizes.gold[1]; } }
    public string GetTextCrystalCount { get { return _data.available_prizes.crystals[0] + "-" + _data.available_prizes.crystals[1]; } }
    public string GetTextCardCount { get { return "+"+ _data.available_prizes.cards.cards_count; } }

    public string GetTextResourcesCount(int index)
    {
        string i = index.ToString();
        if (_data.available_prizes.craft_resources.ContainsKey(i))
        {
            return string.Format("{0}-{1}", _data.available_prizes.craft_resources[i][0], _data.available_prizes.craft_resources[i][1]);
        }
        else
        {
            //Debug.LogWarningFormat("Not find resources id {0}!", i);
            return "";
        }
    }


    public TypeChest Type = TypeChest.Wood;
    public int ID;
    public int Status
    {
        get
        {
            return _status;
        }
        set
        {
            _status = value;
            timeSetData = DateTime.Now;
            if (ChangeStatus != null)
                ChangeStatus();
        }
    }
    private int _status;

    public string Time_decrease_pcnt;
    public DateTime Time_to_open;
    public int Open_price;
    public string Level;

    public Action ChangeStatus;

    public DateTime timeSetData { get; private set; }

    public ChestsData _data { get; private set; }


    public Chest(ChestsData data)
    {
        ID = int.Parse(data.id);
        Type = (TypeChest) (int.Parse(data.type_id) - 1);
        Time_decrease_pcnt = data.time_decrease_pcnt;
        Status = data.status;
        Time_to_open = new DateTime(0);
        Time_to_open = Time_to_open.AddSeconds(data.time_to_open);
        Open_price = data.open_price;
        _data = data;
        Level = data.level;
        timeSetData = DateTime.Now;
    }
   
    public Chest(int id, TypeChest type, string time_decrease,int status,int time_to_open,int price)
    {
        ID = id;
        Type = type;
        Time_decrease_pcnt = time_decrease;
        Status = status;
        Time_to_open = new DateTime(0);
        Time_to_open = Time_to_open.AddSeconds(time_to_open);
        Open_price = price;
        timeSetData = DateTime.Now;
    }

    private bool _AndOpen;
    public void StartOpen(bool AndOpen = false)
    {
        _AndOpen = AndOpen;
        PlayerManager.DM.StartOpenChest(ID, CBStartOpen);
    }

    private Action<string> cbChaestPanel;
    public void FastOpen(Action<string> result)
    {
        cbChaestPanel = result;
        PlayerManager.DM.FastOpenChest(ID, _FastOpenChestResponce);
    }

    public void Open()
    {
        PlayerManager.DM.TakeChestPrizes(ID, _OpenChestResponce);
    }

    private void CBStartOpen(ResponseData rp)
    {
        if (rp.ok == 0)
        {
            Debug.LogError("Start Open Chest Error: " + rp.error.code + " -> " + rp.error.text);
            if(rp.error.code.Contains("no_crystals")==false)
                TooltipMessageServer.Show(rp.error.text);
        }
        else
        {
            Status = 1;
        }

        PlayerManager.SetPlayerData(rp.user);
        ChestManager.instance.UpdateInfo();

        if(_AndOpen)
        {
            Open();
        }
    }

    private void _FastOpenChestResponce(ResponseData data)
    {
        if(data.ok!=0)
        {
            if (onFastOpenChest != null)
                onFastOpenChest(this);
        }
        _OpenChestResponce(data);
    }
    private void _CardOpenChestResponce(ResponseData data)
    {
        if (data.ok != 0)
        {
            if (onCardOpenChest != null)
                onCardOpenChest(this);
        }
        _OpenChestResponce(data);
    }

    private void _OpenChestResponce(ResponseData rp)
    {
        if (rp.ok == 0)
        {
            Debug.LogError("Open Chest Error: " + rp.error.code + " -> " + rp.error.text);
            if (rp.error.code.Contains("no_crystals") == false)
                TooltipMessageServer.Show(rp.error.text);
            if (cbChaestPanel!=null)
            {
                cbChaestPanel(rp.error.code);
            }
            PlayerManager.SetPlayerData(rp.user);
        }
        else
        {
            PlayerManager.SetPlayerData(rp.user);
            //List<UnitTower> bd = TowerDB.Load();
            List<Card> newCard = new List<Card>();

            foreach (ResponseData.PrizesTower PrizesTower in rp.prizes.towers)
            {
                for (int i = 0; i < PlayerManager.CardsList.Count; i++)
                {
                    if(PlayerManager.CardsList[i].IDTowerData == PrizesTower.type-1 && (int)PlayerManager.CardsList[i].rareness == PrizesTower.rarity-1)
                    {
                        newCard.Add(PlayerManager.CardsList[i]);
                        newCard[newCard.Count - 1].AddCount = PrizesTower.count;
                        newCard[newCard.Count - 1].count -= PrizesTower.count;
                        break;
                    }
                }                
            }
            ChestManager.instance.EndOpen(this, newCard, rp.prizes.gold, rp.prizes.crystals,rp.prizes.craft_resources);

            if (onOpenChest != null)
                onOpenChest(this);
        }


        ChestManager.instance.UpdateInfo();
    }

    public void UpgradeRarity(Card card)
    {
        if (card != null && card.Type == _CardType.Upgrade)
            PlayerManager.DM.UpgradeRarityChest(ID,(int)card.rareness + 1, UpgradeResponce);
    }

    public void DecreaseOpenTimeChest(Card card)
    {
        if (card != null && card.Type == _CardType.Upgrade)
            PlayerManager.DM.DecreaseOpenTimeChest(ID, (int)card.rareness + 1, Result);
    }

    private void UpgradeResponce(ResponseData data)
    {
        Result(data);
        if(data.ok != 0)
        {
            if (onUpgradeChest != null)
                onUpgradeChest(ID);
        }
    }

    private void Result(ResponseData data)
    {
        PlayerManager.SetPlayerData(data.user);
        if (data.ok == 0)
        {
            Debug.LogError(data.error.code + ": " + data.error.text);
            TooltipMessageServer.Show(data.error.text);
        }
        else
            ChestPanel.instance.Hide();
    }

    public void OpenChest(Card card)
    {
        if (card != null && card.Type == _CardType.Upgrade)
            PlayerManager.DM.OpenChest(ID, (int)card.rareness + 1, _CardOpenChestResponce);
    }

    public void DestroyChest(Card card)
    {
        if(card!=null && card.Type == _CardType.Upgrade)
            PlayerManager.DM.DestroyChest(ID, (int)card.rareness + 1, DestroyResponce);
    }

    private void DestroyResponce(ResponseData data)
    {
        PlayerManager.SetPlayerData(data.user);
        if (data.ok == 0)
        {
            Debug.LogError(data.error.code + ": " + data.error.text);
            TooltipMessageServer.Show(data.error.text);
        }
        else
        {
            ChestManager.instance.DestroyChestEnd(data.prizes.craft);
            if (onDestroyChest != null)
                onDestroyChest(ID);
        }
    }
}
