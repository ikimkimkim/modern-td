﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class ChestManager : MonoBehaviour
{
    public static ChestManager instance { get; private set; }

    public GameObject PrefabChest;

    public ChestOpenPanel openPanel;
    public ChestDestroyPanel destroyPanel;
    public ChestPanel ChestPanel;

    public Coroutine WaitChest { get; private set;}

    public GameObject[] TutorialGO;


    private static bool _ChestOpening = false;
    public static bool ChestOpening
    {
        get
        {
            _ChestOpening = false;
            for (int i = 0; i < PlayerManager.ChestList.Count; i++)
            {
                if (PlayerManager.ChestList[i].Status == 1)
                {
                    _ChestOpening = true;
                    break;
                }
            }
            return _ChestOpening;
        }
        set { _ChestOpening = value; }
    }

    public static int TypeNewChest = -1;
    public Animator AnimatorChest;
    public GameObject AnimChestObj;
    public Image AnimChestImage;


    public ChestUI[] ChestList=new ChestUI[4];


	void Start ()
    {
        //PlayerManager.DM.getChest(UpdateInfo);
        WaitChest = null;
        instance = this;
        UpdateInfo();
        PlayerManager.InitializationСompleted += UpdateInfo;
    }
    
    void OnDestroy()
    {
        PlayerManager.InitializationСompleted -= UpdateInfo;
    }

    public void ShowPanel(Chest c)
    {
        ChestPanel.Show(c);
        _EndTutorial();
    }

    public void EndOpen(Chest chest, List<Card> cardList, int Gold, int Cristal, Dictionary<string,int> CraftRes)
    {
        ChestOpening = false;
        openPanel.Show(chest,cardList,Gold,Cristal,CraftRes);
        ChestPanel.Hide();
    }  

    public void DestroyChestEnd(Dictionary<string, int> CraftRes)
    {
        ChestPanel.Hide();
        destroyPanel.Show(CraftRes);
    }

    public void UpdateInfo()
    {
        ChestOpening = false;

        for (int i = 0; i <4; i++)
        {

            if (i < PlayerManager.ChestList.Count)
            {
                if (i == PlayerManager.ChestList.Count - 1)
                {
                    if(TypeNewChest > -1)
                    {
                        ChestList[i].DestroyChest();
                        if (WaitChest != null)
                            StopCoroutine(WaitChest);
                        WaitChest = StartCoroutine(WaitAnim(i));
                    }
                    else if(WaitChest==null)
                    {
                        ChestList[i].SetChest(PlayerManager.ChestList[i]);
                        if (PlayerManager.ChestList[i].Status == 1)
                            ChestOpening = true;
                    }
                }
                else
                {
                    ChestList[i].SetChest(PlayerManager.ChestList[i]);
                    if (PlayerManager.ChestList[i].Status == 1)
                        ChestOpening = true;
                }
            }
            else
                ChestList[i].DestroyChest();
        }
    }

    private int _TutorialArrowIndexWait=-1;
    private IEnumerator WaitAnim(int id)
    {
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsChest[TypeNewChest], value => { AnimChestImage.sprite = value; }));
        AnimChestObj.SetActive(true);
        AnimatorChest.SetInteger("ChestPos", id);
        yield return new WaitForSeconds(3.0f);
        TypeNewChest = -1;
        AnimChestObj.SetActive(false);
        WaitChest = null;
        if(_TutorialArrowIndexWait>-1)
        {
            TutorialGO[_TutorialArrowIndexWait].SetActive(true);
            _TutorialArrowIndexWait = -1;
        }
    }

    public void SetLastChest()
    {
        ChestList[PlayerManager.ChestList.Count - 1].SetChest(PlayerManager.ChestList[PlayerManager.ChestList.Count - 1]);
        if (PlayerManager.ChestList[PlayerManager.ChestList.Count - 1].Status == 1)
            ChestOpening = true;
    }

    private Action _tutorialEndEvent;
    public static bool StartTutorial(Chest.TypeChest type, Action endEvent) { return instance._StartTutorial(type, endEvent); }
    public bool _StartTutorial(Chest.TypeChest type, Action endEvent)
    {
        _tutorialEndEvent = endEvent;
        for (int i = 0; i < PlayerManager.ChestList.Count; i++)
        {
            if(PlayerManager.ChestList[i].Type == type)
            {
                if (TypeNewChest > -1)
                    _TutorialArrowIndexWait = i;
                else
                    TutorialGO[i].SetActive(true);
                return true;
            }
        }
        return false;
    }

    public static void EndTutorial() { instance._EndTutorial(); }
    public void _EndTutorial()
    {
        for (int i = 0; i < TutorialGO.Length; i++)
        {
            if (TutorialGO[i].activeSelf)
                TutorialGO[i].SetActive(false);
        }
        _TutorialArrowIndexWait = -1;
        if (_tutorialEndEvent != null)
            _tutorialEndEvent();
    }
}
