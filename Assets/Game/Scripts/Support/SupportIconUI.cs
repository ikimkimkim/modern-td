﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SupportIconUI : MonoBehaviour
{

    public GameObject rootObj;
    public Image icon;
    public Text level;
    public GameObject needLevelUpObj;
    public Button button;



    public void ShowLevelInfo()
    {
        StayInSameWorldPos.setPos(level.transform.position + Vector3.up*10, "Уровень");
    }

    public void HideLevelInfo()
    {
        StayInSameWorldPos.Hide();
    }
}
