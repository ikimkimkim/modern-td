﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TDTK;
using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class SupportUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
 {

    [Serializable]
    public struct panel
    {
        public GameObject rootObj;
        public Image image;
        public Text text;
    }


    public static SupportUI instance { get; private set; }
    private UnitHero unitHero;
    private List<UnitHero> listUnitHero;
    public GameObject panelOneUnit;
    public Animator animator;
    public Image Icon;
    public ProgressBar progressBarShield;
    public ProgressBar progressBarHP;
    public ProgressBar progressBar;
    public Text Name,Level,MaxLevel, CountKillTxt, AllDmgTxt;
    public panel[] imageTalans;
    
    public GameObject EffectNeedActive;

    public Image iconTypeDmg;
    public Text tooltipTypeDmg;
    
    public TowerSelectedInfoPanel.PropertiesPanel[] propertiesPanels;
    public Sprite IconNotProperties;
    public Sprite IconNotSelectProperties;

    //private CardProperties[] listProperties;
    public Color activeTalant, deactiveTalant;
    public Color textDuration, textCooldown;

    public UIEffectPanelManager effectPanelManager;

    [Header("Mass seleted")]
    [SerializeField]
    private GameObject panelAllUnit, PrefabButtonUnit;
    private List<SupportIconUI> listIcons;
    [SerializeField]
    private Transform ContentButton;


    [Space]
    public bool isShow = false;

    private void Awake()
    {
        instance = this;
        listUnitHero = new List<UnitHero>();
        listIcons = new List<SupportIconUI>();
    }

    public static void SelectedHero(UnitHero hero)
    {
        if (instance == null)
        {
            MyLog.LogError("SelectedHero Instance support null",MyLog.Type.build);
            return;
        }
        instance._SelectedHero(hero);
    }
    public void _SelectedHero(UnitHero hero)
    {
        if(listUnitHero.Count == 0)
            animator.SetBool("Show", true);

        listUnitHero.Add(hero);


        if (listUnitHero.Count == 1)
        {
            unitHero = listUnitHero[0];
            HideAllUnit();
            Show();
        }
        else if(listUnitHero.Count>1)
        {
            Hide();
            ShowAllUnit();
        }
    }

    private void ShowAllUnit()
    {
        if(!panelAllUnit.activeSelf)
            panelAllUnit.SetActive(true);
        for (int i = 0; i < listUnitHero.Count; i++)
        {
            if(listIcons.Count<=i)
            {
                GameObject go = Instantiate(PrefabButtonUnit, ContentButton);
                listIcons.Add(go.GetComponent<SupportIconUI>());
            }

            if (!listIcons[i].rootObj.activeSelf)
                listIcons[i].rootObj.SetActive(true);
            listIcons[i].level.text = (listUnitHero[i].currentActiveStat + 1).ToString();
            listIcons[i].icon.sprite = listUnitHero[i].iconSprite;
            if(listUnitHero[i].isNeedUpdate != listIcons[i].needLevelUpObj.activeSelf)
                listIcons[i].needLevelUpObj.SetActive(listUnitHero[i].isNeedUpdate);
            listIcons[i].button.onClick.RemoveAllListeners();
            int index = i;
            listIcons[i].button.onClick.AddListener(() => OnSelectedOneUnit(index));
        }

        for (int i = listUnitHero.Count; i < listIcons.Count; i++)
        {
            if(listIcons[i].rootObj.activeSelf)
                 listIcons[i].rootObj.SetActive(false);
        }
    }

    private void OnSelectedOneUnit(int index)
    {
        var selectUnit = listUnitHero[index];

        var array = listUnitHero.ToArray();
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] != selectUnit)
            {
                array[i].GetComponent<SelectedUnitCompanent>().Deselected();
            }
        }
        UISelected.bLockSelected = true;
    }

    private void HideAllUnit()
    {
        panelAllUnit.SetActive(false);
    }
/*
    private void propertiesAction(object sender, eventPropertiesArgs e)
    {
        UpdateStatusUnit(e);

        //Debug.Log(e.id +"P ACTION"+e.Cooldoun);
    }
    */
    public static void DeSelectedHero(UnitHero hero)
    {
        if(instance==null)
        {
            Debug.LogError("DeSelectedHero Instance support null");
            return;
        }
        instance._DeSelectedHero(hero);
    }
    public void _DeSelectedHero(UnitHero hero)
    {
        listUnitHero.Remove(hero);

        if (listUnitHero.Count == 0)
        {
            animator.SetBool("Show", false);
            Hide();
            unitHero = null;
        }
        else if(UISelected.CountSelectUnit == 1 && listUnitHero.Count == 1)
        {
            unitHero = listUnitHero[0];
            Show();
            HideAllUnit();
        }
        else if(listUnitHero.Count>1)
        {
            ShowAllUnit();
        }
    }

    public void OnShowAllInfoFromSelected()
    {
        CardUIAllInfo.instance.Show(unitHero.MyCard, CardUIAllInfo.Mode.onlyCancel);
    }

    public void OnStartSelectedProperties()
    {
        unitHero.StartActiveProperties();
        if (!TowerSelectProperties.bShowAlreadySelectedProperties)
        {
            Hide();
            Show();
        }
    }

    private void Show()
    {
        panelOneUnit.SetActive(true);
        unitHero.unitUpdateXP += UpdateXpHero;
        Name.text = unitHero.unitName;
        Icon.sprite = unitHero.iconSprite;


        StartCoroutine(TextureDB.instance.load(unitHero.UrlIconDamageType, (Sprite value) => { iconTypeDmg.sprite = value; }));

        List<CardProperties> prop = unitHero.Properties.FindAll(p => p.Requirements.Find(i => i.ID == 5) != null);

        for (int i = 0; i < propertiesPanels.Length; i++)
        {
            /*if (i + 2 > SkillPanelInCardAllInfo.LevelRelease)
            {
                propertiesPanels[i].icon.sprite = IconNotProperties;
                propertiesPanels[i].textInfo.text = "В будущих обновлениях";
                continue;
            }*/

            if (OpenPropertiesStarPerk.isOpenLevel(_CardType.Hero,i+2) == false)
            {
                propertiesPanels[i].icon.sprite = IconNotProperties;
                propertiesPanels[i].textInfo.text = "Не изучено";
                continue;
            }

            if (prop.Count / 2 >= i + 1)
            {
                propertiesPanels[i].icon.sprite = IconNotSelectProperties;
                propertiesPanels[i].textInfo.text = "Не выбрано";
            }
            else
            {
                propertiesPanels[i].icon.sprite = IconNotProperties;
                propertiesPanels[i].textInfo.text = "У более редких башнях";
            }
        }

        RequirementsProperties rp;
        for (int i = 0; i < prop.Count; i++)
        {

            if (prop[i].CanActive())
            {
                rp = prop[i].Requirements.Find(f => f.ID == 5);
                if (rp != null)
                {
                    int levelProp = (int)rp.Param[0];
                    StartCoroutine(TextureDB.instance.load(prop[i].UrlIcon, (Sprite value) => { propertiesPanels[levelProp - 2].icon.sprite = value; }));
                    propertiesPanels[levelProp - 2].textInfo.text = prop[i].getFullInfoText();
                }
            }
        }
        effectPanelManager.Show(unitHero);
        setInfo();
        isShow = true;
    }

    private void Hide()
    {
        if (isShow == false)
            return;
        panelOneUnit.SetActive(false);
        unitHero.unitUpdateXP -= UpdateXpHero;
        effectPanelManager.Hide();
        isShow = false;
    }


    public void UpdateXpHero(object sender, EventArgs a)
    {
        UnitHero hero = (UnitHero)sender;
        if (hero == unitHero)
        {
            setInfo();
        }
        else
        {
            Debug.LogError("SupportUI UpdateXp not select hero");
        }
    }

    private void setInfo()
    {
        tooltipTypeDmg.text = String.Format("<color=#fba81d>{0} урон</color> \r\n{1}-{2}", DamageTable.GetDamageTypeInfo(unitHero.DamageType()).name, Math.Round(unitHero.GetDamageMin(), 1), Math.Round(unitHero.GetDamageMax(), 1));
        Level.text = (unitHero.currentActiveStat + 1).ToString();
        float p = 0;

        if (unitHero.currentActiveStat == unitHero.stats.Count - 1)
        {
            MaxLevel.text = "Max";
            p = 1;
        }
        else
        {
            p = unitHero.XP / 
                unitHero.MaxXP[unitHero.MyCard.getIntRareness]
                [unitHero.currentActiveStat]; 
            MaxLevel.text = "";
        }

        progressBar.SetProgress(p);

        if (unitHero.isNeedUpdate != EffectNeedActive.activeSelf)
            EffectNeedActive.SetActive(unitHero.isNeedUpdate);       
    }
/*
    public void UpdateStatusUnit(eventPropertiesArgs status)
    {
        int i = status.id;
        if (status.Cooldown > 0)
        {
            imageTalans[i].image.color = deactiveTalant;
            imageTalans[i].text.color = textCooldown;
            if (status.Cooldown < 10)
                imageTalans[i].text.text = (Mathf.Round(status.Cooldown * 10f) / 10f).ToString();
            else
                imageTalans[i].text.text = (Mathf.Round(status.Cooldown)).ToString();

        }
        else if (status.TimeDuration > 0)
        {
            imageTalans[i].image.color = activeTalant;
            imageTalans[i].text.color = textDuration;
            if (status.TimeDuration < 10)
                imageTalans[i].text.text = (Mathf.Round(status.TimeDuration * 10f) / 10f).ToString();
            else
                imageTalans[i].text.text = (Mathf.Round(status.TimeDuration)).ToString();
        }
        else
        {
            if (i <= unitHero.currentActiveStat)
                imageTalans[i].image.color = activeTalant;
            else
                imageTalans[i].image.color = deactiveTalant;
            imageTalans[i].text.text = "";
        }
    }
    */
    public void PointEnter(int id)
    {
            Debug.LogWarning("Support UI point enter, id over mass");
    }

    public void PointExit()
    {
        StayInSameWorldPos.Hide();
    }


    void Update()
    {
        if (isShow == false)
            return;
        if(progressBarShield!=null)
            progressBarShield.SetProgress(unitHero.fullHP>0? unitHero.shield / unitHero.fullShield : 0);
        progressBarHP.SetProgress(unitHero.HP / unitHero.fullHP);

        CountKillTxt.text = unitHero.countKillUnitCurrentGame.ToString();
        AllDmgTxt.text = unitHero.allDmgCurrentGame.ToStringDmg();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        UISelected.bLockSelected = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        UISelected.bLockSelected = true;
    }
}
