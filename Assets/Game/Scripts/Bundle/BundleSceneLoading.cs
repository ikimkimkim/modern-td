﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public static class BundleSceneLoading
{
    private static AssetBundle resources, scene;
    public static event Action startLoading, completeLoading;

    public static float Progress { get; private set; }

    public static IEnumerator Loading(string nameBundleLevel)
    {
        Debug.Log("Start loading scene bundle:"+nameBundleLevel);

        Progress = 0;
        if (startLoading != null)
            startLoading();
        yield return null;

        while (!Caching.ready)
            yield return null;

        while (!BundleManager.isManifestLoaded)
            yield return null;

        yield return LoadingResources(nameBundleLevel);

        yield return LoadingScene(nameBundleLevel);

        yield return null;
        if(completeLoading != null)
            completeLoading();

        yield return null;
        UnloadBundle(false);
        Debug.Log("Complete loading scene bundle:" + nameBundleLevel);

        yield break;
    }

    private static void ChangesProgressOperation(float progress,string nameOperation)
    {
        switch(nameOperation)
        {
            case "LoadingResources":
                Progress =0.25f + 0.25f * progress;
                break;
            case "LoadingScene":
                Progress = 0.75f + 0.25f * progress;
                break;
            default:
                Debug.LogError("BundleSceneLoading change progress, name opretaion not known: " + nameOperation);
                break;
        }
    }

    public static object[] AllTerrain, AllNavMesh, AllGameObject;

    private static IEnumerator LoadingResources(string nameBundleLevel)
    {
        if (BundleManager.ExistBundle(nameBundleLevel + "/resources") == false)
        {
            Debug.LogError("Bandle " + nameBundleLevel + "/resources not exit");
            yield break;
        }

        string url = ServerManager._Domain + "/td/static/bundles/" + nameBundleLevel + "/resources";
        var version = BundleManager.GetHashBundle(nameBundleLevel + "/resources");
        Debug.Log(string.Format("Loading bundle {0} v:{1}", url, version.ToString()));

        using (WWW www = new WWW(url +"?v="+ version))
        //using (WWW www = WWW.LoadFromCacheOrDownload(url, version))
        {
            while(!www.isDone)
            {
                Progress = 0.25f * www.progress;
                yield return null;
            }
            Progress = 0.25f;
            Debug.Log("Loading bundle Resources complete.");

            resources = www.assetBundle;
            AllTerrain = resources.LoadAllAssets<TerrainData>();
            Debug.Log("AllTerrain:" + AllTerrain.Length);
            Progress = 0.35f;
            yield return null;
            AllNavMesh = resources.LoadAllAssets(typeof(NavMeshData));
            Debug.Log("AllNavMesh:" + AllNavMesh.Length);
            Progress = 0.45f;
            yield return null;
            AllGameObject = resources.LoadAllAssets();
            Debug.Log("AllGameObject:" + AllGameObject.Length);
            Progress = 0.5f;
            yield return null;
        }
        //yield return AsyncOperations.ShowAsyncOperation(resources.LoadAllAssetsAsync(), "LoadingResources", ChangesProgressOperation);

        Debug.Log("Loading asset Resources complete.");

        yield break;
    }


    private static IEnumerator LoadingScene(string nameBundleLevel)
    {
        if (BundleManager.ExistBundle(nameBundleLevel + "/scene") == false)
        {
            Debug.LogError("Bandle " + nameBundleLevel + "/scene not exit");
            yield break;
        }

        string url = ServerManager._Domain + "/td/static/bundles/" + nameBundleLevel + "/scene";
        var version = BundleManager.GetHashBundle(nameBundleLevel + "/scene");
        Debug.Log(string.Format("Loading bundle {0} v:{1}", url, version.ToString()));

        using (WWW www = new WWW(url + "?v=" + version))
       // using (WWW www = WWW.LoadFromCacheOrDownload(url, version))
        {
            while (!www.isDone)
            {
                Progress = 0.5f + 0.25f * www.progress;
                yield return null;
            }
            Progress = 0.75f;

            Debug.Log("Loading bundle Scene complete.");

            scene = www.assetBundle;
            string[] pathScene = scene.GetAllScenePaths();

            Debug.Log("Loading asset Scene complete.");


            if (scene.isStreamedSceneAssetBundle)
            {
                yield return AsyncOperations.ShowAsyncOperation(SceneManager.LoadSceneAsync(pathScene[0]), "LoadingScene", ChangesProgressOperation);
            }
        }

        Progress = 1;
        Debug.Log("Loading scene complete.");


        yield break;
    }


    public static void UnloadBundle(bool unloadAllLoadedObj)
    {
        Debug.LogWarning("UnloadBundle!");
        if(resources!= null)
        {
            resources.Unload(unloadAllLoadedObj);
            resources = null;
            AllTerrain = null;
            AllNavMesh = null;
        }
        if(scene!=null)
        {
            scene.Unload(unloadAllLoadedObj);
            scene = null;
        }
        
    }
}
