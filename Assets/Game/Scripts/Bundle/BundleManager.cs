﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

public class BundleManager : Singleton<BundleManager>
{
    public const string FolderBundels = "/td/static/bundles/";

    private const string _NameManifest = "bundles";

    private Coroutine loadingSceneCor;
    private AssetBundleManifest manifest;
    protected bool _isManifestLoaded = false;

    private static bool CheckManifest
    {
        get
        {
            if (instance._isManifestLoaded)
                return true;
            else
            {
                Debug.LogError("BundleManager error, not loading manifest");
                return false;
            }
        }
    }

    public static string GetUrlBundle(string nameBundle)
    {
        return ServerManager._Domain + FolderBundels + nameBundle;
    }

    public static bool ExistBundle(string nameBudle)
    {
        if (CheckManifest)        
            return instance.manifest.GetAllAssetBundles().Contains(nameBudle);
        
        return false;
    }

    public static Hash128 GetHashBundle(string nameBundle)
    {
        if (CheckManifest)
            return instance.manifest.GetAssetBundleHash(nameBundle);        
        return new Hash128(0, 0, 0, 0);
    }



    public static bool isManifestLoaded { get { return instance._isManifestLoaded; } }

    private void Start()
    {
        instance = this;
    }

    protected override void InitializationSingleton()
    {
        _isManifestLoaded = false;
        StartCoroutine(LoadManifest(_NameManifest));
    }

    private IEnumerator LoadManifest(string nameManifest)
    {
        while (string.IsNullOrEmpty(ServerManager._Domain))
            yield return null;

        string path = ServerManager._Domain + "/td/static/bundles/" + nameManifest + "?v="+new TimeSpan(DateTime.Now.Ticks).TotalSeconds;
        Debug.Log("Loading manifest:" + path);

        using (WWW www = new WWW(path))
        {
            yield return www;

            if (string.IsNullOrEmpty(www.error) == false)
            {
                Debug.LogError("Loading manifest error:" + www.error);
                yield break;
            }

            manifest = www.assetBundle.LoadAsset("AssetBundleManifest") as AssetBundleManifest;
            if (manifest == null)
            {
                Debug.LogError("Loading manifest is null");
            }
            yield return null;
            www.assetBundle.Unload(false);

            _isManifestLoaded = true;
        }

#if UNITY_EDITOR
       /* Debug.Log("manifest content:");
        var str = manifest.GetAllAssetBundles();
        for (int i = 0; i < str.Length; i++)
        {
            Debug.Log(str[i]);
        }
        Debug.Log("------------------");*/
#endif
        yield break;
    }

    public static void StartLoadingScene(string bundleName) {  instance._StartLoadingScene(bundleName); }
    private void _StartLoadingScene(string bundleName)
    {       
        if(loadingSceneCor !=null)
        {
            Debug.LogError("Can't start loading bundle, because old loading not complete!");
            StopCoroutine(loadingSceneCor);
        }
        loadingSceneCor = StartCoroutine(BundleSceneLoading.Loading(bundleName));
    }


    public static void LoadingAllAsset(string bundleName, Action<Object[]> completedLoad)
    {
        new BundleLoading(bundleName, completedLoad);
    }
    

    public static void StartLoading(IEnumerator metod)
    {
        instance._StartLoading(metod);
    }

    private void _StartLoading(IEnumerator metod)
    {
        StartCoroutine(metod);
    }


}
