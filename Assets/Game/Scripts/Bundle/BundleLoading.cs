﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Object = UnityEngine.Object;

public class BundleLoading
{
    private Object[] allAssetsBundleLoad;
    private Action<Object[]> _CompletedLoad;

    public BundleLoading(string bundleName, Action<Object[]> completedLoading)
    {
        _CompletedLoad = completedLoading;
        allAssetsBundleLoad = null;
        BundleManager.StartLoading(_Loading(bundleName));
    }

    private IEnumerator _Loading(string bundleName)
    {
        while (BundleManager.isManifestLoaded == false)
        {
            yield return new WaitForSecondsRealtime(0.5f);
        }

        if (BundleManager.ExistBundle(bundleName) == false)
        {
            Debug.LogError("Bundle " + bundleName + " not exit");
            _CompletedLoad(null);
            yield break;
        }

        string url = BundleManager.GetUrlBundle(bundleName);
        var version = BundleManager.GetHashBundle(bundleName);
        Debug.Log(string.Format("Loading bundle {0} v:{1}", url, version.ToString()));



        var request = UnityWebRequestAssetBundle.GetAssetBundle(url);

        yield return request.SendWebRequest();
        AssetBundle bundle = null;
        if (!request.isHttpError && !request.isNetworkError)
        {
            bundle = DownloadHandlerAssetBundle.GetContent(request);
        }
        else
        {
            Debug.LogErrorFormat("error request [{0}, {1}]", url, request.error);

            _CompletedLoad(null);
        }

        request.Dispose();





        /*
        WWW www = WWW.LoadFromCacheOrDownload(url , version);


        //using (WWW www = new WWW(url + "?v=" + version))
        //using (WWW www = WWW.LoadFromCacheOrDownload(url, version))
       // {
            while (!www.isDone)
            {
                yield return null;
            }
            Debug.LogFormat("Loading bundle {0} loaded.", bundleName);

        if(string.IsNullOrEmpty(www.error) == false)
        {
            Debug.LogFormat("Loading bundle {0} error {1}.", bundleName, www.error);
            _CompletedLoad(null);
            yield break;
        }

        var load = www.assetBundle.LoadAllAssetsAsync();// LoadAllAssets<T>();
        while(load.isDone ==false)
        {
            yield return null;
        }

        _CompletedLoad(allAssetsBundleLoad);
        yield break;*/

        allAssetsBundleLoad = bundle.LoadAllAssets();
        _CompletedLoad(allAssetsBundleLoad);

        allAssetsBundleLoad = null;
        _CompletedLoad = null;

        Debug.LogFormat("Loading bundle {0} end.", bundleName);

        yield break;
    }


}
