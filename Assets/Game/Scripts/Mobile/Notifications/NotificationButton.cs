﻿using UnityEngine;

using System.Collections;

public class NotificationButton : MonoBehaviour
{
    public GameObject Button;
#if UNITY_WEBGL || UNITY_WEBPLAYER
    void Start()
    {
        //Destroy(gameObject);
        //return;
    }
#endif
#if UNITY_ANDROID || UNITY_IOS
    static bool _bonusPending = false;
    void Start()
    {
        if (_bonusPending || BonusManager.isFirstBonusRecieved())
        {
            Button.SetActive(true);
        }
        else
        {
            Button.SetActive(false);
        }
    }
    void OnEnable()
    {
        BonusManager.OnGetBonus += BonusManager_OnGetBonus;
    }

    void BonusManager_OnGetBonus()
    {
        _bonusPending = true;
        Button.SetActive(true);
    }
    void OnDisable()
    {
        BonusManager.OnGetBonus -= BonusManager_OnGetBonus;
    }
#endif
}
