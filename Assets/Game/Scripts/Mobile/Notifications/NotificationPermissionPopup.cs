﻿using UnityEngine;
using System.Collections;
using System;

public class NotificationPermissionPopup : MonoBehaviour
{
    public GameObject Background;
    public GameObject BlackScreen;
#if UNITY_ANDROID || UNITY_IOS
    public static event Action<bool> OnPlayerGavePermission;
    Action _callbackOnYes;
    public void Show()
    {
        if (!NotificationManager.IOSPlayerChoosed)
        {
            Background.SetActive(true);
            BlackScreen.SetActive(true);
        }

    }
    
    public void OnButtonClick()
    {
        if (OnPlayerGavePermission != null)
        {
            OnPlayerGavePermission(true);
        }
        Hide();
    }
#endif
public void Hide()
    {
        Background.SetActive(false);
        BlackScreen.SetActive(false);
    }
}
