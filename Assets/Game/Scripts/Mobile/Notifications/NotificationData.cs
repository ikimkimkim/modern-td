﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class NotificationData
{
    public bool IOSPlayerChoosed = false;
    public bool RecieveNotificationsIOs = false;
    public List<string> BonusIDs = new List<string>();
    public List<string> PlayerCallIDs = new List<string>();
}