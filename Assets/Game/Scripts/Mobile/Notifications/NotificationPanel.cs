﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using Translation;
public class NotificationPanel : MonoBehaviour
{

    public GameObject Background;
    public GameObject BlackScreen;

    public GameObject BonusRow;
    public Button BonusButton;
    public Text BonusButtonText;
    public Text BounusText;
    public GameObject Tooltip;
    
    public static event Action OnBonusTaken;
    //static bool _bonusPending = false;

    public GameObject GOinfoTimeBonus;
    public GameObject GiftOnRow, GiftOffRow;
    //DateTime TimeTakeBonus;
    
    //bool _showTimeToBonus = false;
    double _secondsToBonus;

    public static event Action OnShow;
    public static event Action OnTimerFinished;

    void Start()
    {
        BonusManager.OnGetBonus += OnGetBonus;
        BonusManager.OnBonusSet += BonusManager_OnBonusSet;
        BonusManager.OnDidntReciveTimeFromServer += BonusManager_OnDidntReciveTimeFromServer;

        if (PlayerManager._instance._playerData.available_help_friends == 1)
            HelpFriendsPanel.ActionHideSendPanel += hideSendHelpPanel;
        else if (PlayerManager._instance._playerData.daily_bonus == 1)
            ShowPanel();
        
        #if UNITY_IOS || UNITY_ANDROID
        NotificationManager.OnLaunchWithNotification += NotificationManager_OnLaunchWithNotification;
#endif
   }
    

    public void hideSendHelpPanel()
    {
        HelpFriendsPanel.ActionHideSendPanel -= hideSendHelpPanel;
        if (PlayerManager._instance._playerData.daily_bonus == 1)
            ShowPanel();
    }

    void BonusManager_OnBonusSet(long obj)
    {
        ShowTimeToNextBonus();
    }

    void NotificationManager_OnLaunchWithNotification(string arg1, string arg2)
    {
        if (arg2 == "Yes")
        {
            ShowPanel();
        }
    }
    void BonusManager_OnDidntReciveTimeFromServer()
    {
        if (Background.activeInHierarchy)
        {
            BonusButtonText.text = TranslationEngine.Instance.Trans["NotificationPanel.BonusRow.ButtonNoConnection"];
        }
    }

    void OnDisable()
    {
        BonusManager.OnGetBonus -= OnGetBonus;
        BonusManager.OnBonusSet -= BonusManager_OnBonusSet;
        BonusManager.OnDidntReciveTimeFromServer -= BonusManager_OnDidntReciveTimeFromServer;
#if UNITY_IOS || UNITY_ANDROID
        NotificationManager.OnLaunchWithNotification -= NotificationManager_OnLaunchWithNotification;
#endif
        }
    void Update()
    {
        if (CardUIAllInfo.isShow == false && BlackScreen.activeSelf == true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                HidePanel();
            }
        }

#if UNITY_IOS || UNITY_ANDROID
        if (_showTimeToBonus)
        {
            _secondsToBonus -= Time.deltaTime;
            if (_secondsToBonus > 0)
            {
                BonusButtonText.text = TimeToBonusFormat(_secondsToBonus);
            }
            else
            {
                if (OnTimerFinished != null)
                {
                    OnTimerFinished();
                }
                if (!InternetChecker.isInternetAvailable())
                {
                    BonusButtonText.text = TranslationEngine.Instance.Trans["NotificationPanel.BonusRow.ButtonNoConnection"];
                }
                else
                {
                    BonusButtonText.text = "00:00:00";
                }
                _showTimeToBonus = false;
            }
        }
#endif
#if UNITY_WEBGL || UNITY_WEBPLAYER

        if (PlayerManager._instance._playerData.daily_bonus == 0 && /*PlayerManager._instance._playerData.timeSet!=null &&*/
            PlayerManager._instance._playerData.timeSet!=DateTime.MinValue && PlayerManager._instance._playerData.daily_bonus_ttl>0)
        {
            GOinfoTimeBonus.SetActive(true);
            TimeSpan timeold = DateTime.Now - PlayerManager._instance._playerData.timeSet;
            _secondsToBonus = PlayerManager._instance._playerData.daily_bonus_ttl - timeold.TotalSeconds;
            if (_secondsToBonus > 0)
            {
                BonusButtonText.text = TimeToBonusFormat(_secondsToBonus);
                GOinfoTimeBonus.GetComponentInChildren<Text>().text=TimeToBonusFormat(_secondsToBonus);
            }
            else
            {
                GOinfoTimeBonus.SetActive(false);
                BonusButtonText.text = "...";
                PlayerManager._instance._playerData.daily_bonus_ttl = -1;
                if (OnTimerFinished != null)
                {
                    OnTimerFinished();
                }
                PlayerManager.DM.getPlayerData(); //SocialManager.Instance.UpdateInfoPlayer();
            }
        }
        else
        { 
            GOinfoTimeBonus.SetActive(false);
        }
        if (PlayerManager._instance._playerData.daily_bonus == 0 && PlayerManager._instance._playerData.daily_bonus_ttl == 0)
        {
            BonusButtonText.text = TranslationEngine.Instance.Trans["NotificationPanel.BonusRow.ButtonOff"];
        }

#endif
        }
    void OnGetBonus()
    {
        //_bonusPending = true;
        ShowPanel();
    }
    public void ShowPanel()
    {

        if (PlayerManager._instance._friendsData == null || PlayerManager._instance._friendsData.friends == null)
        {
            Debug.LogError("ShowPanel error, FriendsData or friends is null");
            return;
        }

        BlackScreen.SetActive(true);
        Background.SetActive(true);

#if UNITY_IOS || UNITY_ANDROID
        BounusText.text = TranslationEngine.Instance.Trans["NotificationPanel.BonusRow.Text"].Replace("\\n", "\n");
        if (_bonusPending)
        {
            BonusButtonText.text = TranslationEngine.Instance.Trans["NotificationPanel.BonusRow.ButtonOn"];
            BonusButton.interactable = true;
            _showTimeToBonus = false;

        }
        else
        {
            if (OnShow != null)
            {
                OnShow();
            }
            if (!InternetChecker.isInternetAvailable())
            {
                BonusButtonText.text = TranslationEngine.Instance.Trans["NotificationPanel.BonusRow.ButtonNoConnection"];
                _showTimeToBonus = false;
            }
            else
            {
                ShowTimeToNextBonus();
            }
            BonusButton.interactable = false;
        }
#endif
#if UNITY_WEBGL || UNITY_WEBPLAYER

        
        BonusManager.instance._crystalsBouns = 10 + 4 * PlayerManager._instance._friendsData.friends.Length;
        if (BonusManager.instance._crystalsBouns > 50)
            BonusManager.instance._crystalsBouns = 50;
        BounusText.text = BonusManager.instance._crystalsBouns.ToString();

        GiftOnRow.SetActive(PlayerManager._instance._playerData.daily_bonus == 1);
        GiftOffRow.SetActive(PlayerManager._instance._playerData.daily_bonus == 0);

        if (PlayerManager._instance._playerData.daily_bonus==1)
        {
            BonusButtonText.text = TranslationEngine.Instance.Trans["NotificationPanel.BonusRow.ButtonOn"];
            BonusButton.interactable = true;
            //_showTimeToBonus = false;
        }
        else
        {
            if (OnShow != null)
            {
                OnShow();
            }
            //
            //_showTimeToBonus = true;
            BonusButtonText.text = TimeToBonusFormat(_secondsToBonus);
            BonusButton.interactable = false;
        }
#endif
    }

    private void ShowTimeToNextBonus()
    {
#if UNITY_IOS || UNITY_ANDROID
        var timeToBonus = BonusManager.GetTimeToNextBonus();
        if (timeToBonus != TimeSpan.MaxValue)
        {
            _secondsToBonus = timeToBonus.TotalSeconds;
            _showTimeToBonus = true;
            BonusButtonText.text = TimeToBonusFormat(_secondsToBonus);
        }
        else
        {
            _showTimeToBonus = false;
            BonusButtonText.text = TranslationEngine.Instance.Trans["NotificationPanel.BonusRow.ButtonOff"];
        }
#endif
#if UNITY_WEBGL || UNITY_WEBPLAYER
        _secondsToBonus = PlayerManager._instance._playerData.daily_bonus_ttl;
       /* if(_secondsToBonus>0)
        {
            _showTimeToBonus = true;
        }
        else
            _showTimeToBonus = false;*/
#endif

    }



    public void OnBonusButton()
    {

#if UNITY_WEBGL || UNITY_WEBPLAYER
        if (PlayerManager._instance._playerData.daily_bonus==1)
        {
            BonusButtonText.text = "...";
            BonusButton.interactable = false;
            //_showTimeToBonus = true;
            PlayerManager._instance._playerData.daily_bonus =-1;

            GiftOnRow.SetActive(false);
            GiftOffRow.SetActive(true);
            
            if (OnBonusTaken != null)
            {
                OnBonusTaken();
                StartCoroutine(ShowTooltip());
            }
            //TimeTakeBonus = DateTime.Now;
        }
#endif
#if UNITY_IOS || UNITY_ANDROID
        if (_bonusPending)
        {
            _bonusPending = false;
            BonusButtonText.text = TranslationEngine.Instance.Trans["NotificationPanel.BonusRow.ButtonOff"];
            BonusButton.interactable = false;
            if (OnBonusTaken != null)
            {
                OnBonusTaken();
                StartCoroutine(ShowTooltip());
            }
        }
#endif
    }
    IEnumerator ShowTooltip()
    {
        Tooltip.SetActive(true);
        yield return new WaitForSeconds(3);
        Tooltip.SetActive(false);
        yield break;
    }

    public void ShowOnIOSPermissionPopupIfNoBonusWas()
    {
#if UNITY_IOS
        var permissionPopup = GameObject.FindGameObjectWithTag("IOSNotificationPermission").GetComponent<NotificationPermissionPopup>();
        permissionPopup.Show();
#endif
    }


    string TimeToBonusFormat(double timeToBonus)
    {
        int hours = (int)(timeToBonus / (60 * 60));
        timeToBonus -= hours * 60 * 60;
        int minutes = (int)(timeToBonus / (60));
        timeToBonus -= minutes * 60;
        int seconds = (int)timeToBonus;
        return string.Format("{0:D2}:{1:D2}:{2:D2}", hours, minutes, seconds);
    }

    public void HidePanel()
    {
        BlackScreen.SetActive(false);
        Background.SetActive(false);
        Tooltip.SetActive(false);
    }



#if UNITY_WEBGL || UNITY_WEBPLAYER



    


    public void OnInviteFriends()
    {
        if (SocialManager.Instance != null)
            SocialManager.Instance.InviteFriends();
        else
            Debug.LogWarning("SM is null!");
    }
#endif
}
