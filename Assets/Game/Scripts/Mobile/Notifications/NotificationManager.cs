﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using VoxelBusters.Utility;
//using VoxelBusters.NativePlugins;
using System.IO;
using System;
using System.Runtime.Serialization.Formatters.Binary;
//"You can configure this feature in NPSettings->Notification Settings.",
//"For Android platform notification, complete customisation of payload keys is allowed. You can modify these keys in Notification Settings.",
//"Notification workflow is straight forward." +
//"\n1. Register for notification types that your application needs." +
//"\n2. Register for remote notifications, only in case your application needs remote notification support.",
//"You can also validate your payload string in Unity Editor itself. Just select Editor Notification Center from Menu (Window->Voxel Busters->Native Plugins) and try push notification.");

public class NotificationManager : MonoBehaviour
{
#if UNITY_ANDROID || UNITY_IOS

    //#if !UNITY_EDITOR
    //Бонусы
    string _bonusText = "Ты можешь забрать свой ежедневный бонус!"; //Время этого уведомления определяется бонус менеджером
    string _bonusText2 = "Твой бонус ждет тебя, забери его!";
    long _bonusTime2 = 24 * 60 * 60;//240;//
    long _bonusTimeShort = 20 * 60;// 60;//
    //#endif
    //Зайди в игру!
    string _playerCallText = "Командир! Твои земли нуждаются в защите!";
    long _playerCallTime = 4 * 60 * 60;//180; //
    string _playerCallText2 = "Командир! Твои земли нуждаются в защите!";
    long _playerCallTime2 = 23 * 60 * 60;//300;//
    string _playerCallText3 = "Нет времени объяснять! Заходи и играй!";
    long _playerCallTime3 = 72 * 60 * 60;//500; //

    public static event Action<string, string> OnLaunchWithNotification;
    public static event Action OnBonusNotification;

    public static bool IOSPlayerChoosed = false;
    static GameObject _instance;
    void Awake()
    {
        if (_instance != null && _instance != gameObject)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            _instance = gameObject;
        }

        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        //Для тестов в редакторе, очищаем все значения если сохранять через CryptoPlayerPrefs
        //PlayerPrefs.DeleteAll();
        //Уберем регистрацию событий в редакторе
        //#if !UNITY_EDITOR 
        NotificationData data = LoadNotificationData();
        // NPBinding.NotificationService.ClearNotifications();
        if (data == null)
        {
            OnFirstGameEnter();
        }
        else
        {
            //Зашли в игру значит нужно перерегистрироваь пуши
            UnregisterPlayerCallNotificationsAndRemoveIDs(data);
            RegisterPlayerCallNotifications(data);
        }
#if UNITY_IOS
        data = LoadNotificationData();
        if (!data.RecieveNotificationsIOs)
        {
            return;
        }
#endif
        Debug.Log("Registration for notification");
        NPBinding.NotificationService.RegisterNotificationTypes(NotificationType.Alert | NotificationType.Badge | NotificationType.Sound);

        //#endif
    }
    /// <summary>
    /// Первый раз входя регистрируем все пуши и сохраням ID в файл
    /// </summary>
    void OnFirstGameEnter()
    {
#if UNITY_EDITOR
        NPBinding.NotificationService.ClearNotifications();// для теста убирает все нотификации из центра уведомлений 
#endif
        NPBinding.NotificationService.CancelAllLocalNotification(); // чтобы из-за удаления приложения не приходили уведомления старые
        NotificationData data = new NotificationData();
        SaveNotificationData(data);
        RegisterPlayerCallNotifications(data);
    }
    void RegisterPlayerCallNotifications(NotificationData data)
    {
#if UNITY_IOS
        if (!data.RecieveNotificationsIOs)
        {
            return;
        }
#endif
        CreateAndRegisterNotification(_playerCallTime, _playerCallText, data.PlayerCallIDs);
        CreateAndRegisterNotification(_playerCallTime2, _playerCallText2, data.PlayerCallIDs);
        CreateAndRegisterNotification(_playerCallTime3, _playerCallText3, data.PlayerCallIDs);
        SaveNotificationData(data);
    }
    /// <summary>
    /// Отменяем регистрацию PlayerCall уведомлений и удаляем их ID из data
    /// </summary>
    void UnregisterPlayerCallNotificationsAndRemoveIDs(NotificationData data)
    {
        for (int i = 0; i < data.PlayerCallIDs.Count; i++)
        {
            NPBinding.NotificationService.CancelLocalNotification(data.PlayerCallIDs[i]);
        }
        data.PlayerCallIDs.Clear();
        SaveNotificationData(data);
    }
    void UnregisterBonusNotificationsAndRemoveIDs(NotificationData data)
    {
        for (int i = 0; i < data.BonusIDs.Count; i++)
        {
            NPBinding.NotificationService.CancelLocalNotification(data.BonusIDs[i]);
        }
        data.BonusIDs.Clear();
        SaveNotificationData(data);
    }
    void NotificationService_DidReceiveLocalNotificationEvent(CrossPlatformNotification _notification)
    {
        NotificationData data = LoadNotificationData();
        //Это значит получили уведомление о бонусе во время игры в приложения, при переходе на экран карты или меню нужно показать бонус 
        if (IsBonusNotification(_notification, data))
        {
            if (OnBonusNotification != null)
            {
                OnBonusNotification();
            }
        }
    }
    //запустили приложение по клику на нотификацию
    void NotificationService_DidLaunchWithLocalNotificationEvent(CrossPlatformNotification _notification)
    {
        NotificationData data = LoadNotificationData();
        if (IsBonusNotification(_notification, data))
        {
            if (OnLaunchWithNotification != null)
                OnLaunchWithNotification(_notification.AlertBody, "Yes");
        }
        else
        {
            if (OnLaunchWithNotification != null)
                OnLaunchWithNotification(_notification.AlertBody, "No");
        }
    }

    private bool IsBonusNotification(CrossPlatformNotification _notification, NotificationData data)
    {
        return data.BonusIDs.Contains(_notification.GetNotificationID());
    }
    void NotificationPanel_OnBonusTaken()
    {
        NotificationData data = LoadNotificationData();
        UnregisterBonusNotificationsAndRemoveIDs(data);
    }
    void BonusManager_OnBonusSet(long obj)
    {
        //#if !UNITY_EDITOR
        NotificationData data = LoadNotificationData();
#if UNITY_IOS
        if (!data.RecieveNotificationsIOs)
        {
            return;
        }
#endif
        UnregisterBonusNotificationsAndRemoveIDs(data);
        CreateAndRegisterNotification(obj, _bonusText, data.BonusIDs);
        CreateAndRegisterNotification(_bonusTime2, _bonusText2, data.BonusIDs);
        SaveNotificationData(data);
        //#endif
    }
    void BonusManager_OnGetBonus()
    {
        //#if !UNITY_EDITOR
        NotificationData data = LoadNotificationData();
#if UNITY_IOS
        if (!data.RecieveNotificationsIOs)
        {
            return;
        }
#endif
        UnregisterBonusNotificationsAndRemoveIDs(data);
        CreateAndRegisterNotification(_bonusTimeShort, _bonusText, data.BonusIDs);
        CreateAndRegisterNotification(_bonusTime2, _bonusText2, data.BonusIDs);
        SaveNotificationData(data);
        //#endif
    }
    void NotificationPermissionPopup_OnPlayerGavePermission(bool obj)
    {
        NotificationData data = LoadNotificationData();
        data.RecieveNotificationsIOs = obj;
        data.IOSPlayerChoosed = true;
        IOSPlayerChoosed = true;
        if (obj)
        {
            //#if !UNITY_EDITOR
            NPBinding.NotificationService.RegisterNotificationTypes(NotificationType.Alert | NotificationType.Badge | NotificationType.Sound);
            RegisterPlayerCallNotifications(data);
            CreateAndRegisterNotification((long)BonusManager.GetTimeToNextBonus().TotalSeconds, _bonusText, data.BonusIDs);
            CreateAndRegisterNotification(_bonusTime2, _bonusText2, data.BonusIDs);
            //#endif
        }
        SaveNotificationData(data);
    }
    void OnEnable()
    {
        NotificationService.DidReceiveLocalNotificationEvent += NotificationService_DidReceiveLocalNotificationEvent;
        NotificationService.DidLaunchWithLocalNotificationEvent += NotificationService_DidLaunchWithLocalNotificationEvent;
        NotificationService.DidFinishRegisterForRemoteNotificationEvent += NotificationService_DidFinishRegisterForRemoteNotificationEvent;
        NotificationPanel.OnBonusTaken += NotificationPanel_OnBonusTaken;
        BonusManager.OnBonusSet += BonusManager_OnBonusSet;
        BonusManager.OnGetBonus += BonusManager_OnGetBonus;
        NotificationPermissionPopup.OnPlayerGavePermission += NotificationPermissionPopup_OnPlayerGavePermission;
    }

    void NotificationService_DidFinishRegisterForRemoteNotificationEvent(string _deviceToken, string _error)
    {
        if (String.IsNullOrEmpty(_error))
        {
            string serverUrl = "";
#if UNITY_ANDROID 
            serverUrl = "http://td-vk.kim-games.ru/fast/android.php";
#endif
#if UNITY_IOS
            serverUrl = "http://td-vk.kim-games.ru/fast/ios.php";
#endif
            StartCoroutine(SendToken(serverUrl, _deviceToken));
        }
    }
    IEnumerator SendToken(string url, string token)
    {
        var form = new WWWForm();
        form.AddField("token", token);
        WWW www = new WWW(url, form);
        yield return www;
    }

    void OnDisable()
    {
        NotificationService.DidReceiveLocalNotificationEvent -= NotificationService_DidReceiveLocalNotificationEvent;
        NotificationService.DidLaunchWithLocalNotificationEvent -= NotificationService_DidLaunchWithLocalNotificationEvent;
        NotificationService.DidFinishRegisterForRemoteNotificationEvent -= NotificationService_DidFinishRegisterForRemoteNotificationEvent;
        NotificationPanel.OnBonusTaken -= NotificationPanel_OnBonusTaken;
        BonusManager.OnBonusSet -= BonusManager_OnBonusSet;
        BonusManager.OnGetBonus -= BonusManager_OnGetBonus;
        NotificationPermissionPopup.OnPlayerGavePermission -= NotificationPermissionPopup_OnPlayerGavePermission;
    }

    /// <summary>
    /// Создаем и регистрируюм уведомления
    /// </summary>
    /// <returns>Возвращаемя ID уведомления</returns>
    void CreateAndRegisterNotification(long _fireAfterSec, string text, List<string> ids)
    {
        CrossPlatformNotification _notification = CreateNotification(_fireAfterSec, text);
        NPBinding.NotificationService.ScheduleLocalNotification(_notification);
        ids.Add(_notification.GetNotificationID());
    }
    CrossPlatformNotification CreateNotification(long _fireAfterSec, string text)
    {
        CrossPlatformNotification.iOSSpecificProperties _iosProperties = new CrossPlatformNotification.iOSSpecificProperties();
        //_iosProperties.HasAction = true;
        // _iosProperties.AlertAction = "alert action";
        CrossPlatformNotification.AndroidSpecificProperties _androidProperties = new CrossPlatformNotification.AndroidSpecificProperties();
        _androidProperties.ContentTitle = "Modern Tower Defence";
        _androidProperties.TickerText = text;
        _androidProperties.LargeIcon = "Icon128x128.png"; //Keep the files in Assets/StreamingAssets/VoxelBusters/NativePlugins/Android folder.
        CrossPlatformNotification _notification = new CrossPlatformNotification();
        _notification.AlertBody = text; //On Android, this is considered as ContentText
        _notification.RepeatInterval = eNotificationRepeatInterval.NONE;
        //Ставим нотификацию на текущее телефонное время
        _notification.FireDate = DateTime.Now.AddSeconds(_fireAfterSec);
        _notification.iOSProperties = _iosProperties;
        _notification.AndroidProperties = _androidProperties;
        _notification.SoundName = "Notification.mp3"; //Keep the files in Assets/StreamingAssets/VoxelBusters/NativePlugins/Android folder.
        return _notification;
    }

    void SaveNotificationData(NotificationData data)
    {
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        CryptoPlayerPrefs.Save("notifications", data.GetType(), data);
#endif
#if UNITY_EDITOR
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/notifications.nd", FileMode.OpenOrCreate);
        Debug.Log(StringSerializationAPI.Serialize(typeof(NotificationData), data));
        binaryFormatter.Serialize(file, data);
        file.Close();
#endif
    }

    NotificationData LoadNotificationData()
    {
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        if (CryptoPlayerPrefs.HasKey("notifications"))
        {
            var  data = (NotificationData)CryptoPlayerPrefs.Load("notifications", typeof(NotificationData));
            IOSPlayerChoosed = data.IOSPlayerChoosed;
            return data;
        }
        else
        {
            return null;
        }
#endif
#if UNITY_EDITOR
        if (File.Exists(Application.persistentDataPath + "/notifications.nd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/notifications.nd", FileMode.Open);
            NotificationData data = (NotificationData)bf.Deserialize(file);
            IOSPlayerChoosed = data.IOSPlayerChoosed;
            //Debug.Log(StringSerializationAPI.Serialize(typeof(NotificationData), data));
            file.Close();
            return data;
        }
        return null;
#endif
    }
#endif

#if UNITY_WEBGL || UNITY_WEBPLAYER
    void Start()
    {
        Destroy(gameObject);
    }
#endif
}
