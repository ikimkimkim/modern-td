﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
//using UnityEngine.Cloud.Analytics;
using TDTK;
//using VoxelBusters.Utility;
//using VoxelBusters.NativePlugins;
public class StatisticManager : MonoBehaviour
{
#if UNITY_WEBGL || UNITY_WEBPLAYER
    void Start()
    {
        Destroy(gameObject);
    }
#endif
#if UNITY_ANDROID || UNITY_IOS
    static GameObject _instance;
    void Awake()
    {

        if (_instance != null && _instance != gameObject)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            _instance = gameObject;
        }

        DontDestroyOnLoad(gameObject);
    }

    StatisticData _data;
    void Start()
    {
#if UNITY_ANDROID
        const string projectId = "a7bb5e67-7d47-4b4d-b1bc-fc3873b40fe0";
        UnityAnalytics.StartSDK(projectId);
        Init();
#endif
#if UNITY_IOS
        const string projectId = "66a960d1-890a-4e6a-86a4-9d2b4aad4b1e";
        UnityAnalytics.StartSDK(projectId);
        Init();
#endif

    }
    void Init()
    {
        _data = LoadData();
        if (_data == null)
        {
            _data = new StatisticData();
            SaveData(_data);
            OnRegistration();
        }
    }
    void OnRegistration()
    {
        UnityAnalytics.CustomEvent("Registration", new Dictionary<string, object> { { "Language", Application.systemLanguage.ToString() } });
    }
    void OnEnable()
    {
        GameControl.onGameStarted += OnLevelStart;
        GameControl.onGameOverE += OnLevelEnd;
        AbilityManager.onAbilityActivatedE += OnAbilityActivated;
        NotificationPanel.OnBonusTaken += OnBonusTaken;
        StoreManager.OnStoreOpen += OnStoreOpen;
        BuyPackPanel.OnBuyPackPanelOpen += OnInfoAboutNextPackOpend;
        BillingManager.OnCrystalBuy += BillingManager_OnCrystalBuy;
        BillingManager.OnLocationBuy += BillingManager_OnLocationBuy;
        RewardedAdsVideoManager.OnRewardRecieved += RewardedAdsVideoManager_OnRewardRecieved;
        NotificationService.DidFinishRegisterForRemoteNotificationEvent += NotificationService_DidFinishRegisterForRemoteNotificationEvent;
    }


    void OnDisable()
    {
        GameControl.onGameStarted -= OnLevelStart;
        GameControl.onGameOverE -= OnLevelEnd;
        AbilityManager.onAbilityActivatedE -= OnAbilityActivated;
        NotificationPanel.OnBonusTaken -= OnBonusTaken;
        StoreManager.OnStoreOpen -= OnStoreOpen;
        BuyPackPanel.OnBuyPackPanelOpen -= OnInfoAboutNextPackOpend;
        BillingManager.OnCrystalBuy -= BillingManager_OnCrystalBuy;
        BillingManager.OnLocationBuy -= BillingManager_OnLocationBuy;
        RewardedAdsVideoManager.OnRewardRecieved -= RewardedAdsVideoManager_OnRewardRecieved;
        NotificationService.DidFinishRegisterForRemoteNotificationEvent -= NotificationService_DidFinishRegisterForRemoteNotificationEvent;
    }

    void NotificationService_DidFinishRegisterForRemoteNotificationEvent(string _deviceToken, string _error)
    {
        if (String.IsNullOrEmpty(_error))
        {
            if (!_data.RegistrationForRemoteNotifications)
            {
                UnityAnalytics.CustomEvent("NotificationRegistrationOK", new Dictionary<string, object> { { "Language", Application.systemLanguage.ToString() } });
                _data.RegistrationForRemoteNotifications = true;
                SaveData(_data);
            }
        }
    }
    //События
    void OnLevelStart(int levelId)
    {
        if (levelId == 1 && !_data.Level1Start)
        {
            UnityAnalytics.CustomEvent("LevelStart_1", new Dictionary<string, object> { { "LevelID", "LevelID_" + levelId.ToString() } });
            _data.Level1Start = true;
            SaveData(_data);
        }
        else if (levelId == 2 && !_data.Level2Start)
        {
            UnityAnalytics.CustomEvent("LevelStart_2", new Dictionary<string, object> { { "LevelID", "LevelID_" + levelId.ToString() } });
            _data.Level2Start = true;
            SaveData(_data);
        }
        else if (levelId == 7 && !_data.Level7Start)
        {
            UnityAnalytics.CustomEvent("LevelStart_7", new Dictionary<string, object> { { "LevelID", "LevelID_" + levelId.ToString() } });
            _data.Level7Start = true;
            SaveData(_data);
        }
        else if (levelId == 12 && !_data.Level12Start)
        {
            UnityAnalytics.CustomEvent("LevelStart_12", new Dictionary<string, object> { { "LevelID", "LevelID_" + levelId.ToString() } });
            _data.Level12Start = true;
            SaveData(_data);
        }
        else
        {
            UnityAnalytics.CustomEvent("LevelStart", new Dictionary<string, object> { { "LevelID", "LevelID_" + levelId.ToString() } });
        }
    }
    void OnLevelEnd(int stars)
    {
        if (GameControl.GetLevelID() == 1 && _data.Level1End)
        {
            UnityAnalytics.CustomEvent("LevelEnd_1", new Dictionary<string, object> { { "LevelID", "LevelID_" + GameControl.GetLevelID().ToString() }, { "Stars", "Stars_" + stars.ToString() } });
            _data.Level1End = true;
            SaveData(_data);
        }
        else
        {
            UnityAnalytics.CustomEvent("LevelEnd", new Dictionary<string, object> { { "LevelID", "LevelID_" + GameControl.GetLevelID().ToString() }, { "Stars", "Stars_" + stars.ToString() } });
        }
    }
    void OnAbilityActivated(Ability ability)
    {
        UnityAnalytics.CustomEvent("AbilityUse", new Dictionary<string, object> { { "Ability", ability.name.ToString() }, { "LevelID", "LevelID_" + GameControl.GetLevelID().ToString() }, { "Wave", "Wave_" + SpawnManager.GetCurrentWaveID().ToString() } });
    }
    void OnBonusTaken()
    {
        UnityAnalytics.CustomEvent("BonusTaken", new Dictionary<string, object> { { "Crystals", PlayerManager._instance.GetCrystals() } });
    }
    void OnStoreOpen(bool fromLevelSelect)
    {
        UnityAnalytics.CustomEvent("StoreOpen", new Dictionary<string, object> { { "Level", (fromLevelSelect) ? "Map" : "Game" } });
    }
    void OnInfoAboutNextPackOpend(int id)
    {
        UnityAnalytics.CustomEvent("BuyLocationPanelOpen", new Dictionary<string, object> { { "LocationID", "LocationID_" + id.ToString() } });
    }

    void BillingManager_OnCrystalBuy(bool fromLevelSelect, int packID)
    {
        if (!_data.FirstCrystalBuy)
        {
            UnityAnalytics.CustomEvent("CrystalBuy_1st", new Dictionary<string, object> { { "PackID", "PackID_" + packID.ToString() } });
            _data.FirstCrystalBuy = true;
            SaveData(_data);
        }
        else
        {
            if (fromLevelSelect)
            {
                UnityAnalytics.CustomEvent("CrystalBuyMap", new Dictionary<string, object> { { "PackID", "PackID_" + packID.ToString() } });
            }
            else
            {
                UnityAnalytics.CustomEvent("CrystalBuyGame", new Dictionary<string, object> { { "PackID", "PackID_" + packID.ToString() } });
            }
        }
    }

    void BillingManager_OnLocationBuy(int id)
    {
        UnityAnalytics.CustomEvent("LocationBuy", new Dictionary<string, object> { { "LocationID", "LocationID_" + id.ToString() } });
    }

    void RewardedAdsVideoManager_OnRewardRecieved()
    {
        UnityAnalytics.CustomEvent("AdsRewardRecieved", new Dictionary<string, object> { { "Language", Application.systemLanguage.ToString() } });
    }


    void SaveData(StatisticData data)
    {
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        CryptoPlayerPrefs.Save("statistic", data.GetType(), data);
#endif
#if UNITY_EDITOR
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/statistic.td", FileMode.OpenOrCreate);
        binaryFormatter.Serialize(file, data);
        file.Close();
#endif
    }
    StatisticData LoadData()
    {
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        if (CryptoPlayerPrefs.HasKey("statistic"))
        {
            return (StatisticData)CryptoPlayerPrefs.Load("statistic", typeof(StatisticData));
        }
        else
        {
            return null;
        }
#endif
#if UNITY_EDITOR
        if (File.Exists(Application.persistentDataPath + "/statistic.td"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/statistic.td", FileMode.Open);
            StatisticData data = (StatisticData)bf.Deserialize(file);
            file.Close();
            return data;
        }
        return null;
#endif
    }
#endif
}
