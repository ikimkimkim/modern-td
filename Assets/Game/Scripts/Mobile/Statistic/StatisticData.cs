﻿using UnityEngine;
using System.Collections;
using System;
[System.Serializable]
public class StatisticData
{
    public bool Level1Start = false;
    public bool Level1End = false;
    public bool Level2Start = false;
    public bool Level7Start = false;
    public bool Level12Start = false;
    public bool FirstCrystalBuy = false;
    public bool RegistrationForRemoteNotifications = false;
}
