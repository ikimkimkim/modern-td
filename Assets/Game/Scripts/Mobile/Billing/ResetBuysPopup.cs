﻿using UnityEngine;
using System.Collections;

public class ResetBuysPopup : MonoBehaviour {

    public GameObject Popup;
    public void Show()
    {
        Popup.SetActive(true);
    }
    public void Hide()
    {
        Popup.SetActive(false);
    }
}
