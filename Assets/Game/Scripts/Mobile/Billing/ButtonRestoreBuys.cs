﻿using UnityEngine;
using System.Collections;

public class ButtonRestoreBuys : MonoBehaviour
{
#if UNITY_ANDROID || UNITY_IOS
    BillingManager _billingManager;
    void Start()
    {
        _billingManager = GameObject.FindGameObjectWithTag("MobileBillingManager").GetComponent<BillingManager>();
    }
    public void RestoreBuys()
    {
        _billingManager.RestoreBuys();
    }
#endif
#if UNITY_WEBGL || UNITY_WEBPLAYER
    void Start()
    {
        Destroy(gameObject);
    }
#endif
}
