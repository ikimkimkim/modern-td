﻿using UnityEngine;
using System.Collections;

public class BillingproductsInfo
{
    public const string CrystalPack1 = "ru.kimgames.td.pack1";
    public const string CrystalPack2 = "ru.kimgames.td.pack2";
    public const string CrystalPack3 = "ru.kimgames.td.pack3";
    public const string Levels2 = "ru.kimgames.td.levels2";

#if UNITY_ANDROID
    public const int CrystalCount1 = 500;
    public const int CrystalCount2 = 1000;
    public const int CrystalCount3 = 3000;
#endif
#if UNITY_IOS
     public const int CrystalCount1 = 500;
    public const int CrystalCount2 = 1500;
    public const int CrystalCount3 = 5000;
#endif
}
