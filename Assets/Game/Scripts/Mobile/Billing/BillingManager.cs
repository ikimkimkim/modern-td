﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
//using UnityEngine.Cloud.Analytics;
//using VoxelBusters.Utility;
//using VoxelBusters.NativePlugins;
//"You can configure this feature in NPSettings->Billing Settings.",
//"In Billing Settings you can also store all the Product information and access it at runtime using getter NPSettings.Billing.Products.",
//"Billing workflow is pretty much simple. " +
//"\n1. Request for product infomation." +
//"\n2. If your product list includes non consummable products, then restore old purchases." +
//"\n3. Initiate purchase using BuyProduct API. Also use IsPurchased API to check if product is already purchase or not."

public class BillingManager : MonoBehaviour
{
#if UNITY_WEBGL || UNITY_WEBPLAYER
    void Start()
    {
        Destroy(gameObject);
    }
#endif

#if UNITY_ANDROID || UNITY_IOS
    BillingProduct[] _products;
    static BillingManager _instance;
    public static event Action<bool, int> OnCrystalBuy;
    public static event Action<int> OnLocationBuy;
    void Awake()
    {

        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (_instance != this)
            {
                Destroy(gameObject);
                return;
            }
        }
    }
    void Start()
    {
        _products = NPSettings.Billing.Products;
        RequestBillingProducts(_products);
    }
    void OnEnable()
    {
        Billing.DidFinishProductsRequestEvent += Billing_DidFinishProductsRequestEvent;
        Billing.DidReceiveTransactionInfoEvent += Billing_DidReceiveTransactionInfoEvent;
    }
    void OnDisable()
    {
        Billing.DidFinishProductsRequestEvent -= Billing_DidFinishProductsRequestEvent;
        Billing.DidReceiveTransactionInfoEvent -= Billing_DidReceiveTransactionInfoEvent;
    }
    void Billing_DidFinishProductsRequestEvent(BillingProduct[] _regProductsList, string _error)
    {
        if (_regProductsList != null)
        {
            _products = _regProductsList;        // Без этого не работает отображение цены в магазине
        }

        //BillingTestText.text = (string.Format("Billing products request finished. Error = {0}.", _error.GetPrintableString()));

        //if (_regProductsList != null)
        //{
        //    BillingTestText.text += (string.Format("Totally {0} billing products information were received.", _regProductsList.Length));
        //    foreach (BillingProduct _eachProduct in _regProductsList)
        //        BillingTestText.text += (_eachProduct.ToString());
        //}
    }
    public StoreItemData[] GetItems()
    {
        List<StoreItemData> data = new List<StoreItemData>();
        for (int i = 0; i < _products.Length; i++)
        {
            //_products[i].Price.ToString() + " " + _products[i].CurrencySymbol
            // _products[i].LocalizedPrice
            data.Add(new StoreItemData(_products[i].ProductIdentifier, _products[i].Price.ToString() + " " + _products[i].CurrencyCode));
        }
        return data.ToArray();
    }
    void Billing_DidReceiveTransactionInfoEvent(BillingTransaction[] finishedTransactions, string error)
    {
        if (finishedTransactions != null)
        {
            foreach (BillingTransaction eachTransaction in finishedTransactions)
            {
                //Debug.Log("Product Identifier = " + _eachTransaction.ProductIdentifier);
                //Debug.Log("Transaction State = " + _eachTransaction.TransactionState);
                //Debug.Log("Verification State = " + _eachTransaction.VerificationState);
                //Debug.Log("Transaction Date[UTC] = " + _eachTransaction.TransactionDateUTC);
                //Debug.Log("Transaction Date[Local] = " + _eachTransaction.TransactionDateLocal);
                //Debug.Log("Transaction Identifier = " + _eachTransaction.TransactionIdentifier);
                //Debug.Log("Transaction Receipt = " + _eachTransaction.TransactionReceipt);
                //Debug.Log("Error = " + _eachTransaction.Error.GetPrintableString());

                if (eachTransaction.TransactionState == eBillingTransactionState.PURCHASED)
                {
                    switch (eachTransaction.ProductIdentifier)
                    {
                        case BillingproductsInfo.CrystalPack1:
                            {
                                GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>().AddCrystals(BillingproductsInfo.CrystalCount1, 0);
                                if (OnCrystalBuy != null)
                                {
                                    OnCrystalBuy(Application.loadedLevel == 1, 1);
                                }
                                break;
                            }
                        case BillingproductsInfo.CrystalPack2:
                            {
                                GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>().AddCrystals(BillingproductsInfo.CrystalCount2, 0);
                                if (OnCrystalBuy != null)
                                {
                                    OnCrystalBuy(Application.loadedLevel == 1, 2);
                                }
                                break;
                            }
                        case BillingproductsInfo.CrystalPack3:
                            {
                                GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>().AddCrystals(BillingproductsInfo.CrystalCount3, 0);
                                if (OnCrystalBuy != null)
                                {
                                    OnCrystalBuy(Application.loadedLevel == 1, 3);
                                }
                                break;
                            }
                        case BillingproductsInfo.Levels2:
                            {
                                GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>().BuyPack(2, true);
                                if (OnLocationBuy != null)
                                {
                                    OnLocationBuy(2);
                                }
                                break;
                            }
                    }
                }
                else if (eachTransaction.TransactionState == eBillingTransactionState.RESTORED)
                {
                    GameObject.FindGameObjectWithTag("ResetBuysPopup").GetComponent<ResetBuysPopup>().Show();
                    switch (eachTransaction.ProductIdentifier)
                    {
                        case BillingproductsInfo.Levels2:
                            {
                                var player = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
                                if (!player.isPackBought(2))
                                    player.BuyPack(2, true);
                                break;
                            }
                    }
                }
            }
        }
    }


    void RequestBillingProducts(BillingProduct[] _products)
    {
        NPBinding.Billing.RequestForBillingProducts(_products);
    }
    public void BuyProduct(string _productIdentifier)
    {
        NPBinding.Billing.BuyProduct(_productIdentifier);
    }
    public void RestoreBuys()
    {
        NPBinding.Billing.RestoreCompletedTransactions();
    }
#endif
}
