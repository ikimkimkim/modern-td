﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
public class InternetChecker
{

    static InternetChecker()
    {

        ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

    }

    public static bool isInternetAvailable()
    {

#if UNITY_WEBGL || UNITY_WEBPLAYER
        return true; //В веб версии всегда есть интернет
#endif
#if (UNITY_IOS || UNITY_ANDROID) && !UNIT_EDITOR

                string HtmlText = "";
#if UNITY_IOS
        HtmlText = GetHtmlFromUri("https://td-vk.kim-games.ru/fast/check.php");
#endif
#if UNITY_ANDROID
        HtmlText = GetHtmlFromUri("http://td-vk.kim-games.ru/fast/check.php"); //GetHtmlFromUri("https://google.com"); TODO я убрал https для теста андройда
#endif

        if (HtmlText == "")
        {
            Debug.Log("isInternetAvailable = false");
            return false;
        }
        else if (!HtmlText.Contains("modertd"))
        {
            //Redirecting since the beginning of googles html contains that 
            //phrase and it was not found
            Debug.Log("isInternetAvailable = false");
            return false;
        }
        else
        {
            //success
            //  Debug.Log("isInternetAvailable = true");
            return true;
        }
#endif
    }

    static string GetHtmlFromUri(string resource)
    {
        string html = string.Empty;
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
        try
        {
            using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
            {
                bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                if (isSuccess)
                {
                    //   Debug.Log("StatusCode " + ((int)resp.StatusCode).ToString() + " Status Desc " + resp.StatusDescription + " isSuccess " + isSuccess);
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                    {
                        //We are limiting the array to 80 so we don't have
                        //to parse the entire html document feel free to 
                        //adjust (probably stay under 300)
                        //char[] cs = new char[80];
                        //reader.Read(cs, 0, cs.Length);
                        //foreach (char ch in cs)
                        //{
                        //    html += ch;
                        //}
                        html = reader.ReadToEnd();
                    }
                }
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.ToString());
            return "";
        }
        //catch
        //{
        //    return "";
        //}


        return html;
    }
    static bool MyRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        bool isOk = true;
        // If there are errors in the certificate chain, look at each error to determine the cause.
        if (sslPolicyErrors != SslPolicyErrors.None)
        {
            for (int i = 0; i < chain.ChainStatus.Length; i++)
            {
                if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown)
                {
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                    }
                }
            }
        }
        return isOk;
    }
}
