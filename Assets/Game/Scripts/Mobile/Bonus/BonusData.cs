﻿using UnityEngine;
using System;
using System.Collections;
[System.Serializable]
public class BonusData
{
    public DateTime TimeToGiveBonus;
    public DateTime LocalTimeToGiveBonus; //нужно для того чтобы показывать если отсутствует бонус время для бонуса в NotificationPanel
    public bool CurrnetBonusTaken = false;
    public bool FirstBonusRecieved = false;
    public bool FirstBonusSet = false; // нужно для случая когда при первом запуске у нас не было интернета и мы не поставили бонус
}
