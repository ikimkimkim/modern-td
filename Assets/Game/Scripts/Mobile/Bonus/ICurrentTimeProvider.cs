﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


interface ICurrentTimeProvider
{
    void GetCurrentTime(Action<DateTime> callback);
}

