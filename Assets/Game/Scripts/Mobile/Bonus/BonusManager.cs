﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;

public class BonusManager : MonoBehaviour
{
    public static BonusManager instance;


    public static event Action OnGetBonus;
    public static event Action<long> OnBonusSet;
    public static event Action OnDidntReciveTimeFromServer;
    //ICurrentTimeProvider _timeProvider;
    public int _crystalsBouns = 100;
    bool _bonusPending = false;
    //long _firstBonusTime = 20 * 60;//60; //
    //long _regularBonusTime = 8 * 60 * 60;// 60 * 5; //

    public BonusData webBonus;

    public static TimeSpan GetTimeToNextBonus()
    {
        BonusData data = _instance.GetComponent<BonusManager>().LoadBonusData();
        if (data != null)
        {
            var difference = data.LocalTimeToGiveBonus - DateTime.Now;
            return difference;
        }
        else
        {
            return TimeSpan.MaxValue;
        }
    }
    public static bool isFirstBonusRecieved()
    {
        BonusData data = _instance.GetComponent<BonusManager>().LoadBonusData();
        if (data != null)
        {
            return data.FirstBonusRecieved;
        }
        else
        {
            return false;
        }
    }
    static GameObject _instance;
    void Awake()
    {
      
        if (_instance != null && _instance != gameObject)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            _instance = gameObject;
            instance = this;
        }

        DontDestroyOnLoad(gameObject);
    }
    void OnEnable()
    {
        NotificationPanel.OnBonusTaken += NotificationPanel_OnBonusTaken;
        NotificationPanel.OnShow += CheckBonus;
        NotificationPanel.OnTimerFinished += CheckBonus;
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        #if UNITY_IOS || UNITY_ANDROID
        NotificationManager.OnBonusNotification += CheckBonus;
#endif
    }


    void OnDisable()
    {
        NotificationPanel.OnBonusTaken -= NotificationPanel_OnBonusTaken;
        NotificationPanel.OnShow -= CheckBonus;
        NotificationPanel.OnTimerFinished -= CheckBonus;
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
#if UNITY_IOS || UNITY_ANDROID
        NotificationManager.OnBonusNotification -= CheckBonus;
#endif
    }
    void Start()
    {
        //_timeProvider = gameObject.AddComponent<ServerTimeProvider>();

        BonusData data = LoadBonusData();
        bool isFirstGameEnter = data == null || (data.FirstBonusSet == false && !data.FirstBonusRecieved); //TODO проверить правильность условия 
        if (isFirstGameEnter)
        {
            if (data == null)
            {
                Debug.Log("BonusManager Data = null");
                CreateBlankData();
            }

            if (InternetChecker.isInternetAvailable())
            {
                Debug.Log("BonusManager Internet available");
                OnFirstGameEnter();
            }
            else
            {
                StartCoroutine(CheckForInternet(60f, OnFirstGameEnter));
            }

        }
        else
        {
            if (InternetChecker.isInternetAvailable())
            {
                CheckBonusTime(data);
            }
            else
            {
                StartCoroutine(CheckForInternet(60f, () => { CheckBonusTime(data); }));
            }
        }
    }
    //Если data == null при первом запуске то записываем пустую data чтобы не было ошибок
    void CreateBlankData()
    {
        BonusData data = new BonusData();
        data.CurrnetBonusTaken = true; //нужно чтобы при следующей попытке проверить наличие бонуса, установился бонус
        SaveBonusData(data);
    }
    IEnumerator CheckForInternet(float period, Action onInternetAvailable)
    {
        while (true)
        {
            yield return new WaitForSeconds(period);
            if (InternetChecker.isInternetAvailable())
            {
                if (onInternetAvailable != null)
                {
                    onInternetAvailable();
                    yield break; // было yield return null;
                }
            }
        }
    }
    void OnFirstGameEnter()
    {
        #if UNITY_IOS || UNITY_ANDROID
        _timeProvider.GetCurrentTime((serverNow) =>
        {
            if (serverNow != DateTime.MinValue)
            {
                BonusData data = new BonusData();
                data.FirstBonusSet = true;
                SetBonusTime(data, serverNow, _firstBonusTime);
            }
            else
            {
                Debug.Log("OnFirstGameEnter server response error");
            }
        });
#endif
    }

    void SetBonusTime(BonusData data, DateTime serverNow, long secondsAdd)
    {
#if UNITY_IOS || UNITY_ANDROID
        data.TimeToGiveBonus = serverNow.AddSeconds(secondsAdd);
        data.LocalTimeToGiveBonus = DateTime.Now.AddSeconds(secondsAdd);
        data.CurrnetBonusTaken = false;
        SaveBonusData(data);
#endif
        if (OnBonusSet != null)
        {
            OnBonusSet(secondsAdd);
        }
    }


    private void SceneManager_sceneLoaded(Scene scene, LoadSceneMode mode)
    {
        OnLevelFinishedLoading(scene.buildIndex);
    }

    /// <summary>
    /// Если есть ожидающий получения бонус уведомляем при заходе в главное меню(0) или карту(1)
    /// </summary>
    void OnLevelFinishedLoading(int level)
    {
        if (level == 0 || level == 1)
        {
#if UNITY_IOS || UNITY_ANDROID
            if (_bonusPending)
            {
                if (OnGetBonus != null)
                {
                    OnGetBonus();
                }
            }
            else if (level == 1)
            {
                if (InternetChecker.isInternetAvailable())
                {
                    BonusData data = LoadBonusData();
                    CheckBonusTime(data);
                }
            }
        }
        if (level == 1)
        {
            

            BonusData data = LoadBonusData();
            if (!data.FirstBonusRecieved)
            {
                var player = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
                if (player.GetLevelsCount() == 2)
                {
                    if (OnGetBonus != null)
                    {
                      //  Debug.Log("Bonus for level 2!");
                        OnGetBonus();
                    }
                }
            }
#endif
#if UNITY_WEBGL || UNITY_WEBPLAYER
            if (_bonusPending)
            {
                if (OnGetBonus != null)
                {
                    OnGetBonus();
                }
            }
#endif
        }
    }
    void CheckBonus()
    {
        if (InternetChecker.isInternetAvailable())
        {
            BonusData data = LoadBonusData();
            CheckBonusTime(data);
        }
        else
        {
            StartCoroutine(CheckForInternet(60f, () => { BonusData data = LoadBonusData(); CheckBonusTime(data); }));
        }
    }

    void NotificationPanel_OnBonusTaken()
    {
        Debug.Log("BM->BonusTaken");

        _bonusPending = false;


#if UNITY_ANDROID || UNITY_IOS
       
        BonusData data = LoadBonusData();
        data.CurrnetBonusTaken = true;
        data.FirstBonusRecieved = true;
        SaveBonusData(data);
        GivePlayerCrystals(0);
        if (InternetChecker.isInternetAvailable())
        {
            SetRegularBonus();
        }
        else
        {
            StartCoroutine(CheckForInternet(60f, SetRegularBonus));
        }
#endif
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
        GivePlayerCrystals(-1);

#endif

        }
    void SetRegularBonus()
    {
        #if UNITY_IOS || UNITY_ANDROID
        _timeProvider.GetCurrentTime((serverNow) =>
        {
            if (serverNow != DateTime.MinValue)
            {
                BonusData data = LoadBonusData();
                SetBonusTime(data, serverNow, _regularBonusTime);
            }
            else
            {
                Debug.Log("SetRegularBonus server response error");
            }
        });
#endif
    }


    void CheckBonusTime(BonusData data)
    {
        Debug.Log("BM->CheckBonusTime: "+data);



#if UNITY_IOS || UNITY_ANDROID
        if (data.CurrnetBonusTaken)
        {
            SetRegularBonus();
        }
        else
        {
            _timeProvider.GetCurrentTime((serverNow) =>
            {
                if (data.TimeToGiveBonus <= serverNow && serverNow != DateTime.MinValue)
                {
                    if (OnGetBonus != null)
                    {
                        OnGetBonus();
                    }
                    else
                    {
                        _bonusPending = true;
                    }
                }
                else if (serverNow == DateTime.MinValue)
                {
                    if (OnDidntReciveTimeFromServer != null)
                    {
                        OnDidntReciveTimeFromServer();
                    }
                    Debug.Log("CheckBonusTime server response error");
                }
            });
        }
#endif
#if UNITY_WEBGL || UNITY_WEBPLAYER
        if (PlayerManager._instance._playerData.daily_bonus == 1)
        {
            _bonusPending = true;
            //SetRegularBonus();
        }
        else
        {
            
            if (OnBonusSet != null)
            {
                OnBonusSet(0);
            }
        }

#endif
    }


    void GivePlayerCrystals(int level)
    {
        var player = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
        player.AddCrystals(_crystalsBouns, level);
    }


    void SaveBonusData(BonusData data)
    {
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        CryptoPlayerPrefs.Save("bonus", data.GetType(), data);
#endif
#if UNITY_EDITOR
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/bonus.d", FileMode.OpenOrCreate);
        Debug.Log(StringSerializationAPI.Serialize(typeof(BonusData), data));
        binaryFormatter.Serialize(file, data);
        file.Close();
#endif
    }
    BonusData LoadBonusData()
    {
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        if (CryptoPlayerPrefs.HasKey("bonus"))
        {
            return (BonusData)CryptoPlayerPrefs.Load("bonus", typeof(BonusData));
        }
        else
        {
            return null;
        }
#endif
#if UNITY_EDITOR
        if (File.Exists(Application.persistentDataPath + "/bonus.d"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/bonus.d", FileMode.Open);
            BonusData data = (BonusData)bf.Deserialize(file);
            file.Close();
            return data;
        }
        return null;
#endif
#if (UNITY_WEBGL || UNITY_WEBPLAYER) && !UNITY_EDITOR
        return webBonus;
#endif
    }

#if UNITY_WEBGL || UNITY_WEBPLAYER

    /*public bool canGetBonus //daily_bonus;
    {
        get; private set;
    } 
    public double timeToBonus //daily_bonus_ttl;
    {
        get; private set;
    }

    public DateTime timeSetData //daily_bonus_ttl;
    {
        get; private set;
    }

    public void setData(int daily_bonus,int daily_bonus_ttl)
    {
        canGetBonus = daily_bonus == 1;
        timeToBonus = daily_bonus_ttl;
        _timeProvider.GetCurrentTime(setTimeFriteData);
    }

    void setTimeFriteData(DateTime time)
    {
        timeSetData = time;
    }
    */
    
#endif
}
