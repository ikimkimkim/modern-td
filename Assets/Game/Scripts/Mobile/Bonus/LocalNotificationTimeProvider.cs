﻿using UnityEngine;
using System.Collections;
using System;
public class LocalNotificationTimeProvider : MonoBehaviour, ICurrentTimeProvider
{
    public void GetCurrentTime(Action<DateTime> callback)
    {
        callback(DateTime.Now);
    }
}
