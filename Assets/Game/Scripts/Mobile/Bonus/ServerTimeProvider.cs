﻿using UnityEngine;
using System.Collections;
using System;
public class ServerTimeProvider : MonoBehaviour, ICurrentTimeProvider
{
    string _URL = "https://td-vk.kim-games.ru/fast/time.php"; 
 
    IEnumerator GetUnixTimeFromServer(Action<DateTime> callback)
    {
         
        WWW www = new WWW(_URL);
        yield return www;

        if (www.error == null)
        {
            callback(UnixTimeStampToDateTime(Convert.ToDouble(www.text)));
        }
        else
        {
            Debug.Log("GetUnixTimeFromServer " + www.error.ToString());
            callback(DateTime.MinValue);
        }
    }

    public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
    {
        DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        //  Debug.Log(dtDateTime.ToShortTimeString() + " " + dtDateTime.ToShortDateString());
        return dtDateTime;
    }

    public void GetCurrentTime(Action<DateTime> callback)
    {
        StartCoroutine(GetUnixTimeFromServer(callback));
    }
}
