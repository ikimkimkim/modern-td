﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MobileButtonScaler : MonoBehaviour
{
    public List<RectTransform> Buttons = new List<RectTransform>();
    public float ScaleFactor;

    void Start()
    {
#if UNITY_ANDROID || UNITY_IOS
        for (int i = 0; i < Buttons.Count; i++)
        {
            Buttons[i].SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Buttons[i].sizeDelta.x * ScaleFactor);
            Buttons[i].SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Buttons[i].sizeDelta.y * ScaleFactor);
        }
#endif
    }

}
