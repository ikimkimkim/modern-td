﻿using UnityEngine;
using System.Collections;
using TDTK;

public class UnityAdsButton : MonoBehaviour
{
    public void ShowAds()
    {
#if UNITY_ANDROID || UNITY_IOS
        if (UnityAdsManager.IsReady("defaultZone") && Application.loadedLevel != 3)
        {
            LevelSelectAdsShower.NeedToShowAds = true;
        }
        OnMainMenuButton();

#endif
    }
    void OnMainMenuButton()
    {
        TimeScaleManager.RefreshTimeScale();
        GameControl.LoadMainMenu();
    }
}
