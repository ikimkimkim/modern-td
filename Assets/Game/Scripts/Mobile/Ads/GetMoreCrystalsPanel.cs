﻿using UnityEngine;
using System.Collections;

public class GetMoreCrystalsPanel : MonoBehaviour
{
    public GameObject Background;
    public GameObject BackgroundWeb;
    public GameObject BlackScreen;
    //bool _fromMap = false;
    public void Show(bool fromMap)
    {
        //_fromMap = fromMap;
        BlackScreen.SetActive(true);


#if UNITY_WEBGL || UNITY_WEBPLAYER
        BackgroundWeb.SetActive(true);
#else
        Background.SetActive(true);
#endif
    }

    public void Hide()
    {
#if UNITY_WEBGL || UNITY_WEBPLAYER
        BackgroundWeb.SetActive(false);
#else
        Background.SetActive(false);
#endif
        BlackScreen.SetActive(false);
        //if (_fromMap)
        //{
#if UNITY_IOS
         var permissionPopup = GameObject.FindGameObjectWithTag("IOSNotificationPermission").GetComponent<NotificationPermissionPopup>();
         permissionPopup.Show();
#endif
        //}
    }



}
