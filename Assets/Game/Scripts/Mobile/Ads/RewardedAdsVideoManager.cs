﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Translation;
using System;
public class RewardedAdsVideoManager : MonoBehaviour
{
    public Image Notification;
    public Text NotificationText;
    int _freeCrystals = 50;
    public static event Action OnRewardRecieved;
    public void ShowAds()
    {
#if UNITY_WEBGL || UNITY_WEBPLAYER
          enabled = false;
#endif
        Translator trans = TranslationEngine.Instance.Trans;
        if (UnityAdsManager.IsReady("rewardedVideoZone"))
        {
            UnityAdsManager.ShowAd("rewardedVideoZone", AddFreeCrystals);
        }
        else
        {
            if (!Notification.gameObject.activeInHierarchy)
                StartCoroutine(ShowNotification(trans["RewardedAdsVideoManager.Notification.Error"]));
        }
    }
    void AddFreeCrystals()
    {
        Translator trans = TranslationEngine.Instance.Trans;
        var player = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
        player.AddCrystals(_freeCrystals, 0);
        if (OnRewardRecieved != null)
        {
            OnRewardRecieved();
        }
        if (!Notification.gameObject.activeInHierarchy)
            StartCoroutine(ShowNotification(trans["RewardedAdsVideoManager.Notification.OK"]));
    }
    IEnumerator ShowNotification(string text)
    {
        NotificationText.text = text;
        Notification.gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
        Notification.gameObject.SetActive(false);
        yield break;
    }
    void OnDisable()
    {
        Notification.gameObject.SetActive(false);
    }
}
