﻿using UnityEngine;
using System.Collections;

public class LevelSelectAdsShower : MonoBehaviour
{
    public static bool NeedToShowAds = false;

    void Start()
    {
#if UNITY_IOS || UNITY_ANDROID
        if (NeedToShowAds)
        {
            if (UnityAdsManager.IsReady("defaultZone"))
            {
                UnityAdsManager.ShowAd("defaultZone", null, null, null);
            }
            NeedToShowAds = false;
        }
#endif
    }
}
