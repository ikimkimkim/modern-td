﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TooltipMessageServer : MonoBehaviour {

    public static TooltipMessageServer instance { get; private set; }

    public GameObject TooltipInfo;
    public Text TooltipText;

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(this.gameObject);
            instance = this;

        }
        else
            Destroy(gameObject);
    }

    public static void Show(string Text, float RealSecond=3f)
    {
        if(instance!=null)
        {
            if (Text.Length > 1)
                instance._Show(Text, RealSecond);
            else
                Debug.LogWarning("TooltipMS: Short text!");
        }
        else
        {
            MyLog.LogError("TooltipMessageServer instance is null, message:"+Text, MyLog.Type.build);
        }
      
    }

    private void _Show(string Text, float RealSecond)
    {
        TooltipText.text = Text;
        StartCoroutine(TooltipShow(RealSecond));
    }

    IEnumerator TooltipShow(float second)
    {
        TooltipInfo.SetActive(true);
        yield return new WaitForSecondsRealtime(second);// StartCoroutine(TimeScaleManager.WaitForRealSeconds(second));
        TooltipInfo.SetActive(false);
    }

}
