﻿using UnityEngine;
using System.Collections;
using TDTK;

public class BleedingDotEffect : DotEffect
{
    public override string URLIcon { get { return "EffectIcon/Bleeding.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Кровотечение</color>"; } }

    protected override string HtmlColor { get { return "#bc121a"; } }

    public BleedingDotEffect(float CountTik, float Interval, float Dmg,int TypeDmg) : base(CountTik, Interval, Dmg, TypeDmg)
    {
        IDStack = IDEffect.BleedingDot;
    }


    public override void Dotted()
    {
        base.Dotted();
#if UNITY_EDITOR
        if (_TargetUnit != null && GameControl.GET_EDITOR_ShowDamage)
            new TextOverlay(_TargetUnit.getPosition, damage.ToString(), Color.red, false);
#endif
    }

    public override BaseEffect Clone()
    {
        return new BleedingDotEffect(countTik, interval, damage, typeDmg);
    }
}
