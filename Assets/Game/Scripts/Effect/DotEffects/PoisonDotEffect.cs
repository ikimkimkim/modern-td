﻿using UnityEngine;
using System.Collections;
using TDTK;

public class PoisonDotEffect : DotEffect
{
    public override string URLIcon { get { return "EffectIcon/Poison.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Отравление</color>"; } }

    protected override string HtmlColor { get { return "#76c149"; } }

    public PoisonDotEffect(float CountTik, float Interval, float Dmg, int TypeDmg) : base(CountTik, Interval, Dmg, TypeDmg)
    {
        IDStack = IDEffect.PoisonDot;
    }

    protected override void Apply()
    {
        base.Apply();
        _TargetUnit.ActionPoisenDotEffect(true);
    }

    protected override void Cancel()
    {
        if(_Stack.Count==1)
            _TargetUnit.ActionPoisenDotEffect(false);
    }

    public override void Dotted()
    {
        base.Dotted();
#if UNITY_EDITOR
        if (_TargetUnit != null && GameControl.GET_EDITOR_ShowDamage)
            new TextOverlay(_TargetUnit.getPosition, damage.ToString(), Color.green, false);
#endif
    }

    public override BaseEffect Clone()
    {
        return new PoisonDotEffect(countTik, interval, damage, typeDmg);
    }

}
