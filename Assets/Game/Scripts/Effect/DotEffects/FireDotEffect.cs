﻿using UnityEngine;
using System.Collections;
using TDTK;

public class FireDotEffect : DotEffect
{
    public override string URLIcon { get { return "EffectIcon/Fire.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Горение</color>"; } }

    protected override string HtmlColor { get { return "#ef4340"; } }

    public FireDotEffect(float CountTik, float Interval, float Dmg, int TypeDmg) : base(CountTik, Interval, Dmg, TypeDmg)
    {
        IDStack = IDEffect.FireDot;
    }

    protected override void Apply()
    {
        base.Apply();
        _TargetUnit.ActionFireDotEffect(true);
    }

    protected override void Cancel()
    {
        if (_Stack.Count == 1)
            _TargetUnit.ActionFireDotEffect(false);
    }

    public override void Dotted()
    {
        base.Dotted();
#if UNITY_EDITOR
        if (_TargetUnit != null && GameControl.GET_EDITOR_ShowDamage)
            new TextOverlay(_TargetUnit.getPosition, damage.ToString(), Color.red, false);
#endif
    }

    public override BaseEffect Clone()
    {
        return new FireDotEffect(countTik, interval, damage,typeDmg);
    }
}
