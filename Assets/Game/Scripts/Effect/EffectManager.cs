﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class EffectManager  {

    private Unit _unit;

    private bool bClearing;
    public Dictionary<int, IStackEffect> Stacks;

    private Dictionary<int, int> CounterEffect;

    public delegate void EffectHandler(Unit unit, IDEffect typeEffect);
    public static event EffectHandler addEffectE;
    public static event EffectHandler removeEffectE;

    public delegate void MyEffectHandler(BaseEffect effect);
    public event MyEffectHandler startAddMyEffectE;
    public event MyEffectHandler addMyEffectE;
    public event EffectHandler removeMyEffectE;

    //public List<FireDotEffect> listFireDot;
    //public bool isFire { get { return listFireDot.Count > 0; } }

    public bool isActiveEffect()
    {
        if (Stacks == null)
            return false;
        return Stacks.Count>0;
    }

    public bool isActiveEffect(IDEffect typeEffect)
    {
        if (Stacks == null)
            return false;
        return Stacks.ContainsKey((int)typeEffect);
    }
    public bool isActiveEffect(params IDEffect[] typeEffect)
    {
        if (Stacks == null)
            return false;
        for (int i = 0; i < typeEffect.Length; i++)
        {
            if(Stacks.ContainsKey((int)typeEffect[i]))
            {
                return true;
            }           
        }
        return false;
    }

    public int getCounterEffectForAllTime(IDEffect typeEffect)
    {
        if (CounterEffect.ContainsKey((int)typeEffect))
            return CounterEffect[(int)typeEffect];
        else
            return 0;
    }

    public int getCounterEffect(IDEffect typeEffect)
    {
        if (Stacks.ContainsKey((int)typeEffect))
            return Stacks[(int)typeEffect].Count;
        else
            return 0;
    }

    public EffectManager(Unit unit)
    {
        _unit = unit;

        Stacks = new Dictionary<int, IStackEffect>();

        CounterEffect = new Dictionary<int, int>();

        //listFireDot = new List<FireDotEffect>();
    }

    

    public void AddEffect(Unit rootUnit, BaseEffect effect)
    {
        if (_unit.dead)
            return;

        bClearing = false;
        if (startAddMyEffectE != null)
            startAddMyEffectE(effect);
        if (Stacks.ContainsKey((int)effect.IDStack) == false)
        {
            Stacks.Add((int)effect.IDStack, effect.GetStackEffect());
            Stacks[(int)effect.IDStack].Add(rootUnit, _unit, effect);
            if (addEffectE != null)
                addEffectE(_unit, effect.IDStack);
        }
        else
            Stacks[(int)effect.IDStack].Add(rootUnit, _unit, effect);

        AddEffectInCounter(effect);

        if (addMyEffectE != null)
            addMyEffectE(effect);
    }
    
    private void AddEffectInCounter(BaseEffect effect)
    {
        if (CounterEffect.ContainsKey((int)effect.IDStack))
        {
            CounterEffect[(int)effect.IDStack]++;
        }
        else
            CounterEffect.Add((int)effect.IDStack, 1);
    }

    public void Update()
    {
        foreach (var k in Stacks)
        {
            k.Value.Update();
            if (bClearing || Remove(k.Key))
                return;
        }
    }

    private bool Remove(int type)
    {
        if (Stacks[type].Count <= 0)
        {
            Stacks.Remove(type);
            if (removeEffectE != null)
                removeEffectE(_unit, (IDEffect)type);
            if (removeMyEffectE != null)
                removeMyEffectE(_unit, (IDEffect)type);
            return true;
        }
        return false;
    }

    public void ClearData()
    {
        //MyLog.Log(_unit.name + " EM: clear data");
        bClearing = true;
        foreach (var stack in Stacks)
        {
            if (removeEffectE != null)
                removeEffectE(_unit, (IDEffect)stack.Key);
            if (removeMyEffectE != null)
                removeMyEffectE(_unit, (IDEffect)stack.Key);
            stack.Value.ClearData();
        }
        Stacks.Clear();
        CounterEffect.Clear();
    }
}
