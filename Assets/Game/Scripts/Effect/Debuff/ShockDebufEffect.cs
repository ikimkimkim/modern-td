﻿using UnityEngine;
using System.Collections;
using TDTK;

public class ShockDebufEffect : PowerEffect
{
    public override bool isSplashUnique { get { return true; } }

    public override string URLIcon { get { return "EffectIcon/Shock.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Увеличен</color> входящий урон всех типов"; } }

    protected override string HtmlColor { get { return "#dbb6e3"; } } //e1cee3
    public override bool isBuff { get { return false; } }

    public ShockDebufEffect(float power, float time) : base(time)
    {
        IDStack = IDEffect.Shock;
        Power = (1 + power);
    }


    protected override void Active()
    {
        _TargetUnit.myInAttakInstanceFirstE += AttaInstanceIn;
    }
    private void AttaInstanceIn(AttackInstance instance)
    {
        instance.damage *= Power;
    }

    protected override void Deactive()
    {
        _TargetUnit.myInAttakInstanceFirstE -= AttaInstanceIn;
    }


    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }

    public override BaseEffect Clone()
    {
        return new ShockDebufEffect(Power, TimeLife);
    }
}
