﻿using UnityEngine;
using System.Collections;
using TDTK;

public class FrostbiteDebuffEffect : PowerEffect
{
    public override bool isSplashUnique { get { return true; } }

    public float Slow;

    public override string URLIcon { get { return "EffectIcon/Frostbite.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Увеличен</color> получаемый урон"; } }

    protected override string HtmlColor { get { return "#81a4d1"; } }
    public override bool isBuff { get { return false; } }

    public FrostbiteDebuffEffect(float power, float slow, float time) : base(time)
    {
        IDStack = IDEffect.Frostbite;
        Power = (1 + power);
        Slow = slow;
    }


    protected override void Active()
    {
        _TargetUnit.myInAttakInstanceFirstE += AttaInstanceIn;
        _TargetUnit.SetMoveMultiplier((int)IDStack, 1 - Slow * _TargetUnit.slowIncrement);
    }

    private void AttaInstanceIn(AttackInstance instance)
    {
        instance.damage *= Power;
    }

    protected override void Deactive()
    {
        if (_Stack.Count == 1)
        {
            _TargetUnit.RemoveMoveMultiplier((int)IDStack);
            _TargetUnit.HideSlowEffect();
        }
        _TargetUnit.myInAttakInstanceFirstE -= AttaInstanceIn;
    }


    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }

    public override BaseEffect Clone()
    {
        return new FrostbiteDebuffEffect(Power,Slow, TimeLife);
    }
}
