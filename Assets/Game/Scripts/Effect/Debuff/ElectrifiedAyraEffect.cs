﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TDTK;

public class ElectrifiedAyraEffect : BaseEffect
{
    public override bool isSplashUnique
    {
        get
        {
            return false;
        }
    }

    public override string URLIcon { get { return ""; } }

    public override string InfoText { get { return "Наэлектризованный"; } }

    public override bool isBuff { get { return false; } }

    protected override string HtmlColor { get { return "#33cccc"; } }

    private float _range, _time, _dmgP;
    private float timeStart,lastTimeDmg;
    private GameObject effect;

    public ElectrifiedAyraEffect(float range, float time, float dmgP)
    {
        IDStack = IDEffect.ElectrifiedAyraDebuff;
        _range = range;
        _time = time;
        _dmgP = dmgP;
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectTimeLimiteWC(_time);
        timeStart = Time.time;
        lastTimeDmg = 0;
    }

    protected override void Update()
    {
        if(timeStart-lastTimeDmg>1f)
        {
            foreach(Unit creep in SpawnManager.instance.ActiveCreeper.FindAll(i => i != _TargetUnit && Vector3.Distance(_TargetUnit.getPosition, i.getPosition) < _range))
            {
                creep.ApplyDamage(_RootUnit, _TargetUnit.GetFullHP() * _dmgP, (int)enumDamageType.Energetic,false);
            }
            lastTimeDmg = Time.time;
        }
    }

    protected override void Cancel()
    {

    }


    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }

    public override BaseEffect Clone()
    {
        return new ElectrifiedAyraEffect(_range, _time, _dmgP);
    }
}
