﻿using UnityEngine;
using System.Collections;

public class WeaknessDebufEffect : PowerEffect
{
    public override float Power { get { return _Slow + _TimeMultiplier; } set { } }

    public override bool isSplashUnique { get { return false; } }

    public float _Slow, _TimeMultiplier;

    public override string URLIcon { get { return "EffectIcon/Weakness.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Увеличено</color> время действия статусов\r\n<color=#ff0000>Уменьшена</color> скорость передвижения"; } }

    protected override string HtmlColor { get { return "#00b2b0"; } }
    public override bool isBuff { get { return false; } }

    public WeaknessDebufEffect(float slow, float timeMultiplier, float time) : base(time)
    {
        _Slow = slow;
        _TimeMultiplier = timeMultiplier;
        IDStack = IDEffect.WeaknessDebuff;
    }

    protected override void Active()
    {
        _TargetUnit.EffectsManager.addMyEffectE += AddNewEffect;
        _TargetUnit.SetMoveMultiplier((int)IDStack, 1 - _Slow * _TargetUnit.slowIncrement);
        //_TargetUnit.weaknessMultiplier = 1 - _Slow;
        UpTimeCurentEffect();
    }

    public void UpTimeCurentEffect()
    {
        foreach (var stack in _TargetUnit.EffectsManager.Stacks)
        {
            if (stack.Key == (int)IDEffect.SlowControl || stack.Key == (int)IDEffect.FireDot ||
               stack.Key == (int)IDEffect.PoisonDot)
            {
                foreach (BaseEffect effect in stack.Value.getAllEffect())
                {
                    if (effect.workСonditionEffect is EffectTimeLimiteWC)
                    {
                        (effect.workСonditionEffect as EffectTimeLimiteWC).TimeLife *= _TimeMultiplier;
                    }
                }
            }
        }
    }

    public void AddNewEffect(BaseEffect effect)
    {
        if(effect.IDStack == IDEffect.SlowControl || effect.IDStack == IDEffect.FireDot ||
            effect.IDStack == IDEffect.PoisonDot)
        {
            if(effect.workСonditionEffect is EffectTimeLimiteWC)
            {               
                (effect.workСonditionEffect as EffectTimeLimiteWC).TimeLife *= _TimeMultiplier;
            }
        }
    }

    
    protected override void Deactive()
    {
        _TargetUnit.EffectsManager.addMyEffectE -= AddNewEffect;
        _TargetUnit.RemoveMoveMultiplier((int)IDStack);
    }
    

    public override BaseEffect Clone()
    {
        return new WeaknessDebufEffect(_Slow, _TimeMultiplier, TimeLife);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }
}
