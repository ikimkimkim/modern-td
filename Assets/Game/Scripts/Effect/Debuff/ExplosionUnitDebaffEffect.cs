﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class ExplosionUnitDebaffEffect : BaseEffect
{

    private float _Range, _DmgAOEMultiplier, _DmgMultiplier, _Time;

    public ExplosionUnitDebaffEffect(float Range, float Dmg, float DmgAOE, float Time) 
    {
        IDStack = IDEffect.ExplosionTimerDebuff;
        _Range = Range;
        _DmgMultiplier = Dmg;
        _DmgAOEMultiplier = DmgAOE;
        _Time = Time;
    }

    public override bool isSplashUnique { get { return false; } }

    public override string URLIcon { get { return "EffectIcon/AOEChanceKill.png"; } }

    public override string InfoText { get { return string.Format("<color=#ff0000>Взорвется</color> через {0} c", _Time); } }

    public override bool isBuff { get { return false; } }

    protected override string HtmlColor { get { return "#9851ff"; } }

    protected override void Apply()
    {
        workСonditionEffect = new EffectTimeLimiteWC(_Time);
    }

    protected override void Active()
    {
        shootObject = _RootUnit.GetShootObjectT().GetComponent<ShootObject>();
    }

    GameObject objSplash;
    ShootObject shootObject;

    private void Explosion()
    {
        if (shootObject != null && shootObject.Splash != null)
        {
            objSplash = ObjectPoolManager.Spawn(shootObject.Splash, _TargetUnit.getPosition, Quaternion.identity);
            objSplash.GetComponent<SplashSpecialEffect>().Emit(_Range);
        }
        else
            Debug.LogWarning("ExplosionUnitDebaffEffect not special effect!");

        foreach (var unit in SpawnManager.instance.ActiveCreeper.FindAll(i => !i.dead && i != _TargetUnit && StaticCommon.DistanceLimit(i.getPosition, _TargetUnit.getPosition, _Range) ))
        {
            AttackInstance attack = new AttackInstance();
            attack.srcUnit = _RootUnit;
            attack.tgtUnit = unit;
            //attack.SetAOERadius(_Range);
            attack.damage = _TargetUnit.fullHP * _DmgAOEMultiplier;
            //attack.ApplyDistance(Vector3.Distance(deadUnit.getPosition, unit.transform.position));
            unit.ApplyEffect(attack);
        }
        _TargetUnit.ApplyDamageHP(_RootUnit, _TargetUnit.fullHP * _DmgMultiplier);
    }

    protected override void Deactive()
    {
    }

    protected override void Update()
    {
        if(isWork==false)
        {
            Explosion();
        }
    }

    public override BaseEffect Clone()
    {
        return new ExplosionUnitDebaffEffect(_Range, _DmgMultiplier,_DmgAOEMultiplier, _Time);
    }

    protected override void Cancel()
    {
        
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackDotEffect();
    }
}
