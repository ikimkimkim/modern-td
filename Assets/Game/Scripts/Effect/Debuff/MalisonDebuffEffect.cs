﻿using UnityEngine;
using System.Collections;
using TDTK;

public class MalisonDebuffEffect : PowerEffect
{

    public override bool isSplashUnique { get { return false; } }

    public override string URLIcon { get { return "EffectIcon/Malison.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Увеличен</color> входящий урон\r\nМагические статусы действуют бесконечно"; } }

    protected override string HtmlColor { get { return "#b7e1cd"; } }
    public override bool isBuff { get { return false; } }

    public MalisonDebuffEffect(float powerDmg, float time) : base(time)
    {
        IDStack = IDEffect.MalisonDebuff;
        Power = (1 + powerDmg);
    }

    protected override void Active()
    {
        _TargetUnit.myInAttakInstanceFirstE += AttaInstanceIn;
        _TargetUnit.EffectsManager.addMyEffectE += EffectsManager_addMyEffectE;
        UpTimeActiveEffects();
    }

    private void EffectsManager_addMyEffectE(BaseEffect effect)
    {
        if (effect.IDStack == IDEffect.FireDot || effect.IDStack == IDEffect.PoisonDot
            || effect.IDStack == IDEffect.SlowControl)
        {
            if (effect.workСonditionEffect != null)
                effect.workСonditionEffect.Destroy();
            effect.workСonditionEffect = new EffectNotLimiteWC();
        }
    }

    private void AttaInstanceIn(AttackInstance instance)
    {
        instance.damage *= Power;
    }

    protected override void Deactive()
    {
        _TargetUnit.myInAttakInstanceFirstE -= AttaInstanceIn;
        _TargetUnit.EffectsManager.addMyEffectE -= EffectsManager_addMyEffectE;
    }

    public void UpTimeActiveEffects()
    {
        foreach (var stack in _TargetUnit.EffectsManager.Stacks)
        {
            if (stack.Key == (int)IDEffect.FireDot || stack.Key == (int)IDEffect.PoisonDot || stack.Key == (int)IDEffect.SlowControl)
            {
                foreach (var item in stack.Value.getAllEffect())
                {
                    if (item.workСonditionEffect != null)
                        item.workСonditionEffect.Destroy();
                    item.workСonditionEffect = new EffectNotLimiteWC();
                }
            }
        }
    }


    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }

    public override BaseEffect Clone()
    {
        return new MalisonDebuffEffect(Power, TimeLife);
    }
}
