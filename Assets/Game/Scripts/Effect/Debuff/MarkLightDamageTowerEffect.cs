﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class MarkLightDamageTowerEffect : PowerEffect
{
    public override bool isSplashUnique { get { return true; } }

    public override string URLIcon { get { return "EffectIcon/MarkLightDamageTowerDebaff.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Увеличен</color> легкий урон"; } }

    protected override string HtmlColor { get { return "#ff6e40"; } } 
    public override bool isBuff { get { return false; } }

    public MarkLightDamageTowerEffect(float power, float time) : base(time)
    {
        IDStack = IDEffect.MarkLightDamageTowerDebuff;
        Power = (1 + power);
    }


    protected override void Active()
    {
        _TargetUnit.myInAttakInstanceFirstE += AttaInstanceIn;
    }

    private void AttaInstanceIn(AttackInstance instance)
    {
        if(instance.srcUnit is UnitTower && instance.srcUnit.DamageType()== (int)enumDamageType.Light)
            instance.damage *= Power;
    }

    protected override void Deactive()
    {
        _TargetUnit.myInAttakInstanceFirstE -= AttaInstanceIn;
    }


    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }

    public override BaseEffect Clone()
    {
        return new MarkLightDamageTowerEffect(Power, TimeLife);
    }
}
