﻿using UnityEngine;
using System.Collections;
using TDTK;

public class MagicShockDebufEffect : PowerEffect
{
    public override bool isSplashUnique { get { return true; } }

    public override string URLIcon { get { return "EffectIcon/ShockMagic.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Увеличен</color> входящий магический урон"; } }

    protected override string HtmlColor { get { return "#c07fb7"; } }
    public override bool isBuff { get { return false; } }

    public MagicShockDebufEffect(float power, float time) : base(time)
    {
        IDStack = IDEffect.MagicShock;
        Power = (1 + power);
    }


    protected override void Active()
    {
        _TargetUnit.myInAttakInstanceFirstE += AttaInstanceIn;
    }
    private void AttaInstanceIn(AttackInstance instance)
    {
        if(instance.getDamageType == 2)
            instance.damage *= Power;
    }

    protected override void Deactive()
    {
        _TargetUnit.myInAttakInstanceFirstE -= AttaInstanceIn;
    }


    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }

    public override BaseEffect Clone()
    {
        return new MagicShockDebufEffect(Power, TimeLife);
    }
}
