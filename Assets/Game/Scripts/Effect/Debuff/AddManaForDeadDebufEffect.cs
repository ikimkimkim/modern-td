﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class AddManaForDeadDebufEffect : PowerEffect
{


    public override string URLIcon { get { return "EffectIcon/AddManaForDead.png"; } }
    public override string InfoText { get { return "Восстановление маны при смерти"; } }

    protected override string HtmlColor { get { return "#025193"; } }
    public override bool isBuff { get { return false; } }

    public AddManaForDeadDebufEffect(float power, float time) : base(time)
    {
        Power = power;
        IDStack = IDEffect.AddManaForDeadDebaff;
    }

    protected override void Active()
    {
        if (_TargetUnit.IsCreep == false)
            return;
        _TargetUnit.onMyStartDestroyedE += onMyDestroyedE;
    }

    private void onMyDestroyedE(Unit unit)
    {
        if (unit.IsCreep == false)
            return;
        List<float> rscGain = new List<float>() { 0, Power };

        ResourceManager.GainResource(rscGain);
    }

    protected override void Deactive()
    {
        if (_TargetUnit.IsCreep == false)
            return;
        _TargetUnit.onMyStartDestroyedE -= onMyDestroyedE;
    }

    protected override void Cancel()
    {

    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }

    public override BaseEffect Clone()
    {
        return new AddManaForDeadDebufEffect(Power, TimeLife);
    }
}
