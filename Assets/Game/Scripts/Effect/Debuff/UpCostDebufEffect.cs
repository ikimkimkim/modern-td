﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class UpCostDebufEffect : PowerEffect
{
    public override string URLIcon { get { return "EffectIcon/UpCost.png"; } }
    public override string InfoText { get { return "Увеличена награда за убийство"; } }

    protected override string HtmlColor { get { return "#ccc201"; } }
    public override bool isBuff { get { return false; } }

    public UpCostDebufEffect(float power, float time) : base(time)
    {
        Power = 1+power;
        IDStack = IDEffect.UpCostDebaff;
    }

    protected override void Active()
    {
        if (_TargetUnit.IsCreep == false)
            return;
        if(_TargetUnit.GetUnitCreep.valueRscMin.Count==0)
        {
            MyLog.LogWarning("UpCost unit:"+_TargetUnit.name+" not cost!");
            return;
        }

        _TargetUnit.GetUnitCreep.valueRscMin[0] *= Power;
        _TargetUnit.GetUnitCreep.valueRscMax[0] *= Power;
        
    }

    protected override void Deactive()
    {
        if (_TargetUnit.IsCreep == false)
            return;
        if (_TargetUnit.GetUnitCreep.valueRscMin.Count == 0)
        {
            return;
        }
        _TargetUnit.GetUnitCreep.valueRscMin[0] /= Power;
        _TargetUnit.GetUnitCreep.valueRscMax[0] /= Power;
    }


    protected override void Cancel()
    {

    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }

    public override BaseEffect Clone()
    {
        return new UpCostDebufEffect(Power, TimeLife);
    }
}
