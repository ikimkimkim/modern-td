﻿using UnityEngine;
using System.Collections;

public class DownDmgUniqueEffect : BaseEffect
{
    private float Multiplier;
    public override string URLIcon { get { return "EffectIcon/DownDmg.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Уменьшен</color> исходящий урон"; } }

    protected override string HtmlColor { get { return "#f5947b"; } }
    public override bool isBuff { get { return false; } }

    public override bool isSplashUnique { get { return false; } }

    public DownDmgUniqueEffect(float power) 
    {
        Multiplier = power;
        IDStack = IDEffect.DownDmgUnique;
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectRootLife(_RootUnit);
    }

    protected override void Active()
    {
        for (int i = 0; i < _TargetUnit.stats.Count; i++)
        {
            _TargetUnit.stats[i].damageMin *= (1 - Multiplier);
            _TargetUnit.stats[i].damageMax *= (1 - Multiplier);
        }
    }

    protected override void Deactive()
    {
        for (int i = 0; i < _TargetUnit.stats.Count; i++)
        {
            _TargetUnit.stats[i].damageMin /= (1 - Multiplier);
            _TargetUnit.stats[i].damageMax /= (1 - Multiplier);
        }
    }

    protected override void Cancel()
    {

    }

    protected override void Update()
    {

    }

    public override BaseEffect Clone()
    {
        return new DownDmgUniqueEffect(Multiplier);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }
}
