﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class ExplosionAfterDeathDebaffEffect : UniqueEffect
{
    protected static int _ID = 3;
    protected float _Range, _Chance, _DmgMultiplier, _Time;

    public ExplosionAfterDeathDebaffEffect(float Chance, float Range, float Dmg, float Time) : base(_ID)
    {
        IDStack = IDEffect.ExplosionAfterDeathDebuff;
        _Chance = Chance;
        _Range = Range;
        _DmgMultiplier = Dmg;
        _Time = Time;
    }

    public override bool isSplashUnique { get { return false; } }

    public override string URLIcon { get { return "EffectIcon/AOEChanceKill.png"; } }

    public override string InfoText { get { return string.Format("Шанс {0} <color=#ff0000>взорваться</color> после смерти",Mathf.Round(_Chance*100f)); } }

    public override bool isBuff { get { return false; } }

    protected override string HtmlColor { get { return "#9851ff"; } }

    protected override void Apply()
    {
        workСonditionEffect = new EffectTimeLimiteWC(_Time);
        shootObject = _RootUnit.GetShootObjectT().GetComponent<ShootObject>();
    }

    protected override void Active()
    {
        _TargetUnit.onMyStartDestroyedE += _TargetUnit_onMyDestroyedE;
    }

    GameObject objSplash;
    protected ShootObject shootObject;
    private void _TargetUnit_onMyDestroyedE(Unit deadUnit)
    {
        _TargetUnit.onMyStartDestroyedE -= _TargetUnit_onMyDestroyedE;
        if(Random.Range(0f, 1f) < _Chance)
        {
            if (shootObject != null && shootObject.Splash != null)
            {
                objSplash = ObjectPoolManager.Spawn(shootObject.Splash, deadUnit.getPosition, Quaternion.identity);
                objSplash.GetComponent<SplashSpecialEffect>().Emit(_Range);
            }

            foreach (var unit in SpawnManager.instance.ActiveCreeper.FindAll(i=> !i.dead && StaticCommon.DistanceLimit(i.getPosition,deadUnit.getPosition,_Range)))
            {
                AttackInstance attack = new AttackInstance();
                attack.SetAOERadius(_Range);
                float Modifier = 1;
                if(_RootUnit!=null)
                {
                    Modifier = _RootUnit.IgnoreTypeArmor() ? 1 : DamageTable.GetModifier(unit.ArmorType(), _RootUnit.DamageType());
                }
                attack.damage = deadUnit.fullHP * _DmgMultiplier * Modifier;
                attack.ApplyDistance(Vector3.Distance(deadUnit.getPosition, unit.transform.position));
                unit.ApplyDamage(null, attack.damage,0,true);
            }

        }

    }

    protected override void Deactive()
    {
        _TargetUnit.onMyStartDestroyedE -= _TargetUnit_onMyDestroyedE;
    }

    protected override void Update()
    {

    }

    public override BaseEffect Clone()
    {
        return new ExplosionAfterDeathDebaffEffect(_Chance,_Range,_DmgMultiplier,_Time);
    }
}
