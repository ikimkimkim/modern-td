﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class ExplosionAfterDeathDebaffUnlimitedEffect : ExplosionAfterDeathDebaffEffect
{
    public override string InfoText { get { return string.Format("<color=#ff0000>Взорваться</color> после смерти", Mathf.Round(_Chance * 100f)); } }

    public ExplosionAfterDeathDebaffUnlimitedEffect(float Range, float Dmg) : base(1, Range, Dmg, 0)
    {

    }
    
    protected override void Apply()
    {
        workСonditionEffect = new EffectNotLimiteWC();
        shootObject = DataBase<GameObject>.LoadIndex(DataBase<GameObject>.NameBase.ShootObject, 1).GetComponent<ShootObject>();//  _RootUnit.GetShootObjectT().GetComponent<ShootObject>();
    }

    public override BaseEffect Clone()
    {
        return new ExplosionAfterDeathDebaffUnlimitedEffect(_Range, _DmgMultiplier);
    }
}
