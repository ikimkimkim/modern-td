﻿using UnityEngine;
using System.Collections;

public class IcyShockDebuffEffect : BaseEffect
{
    public override bool isSplashUnique { get { return false; } }

    public override string URLIcon { get { return "EffectIcon/IcyShockProperties.png"; } }

    public override string InfoText { get { return "Нарастающее обморожение\r\n<color=#ff0000>Увеличен</color> входящий урон всех типов"; } }

    public override bool isBuff { get { return false; } }

    protected override string HtmlColor { get { return "#e1cee3"; } }

    private float power, maxPower, duration;
    private float TimeStart;
    public IcyShockDebuffEffect(float Power, float MaxPower, float Duration)
    {
        power = Power;
        maxPower = MaxPower;
        duration = Duration;
        IDStack = IDEffect.IcyShock;
    }


    protected override void Apply()
    {
        workСonditionEffect = new EffectTimeLimiteWC(duration);
        TimeStart = Time.time;
        _TargetUnit.myInAttakInstanceFirstE += _TargetUnit_myInAttakInstanceFirstE;
    }

    private void _TargetUnit_myInAttakInstanceFirstE(TDTK.AttackInstance attackInstance)
    {
        float mDmg = Mathf.Clamp(Mathf.Floor(1f + Time.time - TimeStart) * power, power, maxPower) + 1f;
        //Debug.LogWarning("Add dmg:"+mDmg+" p:"+power+" mp:"+maxPower+" t:"+ Mathf.Floor(1f + Time.time - TimeStart));
        attackInstance.damage *= mDmg;
    }

    protected override void Update()
    {

    }

    protected override void Cancel()
    {
        _TargetUnit.myInAttakInstanceFirstE -= _TargetUnit_myInAttakInstanceFirstE;
    }


    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }

    public override BaseEffect Clone()
    {
        return new IcyShockDebuffEffect(power,maxPower,duration);
    }
}
