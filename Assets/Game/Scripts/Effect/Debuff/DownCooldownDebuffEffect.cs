﻿using UnityEngine;
using System.Collections;

public class UpCooldownDebuffEffect : BaseEffect
{

    private float Multiplier, Time;
    public override string URLIcon { get { return "EffectIcon/Iceberg_prop.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Уменьшеная</color> скорострельность"; } }

    protected override string HtmlColor { get { return "#fefacb"; } }
    public override bool isBuff { get { return false; } }

    public override bool isSplashUnique { get { return false; } }

    public UpCooldownDebuffEffect(float power,float time)
    {
        Multiplier = power;
        Time = time;
        IDStack = IDEffect.UpCooldownDebufUnique;
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectTimeLimiteWC(Time);
    }

    protected override void Active()
    {
        for (int i = 0; i < _TargetUnit.stats.Count; i++)
        {
            _TargetUnit.stats[i].cooldown *= (1 + Multiplier);
        }
    }

    protected override void Deactive()
    {
        for (int i = 0; i < _TargetUnit.stats.Count; i++)
        {
            _TargetUnit.stats[i].cooldown /= (1 + Multiplier);
        }
    }

    protected override void Cancel()
    {

    }

    protected override void Update()
    {

    }

    public override BaseEffect Clone()
    {
        return new UpCooldownDebuffEffect(Multiplier,Time);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }
}
