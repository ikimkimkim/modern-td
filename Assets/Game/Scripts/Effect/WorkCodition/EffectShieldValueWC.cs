﻿using UnityEngine;
using System.Collections;
using TDTK;

public class EffectShieldValueWC : IWorkСonditionEffect
{
    public float getProcentEnd {
        get {
            if (linkData == null)
                return 0;
            return 1f-linkData.Value / linkData.FullShield;
        }
    }

    public ShieldData linkData;

    public void Destroy()
    {
        linkData = null;
    }

    public void Refresh()
    {

    }

    public EffectShieldValueWC(ShieldData shieldData)
    {
        linkData = shieldData;
    }

    public bool WorkConditionFulfilled
    {
        get
        {
            return linkData.Value > 0;
        }
    }
}
