﻿using UnityEngine;
using System.Collections;
using TDTK;

public class EffectShieldLifeWC : IWorkСonditionEffect
{
    public float getProcentEnd
    {
        get
        {
            if (linkData == null)
                return 0;
            return linkData.Value > 0 ? 0f : 1f;
        }
    }

    public ShieldData linkData;

    public void Destroy()
    {
        linkData = null;
    }

    public void Refresh()
    {

    }

    public EffectShieldLifeWC(ShieldData shieldData)
    {
        linkData = shieldData;
    }

    public bool WorkConditionFulfilled
    {
        get
        {
            return linkData.Value > 0;
        }
    }
}
