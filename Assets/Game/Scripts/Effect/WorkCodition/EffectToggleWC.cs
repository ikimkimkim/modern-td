﻿using UnityEngine;
using System.Collections;

public class EffectToggleWC : IWorkСonditionEffect
{

    public bool Toggle;


    public float getProcentEnd { get { return 0; } }

    public void Destroy()
    {
        Toggle = false;
    }

    public void Refresh()
    {
    }

    public EffectToggleWC(bool isOn)
    {
        Toggle = isOn;
    }

    public bool WorkConditionFulfilled
    {
        get
        {
            return Toggle;
        }
    }

}
