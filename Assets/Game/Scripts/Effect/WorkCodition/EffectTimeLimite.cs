﻿using UnityEngine;
using System.Collections;

public class EffectTimeLimiteWC : IWorkСonditionEffect
{
    public float TimeStart;
    public float TimeLife;

    public float getProcentEnd { get { return (Time.time - TimeStart) / TimeLife; } }

    public EffectTimeLimiteWC(float timeLife)
    {
        TimeLife = timeLife;
        Refresh();
    }

    public bool WorkConditionFulfilled
    {
        get
        {
            return (Time.time - TimeStart) <= TimeLife;
        }
    }
    
    public void Refresh()
    {
        TimeStart = Time.time;
    }

    public void Destroy()
    {
        TimeLife = 0;
    }
}
