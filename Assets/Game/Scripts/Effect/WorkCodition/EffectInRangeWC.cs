﻿using UnityEngine;
using System.Collections;
using TDTK;

public class EffectInRangeWC : IWorkСonditionEffect
{
    public Unit Before;
    public Unit From;
    public float Range;

    public float getProcentEnd { get { return 0; }  }

    public EffectInRangeWC(Unit from, Unit before, float range)
    {
        From = from;
        Before = before;
        Range = range;

        lastB = true;
        lastTime = Time.time;
    }

    private float lastTime;
    private bool lastB;


    public bool WorkConditionFulfilled
    {
        get
        {
            if (From == null || From.dead || Before == null || Before.dead)
                return false;
            if (Time.time > lastTime + 0.2f)
            {
                lastB = Vector3.Distance(From.getPosition, Before.getPosition) - Before.SizeMyCollider <= Range/* * From.thisT.localScale.x*/;
                lastTime = Time.time;
            }
            return lastB;
        }
    }

    public void Refresh()
    {

    }

    public void Destroy()
    {
        From = null;
        Before = null;
    }
}
