﻿using UnityEngine;
using System.Collections;
using TDTK;

public class EffectRootLife : IWorkСonditionEffect
{
    private Unit _root;

    public float getProcentEnd { get { return 0; } }

    public EffectRootLife(Unit root)
    {
        _root = root;
    }

    public bool WorkConditionFulfilled
    {
        get
        {
            return _root != null && _root.dead == false;
        }
    }

    public void Refresh()
    {

    }

    public void Destroy()
    {
        _root = null;
    }
}
