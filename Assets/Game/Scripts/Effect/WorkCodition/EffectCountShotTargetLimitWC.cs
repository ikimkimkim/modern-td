﻿using UnityEngine;
using System.Collections;
using TDTK;

public class EffectCountShotTargetLimitWC : IWorkСonditionEffect
{
    public int count;
    public int MaxCount;
    Unit root;

    public float getProcentEnd { get { return (float) count / ((float)MaxCount+1f); } }

    public EffectCountShotTargetLimitWC(Unit tower, int MaxCountShot)
    {
        root = tower;
        root.onShotTurretE += Shot;

        MaxCount = MaxCountShot;
        Refresh();
    }

    public void Shot(Unit target)
    {
        count++;
    }

    public bool WorkConditionFulfilled
    {
        get
        {
            return count <= MaxCount;
        }
    }

    public void Refresh()
    {
        count = 0;
    }

    public void Destroy()
    {
        root.onShotTurretE -= Shot;
        root = null;
    }
}
