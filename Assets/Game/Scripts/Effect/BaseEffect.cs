﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public abstract class BaseEffect
{
    public abstract bool isSplashUnique { get; }

    public IDEffect IDStack { get; protected set; }

    private bool bApply = false;
    private bool bActive = false;

    public IWorkСonditionEffect workСonditionEffect;
    

    public Unit _TargetUnit { get; protected set; }
    public Unit _RootUnit { get; protected set; }

    public IStackEffect _Stack;

    public BaseEffect()
    {
        IDStack = IDEffect.none;
    }

    public bool isWork
    {
        get {
            if (workСonditionEffect == null)
                return false;
            return workСonditionEffect.WorkConditionFulfilled;
        }
    }

    public void AppylEffect(Unit rootUnit, Unit targetUnit, IStackEffect stack)
    {
        if (bApply == false)
        {
            //MyLog.Log("Appyl effect:"+IDStack);
            _RootUnit = rootUnit;
            _TargetUnit = targetUnit;
            _Stack = stack;
            Apply();
            bApply = true;
        }
    }
    protected abstract void Apply(); // Вызывается при наложение

    public void UpdateEffect()
    {
        if (bApply == false)
            return;

        Update();

        if (isWork == false || _TargetUnit.dead)
        {
            CancelEffect();
            return;
        }

    }
    protected abstract void Update(); // Вызывается каждый кадр

    public void CancelEffect()
    {
        if(bApply)
        {
            //MyLog.Log("Cancel effect:" + IDStack);
            Deactivate();
            Cancel();
            workСonditionEffect.Destroy();
            workСonditionEffect = null;

            _RootUnit = null;
            _TargetUnit = null;
            _Stack = null;
        }
        bApply = false;
    }
    protected abstract void Cancel(); // Вызывается при снятие эффекта

    public void Activate()
    {
        if(bApply && bActive==false)
        {
            Active();
            bActive = true;
        }
    }
    protected virtual void Active() { } // Вызов при активации только после наложения

    public void Deactivate()
    {
        if(bActive)
        {
            Deactive();
            bActive = false;
        }
    }
    protected virtual void Deactive() { } // Вызов при деактивации


    private Color _colorIcon;
    private bool ColorParse = false;
    public Color ColorIcon
    {
        get
        {
            if (ColorParse==false)
            {
                if (!ColorUtility.TryParseHtmlString(HtmlColor, out _colorIcon))
                    Debug.LogError("Error parseHtmlString Color:" + HtmlColor);
                else
                    ColorParse = true;
            }
            return _colorIcon;
        }
    }
    protected abstract string HtmlColor { get; }

    public abstract BaseEffect Clone();
    public abstract IStackEffect GetStackEffect();
    public abstract string URLIcon { get; }
    public abstract string InfoText { get; }
    public abstract bool isBuff { get; }
}
