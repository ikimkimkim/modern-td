﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;

public class StackUniqueEffect : IStackEffect
{
    public List<BaseEffect> Stack;


    public int Count { get { return Stack.Count; } }

    public bool isBuff
    {
        get
        {
            if (Stack.Count <= 0)
                return false;
            else
                return Stack[0].isBuff;
        }
    }

    public Color getColor
    {
        get
        {
            if (Stack.Count <= 0)
                return Color.white;
            else
                return Stack[0].ColorIcon;
        }
    }
    public StackUniqueEffect()
    {
        Stack = new List<BaseEffect>();
    }

    public void Add(Unit root, Unit target, BaseEffect effect)
    {
        if ((effect is UniqueEffect) == false)
            return;

        UniqueEffect unique = effect as UniqueEffect;

        if (isUniqueEffect(unique))
        {
            //Debug.Log("SUE Add effect unique "+target.name);
            Stack.Add(unique);
            unique.AppylEffect(root, target, this);
            effect.Activate();
        }
       // else
       //     Debug.LogWarning("SUE Add effect not unique! " + target.name);

    }

    private bool isUniqueEffect(UniqueEffect effect)
    {
        UniqueEffect unique;
        for (int i = 0; i < Stack.Count; i++)
        {
            unique = Stack[i] as UniqueEffect;
            if(unique.isWork == false)
            {
                unique.CancelEffect();
                Remove(unique);
                i--;
                continue;
            }
            else if(effect.ID == unique.ID)
            {
                return false;
            }
        }
        return true;
    }

    public void Remove(BaseEffect effect)
    {
        Stack.Remove(effect);
        
    }

    public void Update()
    {
        for (int i = 0; i < Stack.Count; i++)
        {
            Stack[i].UpdateEffect();
            if(Stack[i].isWork == false)
            {
                Remove(Stack[i]);
                i--;
            }
        }
    }

    public List<BaseEffect> getAllEffect()
    {
        return Stack;
    }

    public void ClearData()
    {
        for (int i = 0; i < Stack.Count; i++)
        {
            Stack[i].CancelEffect();
        }
        Stack.Clear();
    }

    public UIEffectData GetUIEffectData()
    {
        int i = Stack.Count - 1;
        return new UIEffectData(Stack[i].URLIcon,Stack.Count,Stack[i].workСonditionEffect.getProcentEnd,Stack[i].InfoText, Stack[i].ColorIcon, Stack[i].isBuff);
    }
}
