﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;

public class StackRefreshActiveEffect : IStackEffect
{
    public List<BaseEffect> Stack;
    private int maxStack;

    public int Count { get { return Stack.Count; } }

    public bool isBuff
    {
        get
        {
            if (Stack.Count <= 0)
                return false;
            else
                return Stack[0].isBuff;
        }
    }

    public Color getColor
    {
        get
        {
            if (Stack.Count <= 0)
                return Color.white;
            else
                return Stack[0].ColorIcon;
        }
    }
    public StackRefreshActiveEffect(int countMaxStack)
    {
        Stack = new List<BaseEffect>();
        maxStack = countMaxStack;
    }

    public void Add(Unit root, Unit target, BaseEffect effect)
    {
        Stack.Add(effect);
        effect.AppylEffect(root, target, this);
        if (Stack.Count > 1)
        {
            Stack[0].workСonditionEffect.Refresh();
            Stack[0].Deactivate();
            Stack[0].Activate();
        }
        else
            effect.Activate();

    }


    public void Remove(BaseEffect effect)
    {
        if (Count > 0 && effect == Stack[0])
        {
            Stack.Clear();
        }
    }

    public void Update()
    {
        if (Stack.Count > 0)
        {
            //for (int i = 0; i < Stack.Count; i++)
            //{ 
            Stack[0].UpdateEffect();
            if (Stack[0].isWork == false)
            {
                for (int i = 0; i < Stack.Count; i++)
                    Stack[i].CancelEffect();
                //BaseEffect effect = Stack[0];
                Remove(Stack[0]);
                // effect.CancelEffect();
            }
            //}
        }
    }

    public List<BaseEffect> getAllEffect()
    {
        return Stack;
    }

    public void ClearData()
    {
        for (int i = 0; i < Stack.Count; i++)
        {
            Stack[i].CancelEffect();
        }
        Stack.Clear();
    }

    public UIEffectData GetUIEffectData()
    {
        return new UIEffectData(Stack[0].URLIcon, Mathf.Clamp(Stack.Count,0,maxStack), Stack[0].workСonditionEffect.getProcentEnd, Stack[0].InfoText, Stack[0].ColorIcon, Stack[0].isBuff);
    }
}
