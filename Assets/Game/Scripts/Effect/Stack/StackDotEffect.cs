﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;

public class StackDotEffect : IStackEffect
{
    bool bCleared;
    public List<BaseEffect> Stack;

    public StackDotEffect()
    {
        Stack = new List<BaseEffect>();
    }

    public int Count { get { return Stack.Count; } }

    public bool isBuff {
        get {
            if (bCleared || Stack.Count<=0)
                return false;
            else
                return Stack[0].isBuff;
        }
    }

    public Color getColor
    {
        get
        {
            if (bCleared || Stack.Count <= 0)
                return Color.white;
            else
                return Stack[0].ColorIcon;
        }
    }

    public void Add(Unit root, Unit target, BaseEffect effect)
    {
        bCleared = false;
        //MyLog.Log("Add dot effect");
        effect.AppylEffect(root, target, this);
        effect.Activate();
        Stack.Add(effect);
    }

    public void Remove(BaseEffect effect)
    {
        //MyLog.Log("Remove dot effect");
        Stack.Remove(effect);
        IndexStack--;
        effect.CancelEffect();
    }

    private int IndexStack;

    public void Update()
    {
        for (IndexStack = 0; IndexStack < Stack.Count; IndexStack++)
        {
            Stack[IndexStack].UpdateEffect();
            if (bCleared)
                return;
            if (Stack[IndexStack].isWork == false)
            {
                Remove(Stack[IndexStack]);
            }
        }
    }

    public List<BaseEffect> getAllEffect()
    {
        return Stack;
    }

    public void ClearData()
    {
        bCleared = true;
        for (int i = 0; i < Stack.Count; i++)
        {
            Stack[i].CancelEffect();
        }
        Stack.Clear();
        buff = null;
    }


    BaseEffect buff;
    UIEffectData buffData;
    public UIEffectData GetUIEffectData()
    {
        if (Stack.Count <= 0)
            return buffData;

        buff = Stack[Stack.Count - 1];

        buffData.URLIcon = buff.URLIcon;
        buffData.CountStack = Stack.Count;
        buffData.Progress = buff.workСonditionEffect.getProcentEnd;
        buffData.textInfo = buff.InfoText;
        buffData.Color = buff.ColorIcon;
        buffData.isBuff = buff.isBuff;

        return buffData;// new UIEffectData(buff.URLIcon, Stack.Count, buff.workСonditionEffect.getProcentEnd, buff.InfoText, buff.ColorIcon, buff.isBuff);
        
    }
}
