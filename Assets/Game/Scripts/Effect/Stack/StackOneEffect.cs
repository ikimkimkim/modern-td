﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class StackOneEffect : IStackEffect
{
    public List<BaseEffect> Stack;


    public int Count { get { return Stack.Count; } }

    public bool isBuff
    {
        get
        {
            if (Stack.Count <= 0)
                return false;
            else
                return Stack[0].isBuff;
        }
    }

    public Color getColor
    {
        get
        {
            if ( Stack.Count <= 0)
                return Color.white;
            else
                return Stack[0].ColorIcon;
        }
    }

    public StackOneEffect()
    {
        Stack = new List<BaseEffect>();
    }

    public void Add(Unit root, Unit target, BaseEffect effect)
    {
        if(Count>0)
        {
            Stack[0].workСonditionEffect.Refresh();
        }
        else
        {
            Stack.Add(effect);
            effect.AppylEffect(root, target, this);
            effect.Activate();
        }
    }


    public void Remove(BaseEffect effect)
    {
        if(Count>0 && effect == Stack[0])
        {
            Stack.Clear();
        }
    }

    public void Update()
    {
        if(Count>0)
        {
            Stack[0].UpdateEffect();
            if(Stack[0].isWork==false)
            {
                Stack[0].CancelEffect();
                //BaseEffect effect = Stack[0];
                Remove(Stack[0]);
               // effect.CancelEffect();
            }
        }
    }

    public List<BaseEffect> getAllEffect()
    {
        return Stack;
    }

    public void ClearData()
    {
        for (int i = 0; i < Stack.Count; i++)
        {
            Stack[i].CancelEffect();
        }
        Stack.Clear();
    }

    public UIEffectData GetUIEffectData()
    {
        return new UIEffectData(Stack[0].URLIcon, 0, Stack[0].workСonditionEffect.getProcentEnd, Stack[0].InfoText,Stack[0].ColorIcon,Stack[0].isBuff);
    }
}
