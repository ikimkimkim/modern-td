﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class StackMaxPowerEffect : IStackEffect {

    public List<PowerEffect> Stack;
    public List<BaseEffect> StackClone;


    public PowerEffect ActionEffect;

    public StackMaxPowerEffect()
    {
        Stack = new List<PowerEffect>();
        StackClone = new List<BaseEffect>();
    }

    public bool isBuff
    {
        get
        {
            if (ActionEffect==null)
                return false;
            else
                return ActionEffect.isBuff;
        }
    }

    public Color getColor
    {
        get
        {
            if (ActionEffect == null)
                return Color.white;
            else
                return ActionEffect.ColorIcon;
        }
    }

    public int Count { get { return Stack.Count; } }

    public void Add(Unit root, Unit target, BaseEffect effect)
    {
        if (effect is PowerEffect)
        {
            //Debug.Log("Add Control effect " + effect.IDStack);
            PowerEffect ce = (PowerEffect)effect;
            Stack.Add(ce);
            StackClone.Add(effect);
            ce.AppylEffect(root, target, this);
            if (ActionEffect == null)
            {
                ActionEffect = ce;
                ActionEffect.Activate();
                //Debug.Log("Action1");
            }
            else if (ce.Power > ActionEffect.Power)
            {
                //Debug.Log("Action new effect power");
                ActionEffect.Deactivate();
                ActionEffect = ce;
                ActionEffect.Activate();
            }
            //else
            //    Debug.Log("ce:" + (ce==ActionEffect));
        }
        else
            Debug.LogError("Add effect is not control!");
    }


    private int IndexStack;

    public void Update()
    {        
        for (IndexStack = 0; IndexStack < Stack.Count; IndexStack++)
        {
            Stack[IndexStack].UpdateEffect();
            if(Stack[IndexStack].isWork==false)
            {
                Remove(Stack[IndexStack]);
                //if (Stack.Count == 0)
                //    return;
            }
        }

    }
    
    public void Remove(BaseEffect effect)
    {
        if (effect is PowerEffect)
        {
            Stack.Remove((PowerEffect)effect);
            StackClone.Remove(effect);
            //Debug.Log("Remove Control effect " + Stack.Count);
            IndexStack--;
            effect.CancelEffect();
            if(effect as PowerEffect == ActionEffect)
                UpdateAction();
        }
    }

    public void UpdateAction()
    {
        ActionEffect = null;

        if (Stack.Count == 0)
        {
            //MyLog.Log("Clear Control Stack");
            return;
        }
        /*else
            MyLog.Log("UpdateAction Control Stack");*/

        bool update = false;
        foreach (PowerEffect item in Stack)
        {
            if(item!=ActionEffect && (ActionEffect == null || item.Power>ActionEffect.Power))
            {
                update = true;
                ActionEffect = item;
            }
        }
        if (update)
            ActionEffect.Activate();
    }

    public List<BaseEffect> getAllEffect()
    {
        return StackClone;
    }

    public void ClearData()
    {
        for (int i = 0; i < Stack.Count; i++)
        {
            Stack[i].CancelEffect();
        }
        Stack.Clear();
        StackClone.Clear();
    }

    public UIEffectData GetUIEffectData()
    {
        return new UIEffectData(ActionEffect.URLIcon, Stack.Count, ActionEffect.workСonditionEffect.getProcentEnd, ActionEffect.InfoText,ActionEffect.ColorIcon,ActionEffect.isBuff);
    }
}
