﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class StackOneActiveEffect : IStackEffect
{
    public List<BaseEffect> Stack;


    public int Count { get { return Stack.Count; } }

    public bool isBuff
    {
        get
        {
            if (Stack.Count <= 0)
                return false;
            else
                return Stack[0].isBuff;
        }
    }

    public Color getColor
    {
        get
        {
            if (Stack.Count <= 0)
                return Color.white;
            else
                return Stack[0].ColorIcon;
        }
    }

    public StackOneActiveEffect()
    {
        Stack = new List<BaseEffect>();
    }

    public void Add(Unit root, Unit target, BaseEffect effect)
    {
        Stack.Add(effect);
        effect.AppylEffect(root, target, this);
        UpdateActiveEffect();
    }


    public void Remove(BaseEffect effect)
    {
        if (Count > 0 && effect == Stack[0])
        {
            Stack.Remove(effect);
            UpdateActiveEffect();
        }
    }

    private void UpdateActiveEffect()
    {
        if (Count <= 0)
            return;
        Stack[0].Deactivate();
        Stack[0].Activate();
    }

    public void Update()
    {
        for (int i = 0; i < Count; i++)
        {
            Stack[i].UpdateEffect();
            if (Stack[i].isWork == false)
            {
                Stack[i].CancelEffect();
                Stack.RemoveAt(i);
                UpdateActiveEffect();
                i--;
            }
        }
    }

    public List<BaseEffect> getAllEffect()
    {
        return Stack;
    }

    public void ClearData()
    {
        for (int i = 0; i < Stack.Count; i++)
        {
            Stack[i].CancelEffect();
        }
        Stack.Clear();
    }

    public UIEffectData GetUIEffectData()
    {
        return new UIEffectData(Stack[0].URLIcon, Count, Stack[0].workСonditionEffect.getProcentEnd, Stack[0].InfoText, Stack[0].ColorIcon, Stack[0].isBuff);
    }
}
