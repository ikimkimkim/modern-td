﻿using UnityEngine;
using System.Collections;

public class SwarmEffect : BaseEffect {


    private float MoveMultiplier, ChanceMiss;
    public override string URLIcon { get { return "EffectIcon/Swarm_prop.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Уменьшена</color> скорость перемещения, <color=#00ff00>увеличивает</color> шанс промаха атакующего"; } }

    protected override string HtmlColor { get { return "#999999"; } }
    public override bool isBuff { get { return true; } }

    public override bool isSplashUnique { get { return false; } }

    public SwarmEffect(float slow,float chanceMiss)
    {
        MoveMultiplier = 1f - slow;
        ChanceMiss = chanceMiss;
        IDStack = IDEffect.SwarmEffect;
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectRootLife(_RootUnit);
    }

    protected override void Active()
    {
        _TargetUnit.SetMoveMultiplier((int)IDStack, MoveMultiplier);
        _TargetUnit.stats[0].dodge = ChanceMiss;
    }

    protected override void Deactive()
    {
        _TargetUnit.RemoveMoveMultiplier((int)IDStack);
        _TargetUnit.stats[0].dodge = 0;
    }

    protected override void Cancel()
    {

    }

    protected override void Update()
    {

    }

    public override BaseEffect Clone()
    {
        return new SwarmEffect(1f- MoveMultiplier,ChanceMiss);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }
}
