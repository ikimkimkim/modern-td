﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class UpRangeAndChanceCritBuffEffect : BaseEffect
{
    public override bool isSplashUnique
    {
        get
        {
            throw new System.NotImplementedException("UpRangeAndChanceCritBuffEffect");
        }
    }

    public override string URLIcon { get { return "EffectIcon/CooldownAndRangeUpStunUnity.png"; } }

    public override string InfoText { get { return "Радиус атаки увеличен\r\nШанс критической атаки"; } }

    public override bool isBuff { get { return true; } }

    protected override string HtmlColor { get { return "#c0f400"; } }


    private float _rangeM, _critC, _critD;

    public UpRangeAndChanceCritBuffEffect(float rangeMultiplier, float critChance, float critDmgMultiplier)
    {
        _rangeM = rangeMultiplier;
        _critC = critChance;
        _critD = critDmgMultiplier;
        IDStack = IDEffect.RangeAndCritBuff;
    }

    public override BaseEffect Clone()
    {
        return new UpRangeAndChanceCritBuffEffect(_rangeM, _critC, _critD);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectToggleWC(true);
        for (int i = 0; i < _TargetUnit.stats.Count; i++)
        {
            _TargetUnit.stats[i].range *= _rangeM;
        }

        //_TargetUnit.myOutAttakInstanceFirstE += _TargetUnit_myOutAttakInstanceFirstE; Для 121 свойства. Крит нужен всегда.
    }

    /*private void _TargetUnit_myOutAttakInstanceFirstE(AttackInstance attackInstance)
    {
        if (Random.value < attackInstance.GetChanceCritical(_critC))
        {
            attackInstance.SetMultiplierDmgCritical(_critD);
        }
    }*/

    protected override void Cancel()
    {
        for (int i = 0; i < _TargetUnit.stats.Count; i++)
        {
            _TargetUnit.stats[i].range /= _rangeM;
        }
        //_TargetUnit.myOutAttakInstanceFirstE -= _TargetUnit_myOutAttakInstanceFirstE;
    }

    protected override void Update()
    {

    }
}
