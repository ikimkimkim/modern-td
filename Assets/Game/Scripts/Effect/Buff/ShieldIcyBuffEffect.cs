﻿using UnityEngine;
using System.Collections;
using TDTK;

public class ShieldIcyBuffEffect : ShieldMedicBuffEffect
{
    public const int IDIcyShild = 0;

    private float TimeLife;
    private float Deterioration;

    public ShieldIcyBuffEffect(float life, float fullShield) : base(fullShield)
    {
        IDStack = IDEffect.IceShieldBuff;
        TimeLife = life;
        Deterioration = fullShield / life;
        ID = IDIcyShild;
    }

    public override string URLIcon { get { return "EffectIcon/IcyShieldProperties.png"; } }

    public override string InfoText { get { return "Ледяной щит"; } }

    //protected override string HtmlColor { get { return ""; } }

    protected override void Apply()
    {

    }



    public override BaseEffect Clone()
    {
        return new ShieldIcyBuffEffect(TimeLife, deffFullShield);
    }

    protected override void Update()
    {
        if(shield.Value>0)
            shield.Value -= Deterioration * Time.deltaTime;
    }

}
