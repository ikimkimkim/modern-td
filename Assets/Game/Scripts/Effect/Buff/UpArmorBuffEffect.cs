﻿using UnityEngine;
using System.Collections;

public class UpArmorBuffEffect : BaseEffect
{
    private int MaxStack = 5;

    public override bool isSplashUnique { get { return false; } }

    public override string URLIcon { get { return "EffectIcon/SwordmanUpArmorProperties.png"; } }
    public override string InfoText { get { return "<color=#00ff00>Увеличена</color> броня";  } }

    public override bool isBuff
    {
        get { return true; }
    }

    protected override string HtmlColor
    {
        get { return "#dbb6e3"; }
    }

    private float TimeLife;
    private float Procent;

    public UpArmorBuffEffect(float procent, float time,int maxStack)
    {
        IDStack = IDEffect.UpArmorBuff;
        TimeLife = time;
        Procent = procent;
        MaxStack = maxStack;
    }

    protected override void Apply()
    {
        if (workСonditionEffect != null)
        {
            workСonditionEffect.Refresh();
        }
        else
        {
            workСonditionEffect = new EffectTimeLimiteWC(TimeLife);
        }
    }

    protected override void Active()
    {
        _TargetUnit.additiveArmor = Mathf.Clamp(_Stack.Count,0,MaxStack) * Procent;
    }

    protected override void Deactive()
    {
        _TargetUnit.additiveArmor = 0;
    }

    protected override void Cancel()
    {
        _TargetUnit.additiveArmor = 0;
    }

    protected override void Update()
    {

    }



    public override BaseEffect Clone()
    {
        return new UpArmorBuffEffect(Procent, TimeLife, MaxStack);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackRefreshActiveEffect(MaxStack);
    }
}
