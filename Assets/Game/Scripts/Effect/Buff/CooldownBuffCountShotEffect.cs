﻿using UnityEngine;
using System.Collections;

public class CooldownBuffCountShotEffect : PowerEffect
{

    public float countStackActive;
    public int MaxStacked;
    public int CountShot;


    public override string URLIcon { get { return "EffectIcon/CooldownUp.png"; } }
    public override string InfoText { get { return " <color=#00ff00>Увеличена</color> скорострельность"; } }

    protected override string HtmlColor { get { return "#fefacb"; } }
    public override bool isBuff { get { return true; } }

    public CooldownBuffCountShotEffect(float multiplier, int countShot, int maxStacked = 1, IDEffect idStack = IDEffect.CooldownBuffShotCount) : base(0)
    {
        IDStack = idStack;
        Power = multiplier;
        MaxStacked = maxStacked;
        CountShot = countShot;
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectCountShotTargetLimitWC(_TargetUnit, CountShot);
    }

    protected override void Active()
    {
        countStackActive = Mathf.Clamp(_Stack.Count, 0, MaxStacked);
        _TargetUnit.stats[_TargetUnit.currentActiveStat].cooldown /= (1 + Power * countStackActive);
    }

    protected override void Deactive()
    {
        _TargetUnit.stats[_TargetUnit.currentActiveStat].cooldown *= (1 + Power * countStackActive);
    }

    public override BaseEffect Clone()
    {
        return new CooldownBuffCountShotEffect(Power, CountShot, MaxStacked, IDStack);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackRefreshActiveEffect(MaxStacked);
    }
}
