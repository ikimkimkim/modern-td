﻿using UnityEngine;
using System.Collections;

public class CooldownBuffEffect : PowerEffect
{

    public float countStackActive;
    public int MaxStacked;

    public override string URLIcon { get { return "EffectIcon/CooldownUp.png"; } }
    public override string InfoText { get { return " <color=#00ff00>Увеличена</color> скорострельность"; } }

    public override bool isBuff { get { return true; } }
    protected override string HtmlColor { get { return "#fefacb"; } }

    public CooldownBuffEffect(float multiplier,float time, int maxStacked=1, IDEffect idStack = IDEffect.CooldownBuff) : base(time)
    {
        IDStack = idStack;
        Power = multiplier;
        MaxStacked = maxStacked;
    }

    protected override void Active()
    {
        countStackActive = Mathf.Clamp(_Stack.Count,0,MaxStacked);
        _TargetUnit.stats[_TargetUnit.currentActiveStat].cooldown /= (1 + Power * countStackActive);
        MyLog.Log("CooldownBuff Action, " + _TargetUnit.unitName + " cooldown:" + _TargetUnit.stats[_TargetUnit.currentActiveStat].cooldown);
    }

    protected override void Deactive()
    {
        _TargetUnit.stats[_TargetUnit.currentActiveStat].cooldown *= (1 + Power * countStackActive);
        MyLog.Log("CooldownBuff Deactive, " + _TargetUnit.unitName+ " cooldown:" + _TargetUnit.stats[_TargetUnit.currentActiveStat].cooldown);
    }

    public override BaseEffect Clone()
    {
        return new CooldownBuffEffect(Power, TimeLife, MaxStacked, IDStack);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackRefreshActiveEffect(MaxStacked);
    }
}
