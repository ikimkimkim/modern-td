﻿using UnityEngine;
using System.Collections;
using TDTK;

public class ShieldMedicBuffEffect : UniqueEffect
{
    public const int IDShield = 1;

    protected ShieldData shield;

    public float deffFullShield;

    public ShieldMedicBuffEffect(float fullShield) : base(IDShield)
    {
        IDStack = IDEffect.ShieldBuff;
        deffFullShield = fullShield;
    }

    public override bool isSplashUnique { get { return false; } }

    public override string URLIcon { get { return "EffectIcon/ShieldMedic.png"; } }

    public override string InfoText { get { return "Щит медика"; } }

    public override bool isBuff { get { return true; } }

    protected override string HtmlColor { get { return "#83fffc"; } }

    protected override void Apply()
    {

    }

    protected override void Active()
    {
        shield = new ShieldData(deffFullShield, _TargetUnit.armorType);
        _TargetUnit.AddShield(ID, shield);
        workСonditionEffect = new EffectShieldValueWC(shield);
    }

    protected override void Deactive()
    {
        _TargetUnit.RemoveShield(ID);
    }

    protected override void Cancel()
    {
        
    }


    protected override void Update()
    {

    }


    public override BaseEffect Clone()
    {
        return new ShieldMedicBuffEffect(deffFullShield);
    }

}
