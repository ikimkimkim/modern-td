﻿using UnityEngine;
using System.Collections;

public class UpDmgUniqueEffect : UniqueEffect
{
    private float Multiplier;

    public override string URLIcon { get { return "EffectIcon/UpDmg.png"; } }
    public override string InfoText { get { return "<color=#00ff00>Увеличен</color> исходящий урон"; } }
    
    protected override string HtmlColor { get { return "#f5947b"; } }
    public override bool isBuff { get { return true; } }

    public UpDmgUniqueEffect(float power, int index) : base(index)
    {
        Multiplier = power;
        IDStack = IDEffect.UpDmgUnique;
    }

    int active_count;
    protected override void Active()
    {
        active_count = _Stack.Count;
        for (int i = 0; i < _TargetUnit.stats.Count; i++)
        {
            _TargetUnit.stats[i].damageMin *= (1 + Multiplier * active_count);
            _TargetUnit.stats[i].damageMax *= (1 + Multiplier * active_count);
        }
    }

    protected override void Deactive()
    {
        for (int i = 0; i < _TargetUnit.stats.Count; i++)
        {
            _TargetUnit.stats[i].damageMin /= (1 + Multiplier * active_count);
            _TargetUnit.stats[i].damageMax /= (1 + Multiplier * active_count);
        }
    }

    protected override void Update()
    {
        
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneActiveEffect();
    }

    public override BaseEffect Clone()
    {
        return new UpDmgUniqueEffect(Multiplier, ID);
    }
}
