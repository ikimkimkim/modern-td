﻿using UnityEngine;
using System.Collections;

public class RegenHPBuffEffect : BaseEffect
{
    private float Multiplier;
    public override string URLIcon { get { return "EffectIcon/AyraRegenHP.png"; } }
    public override string InfoText { get { return "<color=#00ff00>Восстановление</color> здоровья"; } }

    protected override string HtmlColor { get { return "#1ae91c"; } }
    public override bool isBuff { get { return true; } }

    public override bool isSplashUnique { get { return false; } }

    public RegenHPBuffEffect(float power)
    {
        Multiplier = power;
        IDStack = IDEffect.RegenHPBuff;
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectRootLife(_RootUnit);
    }

    protected override void Active()
    {
        
    }

    protected override void Deactive()
    {

    }

    protected override void Cancel()
    {

    }

    protected override void Update()
    {
        if(_TargetUnit.HP<_TargetUnit.fullHP)
        {
            _TargetUnit.AddHP(_TargetUnit.fullHP * Multiplier * Time.deltaTime);
        }
    }

    public override BaseEffect Clone()
    {
        return new RegenHPBuffEffect(Multiplier);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }
}
