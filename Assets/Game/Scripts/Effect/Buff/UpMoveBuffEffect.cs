﻿using UnityEngine;
using System.Collections;

public class UpMoveBuffEffect : PowerEffect
{

    public override bool isSplashUnique { get { return true; } }

    public override string URLIcon { get { return "EffectIcon/UpMove.png"; } }
    public override string InfoText { get { return "<color=#00ff00>Увеличена</color> скорость перемещения"; } }

    protected override string HtmlColor { get { return "#67c498"; } }
    public override bool isBuff { get { return true; } }

    public UpMoveBuffEffect(float power, float time) : base(time)
    {
        Power = power;
        IDStack = IDEffect.UpMoveBuff;
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectNotLimiteWC();
    }

    protected override void Active()
    {
        _TargetUnit.SetMoveMultiplier((int)IDStack, 1f + Power);
    }

    protected override void Deactive()
    {
        if (_Stack.Count == 1)
        {
            _TargetUnit.RemoveMoveMultiplier((int)IDStack);
            //_TargetUnit.slowMultiplier = 1;
        }
    }

    public override BaseEffect Clone()
    {
        return new UpMoveBuffEffect(Power, TimeLife);
    }
}

