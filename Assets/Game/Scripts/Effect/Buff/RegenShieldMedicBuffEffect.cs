﻿using UnityEngine;
using System.Collections;
using TDTK;

public class RegenShieldMedicBuffEffect : BaseEffect
{
    public float Procent;
    public float Distance;

    private int IDShield;
    private ShieldData dataShield;

    public RegenShieldMedicBuffEffect(int idShield, float distance, float procent)
    {
        Procent = procent;
        Distance = distance;
        IDShield = idShield;
        IDStack = IDEffect.RegenShildBuff;
    }

    public override bool isSplashUnique { get { return false; } }

    public override string URLIcon { get { return ""; } }

    public override string InfoText { get { return "Восстановление щита медика"; } }

    public override bool isBuff { get { return false; } }

    protected override string HtmlColor { get { return "#60ccd6"; } }

    public override BaseEffect Clone()
    {
        return new RegenShieldMedicBuffEffect(IDShield, Distance, Procent);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }

    protected override void Active()
    {

    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectInRangeWC(_RootUnit, _TargetUnit, Distance);
        dataShield = _TargetUnit.getShield(IDShield);

    }

    protected override void Cancel()
    {

    }

    protected override void Deactive()
    {

    }

    protected override void Update()
    {
        if(_TargetUnit.EffectsManager.isActiveEffect(IDEffect.ShieldBuff))
        {
            dataShield.AddValue(Procent * dataShield.FullShield / Time.deltaTime);
        }
        else
        {
            CancelEffect();
        }
    }
}
