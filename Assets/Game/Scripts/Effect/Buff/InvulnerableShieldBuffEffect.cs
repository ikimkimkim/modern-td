﻿using UnityEngine;
using System.Collections;
using TDTK;

public class InvulnerableShieldBuffEffect : UniqueEffect
{
    public const int IDShield = 2;

    protected ShieldData shield;

    private float timeLife;

    public InvulnerableShieldBuffEffect(float Time) : base(IDShield)
    {
        IDStack = IDEffect.InvulnerableShieldBuff;
        timeLife = Time;
    }

    public override bool isSplashUnique { get { return false; } }

    public override string URLIcon { get { return "EffectIcon/ShieldMedic.png"; } }

    public override string InfoText { get { return "Неуязвимый щит"; } }

    public override bool isBuff { get { return true; } }

    protected override string HtmlColor { get { return "#83fffc"; } }

    protected override void Apply()
    {
        shield = new ShieldData(float.MaxValue, _TargetUnit.armorType);
        _TargetUnit.AddShield(ID, shield);
        workСonditionEffect = new EffectTimeLimiteWC(timeLife);

    }

    protected override void Active()
    {

    }

    protected override void Deactive()
    {
    }

    protected override void Cancel()
    {
        _TargetUnit.RemoveShield(ID);
    }


    protected override void Update()
    {
        shield.Value = shield.FullShield;
    }


    public override BaseEffect Clone()
    {
        return new InvulnerableShieldBuffEffect(timeLife);
    }


}
