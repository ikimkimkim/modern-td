﻿using UnityEngine;
using System.Collections;

public class DownInDmgBuffEffect : BaseEffect
{
    private float Power;
    public override string URLIcon { get { return "EffectIcon/OrcStandard_prop.png"; } }
    public override string InfoText { get { return "<color=#00ff00>Уменьшен</color> входящий урон"; } }

    protected override string HtmlColor { get { return "#dbb6e3"; } }
    public override bool isBuff { get { return true; } }

    public override bool isSplashUnique { get { return false; } }

    public DownInDmgBuffEffect(float power)
    {
        Power = power;
        IDStack = IDEffect.DownInDmgBuff;
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectRootLife(_RootUnit);
    }

    protected override void Active()
    {
        _TargetUnit.myInAttakInstanceFirstE += _TargetUnit_myInAttakInstanceFirstE;
    }

    private void _TargetUnit_myInAttakInstanceFirstE(TDTK.AttackInstance attackInstance)
    {
        attackInstance.damage *= (1f - Power);
    }

    protected override void Deactive()
    {
        _TargetUnit.myInAttakInstanceFirstE -= _TargetUnit_myInAttakInstanceFirstE;
    }

    protected override void Cancel()
    {

    }

    protected override void Update()
    {

    }

    public override BaseEffect Clone()
    {
        return new DownInDmgBuffEffect(Power);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }
}
