﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;

[RequireComponent(typeof(Rigidbody), typeof(BoxCollider))]
public class ZoneSourceEffect : MonoBehaviour {


    public Dictionary<Unit, EffectToggleWC> listUnit;

    public float Power;

	void OnEnable ()
    {
        if (listUnit == null)
            listUnit = new Dictionary<Unit, EffectToggleWC>();
        else
            listUnit.Clear();
	}

    Unit unit;
    void OnTriggerEnter(Collider cols)
    {
        unit = cols.GetComponent<Unit>();
        if (unit == null || unit.prefabID==13 || unit.IsCreep == false || unit.GetUnitCreep.flying == true)
            return;

        if (listUnit.ContainsKey(unit))
            return;


        UpMoveBuffEffect effect = new UpMoveBuffEffect(Power, 1);
        EffectToggleWC effectWC = new EffectToggleWC(true);
        unit.EffectsManager.AddEffect(null, effect);
        effect.workСonditionEffect = effectWC;

        listUnit.Add(unit,effectWC);
    }

    void OnTriggerExit(Collider cols)
    {
        unit = cols.GetComponent<Unit>();
        if (unit == null || unit.prefabID == 13)
            return;
        if(listUnit.ContainsKey(unit))
        {
            listUnit[unit].Destroy();
        }
    }


    void OnDisable()
    {
        foreach (var k in listUnit)
        {
            k.Value.Destroy();
        }
        listUnit.Clear();
    }
}
