﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowStackedControlEffect : BaseEffect
{
    private const int _MaxStack = 10;
    public static int MaxStack
    {
        get
        {
            return _MaxStack;
        }
    }

    private float Power, TimeLife;

    public override bool isSplashUnique { get { return true; } }

    public override string URLIcon { get { return "EffectIcon/Slow.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Уменьшена</color> скорость передвижения от заклинаний"; } }

    protected override string HtmlColor { get { return "#5f8cc8"; } }
    public override bool isBuff { get { return false; } }

    public SlowStackedControlEffect(float slow, float time) 
    {
        Power = slow;
        IDStack = IDEffect.SlowControlStacked;
        TimeLife = time;
        if (Power > 1)
            Debug.LogWarningFormat("SlowControl Effect {0} power greade 1!", IDStack);
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectTimeLimiteWC(TimeLife * _TargetUnit.slowTimeIncrement);        
    }

    protected override void Active()
    {
        Debug.LogWarning(1f - Power * Mathf.Clamp(_Stack.Count, 1, _MaxStack) * _TargetUnit.slowIncrement);
        _TargetUnit.SetMoveMultiplier((int)IDStack, 1f - Power * _Stack.Count * _TargetUnit.slowIncrement);
        _TargetUnit.ShowSlowEffetc();
    }

    protected override void Deactive()
    {
        _TargetUnit.RemoveMoveMultiplier((int)IDStack);
        _TargetUnit.HideSlowEffect();
    }

    public override BaseEffect Clone()
    {
        return new SlowStackedControlEffect(Power, TimeLife);
    }

    protected override void Update()
    {

    }

    protected override void Cancel()
    {

    }

    public override IStackEffect GetStackEffect()
    {
        return new StackRefreshActiveEffect(MaxStack);
    }
}
