﻿using UnityEngine;
using System.Collections;

public class StunControlEffect : PowerEffect
{
    public override bool isSplashUnique { get { return CloneSplash; } }

    protected bool CloneSplash;

    public override string URLIcon { get { return "EffectIcon/Stun.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Оглушение</color>"; } }

    protected override string HtmlColor { get { return "#fff003"; } }
    public override bool isBuff { get { return false; } }

    public StunControlEffect(float time,bool cloneInSplash=true) : base(time)
    {
        CloneSplash = cloneInSplash;
        IDStack = IDEffect.StunControl;
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectTimeLimiteWC(TimeLife * _TargetUnit.stunTimeIncrement);
        if (_TargetUnit.dead)
            return;

        _TargetUnit.AddStun((int)IDStack);
        if (_TargetUnit._Agent != null && _TargetUnit.IsCreep && _TargetUnit.GetUnitCreep.flying == false)
            _TargetUnit._Agent.isStopped = true;
        _TargetUnit.ShowStunEffetc();
    }

    protected override void Active()
    {
    }

    protected override void Deactive()
    {
    }

    protected override void Cancel()
    {
        if (_Stack.Count == 1)
        {
            _TargetUnit.RemoveStun((int)IDStack);
            _TargetUnit.HideStunEffect();
        }
    }
    
    public override IStackEffect GetStackEffect()
    {
        return new StackDotEffect();
    }

    public override BaseEffect Clone()
    {
        return new StunControlEffect(TimeLife,CloneSplash);
    }
}
