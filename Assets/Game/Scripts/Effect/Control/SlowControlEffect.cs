﻿using UnityEngine;
using System.Collections;

public class SlowControlEffect : PowerEffect
{

    public override bool isSplashUnique { get { return true; } }

    public override string URLIcon { get { return "EffectIcon/Slow.png";  } }
    public override string InfoText { get { return "<color=#ff0000>Уменьшена</color> скорость передвижения"; } }

    protected override string HtmlColor { get { return "#5f8cc7"; } }
    public override bool isBuff { get { return false; } }

    public SlowControlEffect(float slow, float time, IDEffect type = IDEffect.SlowControl) : base(time)
    {
        Power = slow;
        IDStack = type;
        if (Power > 1)
            Debug.LogWarningFormat("SlowControl Effect {0} power greade 1!",type);
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectTimeLimiteWC(TimeLife * _TargetUnit.slowTimeIncrement);
    }

    protected override void Active()
    {

        _TargetUnit.SetMoveMultiplier((int)IDStack, 1f - Power * _TargetUnit.slowIncrement);
        //_TargetUnit.slowMultiplier = 1 - Power*_TargetUnit.slowIncrement;


        _TargetUnit.ShowSlowEffetc();
    }

    protected override void Deactive()
    {
        if (_Stack.Count == 1)
        {
            _TargetUnit.RemoveMoveMultiplier((int)IDStack);
            //_TargetUnit.slowMultiplier = 1;
            _TargetUnit.HideSlowEffect();
        }
    }

    public override BaseEffect Clone()
    {
        return new SlowControlEffect(Power, TimeLife);
    }
}
