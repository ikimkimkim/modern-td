﻿using UnityEngine;
using System.Collections;
using TDTK;

public class MadnessControlEffect : BaseEffect
{
    public override bool isSplashUnique { get { return false; } }

    public override string URLIcon { get { return ""; } }

    public override string InfoText { get { return "Безумие"; } }

    public override bool isBuff { get { return false; } }

    protected override string HtmlColor { get { return "#000000"; } }

    private float  time, hpDmg, hpRegen;

    private enum eStatus { serch, attack }
    private eStatus status;
    private LayerMask mask;

    public MadnessControlEffect(float Time, float HpDmg, float HpRegen)
    {
        time = Time;
        hpDmg = HpDmg;
        hpRegen = HpRegen;
        IDStack = IDEffect.MadnessDebuff;
        status = eStatus.serch;
        mask = 1 << LayerManager.LayerCreep() | 1 << LayerManager.LayerCreepF();
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectTimeLimiteWC(time);
        StartPos = _TargetUnit.getPosition;
        _TargetUnit.lockMovePach = true;
        _TargetUnit.myOutAttakInstanceInitE += InitAttak;
    }

    private void InitAttak(AttackInstance attackInstance)
    {
        if (attackInstance.tgtUnit is UnitCreep)
        {
            attackInstance.damage = _TargetUnit.fullHP * hpDmg;
            if(attackInstance.tgtUnit.EffectsManager.isActiveEffect(IDEffect.MadnessDebuff)==false && workСonditionEffect.getProcentEnd<0.9f)
                attackInstance.listEffects.Add(new MadnessControlEffect(time*(1f-workСonditionEffect.getProcentEnd), hpDmg, hpRegen));
            _TargetUnit.AddHeal(attackInstance.damage * hpRegen);
        }
    }

    protected override void Update()
    {
        switch (status)
        {
            case eStatus.serch:
                SerchUpdate();
                break;
            case eStatus.attack:
                if (_TargetUnit.target == null || _TargetUnit.target.IsCreep == false || _TargetUnit.target.dead)
                {
                    status = eStatus.serch;
                    _TargetUnit.lockMovePach = true;
                }
                break;
        }
    }

    Collider[] colsAOE;
    private void SerchUpdate()
    {
        colsAOE = Physics.OverlapSphere(_TargetUnit.getPosition, _TargetUnit.GetRange(), mask);
        if (colsAOE.Length > 0)
        {
            _TargetUnit.target = colsAOE[Random.Range(0, colsAOE.Length)].GetComponent<Unit>();
            if (_TargetUnit.target != _TargetUnit)
            {
                status = eStatus.attack;
                _TargetUnit.lockMovePach = false;
                return;
            }
            else
                _TargetUnit.target = null;
        }

        MoveUpdate();
    }

    private bool oldStun;
    private Vector3 StartPos, TargetPos;
    private void MoveUpdate()
    {
        if (/*_TargetUnit.isMoveEnd() ||*/ Vector3.Distance(TargetPos, _TargetUnit.getPosition) < _TargetUnit.SizeMyCollider
            || oldStun != _TargetUnit.stunned)
        {
            oldStun = _TargetUnit.stunned;
            if (_TargetUnit.dead || _TargetUnit._Agent == null || _TargetUnit.stunned)
                return;

            TargetPos = StartPos + _TargetUnit.thisT.forward.Rotation(Random.Range(0, 360)) * 2f;
            _TargetUnit._Agent.isStopped = false;
            _TargetUnit._Agent.SetDestination(TargetPos);
        }
    }

    protected override void Cancel()
    {
        switch (status)
        {
            case eStatus.serch:
                _TargetUnit.lockMovePach = false;
                break;
            case eStatus.attack:
                _TargetUnit.target = null;
                break;
        }

        _TargetUnit.myOutAttakInstanceInitE -= InitAttak;
    }

    public override BaseEffect Clone()
    {
        return new MadnessControlEffect(time,hpDmg,hpRegen);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }
}
