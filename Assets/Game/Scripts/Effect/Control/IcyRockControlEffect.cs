﻿using UnityEngine;
using System.Collections;

public class IcyRockControlEffect : BaseEffect {

    public override bool isSplashUnique { get { return true; } }

    

    public override string URLIcon { get { return "EffectIcon/IcyRockControlEffect.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Оглушение</color>"; } }

    protected override string HtmlColor { get { return "#fff003"; } }
    public override bool isBuff { get { return false; } }

    private float  deffStunTime, StunTime, SlowTime;

    private float TimeActive, TimeMove;

    private GameObject RockObj, initRockObj;

    bool inRock;

    public IcyRockControlEffect(float stunTime, float slowTime)
    {
        deffStunTime = stunTime;
        SlowTime = slowTime;

        IDStack = IDEffect.IcyRockControlDebaff;

        RockObj = EffectDB.Load()[12];

    }

    protected override void Apply()
    {
        StunTime = deffStunTime * _TargetUnit.stunTimeIncrement;
    }

    protected override void Active()
    {
        if (_TargetUnit.dead)
            return;

        workСonditionEffect = new EffectNotLimiteWC();
        TimeActive = Time.time;

        _TargetUnit.AddStun((int)IDStack);
        if (_TargetUnit._Agent != null && _TargetUnit.IsCreep && _TargetUnit.GetUnitCreep.flying == false)
            _TargetUnit._Agent.isStopped = true;

        initRockObj = ObjectPoolManager.Spawn(RockObj, _TargetUnit.getPosition, _TargetUnit.thisT.rotation);
        inRock = true;
    }

    protected override void Update()
    {
        if (inRock)
        {
            if (TimeActive + StunTime < Time.time)
            {
                workСonditionEffect = new EffectTimeLimiteWC(SlowTime);

                _TargetUnit.SetMoveMultiplier((int)IDStack, 0);
                TimeMove = Time.time;
                _TargetUnit.RemoveStun((int)IDStack);
                inRock = false;
                ObjectPoolManager.Unspawn(initRockObj);
            }
        }
        else
        {
            _TargetUnit.SetMoveMultiplier((int)IDStack, Mathf.Clamp((Time.time - TimeMove) / SlowTime,0f,1f));
        }

    }

    protected override void Deactive()
    {
        if (initRockObj!=null)
            ObjectPoolManager.Unspawn(initRockObj);

        _TargetUnit.RemoveMoveMultiplier((int)IDStack);
    }

    protected override void Cancel()
    {
        _TargetUnit.RemoveStun((int)IDStack);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }

    public override BaseEffect Clone()
    {
        return new IcyRockControlEffect(StunTime, SlowTime);
    }

}
