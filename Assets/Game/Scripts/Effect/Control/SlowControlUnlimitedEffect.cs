﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowControlUnlimitedEffect : SlowControlEffect {

	public SlowControlUnlimitedEffect(float slow, IDEffect type = IDEffect.SlowControlLevelModification) : base(slow,0,type)
    {
        
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectNotLimiteWC();
    }

    public override BaseEffect Clone()
    {
        return new SlowControlUnlimitedEffect(Power, IDStack);
    }
}
