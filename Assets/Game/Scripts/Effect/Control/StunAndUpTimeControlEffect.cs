﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunAndUpTimeControlEffect : StunControlEffect
{
    private float _timeUp, _maxTimeUp, _countUpTime, counter;

    public StunAndUpTimeControlEffect(float time, float timeUp, float maxTimeUp, bool cloneInSplash = true) : base(time,cloneInSplash)
    {
        _timeUp = timeUp;
        _maxTimeUp = maxTimeUp;
        _countUpTime = maxTimeUp/timeUp;
        IDStack = IDEffect.LightningStunControl;
    }

    protected override void Apply()
    {
        counter = 0;
        workСonditionEffect = new EffectTimeLimiteWC(TimeLife * _TargetUnit.stunTimeIncrement);
        _TargetUnit.myInAttakInstanceInitE += _TargetUnit_myInAttakInstanceInitE;
    }

    private void _TargetUnit_myInAttakInstanceInitE(TDTK.AttackInstance attackInstance)
    {
        if(counter<_countUpTime && attackInstance.getDamageType==(int)TDTK.enumDamageType.Energetic)
        {
            counter++;
            (workСonditionEffect as EffectTimeLimiteWC).TimeLife += _timeUp;
        }
    }

    protected override void Cancel()
    {
        if (_Stack.Count == 1)
        {
            _TargetUnit.RemoveStun((int)IDStack);
            _TargetUnit.HideStunEffect();
        }
        _TargetUnit.myInAttakInstanceInitE -= _TargetUnit_myInAttakInstanceInitE;
    }


    public override BaseEffect Clone()
    {
        return new StunAndUpTimeControlEffect(TimeLife, _timeUp, _maxTimeUp, CloneSplash);
    }
}