﻿using UnityEngine;
using System.Collections;

public class DisorientationControlEffect : PowerEffect
{
    public static int counter;
    public int id;

    bool isActive;
    float ChanceConfuse;
    bool Confuse;

    public override bool isSplashUnique { get { return true; } }
    public override string URLIcon { get { return "EffectIcon/Disorientation.png"; } }
    public override string InfoText { get { return "<color=#ff0000>Уменьшена</color> скорость передвижения \r\n Возможна потеря направления движения"; } }

    protected override string HtmlColor { get { return "#fba81d"; } }
    public override bool isBuff { get { return false; } }

    public override float Power { get { return _power + (Confuse ? 1 : 0); } set { _power = value; } }

    private float _power;

    bool oldStun;
    public DisorientationControlEffect(float chanceConfuse, float slow, float time) : base(time)
    {
        //Debug.Log("DisorientationControlEffect INIT!");
        Power = slow;
        ChanceConfuse = chanceConfuse;
        IDStack = IDEffect.DisorientationControl;
        id = counter;
        counter++;
    }

    Vector3 StartPos,BufPos;
    //int LastSubWaypointID;

    protected override void Apply()
    {
        isActive = false;
        Confuse = false;
        base.Apply();
        if (Random.Range(0f, 1f) < ChanceConfuse)
        {
            Confuse = true;
        }
    }

    protected override void Active()
    {
        if (isActive)
            return;

        _TargetUnit.SetMoveMultiplier((int)IDStack, 1f-Power);

        if(_TargetUnit.stats.Count > 0)
            _TargetUnit.stats[_TargetUnit.currentActiveStat].cooldown *= (1 + _power);

        //_TargetUnit.disorientationMultiplier = 1 - _power;

       /*Не понятно зачем так как обновление и так происходит в FixedUpdate 
         if (_TargetUnit.IsCreep)
            _TargetUnit.GetUnitCreep.UpdateMoveSpeedAgent();*/

        isActive = true;
        if (Confuse)
        {
           // MyLog.Log("DisorientationControlEffect Action Confuse "+id);
            _TargetUnit.lockMovePach = true;
            StartPos = _TargetUnit.getPosition;
            SetMovePoint();
        }
        //else
        //    MyLog.Log("DisorientationControlEffect Action not Confuse " + id);
    }

    protected override void Update()
    {
        if (Confuse == false || isActive == false || _TargetUnit.dead)
            return;

        BufPos.y = _TargetUnit.getPosition.y;
        if (/*_TargetUnit.isMoveEnd() ||*/ Vector3.Distance(BufPos, _TargetUnit.getPosition)<_TargetUnit.SizeMyCollider 
            || oldStun!=_TargetUnit.stunned)
        {
            SetMovePoint();
        }
    }

    Vector3 point;
    float ang;
    private void SetMovePoint()
    {
        oldStun = _TargetUnit.stunned;
        if (_TargetUnit.dead || _TargetUnit._Agent == null || _TargetUnit.stunned)
            return;


        //MyLog.Log("DisorientationControlEffect SetMovePoint");
        ang = Random.Range(0, 360) ;
        BufPos = StartPos + _TargetUnit.thisT.forward.Rotation(ang)*3f* _TargetUnit.SizeMyCollider;
        _TargetUnit._Agent.isStopped = false;
        _TargetUnit._Agent.SetDestination(BufPos);
        //BufPos = _TargetUnit._Agent.pathEndPosition;
    }


    protected override void Deactive()
    {
        if (isActive == false)
            return;

        if (_Stack.Count == 1)
            _TargetUnit.RemoveMoveMultiplier((int)IDStack);
        //_TargetUnit.disorientationMultiplier = 1;

        if (_TargetUnit.stats.Count > 0)
            _TargetUnit.stats[_TargetUnit.currentActiveStat].cooldown /= (1 + _power);

        if (_TargetUnit.dead == false && Confuse)
        {
            //MyLog.Log("DisorientationControlEffect Cancel MoveToPointSubPath " + id);
            //_TargetUnit.GetUnitCreep.MoveToPointSubPath(LastSubWaypointID);
            _TargetUnit.lockMovePach = false;
        }
        /*else
            MyLog.Log("DisorientationControlEffect Cancel " + id);*/

        isActive = false;
    }

    public override BaseEffect Clone()
    {
        return new DisorientationControlEffect(ChanceConfuse, Power, TimeLife);
    }
}
