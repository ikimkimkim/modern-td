﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunNextDisoriesntationControlEffect : StunControlEffect
{

    private float _chanceConfuse, _slow, _timeDisorientation;

    public StunNextDisoriesntationControlEffect(float time, float chanceConfuse, float slow, float timeDisorientation, bool cloneInSplash = true)
        : base(time, cloneInSplash)
    {
        _chanceConfuse = chanceConfuse;
        _slow = slow;
        _timeDisorientation = timeDisorientation;
        IDStack = IDEffect.LightningStunControl;
    }

    protected override void Cancel()
    {
        if (_Stack.Count == 1)
        {
            _TargetUnit.RemoveStun((int)IDStack);
            _TargetUnit.HideStunEffect();
            _TargetUnit.EffectsManager.AddEffect(_RootUnit,
                new DisorientationControlEffect(_chanceConfuse, _slow, _timeDisorientation));
        }
    }

    public override BaseEffect Clone()
    {
        return new StunNextDisoriesntationControlEffect(TimeLife, _chanceConfuse, _slow, _timeDisorientation, CloneSplash);
    }
}
