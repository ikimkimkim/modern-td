﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class ElectrifiedEffect : BaseEffect
{
    public override bool isSplashUnique
    {
        get
        {
            return false;
        }
    }

    public override string URLIcon { get { return ""; } }

    public override string InfoText { get { return "Заряженный"; } }

    public override bool isBuff { get { return false; } }

    protected override string HtmlColor { get { return "#33cccc"; } }

    private float _speed, _time, _dmg;
    private GameObject effect;

    public ElectrifiedEffect(float speed, float time,float dmg)
    {
        IDStack = IDEffect.ElectrifiedDebuff;
        _speed = speed;
        _time = time;
        _dmg = dmg;
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectTimeLimiteWC(_time);
        effect = DataBase<GameObject>.LoadIndex(DataBase<GameObject>.NameBase.SpecialEffects,0);
        _TargetUnit.SetMoveMultiplier((int)IDStack, 1f + _speed);
    }

    protected override void Update()
    {

    }

    protected override void Cancel()
    {
        _TargetUnit.RemoveMoveMultiplier((int)IDStack);
        _TargetUnit.ApplyDamage(_RootUnit, _dmg, (int)enumDamageType.Energetic,false);
        if(effect!=null)
            ObjectPoolManager.Spawn(effect, _TargetUnit.getPosition, Quaternion.identity);
    }


    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }

    public override BaseEffect Clone()
    {
        return new ElectrifiedEffect(_speed, _time, _dmg);
    }
}
