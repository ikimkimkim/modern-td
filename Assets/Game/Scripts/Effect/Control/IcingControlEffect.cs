﻿using UnityEngine;
using System.Collections;

public class IcingControlEffect : BaseEffect
{
    public override bool isSplashUnique
    {
        get { return false; }
    }

    public override string URLIcon { get { return ""; } }

    public override string InfoText { get
        {
            if (isStunComplite)
                return "<color=#ff0000>Заморожен</color>";
            else
                return "<color=#ff0000>Увеличен</color> входящий урон всех типов";
        }
    }

    public override bool isBuff { get { return false; } }

    protected override string HtmlColor { get { return "#1718d6"; } }

    private float timeStun, timeUpDmg, upDmg;
    private float timeActive;
    private bool isStunComplite;
    public IcingControlEffect(float TimeStun, float TimeUpDmg, float ProcentUpDmg)
    {
        IDStack = IDEffect.IcingControlDebaff;
        timeStun = TimeStun;
        timeUpDmg = TimeUpDmg;
        upDmg = ProcentUpDmg;
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectTimeLimiteWC(timeStun + timeUpDmg);
        timeActive = Time.time;
        ActiveStun();
    }

    private void ActiveStun()
    {
        isStunComplite = false;
        _TargetUnit.AddStun((int)IDStack);
        _TargetUnit._Agent.isStopped = true;
    }

    private void DeactiveStun()
    {
        isStunComplite = true;
        _TargetUnit.RemoveStun((int)IDStack);
        if(_TargetUnit.stunned==false)
            _TargetUnit._Agent.isStopped = false;
    }

    protected override void Update()
    {
        if(isStunComplite == false && Time.time >= timeActive + timeStun)
        {
            DeactiveStun();
            _TargetUnit.myInAttakInstanceFirstE += _TargetUnit_myInAttakInstanceFirstE;
        }
    }

    private void _TargetUnit_myInAttakInstanceFirstE(TDTK.AttackInstance attackInstance)
    {
        attackInstance.damage *= (1f + upDmg);
    }

    protected override void Cancel()
    {
        if(isStunComplite==true)
        {
            _TargetUnit.myInAttakInstanceFirstE -= _TargetUnit_myInAttakInstanceFirstE;
        }
    }


    public override BaseEffect Clone()
    {
        return new IcingControlEffect(timeStun, timeUpDmg, upDmg);
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackOneEffect();
    }
}
