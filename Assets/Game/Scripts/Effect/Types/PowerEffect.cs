﻿using UnityEngine;
using System.Collections;

public class PowerEffect : BaseEffect {
    

    public float TimeLife;

    public virtual float Power{ get; set; }

    public override bool isSplashUnique { get { throw new System.NotImplementedException(); } }

    public override string URLIcon { get { throw new System.NotImplementedException("URLIcon"); } }
    public override string InfoText { get { throw new System.NotImplementedException("InfoText"); } }

    protected override string HtmlColor { get { throw new System.NotImplementedException("get HtmlColor"); } }
    public override bool isBuff { get { throw new System.NotImplementedException("isBuff"); } }

    public PowerEffect(float time)
    {
        TimeLife = time;
    }
    
    protected override void Apply()
    {
        workСonditionEffect = new EffectTimeLimiteWC(TimeLife);
    }

    protected override void Cancel()
    {

    }

    protected override void Update()
    {

    }

    protected override void Active()
    {
        throw new System.NotImplementedException();
    }

    protected override void Deactive()
    {
        throw new System.NotImplementedException();
    }

    public override IStackEffect GetStackEffect()
    {
        return new StackMaxPowerEffect();
    }

    public override BaseEffect Clone()
    {
        throw new System.NotImplementedException();
    }
}
