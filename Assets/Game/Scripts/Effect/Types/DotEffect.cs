﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;

public class DotEffect : BaseEffect
{

    public float countTik;
    public float interval;
    public float damage;
    public int typeDmg;


    private float TimeLastTik;
    private int countTikNow;

    public override bool isSplashUnique { get { return true; } }

    public override string URLIcon { get { throw new System.NotImplementedException("URLIcon"); } }
    public override string InfoText { get { return "Переодически получает урон"; } }

    protected override string HtmlColor { get { throw new System.NotImplementedException("get HtmlColor"); } }
    public override bool isBuff { get { return false; } }

    //PropertiesAttak attakP;


    public DotEffect(float CountTik,float Interval,float Dmg, int TypeDmg) : base()
    {
        countTik = CountTik;
        countTikNow = 0;
        interval = Interval;
        damage = Dmg;
        typeDmg = TypeDmg;

        //attakP = new PropertiesAttak();
        //attakP.damageType = TypeDmg;
        //attakP.stats[0].damageMax = attakP.stats[0].damageMin = Dmg;
    }

    protected override void Apply()
    {
        workСonditionEffect = new EffectTimeLimiteWC(interval * countTik);
        TimeLastTik = Time.time;
    }

    protected override void Cancel()
    {

    }

    protected override void Update()
    {
        if(Time.time - TimeLastTik >= interval)
        {
            Dotted();
        }
    }

    public virtual void Dotted()
    {
        if (_TargetUnit == null)
            return;

        countTikNow++;
        TimeLastTik += interval;

        _TargetUnit.ApplyDamage(_RootUnit, damage, typeDmg, false);


    }

    public override IStackEffect GetStackEffect()
    {
        return new StackDotEffect();
    }

    public override BaseEffect Clone()
    {
        DotEffect effect = new DotEffect(countTik, interval, damage, typeDmg);
        effect.IDStack = IDStack;
        return effect;
    }
}
