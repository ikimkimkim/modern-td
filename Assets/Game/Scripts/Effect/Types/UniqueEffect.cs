﻿using UnityEngine;
using System.Collections;

public abstract class UniqueEffect : BaseEffect
{
    public int ID { get; protected set; }

    public override bool isSplashUnique { get { throw new System.NotImplementedException(); } }

    public override string URLIcon { get { throw new System.NotImplementedException("URLIcon"); } }
    public override string InfoText { get { throw new System.NotImplementedException("InfoText"); } }

    protected override string HtmlColor { get { throw new System.NotImplementedException("get HtmlColor"); } }
    public override bool isBuff { get { throw new System.NotImplementedException("isBuff"); } }

    public UniqueEffect(int index)
    {
        ID = index;
    }


    protected override void Apply()
    {
        workСonditionEffect = new EffectRootLife(_RootUnit);
    }

    protected override void Cancel()
    {

    }


    public override IStackEffect GetStackEffect()
    {
        return new StackUniqueEffect();
    }

}
