﻿using System.Collections.Generic;
using TDTK;
using UnityEngine;

public interface IWorkСonditionEffect
{
    void Refresh();
    bool WorkConditionFulfilled { get; }
    void Destroy();
    float getProcentEnd { get; }
}

public enum IDEffect
{
    none = -1,
    FireDot,
    PoisonDot,
    BleedingDot,
    SlowControl,
    SlowControlStacked,
    SlowControlLevelModification,
    SlowControlPoisonAb,
    StunControl,
    LightningStunControl,
    DisorientationControl,
    CooldownBuff,
    CooldownBuffStecked,
    CooldownBuffShotCount,
    WeaknessDebuff,
    VulnerabilityDebuff,
    MalisonDebuff,
    UpDmgUnique,
    DownDmgUnique,
    DownInDmgBuff,
    UpCooldownDebufUnique,
    Shock,
    IcyShock,
    MagicShock,
    Frostbite,
    UpMoveBuff,
    UpCostDebaff,
    AddManaForDeadDebaff,
    UpArmorBuff,
    ShieldBuff,
    IceShieldBuff,
    RegenShildBuff,
    RegenHPBuff,
    IcyRockControlDebaff,
    SwarmEffect,
    IcingControlDebaff,
    MadnessDebuff,
    InvulnerableShieldBuff,
    ExplosionAfterDeathDebuff,
    ExplosionTimerDebuff,
    RangeAndCritBuff,
    MarkLightDamageTowerDebuff,
    MarkDamageHeroDebuff,
    ElectrifiedDebuff,
    ElectrifiedAyraDebuff,
}

public interface IStackEffect
{
    void Add(Unit root, Unit target, BaseEffect effect);
    void Remove(BaseEffect effect);
    void Update();
    int Count { get; }
    bool isBuff { get; }
    Color getColor { get; }
    List<BaseEffect> getAllEffect();
    void ClearData();

    UIEffectData GetUIEffectData();
}