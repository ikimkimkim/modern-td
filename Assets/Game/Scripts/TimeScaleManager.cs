﻿using UnityEngine;
using System.Collections;

public class TimeScaleManager : MonoBehaviour
{
    public delegate void ChangeTimeScale();
    public static event ChangeTimeScale onChangeTimeScale;

    public static float BoostIncrement=1f;
    
    private static bool isBoost = false;

    private static float TimeBoost= 2f;
    public static float GetTimeBoost { get { return TimeBoost; } }
    private static float TimeScaleNow = 1;
    public static float TimeScale { get { return TimeScaleNow; } }

    public static void BoostTime()
    {
        if (isBoost == false)
            TimeScaleNow = TimeBoost;
        else
            TimeScaleNow = 1;
        isBoost = !isBoost;

        SetTimeScale(TimeScaleNow);
    }

    public static void SetTimeScale(float scale)
    {
        //Debug.Log("TimeScale:" + scale);
        isBoost = scale == TimeBoost;
        if (scale == 1)
            TimeScaleNow = scale;
        Time.timeScale = scale;
        if (onChangeTimeScale != null)
            onChangeTimeScale();
    }

    public static void RefreshTimeScale()
    {
        SetTimeScale(TimeScaleNow);
    }


   /* public static IEnumerator WaitForRealSeconds(float time)
    {
        float start = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < start + time)
        {
            yield return null;
        }
    }*/
}
