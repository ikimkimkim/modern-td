﻿
/// <summary>
/// Информация о пользователе вк
/// </summary>
public class SocialData
{
    public RequestData Data;
    public VKUserData UserInfo;

    private string _formatNameCache;
    public SocialData(RequestData data, VKUserData info)
    {
        Data = data;
        UserInfo = info;
    }
    public string FormatName
    {
        get
        {
            if (string.IsNullOrEmpty(_formatNameCache))
            {
                _formatNameCache = UserInfo.name;
                if (_formatNameCache.Length > 20)
                {
                    _formatNameCache = _formatNameCache.Remove(17) + "...";
                }
            }

            return _formatNameCache;
        }
    }
}