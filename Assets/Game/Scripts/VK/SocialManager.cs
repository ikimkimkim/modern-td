﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// Управляет взаимодействие с вк и стартом приложения в вебгл
/// </summary>
public class SocialManager : MonoBehaviour
{
    //Image StartButton;
    SocialData _data;
    //Image _playerPic;
    //Text _playerName;
    //Sprite _loadedPic;
    PlayerManager _player;
    public ServerManager _server {  get; private set; }
    public bool ShowOffersButton = false;
    
    public Action actionLoadFriends;

    public static BuyConfigData buyConfig;
    public static BuyCystalConfigData buyCrystalConfig;
    public static Dictionary<string, ChestPrizesData> chest_info_conf;
    private static DateTime _timeServer;

    public static DateTime StartServerTime { get { return _timeServer; } }


    public int getVKUserID { get { return _data.UserInfo.id; } }

    
    public static SocialManager Instance { get; private set; }


    void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;

            var playerInfo = GameObject.FindGameObjectWithTag("PlayerInfo");
            _player = playerInfo.GetComponent<PlayerManager>();
            _server = playerInfo.GetComponent<ServerManager>();
        }
    }

    void Start()
    {
        //StartButton = GameObject.Find("StartGame").GetComponent<Image>();
        //_playerPic = GameObject.FindGameObjectWithTag("PlayerPic").GetComponent<Image>();
        //_playerName = GameObject.FindGameObjectWithTag("PlayerName").GetComponent<Text>();
#if UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR
       // StartButton.color = new Color(StartButton.color.r, StartButton.color.g, StartButton.color.b, 1f);
       // StartButton.GetComponent<Button>().interactable = true;
#endif

#if (UNITY_WEBGL || UNITY_WEBPLAYER)
        Application.ExternalCall("getUserData");

        LoadFriendsPlayer();


        //Используется для теста нормального запуска приложения в вебгл
        //#if UNITY_EDITOR
        //        userData(FakeData);
        //#endif
#endif
    }

    public void LoadFriendsPlayer()
    {
        Debug.Log("load Friands Palyer");
        StartCoroutine(_server.SaveLoadServer("friends", new List<StringPair>(), "", value => CallBackFriends(value, _player)));
    }

    void CallBackFriends(string value, PlayerManager player)
    {
        Debug.Log("CallBackFriends:" + value);
        if(value.Length>1)
        player.SetFiendsData((FriendsData)StringSerializationAPI.Deserialize(typeof(FriendsData), value));
        if (actionLoadFriends != null)
            actionLoadFriends();
    }



    private void Write(string mes)
    {
        Debug.Log("SM->" + mes);
    }
    public void GetOfferDate()
    {
        Application.ExternalCall("isAnyOffers");
    }
    public void OrderOffer()
    {
        Application.ExternalCall("orderOffer");
    }




    //Вызываются из index.html
    #region CALLBACKS

    public void userData(string data)
    {
        Debug.Log("SM->userData:" + data);
        //Application.ExternalCall("BrowserLog", "CALLBACK!!!!!!!!!!!" + data);

        RecievedData recieved = new RecievedData();
        recieved = (RecievedData)StringSerializationAPI.Deserialize(recieved.GetType(), data);

        Write(StringSerializationAPI.Serialize(typeof(RecievedData), recieved));


        _data = new SocialData(recieved.request_data, recieved.vk_user);

        //_playerName.text = _data.FormatName;
        // Debug.Log("PLAYER DATA FIRST SET" + Time.timeSinceLevelLoad);
        _server.SetRequsetData(recieved.request_data);
        buyConfig = recieved.buy_conf;
        buyCrystalConfig = recieved.buy_crystals_conf;
        chest_info_conf = recieved.chest_info_conf;

#if UNITY_EDITOR
        _timeServer = DateTime.Now;
#else
        _timeServer = (new DateTime(1970, 1, 1, 0, 0, 0, 0)).AddSeconds(recieved.time);
        
#endif
        Debug.Log(_timeServer.ToShortDateString());

        _player.InitCraftCardConfig(recieved.craft_cards_conf);
        _player.InitCardPriceSellConfig(recieved.sell_price_conf);

        _player._SetPlayerData(recieved.user_info);
        _player.crystals = recieved.buy_conf.crystals;
        _player.BuyLocations = recieved.buy_conf.locations;
        
        //BonusManager.instance.setData(recieved.daily_bonus, recieved.daily_bonus_ttl);
        
        //Debug.Log("SM->set id fruends:" + recieved.friends_ids);
        //_player._friendsData = new FriendsData(recieved.friends_ids);
        
       /* StartButton.color = new Color(StartButton.color.r, StartButton.color.g, StartButton.color.b, 1f);
        StartButton.GetComponent<Button>().interactable = true;*/
        GetOfferDate();


        //   Application.ExternalCall("BrowserLog", StringSerializationAPI.Serialize(recieved.GetType(), recieved));
        //   Write(StringSerializationAPI.Serialize(recieved.GetType(), recieved));
    }
    /// <summary>
    /// Вызывется если покупка прошла успешно
    /// </summary>
    /// <param name="result"></param>
    public void OrderResult(string result)
    {
        if (result == "ok")
        {
            StartCoroutine(_server.SaveLoadServer("loadinfo", new List<StringPair>(), "", value =>
            {
                Debug.Log("SM-> loadinfo");
                _player._SetPlayerData(((UserData)StringSerializationAPI.Deserialize(_player.GetPlayerDataType(), value)).user);
            }));
        }
    }
    public void OffersCount(string result)
    {
        //Debug.Log("Offers = " + result);
        if (result != "0")
        {
            ShowOffersButton = true;
        }
    }

    public void rewardedVideoEvent(string data)
    {
        Debug.Log("rew video data:" + data);
        ChestManager.instance.ChestPanel.ResultShowVideo(data == "done");
    }

#endregion
    //Вызывают функции js из index.html
    public void PostToWall(string text)
    {
        Application.ExternalCall("PostToWall", text);
    }

    public void GetUserInfo(string viewer_id)
    {
        Application.ExternalCall("GetProfile", viewer_id);
    }

    public void InviteFriends()
    {
        Debug.Log("Trying to invite External Call");
        Application.ExternalCall("ShowInvite");
    }

    public Coroutine DownLoadImage(string url, Action<Sprite> CallBack)
    {
        return StartCoroutine(_server.DownLoadImage(url, CallBack));
    }

    public static void UpdateInfoPlayer()
    {
        if(Instance==null)
        {
            Debug.LogError("SM-> instance is null!");
            return;
        }
        Instance._UpdateInfoPlayer();
    }

    public void _UpdateInfoPlayer()
    {
        Debug.Log("SM-> UpdateInfoPlayer");
        UIWaitPanel.Show();
        StartCoroutine(_server.SaveLoadServer("loadinfo", new List<StringPair>(), "", value =>
        {
            UIWaitPanel.Hide();
            Debug.Log("SM->" + value);
            _player._SetPlayerData(((UserData)StringSerializationAPI.Deserialize(_player.GetPlayerDataType(), value)).user);
        }));
    }

}


