﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrizeRandomCraftCard : PrizeData
{

    private List<int[]> idsRandom = new List<int[]>
    {
        new int[] { 501, 502, 508, 509, 510, 511, 512 },//Usual
        new int[] { 502, 504, 505, 506, 508, 509, 510, 511, 512},//Magic
        new int[] { 502, 503, 504, 505, 506, 508, 509, 510, 511, 512 },//Rare
        new int[] { },//Complete
        new int[] { 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512 },//Legendary

    };

    private int _count;
    private _CardRareness _rareness;
    public override string Value
    {
        get
        {
            var array = idsRandom[(int)_rareness];
            return StringSerializationAPI.Serialize(typeof(int[]), new[] { array[Random.Range(0, array.Length)], (int)_rareness + 1, _count });
        }
    }
    public PrizeRandomCraftCard(_CardRareness rareness, int count = 1) : base(PrizeType.Card)
    {
        _count = count;
        _rareness = rareness;
    }
    public int GetCountCard { get { return _count; } }
    public _CardRareness GetRarenessCard { get { return _rareness; } }

    public override PrizeData Clone()
    {
        return new PrizeRandomCraftCard(_rareness, _count);
    }


}
