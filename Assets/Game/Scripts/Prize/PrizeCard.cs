﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrizeCard : PrizeData
{
    private int _count,_idCard;
    private _CardRareness _rareness;
    public override string Value
    {
        get
        {
            return StringSerializationAPI.Serialize(typeof(int[]), new[] { _idCard, (int)_rareness+1,_count });
        }
    }
    public PrizeCard(int idCard,_CardRareness rareness, int count = 1) : base(PrizeType.Card)
    {
        _count = count;
        _idCard = idCard;
        _rareness = rareness;
    }

    public int GetCountCard { get { return _count; } }
    public int GetTypeCard { get { return _idCard; } }
    public _CardRareness GetRarenessCard { get { return _rareness; } }

    public override PrizeData Clone()
    {
        return new PrizeCard(_idCard, _rareness, _count);
    }
}
