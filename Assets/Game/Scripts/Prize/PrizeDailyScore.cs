﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrizeDailyScore : PrizeData
{
    private int _score;
    public override string Value
    {
        get
        {
            return _score.ToString();
        }
    }

    public PrizeDailyScore(int score):base(PrizeType.Score)
    {
        _score = score;
    }

    public override PrizeData Clone()
    {
        return new PrizeDailyScore(_score);
    }
}

