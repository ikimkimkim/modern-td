﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrizeGold : PrizeData
{
    private int _count;
    public int Count { get { return _count; } }

    public override string Value
    {
        get
        {
            return _count.ToString();
        }
    }
    public PrizeGold(int gold) : base(PrizeType.Gold)
    {
        _count = gold;
    }
    public override PrizeData Clone()
    {
        return new PrizeCrystal(_count);
    }
}

