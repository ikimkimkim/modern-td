﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PrizeType
{
    Score = -1,
    Crystal = 1,
    ResourcesCraft = 2,
    Chest = 3,
    Gold = 4,
    Card = 5
}
public abstract class PrizeData
{
    private PrizeType _Type;
    public PrizeType Type { get { return _Type;} }

    public abstract string Value { get; }

    public PrizeData(PrizeType type)
    {
        _Type = type;
    }

    public abstract PrizeData Clone();
}
