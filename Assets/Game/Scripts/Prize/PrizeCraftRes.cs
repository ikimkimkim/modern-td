﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrizeCraftRes : PrizeData
{
    private int[] count;
    private Dictionary<string, int> value = new Dictionary<string, int>();
    public override string Value
    {
        get
        {
            return StringSerializationAPI.Serialize(typeof(Dictionary<string, int>), value);
        }
    }

    public int GetRes1 { get { return count[0]; } }
    public int GetRes2 { get { return count[1]; } }
    public int GetRes3 { get { return count[2]; } }
    public int GetRes4 { get { return count[3]; } }

    public PrizeCraftRes(int[] res) : base(PrizeType.ResourcesCraft)
    {
        value = new Dictionary<string, int>();
        value.Add("1", res[0]);
        value.Add("2", res[1]);
        value.Add("3", res[2]);
        value.Add("4", res[3]);
        count = res;
    }
    public override PrizeData Clone()
    {
        return new PrizeCraftRes(new int[] { GetRes1,GetRes2,GetRes3, GetRes4});
    }

}
