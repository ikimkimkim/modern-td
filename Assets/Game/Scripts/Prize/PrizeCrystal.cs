﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrizeCrystal : PrizeData
{
    private int _countCrystal;
    public int Count { get { return _countCrystal; } }
    public override string Value
    {
        get
        {
            return _countCrystal.ToString();
        }
    }
    public PrizeCrystal(int crystal) : base(PrizeType.Crystal)
    {
        _countCrystal = crystal;
    }

    public override PrizeData Clone()
    {
        return new PrizeCrystal(_countCrystal);
    }
}
