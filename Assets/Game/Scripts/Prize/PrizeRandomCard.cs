﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrizeRandomCard : PrizeData
{

    private int[] idsRandom = new int[]
    {
        1,2,3,4,5,6,7,8,9,10,11,12,13, //tower
        201,202,203,204,205,206,207, //ability
        401,402,403,404,405,406 //hero
    };

    private int _count, _idCard;
    private _CardRareness _rareness;
    public override string Value
    {
        get
        {
            return StringSerializationAPI.Serialize(typeof(int[]), new[] { _idCard, (int)_rareness + 1, _count });
        }
    }
    public PrizeRandomCard(_CardRareness rareness, int count = 1) : base(PrizeType.Card)
    {
        _count = count;
        _idCard = idsRandom[Random.Range(0, idsRandom.Length)];
        _rareness = rareness;
    }
    private PrizeRandomCard(int idCard, _CardRareness rareness, int count = 1) : base(PrizeType.Card)
    {
        _count = count;
        _idCard = idCard;
        _rareness = rareness;
    }

    public int GetCountCard { get { return _count; } }
    public int GetTypeCard { get { return _idCard; } }
    public _CardRareness GetRarenessCard { get { return _rareness; } }

    public override PrizeData Clone()
    {
        return new PrizeRandomCard(_idCard, _rareness, _count);
    }

}
