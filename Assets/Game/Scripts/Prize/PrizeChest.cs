﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrizeChest : PrizeData
{
    private Chest.TypeChest _typeChest;
    public override string Value
    {
        get
        {
            return ((int)_typeChest + 1).ToString();
        }
    }

    public Chest.TypeChest GetTypeChest { get { return _typeChest; } }

    public PrizeChest(Chest.TypeChest typeChest) : base(PrizeType.Chest)
    {
        _typeChest = typeChest;
    }

    public override PrizeData Clone()
    {
        return new PrizeChest(_typeChest);
    }
}
