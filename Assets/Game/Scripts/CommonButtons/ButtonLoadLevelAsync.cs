﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonLoadLevelAsync : MonoBehaviour
{
    public int Level;
    public GameObject LoadPanel;
    public static bool Loading = false;
    public bool CheckIfLevelAvailable = false;
    public delegate void LevelLoadStarted();
    public static event LevelLoadStarted LevelLoadStartedE;
    public void LoadLevel()
    {
        Debug.Log("SB->LoadLevel");
        TimeScaleManager.SetTimeScale(1f);
        if (CheckIfLevelAvailable)
        {
            if (!GetComponent<LevelItem>().AvailableLevel())
            {
                Debug.Log("SB-> AvailableLevel is false");
                return; }
        }
        if (Loading)
        {
            Debug.Log("SB-> Loading already going!");
            return;
        }
        if (LevelLoadStartedE != null)
        {
            LevelLoadStartedE();
        }
        Loading = true;
        if (LoadPanel != null)
        { LoadPanel.SetActive(true); }

        Debug.Log("SB->level:"+Level);
        PropertiesHelp.listCoomplectPropertiesBild.Clear();
        if (WebGLLevelStreaming.CanStreamedLevelBeLoaded(Level))
        {
            Debug.Log("SB->StartCoroutine LoadLevelProgress");
            AsyncOperation async = SceneManager.LoadSceneAsync(Level); // Application.LoadLevelAsync(Level);
            //async.allowSceneActivation = false;
            StartCoroutine(LoadLevelProgress(async));
        }
        else
        {
            StartCoroutine(WaitThenLoad());
        }
    }
    IEnumerator WaitThenLoad()
    {
        while (!WebGLLevelStreaming.CanStreamedLevelBeLoaded(Level))
        {
            yield return new WaitForSeconds(0.2f);
        }
        AsyncOperation async = SceneManager.LoadSceneAsync(Level); // Application.LoadLevelAsync(Level);
        //async.allowSceneActivation = false;
        StartCoroutine(LoadLevelProgress(async));
        yield break;
    }
    IEnumerator LoadLevelProgress(AsyncOperation async)
    {
        Debug.Log("SB->LoadLevelProgress start");
        while (!async.isDone)
        {
        //    Debug.Log("Loading " + async.progress);
            if (async.progress >= 0.9f)
            {
                // Almost done.
                break;
            }

            yield return null;
        }

        Debug.Log("SB->LoadLevelProgress complete");
        Loading = false;
        TimeScaleManager.SetTimeScale(1f);
        // This is where I'm actually changing the scene
        //async.allowSceneActivation = true;
        yield return async;
    }
}
