﻿using UnityEngine;
using System.Collections;

public class ButtonInstantiate : MonoBehaviour
{

    public GameObject Object;
    public Canvas MainCanvas;

    public void InstantiateOnCanvas()
    {
        var objTransform = (Instantiate(Object) as GameObject).GetComponent<RectTransform>();
        objTransform.SetParent(MainCanvas.GetComponent<RectTransform>(),false);
        objTransform.localPosition = new Vector3(0, 0, 0);
     //   objTransform.localScale = new Vector3(objTransform.localScale.x * MainCanvas.scaleFactor, objTransform.localScale.y * MainCanvas.scaleFactor, objTransform.localScale.z);
    }
}
