﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ButtonQualityChanger : MonoBehaviour
{
    public int Min;
    public int Max;
    public List<string> QualityNames = new List<string>();

    Text _text;
    PlayerManager _player;
    //ServerManager _server;
    void Awake()
    {
        _text = GetComponentInChildren<Text>();
    }
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
       // _server = _player.GetComponent<ServerManager>();
        _text.text = QualityNames[_player.GetQuality() - Min];
    }
    void OnEnable()
    {
        _text.text = QualityNames[QualitySettings.GetQualityLevel() - Min];
    }
    public void ChangeQuality()
    {
        int current = QualitySettings.GetQualityLevel();//_player.GetQuality();
        current++;
        if (current > Max)
        {
            current = Min;
        }
        _player.SetQuality(current);
        _text.text = QualityNames[current - Min];
        Debug.LogError("SetQualityLevel");
        QualitySettings.SetQualityLevel(current, true);

    }
}
