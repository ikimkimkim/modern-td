﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class DownLoadResurces : MonoBehaviour
{

    public enum Type { mainTexture, SpriteImage,Button, TerrainData }
    public Type typeTexturDownload = Type.mainTexture;

    public bool useNameMaterial = true;

    public string Url;
    public string UrlButtonH, UrlButtonP;

    private Button linkButton;


    //private string NameShader;

    void Start()
    {
        //SetNameShader();
        Load();
    }

    void OnEnable()
    {
        if(PlayerManager._instance!=null)
            Load();
    }

    private string NameTextur;
   
    public void Load()
    {
        switch (typeTexturDownload)
        {
            case Type.mainTexture:
                MeshRenderer mesh = GetComponent<MeshRenderer>();
                if (mesh != null)
                    NameTextur = mesh.material.mainTexture.name;
                else
                {
                    SkinnedMeshRenderer skinned = GetComponent<SkinnedMeshRenderer>();
                    NameTextur = skinned.material.mainTexture.name;
                }

                if (useNameMaterial)
                {                    
                    StartCoroutine(TextureDB.instance.load("Texture/" + NameTextur + ".png", SetTexture));
                }
                else
                    StartCoroutine(TextureDB.instance.load(Url, SetTexture));
                break;
            case Type.SpriteImage:
                StartCoroutine(TextureDB.instance.load(Url, SetSprite));
                break;
            case Type.Button:

                StartCoroutine(TextureDB.instance.load(Url, SetSprite));

                linkButton = this.GetComponent<Button>();

                StartCoroutine(TextureDB.instance.load(UrlButtonH, value =>
                {
                    SpriteState ss = new SpriteState();
                    lock (linkButton)
                    {
                        ss.disabledSprite = linkButton.spriteState.disabledSprite;
                        ss.pressedSprite = linkButton.spriteState.pressedSprite;
                        ss.highlightedSprite = value;
                        linkButton.spriteState = ss;
                    }
                }));

                StartCoroutine(TextureDB.instance.load(UrlButtonP, value =>
                {
                    SpriteState ss = new SpriteState();
                    lock (linkButton)
                    {
                        ss.disabledSprite = linkButton.spriteState.disabledSprite;
                        ss.pressedSprite = value;
                        ss.highlightedSprite = linkButton.spriteState.highlightedSprite;
                        linkButton.spriteState = ss;
                    }
                }));

                break;
            case Type.TerrainData:
                //StartCoroutine(ServerManager.DownLoadTerrainData("Terrain 1.asset",false, null));
                break;
        }
    }

    

    public void SetSprite(Sprite sprite)
    {
        GetComponent<Image>().sprite = sprite;
    }

    /*private void SetNameShader()
    {
        MeshRenderer mesh = GetComponent<MeshRenderer>();
        if (mesh != null)
        {
            NameShader = mesh.material.shader.name;
        }
        else
        {
            SkinnedMeshRenderer skinned = GetComponent<SkinnedMeshRenderer>();
            NameShader =  skinned.material.shader.name;

        }
    }*/

    public void SetTexture(Texture2D texture)
    {
        texture.name = NameTextur;
        MeshRenderer mesh = GetComponent<MeshRenderer>();
        if (mesh != null)
        {
            //mesh.material.shader = Shader.Find(NameShader);
            mesh.material.mainTexture = texture;

        }
        else
        {
            SkinnedMeshRenderer skinned = GetComponent<SkinnedMeshRenderer>();
            //skinned.material.shader = Shader.Find(NameShader);
            skinned.material.mainTexture = texture;

        }
    }
}
