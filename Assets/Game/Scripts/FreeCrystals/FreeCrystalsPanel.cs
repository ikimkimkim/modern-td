﻿using UnityEngine;
using System.Collections;

public class FreeCrystalsPanel : MonoBehaviour {

    public GameObject ButtonFreeCrystals;

    public GameObject Screen, Background;

    public void ShowPanel()
    {
        Screen.SetActive(true);
        Background.SetActive(true);
    }
    public void HidePanel()
    {
        Screen.SetActive(false);
        Background.SetActive(false);
     
    }
    
    

    public void OnTask()
    {
        SocialManager.Instance.OrderOffer();
    }

    void Start ()
    {
        //ButtonFreeCrystals.SetActive(PlayerManager._instance.GetLevelsCount() >= 3);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
