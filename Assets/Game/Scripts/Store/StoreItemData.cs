﻿

public class StoreItemData
{
    public string item;
    public int count;
    public int chest_type;
    public string price;
    public StoreItemData()
    { }
    public StoreItemData(string item, string price)
    {
        this.item = item;
        this.price = price;
    }
}

public class StoreCrystalItemData
{
    public string item;
    public int count;
    public int chest_type;
    public int price;
    public StoreCrystalItemData()
    { }
    public StoreCrystalItemData(string item, int price)
    {
        this.item = item;
        this.price = price;
    }
}
