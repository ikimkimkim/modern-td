﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TDTK;


public class StoreManager : MonoBehaviour
{
    [SerializeField] private GameObject _panel;
    public GameObject ItemGroupPrefab;
    public GameObject ItemPrefab;
    public Transform TPanel;
    [SerializeField] private Scrollbar _scrollbarVertical;
    //PlayerManager _player;

    public GameObject ErrorObj;
    public Text ErrorText;
    
    public static StoreManager instance { get; private set; }

    private Coroutine corError;

    [Header("Tutorial")]
    [SerializeField] private GameObject arrowShowButtonOpen;
    [SerializeField] private GameObject arrowShowChestSale;

    public static void ShowError(string text)
    {
        if(instance!=null)
        {
            instance._ShowError(text);
        }
    }

    public void _ShowError(string text)
    {
        ErrorText.text = text;
        if (corError != null)
            StopCoroutine(corError);
        corError = StartCoroutine(corShowError());
    }

    private IEnumerator corShowError()
    {
        ErrorObj.SetActive(true);
        yield return new WaitForSeconds(5);
        ErrorObj.SetActive(false);
    }

    void Start()
    {
        instance = this;
    }


    public List<GameObject> Objs = new List<GameObject>();

    public void LoadItem()
    {
        for (int i = 0; i < Objs.Count; i++)
        {
            GameObject.Destroy(Objs[i]);
        }
        Objs.Clear();
        ErrorObj.SetActive(false);

        if (PlayerManager.isPremium==false)
        {
            AddPremiumPanel();
        }

        GameObject gpoupChest = Instantiate(ItemGroupPrefab, TPanel);
        gpoupChest.transform.localScale = new Vector3(1, 1, 1);
        gpoupChest.GetComponent<StoreItemGroup>().textInfo.text = "Сундуки";
        gpoupChest.GetComponent<GridLayoutGroup>().cellSize = new Vector2(135, 160);
        Objs.Add(gpoupChest);

        GameObject gpoupGold = Instantiate(ItemGroupPrefab, TPanel);
        gpoupGold.transform.localScale = new Vector3(1, 1, 1);
        gpoupGold.GetComponent<StoreItemGroup>().textInfo.text = "Золото";
        gpoupGold.GetComponent<GridLayoutGroup>().cellSize = new Vector2(135, 125);
        Objs.Add(gpoupGold);

        GameObject gpoupCristal = Instantiate(ItemGroupPrefab, TPanel);
        gpoupCristal.transform.localScale = new Vector3(1, 1, 1);
        gpoupCristal.GetComponent<StoreItemGroup>().textInfo.text = "Кристаллы";
        gpoupCristal.GetComponent<GridLayoutGroup>().cellSize = new Vector2(135,150);
        Objs.Add(gpoupCristal);

        GameObject gpoupEnergy = Instantiate(ItemGroupPrefab, TPanel);
        gpoupEnergy.transform.localScale = new Vector3(1, 1, 1);
        gpoupEnergy.GetComponent<StoreItemGroup>().textInfo.text = "Энергия";
        gpoupEnergy.GetComponent<GridLayoutGroup>().cellSize = new Vector2(135, 125);
        if(PlayerManager.isPremium == false)
            gpoupEnergy.GetComponent<GridLayoutGroup>().padding.bottom = 30;
        Objs.Add(gpoupEnergy);

        
        if (SocialManager.buyCrystalConfig.gold.Length>0)
        {      
            for (int i = 0; i < SocialManager.buyCrystalConfig.gold.Length; i++)
            {
                GameObject go = Instantiate(ItemPrefab, gpoupGold.transform);
                go.GetComponent<StoreItem>().setItem(SocialManager.buyCrystalConfig.gold[i], StoreItem.Type.gold);
                go.transform.localScale = new Vector3(1, 1, 1);
            }
        }

        if (SocialManager.buyCrystalConfig.chests.Length > 0)
        {
            for (int i = 0; i < SocialManager.buyCrystalConfig.chests.Length; i++)
            {
                GameObject go = Instantiate(ItemPrefab, gpoupChest.transform);
                go.GetComponent<StoreItem>().setItem(SocialManager.buyCrystalConfig.chests[i], StoreItem.Type.chest);
                go.transform.localScale = new Vector3(1, 1, 1);
                if(i==0)
                    arrowShowChestSale.GetComponent<StayInSameWorldPos>().Target = go;
            }
        }

        if (SocialManager.buyCrystalConfig.energy.Length > 0)
        {
            for (int i = 0; i < SocialManager.buyCrystalConfig.energy.Length; i++)
            {
                GameObject go = Instantiate(ItemPrefab, gpoupEnergy.transform);
                go.GetComponent<StoreItem>().setItem(SocialManager.buyCrystalConfig.energy[i], StoreItem.Type.energy);
                go.transform.localScale = new Vector3(1, 1, 1);
            }
        }


        //---
        if (SocialManager.buyConfig.crystals.Length > 0)
        {
            for (int i = 0; i < SocialManager.buyConfig.crystals.Length; i++)
            {
                GameObject go = Instantiate(ItemPrefab, gpoupCristal.transform);
                go.GetComponent<StoreItem>().setItem(SocialManager.buyConfig.crystals[i], StoreItem.Type.crystal);
                go.transform.localScale = new Vector3(1, 1, 1);
            }
        }
        
        if (SocialManager.buyConfig.energy.Count > 0)
        {
            foreach (var energy in SocialManager.buyConfig.energy)
            {
                GameObject go = Instantiate(ItemPrefab, gpoupEnergy.transform) as GameObject;
                go.GetComponent<StoreItem>().setItem(energy.Value, StoreItem.Type.energy);
                go.transform.localScale = new Vector3(1, 1, 1);
            }
        }

        if (PlayerManager.isPremium)
        {
            AddPremiumPanel();
        }
    }

    private void AddPremiumPanel()
    {
        GameObject gpoupPremium = Instantiate(ItemGroupPrefab, TPanel);
        gpoupPremium.transform.localScale = new Vector3(1, 1, 1);
        gpoupPremium.GetComponent<StoreItemGroup>().textInfo.text = "Подписки";
        gpoupPremium.GetComponent<GridLayoutGroup>().cellSize = new Vector2(135, 160);
        if (PlayerManager.isPremium)
            gpoupPremium.GetComponent<GridLayoutGroup>().padding.bottom = 30;
        Objs.Add(gpoupPremium);

        for (int i = 0; i < 1; i++)
        {
            GameObject go = Instantiate(ItemPrefab, gpoupPremium.transform);
            go.GetComponent<StoreItem>().setItemPrem("7 голосов за 7 дней", StoreItem.Type.premium, PlayerManager.isPremium);
            go.transform.localScale = new Vector3(1, 1, 1);
        }
    }
   
    public void Show()
    {
        if (_panel.activeSelf)
            return;
        _panel.SetActive(true);
        LoadItem();
        _AddStatusTutorial(StatusTutorial.waitOpen);
    }

    public void Close()
    {
        if (_panel.activeSelf == false)
            return;
        _panel.SetActive(false);
        if(isTutorial)
        {
            if (_tutorialStatus == StatusTutorial.chestSale)
                _EndTutorial();
        }
    }


    void Update()
    {
        if (CardUIAllInfo.isShow == false && _panel.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Close();
            }
        }
    }


    #region Tutor
    private enum StatusTutorial { waitOpen, general, chestSale, end }
    [SerializeField] private StatusTutorial _tutorialStatus;
    private Action _endTutorialEvent;
    private bool isTutorial = false;


    public static void StartTutorial(Action endTutorial)
    {
        instance._StartTutorial(endTutorial);
    }

    private void _StartTutorial(Action endTutorial)
    {
        isTutorial = true;
        _endTutorialEvent = endTutorial;
        _tutorialStatus = StatusTutorial.waitOpen;
        _UpdateStatus();
    }

    private void _AddStatusTutorial(StatusTutorial status)
    {
        if (isTutorial == false)
            return;
        if (_tutorialStatus != status)
            return;
        Debug.Log("_AddStatusTutorial");
        _tutorialStatus++;
        _UpdateStatus();
    }

    private void _UpdateStatus()
    {
        switch (_tutorialStatus)
        {
            case StatusTutorial.waitOpen:
                arrowShowButtonOpen.SetActive(true);
                break;
            case StatusTutorial.general:
                arrowShowButtonOpen.SetActive(false);
                GeneralPanel.StartShow(()=> { _AddStatusTutorial(StatusTutorial.general); }, new GeneralState[]{
                    new GeneralState("Здесь выставлены лучшие наши разработки. Вот, попробуйте этот сундук.",
                    GeneralPos.Salutes, GeneralOrientation.right)
                });

                break;
            case StatusTutorial.chestSale:
                _scrollbarVertical.value = 0.9f;
                arrowShowChestSale.SetActive(true);
                break;

            case StatusTutorial.end:
                _EndTutorial();
                break;
        }
    }

    private void _EndTutorial()
    {
        if (isTutorial == false)
            return;
        arrowShowButtonOpen.SetActive(false);
        arrowShowChestSale.SetActive(false);
        isTutorial = false;
        _endTutorialEvent();
    }


    #endregion
}
