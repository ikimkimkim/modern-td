﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using TDTK;
using System;

public class StoreItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public delegate void StoreEvent(string item);
    public static event StoreEvent onBuyForCrystal;

    public enum Type { premium, crystal, energy, gold, chest };
    private Type _type;
    public StoreItemData ItemData;

    public bool bCrystalPrice;

    public Sprite[] CrystalsIcon;
    public string[] UrlCrystals;
    public Sprite[] GoldIcon;
    public string[] UrlGold;
    public Sprite Energy;
    public string UrlEnergy;
    public Sprite PremIcon;

    public Image Icon;
    public GameObject SmallIconCrystal;
    public GameObject ShadowIcon;
    public RectTransform IconRect;
    public Text CountItem, Price;
    public Button thisButton;
    public Text textButton;

    public Color NormalIcon;
    public string _item;
    private Image Fon;

    public GameObject InfoPanel;
    public Text InfoPanelText;
    private Coroutine _CorInfo;

    public GameObject ChestInfoPanel;
    public GameObject ObjCrystalInfo;
    public Text GoldText, CrystalText, CardText, CardInfoText;

    void Start()
    {
        Fon = GetComponent<Image>();
        ExitPointer();
    }

    public void ShowInfo(string text)
    {
        InfoPanelText.text = text;
        if (_CorInfo != null)
            StopCoroutine(_CorInfo);
        _CorInfo = StartCoroutine(CorShowInfo());
    }

    public IEnumerator CorShowInfo()
    {
        InfoPanel.SetActive(true);
        yield return new WaitForSeconds(3);
        InfoPanel.SetActive(false);
    }

    private void CheckedActiveObj()
    {
        if (gameObject.activeSelf == false)
        {
            Debug.LogError("StoreItem is inactive!");
            gameObject.SetActive(true);
        }
    }

    public void setItemPrem(string priceText, Type type, bool premium)
    {
        CheckedActiveObj();
        bCrystalPrice = false;
        _type = type;
        SmallIconCrystal.SetActive(false);
        ShadowIcon.SetActive(false);
        Price.fontSize = 14;
        Price.text = priceText;
        Icon.sprite = PremIcon;
        IconRect.sizeDelta = new Vector2(128, 80);
        CountItem.fontSize = 15;
        CountItem.text = "Улучшенные сундуки";
        if(premium)
        {
            thisButton.interactable = false;
            textButton.text = "Куплено";
        }
    }

    public void setItem(StoreItemData item, Type type)
    {
        CheckedActiveObj();
        _type = type;
        bCrystalPrice = false;
        ItemData = item;
        SmallIconCrystal.SetActive(false);
        _item = item.item;
        CountItem.text = item.count.ToString();
        Price.text = item.price;
        switch (type)
        {
            case Type.crystal:
                int index = int.Parse(_item.Remove(0, _item.Length - 1)) - 1;
                index = Mathf.Max(0, index);
                Icon.sprite = CrystalsIcon[index];
                StartCoroutine(TextureDB.instance.load(UrlCrystals[index], SetIconChest));
                switch(index)
                {
                    case 0:
                        IconRect.sizeDelta = new Vector2(60, 35);

                        break;
                    case 1:
                        IconRect.sizeDelta = new Vector2(60, 35);

                        break;
                    case 2:
                        IconRect.sizeDelta = new Vector2(60, 40);

                        break;
                    case 3:
                        IconRect.sizeDelta = new Vector2(80, 75);

                        break;
                    case 4:
                        IconRect.sizeDelta = new Vector2(95, 80);

                        break;
                    case 5:
                        IconRect.sizeDelta = new Vector2(110, 75);

                        break;
                }
                break;
            case Type.energy:
                Icon.sprite = Energy;
                StartCoroutine(TextureDB.instance.load(UrlEnergy, SetIconChest));
                CountItem.text = item.count.ToString();
                IconRect.sizeDelta = new Vector2(50, 60);
                break;
        }
    }
    private StoreCrystalItemData itemData;
    public void setItem(StoreCrystalItemData item, Type type)
    {
        CheckedActiveObj();
        _type = type;
        bCrystalPrice = true;
        SmallIconCrystal.SetActive(true);
        itemData = item;
        _item = item.item;
        switch (type)
        {
            case Type.chest:
                CountItem.fontSize = 15;
                CountItem.color = Color.white;
                CountItem.text = Chest.getNameChest((Chest.TypeChest) (item.chest_type - 1));
                StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsChest[item.chest_type - 1], SetIconChest));
                IconRect.sizeDelta = new Vector2(80, 80);

                ChestPrizesData data = SocialManager.chest_info_conf[item.chest_type.ToString()];
                if (data != null)
                {
                    GoldText.text = data.gold[0] + "-" + data.gold[1]; // Chest.getCountGold((Chest.TypeChest)(item.chest_type - 1));
                    ObjCrystalInfo.SetActive(data.crystals[1] + data.crystals[0] > 0);
                    CrystalText.text = data.crystals[0] + "-" + data.crystals[1]; //  Chest.getCountCrystal((Chest.TypeChest)(item.chest_type - 1));
                    CardText.text = "+" + data.cards.cards_count; //  Chest.getCountCard((Chest.TypeChest)(item.chest_type - 1));
                    CardInfoText.text = data.cards.max_text; // chest._data.available_prizes.cards.max_text;// Chest.getInfoCardInChest((Chest.TypeChest)(item.chest_type - 1));
                }
                else MyLog.LogError("Data ChestPrizes is null! Type:" + item.chest_type);

                break;
            case Type.gold:
                int index = int.Parse(_item.Remove(0, _item.Length - 1)) - 1;
                index = Mathf.Max(0, index);
                Icon.sprite = GoldIcon[index];
                CountItem.text = item.count.ToString();
                switch (index)
                {
                    case 0:
                        IconRect.sizeDelta = new Vector2(40, 35);

                        break;
                    case 1:
                        IconRect.sizeDelta = new Vector2(70, 35);

                        break;
                    case 2:
                        IconRect.sizeDelta = new Vector2(100, 50);

                        break;
                }
                StartCoroutine(TextureDB.instance.load(UrlGold[index], SetIconChest));
                break;
            case Type.energy:
                Icon.sprite = Energy;
                IconRect.sizeDelta = new Vector2(50, 60);
                StartCoroutine(TextureDB.instance.load(UrlEnergy, SetIconChest));
                CountItem.text = item.count.ToString();
                break;
        }
        Price.text = item.price.ToString();
    }


    public void OnPointerEnter(PointerEventData evd)
    {
        Icon.color = Color.white;
        Fon.color = Color.white;
        if (_type == Type.chest)
        {
            InfoPanel.SetActive(false);
            ChestInfoPanel.SetActive(true);
        }
    }

    public void OnPointerExit(PointerEventData evd)
    {
        ExitPointer();
    }

    private void ExitPointer()
    {
        Icon.color = NormalIcon;
        Fon.color = NormalIcon;
        if(_type == Type.chest)
        {
            ChestInfoPanel.SetActive(false);
        }
    }

    public void SetIconChest(Sprite icon)
    {
       // IconRect.sizeDelta = new Vector2(icon.rect.width, IconRect.sizeDelta.y);
        Icon.sprite = icon;
    }

    public void OnButton()
    {

        switch(_type)
        {
            case Type.chest:
            case Type.gold:
                BuyCristal();
                break;
            case Type.energy:
                if (bCrystalPrice == false)
                    Application.ExternalCall("order", _item);
                else
                    BuyCristal();
                break;

            case Type.crystal:
                Application.ExternalCall("order", _item);
                break;

            case Type.premium:
                Application.ExternalCall("subscr", "create", "subscr_chests_1");
                break;
        }
    }

   private void BuyCristal()
    {
        textButton.text = "Ожидание…";
        thisButton.interactable = false;
        PlayerManager.DM.BuyForCrystals(_item, ResponseBuy);
    }

    private void ResponseBuy(ResponseData rp)
    {
        if (rp.ok == 0)
        {
            Debug.LogError("Buy Error: " + rp.error.code + " -> " + rp.error.text);
            if(rp.error.code== "no_crystals")
            {
                StartCoroutine(ChangeColor());
            }
            else
            {
                StoreManager.ShowError(rp.error.text);
                ShowInfo("Ошибка!");
            }
            PlayerManager.SetPlayerData(rp.user);
        }
        else
        {
            StartCoroutine(ChangeColorIcon());
            //StoreManager.ShowError("Готово!");
            ShowInfo("Готово!");
            ChestInfoPanel.SetActive(false);
            PlayerManager.SetPlayerData(rp.user);

            if (onBuyForCrystal != null)
                onBuyForCrystal(_item);

            /* Для тестов!
            rp.info.prizes = new ResponseData.PrizesData();
            rp.info.prizes.gold = 1;
            rp.info.prizes.crystals = 2;
            rp.info.prizes.towers = new ResponseData.PrizesTower[3];
            rp.info.prizes.towers[0] = new ResponseData.PrizesTower();
            rp.info.prizes.towers[0].count = 2;
            rp.info.prizes.towers[0].rarity = 1;
            rp.info.prizes.towers[0].type = 1;


            rp.info.prizes.towers[1] = new ResponseData.PrizesTower();
            rp.info.prizes.towers[1].count = 1;
            rp.info.prizes.towers[1].rarity = 1;
            rp.info.prizes.towers[1].type = 4;

            rp.info.prizes.towers[2] = new ResponseData.PrizesTower();
            rp.info.prizes.towers[2].count = 1;
            rp.info.prizes.towers[2].rarity = 1;
            rp.info.prizes.towers[2].type = 7;*/

            if (_type == Type.premium)
                StoreManager.instance.Close();

            if (rp.info != null && rp.info.ok == 1 && rp.info.prizes != null && _type == Type.chest)
            {
                StoreManager.instance.Close();
                Chest chest = new Chest(0, (Chest.TypeChest)(itemData.chest_type - 1), "", 0, 0, 0);

                //List<UnitTower> bd = TowerDB.Load();
                List<Card> newCard = new List<Card>();
                foreach (ResponseData.PrizesTower PrizesTower in rp.info.prizes.towers)
                {
                    for (int i = 0; i < PlayerManager.CardsList.Count; i++)
                    {
                        if (PlayerManager.CardsList[i].IDTowerData == PrizesTower.type - 1 && (int)PlayerManager.CardsList[i].rareness == PrizesTower.rarity - 1)
                        {
                            newCard.Add(PlayerManager.CardsList[i]);
                            newCard[newCard.Count - 1].AddCount = PrizesTower.count;
                            newCard[newCard.Count - 1].count -= PrizesTower.count;
                            break;
                        }
                    }
                }
                ChestManager.instance.EndOpen(chest, newCard, rp.info.prizes.gold, rp.info.prizes.crystals,rp.info.prizes.craft_resources);
            }
        }
        thisButton.interactable = true;
        textButton.text = "Купить!";
    }

    IEnumerator ChangeColorIcon()
    {
        for (int i = 0; i < 4; i++)
        {
            Icon.color = Color.white;
            yield return new WaitForSeconds(0.1f);
            Icon.color = NormalIcon;
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator ChangeColor()
    {
        for (int i = 0; i < 4; i++)
        {
            Price.color = new Color(1, 0.2f, 0.2f, 1f);
            yield return new WaitForSeconds(0.1f);
            Price.color = Color.white;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
