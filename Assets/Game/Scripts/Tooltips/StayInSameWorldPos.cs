﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StayInSameWorldPos : MonoBehaviour
{
    public bool isUniversal = false;
    public static StayInSameWorldPos instance;

    public GameObject[] objTooltips;

    public GameObject Target;
    public Vector3 PosTarget;
    public Vector3 Shift;

    RectTransform _transform;
    public Text textTooltip;
    //float Scale;
    void Awake()
    {
        if(isUniversal)
            instance = this;
        _transform = GetComponent<RectTransform>();
    }

    public static void setPos(Vector3 v,string text)
    {
        if (instance == null)
            return;
        instance.textTooltip.text = text;
        instance.SetPos(v);
    }

    public static void Hide()
    {
        if (instance == null)
            return;
        instance.OnHide();
    }
    public void WaitUpdate(Vector3 pos)
    {
        setPos(pos, "Появится в будущих обнолениях");
    }
    public void WaitUpdate(GameObject obj)
    {
        Vector3 pos = obj.transform.position;
        pos.x += obj.GetComponent<RectTransform>().rect.center.x;
        pos.y += obj.GetComponent<RectTransform>().rect.center.y;
        WaitUpdate(pos);
    }

    public void SetPos(GameObject v)
    {
        Target = v;
        for (int i = 0; i < objTooltips.Length; i++)
        {
            objTooltips[i].SetActive(true);
        }
    }

    public void SetPos(Vector3 v)
    {
        Target = null;
        PosTarget = v;
        for (int i = 0; i < objTooltips.Length; i++)
        {
            objTooltips[i].SetActive(true);
        }
    }

    public void OnHide()
    {
        for (int i = 0; i < objTooltips.Length; i++)
        {
            objTooltips[i].SetActive(false);
        }
    }

    void OnEnable()
    {
        //Scale = GameObject.FindObjectOfType<Canvas>().scaleFactor;
        if (Target != null)
            PosTarget = Target.transform.position;
        _transform.position = PosTarget + Shift; /** Scale;*/
    }
    void Update()
    {
        if (Target != null)
            PosTarget = Target.transform.position;
        _transform.position = PosTarget + Shift; /** Scale;*/
    }
}
