﻿using UnityEngine;
using System.Collections;
using TDTK;
using UnityEngine.SceneManagement;

public class InGameTooltipManager : MonoBehaviour
{
    public Tooltip BuildTower1;
    public Tooltip BuildTower2;
    public Tooltip StartBattle;
    public GameObject Instructions;
    public float StartBattleDelay = 10f;

    bool _oneTowerTooltipShown = false;
    bool _startBattlerTooltipShown = false;
    bool _twoTowerTooltipShown = false;
    bool _TutorialLevel = true;
    void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex != 1) //Application.loadedLevel != 1)
        {
            _TutorialLevel = false;
            if(StartBattleDelay>=0)
            StartCoroutine(ShowStartBattleDelayed(StartBattleDelay));
        }
        else
        {
            var gameControlHelper = GameControl.instance.GetComponent<GameControlHelper>();
            BuildTower1.GetComponent<StayInSameWorldPos>().Target = gameControlHelper.Platform1;
            BuildTower2.GetComponent<StayInSameWorldPos>().Target = gameControlHelper.Platform2;
        }
    }
    IEnumerator ShowStartBattleDelayed(float delay)
    {
        yield return new WaitForSeconds(delay);
        StartBattle.gameObject.SetActive(true);
        yield break;
    }
    void Update()
    {
        if (_TutorialLevel)
        {
            //Проверки на отображение строить тут 1
            if (!_oneTowerTooltipShown && Instructions == null && BuildManager.GetTowerCount() == 0)
            {
                //BuildTower1.gameObject.SetActive(true);
                _oneTowerTooltipShown = true;
            }

            //Проверка на отображение  строить тут 2
            if (!_twoTowerTooltipShown && GameControl.IsGameStarted() && BuildManager.GetTowerCount() == 1 && ResourceManager.GetResourceArray()[0] >= 6)
            {
                //BuildTower2.gameObject.SetActive(true);
                _twoTowerTooltipShown = true;
            }
            //Проверка на отображение начать битву
            if (!_startBattlerTooltipShown && BuildManager.GetTowerCount() == 1)
            {
                BuildTower1.gameObject.SetActive(false);
                if (!GameControl.IsGameStarted())
                    StartBattle.gameObject.SetActive(true);
                _startBattlerTooltipShown = true;
            }
            //Если 2 здания то убираем 2ю постройку
            if (BuildManager.GetTowerCount() == 2)
            {
                BuildTower2.gameObject.SetActive(false);
                Destroy(gameObject);
            }
        }
        //Если игра началась то убираем начать битву
        if (GameControl.IsGameStarted())
        {
            StartBattle.gameObject.SetActive(false);
            if (SceneManager.GetActiveScene().buildIndex != 3) //Application.loadedLevel != 3)
            {
                Destroy(gameObject);
            }
        }
    }
}
