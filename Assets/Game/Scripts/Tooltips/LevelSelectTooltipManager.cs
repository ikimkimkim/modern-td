﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelSelectTooltipManager : MonoBehaviour
{
    public Tooltip StartThere;
    public StayInSameWorldPos StartTherePos;
    public Tooltip Upgrade;
    public float TimeToShowStartThere;
    public float StartThereShift;
    PlayerManager _playerInfo;
    LevelSelectManager _levelSelectManager;
    bool _levelLoad = false;
    void Start()
    {
        _playerInfo = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
        _levelSelectManager = GameObject.FindGameObjectWithTag("LevelSelectManager").GetComponent<LevelSelectManager>();

        StartThere.gameObject.SetActive(false);
        
        if (_playerInfo.GetLevelsCountComplele() > 0 && _playerInfo._GetPurchasedID.Count == 0 && _playerInfo.GetLevelStars(1) != 0 || _playerInfo.GetNotSpendStars(_playerInfo.GetSpendStars()) >= 4)
        {
            if(Upgrade!=null)
            Upgrade.gameObject.SetActive(true);
        }
        if (_levelSelectManager.isAnyNotFinishedLevel())
        {
            if (_playerInfo.GetLevelsCountComplele() == 1 && _playerInfo.GetPerksCount() > 0)
            {
                TimeToShowStartThere = 0f;
            }

            SmartPlace(_levelSelectManager.GetCurrentLevelTransform());
            StartCoroutine(ShowStartThere());
        }

        if (StartTherePos == null)
            StartTherePos = StartThere.GetComponent<StayInSameWorldPos>();
    }
    void OnEnable()
    {
        ButtonLoadLevelAsync.LevelLoadStartedE += LoadLevelStarted;
        LevelSelectManager.OnMoveFinished += LevelSelectManager_OnMoveFinished;
        LevelSelectManager.OnMoveStarted += LevelSelectManager_OnMoveStarted;
    }

 
    void OnDisable()
    {
        ButtonLoadLevelAsync.LevelLoadStartedE -= LoadLevelStarted;
        LevelSelectManager.OnMoveFinished -= LevelSelectManager_OnMoveFinished;
        LevelSelectManager.OnMoveStarted -= LevelSelectManager_OnMoveStarted;
    }
    void LevelSelectManager_OnMoveStarted()
    {
        StartThere.gameObject.SetActive(false);
        StopAllCoroutines();
    }

    void LevelSelectManager_OnMoveFinished()
    {
        SmartPlace(_levelSelectManager.GetCurrentLevelTransform());
        StartCoroutine(ShowStartThere());
    }
    void LoadLevelStarted()
    {
        if (!_levelLoad)
        {
            //Debug.Log("Hide Tooltips level load begun!");
            if(Upgrade!=null)
            Upgrade.gameObject.SetActive(false);
            StartThere.gameObject.SetActive(false);
            _levelLoad = true;
        }
    }
    void SmartPlace(Transform target)
    {
        if (target == null)
            return;

       // float scale = GameObject.FindObjectOfType<Canvas>().scaleFactor;

        StartThere.HideAllArrows();
        StartThere.ArrowDown.SetActive(true);
        StartTherePos.Target = target.gameObject;

        /*if (target.position.y + StartThereShift * scale + 15 > Screen.height - 20 * scale)
        {
            //ставим снизу
            StartThere.ArrowUp.SetActive(true);
            StartTherePos.Target = target.gameObject;
            //StartTherePos.Shift = -new Vector3(0, StartThereShift * scale - 25 * scale, 0);
        }
        else
        {
            //ставим сверху
            StartThere.ArrowDown.SetActive(true);
            StartTherePos.Target = target.gameObject;
           // StartTherePos.Shift = new Vector3(0, StartThereShift * scale, 0);
        }

        if (target.position.x + 160 * scale / 2 > Screen.width - 20 * scale)
        {
            //выходим за границу справа
            float shift = target.position.x - (Screen.width - 20 * scale - 160 * scale / 2);
            StartThere.transform.position = new Vector3(Screen.width - 20 * scale - 160 * scale / 2, StartThere.transform.position.y, StartThere.transform.position.z);
            if (StartThere.ArrowDown)
            {
                StartThere.ArrowDown.transform.localPosition += new Vector3(Mathf.Sign(shift) * Mathf.Min(Mathf.Abs(shift), 140 * scale / 2 - 16), 0, 0);
            }
        }

        if (target.position.x - 105 / 2 < 20)
        {
            //выходим за границу слева
        }*/

    }
    IEnumerator ShowStartThere()
    {
        yield return new WaitForSeconds(TimeToShowStartThere);
        if (!_levelLoad)
            StartThere.gameObject.SetActive(true);
        yield break;
    }

}
