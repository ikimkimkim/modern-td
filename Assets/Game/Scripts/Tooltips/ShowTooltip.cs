﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShowTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public string textShow;
    public Transform shotPointText;

    public void OnPointerEnter(PointerEventData eventData)
    {
            if (textShow.Length > 0)
                StayInSameWorldPos.setPos(shotPointText.position, textShow);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        StayInSameWorldPos.Hide();
    }
}
