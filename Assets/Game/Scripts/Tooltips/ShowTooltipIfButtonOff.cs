﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShowTooltipIfButtonOff : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Button _button;
    [SerializeField] private GameObject _tooltip;
 

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_button.interactable == false && _tooltip.activeSelf==false)
            _tooltip.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (_tooltip.activeSelf)
            _tooltip.SetActive(false);
    }
}
