﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tooltip : MonoBehaviour
{
    public float MaxScale;
    public float MinScale;
    public float ChangeSpeed;
    public GameObject ArrowUp;
    public GameObject ArrowDown;
    RectTransform _rectTransform;
    Vector3 _arrowUpStartPosition;
    Vector3 _arrowDownStartPosition;
    void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
        InitArrows();
    }
    float scale;
    bool bScaled;
    Coroutine corScaled;
    void FixedUpdate()
    {
    }

    void OnEnable()
    {
        bScaled = true;
        corScaled = StartCoroutine(startSalce());
    }

    void OnDisable()
    {
        bScaled = false;
        StopCoroutine(corScaled);
    }

    void InitArrows()
    {
        if (ArrowUp != null)
            _arrowUpStartPosition = ArrowUp.transform.localPosition;
        if (ArrowDown != null)
            _arrowDownStartPosition = ArrowDown.transform.localPosition;
    }

    IEnumerator startSalce()
    {
        while (bScaled)
        {
                scale = Mathf.PingPong(Time.realtimeSinceStartup / ChangeSpeed, MaxScale - MinScale) + MinScale;
                _rectTransform.localScale = new Vector3(scale, scale, _rectTransform.localScale.z);
            
            yield return null;
        }
    }

    public void HideAllArrows()
    {
        if (ArrowUp != null)
        {
            ArrowUp.SetActive(false);
            ArrowUp.transform.localPosition = _arrowUpStartPosition;
        }
        if (ArrowDown != null)
        {
            ArrowDown.SetActive(false);
            ArrowDown.transform.localPosition = _arrowDownStartPosition;
        }
    }
}
