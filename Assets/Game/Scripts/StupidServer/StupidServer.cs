﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StupidServerData
{
    public int ok=1;
    public ErrorData error;
    public Dictionary<string, string> data;
    public PlayerData user;
}


public static class StupidServer
{

    private static StupidServerData _serverData;
    private static Dictionary<string, IDataServerHandler> _dataHandlers;

    private static bool isWaitResponce = false;
    private static List<IDataServerHandler> _serverHandlersWaitSave;

    public static void Init()
    {
        InitHandlers();
        LoadData();
    }

    private static void InitHandlers()
    {
        _dataHandlers = new Dictionary<string, IDataServerHandler>();
        _serverHandlersWaitSave = new List<IDataServerHandler>();
        IDataServerHandler handler = new QuestsTraining();
        _dataHandlers.Add(handler.DataKey, handler);

        handler = new QuestsDaily();
        _dataHandlers.Add(handler.DataKey, handler);
    }
    private static void LoadData()
    {
        isWaitResponce = true;
        PlayerManager.DM.LoadUserData(Responce);
    }

    private static void Responce(StupidServerData data)
    {
        if (data.user != null)
            PlayerManager.SetPlayerData(data.user);

        isWaitResponce = false;
        if (data.ok == 0)
        {
            Debug.LogError("DataServer Recovery Error: " + data.error.code + " -> " + data.error.text);
            TooltipMessageServer.Show(data.error.text);
        }
        else
        {
            _serverData = data;
            UpdateDataHandlers();
        }


        NextSaveData();
    }

    private static void UpdateDataHandlers()
    {
        foreach (var key in _dataHandlers.Keys)
        {
            if (_serverData.data.ContainsKey(key))
            {
                MyLog.Log(string.Format("Получены {0} для {1}", _serverData.data[key], key));
                _dataHandlers[key].SetData(_serverData.data[key]);
            }
            else
            {
                Debug.LogWarningFormat("Нет данных для {0}", key);
                _dataHandlers[key].SetData("");
            }
        }
    }

    public static void SaveData(IDataServerHandler dataHandler)
    {
        if (isWaitResponce)
        {
            if (_serverHandlersWaitSave.Contains(dataHandler) == false)
                _serverHandlersWaitSave.Add(dataHandler);
        }
        else
        {
            isWaitResponce = true;
            PlayerManager.DM.SaveUserData(dataHandler.DataKey, dataHandler.SaveData, Responce);
        }
    }


    private static void NextSaveData()
    {
        if(isWaitResponce == false && _serverHandlersWaitSave.Count>0)
        {
            SaveData(_serverHandlersWaitSave[0]);
            _serverHandlersWaitSave.RemoveAt(0);
        }

    }

    public static void CleatData()
    {
        foreach (var key in _dataHandlers.Keys)
        {
            _dataHandlers[key].ClearData();
        }
    }
}
