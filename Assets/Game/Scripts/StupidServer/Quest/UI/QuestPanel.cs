﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestPanel : MonoBehaviour {

    private static QuestPanel instance;
    [SerializeField] private GameObject Panel;
    [SerializeField] private Transform Content;
    [SerializeField] private GameObject PrefabQuestData;
    [SerializeField] private UIGroupGameObject groupGameObject;
    [SerializeField] private UIScrollRectHideObj scrollRectHide;
    [SerializeField] private Scrollbar _scrollbarVertical;
    [SerializeField] private GameObject TextNotQuest;
    [SerializeField] private AudioSource _audioGetPrize;
    

    private List<QuestPanelData> itemsQuest;

    public QuestPanel()
    {
        itemsQuest = new List<QuestPanelData>();
        instance = this;
    }

    void Start()
    {
        QuestsTraining.onUpdateData += QuestManager_onSetData;
    }

    public void Show()
    {
        if (Panel.activeSelf==false)
        {
            Panel.SetActive(true);
            LoadQuest();
        }
    }

    private void QuestManager_onSetData()
    {
        if (Panel.activeSelf)
        {
            if(QuestsTraining.isDone)
            {
                Hide();
                return;
            }
            var value = _scrollbarVertical.value;
            LoadQuest();
            _scrollbarVertical.value = value;
            scrollRectHide.ChangeScroll();

        }
    }

    private void LoadQuest()
    {
        var quests = QuestsTraining.GetQuests(QuestType.none, Quest.QuestStatus.Active, Quest.QuestStatus.Complete, Quest.QuestStatus.WaitPrise);
        bool isWaitPrize = false;
        for (int i = 0; i < quests.Count; i++)
        {
            if (itemsQuest.Count > i)
            {
                if(itemsQuest[i].gameObject.activeSelf == false)
                {
                    itemsQuest[i].gameObject.SetActive(true);
                }
                itemsQuest[i].SetDataQuest(quests[i],_audioGetPrize);
            }
            else
            {
                GameObject go = Instantiate(PrefabQuestData, Content);
                var panel = go.GetComponent<QuestPanelData>();
                itemsQuest.Add(panel);
                panel.SetDataQuest(quests[i], _audioGetPrize);
            }
            if(isWaitPrize == false && quests[i].Status == Quest.QuestStatus.WaitPrise)
            {
                isWaitPrize = true;
            }
        }

        if (itemsQuest.Count > quests.Count)
        {
            for (int i = quests.Count; i < itemsQuest.Count; i++)
            {
                itemsQuest[i].gameObject.SetActive(false);
                //itemsQuest.RemoveAt(i);
                //i--;
            }
        }

        TextNotQuest.SetActive(quests.Count <= 0);
        groupGameObject.UpdateGroup(true);
        scrollRectHide.ChangeCountObj(true);
    }


    public static void Hide()
    {
        instance._Hide();
    }
    public void _Hide()
    {
        if (Panel.activeSelf)
        {
            Panel.SetActive(false);
        }
    }

    void OnDestroy()
    {
        QuestsTraining.onUpdateData -= QuestManager_onSetData;
    }

}
