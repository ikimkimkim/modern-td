﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestButtonShow : MonoBehaviour
{
    [SerializeField] private GameObject effect;
    [SerializeField] private QuestPanel _questTrainingPanel;
    [SerializeField] private QuestDayliPanel _questDayliPanel;

    private void Start()
    {
        UpdateEffect();
        QuestsTraining.onUpdateData += UpdateEffect;
        QuestsDaily.onUpdateData += UpdateEffect;
    }


    private void UpdateEffect()
    {
        //Debug.LogErrorFormat ("UpdateEffect {0}, {1}, {2}",QuestsTraining.isDone, QuestsDaily.GetQuests(QuestType.none, Quest.QuestStatus.WaitPrise).Count, QuestsDaily.AnyPrizeWaitTake);
        if (QuestsTraining.isDone)
            effect.SetActive(QuestsDaily.GetQuests(QuestType.none, Quest.QuestStatus.WaitPrise).Count > 0 || QuestsDaily.AnyPrizeWaitTake);
        else
            effect.SetActive(QuestsTraining.GetQuests(QuestType.none, Quest.QuestStatus.WaitPrise).Count > 0);

    }

    public void Show()
    {
        if (QuestsTraining.isDone)
            _questDayliPanel.Show();
        else
            _questTrainingPanel.Show();
    }

    private void OnDestroy()
    {
        QuestsTraining.onUpdateData -= UpdateEffect;
        QuestsDaily.onUpdateData -= UpdateEffect;
    }
}
