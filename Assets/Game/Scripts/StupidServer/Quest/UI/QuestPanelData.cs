﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestPanelData : MonoBehaviour
{

    private Quest _linkQuest;
    private AudioSource _linkAudioGetPrize;
    [SerializeField] private Image iconQuest;
    [SerializeField] private Text textName, textDesp;
    [SerializeField] private Slider sliderProgress;
    [SerializeField] private Image fonSlider, fonPanel;
    [SerializeField] private Sprite spriteActive, spriteComplete;
    [SerializeField] private Text sliderText;
    [SerializeField] private Color colorDeactive, colorActive, colorComplete;
    [SerializeField] private Button buttonGetPrize;
    [SerializeField] private GameObject effectWaitTakePrize;
    [SerializeField] private QuestPrizePanel prizePanel;
    [SerializeField] private GameObject _fog, prizeGiveCheck;
    [SerializeField] private ParticleSystem particleGetPrize;

    public void SetDataQuest(Quest quest, AudioSource sourceGetPrize)
    {
        _linkQuest = quest;
        _linkAudioGetPrize = sourceGetPrize;
        textName.text = quest.Name;
        textDesp.text = quest.Desp;
        sliderProgress.value = quest.Procent;
        fonSlider.sprite = quest.Procent >= 1 ? spriteComplete : spriteActive;
        switch (quest.Status)
        {
            case Quest.QuestStatus.Active:
                sliderText.text = quest.Progress + "/" + quest.MaxProgress;
                fonPanel.color = colorActive;
                buttonGetPrize.gameObject.SetActive(false);
                buttonGetPrize.interactable = false;
                prizeGiveCheck.SetActive(false);
                _fog.SetActive(false);
                break;
            case Quest.QuestStatus.WaitPrise:
                sliderText.text = quest.MaxProgress + "/" + quest.MaxProgress;
                fonPanel.color = colorActive;
                buttonGetPrize.gameObject.SetActive(true);
                buttonGetPrize.interactable = true;
                prizeGiveCheck.SetActive(false);
                _fog.SetActive(false);
                break;
            case Quest.QuestStatus.Complete:
                sliderText.text = quest.MaxProgress + "/" + quest.MaxProgress;
                fonPanel.color = colorComplete;
                buttonGetPrize.gameObject.SetActive(false);
                buttonGetPrize.interactable = false;
                prizeGiveCheck.SetActive(true);
                _fog.SetActive(true);
                break;
            case Quest.QuestStatus.Hide:
                sliderText.text = "0/" + quest.MaxProgress;
                fonPanel.color = colorDeactive;
                buttonGetPrize.gameObject.SetActive(false);
                buttonGetPrize.interactable = false;
                prizeGiveCheck.SetActive(false);
                _fog.SetActive(false);
                break;
        }
        effectWaitTakePrize.SetActive(quest.Status == Quest.QuestStatus.WaitPrise);
        prizePanel.SetPrizeData(quest.prize);
        StartCoroutine(TextureDB.instance.load(quest.UrlIcon, value => { iconQuest.sprite = value; }));
    }

    public void OnGetPrize()
    {
        if (_linkQuest.Status == Quest.QuestStatus.WaitPrise)
        {
            if (_linkQuest.prizeType == PrizeType.Score)
            {
                if (PlayerManager.isSoundOn())
                    _linkAudioGetPrize.Play();
                QuestsDaily.TakeQuestPrize(_linkQuest.ID);
                QuestDayliPanel.StartAnim(int.Parse(_linkQuest.prizeValue));
            }
            else
            {
                PlayerManager.DM.GiveGoods(_linkQuest.prizeType, _linkQuest.prizeValue, Responce);
            }
        }
    }

    private void Responce(ResponseData data)
    {
        PlayerManager.SetPlayerData(data.user);

        if (data.ok == 1)
        {
            if(PlayerManager.isSoundOn())
                _linkAudioGetPrize.Play();
            _linkQuest.CompleteGetPrize();
            particleGetPrize.Play();
        }
        else
        {
            Debug.LogError("Quest Recovery Error: " + data.error.code + " -> " + data.error.text);
            TooltipMessageServer.Show(data.error.text);
        }

        if (data.prizes != null)
        {
            var type = (Chest.TypeChest)(int.Parse(_linkQuest.prizeValue) - 1);
            ChestManager.instance.openPanel.SetResponceDataAndShow(-1, type, data.prizes);
            QuestPanel.Hide();
        }
    }
}
