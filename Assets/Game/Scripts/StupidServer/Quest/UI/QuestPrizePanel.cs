﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;
using UnityEngine.UI;

public class QuestPrizePanel : MonoBehaviour
{
    [SerializeField] private GameObject goldPanel,crystalPanel,chestPanel,craftResPanel,cardPanel, scorePanel;
    [SerializeField] private Text textGoldPanel, textCrystalPanel, textCraftResPanel, textCountCardPanel, textScoreCount;
    [SerializeField] private Image imageChest,imageCrafRes;
    [SerializeField] private CardUI cardInfo;
    [SerializeField] private GameObject _tooltip;
    [SerializeField] private Text _tooltipText;

    public void SetPrizeData(PrizeData prizeData)
    {
        HideAll();
        switch (prizeData.Type)
        {
            case PrizeType.Gold:
                SetPrizeGold(prizeData as PrizeGold);
                break;
            case PrizeType.Crystal:
                SetPrizeCrystal(prizeData as PrizeCrystal);
                break;
            case PrizeType.Chest:
                SetPrizeChest(prizeData as PrizeChest);
                break;

            case PrizeType.ResourcesCraft:
                SetPrizeRes(prizeData as PrizeCraftRes);
                break;
            case PrizeType.Card:
                SetPrizeCard(prizeData as PrizeCard);
                break;
            case PrizeType.Score:
                SetPrizeScore(prizeData as PrizeDailyScore);
                break;
        }
    }

    private void HideAll()
    {
        goldPanel.SetActive(false);
        crystalPanel.SetActive(false);
        chestPanel.SetActive(false);
        craftResPanel.SetActive(false);
        cardPanel.SetActive(false);
        if(scorePanel!=null)
            scorePanel.SetActive(false);
    }

    private void SetPrizeGold(PrizeGold prize)
    {
        goldPanel.SetActive(true);
        _tooltipText.text = string.Format("<color=#fba81d>Золотые монеты</color>\n\rЗа них вы улучшаете свою колоду карт.");
        textGoldPanel.text = prize.Value;
    }

    private void SetPrizeCrystal(PrizeCrystal prize)
    {
        crystalPanel.SetActive(true);
        _tooltipText.text = string.Format("<color=#fba81d>Кристалы</color>\n\rОни нужны для покупок в магазине и быстрое открытие сундуков.");
        textCrystalPanel.text = prize.Value;
    }

    private void SetPrizeChest(PrizeChest prize)
    {
        chestPanel.SetActive(true);
        StartCoroutine(TextureDB.instance.load(Chest.getIconUrlsChest[(int)prize.GetTypeChest], value => { imageChest.sprite = value; }));
        _tooltipText.text = string.Format("<color=#fba81d>{0}</color>\n\r{1}",Chest.getNameChest(prize.GetTypeChest),Chest.DespAllChest);
    }
    private void SetPrizeRes(PrizeCraftRes prize)
    {
        craftResPanel.SetActive(true);
        var resDB = DataBase<TDTKItem>.Load(DataBase<TDTKItem>.NameBase.Resources);
        if (prize.GetRes1 > 0)
        {
            var item = resDB.Find(j => j.ID == 1);
            imageCrafRes.sprite = item.icon;
            _tooltipText.text = string.Format("<color=#fba81d>{0}</color>\n\r{1}", item.name, item.desp);
            textCraftResPanel.text = prize.GetRes1.ToString();
        }
        else if (prize.GetRes2 > 0)
        {
            var item = resDB.Find(j => j.ID == 2);
            imageCrafRes.sprite = item.icon;
            _tooltipText.text = string.Format("<color=#fba81d>{0}</color>\n\r{1}", item.name, item.desp);
            textCraftResPanel.text = prize.GetRes2.ToString();
        }
        else if (prize.GetRes3 > 0)
        {
            var item = resDB.Find(j => j.ID == 3);
            imageCrafRes.sprite = item.icon;
            _tooltipText.text = string.Format("<color=#fba81d>{0}</color>\n\r{1}", item.name, item.desp);
            textCraftResPanel.text = prize.GetRes3.ToString();
        }
        else if (prize.GetRes4 > 0)
        {
            var item = resDB.Find(j => j.ID == 4);
            imageCrafRes.sprite = item.icon;
            _tooltipText.text = string.Format("<color=#fba81d>{0}</color>\n\r{1}", item.name, item.desp);
            textCraftResPanel.text = prize.GetRes4.ToString();
        }
    }

    private void SetPrizeCard(PrizeCard prize)
    {
        cardPanel.SetActive(true);

        textCountCardPanel.text = string.Format("X{0}", prize.GetCountCard);

        var bdTower = TowerDB.Load();
        var bdAbility = AbilityDB.Load();
        var bdHero = HeroDB.Load();
        var dbCardUpgrade = DataBase<CardUpgradeData>.Load(DataBase<CardUpgradeData>.NameBase.CardUpgrade);
        TowersData towersData = new TowersData();
        towersData.type = prize.GetTypeCard;
        towersData.rarity = (int)prize.GetRarenessCard + 1;
        towersData.properties = new Dictionary<string, float[]>();
        towersData.set_properties = new Dictionary<string, float[]>();

        Card card;
        if (towersData.type > 200)
        {
            if (towersData.type > 400)
            {
                if (towersData.type > 500)
                {
                    card = new Card(towersData, dbCardUpgrade[towersData.type - 501]);
                    _tooltipText.text = string.Format("<color=#fba81d>{0}</color>\r\n{1}", dbCardUpgrade[towersData.type - 501].data[0].name, dbCardUpgrade[towersData.type - 501].generalDesp);
                }
                else
                {
                    card = new Card(towersData, bdHero[towersData.type - 401].gameObject, _CardType.Hero, towersData.type - 401);
                    _tooltipText.text = string.Format("<color=#fba81d>{0}</color>", bdHero[towersData.type - 401].unitName);
                }
            }
            else
            {
                card = new Card(towersData, bdAbility[towersData.type - 201]);
                _tooltipText.text = string.Format("<color=#fba81d>{0}</color>", bdAbility[towersData.type - 201].GetName());
            }
        }
        else
        {
            card = new Card(towersData, bdTower[towersData.type - 1].gameObject, _CardType.Tower, 0);
            _tooltipText.text = string.Format("<color=#fba81d>{0}</color>", bdTower[towersData.type - 1].unitName);
        }
        cardInfo.SetCard(card);
    }


    private void SetPrizeScore(PrizeDailyScore prize)
    {
        scorePanel.SetActive(true);
        textScoreCount.text = prize.Value;
        _tooltipText.text = string.Format("<color=#fba81d>Баллы</color>\r\nНужны для получения ежедневных и еженедельных наград", prize.Value);
    }
}
