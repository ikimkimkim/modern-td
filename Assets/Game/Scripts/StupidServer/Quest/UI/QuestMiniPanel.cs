﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class QuestMiniPanel : MonoBehaviour
{

    [SerializeField] private GameObject panel;
    [SerializeField] private Text textQuests;
    [SerializeField] private int _showCount = 3;
    [SerializeField] private Toggle _toggleShow;
    private List<Quest> _questsShow;
    private List<string> _questsIdShow;
    private static bool isShowText = true;
    [SerializeField] private QuestType ShowQuestType = QuestType.none;

    void Start()
    {
        _questsShow = new List<Quest>();

        if (ShowQuestType == QuestType.Level)
            SelectedLockQuestsInLevel();

        QuestsDaily.onUpdateData += QuestManager_onSetData;
        QuestsTraining.onUpdateData += QuestManager_onSetData;
        Quest.onChangeProgressQuest += Quest_onChangeProgressQuest;
        if (_toggleShow.isOn != !isShowText)
            _toggleShow.isOn = !isShowText;
        else
            UpdateData();
    }

    private void SelectedLockQuestsInLevel()
    {
        if (_questsIdShow == null)
            _questsIdShow = new List<string>();
        else
            _questsIdShow.Clear();
        var quests = QuestsTraining.isDone ?
            QuestsDaily.GetQuests(ShowQuestType, Quest.QuestStatus.Active) :
            QuestsTraining.GetQuests(ShowQuestType, Quest.QuestStatus.Active);
        Debug.Log("SelectedLockQuestsInLevel " + quests.Count);
        for (int i = 0; i < quests.Count; i++)
        {
            _questsIdShow.Add(quests[i].ID);
        }
    }

    private void Quest_onChangeProgressQuest(string id)
    {
        if (_questsShow.Exists(i => i.ID == id))
        {
            SetQuests(_questsShow.OrderByDescending(i => i.Procent).ToArray());
        }
        else
            UpdateData();
    }

    private void QuestManager_onSetData()
    {
        switch (ShowQuestType)
        {
            case QuestType.MainMap:
            case QuestType.none:
                UpdateData();
                break;
            case QuestType.Level:
                if (_questsIdShow.Count <= 0)
                    SelectedLockQuestsInLevel();
                UpdateData();
                break;
        }
    }

    void OnDestroy()
    {
        QuestsDaily.onUpdateData -= QuestManager_onSetData;
        QuestsTraining.onUpdateData -= QuestManager_onSetData;
        Quest.onChangeProgressQuest -= Quest_onChangeProgressQuest;
        _questsShow.Clear();
    }

    private void UpdateData()
    {
        List<Quest> quests = new List<Quest>();
        switch (ShowQuestType)
        {
            case QuestType.MainMap:
            case QuestType.none:
                quests = QuestsTraining.isDone ?
                    QuestsDaily.GetQuests(ShowQuestType, Quest.QuestStatus.Active, Quest.QuestStatus.WaitPrise) :
                    QuestsTraining.GetQuests(ShowQuestType, Quest.QuestStatus.Active, Quest.QuestStatus.WaitPrise);
                break;
            case QuestType.Level:
                quests = QuestsTraining.isDone ?
                    QuestsDaily.GetQuests(_questsIdShow.ToArray()) :
                    QuestsTraining.GetQuests(_questsIdShow.ToArray());
                break;
        }

        if (quests.Count > 0)
        {
            //Debug.Log("QuestMiniPanel show");
            Show();
            SetQuests(quests.OrderByDescending(i => i.Procent).Take(_showCount).ToArray());
        }
        else
        {
            //Debug.Log("QuestMiniPanel hide");
            Hide();
        }
    }

    private void SetQuests(Quest[] quest)
    {
        _questsShow.Clear();
        _questsShow.AddRange(quest);
        textQuests.text = "";
        if (isShowText == false)
            return;
        for (int i = 0; i < quest.Length; i++)
        {
            switch (quest[i].Status)
            {
                case Quest.QuestStatus.WaitPrise:
                case Quest.QuestStatus.Complete:
                    textQuests.text += string.Format("<color=#3cd10a>{0}</color>\r\n{1}.  {2}\\{3}\r\n",
                        quest[i].Name, quest[i].Desp, quest[i].MaxProgress, quest[i].MaxProgress);
                    break;
                case Quest.QuestStatus.Active:
                    textQuests.text += string.Format("<color=#FFA500>{0}</color>\r\n{1}.  {2}\\{3}\r\n",
                        quest[i].Name, quest[i].Desp, quest[i].Progress, quest[i].MaxProgress);
                    break;
            }
        }
    }

    public void OnToggle(bool check)
    {
        isShowText = !check;
        UpdateData();
    }

    private void Hide()
    {
        panel.SetActive(false);
    }
    private void Show()
    {
        panel.SetActive(true);
    }
}
