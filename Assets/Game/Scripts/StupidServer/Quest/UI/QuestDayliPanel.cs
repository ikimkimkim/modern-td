﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class QuestDayliPanel : MonoBehaviour
{
    [System.Serializable]
    private struct PrizeDay
    {
        public Button button;
        public GameObject check, effect, tooltipTake;
        public ParticleSystem particleEffect;
        public Text information, price;
        public int scoreOpen;
    }
    [System.Serializable]
    private struct PrizeWeek
    {
        public Image prize;
        public Button button;
        public Image progress;
        public GameObject check, effect, tooltipTake;
        public ParticleSystem particleEffect;
        public Text information, price;
        public int scoreOpen;
    }

    [SerializeField] private GameObject _panel;
    [SerializeField] private Slider _dayProgress;
    [SerializeField] private Text _textDayProgress;
    [SerializeField] private Text _textDay;
    [SerializeField] private PrizeDay[] _prizesDay;
    [SerializeField] private Text _textWeek;
    [SerializeField] private PrizeWeek[] _prizesWeek;

    [SerializeField] private Transform Content;
    [SerializeField] private GameObject PrefabQuestData;
    [SerializeField] private AudioSource _audioGetPrize;
    [SerializeField] private UIGroupGameObject groupGameObject;
    [SerializeField] private UIScrollRectHideObj scrollRectHide;
    private List<QuestPanelData> _itemsQuest;
    private Animator _animator;
    private static QuestDayliPanel _instance;

    private bool _IsAnimOn;
    [SerializeField] private GameObject[] _arrayObjScores;

    private void Start()
    {
        _itemsQuest = new List<QuestPanelData>();
        _animator = GetComponent<Animator>();
        _instance = this;
        UpdateData();
        QuestsDaily.onUpdateData += UpdateData;
    }
    void OnDestroy()
    {
        QuestsDaily.onUpdateData -= UpdateData;
    }

    public void Show()
    {
        _panel.SetActive(true);
        UpdateData();
    }

    private void UpdateData()
    {
        if (_IsAnimOn || _panel.activeSelf == false)
            return;
        InitDay();
        InitWeek();
        InitQuest();
    }

    public void Hide()
    {
        _panel.SetActive(false);
    }


    private void InitDay()
    {
        _dayProgress.maxValue = QuestsDaily.ScoreDayMax;
        _dayProgress.value = QuestsDaily.ScoreDay;
        _textDayProgress.text = string.Format("{0}/{1}", _dayProgress.value, _dayProgress.maxValue);

        for (int i = 0; i < _prizesDay.Length; i++)
        {
            bool prizeIsReceived = QuestsDaily.isPrizeDayReceived(i);
            int score = QuestsDaily.MaxScorePrizeDay(i);
            _prizesDay[i].check.SetActive(prizeIsReceived);
            bool prizeCanTake = prizeIsReceived == false && score <= _dayProgress.value;
            _prizesDay[i].effect.SetActive(prizeCanTake);
            _prizesDay[i].tooltipTake.SetActive(prizeCanTake);
            _prizesDay[i].button.interactable = prizeCanTake;
            if (prizeCanTake)
                _prizesDay[i].particleEffect.Play();
            else
                _prizesDay[i].particleEffect.Stop();

            _prizesDay[i].price.text = score.ToString();

            if (prizeIsReceived)
                _prizesDay[i].information.text = string.Format("<color=#fba81d>Награда</color>\r\n{0}\r\n<color=#fba81d>Получена</color>", QuestsDaily.DespPrizeDay(i));
            else
                _prizesDay[i].information.text = string.Format("<color=#fba81d>Награда</color>\r\n{0}", QuestsDaily.DespPrizeDay(i));

        }

        _textDay.text = string.Format("Прогресс на сегодня ({0})", QuestsDaily.EndDataDay);
    }

    private void InitWeek()
    {
        int TotalScore = QuestsDaily.ScoreTotal;
        for (int i = 0; i < _prizesWeek.Length; i++)
        {
            bool prizeIsReceived = QuestsDaily.isPrizeWeekReceived(i);
            int score = QuestsDaily.MaxScorePrizeWeek(i);
            _prizesWeek[i].check.SetActive(prizeIsReceived);
            bool prizeCanTake = prizeIsReceived == false && score <= TotalScore;
            _prizesWeek[i].effect.SetActive(prizeCanTake);
            _prizesWeek[i].tooltipTake.SetActive(prizeCanTake);
            _prizesWeek[i].button.interactable = prizeCanTake;


            _prizesWeek[i].progress.fillAmount = Mathf.Clamp(TotalScore / (float)score, 0f, 1f);
            _prizesWeek[i].price.text = score > TotalScore ? string.Format("{0}/{1}", TotalScore, score) : "";

            if (prizeCanTake)
                _prizesWeek[i].particleEffect.Play();
            else
                _prizesWeek[i].particleEffect.Stop();

            if (prizeIsReceived)
                _prizesWeek[i].information.text = string.Format("<color=#fba81d>Награда</color>\r\n{0}\r\n<color=#fba81d>Получена</color>", QuestsDaily.DespPrizeWeek(i));
            else
                _prizesWeek[i].information.text = string.Format("<color=#fba81d>Награда</color>\r\n{0}", QuestsDaily.DespPrizeWeek(i));

        }
        _textWeek.text = string.Format("Награды можно получить до {0}", QuestsDaily.EndDataWeek);
    }


    private void InitQuest()
    {
        List<Quest> quests = new List<Quest>();
        quests.AddRange(QuestsDaily.GetQuests(QuestType.none, Quest.QuestStatus.Active, Quest.QuestStatus.WaitPrise).OrderBy(i => i.Status).ToArray());
        quests.AddRange(QuestsDaily.GetQuests(QuestType.none, Quest.QuestStatus.Complete));

        bool isWaitPrize = false;
        for (int i = 0; i < quests.Count; i++)
        {
            if (_itemsQuest.Count > i)
            {
                if (_itemsQuest[i].gameObject.activeSelf == false)
                {
                    _itemsQuest[i].gameObject.SetActive(true);
                }
                _itemsQuest[i].SetDataQuest(quests[i], _audioGetPrize);
            }
            else
            {
                GameObject go = Instantiate(PrefabQuestData, Content);
                var panel = go.GetComponent<QuestPanelData>();
                _itemsQuest.Add(panel);
                panel.SetDataQuest(quests[i], _audioGetPrize);
            }
            if (isWaitPrize == false && quests[i].Status == Quest.QuestStatus.WaitPrise)
            {
                isWaitPrize = true;
            }
        }

        if (_itemsQuest.Count > quests.Count)
        {
            for (int i = quests.Count; i < _itemsQuest.Count; i++)
            {
                _itemsQuest[i].gameObject.SetActive(false);
                //itemsQuest.RemoveAt(i);
                //i--;
            }
        }
        quests.Clear();
        //EffectButtonCard.SetActive(isWaitPrize);
        //TextNotQuest.SetActive(quests.Count <= 0);
        groupGameObject.UpdateGroup(true);
        scrollRectHide.ChangeCountObj(true);
    }

    

    public void TakePrizeDay(int index)
    {
        if (QuestsDaily.isPrizeDayReceived(index))
        {
            TooltipMessageServer.Show("Награда уже получена", 2);
            return;
        }

        _prizesDay[index].particleEffect.Stop();
        _prizesDay[index].effect.SetActive(false);
        _prizesDay[index].tooltipTake.SetActive(false);
        QuestsDaily.TakePrizeDay(index);
    }


    public void TakePrizeWeek(int index)
    {
        if (QuestsDaily.isPrizeWeekReceived(index))
        {
            TooltipMessageServer.Show("Награда уже получена", 2);
            return;
        }

        _prizesWeek[index].particleEffect.Stop();
        _prizesWeek[index].effect.SetActive(false);
        _prizesWeek[index].tooltipTake.SetActive(false);
        QuestsDaily.TakePrizeWeek(index);
    }



    private void ShowScoreObj(int count)
    {
        count = Mathf.Clamp(count, 0, _arrayObjScores.Length);
        if (count == _arrayObjScores.Length)
        {
            for (int i = 0; i < count; i++)
            {
                _arrayObjScores[i].SetActive(true);
            }
            return;
        }

        for (int i = 0; i < _arrayObjScores.Length; i++)
        {
            _arrayObjScores[i].SetActive(false);
        }
        int index;
        while(count>0)
        {
            index = Random.Range(0, _arrayObjScores.Length);
            if(_arrayObjScores[index].activeSelf == false)
            {
                _arrayObjScores[index].SetActive(true);
                count--;
            }
        }
    }


    public static void StartAnim(int countStars)
    {
        _instance._StartAnim(countStars);
    }
    private void _StartAnim(int countStars)
    {
        _IsAnimOn = true;
        _animator.SetTrigger("Show");
        ShowScoreObj(countStars);
    }


    private void _EndAnim()
    {
        _IsAnimOn = false;
        UpdateData();
    }


}
