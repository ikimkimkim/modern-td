﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using TDTK;
using UnityEngine.SceneManagement;

public class QuestsTraining : IDataServerHandler
{
    public static event Action onUpdateData;
    private static QuestsTraining _instance;

    public string DataKey { get { return "1"; } }
    public string SaveData
    {
        get
        {
            return CompilationSaveData();
        }
    }
    private string _setData;
    private Dictionary<QuestID, Quest> _questList;
    private Dictionary<string, string> _questDataSave;

    private bool isNeedSaveChange = false;

    public static bool isDone { get { return _instance._setData == "Done"; } }

    public QuestsTraining()
    {
        _instance = this;

        _questDataSave = new Dictionary<string, string>();
        _questList = new Dictionary<QuestID, Quest>();

        Quest.onChangeProgressQuest += Quest_onChangeProgressQuest;
        SpawnManager.onWaveClearedE += SpawnManager_onWaveClearedE;
        GameControl.onGameOverE += GameControl_onGameOverE;
        
    }

    private void GameControl_onGameOverE(int stars, ResponseData prizes)
    {
        if (isNeedSaveChange)
        {
            Debug.Log("QM GameOver save change");
            _SaveChangeQuest();
        }
    }

    private void SpawnManager_onWaveClearedE(int waveID)
    {
        if (isNeedSaveChange)
        {
            Debug.Log("QM WaveCleared save change");
            _SaveChangeQuest();
        }
    }

    private void Quest_onChangeProgressQuest(string id)
    {
        if (SceneManager.GetActiveScene().buildIndex == 1)//Главная сцена
        {
            _SaveChangeQuest();
        }
        else
        {
            isNeedSaveChange = true;
        }
    }

    #region Editor
    public static void ClearAll()
    {
        _instance._SetData(-3);
    }
    public static void Done()
    {
        _instance._SetData(-2);
    }
    public static void CompliteAll()
    {
        _instance._SetData(-1);
    }
    public static void StartAll()
    {
        _instance._SetData(0);
    }
    private void _SetData(int value)
    {
        if (ShowOnlyAdmin.isAdmin == false)
            return;
        _setData = "";
        for (int i = 1; i <= 15; i++)
        {
            var id = (QuestID)i;
            if (_questList.ContainsKey(id) == false)
                _questList.Add(id, Quest.GetTrainingQuest(id));
        }
        foreach (var key in _questList.Keys)
        {
            _questList[key].SetData(value.ToString());
        }
        SaveChangeQuest();
        QuestsDaily.SaveChangeQuest();
    }
    #endregion



    private string CompilationSaveData()
    {
        bool _isDone = true;
        foreach (var key in _questList.Keys)
        {
            if (_isDone && _questList[key].Status != Quest.QuestStatus.Complete)
                _isDone = false;

            var strKey = Quest.GetStrID(key);

            if (_questList[key].Status != Quest.QuestStatus.Hide)
            {
                if (_questDataSave.ContainsKey(strKey))
                    _questDataSave[strKey] = _questList[key].SaveData;
                else
                    _questDataSave.Add(_questList[key].ID, _questList[key].SaveData);
            }
            else
            {
                if (_questDataSave.ContainsKey(strKey))
                    _questDataSave.Remove(strKey);
            }
        }
        if (_isDone)
            return "Done";
        else
            return StringSerializationAPI.Serialize(typeof(Dictionary<string, string>), _questDataSave);
    }

    public static void ReturnData()
    {
        _instance._ReturnData();
    }
    private void _ReturnData()
    {
        Debug.Log("QM ReturnData");
       // SetData(_setData);
    }

    public void SetData(string data)
    {
        _setData = data;


        if (isDone)
        {
            ClearData();
            if (onUpdateData != null)
                onUpdateData();
            return;
        }

        if (_setData != "Done" && _setData.Length > 4)
        {
            Dictionary<string, string> questData;
            try
            {
                questData = (Dictionary<string, string>)StringSerializationAPI.Deserialize(typeof(Dictionary<string, string>), data);
            }
            catch
            {
                Debug.LogError("Data pars error, try pars old structure");
                Dictionary<string, int> questDataOld = (Dictionary<string, int>)StringSerializationAPI.Deserialize(typeof(Dictionary<string, int>), data);
                questData = new Dictionary<string, string>();
                foreach (var key in questDataOld.Keys)
                {
                    questData.Add(key, questDataOld[key].ToString());
                }
            }

            foreach (var key in questData.Keys)
            {
                var id = Quest.GetID(key);
                if (_questList.ContainsKey(id))
                {
                    _questList[id].SetData(questData[key]);
                }
                else
                {
                    _questList.Add(id, Quest.GetTrainingQuest(id));
                    _questList[id].SetData(questData[key]);
                }
            }
        }
        else if(PlayerManager._instance.GetLevelsCountComplele() >= 14)
        {
            _setData = "Done";
            ClearData();
            return;
        }

        if (onUpdateData!=null)
            onUpdateData();

    }

    public static List<Quest> GetQuests(params string[] ids)
    {
        if (_instance == null)
        {
            Debug.LogWarning("QuestManaget instance is null");
            return null;
        }
        return _instance._GetQuests(ids);
    }
    private List<Quest> _GetQuests(params string[] ids)
    {
        List<Quest> quests = new List<Quest>();
        foreach (var key in _questList.Keys)
        {
            var strKey = Quest.GetStrID(key);
            if(ids.Contains(strKey))
                quests.Add(_questList[key]);
        }
        return quests;
    }


    public static List<Quest> GetQuests(QuestType type, params Quest.QuestStatus[] status)
    {
        if (_instance == null)
        {
            Debug.LogWarning("QuestManaget instance is null");
            return null;
        }
        return _instance._GetQuests(type, status);
    }
    private List<Quest> _GetQuests(QuestType type, params Quest.QuestStatus[] status)
    {
        List<Quest> quests = new List<Quest>();
        foreach (var key in _questList.Keys)
        {
            if(type!=QuestType.none && _questList[key].Type!=type)
            {
                continue;
            }
            if(status.Contains(_questList[key].Status))
            {
                quests.Add(_questList[key]);
            }
        }
        return quests;
    }

    public static void StartQuest(QuestID ID)
    {
        if (_instance == null)
        {
            Debug.LogWarning("QuestManaget instance is null");
            return;
        }
        _instance._StartQuest(ID);
    }
    private void _StartQuest(QuestID ID)
    {
        Debug.LogFormat("StartQuest {0}", ID);

        if (_questList.ContainsKey(ID))
        {
            if(_questList[ID].Status == Quest.QuestStatus.Hide)
                _questList[ID].StartQuest();
            else
                Debug.LogErrorFormat("StartQuest error: Status quest {0} {1}", ID, _questList[ID].Status);

        }
        else
        {
            _questList.Add(ID, Quest.GetTrainingQuest(ID));
            _questList[ID].StartQuest();
        }
    }

    public static void SaveChangeQuest()
    {
        if (_instance == null)
        {
            Debug.LogWarning("QuestManaget instance is null");
            return;
        }
        _instance._SaveChangeQuest();
    }
    public void _SaveChangeQuest()
    {
        isNeedSaveChange = false;
        StupidServer.SaveData(this);
    }


    public void ClearData()
    {
        Debug.LogWarning("ClearData QuestsTraining!");
        Quest.onChangeProgressQuest -= Quest_onChangeProgressQuest;
        SpawnManager.onWaveClearedE -= SpawnManager_onWaveClearedE;
        GameControl.onGameOverE -= GameControl_onGameOverE;
        foreach (var key in _questList.Keys)
        {
            _questList[key].Deactive();
        }
        _questList.Clear();
    }
}
