﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum QuestType {
    none,
    MainMap,
    Level
}

public enum QuestID {
    none,
    BuildTower,
    KillFly,
    OpenChest,
    AbilityKiller,
    SummonHero,
    CraftCard,
    UpgradeAbility,
    UpgradeTower,
    UpgradeCard,
    CountRedStars,
    ChangeCardStars,
    CountLinkCombinationStars,
    ScoreSurvive,
    KillBoss,
    BuyChest,

    EnterGame,
    CompanyOrScoreSurvive,
    ToSpendGold,
    GetAnyCraftRes,
    KillCurrentCreep,
    KillAllCreep,
    HeroKiller,
    Damager,
    GetChest,
    DestoryChest,
    UpgradeChest,
    FastOrCardOpenChest,
    GetPrizeFriends,
    ToSpendCrystal,
    OpenMoreChest,
    UseAbility
}


public abstract class Quest
{
    public static event Action<string> onChangeProgressQuest;
    public static event Action<string> onCompletedQuest;
    public enum QuestStatus { Hide = -3, Complete = -2, WaitPrise = -1, Active = 0 }



    public QuestStatus Status
    {
        get
        {
            if (Progress >= 0)
                return QuestStatus.Active;

            switch (Progress)
            {
                case -1:
                    return QuestStatus.WaitPrise;
                case -2:
                    return QuestStatus.Complete;
                case -3:
                    return QuestStatus.Hide;
                default:
                    throw new NotImplementedException("Quest _Progress " + Progress);
            }
        }
    }


    public abstract string ID { get; }
    public abstract string Name { get; }
    public abstract string Desp { get; }
    public abstract string UrlIcon { get; }
    public abstract QuestType Type { get; }

    public bool isActive { get; private set; }
    public float Procent
    {
        get
        {
            switch (Status)
            {
                case QuestStatus.WaitPrise:
                case QuestStatus.Complete:
                    return 1;
                case QuestStatus.Hide:
                    return 0;
                default:
                    return (float) Progress / (float) MaxProgress;
            }
            
        }
    }

    private PrizeData _prize;
    public PrizeData prize { get { return _prize.Clone(); } }
    public PrizeType prizeType { get { return _prize.Type; } }
    public string prizeValue { get { return _prize.Value; } }

    protected int _progress = -3;
    public int Progress
    {
        get
        {
            return _progress;
        }
        protected set
        {
            _progress = value;
            if (onChangeProgressQuest != null)
                onChangeProgressQuest(ID);
        }
    }
    public abstract int MaxProgress { get; }

    //Если переопределяется SaveData то нужно переопределить и _SetData
    public virtual string SaveData { get { return Progress.ToString(); } }

    public Quest(PrizeData prizeData)
    {
        _prize = prizeData;
    }

    public void SetData(string data)
    {
        _SetData(data);
        ChangeProgress(0);
        if (Status == QuestStatus.Active)
            Active();
        else
            Deactive();
    }

    protected virtual void _SetData(string data)
    {
        var progress = int.Parse(data);
        if (Status == QuestStatus.WaitPrise && progress > (int) QuestStatus.Complete)
        {
            Debug.LogWarningFormat("Quest {0} set data {2} ignore, he is {1}!", ID, Status, progress);
            return;
        }

        if (Status == QuestStatus.Complete)
        {
            if(progress != -2)
                Debug.LogWarningFormat("Quest {0} set data {2} ignore, he is {1}!", ID, Status, progress);
            return;
        }

        if (progress>=0 && progress < _progress)
        {
            Debug.LogWarningFormat("Quest {0} set data {1} ignore, set progress less than current!", ID, progress);
            return;
        }

        _progress = progress;
    }

    public void StartQuest()
    {
        Progress = 0;
        Active();
    }
    
    public void Active()
    {
        //Debug.Log("Active " + isActive);
        if (isActive == false)
        {
            isActive = true;
            _Active();
        }
    }
    public void Deactive()
    {
        //Debug.Log("Deactive " + isActive);
        if (isActive)
        {
            isActive = false;
            _Deactive();
        }
    }
    protected abstract void _Active();
    protected abstract void _Deactive();

    protected void ChangeProgress(int add=1)
    {
        if (_progress + add >= MaxProgress)
            Complete();
        else if(add > 0)
            Progress += add;
    }

    protected void Complete()
    {
        Debug.LogFormat("CompleteProgress Quest {0}", ID);
        if (Status == QuestStatus.Active)
            Deactive();

        if (Status != QuestStatus.WaitPrise)
        {
            Progress = -1;
            if (onCompletedQuest != null)
                onCompletedQuest(ID);
        }
        else
        {
            Progress = -1;
        }
    }

    public void CompleteGetPrize()
    {
        Debug.LogFormat("CompleteGetPrize Quest {0}", ID);
        if (Status != QuestStatus.WaitPrise)
            return;
        Progress = -2;
    }





    public static QuestID GetID(string ID)
    {
        return (QuestID)int.Parse(ID);
    }
    public static string GetStrID(QuestID ID)
    {
        return ((int)ID).ToString();
    }

    public static Quest GetTrainingQuest(QuestID questID)
    {
        switch (questID)
        {
            case QuestBuildTower.questID:
                return new QuestBuildTower(3,new PrizeGold(5));
            case QuestID.KillFly:
                return new QuestKillFlyUnit(new PrizeGold(10));
            case QuestOpenChest.questID:
                return new QuestOpenChest(5, Chest.TypeChest.Silver, new PrizeChest(Chest.TypeChest.Silver));
            case QuestKillUnitAtAbility.questID:
                return new QuestKillUnitAtAbility(30,new PrizeGold(15));
            case QuestID.SummonHero:
                return new QuestSummonHero(20, new PrizeGold(20));
            case QuestCraftCard.questID:
                return new QuestCraftCard(10, new PrizeCraftRes(new[] { 200, 0, 0, 0 }));
            case QuestID.UpgradeAbility:
                return new QuestUpgradeAbility(new PrizeCraftRes(new[] { 0, 60, 0, 0 }));
            case QuestID.UpgradeTower:
                return new QuestUpgradeTower(new PrizeCraftRes(new[] { 0, 0, 30, 0 }));
            case QuestID.UpgradeCard:
                return new QuestUpgradeCard(new PrizeCard(501, _CardRareness.Usual, 10));
            case QuestID.CountRedStars:
                return new QuestRedStar(new PrizeCard(507, _CardRareness.Legendary));
            case QuestID.ChangeCardStars:
                return new QuestChangeCardStars(new PrizeCrystal(70));
            case QuestID.CountLinkCombinationStars:
                return new QuestCombinationStars(new PrizeCrystal(25));
            case QuestID.ScoreSurvive:
                return new QuestScoreSurvive(new PrizeChest(Chest.TypeChest.Gold));
            case QuestKillBoss.questID:
                return new QuestKillBoss(1,new PrizeChest(Chest.TypeChest.Gold));
            case QuestID.BuyChest:
                return new QuestBuyChest(new PrizeCrystal(400));

            default:
                throw new System.ArgumentException("QuestID unknown");
        }
    }

    public static Quest GetDeilyQuest(QuestID questID)
    {
        switch (questID)
        {
                //--- Легкие
            case QuestEnterGame.questID:
                return new QuestEnterGame(new PrizeDailyScore(5));
            case QuestCompanyOrScoreSurvive.questID:
                return new QuestCompanyOrScoreSurvive(new PrizeDailyScore(5));
            case QuestOpenChest.questID:
                return new QuestOpenChest(1, Chest.TypeChest.Wood, new PrizeDailyScore(5));
            case QuestBuildTower.questID:
                return new QuestBuildTower(20, new PrizeDailyScore(5));
            case QuestSpendGold.questID:
                return new QuestSpendGold(100, new PrizeDailyScore(5));
            case QuestCraftCard.questID:
                return new QuestCraftCard(1, new PrizeDailyScore(5));
            case QuestSummonHero.questID:
                return new QuestSummonHero(20, new PrizeDailyScore(5));
            case QuestUseAbility.questID:
                return new QuestUseAbility(30, new PrizeDailyScore(5));

            //---- Среднее
            case QuestKillBoss.questID:
                return new QuestKillBoss(3, new PrizeDailyScore(10));
            case QuestGetAnyCraftRes.questID:
                return new QuestGetAnyCraftRes(50, new PrizeDailyScore(10));
            case QuestKillUnitAtAbility.questID:
                return new QuestKillUnitAtAbility(75, new PrizeDailyScore(10));
            case QuestDamager.questID:
                return new QuestDamager(50000, new PrizeDailyScore(10));
            case QuestGetChest.questID:
                return new QuestGetChest(4, new PrizeDailyScore(10));
            case QuestKillUnitAtHero.questID:
                return new QuestKillUnitAtHero(100, new PrizeDailyScore(10));

            //---- Соложные
            case QuestKillAllCreep.questID:
                return new QuestKillAllCreep(new PrizeDailyScore(15));
            case QuestDestroyChest.questID:
                return new QuestDestroyChest(5, new PrizeDailyScore(15));
            case QuestUpgradeChest.questID:
                return new QuestUpgradeChest(1, new PrizeDailyScore(15));
            case QuestFastOrCardOpenChest.questID:
                return new QuestFastOrCardOpenChest(1, new PrizeDailyScore(15));

                //--- Платные
            case QuestSpendCrystal.questID:
                return new QuestSpendCrystal(1, new PrizeDailyScore(20));
            case QuestGetPrizeFriends.questID:
                return new QuestGetPrizeFriends(3, new PrizeDailyScore(20));
            case QuestOpenMoreChest.questID:
                return new QuestOpenMoreChest(3, Chest.TypeChest.Silver, new PrizeDailyScore(20));



            default:
                throw new System.ArgumentException("QuestID unknown");
        }
    }
}
