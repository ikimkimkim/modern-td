﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestSpendGold : Quest
{
    public const QuestID questID = QuestID.ToSpendGold;
    public override string ID { get { return Quest.GetStrID(questID); } }

    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Инвестиции"; } }
    public override string Desp { get { return string.Format("Потратить {0} монет", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestSpandGold.png"; } }
    public override QuestType Type { get { return QuestType.MainMap; } }

    private int _currentCountGold;

    public QuestSpendGold(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        _currentCountGold = PlayerManager.CountGolds;
        PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;
    }

    private void PlayerManager_InitializationСompleted()
    {
        int gold = PlayerManager.CountGolds;
        if (gold == _currentCountGold)
            return;

        if(gold>_currentCountGold)
        {
            _currentCountGold = gold;
        }
        else 
        {
            int delta = _currentCountGold - gold;
            _currentCountGold = gold;
            ChangeProgress(delta);
        }
    }

    protected override void _Deactive()
    {
        PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;
    }
}
