﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestCraftCard : Quest
{
    public const QuestID questID = QuestID.CraftCard;

    public override string ID { get { return GetStrID(questID); } }
    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Инженерный отдел"; } }
    public override string Desp { get {
            if(MaxProgress==1)
                return "Создать карту в мастерской";
            return string.Format("Создать {0} карт улучшений", _currentMax);
        } }
    public override string UrlIcon { get { return "Quest/QuestCraft.png"; } }
    public override QuestType Type { get { return QuestType.MainMap; } }
    public QuestCraftCard(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        UICraftPanel.OnCreatedCard += UICraftPanel_OnCreatedCard;
    }

    private void UICraftPanel_OnCreatedCard(int count)
    {
        ChangeProgress(count);
    }

    protected override void _Deactive()
    {
        UICraftPanel.OnCreatedCard -= UICraftPanel_OnCreatedCard;
    }
}
