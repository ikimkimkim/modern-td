﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TDTK;

public class QuestUpgradeTower : Quest
{
    public override string ID { get { return "8"; } }
    public override int MaxProgress { get { return 10; } }
    public override string Name { get { return "Технический прогресс"; } }
    public override string Desp { get { return string.Format("Улучшить башни в бою {0} раз", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestUpTower.png"; } }
    public override QuestType Type { get { return QuestType.Level; } }
    public QuestUpgradeTower(PrizeData prize) : base(prize)
    {

    }

    protected override void _Active()
    {
        UnitTower.onUpgradedE += UnitTower_onUpgradedE;
    }

    private void UnitTower_onUpgradedE(UnitTower tower)
    {
        ChangeProgress();
    }

    protected override void _Deactive()
    {
        UnitTower.onUpgradedE -= UnitTower_onUpgradedE;
    }
}
