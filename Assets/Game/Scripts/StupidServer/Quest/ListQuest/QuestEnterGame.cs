﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestEnterGame : Quest
{
    public const QuestID questID = QuestID.EnterGame;
    public override string ID
    {
        get
        {
            return Quest.GetStrID(questID);
        }
    }

    public override string Name { get { return "Добро пожаловать"; } }

    public override string Desp { get { return "Войти в игру"; } }

    public override string UrlIcon { get { return "Quest/QuestEnterGame.png"; } }

    public override QuestType Type { get { return QuestType.MainMap; } }

    public override int MaxProgress { get { return 1; } }

    public QuestEnterGame(PrizeData prizeData) : base(prizeData)
    {

    }

    protected override void _Active()
    {
        Complete();
    }

    protected override void _Deactive()
    {

    }
}
