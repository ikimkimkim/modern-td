﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class QuestCompanyOrScoreSurvive : Quest
{
    public const QuestID questID = QuestID.CompanyOrScoreSurvive;
    public override string ID
    {
        get
        {
            return Quest.GetStrID(questID);
        }
    }

    private float ScoreMax = 1500;

    public override string Name { get { return "Игрок"; } }

    public override string Desp { get { return string.Format("Пройти {0} уровня в сюжетной кампании или набрать {1} очков в режиме \"На выживание\"", MaxProgress, ScoreMax); } }

    public override string UrlIcon { get { return "Quest/QuestCompanyOrScore.png"; } }

    public override QuestType Type { get { return QuestType.MainMap; } }

    public override int MaxProgress { get { return 3; } }

    public QuestCompanyOrScoreSurvive(PrizeData prizeData) : base(prizeData)
    {

    }

    protected override void _Active()
    {
        CardAndLevelSelectPanel.OnStartLevel += CardAndLevelSelectPanel_OnStartLevel;
        UIScorePanel.OnChangeScore += UIScorePanel_OnChangeScore;
    }

    private void UIScorePanel_OnChangeScore()
    {
        int score = Mathf.RoundToInt(UIScorePanel.GetScore);
        if (score > ScoreMax)
        {
            Complete();
        }
    }

    private void CardAndLevelSelectPanel_OnStartLevel(MapType type)
    {
        if(type == MapType.Company)
        {
            ChangeProgress();
        }
    }

    protected override void _Deactive()
    {
        CardAndLevelSelectPanel.OnStartLevel -= CardAndLevelSelectPanel_OnStartLevel;
        UIScorePanel.OnChangeScore -= UIScorePanel_OnChangeScore;
    }
}
