﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestDestroyChest :  Quest
{
    public const QuestID questID = QuestID.DestoryChest;


    public override string ID { get { return GetStrID(questID); } }
    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Не нужны хлам"; } }
    public override string Desp { get { return string.Format("Разрушить {0} сундук", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestDestroyChest.png"; } }
    public override QuestType Type { get { return QuestType.MainMap; } }

    public QuestDestroyChest(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        Chest.onDestroyChest += Chest_onUpgradeChest;
    }

    private void Chest_onUpgradeChest(int id)
    {
        ChangeProgress();
    }


    protected override void _Deactive()
    {
        Chest.onDestroyChest -= Chest_onUpgradeChest;
    }
}
