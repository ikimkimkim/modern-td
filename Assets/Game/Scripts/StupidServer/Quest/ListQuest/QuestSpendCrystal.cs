﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestSpendCrystal : Quest
{
    public const QuestID questID = QuestID.ToSpendCrystal;
    public override string ID { get { return Quest.GetStrID(questID); } }

    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Нужные вложение"; } }
    public override string Desp { get {
            if (MaxProgress == 1)
                return "Потратить алмазы";
            return string.Format("Потратить {0} алмазов", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestSpandeCrystal.png"; } }
    public override QuestType Type { get { return QuestType.MainMap; } }

    private int _currentCountCrystals;

    public QuestSpendCrystal(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        _currentCountCrystals = PlayerManager.CountCrystals;
        PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;
    }

    private void PlayerManager_InitializationСompleted()
    {
        int count = PlayerManager.CountCrystals;
        if (count == _currentCountCrystals)
            return;

        if (count > _currentCountCrystals)
        {
            _currentCountCrystals = count;
        }
        else
        {
            int delta = _currentCountCrystals - count;
            _currentCountCrystals = count;
            ChangeProgress(delta);
        }
    }

    protected override void _Deactive()
    {
        PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;
    }
}
