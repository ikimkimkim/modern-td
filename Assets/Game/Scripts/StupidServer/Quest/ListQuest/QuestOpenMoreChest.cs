﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestOpenMoreChest : QuestOpenChest
{
    public new const QuestID questID = QuestID.OpenMoreChest;
    public override string ID { get { return GetStrID(questID); } }

    public override string UrlIcon { get { return Chest.getIconUrlsChest[1]; } }

    public QuestOpenMoreChest(int count, Chest.TypeChest typeChest, PrizeData prize) : base(count, typeChest, prize)
    {

    }
}
