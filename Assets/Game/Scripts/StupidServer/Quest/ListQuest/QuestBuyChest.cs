﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestBuyChest : Quest
{
    public override string ID { get { return "15"; } }
    public override int MaxProgress { get { return 1; } }
    public override string Name { get { return "Кристальное богатство"; } }
    public override string Desp { get { return string.Format("Купить сундук за кристаллы в магазине", MaxProgress); } }
    public override string UrlIcon { get { return Chest.getIconUrlsChest[(int)Chest.TypeChest.SuperMagic]; } }
    public override QuestType Type { get { return QuestType.MainMap; } }
    public QuestBuyChest(PrizeData prize) : base(prize)
    {

    }

    protected override void _Active()
    {
        StoreItem.onBuyForCrystal += StoreItem_onBuyForCrystal;
    }

    private void StoreItem_onBuyForCrystal(string item)
    {
        if (item.Contains("chest"))
        {
            Complete();
        }
    }

    protected override void _Deactive()
    {
        StoreItem.onBuyForCrystal -= StoreItem_onBuyForCrystal;
    }
}
