﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestUpgradeChest : Quest
{
    public const QuestID questID = QuestID.UpgradeChest;


    public override string ID { get { return GetStrID(questID); } }
    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Можно и лучше"; } }
    public override string Desp { get { return string.Format("Улучшить редкость {0} сундука", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestUpgradeChest.png"; } }
    public override QuestType Type { get { return QuestType.MainMap; } }

    public QuestUpgradeChest(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        Chest.onUpgradeChest += Chest_onUpgradeChest;
    }

    private void Chest_onUpgradeChest(int id)
    {
        ChangeProgress();
    }


    protected override void _Deactive()
    {
        Chest.onUpgradeChest -= Chest_onUpgradeChest;
    }
}
