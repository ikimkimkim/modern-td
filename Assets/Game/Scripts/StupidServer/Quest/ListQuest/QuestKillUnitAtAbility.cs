﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class QuestKillUnitAtAbility : Quest
{
    public const QuestID questID = QuestID.AbilityKiller;

    public override string ID { get { return GetStrID(questID); } }
    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Магический шквал"; } }
    public override string Desp { get { return string.Format("Убить {0} монстров при помощи заклинания", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestKillAtAbility.png"; } }
    public override QuestType Type { get { return QuestType.Level; } }

    public QuestKillUnitAtAbility(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        Unit.onDestroyedE += Unit_onDestroyedE;
    }

    private void Unit_onDestroyedE(Unit unit)
    {
        if (unit.KillMyUnit is Ability)
        {
            ChangeProgress();
        }
    }

    protected override void _Deactive()
    {
        Unit.onDestroyedE -= Unit_onDestroyedE;
    }
}
