﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class QuestKillBoss : Quest
{
    public const QuestID questID = QuestID.KillBoss;

    public override string ID { get { return GetStrID(questID); } }
    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Покорение великанов"; } }
    public override string Desp { get {
            if (MaxProgress == 3)
                return "Убить трёх боссов в режиме \"На прохождение\"";
            return string.Format("Убить босса в режиме \"На прохождение\" не менее {0} раз", _currentMax); } }
    public override string UrlIcon { get { return "Quest/QuestKillBoss.png"; } }
    public override QuestType Type { get { return QuestType.Level; } }
    public QuestKillBoss(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }
    protected override void _Active()
    {
        GameControl.onGameOverE += GameControl_onGameOverE;
        //Unit.onDestroyedE += Unit_onDestroyedE;
    }

    private void GameControl_onGameOverE(int stars, ResponseData prizes)
    {
        if(CardAndLevelSelectPanel.BigPortal && stars>0)
        {
            ChangeProgress();
        }
    }

    /*private void Unit_onDestroyedE(Unit unit)
    {
        if(unit.IsCreep && unit.GetUnitCreep.isBossPortal)
        {
            ChangeProgress();
        }
    }*/

    protected override void _Deactive()
    {
        GameControl.onGameOverE -= GameControl_onGameOverE;
       // Unit.onDestroyedE -= Unit_onDestroyedE;
    }
}
