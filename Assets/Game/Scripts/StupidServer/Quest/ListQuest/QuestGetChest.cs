﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class QuestGetChest : Quest
{
    public const QuestID questID = QuestID.GetChest;

    private int _currentCountChest;
    public override string ID { get { return GetStrID(questID); } }
    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Сборщик"; } }
    public override string Desp { get { return string.Format("Получить {0} сундука", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestGetChest.png"; } }
    public override QuestType Type { get { return QuestType.MainMap; } }

    public QuestGetChest(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        _currentCountChest = PlayerManager.ChestList.Count;
        PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;
    }

    private void PlayerManager_InitializationСompleted()
    {
        var countChest = PlayerManager.ChestList.Count;
        if (_currentCountChest != countChest)
        {
            if(_currentCountChest < countChest)
            {
                ChangeProgress(countChest - _currentCountChest);
            }
            _currentCountChest = countChest;
        }
    }

    protected override void _Deactive()
    {
        PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;
    }
}
