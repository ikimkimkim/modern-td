﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;
using System.Linq;

public class QuestKillAllCreep : Quest
{
    public const QuestID questID = QuestID.KillAllCreep;

    [System.Serializable]
    private struct SaveDataStruct
    {
        public int p;
        public List<int> ids;
    }
    private SaveDataStruct _saveData;

    private List<int> idNeedKill = new List<int> { 1, 3, 4, 6,7,8,9,10,12,13,14,15,16,17,18,19,20,21 };

    public override string ID { get { return GetStrID(questID); } }

    public override int MaxProgress { get { return idNeedKill.Count; } }
    public override string Name { get { return "Уникальные враги"; } }
    public override string Desp { get { return string.Format("Уничтожить по одному монстру каждого вида", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestKillFly.png"; } }
    public override QuestType Type { get { return QuestType.Level; } }

    public QuestKillAllCreep(PrizeData prize) : base(prize)
    {
        _saveData = new SaveDataStruct();
        _saveData.ids = new List<int>();
    }

    public override string SaveData
    {
        get
        {
            _saveData.p = Progress;
            return StringSerializationAPI.Serialize(typeof(SaveDataStruct), _saveData);
        }
    }

    protected override void _SetData(string data)
    {
        if (Status != QuestStatus.Active && Status != QuestStatus.Hide)
        {
            Debug.LogWarningFormat("Quest {0} set data ignore, he is {1}!", ID, Status);
            return;
        }

        try
        {
            _saveData = (SaveDataStruct)StringSerializationAPI.Deserialize(typeof(SaveDataStruct), data);
            if (_saveData.p < 0)
            {
                _progress = _saveData.p;
            }
            else
            {
                _progress = _saveData.ids.Count;
            }
        }
        catch
        {
            Debug.LogError("QuestKillAllCreep set data error!");
            _saveData = new SaveDataStruct();
            _saveData.ids = new List<int>();
            _progress = 0;
        }
    }

    protected override void _Active()
    {
        Unit.onDestroyedE += Unit_onDestroyedE;
    }

    private void Unit_onDestroyedE(Unit unit)
    {
        if (unit.IsCreep && _saveData.ids.Contains(unit.prefabID) == false && idNeedKill.Contains(unit.prefabID))
        {
            _saveData.ids.Add(unit.prefabID);
            ChangeProgress();
        }
    }

    protected override void _Deactive()
    {
        Unit.onDestroyedE -= Unit_onDestroyedE;
    }
}
