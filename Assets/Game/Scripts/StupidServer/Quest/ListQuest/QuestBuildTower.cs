﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TDTK;

public class QuestBuildTower : Quest
{
    public const QuestID questID = QuestID.BuildTower;
    public override string ID { get { return "1"; } }
    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Линия обороны"; } }
    public override string Desp { get { return string.Format("Построить {0} башен", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestTowerBuild.png"; } }
    public override QuestType Type { get { return QuestType.Level; } }

    public QuestBuildTower(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
       BuildManager.onBuildNewTowerE += BuildNewTowerE;
    }

    private void BuildNewTowerE(UnitTower tower)
    {
        ChangeProgress();
    }

    protected override void _Deactive()
    {
        BuildManager.onBuildNewTowerE -= BuildNewTowerE;
    }


}
