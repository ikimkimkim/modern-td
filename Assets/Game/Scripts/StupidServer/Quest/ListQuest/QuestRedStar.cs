﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestRedStar : Quest
{
    public override string ID { get { return "10"; } }
    public override int MaxProgress { get { return 5; } }
    public override string Name { get { return "Проторенные тропы"; } }
    public override string Desp { get { return string.Format("Пройти {0} уровней за красные звёзды", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestRedStars.png"; } }
    public override QuestType Type { get { return QuestType.MainMap; } }
    public QuestRedStar(PrizeData prize) : base(prize)
    {

    }

    protected override void _Active()
    {
        PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;
    }

    private void PlayerManager_InitializationСompleted()
    {
        var countLevel = PlayerManager.GetLevelCountComplete(4);
        if(countLevel>=MaxProgress)
        {
            Complete();
        }
        else if (Progress != countLevel)
        {
            Progress = countLevel;
        }
    }

    protected override void _Deactive()
    {
        PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;
    }
}
