﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestUpgradeCard : Quest
{
    public override string ID { get { return "9"; } }
    public override int MaxProgress { get { return 4; } }
    public override string Name { get { return "Наращивание мощи"; } }
    public override string Desp { get { return string.Format("Улучшить {0} карты до 2-го уровня или выше за монеты", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestUpCard.png"; } }
    public override QuestType Type { get { return QuestType.MainMap; } }

    public QuestUpgradeCard(PrizeData prize) : base(prize)
    {

    }

    protected override void _Active()
    {
        CardUIAllInfo.StartUpgradeCard += CardUIAllInfo_EndUpgradeCard;
    }

    private void CardUIAllInfo_EndUpgradeCard()
    {
        ChangeProgress();
    }

    protected override void _Deactive()
    {
        CardUIAllInfo.StartUpgradeCard -= CardUIAllInfo_EndUpgradeCard;
    }
}
