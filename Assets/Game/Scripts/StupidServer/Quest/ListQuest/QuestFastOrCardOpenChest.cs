﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestFastOrCardOpenChest : Quest
{
    public const QuestID questID = QuestID.FastOrCardOpenChest;


    public override string ID { get { return GetStrID(questID); } }
    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Я сказал открывайся!"; } }
    public override string Desp { get { return string.Format("Мгновенно открыть {0} сундук", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestFastOpenChest.png"; } }
    public override QuestType Type { get { return QuestType.MainMap; } }

    public QuestFastOrCardOpenChest(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        Chest.onFastOpenChest += Chest_onFastOpenChest;
        Chest.onCardOpenChest += Chest_onCardOpenChest;
    }

    private void Chest_onCardOpenChest(Chest obj)
    {
        ChangeProgress();
    }

    private void Chest_onFastOpenChest(Chest obj)
    {
        ChangeProgress();
    }


    protected override void _Deactive()
    {
        Chest.onFastOpenChest -= Chest_onFastOpenChest;
        Chest.onCardOpenChest -= Chest_onCardOpenChest;
    }
}
