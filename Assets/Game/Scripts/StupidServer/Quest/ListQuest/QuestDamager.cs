﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class QuestDamager : Quest
{
    public const QuestID questID = QuestID.Damager;

    public override string ID { get { return GetStrID(questID); } }
    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Истребитель"; } }
    public override string Desp { get { return string.Format("Нанести {0} урона монстрам (любым вооружением)", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestDamager.png"; } }
    public override QuestType Type { get { return QuestType.Level; } }

    public QuestDamager(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        Unit.onDamagedE += Unit_onDamagedE;
    }

    private void Unit_onDamagedE(Unit unit, float damag = 0)
    {
        if(unit.IsCreep)
            ChangeProgress(Mathf.CeilToInt(damag));
    }

    protected override void _Deactive()
    {
        Unit.onDamagedE -= Unit_onDamagedE;
    }
}
