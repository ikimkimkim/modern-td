﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGetAnyCraftRes : Quest
{
    public const QuestID questID = QuestID.GetAnyCraftRes;
    public override string ID { get { return GetStrID(questID); } }

    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Кладовщик"; } }
    public override string Desp { get { return string.Format("Получить {0} единиц любых ресурсов", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestGetCraftRes.png"; } }
    public override QuestType Type { get { return QuestType.MainMap; } }

    private int[] _currentCountRes;

    public QuestGetAnyCraftRes(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        _currentCountRes = new int[4];
        for (int i = 0; i < 4; i++)
        {
            _currentCountRes[i] = PlayerManager.GetCraftResources(i + 1);
        }

        PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;
    }

    private void PlayerManager_InitializationСompleted()
    {
        int countChange = 0;
        int newCountRes;
        for (int i = 0; i < 4; i++)
        {
            newCountRes = PlayerManager.GetCraftResources(i + 1);
            if(_currentCountRes[i] < newCountRes)
            {
                countChange += newCountRes - _currentCountRes[i];
                _currentCountRes[i] = newCountRes;
            }
        }

        if (countChange > 0)
            ChangeProgress(countChange);

    }

    protected override void _Deactive()
    {
        PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;
    }
}
