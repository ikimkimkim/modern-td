﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestChangeCardStars : Quest
{
    public override string ID { get { return "11"; } }
    public override int MaxProgress { get { return 5; } }
    public override string Name { get { return "Ветер перемен"; } }
    public override string Desp { get { return string.Format("Изменить предложенные характеристики в дереве улучшений {0} раз", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestChangeCardStar.png"; } }
    public override QuestType Type { get { return QuestType.MainMap; } }
    public QuestChangeCardStars(PrizeData prize) : base(prize)
    {

    }
    protected override void _Active()
    {
        UIStarPerk.onDropCard += UIStarPerk_onDropCard;
    }

    private void UIStarPerk_onDropCard()
    {
        ChangeProgress();
    }

    protected override void _Deactive()
    {
        UIStarPerk.onDropCard -= UIStarPerk_onDropCard;
    }
}
