﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class QuestUpgradeAbility : Quest
{

    public override string ID { get { return "7"; } }
    public override int MaxProgress { get { return 5; } }
    public override string Name { get { return "Магический прогресс"; } }
    public override string Desp { get { return string.Format("Улучшить заклинания в бою {0} раз", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestUpAbility.png"; } }
    public override QuestType Type { get { return QuestType.Level; } }

    public QuestUpgradeAbility(PrizeData prize) : base(prize)
    {

    }


    protected override void _Active()
    {
        AbilityManager.onUpgradeAbility += AbilityManager_onUpgradeAbility;
    }

    private void AbilityManager_onUpgradeAbility(Ability obj)
    {
        ChangeProgress();
    }

    protected override void _Deactive()
    {
        AbilityManager.onUpgradeAbility -= AbilityManager_onUpgradeAbility;
    }
}
