﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestOpenChest : Quest
{
    public const QuestID questID = QuestID.OpenChest;
    public override string ID { get { return Quest.GetStrID(questID); } }
    private int _currentMax;
    private Chest.TypeChest _minTypeChest;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Открывашка"; } }
    public override string Desp { get {
            if (MaxProgress == 1)
                return "Открыть сундук";
            return string.Format("Открыть {0} сундука (\"{1}\" или лучше)", MaxProgress, Chest.getNameChest(_minTypeChest)); } }
    public override string UrlIcon { get { return Chest.getIconUrlsChest[(int)_minTypeChest]; } }
    public override QuestType Type { get { return QuestType.MainMap; } }
    public QuestOpenChest(int count, Chest.TypeChest typeChest, PrizeData prize) : base(prize)
    {
        _currentMax = count;
        _minTypeChest = typeChest;
    }

    protected override void _Active()
    {
        Chest.onOpenChest += Chest_onOpenChest;
    }

    private void Chest_onOpenChest(Chest chest)
    {
        if(chest.Type >= _minTypeChest)
        {
            ChangeProgress();
        }
    }

    protected override void _Deactive()
    {
        Chest.onOpenChest -= Chest_onOpenChest;
    }
}
