﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class QuestScoreSurvive : Quest
{
    public override string ID { get { return "13"; } }
    public override int MaxProgress { get { return 1500; } }
    public override string Name { get { return "Напролом"; } }
    public override string Desp { get { return string.Format("Набрать {0} очков в режиме \"На выживание\"", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestSurvive.png"; } }
    public override QuestType Type { get { return QuestType.Level; } }
    public QuestScoreSurvive(PrizeData prize) : base(prize)
    {

    }
    protected override void _Active()
    {
        UIScorePanel.OnChangeScore += OnChangeScore;
    }

    private void OnChangeScore()
    {
        int score = Mathf.RoundToInt(UIScorePanel.GetScore);
        if(Progress < score)
        {
            ChangeProgress(score - Progress);
        }
    }
    
    protected override void _Deactive()
    {
        UIScorePanel.OnChangeScore -= OnChangeScore;
    }
}
