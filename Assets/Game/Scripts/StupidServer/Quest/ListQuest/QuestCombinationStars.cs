﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestCombinationStars : Quest
{
    public override string ID { get { return "12"; } }
    public override int MaxProgress { get { return 5; } }
    public override string Name { get { return "Великие комбинации"; } }
    public override string Desp { get { return string.Format("Составить комбинацию в дереве улучшений на {0} связей", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestCombinationStar.png"; } }
    public override QuestType Type { get { return QuestType.MainMap; } }
    public QuestCombinationStars(PrizeData prize) : base(prize)
    {

    }
    protected override void _Active()
    {
        PlayerManager.InitializationСompleted += PlayerManager_InitializationСompleted;
    }

    private void PlayerManager_InitializationСompleted()
    {
        var countLinks = Mathf.Max(0, StarsPerk.CountPerk(-1), StarsPerk.CountPerk(-10), StarsPerk.CountPerk(-20), StarsPerk.CountPerk(-30));

        if (MaxProgress <= countLinks)
        {
            Complete();
        }
        else if (Progress != countLinks)
        {
            Progress = countLinks;
        }
    }

    protected override void _Deactive()
    {
        PlayerManager.InitializationСompleted -= PlayerManager_InitializationСompleted;
    }
}
