﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class QuestKillUnitAtHero : Quest
{
    public const QuestID questID = QuestID.HeroKiller;

    public override string ID { get { return GetStrID(questID); } }
    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Передовая"; } }
    public override string Desp { get { return string.Format("Убить {0} монстров при помощи поддержки", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestKillAtHero.png"; } }
    public override QuestType Type { get { return QuestType.Level; } }

    public QuestKillUnitAtHero(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        Unit.onDestroyedE += Unit_onDestroyedE;
    }

    private void Unit_onDestroyedE(Unit unit)
    {
        if (unit.KillMyUnit is UnitHero)
        {
            ChangeProgress();
        }
    }

    protected override void _Deactive()
    {
        Unit.onDestroyedE -= Unit_onDestroyedE;
    }
}
