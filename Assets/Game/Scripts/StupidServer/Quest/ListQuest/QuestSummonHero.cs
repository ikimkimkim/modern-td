﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class QuestSummonHero : Quest
{
    public const QuestID questID = QuestID.SummonHero;
    public override string ID { get { return GetStrID(questID); } }
    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Живая сила"; } }
    public override string Desp { get { return string.Format("Вызвать поддержку {0} раз", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestSummonHero.png"; } }
    public override QuestType Type { get { return QuestType.Level; } }
    public QuestSummonHero(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        UIHeroSpawn.onSpawnHero += UIHeroSpawn_onSpawnHero;
    }

    private void UIHeroSpawn_onSpawnHero(UnitHero obj)
    {
        ChangeProgress();
    }

    protected override void _Deactive()
    {
        UIHeroSpawn.onSpawnHero -= UIHeroSpawn_onSpawnHero;
    }
}
