﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class QuestKillFlyUnit : Quest
{
    //private bool _isChange = false;
    public override string ID { get { return "2"; } }
    public override int MaxProgress { get { return 30; } }

    public override string Name { get { return "Воздушный щит"; } }
    public override string Desp { get { return string.Format("Убить {0} летающих монстров", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestKillFly.png"; } }
    public override QuestType Type { get { return QuestType.Level; } }

    public QuestKillFlyUnit(PrizeData prize) : base(prize)
    {

    }

    protected override void _Active()
    {
        Unit.onDestroyedE += Unit_onDestroyedE;
    }


    private void Unit_onDestroyedE(Unit unit)
    {
        if(unit is UnitCreep && ((UnitCreep)unit).flying)
        {
            ChangeProgress();
        }
    }

    protected override void _Deactive()
    {
        Unit.onDestroyedE -= Unit_onDestroyedE;
    }

}
