﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class QuestUseAbility : Quest
{
    public const QuestID questID = QuestID.UseAbility;
    public override string ID { get { return GetStrID(questID); } }
    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Заклинатель"; } }
    public override string Desp { get { return string.Format("Использовать заклинания {0} раз", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestUseAbility.png"; } }
    public override QuestType Type { get { return QuestType.Level; } }

    public QuestUseAbility(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        AbilityManager.onAbilityActivatedE += AbilityManager_onAbilityActivatedE;
    }

    private void AbilityManager_onAbilityActivatedE(Ability ab)
    {
        ChangeProgress();
    }

    protected override void _Deactive()
    {
        AbilityManager.onAbilityActivatedE -= AbilityManager_onAbilityActivatedE;
    }

}
