﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGetPrizeFriends : Quest
{
    public const QuestID questID = QuestID.GetPrizeFriends;
    public override string ID { get { return Quest.GetStrID(questID); } }

    private int _currentMax;
    public override int MaxProgress { get { return _currentMax; } }
    public override string Name { get { return "Лучшие друзья"; } }
    public override string Desp { get { return string.Format("Открыть {0} подарка от друзей", MaxProgress); } }
    public override string UrlIcon { get { return "Quest/QuestGiftFreinds.png"; } }
    public override QuestType Type { get { return QuestType.MainMap; } }


    public QuestGetPrizeFriends(int count, PrizeData prize) : base(prize)
    {
        _currentMax = count;
    }

    protected override void _Active()
    {
        RowFriend.onTakePrizeFriends += RowFriend_onTakePrizeFriends;
    }

    private void RowFriend_onTakePrizeFriends()
    {
        ChangeProgress();
    }


    protected override void _Deactive()
    {
        RowFriend.onTakePrizeFriends -= RowFriend_onTakePrizeFriends;
    }
}
