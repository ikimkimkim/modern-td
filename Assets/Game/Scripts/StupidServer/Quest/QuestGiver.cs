﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGiver : MonoBehaviour
{


	void Start ()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() == 1)
        {
            QuestsTraining.StartQuest(QuestBuildTower.questID);
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 2)
        {
            QuestsTraining.StartQuest(QuestID.KillFly);
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 3)
        {
            QuestsTraining.StartQuest(QuestID.OpenChest);
            QuestsTraining.StartQuest(QuestID.AbilityKiller);
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 4)
        {
            QuestsTraining.StartQuest(QuestID.SummonHero);
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 5)
        {
            QuestsTraining.StartQuest(QuestID.UpgradeAbility);
            QuestsTraining.StartQuest(QuestID.UpgradeCard);
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 6)
        {
            QuestsTraining.StartQuest(QuestID.UpgradeTower);
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 7)
        {
            QuestsTraining.StartQuest(QuestID.ChangeCardStars);
            QuestsTraining.StartQuest(QuestID.CountLinkCombinationStars);
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 9)
        {
            QuestsTraining.StartQuest(QuestID.ScoreSurvive);
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 10)
        {
            QuestsTraining.StartQuest(QuestID.KillBoss);
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 11)
        {
            QuestsTraining.StartQuest(QuestID.BuyChest);
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 12)
        {
            QuestsTraining.StartQuest(QuestID.CountRedStars);
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 14)
        {
            QuestsTraining.StartQuest(QuestID.CraftCard);
        }
    }
	


}
