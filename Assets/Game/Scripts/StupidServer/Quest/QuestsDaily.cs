﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TDTK;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuestsDaily : IDataServerHandler
{
    public const int ScoreDayMax = 190;

    public static event Action onUpdateData;

    [System.Serializable]
    private struct ServerData
    {
        public int score, totalScore;
        public string current, end;
        public Dictionary<string, string> quest;
        public List<int> prizeGetD, prizeGetW;
    }
    private ServerData _serverData;
    private static QuestsDaily _instance;
    public string DataKey { get { return "2"; } }

    private class Prizes
    {
        public PrizeData[] prize;
        public bool isReceived;
        public int score;
        public string Desp;
        public Prizes(PrizeData[] prizeData, int scoreNeed, string Desp)
        {
            prize = prizeData;
            score = scoreNeed;
            isReceived = false;
            this.Desp = Desp;
        }
    }
    private List<Prizes> _prizesDay, _prizesWeek;
    private DateTime DateEndDay, DateEndWeek;
    private Dictionary<QuestID, Quest> _questList;
    private bool isNeedSaveChange = false;

    private List<int[]> idsRandom = new List<int[]>
    {
        new int[] { 501, 502, 508, 509, 510, 511, 512 },//Usual
        new int[] { 502, 504, 505, 506, 508, 509, 510, 511, 512},//Magic
        new int[] { 502, 503, 504, 505, 506, 508, 509, 510, 511, 512 },//Rare
        new int[] { },//Complete
        new int[] { 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512 },//Legendary

    };

    private int _countPrizeLast;
    private int _indexCurrentPrize;
    private Prizes[] _currentGetPrizes;
    private PrizeData _lastPrize;
    private ResponseData.PrizesData _PrizesData;
    private List<PrizeChest> _prizeChests;
    private PlayerData _bufferPlayerData;


    public QuestsDaily()
    {
        _instance = this;

        _questList = new Dictionary<QuestID, Quest>();

        _serverData = new ServerData();
        _serverData.quest = new Dictionary<string, string>();
        _serverData.prizeGetD = new List<int>();
        _serverData.prizeGetW = new List<int>();

        //Quest.onCompletedQuest += Quest_onCompletedQuest;
        Quest.onChangeProgressQuest += Quest_onChangeProgressQuest;
        SpawnManager.onWaveClearedE += SpawnManager_onWaveClearedE;
        GameControl.onGameOverE += GameControl_onGameOverE;
    }

    private PrizeData[] RandomCraftCard(_CardRareness rareness, int countCard, int countPrize)
    {
       // Debug.Log("Count: " + countPrize);
        List<int> ids = new List<int>();
        ids.AddRange(idsRandom[(int)rareness]);
        if (countPrize > ids.Count)
            countPrize = ids.Count;
        PrizeData[] prizes = new PrizeData[countPrize];
        int free = countCard - countPrize;
        int count, id;
        for (int i = 0; i < countPrize-1; i++)
        {
            count = UnityEngine.Random.Range(1, free + 2);
            id = ids[UnityEngine.Random.Range(0, ids.Count)];
            prizes[i] = new PrizeCard(id, rareness, count);
            ids.Remove(id);
            free -= count - 1;
           // Debug.LogFormat("P {0} count:{1} m{2}", i, count, free);
        }
        id = ids[UnityEngine.Random.Range(0, ids.Count)];
        prizes[countPrize-1] = new PrizeCard(id, rareness, free + 1);
        // Debug.LogFormat("P {0} count:{1} m{2}", countPrize - 1, free + 1, 0);
        return prizes;
    }

    public static int ScoreDay { get { return _instance._serverData.score; } }
    public static int ScoreTotal { get { return _instance._serverData.totalScore; } }

    public static bool AnyPrizeWaitTake { get { return _instance._AnyPrizeWaitTake; } }
    public bool _AnyPrizeWaitTake { get {
            if (_prizesDay == null || _prizesWeek == null)
                return false;

            for (int i = 0; i < _prizesDay.Count; i++)
            {
                if (_prizesDay[i].isReceived == false && _prizesDay[i].score <= ScoreDay)
                    return true;
            }
            for (int i = 0; i < _prizesWeek.Count; i++)
            {
                if (_prizesWeek[i].isReceived == false && _prizesWeek[i].score <= ScoreTotal)
                    return true;
            }
            return false;
                
                } }


    #region Day Get Data
    public static string EndDataDay
    {
        get
        {
            return _instance._EndDataDay;
        }
    }
    private string _EndDataDay
    {
        get
        {
            return DateEndDay.ToString("dd.MM.yyyy");
        }
    }

    public static bool isPrizeDayReceived(int prizeIndex)
    {
        return _instance._isPrizeDayReceived(prizeIndex);
    }
    private bool _isPrizeDayReceived(int prizeIndex)
    {
        return _prizesDay[prizeIndex].isReceived;
    }

    public static PrizeData[] GetAllPrizeDay(int prizeIndex)
    {
        return _instance._GetAllPrizeDay(prizeIndex);
    }
    private PrizeData[] _GetAllPrizeDay(int prizeIndex)
    {
        return _prizesDay[prizeIndex].prize;
    }

    public static int MaxScorePrizeDay(int prizeIndex)
    {
        return _instance._MaxScorePrizeDay(prizeIndex);
    }
    public int _MaxScorePrizeDay(int prizeIndex)
    {
        return _prizesDay[prizeIndex].score;
    }

    public static string DespPrizeDay(int prizeIndex)
    {
        return _instance._DespPrizeDay(prizeIndex);
    }
    public string _DespPrizeDay(int prizeIndex)
    {
        return _prizesDay[prizeIndex].Desp;
    }
    #endregion

    #region Week Get Data
    public static string EndDataWeek
    {
        get
        {
            return _instance._EndDataWeek;
        }
    }
    private string _EndDataWeek
    {
        get
        {
            return DateEndWeek.ToString("dd.MM.yyyy");
        }
    }
    public static bool isPrizeWeekReceived(int prizeIndex)
    {
        return _instance._isPrizeWeekReceived(prizeIndex);
    }
    private bool _isPrizeWeekReceived(int prizeIndex)
    {
        return _prizesWeek[prizeIndex].isReceived;
    }

    public static PrizeData[] GetAllPrizeWeek(int prizeIndex)
    {
        return _instance._GetAllPrizeWeek(prizeIndex);
    }
    private PrizeData[] _GetAllPrizeWeek(int prizeIndex)
    {
        return _prizesWeek[prizeIndex].prize;
    }

    public static int MaxScorePrizeWeek(int prizeIndex)
    {
        return _instance._MaxScorePrizeWeek(prizeIndex);
    }
    public int _MaxScorePrizeWeek(int prizeIndex)
    {
        return _prizesWeek[prizeIndex].score;
    }

    public static string DespPrizeWeek(int prizeIndex)
    {
        return _instance._DespPrizeWeek(prizeIndex);
    }
    public string _DespPrizeWeek(int prizeIndex)
    {
        return _prizesWeek[prizeIndex].Desp;
    }
    #endregion    

    public string SaveData
    {
        get
        {
            return CompilationSaveData();
        }
    }
    private string CompilationSaveData()
    {
        if (QuestsTraining.isDone == false)
            return string.Empty;
        foreach (var key in _questList.Keys)
        {

            var strKey = Quest.GetStrID(key);

            if (_questList[key].Status != Quest.QuestStatus.Hide)
            {
                if (_serverData.quest.ContainsKey(strKey))
                    _serverData.quest[strKey] = _questList[key].SaveData;
                else
                    _serverData.quest.Add(_questList[key].ID, _questList[key].SaveData);
            }
            else
            {
                if (_serverData.quest.ContainsKey(strKey))
                    _serverData.quest.Remove(strKey);
            }
        }

        return StringSerializationAPI.Serialize(typeof(ServerData), _serverData);
    }

    public static List<Quest> GetQuests(params string[] ids)
    {
        if (_instance == null)
        {
            Debug.LogWarning("QD instance is null");
            return null;
        }
        return _instance._GetQuests(ids);
    }

    private List<Quest> _GetQuests(params string[] ids)
    {
        List<Quest> quests = new List<Quest>();
        foreach (var key in _questList.Keys)
        {
            var strKey = Quest.GetStrID(key);
            if (ids.Contains(strKey))
                quests.Add(_questList[key]);
        }
        return quests;
    }
    public static List<Quest> GetQuests(QuestType type, params Quest.QuestStatus[] status)
    {
        if (_instance == null)
        {
            Debug.LogWarning("QD instance is null");
            return null;
        }
        return _instance._GetQuests(type, status);
    }
    private List<Quest> _GetQuests(QuestType type, params Quest.QuestStatus[] status)
    {
        List<Quest> quests = new List<Quest>();
        foreach (var key in _questList.Keys)
        {
            if (type != QuestType.none && _questList[key].Type != type)
            {
                continue;
            }
            if (status.Contains(_questList[key].Status))
            {
                quests.Add(_questList[key]);
            }
        }
        return quests;
    }



    public void SetData(string data)
    {
        if (QuestsTraining.isDone == false)
            return;

        if (data.Length > 0)
        {
            int buffTotalScore = _serverData.totalScore;
            int buffDayScore = _serverData.score;
            _serverData = (ServerData)StringSerializationAPI.Deserialize(typeof(ServerData), data);

            if(buffDayScore > _serverData.score)
            {
                Debug.LogWarning("Set score less current score!");
                _serverData.score = buffDayScore;
            }

            if (buffTotalScore > _serverData.totalScore)
            {
                Debug.LogWarning("Set totalScore less current totalScore!");
                _serverData.totalScore = buffTotalScore;
            }


            if (_serverData.end.Length <= 0 || string.IsNullOrEmpty(_serverData.end))
            {
                Debug.LogWarning("Date end is null!");
                NewWeek();
                return;
            }
            DateEndDay = DateTime.Parse(_serverData.current);
            DateEndWeek = DateTime.Parse(_serverData.end);

            var NowTick = SocialManager.StartServerTime.Ticks;

            if (DateEndWeek.Ticks < NowTick)
            {
                NewWeek();
                return;
            }

            if (NowTick > DateEndDay.Ticks && NowTick < DateEndDay.AddDays(1).Ticks)
            {
                ActiveCurrentQuest();
                InitPrize();
                if (onUpdateData != null)
                    onUpdateData();
            }
            else
            {
                ActiveNewQuest();
            }
        }
        else
            NewWeek();
    }

    private void InitPrize()
    {
        _prizesDay = new List<Prizes>();
        _prizesDay.Add(new Prizes(RandomCraftCard(_CardRareness.Usual, 7, UnityEngine.Random.Range(2, 6))/* new PrizeData[] { new PrizeRandomCraftCard(_CardRareness.Usual) }*/, 30, "Семь обычных, случайных карт усиления"));
        _prizesDay.Add(new Prizes(RandomCraftCard(_CardRareness.Magic, 5, UnityEngine.Random.Range(2, 5)), 70, "Пять магических, случайных карт усиления"));
        _prizesDay.Add(new Prizes(RandomCraftCard(_CardRareness.Rare, 3, UnityEngine.Random.Range(2, 4)), 110, "Три редких, случайных карты усиления"));
        _prizesDay.Add(new Prizes(RandomCraftCard(_CardRareness.Legendary, 1, 1), 150, "Случайная легендарная карта усиления"));
        _prizesDay.Add(new Prizes(RandomCraftCard(_CardRareness.Legendary, 3, UnityEngine.Random.Range(1, 3)), 190, "Три случайных, легендарных карты усиления"));

        for (int i = 0; i < _serverData.prizeGetD.Count; i++)
        {
            _prizesDay[_serverData.prizeGetD[i]].isReceived = true;
        }


        _prizesWeek = new List<Prizes>();
        _prizesWeek.Add(new Prizes(new PrizeData[] { new PrizeChest(Chest.TypeChest.Gold) }, 550, Chest.getNameChest(Chest.TypeChest.Gold)));
        _prizesWeek.Add(new Prizes(new PrizeData[] { new PrizeChest(Chest.TypeChest.Huge) }, 900, Chest.getNameChest(Chest.TypeChest.Huge)));
        _prizesWeek.Add(new Prizes(new PrizeData[] { new PrizeChest(Chest.TypeChest.Magic) }, 1330, Chest.getNameChest(Chest.TypeChest.Magic)));


        for (int i = 0; i < _serverData.prizeGetW.Count; i++)
        {
            _prizesWeek[_serverData.prizeGetW[i]].isReceived = true;
        }

    }

    private void NewWeek()
    {
        var addDay = CountDayToNextWednesday();
        _serverData.end = SocialManager.StartServerTime.AddDays(addDay).ToShortDateString();
        _serverData.totalScore = 0;
        _serverData.prizeGetW.Clear();

        Debug.Log("New end week date " + _serverData.end);
        ActiveNewQuest();
    }

    private int CountDayToNextWednesday()
    {
        switch(SocialManager.StartServerTime.DayOfWeek)
        {
            case DayOfWeek.Monday:
                return 2;
            case DayOfWeek.Tuesday:
                return 1;
            case DayOfWeek.Wednesday:
                return 7;
            case DayOfWeek.Thursday:
                return 6;
            case DayOfWeek.Friday:
                return 5;
            case DayOfWeek.Saturday:
                return 4;
            case DayOfWeek.Sunday:
                return 3;
        }
        return 0;
    }


    private void ActiveCurrentQuest()
    {
        Debug.Log("QD ActiveCurrentQuest");
        foreach (var key in _serverData.quest.Keys)
        {
            var id = Quest.GetID(key);
            if (_questList.ContainsKey(id))
            {
                _questList[id].SetData(_serverData.quest[key]);
            }
            else
            {
                _questList.Add(id, Quest.GetDeilyQuest(id));
                _questList[id].SetData(_serverData.quest[key]);
            }
        }
    }

    private bool isActiveNewQuesst = false;
    private void ActiveNewQuest()
    {
        isActiveNewQuesst = true;
        _serverData.current = SocialManager.StartServerTime.ToShortDateString();
        _serverData.score = 0;
        _serverData.prizeGetD.Clear();
        Debug.Log("New current date " + _serverData.current);

        ClearQuest();
        StartQuest(QuestID.EnterGame);
        StartQuest(QuestID.CompanyOrScoreSurvive);
        StartQuest(QuestID.OpenChest);
        StartQuest(QuestID.BuildTower);
        StartQuest(QuestID.SummonHero);
        StartQuest(QuestID.UseAbility);
        StartQuest(QuestID.CraftCard);

        StartQuest(QuestID.KillBoss);
        StartQuest(QuestID.HeroKiller);
        StartQuest(QuestID.GetAnyCraftRes);
        StartQuest(QuestID.KillAllCreep);
        StartQuest(QuestID.Damager);

        StartQuest(QuestID.AbilityKiller);
        StartQuest(QuestID.UpgradeChest);
        StartQuest(QuestID.FastOrCardOpenChest);


        StartQuest(QuestID.GetPrizeFriends);
        StartQuest(QuestID.ToSpendCrystal);
        StartQuest(QuestID.OpenMoreChest);


        isActiveNewQuesst = false;
        _SaveChangeQuest();
    }

    public static void TakeQuestPrize(string ID)
    {
        _instance._TakeQuestPrize(ID);
    }
    private void _TakeQuestPrize(string ID)
    {
        if (QuestsTraining.isDone == false)
            return;
        var quest = _questList[Quest.GetID(ID)];
        if(quest.Status != Quest.QuestStatus.WaitPrise)
        {
            Debug.LogError("Take prize error, quest is not waitPrise!");
            return;
        }
        var value = quest.prizeValue;
        var intValue = int.Parse(value);
        _serverData.score += intValue;
        _serverData.totalScore += intValue;
        quest.CompleteGetPrize();
    }

    private void GameControl_onGameOverE(int stars, ResponseData prizes)
    {
        if (isNeedSaveChange)
        {
            Debug.Log("QM GameOver save change");
            _SaveChangeQuest();
        }
    }

    private void SpawnManager_onWaveClearedE(int waveID)
    {
        if (isNeedSaveChange)
        {
            Debug.Log("QD save change");
            _SaveChangeQuest();
        }
    }

    private void Quest_onChangeProgressQuest(string id)
    {
        if (QuestsTraining.isDone == false)
            return;

        if (SceneManager.GetActiveScene().buildIndex == 1)//Главная сцена
        {
            _SaveChangeQuest();
        }
        else
        {
            isNeedSaveChange = true;
        }
    }
    public static void SaveChangeQuest()
    {
        if (_instance == null)
        {
            Debug.LogWarning("QuestManaget instance is null");
            return;
        }
        _instance._SaveChangeQuest();
    }
    private void _SaveChangeQuest()
    {
        if (isActiveNewQuesst)
            return;
        isNeedSaveChange = false;
        StupidServer.SaveData(this);
    }

    public void ClearData()
    {
        Debug.LogWarning("ClearData QuestsDaily!");
        //Quest.onCompletedQuest -= Quest_onCompletedQuest;
        Quest.onChangeProgressQuest -= Quest_onChangeProgressQuest;
        SpawnManager.onWaveClearedE -= SpawnManager_onWaveClearedE;
        GameControl.onGameOverE -= GameControl_onGameOverE;
        ClearQuest();
    }

    private void ClearQuest()
    {
        Debug.Log("ClearQuest QuestsDaily!");
        foreach (var key in _questList.Keys)
        {
            _questList[key].Deactive();
        }
        _questList.Clear();
    }

    private void StartQuest(QuestID ID)
    {
        Debug.LogFormat("StartQuest {0}", ID);

        if (_questList.ContainsKey(ID))
        {
            if (_questList[ID].Status == Quest.QuestStatus.Hide)
                _questList[ID].StartQuest();
            else
                Debug.LogErrorFormat("StartQuest error: Status quest {0} {1}", ID, _questList[ID].Status);

        }
        else
        {
            _questList.Add(ID, Quest.GetDeilyQuest(ID));
            _questList[ID].StartQuest();
        }
    }

    #region TakePrizes

    public static void TakePrizeDay(int index)
    {
        _instance._TakePrizeDay(index);
    }
    public void _TakePrizeDay(int index)
    {
        if (_prizesDay[index].isReceived)
            return;

        _indexCurrentPrize = index;
        _currentGetPrizes = _prizesDay.ToArray();
        StartGetPrizes();
        _serverData.prizeGetD.Add(index);
    }


    public static void TakePrizeWeek(int index)
    {
        _instance._TakePrizeWeek(index);
    }
    public void _TakePrizeWeek(int index)
    {
        if (_prizesWeek[index].isReceived)
            return;

        _indexCurrentPrize = index;
        _currentGetPrizes = _prizesWeek.ToArray();
        StartGetPrizes();
        _serverData.prizeGetW.Add(index);
    }


    private void StartGetPrizes()
    {
        _countPrizeLast = _currentGetPrizes[_indexCurrentPrize].prize.Length;

        if (_prizeChests == null)
            _prizeChests = new List<PrizeChest>();
        else
            _prizeChests.Clear();

        _PrizesData = new ResponseData.PrizesData();
        _PrizesData.towers = new ResponseData.PrizesTower[0];

        NextGetPrizeDay();
    }
    private void NextGetPrizeDay()
    {
        Debug.LogFormat("Next index {0} count {2} current {1}", _indexCurrentPrize, _countPrizeLast, _currentGetPrizes[_indexCurrentPrize].prize.Length);
        
        if (_countPrizeLast <= 0)
        {
            ChestOpenPanel.OnHide += NextChestPrize;
            if (_bufferPlayerData != null)
            {
                PlayerManager.SetPlayerData(_bufferPlayerData);
                ChestManager.instance.openPanel.SetResponceDataAndShow(_PrizesData);
            }
            else
                NextChestPrize();
            return;
        }
        UIWaitPanel.Show();
        int id = _currentGetPrizes[_indexCurrentPrize].prize.Length - _countPrizeLast;
        _lastPrize = _currentGetPrizes[_indexCurrentPrize].prize[id];
        _countPrizeLast--;
        if (_lastPrize is PrizeChest)
        {
            _prizeChests.Add(_lastPrize as PrizeChest);
            NextGetPrizeDay();
        }
        else
        {
            PlayerManager.DM.GiveGoods(_lastPrize.Type, _lastPrize.Value, ResponsePrizeDay);
        }
    }


    private void ResponsePrizeDay(ResponseData data)
    {
        _bufferPlayerData = data.user;
        PlayerManager.sig_key = data.user.sig_key;

        if (data.ok==1)
        {
            WriteGetPrize();
            NextGetPrizeDay();
        }
        else
        {
            TooltipMessageServer.Show(data.error.text);
            Debug.LogErrorFormat("QD Response prize error.\r\n {0}: {1}",data.error.code,data.error.text);
            PlayerManager.SetPlayerData(data.user);
            _bufferPlayerData = null;
            return;
        }
    }

    private void NextChestPrize()
    {
        if (_prizeChests.Count <= 0)
        {
            ChestOpenPanel.OnHide -= NextChestPrize;
            EndTakePrize();
            return;
        }

        _lastPrize = _prizeChests[0];
        _prizeChests.RemoveAt(0);

        PlayerManager.DM.GiveGoods(_lastPrize.Type, _lastPrize.Value, ResponseChestPrizeDay);
    }

    private void ResponseChestPrizeDay(ResponseData data)
    {
        if (data.ok == 1)
        {
            PlayerManager.SetPlayerData(data.user);
            ChestManager.instance.openPanel.SetResponceDataAndShow(-1,(_lastPrize as PrizeChest).GetTypeChest, data.prizes);
        }
        else
        {
            TooltipMessageServer.Show(data.error.text);
            Debug.LogErrorFormat("QD Response prize error.\r\n {0}: {1}", data.error.code, data.error.text);
            PlayerManager.SetPlayerData(data.user);
            return;
        }
    }

    private void WriteGetPrize()
    {
        switch (_lastPrize.Type)
        {
            case PrizeType.Card:
                _AddCard(_lastPrize as PrizeCard);
                break;
            case PrizeType.Gold:
                _PrizesData.gold = (_lastPrize as PrizeGold).Count;
                break;
            case PrizeType.Crystal:
                _PrizesData.crystals = (_lastPrize as PrizeCrystal).Count;
                break;
        }
        _lastPrize = null;
    }

    private void _AddCard(PrizeCard prize)
    {
        var newTower = new ResponseData.PrizesTower();
        newTower.count = prize.GetCountCard;
        newTower.rarity = (int)prize.GetRarenessCard + 1;
        newTower.type = prize.GetTypeCard;

        int count = _PrizesData.towers.Length;
        ResponseData.PrizesTower[] towers = new ResponseData.PrizesTower[count + 1];
        for (int i = 0; i < count; i++)
        {
            towers[i] = _PrizesData.towers[i];
        }
        towers[count] = newTower;

        _PrizesData.towers = towers;
    }


    private void EndTakePrize()
    {
        _prizeChests.Clear();
        _bufferPlayerData = null;
        _PrizesData = null;
        _currentGetPrizes = null;
        Debug.Log("End take daily prize!");

        UIWaitPanel.Show();
        _SaveChangeQuest();
    }

    #endregion
}
