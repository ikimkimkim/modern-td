﻿
public interface IDataServerHandler
{
    string DataKey { get; }
    void SetData(string data);
    string SaveData { get; }
    void ClearData();
}
