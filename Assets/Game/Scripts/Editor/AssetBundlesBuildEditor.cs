﻿using UnityEditor;

public class AssetBundlesBuildEditor
{
    [MenuItem("Tools/Bundles/Build all")]
    static void BuildAllAssetBundles()
    {
        string path = EditorUtility.SaveFolderPanel("Save Bundles", "", "");
        if (string.IsNullOrEmpty(path) == false)
        {
            BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.None, BuildTarget.WebGL);// EditorUserBuildSettings.activeBuildTarget);
        }
    }
}
