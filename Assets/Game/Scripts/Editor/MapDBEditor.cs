﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapDB))]
public class MapDBEditor : Editor
{
    GUIContent content = new GUIContent();
    private MapDB mapDB;
    private void OnEnable()
    {
        mapDB = target as MapDB;

        content.text = "Уровни";
    }

    int indexSelect = 0, startI, endI;

    MapDB.MapData data;
    private static bool showDeffGUI;
    public override void OnInspectorGUI()
    {
        EditorGUILayout.BeginVertical("box");
        string[] param = new string[Mathf.CeilToInt(mapDB.MapList.Count / 12f) + 1];
        param[0] = "Все";
        for (int i = 0; i < param.Length-1; i++)
        {
            param[i+1] = string.Format("{0}-{1}", i * 12 + 1, (i + 1) * 12);
        }
        if (indexSelect >= param.Length)
            indexSelect = 0;
        indexSelect = EditorGUILayout.Popup("Фильтр:", indexSelect, param);

        if(indexSelect==0)
        {
            startI = 0;
            endI = mapDB.MapList.Count;
        }
        else
        {
            startI = (indexSelect - 1)* 12;
            endI = indexSelect * 12 > mapDB.MapList.Count?mapDB.MapList.Count : indexSelect * 12;
        }

        EditorGUILayout.BeginHorizontal();
        if(GUILayout.Button("Открыть все", GUILayout.Width(150)))
        {
            for (int i = startI; i < endI; i++)
            {
                data = mapDB.MapList[i];
                data.ShowDataEditor = true;
                mapDB.MapList[i] = data;
            }
        }
        if (GUILayout.Button("Скрыть все", GUILayout.Width(150)))
        {
            for (int i = startI; i < endI; i++)
            {
                data = mapDB.MapList[i];
                data.ShowDataEditor = false;
                mapDB.MapList[i] = data;
            }
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndVertical();




        for (int i = startI; i < endI; i++)
        {
            EditorGUI.BeginChangeCheck();
            data = mapDB.MapList[i];

            EditorGUILayout.BeginHorizontal();
            data.ShowDataEditor = EditorGUILayout.Foldout(data.ShowDataEditor, string.Format("{0}:{1}({2})", i + 1, data.Name, data.LevelID));

            if (GUILayout.Button((i + 1) + " удалить", GUILayout.Width(100)))
            {
                mapDB.MapList.RemoveAt(i);
                break;
            }
            EditorGUILayout.EndHorizontal();
            if (data.ShowDataEditor)
            {
                EditorGUILayout.BeginVertical("box");
                data.LevelID = EditorGUILayout.IntField("ID уровня", data.LevelID);
                data.Name = EditorGUILayout.TextField("Название уровня", data.Name);
                data.NameBundle = EditorGUILayout.TextField("Bundle", data.NameBundle);
                data.Icon = EditorGUILayout.ObjectField("Изображение уровня", data.Icon, typeof(Sprite), false) as Sprite;
                if(data.Icon!=null)
                    GUILayout.Box(data.Icon.texture,GUILayout.MaxWidth(256), GUILayout.MaxHeight(145));

                EditorGUILayout.LabelField(string.Format("Аддрес изображения на сервере:\r\n{0}", data.getUrlIcon),GUILayout.Height(30));

                EditorGUILayout.EndVertical();

            }
            EditorGUI.EndChangeCheck();
            if (GUI.changed)
                mapDB.MapList[i] = data;
        }

        if (GUILayout.Button("Добавить данные нового уровня",GUILayout.Height(25)))
        {
            data = mapDB.MapList[mapDB.MapList.Count-1];
            mapDB.MapList.Add(data);
            if (indexSelect > 0)
                indexSelect = Mathf.CeilToInt(mapDB.MapList.Count / 12f);
        }

        GUILayout.Space(10);
        showDeffGUI = EditorGUILayout.Foldout(showDeffGUI, "Show deff UI");
        if (showDeffGUI)
            DrawDefaultInspector();

        if (GUI.changed)
        {
            EditorUtility.SetDirty(mapDB.gameObject);
        }
    }
}
