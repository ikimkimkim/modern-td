﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using TDTK;

[CustomEditor(typeof(EffectImmunity))]
public class EffectImmunityEditor : Editor {

    private bool showDefaultFlag;

    private EffectImmunity immunity;

    void OnEnable()
    {
        immunity = target as EffectImmunity;      
    }

    IDEffect newEffect = IDEffect.none;

    public override void OnInspectorGUI()
    {
        immunity.rootUnit = EditorGUILayout.ObjectField("Root unit:",immunity.rootUnit, typeof(Unit), false) as Unit;

        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();

        newEffect = (IDEffect)EditorGUILayout.EnumPopup("Добавить иммунитет к эффекту:", newEffect);
        if (newEffect!=IDEffect.none && (immunity.setting == null || immunity.setting.ContainsKey((int)newEffect) == false))
        {
            if (immunity.setting == null)
                immunity.setting = new System.Collections.Generic.Dictionary<int, ImmunitySetting>();
            
            immunity.setting.Add((int)newEffect,new ImmunitySetting());
            GUI.changed = true;
            newEffect = IDEffect.none;
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();

        if (immunity.setting != null)
        {
            foreach (var setting in immunity.setting)
            {

                EditorGUILayout.BeginVertical("box");
                EditorGUILayout.LabelField(((IDEffect)setting.Key).ToString());

                    EditorGUILayout.LabelField("Текущие значение:" + immunity.setting[setting.Key].current);
                    EditorGUILayout.LabelField("Текущие количество наложений:" + immunity.setting[setting.Key].count);
                   /* EditorGUILayout.LabelField("Тип:" + immunity.setting[setting.Key].type);
                    EditorGUILayout.LabelField("Минимальное:" + immunity.setting[setting.Key].min);
                    EditorGUILayout.LabelField("Минимальное:" + immunity.setting[setting.Key].max);
                    EditorGUILayout.LabelField("Множитель:" + immunity.setting[setting.Key].multiplier);*/

                    immunity.setting[setting.Key].type = (ImmunitySetting.Type)EditorGUILayout.EnumPopup("Тип", immunity.setting[setting.Key].type);
                    immunity.setting[setting.Key].min = EditorGUILayout.FloatField("Минимальное", immunity.setting[setting.Key].min);
                    immunity.setting[setting.Key].max = EditorGUILayout.FloatField("Минимальное", immunity.setting[setting.Key].max);
                    immunity.setting[setting.Key].multiplier = EditorGUILayout.FloatField("Множитель", immunity.setting[setting.Key].multiplier);


                if (GUILayout.Button("Удалить"))
                {
                    immunity.setting.Remove(setting.Key);
                    EditorUtility.SetDirty(immunity.gameObject);
                    return;
                }
                EditorGUILayout.EndVertical();
            }
        }


        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        showDefaultFlag = EditorGUILayout.Foldout(showDefaultFlag, "Default editor");
        EditorGUILayout.EndHorizontal();
        if (showDefaultFlag) DrawDefaultInspector();

        if (GUI.changed)
        {
            EditorUtility.SetDirty(immunity.gameObject);
        }
    }



}
