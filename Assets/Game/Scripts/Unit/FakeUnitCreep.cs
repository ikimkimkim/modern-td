﻿using UnityEngine;
using System.Collections;
using TDTK;

public class FakeUnitCreep : UnitCreep
{
    [Header("FakeCreep")]
    [SerializeField]
    private float LifeTime;
    private float TimeInit;

    [SerializeField] private bool isSolo;

    protected override void Start()
    {
        base.Start();
        if(isSolo)
        {
            base.Init();
        }
    }

    public override void Init(PathTD p, int ID, int wID, UnitCreep parentUnit = null)
    {
        if (parentUnit == null)
        {
            Debug.LogError("FakeUnitCreep init parent is null!");
            throw new System.NullReferenceException("FakeUnitCreep init parent is null!");
        }

        base.Init(p, ID, wID, parentUnit);

        TimeInit = Time.time;
    }

    public override void SystemUpdate()
    {
        if (dead)
            return;

        base.SystemUpdate();

        if(TimeInit+LifeTime<Time.time)
        {
            Dead();
        }
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

}
