﻿using UnityEngine;
using System.Collections;
using TDTK;

public class VisionSystem : MonoBehaviour {

    public Unit UnitInfomation;


    void OnTriggerEnter(Collider other)
    {
        UnitInfomation.SeeNewTarget(other);
    }

    void OnTriggerExit(Collider other)
    {
        UnitInfomation.LostNewTarget(other);
    }
    
}
