﻿using System.Collections;
using System.Collections.Generic;
using SystemUpdate;
using TDTK;
using UnityEngine;

public class UnitsVisionSystem : Singleton<UnitsVisionSystem>, IItemUpdate
{

    [SerializeField] private List<UnitCreep> unitCreeps;
    [SerializeField] private List<UnitTower> unitTowers;
    [SerializeField] private List<UnitHero> unitHeroes;



    protected override void InitializationSingleton()
    {
        unitCreeps = new List<UnitCreep>();
        unitTowers = new List<UnitTower>();
        unitHeroes = new List<UnitHero>();
        MainSystem.AddItem(this);
    }
    
    private void OnDestoy()
    {
        MainSystem.RemoveItem(this);
    }


    public static void AddUnit(UnitCreep unit)
    {
        instance._AddUnit(unit);
    }
    private void _AddUnit(UnitCreep unit)
    {
        unitCreeps.Add(unit);
    }
    public static void RemoveUnit(UnitCreep unit)
    {
        instance._RemoveUnit(unit);
    }
    private void _RemoveUnit(UnitCreep unit)
    {
        unitCreeps.Remove(unit);
    }

    public static void AddUnit(UnitTower unit)
    {
        instance._AddUnit(unit);
    }
    private void _AddUnit(UnitTower unit)
    {
        unitTowers.Add(unit);
    }
    public static void RemoveUnit(UnitTower unit)
    {
        instance._RemoveUnit(unit);
    }
    private void _RemoveUnit(UnitTower unit)
    {
        unitTowers.Remove(unit);
    }

    public static void AddUnit(UnitHero unit)
    {
        instance._AddUnit(unit);
    }
    private void _AddUnit(UnitHero unit)
    {
        unitHeroes.Add(unit);
    }
    public static void RemoveUnit(UnitHero unit)
    {
        instance._RemoveUnit(unit);
    }
    private void _RemoveUnit(UnitHero unit)
    {
        unitHeroes.Remove(unit);
    }

    public void SystemUpdate()
    {
        if (unitTowers.Count == 0 && unitHeroes.Count == 0)
            return;

        for (int i = 0; i < unitCreeps.Count; i++)
        {
            if (unitCreeps[i].dead)
                continue;

            for (int t = 0; t < unitTowers.Count; t++)
            {
                float dis = Unit.DistanceUnitForUnit(unitCreeps[i], unitTowers[t]);
                if (unitTowers[t].targetMode == _TargetMode.Hybrid || (unitTowers[t].targetMode == _TargetMode.Ground && unitCreeps[i].flying==false))
                {
                    if (dis <= unitTowers[t].RangeVition)
                    {
                        if (unitTowers[t].ListTarget.Contains(unitCreeps[i]) == false)
                            unitTowers[t].SeeNewTarget(unitCreeps[i]);
                    }
                    else
                    {
                        if (unitTowers[t].ListTarget.Contains(unitCreeps[i]))
                            unitTowers[t].LostNewTarget(unitCreeps[i]);
                    }
                }

                if (unitCreeps[i].targetMode != _TargetMode.OnlyArtifact)
                {
                    if (dis <= unitCreeps[i].RangeVition)
                    {
                        if (unitCreeps[i].ListTarget.Contains(unitTowers[t]) == false)
                            unitCreeps[i].SeeNewTarget(unitTowers[t]);
                    }
                    else
                    {
                        if (unitCreeps[i].ListTarget.Contains(unitTowers[t]))
                            unitCreeps[i].LostNewTarget(unitTowers[t]);
                    }
                }
            }

            for (int h = 0; h < unitHeroes.Count; h++)
            {
                float dis = Unit.DistanceUnitForUnit(unitCreeps[i], unitHeroes[h]);
                if (unitHeroes[h].targetMode == _TargetMode.Hybrid || (unitHeroes[h].targetMode == _TargetMode.Ground && unitCreeps[i].flying==false))
                {
                    if (dis <= unitHeroes[h].RangeVition)
                    {
                        if (unitHeroes[h].ListTarget.Contains(unitCreeps[i]) == false)
                            unitHeroes[h].SeeNewTarget(unitCreeps[i]);
                    }
                    else
                    {
                        if (unitHeroes[h].ListTarget.Contains(unitCreeps[i]))
                            unitHeroes[h].LostNewTarget(unitCreeps[i]);
                    }
                }

                if (unitCreeps[i].targetMode != _TargetMode.OnlyArtifact)
                {
                    if (dis <= unitCreeps[i].RangeVition)
                    {
                        if (unitCreeps[i].ListTarget.Contains(unitHeroes[h]) == false)
                            unitCreeps[i].SeeNewTarget(unitHeroes[h]);
                    }
                    else
                    {
                        if (unitCreeps[i].ListTarget.Contains(unitHeroes[h]))
                            unitCreeps[i].LostNewTarget(unitHeroes[h]);
                    }
                }
            }
        }
    }
}
