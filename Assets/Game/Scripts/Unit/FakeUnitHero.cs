﻿using UnityEngine;
using System.Collections;
using TDTK;

public class FakeUnitHero : UnitHero
{

    [Header("FakeHero")]
    public float TimeLife;
    private float TimeInit;

    public override void Init(int startCountKill = 0, float startAllDmg = 0)
    {
        base.Init();
        TimeInit = Time.time;
    }

    public override void SystemUpdate()
    {
        if (dead)
            return;
        base.SystemUpdate();
        if(TimeInit+TimeLife<Time.time)
        {
            Dead();
        }
    }
}
