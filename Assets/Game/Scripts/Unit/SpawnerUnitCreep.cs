﻿using UnityEngine;
using System.Collections;
using TDTK;

public class SpawnerUnitCreep : UnitCreep
{
    [Header("Spawner setting")]
    [SerializeField]
    private float TimeLife;
    private float TimeInit;
    [SerializeField]
    [Range(0f, 1f)]
    private float DegradationProcent;
    [SerializeField]
    [Range(0f, 1f)]
    private float ScaleProcent = 1;

    public bool isSpawn = false;


    [SerializeField]
    private UnitCreep RootUnit;
    [SerializeField]
    private GameObject SpawnPrefab;
    [SerializeField]
    private int CountSpawn;

    public override void Init(PathTD p, int ID, int wID, UnitCreep parentUnit = null)
    {
        if (parentUnit == null)
        {
            Debug.LogError("SpawnerUnitCreep init parent is null!");
            throw new System.NullReferenceException("SpawnerUnitCreep init parent is null!");
        }

        instanceID = ID;
        waveID = wID;

        Init();

        fullHP = parentUnit.fullHP * parentUnit.spawnUnitHPMultiplier;
        //fullShield = parentUnit.fullShield * parentUnit.spawnUnitHPMultiplier;
        HP = fullHP;
        //shield = fullShield;

        distFromDestination = parentUnit._GetDistFromDestination();


        RootUnit = parentUnit;
        isSpawn = false;
        TimeInit = Time.time;
        if (SpawnPrefab == null)
            SpawnPrefab = RootUnit.thisObj;
    }


    public override void SystemUpdate()
    {
        if (isSpawn == false && Time.time > TimeInit + TimeLife)
        {
            StartSpawn();            
            isSpawn = true;
        }
    }

    public override void FixedUpdate()
    {

    }

    private void StartSpawn()
    {
        if (animationUnit != null)
        {
            animationUnit.PlayDestination();
            dead = true;
        }
    }


    public void SpawnChildAnimEvent()
    {
        for (int i = 0; i < CountSpawn; i++)
        {
            UnitCreep unit = ObjectPoolManager.Spawn(SpawnPrefab, thisT.position, thisT.rotation).GetComponent<UnitCreep>();

            unit.waveID = waveID;
            int ID = SpawnManager.AddDestroyedSpawn(unit);
            unit.Init(RootUnit.path, ID, waveID, RootUnit);
            unit.thisT.position = thisT.position;

            HeightTerrainPoint = Terrain.activeTerrain.SampleHeight(unit.thisT.position);
            if (unit.thisT.position.y < HeightTerrainPoint)
                unit.thisT.position = new Vector3(unit.thisT.position.x, HeightTerrainPoint, unit.thisT.position.z);

            unit.fullHP = RootUnit.fullHP * DegradationProcent;
            //unit.fullShield = RootUnit.fullShield * DegradationProcent;
            unit.HP = unit.fullHP;
            //unit.shield = unit.fullShield;
            unit.thisT.localScale = RootUnit.deffScale * ScaleProcent;
        }

    }

    

}
