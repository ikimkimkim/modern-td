﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TDTK
{

    public class UnitEggCreep : Unit
    {
        [Header("Egg")]
        [SerializeField] private GameObject _creepSpawn;
        [SerializeField] private PathTD _path;
        [SerializeField] private int _waypointStartIndex;
        [SerializeField] private float _multiplierHP = 10f, _moveSpeed;

        protected override void Start()
        {
            base.Start();
            Init();
        }

        public override void Dead(bool getRsc = true)
        {
            base.Dead(getRsc);
            SpawnCreep();

        }


        private void SpawnCreep()
        {

            Vector3 posOffset = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)) * SizeMyCollider;
            GameObject obj = ObjectPoolManager.Spawn(_creepSpawn, getPosition + posOffset, thisT.rotation);
            UnitCreep unit = obj.GetComponent<UnitCreep>();

            unit.waveID = 0;
            int ID = SpawnManager.AddDestroyedSpawn(unit);
            unit.defaultHP = _multiplierHP * defaultHP;
            unit.fullHP = _multiplierHP * fullHP;
            unit.stats[0].moveSpeed = _moveSpeed;
            unit.Init(_path, ID, 0);

            unit.pathOffset = posOffset;

            unit.SetWaypointID(_waypointStartIndex);

           /* HeightTerrainPoint = Terrain.activeTerrain.transform.position.y + Terrain.activeTerrain.SampleHeight(unit.thisT.position);
            if (unit.thisT.position.y < HeightTerrainPoint)
                unit.thisT.position = new Vector3(unit.thisT.position.x, HeightTerrainPoint, unit.thisT.position.z);*/

           // unit.lifeCost = _lifeCost;

        }
    }
}