﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class ManaCollectManager : MonoBehaviour {

    public GameObject prefabMana;

    public static ManaCollectManager instance { get; private set; }


	void Start ()
    {
        instance = this;
	}


    void OnDestoy()
    {

    }
    

    public GameObject CreadMana(Transform t)
    {
        GameObject go = GameObject.Instantiate(prefabMana);        
        go.transform.position = Camera.main.WorldToScreenPoint(t.position);
        go.transform.SetParent(transform);
        go.transform.localScale = new Vector3(1, 1, 1);
        return go;
    }
    
}
