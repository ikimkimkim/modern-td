﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
/// <summary>
/// В вебгл ServerManager имеет метод SaveLoadServer который в коллбек возвращает полученную json строку от сервера которую можно пропарсить с помощью  StringSerializationAPI 
/// </summary>
public class ServerManager : MonoBehaviour
{
    string _URL; //= "https://space-td.ru/vk/index.php";
    public static string _Domain;
    RequestData _requset;
    //PlayerManager _player;

    //public int getViewerID { get { return _requset.viewer_id; } }

    void Awake()
    {
        //_player = GetComponent<PlayerManager>();
    }

    public void SetRequsetData(RequestData data)
    {
        _requset = data;
        _URL = _requset.host;
        _Domain = _requset.domain;
    }
    ///<summary>
    /// Отправляет запрос с опренделенным методом на сервер и получает назад PlayerData, которое устонавливается текущим
    /// </summary>
    /// <param name="method">Метод</param>
    /// <param name="extraData">Дополнительные параметры метода</param>
    /// <param name="salt">Соль для md5(viewer_id + '-' + extra1 + '-' + extra2 + ... + '-' + "salt"). Если соль неуказана md5 не вычисляется. </param>
    /// <param name="result">CallBack в который передается полученная от сервера строка </param>

    /*public IEnumerator SaveLoadServer(string method, List<StringPair> extraData, string salt, Action<string> result)
    {
       
        WWWForm form = new WWWForm();
        //form.AddField("viewer_id", _requset.viewer_id.ToString());
        //form.AddField("api_id", _requset.api_id.ToString());
        //form.AddField("auth_key", _requset.auth_key.ToString());
        if(extraData !=null)
        for (int i = 0; i < extraData.Count; i++)
        {
            form.AddField(extraData[i].X, extraData[i].Y);
        }

        if (salt.Length != 0)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(_requset.viewer_id);
            for (int i = 0; i < extraData.Count; i++)
            {
                builder.Append("-" + extraData[i].Y);
            }
            builder.Append("-" + salt);

            string hashData = builder.ToString();
            form.AddField("sig", Md5Sum(hashData));
        }

        WWW www = new WWW(_URL +"?a=game&m=" +method, form);
        yield return www;

        //Debug.Log("ServerResponse Text = " + www.text);

        result(www.text);
    }
    */
    public IEnumerator SaveLoadServerTestGET(string method,  Action<string> result, bool isBuy = false)
    {

        /*WWWForm form = new WWWForm();
        //form.AddField("viewer_id", _requset.viewer_id.ToString());
        //form.AddField("api_id", _requset.api_id.ToString());
        //form.AddField("auth_key", _requset.auth_key.ToString());
        if (extraData != null)
            for (int i = 0; i < extraData.Count; i++)
            {
                form.AddField(extraData[i].X, extraData[i].Y);
            }

        if (salt.Length != 0)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(_requset.viewer_id);
            for (int i = 0; i < extraData.Count; i++)
            {
                builder.Append("-" + extraData[i].Y);
            }
            builder.Append("-" + salt);

            string hashData = builder.ToString();
            form.AddField("sig", Md5Sum(hashData));
        }*/

        //Тут должно быть добавление к method параметров из result по правилам GET запроса.

        WWW www;
        if (isBuy == false)
            www = new WWW(_URL + "/game/index/" + method);
        else
            www = new WWW(_URL + "/game/buy/" + method);

        yield return www;

        //Debug.Log("ServerResponse Text = " + www.text);

        result(www.text);
    }

    private WWWForm addParam(WWWForm form)
    {
        if (_requset == null || _requset.viewer_id == 0)
            return form;
        form.AddField("viewer_id", _requset.viewer_id.ToString());
        form.AddField("auth_key", _requset.auth_key.ToString());
        form.AddField("api_id", _requset.api_id.ToString());
        //Debug.Log(_requset.viewer_id +"|"+ _requset.auth_key+"|"+ _requset.api_id);
        return form;
    }

    public IEnumerator SaveLoadServer(string method, List<StringPair> extraData, string salt, Action<string> result, bool isBuy=false,bool isTest=false)
    {

        WWWForm form = new WWWForm();
        //form.AddField("viewer_id", _requset.viewer_id.ToString());
        //form.AddField("api_id", _requset.api_id.ToString());
        //form.AddField("auth_key", _requset.auth_key.ToString());

        addParam(form);

        if (extraData != null)
            for (int i = 0; i < extraData.Count; i++)
            {
                form.AddField(extraData[i].X, extraData[i].Y);
            }

        if (salt.Length != 0)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(_requset.viewer_id);
            for (int i = 0; i < extraData.Count; i++)
            {
                builder.Append("-" + extraData[i].Y);
            }
            builder.Append("-" + salt);

            string hashData = builder.ToString();
            form.AddField("sig", Md5Sum(hashData));
        }
        WWW www;
        if (isTest)
        {
            www = new WWW(_URL + "/admin/test/" + method, form);
        }
        else
        {
            if (isBuy == false)
                www = new WWW(_URL + "/game/index/" + method, form);
            else
                www = new WWW(_URL + "/game/buy/" + method, form);
        }
        yield return www;

        //Debug.Log("ServerResponse Text = " + www.text);

        result(www.text);
    }

    /// <summary>
    /// Загружает картинку пользователя
    /// </summary>
    public IEnumerator DownLoadImage(string url, Action<Sprite> CallBack)
    {
        WWWForm form = new WWWForm();
        /*form.headers.Add("Access-Control-Allow-Credentials", "true");
        form.headers.Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        form.headers.Add("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
        form.headers.Add("Access-Control-Allow-Origin", "*");*/

        addParam(form);
        Debug.Log(_Domain + "/fast/proxy.php?url=" + url);
        WWW www = new WWW(_Domain + "/fast/proxy.php?url="+url, form);

        yield return www;

        CallBack(Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f)));

        //_loadedPic = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));
        //_playerPic.sprite = _loadedPic;

    }

    public static IEnumerator DownLoadTerrainData(string url, bool addForm, Action<TerrainData> CallBack)
    {
        WWWForm form = new WWWForm();

        form.headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        form.headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        form.headers.Add("Connection", "keep-alive");
        form.headers.Add("Accept - Encoding", "gzip, deflate, sdch");

        form.headers.Add("Access-Control-Allow-Credentials", "true");
        form.headers.Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        form.headers.Add("Access-Control-Allow-Methods", "GET, OPTIONS");
        form.headers.Add("Access-Control-Allow-Origin", "*");


        MyLog.Log(_Domain + "/td/static/terrain_data/" + url + "?v=2");

        WWW www;
        if (addForm)
            www = new WWW(_Domain + "/td/static/terrain_data/" + url + "?v=2", form);
        else
            www = new WWW(_Domain + "/td/static/terrain_data/" + url + "?v=2");


        yield return www;
        TerrainData data = new TerrainData();


        //data = AssetDatabase.LoadAssetAtPath("Assets/New Terrain.asset", typeof(TerrainData)) as TerrainData;


        if (www.isDone)
        {
            Debug.Log("P" + www.progress + " b" + www.bytesDownloaded + " ");
            data =(TerrainData) ByteArrayToObject(www.bytes);

        }

        //GameObject terrainGameObject = Terrain.CreateTerrainGameObject(data);

        if (CallBack!=null)
            CallBack(data);
    }

    public static object ByteArrayToObject(byte[] arrBytes)
    {
       MemoryStream _MemoryStream = new MemoryStream(arrBytes);

        BinaryFormatter _BinaryFormatter
                    = new BinaryFormatter();
        _MemoryStream.Position = 0;
        return _BinaryFormatter.Deserialize(_MemoryStream);
/*
        MemoryStream memStream = new MemoryStream();
        BinaryFormatter binForm = new BinaryFormatter();

        memStream.Write(arrBytes, 0, arrBytes.Length);
        memStream.Seek(0, SeekOrigin.Begin);

        object obj = (object)binForm.Deserialize(memStream);

        return obj;*/
    }

    public static T FromByteArray<T>(byte[] data)
    {
        if (data == null)
            return default(T);
        BinaryFormatter bf = new BinaryFormatter();
        using (MemoryStream ms = new MemoryStream(data))
        {
            object obj = bf.Deserialize(ms);
            return (T)obj;
        }
    }

    private const int _version = 6;

    public static IEnumerator DownLoadTextur(string url,bool addForm, Action<Texture2D> CallBack)
    {
        MyLog.Log(_Domain + "/td/static/unity_images/" + url + "?v=" + _version);

        WWW www;
        if (addForm)
        {
            WWWForm form = new WWWForm();

            form.headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            form.headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            form.headers.Add("Connection", "keep-alive");
            form.headers.Add("Accept - Encoding", "gzip, deflate, sdch");

            form.headers.Add("Access-Control-Allow-Credentials", "true");
            form.headers.Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            form.headers.Add("Access-Control-Allow-Methods", "GET, OPTIONS");
            form.headers.Add("Access-Control-Allow-Origin", "*");

            www = new WWW(_Domain + "/td/static/unity_images/" + url + "?v=" + _version, form);
        }
        else
            www = new WWW(_Domain + "/td/static/unity_images/" + url + "?v=" + _version);


        yield return www;
        Texture2D textur = null;
        try
        {
            textur = www.textureNonReadable;
            textur.name = url;
        }
        catch
        {
            textur = null;
        }


        CallBack(textur);
    }

    public static IEnumerator DownLoadAudioClip(string url, bool addForm, Action<AudioClip> CallBack)
    {
        WWWForm form = new WWWForm();

        form.headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        form.headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        form.headers.Add("Connection", "keep-alive");
        form.headers.Add("Accept - Encoding", "gzip, deflate, sdch");

        form.headers.Add("Access-Control-Allow-Credentials", "true");
        form.headers.Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        form.headers.Add("Access-Control-Allow-Methods", "GET, OPTIONS");
        form.headers.Add("Access-Control-Allow-Origin", "*");


        Debug.Log(_Domain + "/td/static/unity_sound/" + url);

        WWW www;
        if (addForm)
            www = new WWW(_Domain + "/td/static/unity_sound/" + url, form);
        else
            www = new WWW(_Domain + "/td/static/unity_sound/" + url);
        yield return www;

        while (www.isDone==false)
        {
            Debug.Log("wait load web page");
            yield return new WaitForSeconds(1f);
        }
        AudioClip ac = www.GetAudioClip();
        ac.name = url;
        int countWait = 1;
        while(ac.length==0 && countWait <10)
        {
            yield return new WaitForSeconds(2f);
            countWait++;
        }

        while (ac.loadState == AudioDataLoadState.Loading)
        {
            Debug.Log("wait load audioClip " + url);
            yield return new WaitForSeconds(1f);
        }

        if (ac.loadState == AudioDataLoadState.Failed)
        {
            Debug.LogError("Audio Clip load Fail! " + url);
        }
        else if(ac.loadState != AudioDataLoadState.Loaded)
        {
            Debug.LogWarning("Audio Clip: " + url + " Warning: "+ ac.loadState);
        }
        
        CallBack(ac);
    }

    public static Texture2D getDownLoadTextur(string url, bool addForm, Action<Texture2D> CallBack)
    {
        WWWForm form = new WWWForm();

        form.headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        form.headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        form.headers.Add("Connection", "keep-alive");
        form.headers.Add("Accept - Encoding", "gzip, deflate, sdch");

        form.headers.Add("Access-Control-Allow-Credentials", "true");
        form.headers.Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        form.headers.Add("Access-Control-Allow-Methods", "GET, OPTIONS");
        form.headers.Add("Access-Control-Allow-Origin", "*");


        Debug.Log(_Domain + "/td/static/unity_images/" + url);

        WWW www;
        if (addForm)
            www = new WWW(_Domain + "/td/static/unity_images/" + url, form);
        else
            www = new WWW(_Domain + "/td/static/unity_images/" + url);


        while(www.isDone==false)
        {
            //new WaitForSeconds(1);
        }

        return www.textureNonReadable;
    }


    byte[] GetBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }
    public static string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }
}
public struct StringPair
{
    public string X;
    public string Y;
    public StringPair(string x, string y)
    {
        X = x;
        Y = y;
    }
}