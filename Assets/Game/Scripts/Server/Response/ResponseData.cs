﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResponseData
{

    /*   'ok' : 1/0 -успех или неудача,
    'error' : - будет присутствовать только при ошибке
        {
            'code' : 'unknown_error', // код ошибки, строковый
            'text' : 'Неизвестная ошибка', // человекопонятное описание ошибки
        },
    'user' : информация о пользователе(https://bitbucket.org/ikimkimkim/modern-td/wiki/Server_api/uinfo)
     'rnd' : 123 - в случае успеха тут возвращается специальный ключ, который необходимо будет использовать в запросе окончания уровня
    'prizes' : - информация о призах за уровень
        {
            'gold' : int - тут будет количество золота за уровень. может отсутствовать
            'crystals' : int - тут будет количество кристаллов за уровень. может отсутствовать
            'chest' : int - всегда присутствует. Тут или тип выданного сундука, или 0 если сундук не выдан
        },*/


    public int ok;
    public Info info;
    public int great_new_place;
    public PlayerData user;
    public ErrorData error;
    public PrizesData prizes;
    public PrizesTower prizes2;
    public int rnd;
    public int new_stars;
    public Dictionary<string, int> stars_config;
    public Dictionary<string, string> data;

    //Наброски для звезд
    public int[] cards;
    public int time;

    //"stars_config":{"1":1,"2":2,"3":3}
    public class Info
    {
        public int ok;
        public PrizesData prizes;
    }
    

    public class PrizesData
    {
        public int gold;
        public int crystals;
        public int chest;
        public PrizesTower[] towers;
        public Dictionary<string, int> craft_resources;
        
        //Для разбора карт
        public Dictionary<string, int> craft;
    }

    public class PrizesTower
    {
        public int rarity;
        public int type;
        public int count;
    }
}
