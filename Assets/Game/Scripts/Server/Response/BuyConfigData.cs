﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BuyConfigData
{
    public StoreItemData[] crystals;
    public Dictionary<string, StoreItemData> energy;
    public Dictionary<string, LocationItemData> locations = new Dictionary<string, LocationItemData>();
}
[System.Serializable]
public class BuyCystalConfigData
{
    public StoreCrystalItemData[] gold;
    public StoreCrystalItemData[] chests;
    public StoreCrystalItemData[] energy;
}