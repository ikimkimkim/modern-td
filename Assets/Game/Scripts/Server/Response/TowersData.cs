﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TowersData
{

    //public string id;
    public int selected;
    public string name;
    public int card_level;
    public int rarity;
    public int type;
    public int count;
    public int next_level_count;
    public Dictionary<string, float[]> properties;
    public Dictionary<string, float[]> set_properties;
    public int set_id;
    public int legendary_id;
    public SettingsData[] settings;
    public float[] next_damage;
    public int price_upgrade;
    public int price_sell_one;

    //public int exp;
    //public int user_id;


    [System.Serializable]
    public class SettingsData
    {
        public float price;
        public float[] dmg;
        public float range;
        public float speed;
        public float speedmove=1;
        public float hp=1;
        public float xpnextlevel;

        public float mana;
        public float cooldown;

        public float slow;
        public float duration;

        public float count;//regen Mana

    }

}
