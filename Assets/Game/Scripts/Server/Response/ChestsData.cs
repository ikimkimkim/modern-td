﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ChestsData
{

    /*"id":"21", // id сундука, используется во всех запросах
                    "type_id":"1", // тип. деревянный/обычный и т.п.
                    "time_decrease_pcnt":"0", // количество процентов, на которые уменьшено время открытия. просто строка
                    "status":"closed", // статус. бывает closed, opening, opened
                    "time_to_open":15, // время до открытия сундука. в процессе открытия уменьшается
                    "open_price":1 // цена в кристаллах, которую необходимо заплатить за то, чтобы сундук открылся сразу. Меняется в зависимости от времени, которое осталось до открытия
                    */

    public string id;
    public string type_id;
    public string time_decrease_pcnt;
    public int status;
    public int time_to_open;
    public int open_price;
    public ChestPrizesData available_prizes;
    public string level;
}

public class ChestPrizesData
{
    public int[] gold;
    public int[] crystals;
    public ChestPrizesCardData cards;
    public Dictionary<string, float[]> craft_resources;
}

public class ChestPrizesCardData
{
    public int cards_count;
    public string max_text;
}



