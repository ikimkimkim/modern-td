﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class VKUserData
{
    public int id;
    public string name;
    public string ava;
    /*
    [id] => 203654
    [name] => ааЛаЕаКбаЕаЙ ааИаМ
    [ava] => https://pp.vk.me/c10114/u203654/d_27ef02fe.jpg
    */
}
