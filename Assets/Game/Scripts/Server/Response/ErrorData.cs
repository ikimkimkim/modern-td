﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ErrorData
{
    public string code;
    public string text;
}
