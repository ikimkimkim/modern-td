﻿using System.Collections.Generic;
/// <summary>
/// При старте приложения в вебгл получаем данные в таком виде
/// </summary>
public class RecievedData
{
    public RequestData request_data;
    public VKUserData vk_user;
    public PlayerData user_info;
    public BuyConfigData buy_conf;
    public BuyCystalConfigData buy_crystals_conf;
    public Dictionary<string, ChestPrizesData> chest_info_conf;
    public Dictionary<string, Dictionary<string, Dictionary<string, float>>> craft_cards_conf = new Dictionary<string, Dictionary<string, Dictionary<string, float>>>();
    public Dictionary<string, Dictionary<string, float[]>> sell_price_conf = new Dictionary<string, Dictionary<string, float[]>>();
    public int time;



    /*public PlayerData settings;
    public UserInfo user;
    public int[] friends_ids;
    public StoreItemData[] crystals;
    public Dictionary<string, LocationItemData> buy_locations;
    */

    public RecievedData()
    {

    }
}

