﻿using UnityEngine;
using System.Collections;

public class LocationItemData
{
    public string item;
    public string price;
    public LocationItemData()
    { }
    public LocationItemData(string item, string price)
    {
        this.item = item;
        this.price = price;
    }
}
