﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class TutorialFirstLevel : MonoBehaviour {

    public GameObject ButtonStart;
    public GameObject InfoButtonStart;
    public GameObject[] Locks;
    public GameObject[] TextInfo;

    public PlatformTD[] platforms;
    public GameObject[] TextInfoPlatform;

    public enum listStatus { history, general1_1, general1_2, general1_3, posttion1, tower1, waitBild1, general2, startGame, waitCost1, WaitTime, general3, position2, tower2, waitBild2, end, waitCost2, position3,tower3, waitCost3, position4,tower4 };
    public listStatus _status;
    public listStatus Status { get { return _status; } set { _status = value; ChangeStatus(); } }
    //public int Status=0;
    public int CountTower = 0;
    public bool ShowUI = false;

	// Use this for initialization
	void Start ()
    {
        if(PlayerManager._instance.GetLevelsCountComplele()> 0 || GameControl.GetLevelID() != 1)
        {
            Status = listStatus.end;
            gameObject.SetActive(false);
            return;
        }

        CountTower = 0;
        ShowUI = false;
        UIBuildButton.ShowEvent += ShowUIBuild;
        UIBuildButton.HideEvent += HideUIBuild;
        UnitTower.onConstructionStartE += BuildTower;
        UnitTower.onConstructionCompleteE += BuildTower;
        ResourceManager.onRscChangedE += OnResourceChanged;
        UIHUD.ClickOnSpawnButton += ClickOnSpawnButton;
        ButtonStart.SetActive(false);
        InfoButtonStart.SetActive(false);
        /*for (int i = 0; i < Locks.Length; i++)
        {
            Locks[i].SetActive(true);
        }*/
        for (int i = 0; i < TextInfo.Length; i++)
        {
            TextInfo[i].SetActive(false);
        }
        Status = 0;
    }

    private void EndTutorial()
    {
        Status = listStatus.end;
        gameObject.SetActive(false);
    }

    void OnDestroy()
    {
        UIBuildButton.ShowEvent -= ShowUIBuild;
        UIBuildButton.HideEvent -= HideUIBuild;
        UnitTower.onConstructionStartE -= BuildTower;
        UnitTower.onConstructionCompleteE -= BuildTower;
        ResourceManager.onRscChangedE -= OnResourceChanged;
        UIHUD.ClickOnSpawnButton -= ClickOnSpawnButton;
    }


    public void ShowUIBuild()
    {
        if (ShowUI || (Status != listStatus.posttion1 && Status != listStatus.position2 && Status != listStatus.position3))
            return;
        if(Status!=listStatus.end)
            TimeScaleManager.SetTimeScale(0);
        ShowUI = true;
        Status++;
    }

    public void HideUIBuild()
    {
        ShowUI = false;
        if(Status== listStatus.tower1 || Status == listStatus.tower2
            || Status == listStatus.tower3 || Status == listStatus.tower4)
        {
            TimeScaleManager.SetTimeScale(0);
            Status--;
        }
    }

    public void BuildTower(UnitTower t)
    {
        if (_status == listStatus.tower1 || _status == listStatus.tower2 || _status == listStatus.tower3
            || _status == listStatus.waitBild1 || _status == listStatus.waitBild2)
        {
            Status++;
            CountTower++;
        } 
    }

    public void AddStatus()
    {
        Status++;
    }

    public void ChangeStatus()
    {
        switch (_status)
        {
            case listStatus.history:
                HistoryPanel.StartShow(AddStatus, 1);
                break;
            case listStatus.general1_1:
                UIBuildButton.Hide();
                /* GeneralPanel.StartShow(AddStatus, new string[] {
                     "Добро пожаловать, <color=#00FF00>Командор!</color> Я - ваш помощник, меня называют Полковник."//,
                     //"Для лучшего погружения в атмосферу, <color=#FFA500>советую</color> вам перейти в <color=#00FF00>полноэкранный режим</color>",
                     //"Разведка докладывает, что небольшой отряд <color=#FF0000>тварей</color> приближается к нашей территории.",
                     //"Давайте приготовимся к <color=#00FF00>обороне.</color>",
                     //"Для начала построим <color=#FFA500>пулемёт.</color>"
                     //"Вы прибыли как раз вовремя! В ниших местах врага не видели с самого начала Вторжения, но сейчас разведка докладывает, что небольшой отряд тварей движется к нашей территории. Они близко, давайте приготовимся к обороне.",
                     //"Кажется, у вас еще нет опыта ведения боевых действий? Постараюсь помочь вам чем смогу. Для начала давайте построим оборонительную башню" 
                 }, new int[] { 0//,1,0,0,1
                    });*/
                break;
            case listStatus.general1_2:
                TextInfo[4].SetActive(true);
                UIBuildButton.Hide();
               /* GeneralPanel.StartShow(AddStatus, new string[] {
                    //"Добро пожаловать, <color=#00FF00>Командор!</color> Я - ваш помощник, меня называют Полковник.",
                    "Для лучшего погружения в атмосферу, <color=#FFA500>советую</color> вам перейти в <color=#00FF00>полноэкранный режим</color>"//,
                    //"Разведка докладывает, что небольшой отряд <color=#FF0000>тварей</color> приближается к нашей территории.",
                    //"Давайте приготовимся к <color=#00FF00>обороне.</color>",
                    //"Для начала построим <color=#FFA500>пулемёт.</color>"
                 }, new int[] { 1 },false);*/
                break;
            case listStatus.general1_3:
                TextInfo[4].SetActive(false);
                UIBuildButton.Hide();
               /* GeneralPanel.StartShow(AddStatus, new string[] {
                    //"Добро пожаловать, <color=#00FF00>Командор!</color> Я - ваш помощник, меня называют Полковник.",
                    //"Для лучшего погружения в атмосферу, <color=#FFA500>советую</color> вам перейти в <color=#00FF00>полноэкранный режим</color>",
                    "Разведка докладывает, что небольшой отряд <color=#FF0000>тварей</color> приближается к нашей территории.",
                    "Давайте приготовимся к <color=#00FF00>обороне.</color>",
                    "Для начала построим <color=#FFA500>пулемёт.</color>"
                 }, new int[] { 0, 0, 1 });*/
                break;
            case listStatus.posttion1:
                //TutorialBuildTowerManager.Hide();
                ShowInfoFreePlatform();
                //TextInfo[0].SetActive(true);
                break;
            case listStatus.tower1:
                TextInfo[0].SetActive(false);
                HideInfoPlatform();
                if (ResourceManager.GetResourceArray()[0] < 20)
                    ResourceManager.GainResource(new List<float>() { 20,0 });
               /* if (TutorialBuildTowerManager.ShowTower(0, "Кликните чтобы построить башню")<=0)
                    EndTutorial();*/
                break;
            case listStatus.waitBild1:
            case listStatus.waitBild2:
               // TutorialBuildTowerManager.Hide();
                break;
            case listStatus.general2:
               /* GeneralPanel.StartShow(AddStatus, new string[] { "Мы готовы. <color=#00FF00>Начинаем!</color>" },
                    new int[] { 0 });*/
                break;
            case listStatus.startGame:
                //TutorialBuildTowerManager.Hide();
                ButtonStart.SetActive(true);
                InfoButtonStart.SetActive(true);
                break;
            case listStatus.waitCost1:
                break;
            case listStatus.general3:
                TextInfo[3].SetActive(true);
                UIBuildButton.Hide();
               /* GeneralPanel.StartShow(AddStatus, new string[] {
                    "Командор! Мы заметили <color=#FF0000>массовые</color> скопления врага",
                    "Рекомендую построить <color=#FFA500>Тяжелую</color> пушку"
                    //"Эти твари идут толпой, пулемет не успевает разделаться со всеми.",
                    //"Давайте построим артиллерию, она хорошо справляется с массовыми скоплениями врага" 
                }, new int[] { 0,  1 });*/
                break;
            case listStatus.position2:
                TextInfo[3].SetActive(false);
                Locks[0].SetActive(false);
                //TextInfo[2].SetActive(true);

                ShowInfoFreePlatform();
               // TutorialBuildTowerManager.Hide();
                break;
            case listStatus.tower2:
                HideInfoPlatform();
                if (ResourceManager.GetResourceArray()[0] < 24)
                    ResourceManager.GainResource(new List<float>() { 24, 0 });
                TextInfo[2].SetActive(false);
               /*if( TutorialBuildTowerManager.ShowTower(1, "Кликните чтобы построить башню") <= 0)
                    EndTutorial();*/
                break;
            case listStatus.waitCost2:
                //TutorialBuildTowerManager.Hide();
                break;
            case listStatus.position3:
                Locks[1].SetActive(false);
                TextInfo[4].SetActive(true);
                TextInfo[5].SetActive(false);
                break;
            case listStatus.tower3:
                TextInfo[5].SetActive(true);
                TextInfo[4].SetActive(false);
                break;

            case listStatus.waitCost3:
                TextInfo[5].SetActive(false);
                break;
            case listStatus.position4:
                Locks[2].SetActive(false);
                TextInfo[6].SetActive(true);
                TextInfo[7].SetActive(false);
                break;
            case listStatus.tower4:
                TextInfo[7].SetActive(true);
                TextInfo[6].SetActive(false);
                break;
            case listStatus.end:
                //TutorialBuildTowerManager.Hide();
                for (int i = 0; i < Locks.Length; i++)
                {
                    Locks[i].SetActive(false);
                }
                for (int i = 0; i < TextInfo.Length; i++)
                {
                    TextInfo[i].SetActive(false);
                }
                GameObject.Destroy(this.gameObject);
                break;
        }
    }

    private void HideInfoPlatform()
    {
        for (int i = 0; i < TextInfoPlatform.Length; i++)
        {
            TextInfoPlatform[i].SetActive(false);
        }
    }

    private void ShowInfoFreePlatform()
    {
        HideInfoPlatform();
        for (int i = 0; i < platforms.Length; i++)
        {
            _TileStatus t = BuildManager.CheckBuildPoint(Camera.main.WorldToScreenPoint(platforms[i].thisT.position));
            if (t == _TileStatus.Available)
            {
                TextInfoPlatform[i].SetActive(true);
                return;
            } 
        }
    }


    public void OnResourceChanged(List<float> valueChangedList)
    {
        if (_status == listStatus.waitCost1 || _status == listStatus.waitCost2 || _status == listStatus.waitCost3)
        {
            float[] rscList = ResourceManager.GetResourceArray();

            if (rscList[0] == 4 && (_status == listStatus.waitCost2 || _status == listStatus.waitCost3))
            {
                Status++;
                TimeScaleManager.SetTimeScale(0);
            }
            /*else if (SpawnManager.instance.waveClearedCount==1 && _status == listStatus.waitCost1)
            {
                Status++;
                StartCoroutine(WaitTime(20));
            }*/
        }
    }

    private IEnumerator WaitTime(float time)
    {
        yield return new WaitForSeconds(time);
        Status++;
        TimeScaleManager.SetTimeScale(0);
    }

    public void ClickOnSpawnButton()
    {
        TimeScaleManager.RefreshTimeScale();
        Status++;
        
    }

    void Update()
    {
        if (GameControl.GetGameState() == _GameState.Play)
        {
            switch (_status)
            {
                case listStatus.posttion1:
                case listStatus.tower1:
                    TimeScaleManager.SetTimeScale(0);
                    break;
                case listStatus.startGame:
                case listStatus.waitCost1:
                    /*if (SpawnManager.instance.waveClearedCount == 1)
                    {
                        Status++;
                        StartCoroutine(WaitTime(20));
                    }*/
                    TimeScaleManager.RefreshTimeScale();
                    break;
                case listStatus.position2:
                case listStatus.tower2:
                    TimeScaleManager.SetTimeScale(0);
                    break;
                case listStatus.end:
                    TimeScaleManager.RefreshTimeScale();
                    break;
            }
        }
    }

}
