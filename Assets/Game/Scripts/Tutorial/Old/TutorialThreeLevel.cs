﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class TutorialThreeLevel : MonoBehaviour {

    public GameObject ButtonStart;
    public GameObject InfoButtonStart;
    public GameObject[] Locks;
    public GameObject[] TextInfo;

    public enum listStatus { history, posttion1, tower1, clickTower1, upgradeTower1, startGame, end, waitCost1, WaitTime, position2, tower2, position3, tower3, waitCost2, waitCost3, position4, tower4 };
    public listStatus _status;
    public listStatus Status { get { return _status; } set { _status = value; ChangeStatus(); } }
    //public int Status=0;
    public int CountTower = 0;
    public bool ShowUI = false;

    private bool bHaveT;

    // Use this for initialization
    void Start()
    {
        bHaveT = false;
        if (PlayerManager._instance.GetLevelsCountComplele() != 2 || GameControl.GetLevelID() != 3)
        {
            Status = listStatus.end;
            gameObject.SetActive(false);
            return;
        }

        for (int i = 0; i < PlayerManager.CardsList.Count; i++)
        {
            if (PlayerManager.CardsList[i].disableInBuildManager == false)
            {
                if (PlayerManager.CardsList[i].damageType == 0 && PlayerManager.CardsList[i].LevelCard>=2)
                    bHaveT = true;
            }
        }

        if(bHaveT==false)
        {
            Status = listStatus.end;
            gameObject.SetActive(false);
            return;
        }

        CountTower = 0;
        ShowUI = false;
        UIBuildButton.ShowEvent += ShowUIBuild;
        UIBuildButton.HideEvent += HideUIBuild;
        UnitTower.onConstructionStartE += BuildTower;
        UnitTower.onSoldE += SoldTower;
        UnitTower.onUpgradedE += UpgradeTower;
        UI.onSelectTower += onSelectTower;
        UI.onClearSelectTower += onClearSelectTower;
        ResourceManager.onRscChangedE += OnResourceChanged;
        UIHUD.ClickOnSpawnButton += ClickOnSpawnButton;
        ButtonStart.SetActive(false);
        //InfoButtonStart.SetActive(false);
        for (int i = 0; i < Locks.Length; i++)
        {
            Locks[i].SetActive(true);
        }
        for (int i = 0; i < TextInfo.Length; i++)
        {
            TextInfo[i].SetActive(false);
        }
        Status = 0;
    }

    void OnDestroy()
    {
        UIBuildButton.ShowEvent -= ShowUIBuild;
        UIBuildButton.HideEvent -= HideUIBuild;
        UnitTower.onConstructionStartE -= BuildTower;
        UnitTower.onUpgradedE -= UpgradeTower;
        UnitTower.onSoldE -= SoldTower;
        UI.onSelectTower -= onSelectTower;
        UI.onClearSelectTower -= onClearSelectTower;
        ResourceManager.onRscChangedE -= OnResourceChanged;
        UIHUD.ClickOnSpawnButton -= ClickOnSpawnButton;
    }

    public void EndTutorial()
    {
        Status = listStatus.end;
        gameObject.SetActive(false);
    }

    public void onClearSelectTower()
    {
        if (Status == listStatus.upgradeTower1)
            Status--;
    }

    public void onSelectTower(UnitTower t)
    {

        if (Status == listStatus.clickTower1)
            AddStatus();
    }

    public void ShowUIBuild()
    {
        if (ShowUI)
            return;
        if (Status != listStatus.end)
            TimeScaleManager.SetTimeScale(0);
        ShowUI = true;
        Status++;
    }

    public void HideUIBuild()
    {
        ShowUI = false;
        if (Status == listStatus.tower1 || Status == listStatus.tower2 || Status == listStatus.tower3 || Status == listStatus.tower4)
        {
            TimeScaleManager.SetTimeScale(0);
            Status--;
        }
    }

    public void SoldTower(UnitTower t)
    {
        Status = listStatus.end;
        gameObject.SetActive(false);
    }

    public void BuildTower(UnitTower t)
    {
        Status++;
        CountTower++;
    }

    public void UpgradeTower(UnitTower t)
    {
        AddStatus();
    }

    public void AddStatus()
    {
        Status++;
    }

    public void ChangeStatus()
    {
        switch (_status)
        {
            case listStatus.history:
                AddStatus();
                //HistoryPanel.StartShow(AddStatus, new int[] { 4, 5, 6 });
                break;
            case listStatus.posttion1:
                //TutorialBuildTowerManager.Hide();
                TextInfo[0].SetActive(true);
                break;
            case listStatus.tower1:
                TextInfo[0].SetActive(false);
                if (ResourceManager.GetResourceArray()[0] < 20)
                    ResourceManager.GainResource(new List<float>() { 20, 0 });
                /*if (TutorialBuildTowerManager.ShowTower(0, "Кликните чтобы построить башню", 2) <= 0)
                    EndTutorial();*/
                break;
            case listStatus.clickTower1:
               // TutorialBuildTowerManager.Hide();
                TextInfo[1].SetActive(true);
                TextInfo[2].SetActive(false);
                break;
            case listStatus.upgradeTower1:
                if (ResourceManager.GetResourceArray()[0] < 22f)
                    ResourceManager.GainResource(new List<float>() { 22f, 0 });
                TextInfo[1].SetActive(false);
                TextInfo[2].SetActive(true);
                break;
            case listStatus.startGame:
                TextInfo[2].SetActive(false);
                ButtonStart.SetActive(true);
                break;
            case listStatus.end:
               //TutorialBuildTowerManager.Hide();
                for (int i = 0; i < Locks.Length; i++)
                {
                    Locks[i].SetActive(false);
                }
                for (int i = 0; i < TextInfo.Length; i++)
                {
                    TextInfo[i].SetActive(false);
                }
                GameObject.Destroy(this.gameObject);
                break;
        }
    }


    public void OnResourceChanged(List<float> valueChangedList)
    {
       /* if (_status == listStatus.waitCost1 || _status == listStatus.waitCost2 || _status == listStatus.waitCost3)
        {
            float[] rscList = ResourceManager.GetResourceArray();

            if (rscList[0] >= 4 && (_status == listStatus.waitCost2 || _status == listStatus.waitCost3))
            {
                Status++;
                TimeScaleManager.SetTimeScale(0);
            }
            else if (SpawnManager.instance.waveClearedCount == 1 && _status == listStatus.waitCost1)
            {
                Status++;
                StartCoroutine(WaitTime(8));
            }
        }*/
    }

    private IEnumerator WaitTime(float time)
    {
        yield return new WaitForSeconds(time);
        Status++;
        TimeScaleManager.SetTimeScale(0);
    }

    public void ClickOnSpawnButton()
    {
        TimeScaleManager.RefreshTimeScale();
        Status++;

    }

    void Update()
    {
        if (GameControl.GetGameState() == _GameState.Play)
        {
            switch (_status)
            {
                case listStatus.posttion1:
                case listStatus.tower1:
                    TimeScaleManager.SetTimeScale(0);
                    break;
                case listStatus.startGame:
                case listStatus.waitCost1:
                case listStatus.waitCost2:
                    TimeScaleManager.RefreshTimeScale();
                    break;
                case listStatus.position2:
                case listStatus.tower2:
                    TimeScaleManager.SetTimeScale(0);
                    break;
                case listStatus.position3:
                case listStatus.tower3:
                    TimeScaleManager.SetTimeScale(0);
                    break;
                case listStatus.end:
                    TimeScaleManager.RefreshTimeScale();
                    break;
            }
        }
    }
}
