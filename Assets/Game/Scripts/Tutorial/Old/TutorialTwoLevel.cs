﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;

public class TutorialTwoLevel : MonoBehaviour {

    public GameObject ButtonStart;
    public GameObject InfoButtonStart;
    public GameObject[] Locks;
    public GameObject[] TextInfo;

    public PlatformTD[] platforms;
    public GameObject[] TextInfoPlatform;

    public enum listStatus { general1, posttion1, tower1, waitBild1, startGame, waitCost1, WaitTime, general3, position2, tower2, waitCost2, WaitTime2, general2, waitSelectTower, waitUpgrade, endUpgrade, end, position3, tower3,  waitCost3, position4,  tower4 };
    public listStatus _status;
    public listStatus Status { get { return _status; } set { _status = value; ChangeStatus(); } }
    //public int Status=0;
    public int CountTower = 0;
    public bool ShowUI = false;

    private bool bHaveCannonT,bHaveLiteT2lvl;

    public GameObject tooltipUpgradeTGo;
    public MoveUItoGameObj MoveTooltipUpgradeT;

    private Transform towerUpgradeT;

    void Start()
    {
        return;
        bHaveCannonT = false;
        List<Card> Towers = PlayerManager.getTowerCards;
        for (int i = 0; i < Towers.Count; i++)
        {
            if(Towers[i].disableInBuildManager==false)
            {
                if (Towers[i].damageType == 1)
                    bHaveCannonT = true;
                else if (Towers[i].damageType == 0 && Towers[i].LevelCard >= 2)
                    bHaveLiteT2lvl = true;


            }
        }

        if (PlayerManager._instance.GetLevelsCountComplele() != 1 || bHaveCannonT==false || GameControl.GetLevelID()!=2 || bHaveLiteT2lvl==false)
        {
            Status = listStatus.end;
            gameObject.SetActive(false);
            return;
        }

        CountTower = 0;
        ShowUI = false;
        UIBuildButton.ShowEvent += ShowUIBuild;
        UIBuildButton.HideEvent += HideUIBuild;
        
        UnitTower.onConstructionStartE += BuildTower;
        UnitTower.onConstructionCompleteE += BuildTower;

        UITowerInfo.OnShow += UITowerShot;
        UITowerInfo.OnHide += RemoveStatus;
        TowerSelectProperties.onShowE += SelectPropertis;
        TowerSelectProperties.onHideE += HideTSP;

        //ResourceManager.onRscChangedE += OnResourceChanged;
        UIHUD.ClickOnSpawnButton += ClickOnSpawnButton;
        ButtonStart.SetActive(false);
        InfoButtonStart.SetActive(false);
        /*for (int i = 0; i < Locks.Length; i++)
        {
            Locks[i].SetActive(true);
        }*/
        for (int i = 0; i < TextInfo.Length; i++)
        {
            TextInfo[i].SetActive(false);
        }
        Status = 0;
    }

    void OnDestroy()
    {
        UIBuildButton.ShowEvent -= ShowUIBuild;
        UIBuildButton.HideEvent -= HideUIBuild;
        UnitTower.onConstructionStartE  -= BuildTower;
        UnitTower.onConstructionCompleteE -= BuildTower;
        //ResourceManager.onRscChangedE -= OnResourceChanged;
        UIHUD.ClickOnSpawnButton -= ClickOnSpawnButton;

        UITowerInfo.OnShow -= UITowerShot;
        UITowerInfo.OnHide -= RemoveStatus;
        TowerSelectProperties.onShowE -= SelectPropertis;
        TowerSelectProperties.onHideE -= HideTSP;
    }

    private void EndTutorial()
    {
        Status = listStatus.end;
        gameObject.SetActive(false);
    }

    public void ShowUIBuild()
    {
        if (ShowUI || (Status != listStatus.posttion1 && Status != listStatus.position2 && Status != listStatus.position3))
            return;
        if (Status != listStatus.end)
            TimeScaleManager.SetTimeScale(0);
        ShowUI = true;
        Status++;
    }

    public void HideUIBuild()
    {
        ShowUI = false;
        if (Status == listStatus.tower1 || Status == listStatus.tower2 || Status == listStatus.tower3 || Status == listStatus.tower4)
        {
            TimeScaleManager.SetTimeScale(0);
            Status--;
        }
    }

    public void BuildTower(UnitTower t)
    {
        if (Status == listStatus.tower2)
            towerUpgradeT = t.thisT;
        if (Status == listStatus.tower1 || Status == listStatus.tower2 || Status == listStatus.tower3 || Status == listStatus.tower4 
            || _status == listStatus.waitBild1)
        {
            Status++;
            CountTower++;
        }
    }

    public void UITowerShot()
    {
        if (Status == listStatus.waitSelectTower)
            Status++;
    }

    public void SelectPropertis()
    {
        if (Status == listStatus.waitUpgrade)
            Status++;
    }

    public void AddStatus()
    {
        //Debug.LogWarning("AddStatus"+Status.ToString());
        Status++;
    }
    public void RemoveStatus()
    {
        if (Status == listStatus.waitUpgrade)
        {
            //Debug.LogWarning("RemoveStatus");
            Status--;
        }
    }
    public void HideTSP(bool upgrade)
    {
        if (upgrade)
        {
            if (Status <= listStatus.endUpgrade)
                Status = listStatus.end;
            else
                Status++;
        }
        else if (Status >= listStatus.endUpgrade)
            Status--;
    }

    public void ChangeStatus()
    {
        switch (_status)
        {
            case listStatus.general1:
                //TextInfo[2].SetActive(true);
                /*GeneralPanel.StartShow(AddStatus, new string[] {
                    "Командор! Приближаются <color=#FF0000>массовые</color> противники.",
                    "Рекомендую построить <color=#FFA500>Тяжелую</color> пушку."
                    //"Командор! Разведка докладывает, что на нас скоро нападут массовые противники.",
                    //"Как вы помните, против них рекомендуется использовать орудия с тяжелым уроном, давайте поставим одно."
                }, new int[] { 0, 1 });*/
                break;
            case listStatus.posttion1:
               // TutorialBuildTowerManager.Hide();
                ShowInfoFreePlatform();
                //TextInfo[0].SetActive(true);
                TextInfo[2].SetActive(false);
                break;
            case listStatus.tower1:
                HideInfoPlatform();
                TextInfo[0].SetActive(false);
                if (ResourceManager.GetResourceArray()[0] < 33)
                    ResourceManager.GainResource(new List<float>() { 10, 0 });
                /*if (TutorialBuildTowerManager.ShowTower(1, "Кликните чтобы построить башню") <= 0)
                    EndTutorial();*/
                break;
            case listStatus.waitBild1:
                //TutorialBuildTowerManager.Hide();
                break;
            case listStatus.startGame:
               // TutorialBuildTowerManager.Hide();
                ButtonStart.SetActive(true);
                InfoButtonStart.SetActive(true);
                break;
            case listStatus.general2:
                UIBuildButton.Hide();
               /* GeneralPanel.StartShow(AddStatus, new string[] {
                    "Командор! Противники всё <color=#FF0000>сильнее и сильнее</color>. Нам нужно что-то им <color=#00FF00>противопоставить</color>!",
                    "Срочно <color=#00FF00>улучшайте</color> наши <color=#FFA500>башни</color>!"
                    //"Отлично, мы готовы их встретить. Поехали!" 
                }, new int[] { 0, 0 });*/
                break;
            
            case listStatus.general3:
                UIBuildButton.Hide();
                /*GeneralPanel.StartShow(AddStatus, new string[] {
                    "Командор! Пушки с <color=#FFA500>тяжелым</color> уроном не атакуют <color=#FF0000>воздушные</color> цели.",
                    "Нам нужны пушки с <color=#FFA500>лёгким</color> уроном."
                    //"Командор! Совсем забыл вас предупредить - пушки с тяжелым уроном не атакуют воздушные цели.",
                    //"Против них лучше всего работают пушки с легким уроном, давайте построим одну." 
                },new int[] { 0, 1 });*/
                break;
            case listStatus.position2:
                if (FreePlatform(3))
                    TextInfoPlatform[3].SetActive(true);
                else
                    ShowInfoFreePlatform();
                //Locks[0].SetActive(false);
                //TextInfo[1].SetActive(true);
               // TutorialBuildTowerManager.Hide();
                break;
            case listStatus.tower2:
                TextInfo[1].SetActive(false);
                HideInfoPlatform();
                if (ResourceManager.GetResourceArray()[0] < 20)
                    ResourceManager.GainResource(new List<float>() { 20, 0 });
               /* if (TutorialBuildTowerManager.ShowTower(0, "Кликните чтобы построить башню",2) <= 0)
                    EndTutorial();*/
                break;
            case listStatus.waitCost2:
               // TutorialBuildTowerManager.Hide();
                break;

            case listStatus.waitSelectTower:
                if (ResourceManager.GetResourceArray()[0] < 30)
                    ResourceManager.GainResource(new List<float>() { 20, 0 });
                tooltipUpgradeTGo.SetActive(true);
                MoveTooltipUpgradeT.target = towerUpgradeT;
                break;
            case listStatus.waitUpgrade:
                tooltipUpgradeTGo.SetActive(false);
                UITowerInfo.ShowUpgradeCursor();
                break;
            case listStatus.endUpgrade:
                TowerSelectProperties.TutorialArrow(true);
                break;

            /*
            case listStatus.position3:
                Locks[1].SetActive(false);
                TextInfo[2].SetActive(true);
                TutorialBuildTowerManager.Hide();
                break;
            case listStatus.tower3:
                TextInfo[2].SetActive(false);
                if (bHaveRayT)
                {
                    if (ResourceManager.GetResourceArray()[0].value < 24)
                        ResourceManager.GainResource(new List<float>() { 24, 0 });
                    if (TutorialBuildTowerManager.ShowTower(3, "Кликните чтобы построить башню") <= 0)
                        EndTutorial();
                }
                else
                {
                    if (ResourceManager.GetResourceArray()[0].value < 20)
                        ResourceManager.GainResource(new List<float>() { 20, 0 });
                    if (TutorialBuildTowerManager.ShowTower(0, "Кликните чтобы построить башню") <= 0)
                        EndTutorial();
                }
                break;

            case listStatus.waitCost3:
                TextInfo[5].SetActive(false);
                break;
            case listStatus.position4:
                Locks[2].SetActive(false);
                TextInfo[6].SetActive(true);
                TextInfo[7].SetActive(false);
                break;
            case listStatus.tower4:
                TextInfo[7].SetActive(true);
                TextInfo[6].SetActive(false);
                break;*/
            case listStatus.end:
               // TutorialBuildTowerManager.Hide();
                for (int i = 0; i < Locks.Length; i++)
                {
                    Locks[i].SetActive(false);
                }
                for (int i = 0; i < TextInfo.Length; i++)
                {
                    TextInfo[i].SetActive(false);
                }
                TimeScaleManager.RefreshTimeScale();
                TowerSelectProperties.TutorialArrow(false);
                GameObject.Destroy(this.gameObject);
                break;
        }
    }

    private void HideInfoPlatform()
    {
        for (int i = 0; i < TextInfoPlatform.Length; i++)
        {
            TextInfoPlatform[i].SetActive(false);
        }
    }

    private int ShowInfoFreePlatform()
    {
        HideInfoPlatform();
        for (int i = 0; i < platforms.Length; i++)
        {
            if (FreePlatform(i))
            {
                TextInfoPlatform[i].SetActive(true);
                return i; 
            }
        }
        return -1;
    }

    private bool FreePlatform(int id)
    {
        return BuildManager.CheckBuildPoint(Camera.main.WorldToScreenPoint(platforms[id].thisT.position)) == _TileStatus.Available;
    }

    public void OnResourceChanged(List<float> valueChangedList)
    {
       /* if (_status == listStatus.waitCost1 || _status == listStatus.waitCost2 || _status == listStatus.waitCost3)
        {
            List<Rsc> rscList = ResourceManager.GetResourceArray();

            if (rscList[0].value >= 4 && (_status == listStatus.waitCost2 || _status == listStatus.waitCost3))
            {
                Status++;
                TimeScaleManager.SetTimeScale(0);
            }
            else if (SpawnManager.instance.waveClearedCount == 1 && _status == listStatus.waitCost1)
            {
                Status++;
                StartCoroutine(WaitTime(8));
            }
        }*/
    }

    private IEnumerator WaitTime(float time)
    {
        yield return new WaitForSeconds(time);
        Status++;
        TimeScaleManager.SetTimeScale(0);
    }

    public void ClickOnSpawnButton()
    {
        TimeScaleManager.RefreshTimeScale();
        Status++;

    }

 
    void Update()
    {
        if (GameControl.GetGameState() == _GameState.Play)
        {
            switch (_status)
            {
                case listStatus.posttion1:
                case listStatus.tower1:
                case listStatus.position2:
                case listStatus.tower2:
                case listStatus.position3:
                case listStatus.tower3:
                case listStatus.waitSelectTower:
                case listStatus.waitUpgrade:
                case listStatus.endUpgrade:
                    TimeScaleManager.SetTimeScale(0);
                    break;
                case listStatus.startGame:
                case listStatus.waitCost1:
                   /* if (SpawnManager.instance.waveClearedCount == 1)
                    {
                        Status++;
                        StartCoroutine(WaitTime(8));
                    }*/
                    TimeScaleManager.RefreshTimeScale();
                    break;
                case listStatus.waitCost2:
                    /*if (SpawnManager.instance.waveClearedCount == 2)
                    {
                        Status++;
                        StartCoroutine(WaitTime(8));
                    }*/
                    TimeScaleManager.RefreshTimeScale();
                    break;
                case listStatus.end:
                    TimeScaleManager.RefreshTimeScale();
                    break;
            }
        }
    }
}
