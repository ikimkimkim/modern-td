﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialUpdateCard : MonoBehaviour {

    public GameObject[] FonNotClick;
    public GameObject[] TextInfo;

    public enum listStatus { openManager,selectCard,ButtonUpgrade,selectCard2,ButtonCompite,end};
    public listStatus _status;
    public listStatus Status { get { return _status; } set { _status = value; ChangeStatus(); } }

    // Use this for initialization
    void Start ()
    {
        if(PlayerManager._instance.GetLevelsCountComplele()!=2)
        {
            Status = listStatus.end;
            return;
        }


        CardUpgradeManager.OnShowPanel += OpenManager;
        CardUpgradeManager.OnChangeSelectCard += SelectCard;
        CardUpgradeManager.instance.AddCardResurse += AddResurce;
        CardUpgradeManager.OnChangeMode += ChangeMode;
        CardUpgradeManager.OnUpdateCard += UpdateComplite;

        for (int i = 0; i < TextInfo.Length; i++)
        {
            TextInfo[i].SetActive(false);
        }
        Status = listStatus.openManager;
	}
	
	
    public void ChangeStatus()
    {
        switch(Status)
        {
            case listStatus.openManager:
                TextInfo[0].SetActive(true);
                break;
            case listStatus.selectCard:
                TextInfo[0].SetActive(false);
                TextInfo[1].SetActive(true);
                break;
            case listStatus.ButtonUpgrade:
                TextInfo[1].SetActive(false);
                TextInfo[2].SetActive(true);
                break;
            case listStatus.selectCard2:
                TextInfo[2].SetActive(false);
                TextInfo[3].SetActive(true);
                break;
            case listStatus.ButtonCompite:
                TextInfo[3].SetActive(false);
                TextInfo[4].SetActive(true);
                break;
            case listStatus.end:
                for (int i = 0; i < TextInfo.Length; i++)
                {
                    TextInfo[i].SetActive(false);
                }
                break;
        }
    }

    public void OpenManager()
    {
        Status++;
    }

    public void SelectCard()
    {
        if(_status== listStatus.selectCard)
            Status++;
    }

    public void AddResurce(List<GameObject> cards)
    {
        if(_status== listStatus.selectCard2)
            Status++;
    }

    public void ChangeMode()
    {
        Status++;
    }

    public void UpdateComplite()
    {
        Status++;
    }
}
