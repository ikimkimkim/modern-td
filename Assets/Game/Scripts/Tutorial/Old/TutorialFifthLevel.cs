﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;

//Переделан под 3 уровень!
public class TutorialFifthLevel : MonoBehaviour {

    public GameObject ButtonStart;
    public GameObject InfoButtonStart;
    public GameObject[] Locks;
    public GameObject[] TextInfo;

    public enum listStatus { history, general1, startButton,  end};
    public listStatus _status;
    public listStatus Status { get { return _status; } set { _status = value; ChangeStatus(); } }
    //public int Status=0;
    public bool ShowUI = false;

    private bool bHaveRayT, bHaveCannonT;


    void Start()
    {
        bool abilityHave = false;
        List<Card> Towers = PlayerManager.getAbilityCards;
        for (int i = 0; i < Towers.Count; i++)
        {
            if (Towers[i].disableInBuildManager == false)
            {

                abilityHave = true;
                break;
            }
        }

        if (PlayerManager._instance.GetLevelsCountComplele() != 2 || GameControl.GetLevelID() != 3 || abilityHave==false)
        {
            Status = listStatus.end;
            gameObject.SetActive(false);
            return;
        }
        
        ShowUI = false;
        UIBuildButton.ShowEvent += ShowUIBuild;
        //ResourceManager.onRscChangedE += OnResourceChanged;
        UIHUD.ClickOnSpawnButton += ClickOnSpawnButton;
        ButtonStart.SetActive(false);
        InfoButtonStart.SetActive(false);
        for (int i = 0; i < Locks.Length; i++)
        {
            Locks[i].SetActive(true);
        }
        for (int i = 0; i < TextInfo.Length; i++)
        {
            TextInfo[i].SetActive(false);
        }
        Status = 0;
    }

    void OnDestroy()
    {
        UIBuildButton.ShowEvent -= ShowUIBuild;
        //ResourceManager.onRscChangedE -= OnResourceChanged;
        UIHUD.ClickOnSpawnButton -= ClickOnSpawnButton;
    }

    private void EndTutorial()
    {
        Status = listStatus.end;
        gameObject.SetActive(false);
    }

    public void ShowUIBuild()
    {
        if (ShowUI)
            return;
        if (Status != listStatus.end)
            TimeScaleManager.SetTimeScale(0);
        ShowUI = true;
        Status++;
    }

    public void AddStatus()
    {
        Status++;
    }

    public void GeneralStatus(int status)
    {
        TextInfo[0].SetActive(status == 3);
        TextInfo[1].SetActive(status == 2);
    }

    public void ChangeStatus()
    {
        switch (_status)
        {
            case listStatus.history:
                AddStatus();
                //HistoryPanel.StartShow(AddStatus, new int[] { 4, 5, 6 });
                break;
            case listStatus.general1:
               // GeneralPanel.ChangeStatus += GeneralStatus;
               /* GeneralPanel.StartShow(AddStatus, new string[] {
                    "Командор, давайте я расскажу вам про <color=#FFA500>магию</color>.",
                    "Мы точно не знаем, как именно работает магия пришельцев. Но мы научились ее <color=#FFA500>использовать</color>.",
                    "Снизу вы видите список доступных вам <color=#FFA500>заклинаний</color>. Вы их можете <color=#00FF00>улучшать</color> в бою также, как башни.",
                    "Сверху вы можете видеть количество доступной <color=#0000FF>маны</color>. Она необходима для использования заклинаний и <color=#00FF00>пополняется</color> со временем.",
                    "<color=#FFA500>Заклинания</color> - это мощное оружие, используйте их с умом!"},
                     new int[] { 0, 1 ,1,1,1});*/
                break;
            case listStatus.startButton:
                ButtonStart.SetActive(true);
                InfoButtonStart.SetActive(true);
                Status++;
                break;
            case listStatus.end:
                //TutorialBuildTowerManager.Hide();
                for (int i = 0; i < Locks.Length; i++)
                {
                    Locks[i].SetActive(false);
                }
                for (int i = 0; i < TextInfo.Length; i++)
                {
                    TextInfo[i].SetActive(false);
                }
               // GeneralPanel.ChangeStatus -= GeneralStatus;
                GameObject.Destroy(this.gameObject);
                break;
        }
    }


    public void OnResourceChanged(List<float> valueChangedList)
    {
        /* if (_status == listStatus.waitCost1 || _status == listStatus.waitCost2 || _status == listStatus.waitCost3)
         {
             List<Rsc> rscList = ResourceManager.GetResourceArray();

             if (rscList[0].value >= 4 && (_status == listStatus.waitCost2 || _status == listStatus.waitCost3))
             {
                 Status++;
                 TimeScaleManager.SetTimeScale(0);
             }
             else if (SpawnManager.instance.waveClearedCount == 1 && _status == listStatus.waitCost1)
             {
                 Status++;
                 StartCoroutine(WaitTime(8));
             }
         }*/
    }

    private IEnumerator WaitTime(float time)
    {
        yield return new WaitForSeconds(time);
        Status++;
        TimeScaleManager.SetTimeScale(0);
    }

    public void ClickOnSpawnButton()
    {
        TimeScaleManager.RefreshTimeScale();
        Status++;

    }

    void Update()
    {
        if (GameControl.GetGameState() == _GameState.Play)
        {
            switch (_status)
            {
                case listStatus.end:
                    TimeScaleManager.RefreshTimeScale();
                    break;
            }
        }
    }
}
