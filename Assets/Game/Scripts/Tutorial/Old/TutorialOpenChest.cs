﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialOpenChest : MonoBehaviour {

    public GameObject[] FonNotClick;
    public GameObject[] TextInfo;
    public GameObject TextInfoPanelAllCard, TextInfoPanelUpgrades, ArrowCraftCreat, ArrowCraftUpgrade;

    public enum TypeTutorial { OpenChest, OpenChest2, ShowUpgrades, UpgradeCard, WastOpenChest, Craft };
    public TypeTutorial Type;
    public enum enumOpenChest { general1, WaitChest, openPanelChest, startOpenChest, waitTime, getPrizeChest,
        waitGetPrize, general2, OpenPanelAllCard, WaitOpenPanelInfoCard, WaitUpgradeCard, WaitClosePanelInfoCard,
        WaitCloseAllCardPanel, NextLevel, StartLevel,  end , SelectNewCard};
    public enum enumOpenChest2 { general1, WaitChest, openPanelChest, startOpenChest, waitTime, getPrizeChest, waitGetPrize, general2,  end };

    public enum enumShowUpgrades { general1, WaitOpenUpgrades , end}

    public enum enumUpgradeCard { WaitChest, openPanelChest, startOpenChest, waitTime, getPrizeChest, WaitAllInfoPanel,
        UpgradeButtonInfo, WaitAnimCard , CloseAllInfo, waitGetPrize, NextLevel,  end };
    public enum enumWastOpenChest { general1, WaitChest, openPanelChest, startOpenChest, getNowPrizeChest, end };

    public enum enumCraft { general1, waitOpenCraft, waitCloseCraft, general2, waitOpenCraftUpdate, waitCloseCraftUpdate, ganaral3, end}

    public int _status;
    public int Status { get { return _status; } set { _status = value; ChangeStatus(); } }

    public static TutorialOpenChest instance { get; private set; }

    void Start ()
    {
        instance = this;

        foreach (var top_info in PlayerManager.topDaysData.last_top_info)
        {
            if (top_info.Value.prize_geted != 1 && top_info.Value.user_res.place > 0)
            {
                Debug.LogError("Breack tutorial, now show panel top prize");
                return;
            }
        }

        /*if (PlayerManager.ChestList.Count == 0 || PlayerManager.ChestList[0].Status!=0)
        {
            Type = TypeTutorial.OpenChest;
            Status = (int)enumOpenChest.end;
            return;
        }*/

        if (PlayerManager._instance.GetLevelsCountComplele() == 1 && PlayerManager.ChestList.Find(c => c.Type == Chest.TypeChest.Wood) != null)
        {
            Type = TypeTutorial.OpenChest;
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 99999 && PlayerManager.ChestList.Find(c => c.Type == Chest.TypeChest.Wood) != null)
        {
            Type = TypeTutorial.ShowUpgrades;
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 99999)
        {
            Type = TypeTutorial.UpgradeCard;
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 4 && PlayerManager.ChestList.Find(c => c.Type == Chest.TypeChest.Silver)!=null)
        {
            Type = TypeTutorial.WastOpenChest;
        }
        else if (PlayerManager._instance.GetLevelsCountComplele() == 5 && CraftLevelData.getCountGrades == 0 && PlayerManager.GetCraftResources(1) >= 10)
        {
            Type = TypeTutorial.Craft;
        }
        else 
        {
            Type = TypeTutorial.OpenChest;
            Status = (int)enumOpenChest.end;
            return;
        }

        CardUIAllInfo.OnShow += OpenAllInfo;
        CardUIAllInfo.OnHide += HideAllInfo;
        CardUIAllInfo.StartUpgradeCard += StartUpgradeCard;

        ChestPanel.OnShow += AddStatus;
        ChestPanel.OnHide += HidePanelChest;
        ChestPanel.ChangeStatusNowPanelChest += OnChestNewStatus;
        ChestOpenPanel.OnHide += AddStatus;
        CardAndLevelSelectPanel.OnShow += CardAndLevelShow;
        CardAndLevelSelectPanel.OnChangeSelectCard += ChangeSelectCard;

        UICardsInventory.onShow += OpenManager;
        UICardsInventory.onHide += OpenManager;
        /*CardUpgradeManager.instance.AddCardResurse += AddResurce;
        CardUpgradeManager.OnChangeMode += ChangeModeUpgrade;
        CardUpgradeManager.OnUpdateCard += UpdateComplite;*/


        for (int i = 0; i < TextInfo.Length; i++)
        {
            TextInfo[i].SetActive(false);
        }
        Status = 0;
    }

    void OnDestroy()
    {
        CardUIAllInfo.OnShow -= OpenAllInfo;
        CardUIAllInfo.OnHide -= HideAllInfo;
        CardUIAllInfo.StartUpgradeCard -= StartUpgradeCard;

        ChestPanel.OnShow -= AddStatus;
        ChestPanel.OnHide -= HidePanelChest;
        ChestPanel.ChangeStatusNowPanelChest -= OnChestNewStatus;
        ChestOpenPanel.OnHide -= AddStatus;
        CardAndLevelSelectPanel.OnShow -= CardAndLevelShow;
        CardAndLevelSelectPanel.OnChangeSelectCard -= ChangeSelectCard;

        CardUpgradeManager.OnShowPanel -= OpenManager;
        CardUpgradeManager.OnHidePanel -= OpenManager;
        /*CardUpgradeManager.instance.AddCardResurse -= AddResurce;
        CardUpgradeManager.OnChangeMode -= ChangeModeUpgrade;
        CardUpgradeManager.OnUpdateCard -= UpdateComplite;*/
    }

    void OnChestNewStatus(int status)
    {
        Status++;
    }

    void AddStatus()
    {
        Status++;
    }

    void CardAndLevelShow()
    {
        if(_status == (int) enumOpenChest.NextLevel || _status == (int)enumUpgradeCard.NextLevel)
            Status++;
    }

    void StartUpgradeCard()
    {
        if (Type == TypeTutorial.UpgradeCard || Type == TypeTutorial.OpenChest && _status == (int)enumOpenChest.WaitUpgradeCard)
            Status++;
    }

    void OpenAllInfo()
    {
        if (_status == (int)enumUpgradeCard.WaitAllInfoPanel && Type == TypeTutorial.UpgradeCard || 
            Type == TypeTutorial.OpenChest && _status == (int) enumOpenChest.WaitOpenPanelInfoCard)
            Status++;
    }

    void HideAllInfo()
    {
        if (Type == TypeTutorial.UpgradeCard ||
            Type == TypeTutorial.OpenChest && _status == (int)enumOpenChest.WaitClosePanelInfoCard)
            Status++;
    }

    void HidePanelChest()
    {
        if (Type == TypeTutorial.WastOpenChest)
        {
            Status = (int)enumWastOpenChest.end;
            return;
        }
        Status++;
    }

    public void OpenManager()
    {
        if(_status == (int)enumOpenChest.OpenPanelAllCard || _status ==(int)enumOpenChest.WaitCloseAllCardPanel)
        Status++;
    }

    public void AddResurce(List<GameObject> cards)
    {
        //if (_status == (int)enumUpgradeCard.SelectTwoCard)
        //    Status++;
    }

    public void ChangeModeUpgrade()
    {
        //if(_status==(int)enumUpgradeCard.UpgradeButton)
       // Status++;
    }

    public void UpdateComplite()
    {
        Status++;
    }

    void ChangeSelectCard()
    {
        if (Type == TypeTutorial.OpenChest)
        {
            if((enumOpenChest) Status== enumOpenChest.SelectNewCard)
            {
                CardAndLevelSelectPanel.OnChangeSelectCard -= ChangeSelectCard;
                Status++;
            }
        }
        else
        {
            CardAndLevelSelectPanel.OnChangeSelectCard -= ChangeSelectCard;
            Status++;
        }
    }

    public void ChangeStatus()
    {
        for (int i = 0; i < TextInfo.Length; i++)
        {
            TextInfo[i].SetActive(false);
        }
        for (int i = 0; i < FonNotClick.Length; i++)
        {
            FonNotClick[i].SetActive(false);
        }

        switch (Type)
        {
            case TypeTutorial.OpenChest: OpenChest(); break;
            case TypeTutorial.OpenChest2: OpenChest2(); break;
            case TypeTutorial.ShowUpgrades: ShowUpgrades(); break;
            case TypeTutorial.UpgradeCard: UpgradeCard(); break;
            case TypeTutorial.WastOpenChest: WastOpenChest(); break;
            case TypeTutorial.Craft: CraftTutorial(); break;
        }
    }

    private void CraftTutorial()
    {
        Debug.LogWarning("CraftTutorial");
        switch (_status)
        {
            case (int)enumCraft.general1:
               /* GeneralPanel.StartShow(AddStatus, new string[] {
                    "Командор! Наши инженеры предоставляют в ваше распоряжение секретные наработки для <color=#eacf22>улучшения</color> вооружения.",
                    "Загляните в <color=#eacf22>мастерскую \"Сборка\"</color>! Вам однозначно это пригодится в этом нелёгком противостоянии."
                }, new int[] { 0, 1});*/
                break;

            case (int)enumCraft.waitOpenCraft:
                ArrowCraftCreat.SetActive(true);
                UICraftPanel.OnShow += AddStatus;
                break;

            case (int)enumCraft.waitCloseCraft:
                ArrowCraftCreat.SetActive(false);
                UICraftPanel.OnShow -= AddStatus;
                UICraftPanel.OnHide += AddStatus;
                //UICraftPanel.StartTutorial();
                break;

            case (int)enumCraft.general2:
                UICraftPanel.OnHide -= AddStatus;
               /* GeneralPanel.StartShow(AddStatus, new string[] {
                    "Теперь с имеющимися наработками выдвигайтесь в <color=#eacf22>мастерскую \"Улучшения\"!</color>"
                }, new int[] { 1 });*/
                break;

            case (int)enumCraft.waitOpenCraftUpdate:
                ArrowCraftUpgrade.SetActive(true);
                UICardUpgradePanel.OnShow += AddStatus;
                break;

            case (int)enumCraft.waitCloseCraftUpdate:
                ArrowCraftUpgrade.SetActive(false);
                UICardUpgradePanel.OnShow -= AddStatus;
                UICardUpgradePanel.OnHide += AddStatus;
                //UICardUpgradePanel.StartTutorial();
                break;

            case (int)enumCraft.ganaral3:
                UICardUpgradePanel.OnHide -= AddStatus;
                /*GeneralPanel.StartShow(AddStatus, new string[] {
                    "Удачи на поле боя! Надеюсь вы справитесь!"
                }, new int[] { 0 });*/
                break;

            case (int)enumCraft.end:
                Destroy(gameObject);
                break;
        }
    }

    private void WastOpenChest()
    {
           switch (_status)
        {
            case (int)enumWastOpenChest.general1:
                /*GeneralPanel.StartShow(AddStatus, new string[] {
                    "Командор, мы получили <color=#FFA500>серебряный</color> сундук.",
                    "Он намного лучше <color=#FFA500>деревянного</color>, но и открывается дольше.",
                    "Вы всегда можете <color=#00FF00>ускорить</color> открытие сундука, использовав <color=#0000FF>кристаллы</color>.",
                    "Не бойтесь их <color=#00FF00>тратить</color> - вы всегда можете <color=#0000FF>найти</color> или приобрести новые."
                    //"Командор, мы получили серебряный сундук. Он намного лучше деревяного, но и на его открытие требуется больше времени.",
                    //"Вы всегда можете ускорить открытие сундука, использовав кристаллы. Не бойтесь их тратить - вы всегда можете найти или приобрести новые."
                }, new int[] { 0, 1, 1, 1 });*/
                break;
            case (int)enumWastOpenChest.WaitChest:
                FonNotClick[0].SetActive(true);
                StartCoroutine(WaitChestAmin());
                break;
            case (int)enumWastOpenChest.openPanelChest:
                //if (ChestManager.StartTutorial(Chest.TypeChest.Silver)==false)
                {
                    Status = (int)enumWastOpenChest.end;
                    return;
                }
                FonNotClick[0].SetActive(true);
                break;
            case (int)enumWastOpenChest.startOpenChest:
                if (ChestPanel.instance.chest.Type == Chest.TypeChest.Wood)
                {
                    _status = (int)enumWastOpenChest.end;
                }
                ChestManager.EndTutorial();
                //ChestPanel.TutorialStartOpen(true);
                break;
            case (int)enumWastOpenChest.getNowPrizeChest:
                //ChestPanel.TutorialStartOpen(false);
                //ChestPanel.TutorialSkip(true);
                //FonNotClick[1].SetActive(true);
                break;
            case (int)enumWastOpenChest.end:
                //ChestPanel.TutorialSkip(false);
                Destroy(gameObject);
                break;
        }
    }

    private void UpgradeCard()
    {
        switch (_status)
        {
           
            case (int)enumUpgradeCard.WaitChest:
                FonNotClick[0].SetActive(true);
                StartCoroutine(WaitChestAmin());
                break;
            case (int)enumUpgradeCard.openPanelChest:
                TextInfo[0].SetActive(true);
                FonNotClick[0].SetActive(true);
                break;
            case (int)enumUpgradeCard.startOpenChest:
                TextInfo[1].SetActive(true);
                FonNotClick[1].SetActive(true);
                break;
            case (int)enumUpgradeCard.waitTime:
                TextInfo[2].SetActive(true);
                FonNotClick[1].SetActive(true);
                break;
            case (int)enumUpgradeCard.getPrizeChest:
                TextInfo[3].SetActive(true);
                FonNotClick[1].SetActive(true);
                break;

            case (int)enumUpgradeCard.WaitAllInfoPanel:
                TextInfo[14].SetActive(true);
                FonNotClick[10].SetActive(true);
                break;
            case (int)enumUpgradeCard.UpgradeButtonInfo:
                TextInfo[15].SetActive(true);
                FonNotClick[11].SetActive(true);
                break;
            case (int)enumUpgradeCard.WaitAnimCard:
                StartCoroutine(WaitCardAmin());
                break;
            case (int)enumUpgradeCard.CloseAllInfo:
                TextInfo[16].SetActive(true);
                FonNotClick[12].SetActive(true);
                break;

           /* case (int)enumUpgradeCard.ButtonCard:
                TextInfo[7].SetActive(true);
                FonNotClick[5].SetActive(true);
                break;
            case (int)enumUpgradeCard.UpgradeButton:
                TextInfo[8].SetActive(true);
                FonNotClick[6].SetActive(true);
                break;
            case (int)enumUpgradeCard.SelectTwoCard:
                TextInfo[9].SetActive(true);
                FonNotClick[7].SetActive(true);
                //TextInfo[5].GetComponent<RectTransform>().anchoredPosition = new Vector2(225 + 80 * CardAndLevelSelectPanel.indexCardNotSelect, -30);
                break;
            case (int)enumUpgradeCard.CompliteUpgradeButton:
                TextInfo[10].SetActive(true);
                FonNotClick[6].SetActive(true);
                break;
            case (int)enumUpgradeCard.CloseCardPanel:
                TextInfo[11].SetActive(true);
                FonNotClick[8].SetActive(true);
                break;*/
            case (int)enumUpgradeCard.NextLevel:
                TextInfo[12].SetActive(true);
                FonNotClick[9].SetActive(true);
                break;
            case (int)enumUpgradeCard.end:
                Destroy(gameObject);
                break;
        }
    }

    public void OpenUpgradsPanel()
    {
        if (Type == TypeTutorial.ShowUpgrades && Status == (int)enumShowUpgrades.WaitOpenUpgrades)
            Status++;
    }

    private void OpenChest()
    {
        switch (_status)
        {
            case (int)enumOpenChest.general1:
               /* GeneralPanel.StartShow(AddStatus, new string[] {
                    "Командор! Мы захватили <color=#FFA500>сундук</color>. Надо его срочно <color=#00FF00>исследовать</color>.",
                    "Помните, чем круче сундук - тем больше там <color=#00FF00>добычи</color>, но и открывается он <color=#FF0000>дольше</color>.",
                    "А сейчас давайте посмотрим на нашу первую <color=#00FF00>добычу</color>."
                    //"Командор! Отряд тварей, который мы уничтожили, перевозил сундук. Мы можем найти в нём множество интересных вещей.",
                    //"Помните, чем более редкий сундук вы захватите, тем больше в нём будет полезных вещей, но и на его открытие наши инженеры потратят больше времени.",
                    //"А сейчас, давайте посмотрим на нашу первую добычу."
                }, new int[] { 0, 1, 1 });*/
                break;
            case (int)enumOpenChest.WaitChest:
                FonNotClick[0].SetActive(true);
                StartCoroutine(WaitChestAmin());
                break;
            case (int)enumOpenChest.openPanelChest:
                //if(ChestManager.StartTutorial(Chest.TypeChest.Wood) == false)
                {
                    Status = (int)enumOpenChest.end;
                    return;
                }
                FonNotClick[0].SetActive(true);
                break;
            case (int)enumOpenChest.startOpenChest:
                ChestManager.EndTutorial();
                //ChestPanel.TutorialStartOpen(true);
                //TextInfo[1].SetActive(true);
                FonNotClick[1].SetActive(true);
                break;
            case (int)enumOpenChest.waitTime:
                //ChestPanel.TutorialStartOpen(false);
                //ChestPanel.TutorialWait(true);
                //TextInfo[2].SetActive(true);
                FonNotClick[1].SetActive(true);
                break;
            case (int)enumOpenChest.getPrizeChest:
                //ChestPanel.TutorialWait(false);
                //ChestPanel.TutorialGetPrize(true);

                //TextInfo[3].SetActive(true);
                FonNotClick[1].SetActive(true);
                break;
            case (int)enumOpenChest.general2:
                bool find = false;
                for (int i = 0; i < PlayerManager.CardsList.Count; i++)
                {
                    if (PlayerManager.CardsList[i].LevelCard>1)
                    {
                        Status = (int)enumOpenChest.end;
                        return;
                    }
                    if (PlayerManager.CardsList[i].count >= PlayerManager.CardsList[i].next_level_count)
                    {
                        find = true;
                        break;
                    }
                }
                if (find == false)
                {
                    Status = (int)enumOpenChest.end;
                    return;
                }
              /*  GeneralPanel.StartShow(AddStatus, new string[] {
                    "Мы получили образцы технологий пришельцев. Теперь наши инженеры могут <color=#00FF00>улучшить</color> имеющиеся <color=#FFA500>башни</color>."
                    //"В <color=#00FF00>сундуках</color> мы находим <color=#FFA500>башни</color> противника. Они отображаются у вас в виде <color=#FFA500>карточек</color>.",
                    //"Когда у вас <color=#00FF00>накапливается</color> достаточное количество <color=#FFA500>карточек</color> - мы можем их <color=#00FF00>улучшать</color>, чтобы строить более мощные <color=#FFA500>башни</color>.",
                    //"Не забывайте улучшать <color=#FFA500>карты</color>, когда появляется такая возможность!"
                    
                    //"Мы нашли очередную карту башни. Когда у нас накапливается достаточное количество одинаковых карт - наши инженеры и ученые могут улучшить карту данного типа, это улучшает ее характеристики.",
                    //"Не забывайте улучшать карты, когда появляется такая возможность!"
                }, new int[] { 1 });*/
                /*GeneralPanel.StartShow(AddStatus, new string[] {
                    "Отлично. <color=#00FF00>Добычу</color> взяли, можно переходить к следующей <color=#FFA500>миссии</color>."},
                    new int[] { 0 });*/
                break;

            case (int)enumOpenChest.OpenPanelAllCard:
                TextInfoPanelAllCard.SetActive(true);
                break;
            case (int)enumOpenChest.WaitOpenPanelInfoCard:
                TextInfoPanelAllCard.SetActive(false);
                UICardsInventory.instance.ShowTutorialUpgradeCard();
                //CardUpgradeManager.TutorialSelectCardUpgrade();

                break;
            case (int)enumOpenChest.WaitUpgradeCard:

               // UICardsInventory.instance.HideTutorial();
                //CardUpgradeManager.TutorialHide();
                //CardUIAllInfo.TutorialShowUpgrade(true);

                break;
            case (int)enumOpenChest.WaitClosePanelInfoCard:
               // CardUIAllInfo.TutorialShowUpgrade(false);

                break;
            case (int)enumOpenChest.WaitCloseAllCardPanel:
                break;

            case (int)enumOpenChest.NextLevel:
                TextInfo[4].SetActive(true);
                FonNotClick[2].SetActive(true);
                break;
            case (int)enumOpenChest.SelectNewCard:
                FonNotClick[3].SetActive(true);
                //TextInfo[5].GetComponent<RectTransform>().anchoredPosition = new Vector2(TextInfo[5].GetComponent<RectTransform>().anchoredPosition.x + 80 * CardAndLevelSelectPanel.indexCardNotSelect, -160);
                TextInfo[5].SetActive(true);
                break;
            case (int)enumOpenChest.StartLevel:
                TextInfo[6].SetActive(true);
                FonNotClick[4].SetActive(true);
                break;
            case (int)enumOpenChest.end:
                Destroy(gameObject);
                break;
        }
    }

    private void OpenChest2()
    {
        switch (_status)
        {
            case (int)enumOpenChest2.general1:
               /* GeneralPanel.StartShow(AddStatus, new string[] {
                    "Командор! Мы получили очередной <color=#00FF00>сундук</color>, давайте его откроем!"},
                    new int[] {  1});*/
                break;
            case (int)enumOpenChest2.WaitChest:
                FonNotClick[0].SetActive(true);
                StartCoroutine(WaitChestAmin());
                break;
            case (int)enumOpenChest2.openPanelChest:
                TextInfo[0].SetActive(true);
                FonNotClick[0].SetActive(true);
                break;
            case (int)enumOpenChest2.startOpenChest:
                TextInfo[1].SetActive(true);
                FonNotClick[1].SetActive(true);
                break;
            case (int)enumOpenChest2.waitTime:
                TextInfo[2].SetActive(true);
                FonNotClick[1].SetActive(true);
                break;
            case (int)enumOpenChest2.getPrizeChest:
                TextInfo[3].SetActive(true);
                FonNotClick[1].SetActive(true);
                break;
            case (int)enumOpenChest2.general2:
               /* GeneralPanel.StartShow(AddStatus, new string[] {
                    "В <color=#00FF00>сундуках</color> мы находим <color=#FFA500>башни</color> противника. Они отображаются у вас в виде <color=#FFA500>карточек</color>.",
                    "Когда у вас <color=#00FF00>накапливается</color> достаточное количество <color=#FFA500>карточек</color> - мы можем их <color=#00FF00>улучшать</color>, чтобы строить более мощные <color=#FFA500>башни</color>.",
                    "Не забывайте улучшать <color=#FFA500>карты</color>, когда появляется такая возможность!"
                    //"Мы нашли очередную карту башни. Когда у нас накапливается достаточное количество одинаковых карт - наши инженеры и ученые могут улучшить карту данного типа, это улучшает ее характеристики.",
                    //"Не забывайте улучшать карты, когда появляется такая возможность!"
                }, new int[] { 1,1, 1 });*/
                break;
            case (int)enumOpenChest.end:
                Destroy(gameObject);
                break;
        }
    }

    private void ShowUpgrades()
    {
        switch (_status)
        {
            case (int)enumShowUpgrades.general1:
               /* GeneralPanel.StartShow(AddStatus, new string[] {
                    "Командор! Теперь вы можете <color=#00FF00>модернизировать</color> башни!",
                    "Чем больше <color=#FFA500>звёзд</color> вы накопите проходя уровни, тем больше <color=#00FF00>улучшений</color> сможете произвести."
                    //"Командор! Мы получили очередной сундук, давайте его откроем!"
                }, new int[] { 0, 1 });*/
                break;
            case (int)enumShowUpgrades.WaitOpenUpgrades:
                TextInfo[17].SetActive(true);

                break;
           
            case (int)enumShowUpgrades.end:
                Destroy(gameObject);
                break;
        }
    }
    IEnumerator WaitChestAmin()
    {
       /* if(ChestManager.TypeNewChest>-1)
        {
            yield return new WaitForSeconds(0.5f);
            while (ChestManager.instance.WaitChest!=null)
            {
                yield return new WaitForSeconds(0.2f);
            }
        }*/

        Status++;
        yield break;
    }

    IEnumerator WaitCardAmin()
    {
        while(CardUIAllInfo.instance.isUpgradeNow==true)
        {
            yield return new WaitForSecondsRealtime(0.8f);// StartCoroutine(TimeScaleManager.WaitForRealSeconds(0.8f));
        }
        Status++;
    }
}
