﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDTK;

public class Tutorial5Level : MonoBehaviour
{

    public enum listStatus { history, waitWave, waitTime, general, waitUpgradeAbility, waitSelectProperties, end };
    public listStatus _status;
    public listStatus Status { get { return _status; } set { _status = value; ChangeStatus(); } }

    void Start ()
    {
        return;
        bool abilityHave = false;
        List<Card> Towers = PlayerManager.getAbilityCards;
        for (int i = 0; i < Towers.Count; i++)
        {
            if (Towers[i].disableInBuildManager == false && Towers[i].LevelCard>=2)
            {
                abilityHave = true;
                break;
            }
        }

        if (PlayerManager._instance.GetLevelsCountComplele() != 4 || GameControl.GetLevelID() != 5 || abilityHave == false)
        {
            Status = listStatus.end;
            gameObject.SetActive(false);
            return;
        }

        TowerSelectProperties.onShowE += SelectPropertis;
        TowerSelectProperties.onHideE += HideTSP;
        Status = 0;
    }

    void OnDestroy()
    {
        TowerSelectProperties.onShowE -= SelectPropertis;
        TowerSelectProperties.onHideE -= HideTSP;
    }

    public void SelectPropertis()
    {
        if (Status == listStatus.waitUpgradeAbility && TowerSelectProperties.ShowCard.Type == _CardType.Ability)
            Status++;
    }

    public void HideTSP(bool upgrade)
    {
        if (upgrade)
        {
            if (Status <= listStatus.waitSelectProperties && TowerSelectProperties.ShowCard.Type == _CardType.Ability)
                Status = listStatus.end;
            else
                Status++;
        }
        else if (Status >= listStatus.waitSelectProperties)
            Status--;
    }

    public void AddStatus()
    {
        Status++;
    }

    public void ChangeStatus()
    {
        switch (_status)
        {
            case listStatus.history:
                AddStatus();
                //HistoryPanel.StartShow(AddStatus, new int[] { 10, 11, 12 });
                break;
            case listStatus.general:
                UIBuildButton.Hide();
                /*GeneralPanel.StartShow(AddStatus, new string[] {
                    "Командор! Сила врага растёт.",
                    "Но мы тоже не пальцем деланы! Нужно <color=#00FF00>улучшить</color> <color=#FFA500>заклинания</color>!"},
                     new int[] { 0, 1 });*/
                break;
            case listStatus.waitUpgradeAbility:
                if (ResourceManager.GetResourceArray()[0] < 25)
                    ResourceManager.GainResource(new List<float>() { 25, 0 });
                UIAbilityButton.instance.TutorialShowUpgradeCurcor(2);
                break;
            case listStatus.waitSelectProperties:
                UIAbilityButton.instance.TutorialHide();
                TowerSelectProperties.TutorialArrow(true);
                break;
            case listStatus.end:
                //TutorialBuildTowerManager.Hide();
                TowerSelectProperties.TutorialArrow(false);
                GameObject.Destroy(this.gameObject);
                break;
        }
    }

    private IEnumerator WaitTime(float time)
    {
        yield return new WaitForSeconds(time);
        Status++;
        TimeScaleManager.SetTimeScale(0);
    }

    void Update()
    {

        if (GameControl.GetGameState() == _GameState.Play)
        {
            switch (_status)
            {
                case listStatus.waitWave:
                   /* if (SpawnManager.instance.waveClearedCount == 1)
                    {
                        Status++;
                        StartCoroutine(WaitTime(5));
                    }*/
                    TimeScaleManager.RefreshTimeScale();
                    break;
                case listStatus.waitTime:
                case listStatus.end:
                    TimeScaleManager.RefreshTimeScale();
                    break;
                default:
                    TimeScaleManager.SetTimeScale(0);
                    break;
            }
        }
    }
}
