﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorualFirsOpenChest : MonoBehaviour
{   
    private enum StatusEnum { General, OpenInfoChest, OpenChest, WaitTakePrize, General2, end }
    [SerializeField]
    private StatusEnum _Status;
    private static bool isEnd = false;

    void Start()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() != 1 || PlayerManager.ChestList.Exists(c => c.Type == Chest.TypeChest.Wood)==false 
             || isEnd)
        {
            EndTutorial();
            return;
        }

        isEnd = true;
        _Status = StatusEnum.General;
        UpdateTutorial();
    }

    public void Done()
    {
        QuestsTraining.Done();
    }
    public void Clear()
    {
        QuestsTraining.ClearAll();
    }

    public void Complited()
    {
        QuestsTraining.CompliteAll();
    }

    public void StartAll()
    {
        QuestsTraining.StartAll();
    }

    private void UpdateStatus()
    {
        _Status++;
        UpdateTutorial();
    }


    private void UpdateTutorial()
    {
        switch (_Status)
        {
            case StatusEnum.General:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор! Мы захватили <color=#FFA500>сундук</color>. Надо его срочно <color=#00FF00>исследовать</color>.",
                    GeneralPos.Salutes),
                    new GeneralState( "Помните, чем круче сундук - тем больше там <color=#00FF00>добычи</color>, но и открывается он <color=#FF0000>дольше</color>.",
                    GeneralPos.Teaches),
                    new GeneralState("А сейчас давайте посмотрим на нашу первую <color=#00FF00>добычу</color>.",
                    GeneralPos.Teaches)
                });
                break;
            case StatusEnum.OpenInfoChest:
                if (ChestManager.StartTutorial(Chest.TypeChest.Wood, UpdateStatus) == false)
                    EndTutorial();
                break;
            case StatusEnum.OpenChest:
                ChestPanel.StartTutorial(ChestPanel.TutorialType.WaitTime, UpdateStatus);
                break;
            case StatusEnum.WaitTakePrize:
                ChestOpenPanel.StartTutorial(UpdateStatus);
                break;
            case StatusEnum.General2:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("В сундуках мы находим башни противника. Они отображаются у вас в виде карточек.",
                    GeneralPos.Teaches),
                    new GeneralState("Башни это наше всё. Только благодаря им и вашим умениям мы сможем сдержать натиск врага.",
                    GeneralPos.Teaches)
                });
                break;
            case StatusEnum.end:
                EndTutorial();
                break;
        }
        
    }

    private void EndTutorial()
    {
        Debug.Log("TutorualFirsOpenChest EndTutorial");
        _Status = StatusEnum.end;
        this.enabled = false;
    }

}
