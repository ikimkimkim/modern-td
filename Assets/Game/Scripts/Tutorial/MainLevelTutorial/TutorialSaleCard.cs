﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialSaleCard : MonoBehaviour
{
    private PrizeCard prizeCard;
    private enum StatusEnum { General, InventorySaleCard, General2, end }
    [SerializeField]
    private StatusEnum _Status;
    private static bool isEnd = false;
    void Start()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() != 4 || CardAndLevelSelectPanel.isLastLevelWin==false || isEnd)
        {
            EndTutorial();
            return;
        }

        isEnd = true;
        
        _Status = StatusEnum.General;
        UpdateTutorial();
    }



    private void UpdateStatus()
    {
        _Status++;
        UpdateTutorial();
    }


    private void UpdateTutorial()
    {
        switch (_Status)
        {
            case StatusEnum.General:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор, если у вас появились ненужные карты вы можете разобрать их на ресурсы. Сейчас покажу.",
                    GeneralPos.Teaches)
                });
                break;
           
            case StatusEnum.InventorySaleCard:
                UICardsInventory.StartTutorial(UICardsInventory.TutorialType.saleCard, UpdateStatus);
                break;

            case StatusEnum.General2:
                if (CardSalePanel.isTutorialBreak)
                    EndTutorial();
                else
                    GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                        new GeneralState("Данные ресурсы очень ценны. Из них можно создавать различные полезные вещи.",
                        GeneralPos.Tactic),
                        new GeneralState("Но в дальнейшем выбор только за вами. Расставляйте правильные приоритеты!",
                        GeneralPos.Salutes)
                    });
                
                break;

            case StatusEnum.end:
                EndTutorial();
                break;
        }

    }


    private void EndTutorial()
    {
        Debug.Log("TutorialSaleCard EndTutorial");
        _Status = StatusEnum.end;
        this.enabled = false;
    }


    private void Responce(ResponseData data)
    {

    }
}
