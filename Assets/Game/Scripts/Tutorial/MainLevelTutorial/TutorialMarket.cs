﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMarket : MonoBehaviour {


    private enum StatusEnum { General, Market, General2, end }
    [SerializeField]
    private StatusEnum _Status;
    private static bool isEnd = false;

    void Start()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() != 11 || CardAndLevelSelectPanel.isLastLevelWin == false || isEnd)
        {
            EndTutorial();
            return;
        }

        isEnd = true;
        _Status = StatusEnum.General;
        UpdateTutorial();
    }



    private void UpdateStatus()
    {
        _Status++;
        UpdateTutorial();
    }


    private void UpdateTutorial()
    {
        switch (_Status)
        {
            case StatusEnum.General:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор, загляните в наш магазин. Там много полезного для улучшения вашего отряда.",
                    GeneralPos.Salutes, GeneralOrientation.right)
                });
                break;

            case StatusEnum.Market:
                StoreManager.StartTutorial(UpdateStatus);
                break;

            case StatusEnum.General2:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор, заглядывайте сюда чаще. Иногда появляются весьма интересные предложения",
                    GeneralPos.Teaches)
                });
                break;

            case StatusEnum.end:
                EndTutorial();
                break;
        }

    }


    private void EndTutorial()
    {
        Debug.Log("TutorialMarket EndTutorial");
        _Status = StatusEnum.end;
        this.enabled = false;
    }

}
