﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialEndPack : MonoBehaviour {


    private enum StatusEnum { General,  end }
    [SerializeField]
    private StatusEnum _Status;
    private static bool isEnd = false;

    void Start()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() != 12 || CardAndLevelSelectPanel.isLastLevelWin == false || isEnd)
        {
            EndTutorial();
            return;
        }

        isEnd = true;
        _Status = StatusEnum.General;
        UpdateTutorial();
    }



    private void UpdateStatus()
    {
        _Status++;
        UpdateTutorial();
    }


    private void UpdateTutorial()
    {
        switch (_Status)
        {
            case StatusEnum.General:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор, до меня дошли тревожные новости. На пройденных участках снова заметили пришельцев.",
                    GeneralPos.Salutes),
                    new GeneralState("Придётся провести повторную зачистку.",
                    GeneralPos.Tactic),
                    new GeneralState("Не волнуйтесь, командование назначило вам дополнительную награду за подобные вылазки. Вперёд!",
                    GeneralPos.Teaches)
                });
                break;
            case StatusEnum.end:
                EndTutorial();
                break;
        }

    }


    private void EndTutorial()
    {
        Debug.Log("TutorialMarket EndTutorial");
        _Status = StatusEnum.end;
        this.enabled = false;
    }

}
