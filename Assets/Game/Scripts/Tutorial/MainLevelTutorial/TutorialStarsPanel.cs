﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialStarsPanel : MonoBehaviour
{
    private enum StatusEnum { General, StarsPanel, end }
    [SerializeField]
    private StatusEnum _Status;
    private static bool isEnd = false;

    void Start()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() != 7 || CardAndLevelSelectPanel.isLastLevelWin == false || isEnd)
        {
            EndTutorial();
            return;
        }
        isEnd = true;

        _Status = StatusEnum.General;
        UpdateTutorial();
    }



    private void UpdateStatus()
    {
        _Status++;
        UpdateTutorial();
    }


    private void UpdateTutorial()
    {
        switch (_Status)
        {
            case StatusEnum.General:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор, у наших учёных сюрприз для вас. Загляните в отдел исследований.",
                    GeneralPos.Salutes)
                });
                break;

            case StatusEnum.StarsPanel:
                UIStarPerk.StartTutorial(UpdateStatus);
                break;

            case StatusEnum.end:
                EndTutorial();
                break;
        }

    }


    private void EndTutorial()
    {
        Debug.Log("TutorialStarsPanel EndTutorial");
        _Status = StatusEnum.end;
        this.enabled = false;
    }


}
