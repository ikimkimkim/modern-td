﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialGetAbility : MonoBehaviour
{
    private enum StatusEnum { General, TakeAbility, General2, end }
    [SerializeField]
    private StatusEnum _Status;

    private static bool isEnd = false;

    void Start()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() != 2 || CardAndLevelSelectPanel.isLastLevelWin == false || isEnd)
        {
            EndTutorial();
            return;
        }
        isEnd = true;
        CardAndLevelSelectPanel.OnShow += CardAndLevelSelectPanel_OnShow;
    }

    private void CardAndLevelSelectPanel_OnShow()
    {
        CardAndLevelSelectPanel.OnShow -= CardAndLevelSelectPanel_OnShow;
        if(PlayerManager.CardsList.Exists(c => c.Type == _CardType.Ability) == false)
        {
            EndTutorial();
            return;
        }

        _Status = StatusEnum.General;
        UpdateTutorial();
    }

    void Destory()
    {
        CardAndLevelSelectPanel.OnShow -= CardAndLevelSelectPanel_OnShow;
    }

    private void UpdateStatus()
    {
        _Status++;
        UpdateTutorial();
    }


    private void UpdateTutorial()
    {
        switch (_Status)
        {
            case StatusEnum.General:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор! У нас пополнение арсенала. Возьмите с собой в бой заклинание.",
                    GeneralPos.Teaches)
                });
                break;
            case StatusEnum.TakeAbility:
                CardAndLevelSelectPanel.StartTutorial(_CardType.Ability, UpdateStatus);
                break;
            case StatusEnum.General2:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Думаю с такой мощью пришельцы дважды подумают, чтобы нападать на нас в следующий раз!",
                    GeneralPos.Teaches)
                });
                break;
            case StatusEnum.end:
                EndTutorial();
                break;
        }

    }

    private void EndTutorial()
    {
        Debug.Log("TutorialGetAbility EndTutorial");
        _Status = StatusEnum.end;
        this.enabled = false;
    }

}