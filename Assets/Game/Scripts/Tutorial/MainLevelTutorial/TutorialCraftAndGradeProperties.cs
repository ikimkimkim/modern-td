﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialCraftAndGradeProperties : MonoBehaviour {


    private enum StatusEnum { General, Craft, General2, Grade, end }
    [SerializeField]
    private StatusEnum _Status;
    private static bool isEnd = false;

    void Start()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() != 14 || CardAndLevelSelectPanel.isLastLevelWin == false || isEnd)
        {
            EndTutorial();
            return;
        }

        isEnd = true;
        _Status = StatusEnum.General;
        UpdateTutorial();
    }



    private void UpdateStatus()
    {
        _Status++;
        UpdateTutorial();
    }


    private void UpdateTutorial()
    {
        switch (_Status)
        {
            case StatusEnum.General:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор, спешу вас порадовать! У нас для вас есть нечто очень интересное и полезное!",
                    GeneralPos.Salutes, GeneralOrientation.right)
                });
                break;

            case StatusEnum.Craft:
                UICraftPanel.StartTutorial(UpdateStatus);
                break;

            case StatusEnum.General2:
                if (PlayerManager.CardsList.Exists(i => i.IDTowerData == 500 && i.rareness == _CardRareness.Usual) == false)
                {
                    Debug.LogError("Tutorial Card upgrade not find!");
                    EndTutorial();
                    return;
                }

                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Это ещё не всё. Вы можете дополнительно усилить ваши башни, заклинания и поддержку!",
                    GeneralPos.Teaches, GeneralOrientation.right),
                    new GeneralState("Зайдите в Мастрескую Улучшения",
                    GeneralPos.Tactic, GeneralOrientation.right)
                });
                break;

            case StatusEnum.Grade:
                UICardUpgradePanel.StartTutorial(UpdateStatus);
                break;

            case StatusEnum.end:
                EndTutorial();
                break;
        }

    }


    private void EndTutorial()
    {
        Debug.Log("TutorialMarket EndTutorial");
        _Status = StatusEnum.end;
        this.enabled = false;
    }

}
