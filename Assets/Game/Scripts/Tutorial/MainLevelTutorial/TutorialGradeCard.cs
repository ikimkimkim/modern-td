﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialGradeCard : MonoBehaviour
{

    private enum StatusEnum { General, InventorySaleCard, end }
    [SerializeField]
    private StatusEnum _Status;
    private static bool isEnd = false;

    void Start()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() != 5 || CardAndLevelSelectPanel.isLastLevelWin == false || isEnd)
        {
            EndTutorial();
            return;
        }
        isEnd = true;

        _Status = StatusEnum.General;
        UpdateTutorial();
    }



    private void UpdateStatus()
    {
        _Status++;
        UpdateTutorial();
    }


    private void UpdateTutorial()
    {
        switch (_Status)
        {
            case StatusEnum.General:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор! Вы можете модернизировать свои башни!",
                    GeneralPos.Salutes),
                    new GeneralState("Чем мощнее ваши башни, тем легче держать оборону!",
                    GeneralPos.Teaches)
                });
                break;

            case StatusEnum.InventorySaleCard:
                UICardsInventory.StartTutorial(UICardsInventory.TutorialType.gradeCard, UpdateStatus);
                break;

            case StatusEnum.end:
                EndTutorial();
                break;
        }

    }


    private void EndTutorial()
    {
        Debug.Log("TutorialGradeCard EndTutorial");
        _Status = StatusEnum.end;
        this.enabled = false;
    }


}
