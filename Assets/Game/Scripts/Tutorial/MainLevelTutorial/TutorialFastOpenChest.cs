﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialFastOpenChest : MonoBehaviour {


    private enum StatusEnum { General, OpenInfoChest, OpenChest, WaitTakePrize, General2, IfHaveHero, General3, OpenCanAndLevelTutor, General4, end }
    [SerializeField]
    private StatusEnum _Status;
    private static bool isEnd = false;

    void Start()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() != 3 || PlayerManager.ChestList.Exists(c => c.Type == Chest.TypeChest.Silver) == false
            || CardAndLevelSelectPanel.isLastLevelWin == false || isEnd)
        {
            EndTutorial();
            return;
        }
        isEnd = true;
        _Status = StatusEnum.General;
        UpdateTutorial();
    }



    private void UpdateStatus()
    {
        _Status++;
        UpdateTutorial();
    }


    private void UpdateTutorial()
    {
        switch (_Status)
        {
            case StatusEnum.General:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор, мы получили серебряный сундук.",
                    GeneralPos.Salutes),
                    new GeneralState("Он намного лучше деревянного, но и открывается дольше.",
                    GeneralPos.Tactic),
                    new GeneralState("Вы всегда можете ускорить открытие сундука, использовав кристаллы.",
                    GeneralPos.Teaches),
                    new GeneralState("Не бойтесь их тратить. Вы всегда сможете найти или приобрести новые.",
                    GeneralPos.Tactic)
                });
                break;
            case StatusEnum.OpenInfoChest:
                if (ChestManager.StartTutorial(Chest.TypeChest.Silver, UpdateStatus) == false)
                    EndTutorial();
                break;
            case StatusEnum.OpenChest:
                ChestPanel.StartTutorial(ChestPanel.TutorialType.FastOpen, UpdateStatus);
                break;
            case StatusEnum.WaitTakePrize:
                ChestOpenPanel.StartTutorial(UpdateStatus);
                break;
            case StatusEnum.General2:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Так гораздо быстрее. Чем быстрее вы будете получать подкрепление, тем эффективней будет ваш отряд по защите планеты!",
                    GeneralPos.Teaches)
                });
                break;
            case StatusEnum.IfHaveHero:
                if(PlayerManager.CardsList.Exists(i=>i.Type == _CardType.Hero))
                {
                    CardAndLevelSelectPanel.OnShow += UpdateStatus;
                }
                else
                {
                    EndTutorial();
                }
                break;
            case StatusEnum.General3:
                CardAndLevelSelectPanel.OnShow -= UpdateStatus;
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор! У нас пополнение. Возьмите с собой в бой бойца поддержки.",
                    GeneralPos.Teaches)
                });
                break;
            case StatusEnum.OpenCanAndLevelTutor:
                CardAndLevelSelectPanel.StartTutorial(_CardType.Hero, UpdateStatus);
                break;
            case StatusEnum.General4:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState( "Наши солдаты с современнейшей экипировкой и вооружением помогут вам дать отпор монстрам!",
                    GeneralPos.Teaches)
                });
                break;
            case StatusEnum.end:
                EndTutorial();
                break;
        }

    }


    private void EndTutorial()
    {
        Debug.Log("TutorualFirsOpenChest EndTutorial");
        _Status = StatusEnum.end;
        this.enabled = false;
    }


}
