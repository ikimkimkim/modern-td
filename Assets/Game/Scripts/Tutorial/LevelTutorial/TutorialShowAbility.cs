﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialShowAbility : MonoBehaviour {



    private enum StatusEnum { General, StartButton, end }
    [SerializeField]
    private StatusEnum _Status;

    void Start()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() != 2 || PlayerManager.getAbilityCards.Exists(i=>i.disableInBuildManager==false) == false)
        {
            EndTutorial();
            return;
        }
        _Status = StatusEnum.General;
        UpdateTutorial();
    }

    private void UpdateStatus()
    {
        _Status++;
        UpdateTutorial();
    }


    private void UpdateTutorial()
    {
        switch (_Status)
        {
            case StatusEnum.General:
                TutorialLevelUIArrowShow.HideStartButton();
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор, давайте я расскажу вам про <color=#FFA500>магию</color>.",
                    GeneralPos.Salutes, GeneralOrientation.right),
                    new GeneralState("Мы точно не знаем, как именно работает магия пришельцев. Но мы научились ее <color=#FFA500>использовать</color>.",
                    GeneralPos.Tactic, GeneralOrientation.right),
                    new GeneralState("Снизу вы видите список доступных вам <color=#FFA500>заклинаний</color>. Вы их можете <color=#00FF00>улучшать</color> в бою также, как башни.",
                    GeneralPos.Tactic, GeneralOrientation.right, TutorialLevelUIArrowShow.ShowArrowAbility),
                    new GeneralState("Сверху вы можете видеть количество доступной <color=#0000FF>маны</color>. Она необходима для использования заклинаний и <color=#00FF00>пополняется</color> со временем.",
                    GeneralPos.Tactic, GeneralOrientation.right, TutorialLevelUIArrowShow.ShowArrowMana),
                    new GeneralState("<color=#FFA500>Заклинания</color> - это мощное оружие, используйте их с умом!",
                    GeneralPos.Teaches, GeneralOrientation.right, TutorialLevelUIArrowShow.HideAllArrow)
                });
                break;

            case StatusEnum.StartButton:
                TutorialLevelUIArrowShow.ShowStartButton();
                TutorialLevelUIArrowShow.ShowArrowStartButton();
                UpdateStatus();
                break;


            case StatusEnum.end:
                EndTutorial();
                break;
        }

    }


    private void EndTutorial()
    {
        Debug.Log("TutorialShowAbility EndTutorial");
        _Status = StatusEnum.end;
        this.enabled = false;
    }
}
