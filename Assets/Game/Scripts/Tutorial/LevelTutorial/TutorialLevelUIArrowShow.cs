﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TutorialLevelUIArrowShow : MonoBehaviour {

    private static TutorialLevelUIArrowShow instance;

    [SerializeField] private GameObject _startButton, _arrowStartButton, _arrowFullSreen, _arrowMana,
        _arrowFirstAbility,_arrowFirstHero;

    private enum ItemShow { startButton, fullSreen, mana, firstAbility, firstHero }

    void Awake()
    {
        instance = this;
    }

    public static void ShowStartButton()
    {
        if (instance == null)
            return;
        instance._startButton.SetActive(true);
    }
    public static void HideStartButton()
    {
        if (instance == null)
            return;
        instance._startButton.SetActive(false);
    }

    public static void ShowArrowStartButton()
    {
        if (instance == null)
            return;
        instance.Show(ItemShow.startButton);
    }
    public static void ShowArrowFullSreenButton()
    {
        if (instance == null)
            return;
        instance.Show(ItemShow.fullSreen);
    }
    public static void ShowArrowMana()
    {
        if (instance == null)
            return;
        instance.Show(ItemShow.mana);
    }
    public static void ShowArrowAbility()
    {
        if (instance == null)
            return;
        instance.Show(ItemShow.firstAbility);
    }
    public static void ShowArrowHero()
    {
        if (instance == null)
            return;
        instance.Show(ItemShow.firstHero);
    }



    private void Show(ItemShow item)
    {
        _HideAllArrow();
        switch(item)
        {
            case ItemShow.startButton:
                if (_arrowStartButton.activeSelf == false)
                    _arrowStartButton.SetActive(true);
                break;
            case ItemShow.fullSreen:
                if (_arrowFullSreen.activeSelf == false)
                    _arrowFullSreen.SetActive(true);
                break;
            case ItemShow.mana:
                if (_arrowMana.activeSelf == false)
                    _arrowMana.SetActive(true);
                break;
            case ItemShow.firstAbility:
                if (_arrowFirstAbility.activeSelf == false)
                    _arrowFirstAbility.SetActive(true);
                break;
            case ItemShow.firstHero:
                if (_arrowFirstHero.activeSelf == false)
                    _arrowFirstHero.SetActive(true);
                break;
        }
    }

    public static void HideAllArrow()
    {
        if (instance == null)
            return;
        instance._HideAllArrow();
    }

    private void _HideAllArrow()
    {
        if (_arrowStartButton.activeSelf)
            _arrowStartButton.SetActive(false);
        if (_arrowFullSreen.activeSelf)
            _arrowFullSreen.SetActive(false);
        if (_arrowMana.activeSelf)
            _arrowMana.SetActive(false);
        if (_arrowFirstAbility.activeSelf)
            _arrowFirstAbility.SetActive(false);
        if (_arrowFirstHero.activeSelf)
            _arrowFirstHero.SetActive(false);
    }
}
