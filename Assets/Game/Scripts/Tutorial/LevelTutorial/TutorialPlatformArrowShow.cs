﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class TutorialPlatformArrowShow : MonoBehaviour
{
    private static TutorialPlatformArrowShow instance;

    [SerializeField] private List<PlatformTD> platforms = new List<PlatformTD>();
    [SerializeField] private GameObject Arrow;
    private MoveUItoGameObj arrowMover;

    public void Awake()
    {
        instance = this;
        CheckPlatfromArray();
        arrowMover = Arrow.GetComponent<MoveUItoGameObj>();
    }

    private void CheckPlatfromArray()
    {
        for (int i = 0; i < platforms.Count; i++)
        {
            if (platforms[i] == null)
            {
                platforms.RemoveAt(i);
                i--;
            }
        }
    }

    public static bool ShowArrowFreePlatform()
    {
        if (instance == null)
            return false;
        return instance._ShowArrowFreePlatform();
    }

    private bool _ShowArrowFreePlatform()
    {
        if(platforms.Count<=0)
            platforms.AddRange(BuildManager.GetPlatfoms);

        if(platforms.Count <= 0)
        {
            Debug.LogError("TutorialPlatformArrowShow not init platform!");
            return false;
        }
        _HideInfoPlatform();
        for (int i = 0; i < platforms.Count; i++)
        {
            _TileStatus t = BuildManager.CheckBuildPoint(Camera.main.WorldToScreenPoint(platforms[i].thisT.position));
            if (t == _TileStatus.Available)
            {
                Arrow.SetActive(true);
                if(arrowMover != null)
                    arrowMover.target = platforms[i].thisT;
                return true;
            }
        }
        return false;
    }




    public static void Hide()
    {
        if (instance != null)
            instance._HideInfoPlatform();
    }
    private void _HideInfoPlatform()
    {
        if(Arrow.activeSelf)
            Arrow.SetActive(false);
    }

}
