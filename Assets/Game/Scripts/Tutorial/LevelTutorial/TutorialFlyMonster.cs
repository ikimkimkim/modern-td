﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class TutorialFlyMonster : MonoBehaviour {


    private enum StatusEnum {  General, BuildTower, StartButton, WaitWave, General2, BuildTower2, end }
    [SerializeField]
    private StatusEnum _Status;

    void Start()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() != 1)
        {
            EndTutorial();
            return;
        }
        _Status = StatusEnum.General;
        UpdateTutorial();
    }

    private void UpdateStatus()
    {
        _Status++;
        UpdateTutorial();
    }


    private void UpdateTutorial()
    {
        switch (_Status)
        {
            case StatusEnum.General:
                TutorialLevelUIArrowShow.HideStartButton();
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор! Приближаются <color=#FF0000>массовые</color> противники.",
                    GeneralPos.Salutes),
                    new GeneralState("Рекомендую построить <color=#FFA500>Тяжелую</color> пушку.",
                    GeneralPos.Teaches)
                });
                break;

            case StatusEnum.BuildTower:
                if (ResourceManager.GetResourceArray()[0] < 20)
                    ResourceManager.GainResource(new List<float>() { 20, 0 });
                TutorialBuildTowerManager.StartTutorialBuildTower(UpdateStatus, 1);
                break;


            case StatusEnum.StartButton:
                TutorialLevelUIArrowShow.ShowStartButton();
                TutorialLevelUIArrowShow.ShowArrowStartButton();
                UpdateStatus();
                break;

            case StatusEnum.WaitWave:
                SpawnManager.onWaveClearedE += SpawnManager_onWaveSpawnedE;
                break;

            case StatusEnum.General2:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор! Пушки с <color=#FFA500>тяжелым</color> уроном не атакуют <color=#FF0000>воздушные</color> цели.",
                    GeneralPos.Teaches),
                    new GeneralState("Нам нужны пушки с <color=#FFA500>лёгким</color> уроном.",
                    GeneralPos.Tactic)
                });
                break;

            case StatusEnum.BuildTower2:
                if (ResourceManager.GetResourceArray()[0] < 24)
                    ResourceManager.GainResource(new List<float>() { 24, 0 });
                TutorialBuildTowerManager.StartTutorialBuildTower(UpdateStatus, 0);
                break;

            case StatusEnum.end:
                EndTutorial();
                break;
        }

    }

    private void SpawnManager_onWaveSpawnedE(int id)
    {
        SpawnManager.onWaveClearedE -= SpawnManager_onWaveSpawnedE;
        Invoke("UpdateStatus", 5);
    }

    private void EndTutorial()
    {
        Debug.Log("TutorialFlyMonster EndTutorial");
        _Status = StatusEnum.end;
        this.enabled = false;
    }
}
