﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class TutorialFirstStart : MonoBehaviour {

    private enum StatusEnum { history, General, BuildTower, General2, WaitWave, General3, BuildTower2, end }
    [SerializeField]
    private StatusEnum _Status;

    void Start()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() != 0)
        {
            EndTutorial();
            return;
        }
        _Status = StatusEnum.history;
        UpdateTutorial();
    }

    private void UpdateStatus()
    {
        _Status++;
        UpdateTutorial();
    }


    private void UpdateTutorial()
    {
        switch (_Status)
        {
            case StatusEnum.history:
                HistoryPanel.StartShow(UpdateStatus, 1);
                break;

            case StatusEnum.General:
                TutorialLevelUIArrowShow.HideStartButton();
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Добро пожаловать, <color=#00FF00>Командор!</color> Я - ваш помощник, меня называют Полковник.",
                    GeneralPos.Salutes),
                    new GeneralState("Для лучшего погружения в атмосферу, <color=#FFA500>советую</color> вам перейти в <color=#00FF00>полноэкранный режим</color>",
                    GeneralPos.Teaches, GeneralOrientation.left, TutorialLevelUIArrowShow.ShowArrowFullSreenButton),
                    new GeneralState("Разведка докладывает, что небольшой отряд <color=#FF0000>тварей</color> приближается к нашей территории.",
                    GeneralPos.Tactic, GeneralOrientation.left, TutorialLevelUIArrowShow.HideAllArrow),
                    new GeneralState("Давайте приготовимся к <color=#00FF00>обороне.</color>",
                    GeneralPos.Tactic),
                    new GeneralState("Для начала построим <color=#FFA500>пулемёт.</color>",
                    GeneralPos.Tactic)
                });
                break;

            case StatusEnum.BuildTower:
                if (ResourceManager.GetResourceArray()[0] < 20)
                    ResourceManager.GainResource(new List<float>() { 20, 0 });
                TutorialBuildTowerManager.StartTutorialBuildTower(UpdateStatus, 0);
                break;


            case StatusEnum.General2:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Мы готовы. <color=#00FF00>Начинаем!</color>",
                    GeneralPos.Salutes)
                });
                TutorialLevelUIArrowShow.ShowStartButton();
                TutorialLevelUIArrowShow.ShowArrowStartButton();
                break;

            case StatusEnum.WaitWave:
                SpawnManager.onWaveClearedE += SpawnManager_onWaveSpawnedE;
                break;


            case StatusEnum.General3:
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор! Мы заметили <color=#FF0000>массовые</color> скопления врага.",
                    GeneralPos.Teaches),
                    new GeneralState("Рекомендую построить <color=#FFA500>Тяжелую</color> пушку",
                    GeneralPos.Teaches)
                });
                break;

            case StatusEnum.BuildTower2:
                if (ResourceManager.GetResourceArray()[0] < 24)
                    ResourceManager.GainResource(new List<float>() { 24, 0 });
                TutorialBuildTowerManager.StartTutorialBuildTower(UpdateStatus, 1);
                break;

            case StatusEnum.end:
                EndTutorial();
                break;
        }

    }

    private void SpawnManager_onWaveSpawnedE(int id)
    {
        SpawnManager.onWaveClearedE -= SpawnManager_onWaveSpawnedE;
        Invoke("UpdateStatus",15);
    }

    private void EndTutorial()
    {
        Debug.Log("TutorialFirstStart EndTutorial");
        _Status = StatusEnum.end;
        this.enabled = false;
    }



}
