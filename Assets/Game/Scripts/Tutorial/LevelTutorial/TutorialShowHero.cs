﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialShowHero : MonoBehaviour {



    private enum StatusEnum { General, StartButton, end }
    [SerializeField]
    private StatusEnum _Status;

    void Start()
    {
        if (PlayerManager._instance.GetLevelsCountComplele() != 3 || PlayerManager.getHeroCards.Exists(i=>i.disableInBuildManager==false) == false)
        {
            EndTutorial();
            return;
        }
        _Status = StatusEnum.General;
        UpdateTutorial();
    }

    private void UpdateStatus()
    {
        _Status++;
        UpdateTutorial();
    }


    private void UpdateTutorial()
    {
        switch (_Status)
        {
            case StatusEnum.General:
                TutorialLevelUIArrowShow.HideStartButton();
                GeneralPanel.StartShow(UpdateStatus, new GeneralState[]{
                    new GeneralState("Командор, давайте я расскажу вам про наших бойцов!",
                    GeneralPos.Salutes, GeneralOrientation.left),
                    new GeneralState("Это лучшие воины планеты. Профессионалы своего дела!",
                    GeneralPos.Tactic, GeneralOrientation.left),
                    new GeneralState("Снизу вы видите список доступных вам бойцов. Бойцы улучшаются со временем. В зависимости от получаемого опыта.",
                    GeneralPos.Tactic, GeneralOrientation.left, TutorialLevelUIArrowShow.ShowArrowHero),
                    new GeneralState("Вы можете вызвать несколько бойцов одного типа на поле. Но каждый следующий вызов дороже.",
                    GeneralPos.Tactic, GeneralOrientation.left),
                    new GeneralState("Основное преимущество бойцов - мобильность. Перемещайте их по полю боя как вам вздумается.",
                    GeneralPos.Teaches, GeneralOrientation.left),
                    new GeneralState("Пора разнести черепушки этим уродам!",
                    GeneralPos.Salutes, GeneralOrientation.left)
                });
                break;

            case StatusEnum.StartButton:
                TutorialLevelUIArrowShow.HideAllArrow();
                TutorialLevelUIArrowShow.ShowStartButton();
                TutorialLevelUIArrowShow.ShowArrowStartButton();
                UpdateStatus();
                break;


            case StatusEnum.end:
                EndTutorial();
                break;
        }

    }


    private void EndTutorial()
    {
        Debug.Log("TutorialShowHero EndTutorial");
        _Status = StatusEnum.end;
        this.enabled = false;
    }
}
