﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using TDTK;

public class TutorialBuildTowerManager : MonoBehaviour
{

    private static TutorialBuildTowerManager instance; 

    public GameObject[] Blocks;
    public GameObject[] Cursor;
    public GameObject Tooltip;
    public Text textTooltip;

    public RectTransform thisTransform;

    private enum TutorialStatus { ShowFreePlatform, ShowTower, WaitBuild, end}
    private TutorialStatus _status;
    private Action _endTutorial;
    private int _typeTowerShow, _minLevelTowerShow;
    private string _textShow;
    private bool isTutorial = false;

    void Start()
    {
        instance = this;
        thisTransform = GetComponent<RectTransform>();
    }

    public static void StartTutorialBuildTower(Action endTutorial, int typeTower, string text = "Кликните чтобы построить башню", int minLevelTower = 0)
    {
        if (instance != null)
            instance._StartTutorialBuildTower(typeTower, text, minLevelTower, endTutorial);

    }
    private void _StartTutorialBuildTower(int typeTower, string text, int minLevelTower, Action endTutorial)
    {
        isTutorial = true;
        _typeTowerShow = typeTower;
        _minLevelTowerShow = minLevelTower;
        _textShow = text;
        _status = TutorialStatus.ShowFreePlatform;
        _endTutorial = endTutorial;
        _UpdateStatus();
    }

    private void UnitTower_onConstructionStartE(UnitTower tower)
    {
        _AddStatus();
    }
    private void _AddStatus()
    {
        if (isTutorial == false)
            return;
        _status++;
        _UpdateStatus();
    }
    private void _RemoveStatus()
    {
        if (isTutorial == false)
            return;
        _status--;
        _UpdateStatus();
    }

    private void _UpdateStatus()
    {
        switch(_status)
        {
            case TutorialStatus.ShowFreePlatform:
                if(TutorialPlatformArrowShow.ShowArrowFreePlatform() == false)
                {
                    _EndTutorial();
                    return;
                }
                _Hide();
                UIBuildButton.ShowEvent += _AddStatus;
                UIBuildButton.HideEvent -= _RemoveStatus;
                UnitTower.onConstructionStartE -= UnitTower_onConstructionStartE;
                break;

            case TutorialStatus.ShowTower:
                TutorialPlatformArrowShow.Hide();
                UIBuildButton.ShowEvent -= _AddStatus;
                UIBuildButton.HideEvent += _RemoveStatus;
                UnitTower.onConstructionStartE += UnitTower_onConstructionStartE;

                if(_ShowTower(_typeTowerShow, _textShow, _minLevelTowerShow)<=0)
                {
                    Debug.LogError("Not find tiwer for type "+_typeTowerShow);
                    _AddStatus();
                    _AddStatus();
                }
                break;

            case TutorialStatus.WaitBuild:
                _Hide();
                UIBuildButton.HideEvent -= _RemoveStatus;
                UnitTower.onConstructionStartE -= UnitTower_onConstructionStartE;
                UnitTower.onConstructionCompleteE+= UnitTower_onConstructionStartE;
                break;

            case TutorialStatus.end:
                UnitTower.onConstructionCompleteE -= UnitTower_onConstructionStartE;
                _EndTutorial();
                break;
        }
    }


    private void _EndTutorial()
    {
        isTutorial = false;
        TimeScaleManager.RefreshTimeScale();
        if (_endTutorial != null)
            _endTutorial();
    }



    public static void SetPosition(Vector3 position)
    {
        if (instance != null)
            instance._SetPosition(position);
    }

    private void _SetPosition(Vector3 position)
    {
        thisTransform.position = position;
    }


    /*public static int ShowTower(int TypeTower, string text, int minLevel = 0)
    {
        if (instance != null)
            return instance._ShowTower(TypeTower, text, minLevel);
        else
            return 0;
    }*/

    public int _ShowTower(int TypeTower,string text,int minLevel)
    {
        int indexSelect = 0;
        for (int i = 0; i < Blocks.Length; i++)
            Blocks[i].SetActive(true);
        Tooltip.SetActive(true);
        textTooltip.text = text;
        int index = 0;
        List<Card> Towers = PlayerManager.getTowerCards;
        for (int i = 0; i < Towers.Count; i++)
        {
            if(Towers[i].disableInBuildManager==false)
            {
                if(Towers[i].damageType == TypeTower && Towers[i].LevelCard>=minLevel)
                {
                    Blocks[index].SetActive(false);
                    Cursor[index].SetActive(true);
                    indexSelect++;
                }
                index++;
            }
        }
        return indexSelect;
    }


   /* public static void Hide()
    {
        if (instance != null)
            instance._Hide();
    }*/

    public void _Hide()
    {
        for (int i = 0; i < Blocks.Length; i++)
        {
            if(Blocks[i].activeSelf)
                Blocks[i].SetActive(false);
        }
        for (int i = 0; i < Cursor.Length; i++)
        {
            if(Cursor[i].activeSelf)
                Cursor[i].SetActive(false);
        }
        if(Tooltip.activeSelf)
            Tooltip.SetActive(false);
    }
    


    private void Update()
    {
        if (isTutorial == false)
            return;

        if (_status == TutorialStatus.WaitBuild)
            TimeScaleManager.RefreshTimeScale();
        else
            TimeScaleManager.SetTimeScale(0);
    }
}
