﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;



public enum GeneralOrientation { left, right }
public enum GeneralPos { Salutes, Teaches, Tactic }

public struct GeneralState
{
    public string text;
    public GeneralPos pos;
    public GeneralOrientation orientation;
    public bool showFon;
    public Action onAction;

    public GeneralState(string text, GeneralPos pos, GeneralOrientation orientation = GeneralOrientation.left, Action action = null, bool fon = true)
    {
        this.text = text;
        this.pos = pos;
        this.orientation = orientation;
        this.showFon = fon;
        this.onAction = action;
    }
}

public class GeneralPanel : MonoBehaviour {

    private Action hide;


    public static GeneralPanel instance { get; private set; }
    //public static Action<int> ChangeStatus;

    [SerializeField] private GameObject Fon;
    [Header("Left")]
    [SerializeField] private GameObject MainObjL;
    [SerializeField] private Text textGeneralL;
    [SerializeField] private GameObject objTextGeneralL, GeneralSalutesL, GeneralTeachesL, GeneralTacticL;
    [Header("Right")]
    [SerializeField] private GameObject MainObjR;
    [SerializeField] private Text textGeneralR;
    [SerializeField] private GameObject objTextGeneralR, GeneralSalutesR, GeneralTeachesR, GeneralTacticR;


    public void Awake()
    {
        instance = this;
        WaitMouseClick = false;
    }

    public static void StartShow(Action endAction, GeneralState[] states)
    {
        if (instance == null)
        {
            Debug.LogError("Start shot general, instance is null");
            endAction();
        }
        else
        {
            instance._StartShow(endAction, states);
        }
    }

    private void _StartShow(Action endAction, GeneralState[] states)
    {
        hide = endAction;
        nowStatus = -1;
        _generalStates = states;
        MainObjL.SetActive(true);
        MainObjR.SetActive(true);
        NextStatus();
    }


    [SerializeField] private int nowStatus;
    private GeneralState[] _generalStates;

    private bool WaitMouseClick;


    private void NextStatus()
    {
        WaitMouseClick = false;
        nowStatus++;

       // if (ChangeStatus != null)
       //     ChangeStatus(nowStatus);

        if (nowStatus >= _generalStates.Length)
        {
            Hide();
            return;
        }


        Fon.SetActive(_generalStates[nowStatus].showFon);

        switch (_generalStates[nowStatus].orientation)
        {
            case GeneralOrientation.left:
                objTextGeneralL.SetActive(true);
                textGeneralL.text = _generalStates[nowStatus].text;

                GeneralSalutesL.SetActive(_generalStates[nowStatus].pos == GeneralPos.Salutes);
                GeneralTeachesL.SetActive(_generalStates[nowStatus].pos == GeneralPos.Teaches);
                GeneralTacticL.SetActive(_generalStates[nowStatus].pos == GeneralPos.Tactic);

                objTextGeneralR.SetActive(false);
                GeneralSalutesR.SetActive(false);
                GeneralTeachesR.SetActive(false);
                GeneralTacticR.SetActive(false);

                break;
            case GeneralOrientation.right:
                objTextGeneralR.SetActive(true);
                textGeneralR.text = _generalStates[nowStatus].text;

                GeneralSalutesR.SetActive(_generalStates[nowStatus].pos == GeneralPos.Salutes);
                GeneralTeachesR.SetActive(_generalStates[nowStatus].pos == GeneralPos.Teaches);
                GeneralTacticR.SetActive(_generalStates[nowStatus].pos == GeneralPos.Tactic);

                objTextGeneralL.SetActive(false);
                GeneralSalutesL.SetActive(false);
                GeneralTeachesL.SetActive(false);
                GeneralTacticL.SetActive(false);

                break;
        }

        if (_generalStates[nowStatus].onAction != null)
            _generalStates[nowStatus].onAction();

        WaitMouseClick = true;

    }

    private void Update()
    {
        if(WaitMouseClick)
        {
            TimeScaleManager.SetTimeScale(0);
            if (Input.GetMouseButtonDown(0))
                NextStatus();
        }

    }

    public void Hide()
    {
        WaitMouseClick = false;
        Fon.SetActive(false);
        MainObjL.SetActive(false);
        MainObjR.SetActive(false);
        TimeScaleManager.RefreshTimeScale();
        if (hide != null)
            hide();
    }

}
