﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using TDTK;
using System.Collections.Generic;

public class HistoryPanel : MonoBehaviour {

    public static event Action OnStartShow, OnEndShow;
    public static HistoryPanel instance { get; private set; }
    public GameObject Fon;
    public Image imageHistory;

    public Dictionary<int, string> urlsImageHistory;
    public bool[] waitLoads;
    public Sprite[] spritesHistory;
    private Action _aciontEnd;

    private bool musicBuff;
    private List<int[]> listPack;
    private void Awake()
    {
        instance = this;

        urlsImageHistory = new Dictionary<int, string>();
        listPack = new List<int[]>();

        for (int i = 1; i <= 37; i++)
        {
            urlsImageHistory.Add(i, string.Format("History/history_{0}.png", i));
        }

        urlsImageHistory.Add(40, "History/history_40.png");
        urlsImageHistory.Add(41, "History/history_41.png");
        urlsImageHistory.Add(42, "History/history_42.png");

        for (int i = 1; i <= 9; i++)
        {
            urlsImageHistory.Add(42+i, string.Format("History/history_end2_{0}.png", i));
        }

        for (int i = 1; i <= 6; i++)
        {
            urlsImageHistory.Add(51 + i, string.Format("History/history_end3_{0}.png", i));
        }



        listPack.Add(new int[] { 1, 2, 3 });
        listPack.Add(new int[] { 4, 5, 6});
        listPack.Add(new int[] { 7, 8, 9 });
        listPack.Add(new int[] { 10, 11, 12 });

        listPack.Add(new int[] { 13, 14, 15 });
        listPack.Add(new int[] { 16, 17, 18 });
        listPack.Add(new int[] { 19, 20, 21 });
        listPack.Add(new int[] { 22, 23, 24 });

        listPack.Add(new int[] { 25, 26, 27 });
        listPack.Add(new int[] { 28, 29, 30 });
        listPack.Add(new int[] { 31, 32, 33 });
        listPack.Add(new int[] { 34, 35, 36 });

        listPack.Add(new int[] { 37, 40 });
        listPack.Add(new int[] { 41, 42 });
        listPack.Add(new int[] { 43,44,45, 46,47,48, 49,50,51});
        listPack.Add(new int[] { 52,53,54, 55,56,57 });
    }



    private void Start()
    {
        int count = PlayerManager._instance.GetLevelsCountComplele();
        int id = GameControl.GetLevelID();
        MyLog.Log("History lvl count:" + count + "  id->" + id);
        if (count == 6 && id == 7)
        {
            WaitLoad(2);
        }
        else
        if (count == 10 && id == 11)
        {
            WaitLoad(3);
        }
        else
        if (count == 11 && id == 12 ||
            count == 23 && id == 212 ||
            count == 35 && id == 312 || 
            count == 47 && id == 412 ||
            count == 59 && id == 512 ||
            count == 71 && id == 612 ||
            count == 83 && id == 712 ||
            count == 95 && id == 812 ||
            count == 107 && id == 912)
        {
            WaitLoad(8);
        }
        else
        if (count == 21 && id == 210)
        {
            WaitLoad(4);
        }
        else
        if (count == 32 && id == 309)
        {
            WaitLoad(5);
        }
        else
        if (count == 42 && id == 407)
        {
            WaitLoad(6);
        }
        else
        if (count == 51 && id == 504)
        {
            WaitLoad(7);
        }
        else
        if (count == 109 && id == 1001)
        {
            WaitLoad(9);
        }
        else
        if (count == 119 && id == 1012)
        {
            WaitLoad(10);
        }
        else
        if (count == 125 && id == 1106)
        {
            WaitLoad(11);
        }
        else
        if (count == 131 && id == 1112)
        {
            WaitLoad(12);
        }
        else
        if (count == 132 && id == 1201)
        {
            WaitLoad(13);
        }
        else
        if (count == 135 && id == 1204)
        {
            WaitLoad(14);
        }
        else
        if (count == 140 && id == 1209)
        {
            WaitLoad(15);
        }
        else
        if (count == 143 && id == 1212)
        {
            WaitLoad(16);
        }
    }

    public static void StartShow(Action endAction, int PackID)
    {
        if (instance == null)
        {
            MyLog.LogError("Start show History, instance is null", MyLog.Type.build);
            endAction();
        }

        instance._aciontEnd = endAction;
        instance.WaitLoad(PackID);
    }

    private void WaitLoad(int indexPack)
    {
        WaitLoad(listPack[indexPack-1]);
    }

    private void WaitLoad(int[] idsPage)
    {

        musicBuff = PlayerManager._instance.isMusicOn();
        if (musicBuff)
            PlayerManager._instance.SetMusicOn(false);
        setImgaeOrger = idsPage;
        spritesHistory = new Sprite[idsPage.Length];
        waitLoads = new bool[idsPage.Length];

        UIWaitPanel.Show();
        for (int i = 0; i < idsPage.Length; i++)
        {
            if (musicBuff)
                Application.ExternalCall("playStoryMusic");
            waitLoads[i] = false;
            int id = i;
            StartCoroutine(TextureDB.instance.load(urlsImageHistory[idsPage[i]], value => { SetSprite(id, value); } ));
        }
    }

    private void SetSprite(int id, Sprite sprite)
    {
        spritesHistory[id] = sprite;
        waitLoads[id] = true;
        bool loadEnd = true;
        for (int i = 0; i < waitLoads.Length; i++)
        {
            if (waitLoads[i] == false)
            {
                loadEnd = false;
                break;
            }
        }
        if (loadEnd)
        {
            UIWaitPanel.Hide();
            Show();
        }
    }

    private bool WaitMouseClick;
    private int nowStatus;
    private int[] setImgaeOrger;
    private float WaitTime;
    private void Show()
    {
        nowStatus = -1;
        Anim = false;
        Fon.SetActive(true);
        NextStatus();
        if (OnStartShow != null)
            OnStartShow();
    }

    private void NextStatus()
    {
        WaitMouseClick = false;
        WaitTime = 6;
        nowStatus++;
        if (nowStatus >= setImgaeOrger.Length)
        {
            StartCoroutine(Hide());
            return;
        }

        imageHistory.sprite = spritesHistory[nowStatus/*setImgaeOrger[nowStatus] - 1*/];

        imageHistory.color = new Color(1, 1, 1, Anim ? 0.01f : 1f);

        WaitMouseClick = true;
    }


    private void Update()
    {
        if (WaitMouseClick)
        {
            if (Input.GetMouseButtonDown(0))
                NextStatus();
            if (WaitTime <= 0 && Anim==false)
            {
                Anim = true;
                colorChange = true;
            }
            WaitTime -= Time.deltaTime;
        }
        if(Anim)
        {
            colorChangeImage();
        }
    }

    public bool Anim, colorChange;
    private Color color;
    private void colorChangeImage()
    {
        color = imageHistory.color;

        if (colorChange)
            imageHistory.color = new Color(color.r, color.g, color.b, color.a - 2 * Time.deltaTime);
        else
            imageHistory.color = new Color(color.r, color.g, color.b, color.a + 2 * Time.deltaTime);

        if (color.a <= 0)
        {
            colorChange = false;
            NextStatus();
        }
        else if (color.a >= 1)
        {
            Anim = false;
        }
    }



    private IEnumerator Hide()
    {
        Fon.SetActive(false);
        Application.ExternalCall("stopBackgroundMusic");
        PlayerManager._instance.SetMusicOn(musicBuff);

        yield return null;

        if (_aciontEnd != null)
            _aciontEnd();
        _aciontEnd = null;

        if (OnEndShow != null)
            OnEndShow();

    }
    
}
