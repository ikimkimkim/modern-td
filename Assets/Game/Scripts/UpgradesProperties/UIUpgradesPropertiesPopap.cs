﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using TDTK;
using System.Linq;

public class UIUpgradesPropertiesPopap : MonoBehaviour {

    [Header("ButtonInfo")]
    public GameObject buttonObj;
    public GameObject buttonInfoPanel;
    public Text textButtonInfo;
    public Color colordef, color1, color2;
    private Coroutine coroutineButtinInfoColor;

    [Header("Setting")]

    public Text textStars, textRedStars;

    public static UIUpgradesPropertiesPopap instance { get; private set; }

    public GameObject NotSelectInfo, SelectInfo;
    public Text textNamePerk, textInfoPerk;
    public Image IconPerk, Fon;
    public Sprite fonBlue, fonGreen, fonRed;



    public Text CostStar, CostRedStar;
    public GameObject CostStarObj, CostRedStarObj;
    
    public Perk PerkSelect { get; private set; }

    public Button Buy, Cancel, Refresh;

    private int starActive, starRedActive;
    private int countStar, countStarRed;

    public List<Perk> perkList { get; private set; }
    public static Perk getPerk(int id)
    {
        return instance.perkList.Find(i => i.ID == id);
    }

	void Awake ()
    {
        instance = this;
        perkList = PerkDB.Load();
        NotSelectPerk();
        
    }

    public void Start()
    {
        bool b = PlayerManager._instance.GetLevelsCountComplele() >= 3;
        buttonObj.SetActive(b);
        if(b)
            UpdateInfo();
    }

    public void OnEnable()
    {
        PlayerManager.InitializationСompleted += UpdateInfo;
    }

    public void OnDisable()
    {
        PlayerManager.InitializationСompleted -= UpdateInfo;
    }

    public void UpdateInfo()
    {
        countStar = 0;
        countStarRed = 0;
        int count;
        for (int i = 0; i < PlayerManager._instance.GetLevelsCountComplele(); i++)
        {
            count = PlayerManager._instance._playerData.Levels[i].stars;
            if(count>=3)
            {
                countStar += 3;
                countStarRed += count - 3;
            }
            else
            {
                countStar += count;
            }
        }

        starActive = countStar;
        starRedActive = countStarRed;
        Perk[] active = perkList.Where(i => PlayerManager._instance.isUpgradeBought(i.ID)).ToArray();
        foreach (Perk p in active)
        {
            if(p.cost.Count>1)
            {
                starActive -= (int)p.cost[0];
                starRedActive -= (int)p.cost[1];
            }
            else if(p.cost.Count>0)
            {
                starActive -= (int) p.cost[0];
            }
        }

        textStars.text = starActive + "/" + countStar;
        textRedStars.text = starRedActive + "/" + countStarRed;

        UpdateButtonInfo();
    }

    

    public static void ShowSelectPerk(int id)
    {
        instance._ShowSelectPerk(id);
    }

    public void _ShowSelectPerk(int id)
    {
        if(id<0)
        {
            NotSelectPerk();
        }
        else
        {
            SelectPerk(getPerk(id));
        }
    }

    private void SelectPerk(Perk perk)
    {
        Refresh.interactable = true;
        NotSelectInfo.SetActive(false);
        SelectInfo.SetActive(true);
        textNamePerk.text = perk.name;
        textInfoPerk.text = perk.desp;

        IconPerk.sprite = perk.icon;
        
        ShowCost(perk);

        if (PlayerManager._instance.isUpgradeBought(perk.ID))
        {
            Buy.gameObject.SetActive(false);
            //Buy.interactable = false;
            //Cancel.interactable = true;
            Cancel.gameObject.SetActive(true);
            Cancel.interactable = true;

            _PerkType type = perk.type;
            int g = perk.Group;
            int p = perk.PropertiesPriority + 1;
            Perk BeforePerk = perkList.FindAll(i => i.type == type).FindAll(i => i.Group == g).Find(i => i.PropertiesPriority == p);
            if (BeforePerk != null && PlayerManager._instance.isUpgradeBought(BeforePerk.ID))
            {
                //Cancel.interactable = false;
                Cancel.gameObject.SetActive(false);
                Buy.gameObject.SetActive(true);
                Buy.interactable = false;
            }

        }
        else
        {
            Buy.gameObject.SetActive(true);
            Buy.interactable = false;
            if (perk.PropertiesPriority <= 1)
            {
                Buy.interactable = true;
                //Refresh.interactable = false;
            }
            else
            {
                _PerkType type = perk.type;
                int g = perk.Group;
                int p = perk.PropertiesPriority - 1;
                Perk BeforePerk = perkList.FindAll(i => i.type == type).FindAll(i => i.Group == g).Find(i => i.PropertiesPriority == p);
                if (BeforePerk != null && PlayerManager._instance.isUpgradeBought(BeforePerk.ID))
                {
                    Buy.interactable = true;
                }
            }
            //Cancel.interactable = false;
            Cancel.gameObject.SetActive(false);
        }

        if (perk.PropertiesPriority >= 5)
        {
            Fon.sprite = fonRed;
        }
        else if (perk.PropertiesPriority >= 4)
        {
            Fon.sprite = fonGreen;
        }
        else
        {
            Fon.sprite = fonBlue;
        }

        PerkSelect = perk;
    }

    private void ShowCost(Perk perk)
    {
        if (perk.cost.Count > 0 && perk.cost[0] > 0)
        {
            CostStar.text = perk.cost[0].ToString();
            CostStarObj.SetActive(true);
        }
        else
        {
            CostStarObj.SetActive(false);
        }

        if (perk.cost.Count > 1 && perk.cost[1] > 0)
        {
            CostRedStar.text = perk.cost[1].ToString();
            CostRedStarObj.SetActive(true);
        }
        else
        {
            CostRedStarObj.SetActive(false);
        }
    }

    private void NotSelectPerk()
    {
        NotSelectInfo.SetActive(true);
        SelectInfo.SetActive(false);
        Buy.interactable = false;
        Cancel.gameObject.SetActive(false);
        PerkSelect = null;
    }


    public void onBuy()
    {
        if(PerkSelect.cost[0]<=starActive)
        {
            if(PerkSelect.cost.Count>1)
            {
                if(PerkSelect.cost[1] <= starRedActive)
                {
                    BuyUpgrade();
                }
                else
                {
                    RedBlinking.StartBlinking(textRedStars.gameObject);
                }
            }
            else
            {
                BuyUpgrade();
            }
        }
        else
        {
            RedBlinking.StartBlinking(textStars.gameObject);
        }
        
    }

    private void BuyUpgrade()
    {
        Buy.interactable = false;
        PlayerManager.DM.BuyPerk(PerkSelect.ID, Response);
        
    }

    public void onCancel()
    {
        if (PerkSelect !=null)
        {
            Cancel.interactable = false;
            PlayerManager.DM.CancelPerk(new int[] { PerkSelect.ID }, Response);  
        }
    }

    public void onRefresh()
    {
        if (PerkSelect != null)
        {
            Refresh.interactable = false;
            List<Perk> listP = perkList.FindAll(i => i.type == PerkSelect.type);
            int[] ids = new int[listP.Count];            
            for (int i = 0; i < listP.Count; i++)
            {
                ids[i] = listP[i].ID;
            }
            PlayerManager.DM.CancelPerk(ids, Response);
            //PlayerManager.DM.RefrechPerk(Response);
        }
    }

    public void Response(ResponseData rp)
    {
        if(rp.ok==0)
        {
            MyLog.LogError("Error:" + rp.error.code + " -> " + rp.error.text, MyLog.Type.build);
        }

        PlayerManager.SetPlayerData(rp.user);
    }


    public void UpdateButtonInfo()
    {
        int count = starActive + starRedActive;
        buttonInfoPanel.SetActive(count > 0);
        textButtonInfo.text = count.ToString();
        if (count > 5)
            StartChangeColor();
        else
            StopChangeColor();
    }


    private void StartChangeColor()
    {
        if(coroutineButtinInfoColor==null)
            coroutineButtinInfoColor = StartCoroutine(ChangeColor());
    }

    private void StopChangeColor()
    {
        if(coroutineButtinInfoColor!=null)
            StopCoroutine(coroutineButtinInfoColor);
        textButtonInfo.color = colordef;
    }

    private IEnumerator ChangeColor()
    {
        while(true)
        {
            textButtonInfo.color = color1;
            yield return new WaitForSeconds(1);
            textButtonInfo.color = color2;
            yield return new WaitForSeconds(1);
        }
    }



}
