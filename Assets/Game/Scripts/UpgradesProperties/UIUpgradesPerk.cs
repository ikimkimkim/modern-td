﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TDTK;
using UnityEngine.EventSystems;

public class UIUpgradesPerk : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public Image IconPerk;
    public int ID;

    public Sprite fonBlue, fonRed, fonGreen;

    public Sprite fonBlueActive, fonRedActive, fonGreenActive;

    private Toggle toggle;
    private Image fon;
    private Color colorIconDisable, colorIconEnable, colorIconBlock;

    private Perk perk;

    private bool canSelect;

    private bool bInit = false;
    void Start()
    {
        if (bInit == true)
            return;
        bInit = true;
        canSelect = false;

        toggle = GetComponent<Toggle>();
        fon = GetComponent<Image>();
        colorIconBlock = new Color(1, 1, 1, 0.2f);
        colorIconDisable = new Color(1, 1, 1, 1f);
        colorIconEnable = new Color(1, 1, 1, 1f);
        if(ID<0)
        {
            toggle.interactable = false;
            IconPerk.color = new Color(1, 1, 1, 0f);
            return;
        }
        perk = UIUpgradesPropertiesPopap.getPerk(ID);
        IconPerk.sprite = perk.icon;
    }

    void OnEnable()
    {
        if (bInit == false)
            Start();
        UpdateInfo();
        PlayerManager.InitializationСompleted += UpdateInfo;	    
	}
	
    public void UpdateInfo()
    {
        if(ID<0)
        {
            toggle.interactable = false;
            IconPerk.color = new Color(1, 1, 1, 0f);
            return;
        }

        
        

        if (PlayerManager._instance.isUpgradeBought(ID))
        {
            if(toggle.isOn)
                SelectPerk(toggle.isOn);
            toggle.interactable = true;
            IconPerk.color = colorIconEnable;
            fon.color = colorIconEnable;
            if (perk.PropertiesPriority >= 5)
            {
                fon.sprite = fonRedActive;
            }
            else if (perk.PropertiesPriority >= 4)
            {
                fon.sprite = fonGreenActive;
            }
            else
            {
                fon.sprite = fonBlueActive;
            }
        }
        else
        {
            if (perk.PropertiesPriority > 1)
            {
                canSelect = PlayerManager._instance.isUpgradeBought(ID - 1);

                if (canSelect == false && toggle.isOn)
                {
                    toggle.isOn = false;
                    UIUpgradesPropertiesPopap.ShowSelectPerk(-1);
                }
                else if (canSelect && toggle.isOn)
                    SelectPerk(toggle.isOn);
            }
                
            else
            {
                canSelect = true;
                if(toggle.isOn)
                    SelectPerk(toggle.isOn);
            }

            toggle.interactable = canSelect;


            if (canSelect)
            {
                IconPerk.color = colorIconDisable;
                fon.color = colorIconDisable;
            }
            else
            {
                fon.color = new Color(0.6f, 0.6f, 0.6f, 1f);
                IconPerk.color = colorIconBlock;
            }
            if (perk.PropertiesPriority >= 5)
            {
                fon.sprite = fonRed;
            }
            else if (perk.PropertiesPriority >= 4)
            {
                fon.sprite = fonGreen;
            }
            else
            {
                fon.sprite = fonBlue;
            }
        }
    }

    public void SelectPerk(bool toggleValue)
    {
        if(toggleValue)
            UIUpgradesPropertiesPopap.ShowSelectPerk(ID);
        else
            UIUpgradesPropertiesPopap.ShowSelectPerk(-1);
    }

    private void OnDisable()
    {
        PlayerManager.InitializationСompleted -= UpdateInfo;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(ID<0)
            StayInSameWorldPos.instance.WaitUpdate(transform.position+transform.up*14f);
        //else
        //  StayInSameWorldPos.setPos(transform.position + transform.up * 14f, perk.name);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        StayInSameWorldPos.Hide();
    }
}
