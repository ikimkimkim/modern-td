﻿using UnityEngine;
using System.Collections;
using System;

public class RLevelTower : IRequirements
{
    public int levelTower;

    public bool CanApply(Card card)
    {
        return (card.LevelTower >= levelTower);
    }

    public string GetTextInfo()
    {
        return "Нужен "+levelTower+" ур. башни";
    }

    public void SetProperties(object[] param)
    {
        levelTower = (int)Convert.ToInt32(param[0]);
    }
}
