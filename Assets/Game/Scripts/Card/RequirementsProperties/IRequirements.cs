﻿using UnityEngine;
using System.Collections;

public interface IRequirements
{
    void SetProperties(object[] param);

    bool CanApply(Card card);

    string GetTextInfo();
}
