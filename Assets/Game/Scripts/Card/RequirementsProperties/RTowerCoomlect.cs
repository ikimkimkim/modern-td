﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class RTowerCoomlect : IRequirements
{

    public int IDCoomlect;
    public int CountTowerCoomlect;

    public bool CanApply(Card card)
    {
        int i = 0;
        List<int> TypeBild = new List<int>();

        //Debug.Log("Count PropertiesBild" + PropertiesHelp.listPropertiesBild.Count);
        foreach (CardProperties cp in PropertiesHelp.listCoomplectPropertiesBild)
        {
            if (cp.ID == CardProperties.IDCoomplect)
            {
                if (((Coomplect)cp.properties).CoomplectID == IDCoomlect && TypeBild.Contains(cp.getTowerType())==false)
                {
                    TypeBild.Add(cp.getTowerType());
                    i++;
                }
            }
        }
        TypeBild.Clear();

        return (i >= CountTowerCoomlect);
    }

    public string GetTextInfo()
    {
        return "Нужно "+CountTowerCoomlect+ " башен из комплекта " + IDCoomlect;
    }

    public void SetProperties(object[] param)
    {
        IDCoomlect = (int)Convert.ToInt32(param[0]);
        CountTowerCoomlect = (int)Convert.ToInt32(param[1]);
    }

}
