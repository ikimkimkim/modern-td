﻿using UnityEngine;
using System.Collections;

public class RTowerUpgradeLevel : IRequirements
{
    public int LevelActive;
    public bool isActive;

    public bool CanApply(Card card)
    {
       // Debug.Log("-> RTUL -> active:" + isActive);
        return card.LevelTower >= LevelActive && isActive;
    }

    public string GetTextInfo()
    {
        return string.Format("Выбирается на {0} уровне башни.", LevelActive);
    }

    public void SetProperties(object[] param)
    {
       // Debug.Log("-> RTUL -> SetProperties");
        LevelActive = (int)param[0];
        isActive = false;
    }
     
}
