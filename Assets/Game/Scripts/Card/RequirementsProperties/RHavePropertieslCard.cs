﻿using UnityEngine;
using System.Collections;
using System;

public class RHavePropertieslCard : IRequirements
{
    public int IDProper;

    public bool CanApply(Card card)
    {
        return (card.Properties.Find(i=>i.ID== IDProper)!=null);
    }

    public string GetTextInfo()
    {
        return "Нужно свойство:"+IDProper;
    }

    public void SetProperties(object[] param)
    {
        IDProper = (int)Convert.ToDouble(param[0]);
    }
}
