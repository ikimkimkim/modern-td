﻿using UnityEngine;
using System.Collections;
using System;

public class RLevelCard : IRequirements
{
    public int LevelCard;

    public bool CanApply(Card card)
    {
        return (card.LevelCard >= LevelCard);
    }

    public string GetTextInfo()
    {
        return "Нужен " + LevelCard + " ур. карты";
    }

    public void SetProperties(object[] param)
    {
        LevelCard = (int)Convert.ToDouble(param[0]);
    }
}
