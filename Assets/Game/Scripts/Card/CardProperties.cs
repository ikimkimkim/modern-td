﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;
using System;

public class PropertiesHelp
{
    //public static Action<bool> aBuldTower; // для обновление свойств после постройки башни
    public static List<CardProperties> listCoomplectPropertiesBild = new List<CardProperties>(); //Список свой в построеных башнях 

    //Обновление всех свойств на всех башнях после постройки новой башни
    public static void RefreshAllCoomplectProperties()
    {
        ActivePorioerties(listCoomplectPropertiesBild);
    }

    public static void ActivePorioerties(List<CardProperties> activeProperties)
    {
        List<CardProperties> newListPrioritet;
        for (int prioritet = 0; prioritet < 5; prioritet++)
        {
            newListPrioritet = activeProperties.FindAll(i => i.MyPrioritet == (CardProperties.Prioritet)prioritet);
            foreach (CardProperties propertie in newListPrioritet)
            {
                propertie.Refresh(true);
            }
        }
    }
}

public class CardProperties
{
    public enum TypeEnum { Card, Perk }
    public TypeEnum type;
    public enum Prioritet { UltraHigh, High, Middle, Low, Last }

    public const int IDCoomplect = -100;

    public Prioritet MyPrioritet = Prioritet.High;

    public int ID { get; private set; }
    public int Level { get; private set; }
    public List<RequirementsProperties> Requirements;

    public baseProperties properties;
    


    public Unit _unit { get; private set; }
    //public UnitTower _tower { get; private set; }
    public Ability _ability { get; private set; }
    private bool bApplay,bActive;
    private bool bMustBuild;

    public object[] SetParam { get; private set;}

    public string UrlIcon {
        get
        {
            if (checkInterfaceINameAndIcon())
                return (properties as INameAndIcon).getUrlIcon;
            else
            {
                MyLog.LogWarning("Properties " + ID + " not url icon!");
                return "";
            }
        }
    }

    public string getName {
        get
        {
            if (checkInterfaceINameAndIcon())
                return (properties as INameAndIcon).getName;
            else
            {
                MyLog.LogWarning("Properties "+ID+" not name!");
                return "";
            }
        }
    }

    private bool checkInterfaceINameAndIcon()
    {
        if (properties == null)
        {
            Debug.Log("Error, propreties is null");
            return false;
        }
        return properties is INameAndIcon;           
    }
    public string getInfoText()
    {
        return getInfoText(Level);
    }

    public string getInfoText(int level)
    {
        if (properties == null)
            return "Error, properties is null";

        if (properties is basePropertiesGrade)
            return (properties as basePropertiesGrade).GetTextInfo(level);
        else
            return properties.GetTextInfo();
    }

    public string getFullInfoText()
    {
        return getFullInfoText(Level);
    }

    public string getFullInfoText(int level)
    {
        if(properties==null)
        {
            return "Error, propreties in null";
        }
        if (string.IsNullOrEmpty(getName))
            return getInfoText(level);
        if(level>0)
            return string.Format("<color=#fba81d>{0} ({2})</color>\r\n{1}", getName, getInfoText(level),level);
        else
            return string.Format("<color=#fba81d>{0}</color>\r\n{1}", getName, getInfoText(level));


    }

    public float getRangeActive()
    {
        if(properties is IRangeActionProperties)
        {
            return (properties as IRangeActionProperties).getRangeAction;
        }
        return -1;
    }

    public int getTowerType()
    {
        return _unit.MyCard.IDTowerData;
    }

    public int getIdTower()
    {
        return _unit.prefabID;
    }

    public string getNameTower()
    {
        return _unit.unitName;
    }

    public CardProperties(int id, object[] Param, int level, RequirementsProperties[] requirements)
    {
        type = TypeEnum.Card;
        ID = id;
        Level = level;
        if (Requirements == null)
            Requirements = new List<RequirementsProperties>();
        else
            Requirements.Clear();
        Requirements.AddRange(requirements);
        bApplay = false;
        bActive = false;
        SetParam = Param;

        switch (id)
        {

            case 1://event
                properties = new Crit();
                MyPrioritet = Prioritet.High;
                break;
            case 2://event
                properties = new Slow();
                MyPrioritet = Prioritet.High;
                break;
            case 3:
                properties = new UpDmg();
                MyPrioritet = Prioritet.High;
                break;
            case 4:
                properties = new UpMinDmg();
                MyPrioritet = Prioritet.Middle;
                break;
            case 5:
                properties = new UpMaxDmg();
                MyPrioritet = Prioritet.Middle;
                break;
            case 6://event
                properties = new StunProp();//----
                MyPrioritet = Prioritet.High;
                break;
            case 7://event
                properties = new FireDot();
                MyPrioritet = Prioritet.Low;
                break;
            case 8://event
                properties = new PoisonDot();
                MyPrioritet = Prioritet.Low;
                break;
            case 10:
                properties = new UpRange();
                MyPrioritet = Prioritet.High;
                break;
            case 11:
                properties = new Cooldown();
                MyPrioritet = Prioritet.High;
                break;
            case 12:
                properties = new SecondaryAttack();
                MyPrioritet = Prioritet.High;
                break;
            case 13://event
                properties = new AOE();
                MyPrioritet = Prioritet.High;
                break;
            case 14:
                properties = new ResourceRegen();
                MyPrioritet = Prioritet.Low;
                break;
            case 15:
                properties = new ResourceUpMax();
                MyPrioritet = Prioritet.High;
                break;
            case 16:
                properties = new CostDrop();
                MyPrioritet = Prioritet.Last;
                break;
            case 17://event
                properties = new UpAOE();
                MyPrioritet = Prioritet.Middle;
                break;
            case 18: //event
                properties = new UpTimeAllMagicEffect();
                MyPrioritet = Prioritet.Low;
                break;
            case 19:
                properties = new ThrowingGrenades();
                MyPrioritet = Prioritet.High;
                break;
            case 20:
                properties = new InRage();
                MyPrioritet = Prioritet.Middle;
                break;
            case 21:
                properties = new CooldownProperties();
                MyPrioritet = Prioritet.Last;
                break;
            case 22:
                properties = new DurationProperties();
                MyPrioritet = Prioritet.Last;
                break;
            case 23:
                properties = new IgnoreArmor();
                MyPrioritet = Prioritet.Middle;
                break;
            case 24://event
                properties = new CritOneShot();//----
                MyPrioritet = Prioritet.Middle;
                break;
           /* case 25:
                properties = new StunAOE();
                MyPrioritet = Prioritet.Middle;
                break;*/
            case 26:
                properties = new Healing();
                MyPrioritet = Prioritet.High;
                break;
            case 27:
                properties = new HealingUpCount();
                MyPrioritet = Prioritet.Middle;
                break;
            case 28:
                properties = new HealingUpRange();
                MyPrioritet = Prioritet.Middle;
                break;
            case 29:
                properties = new HealingUpCooldown();
                MyPrioritet = Prioritet.Middle;
                break;
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
                ID = 30;
                properties = new CastAbility();
                MyPrioritet = Prioritet.High;
                break;
            case 35:
                properties = new ShieldBuff();
                MyPrioritet = Prioritet.Middle;
                break;
            case 36:
                properties = new UpHP();
                MyPrioritet = Prioritet.High;
                break;
            case 37:
                properties = new UpXP();
                MyPrioritet = Prioritet.High;
                break;
            case 38://event
                properties = new ShockProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 39://event
                properties = new SalfBuffCooldownProperties();//---
                MyPrioritet = Prioritet.Middle;
                break;
            case 40://event
                properties = new BleedingDot();
                MyPrioritet = Prioritet.Middle;
                break;
            case 41://event
                properties = new DisorientationProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 42://event
                properties = new CritDownAOE();
                MyPrioritet = Prioritet.Low;
                break;
            case 43://event
                properties = new AOEMagicTower();
                MyPrioritet = Prioritet.High;
                break;
            case 44:
                properties = new CastAbilityMeteoritChanceShot();
                MyPrioritet = Prioritet.Middle;
                break;
            case 45://event
                properties = new StunMagiChanceProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 46://event
                properties = new CritDownRange();
                MyPrioritet = Prioritet.Low;
                break;
            case 47:
                properties = new CastAbilitySlowChanceShot();
                MyPrioritet = Prioritet.Middle;
                break;
            case 48:
                properties = new CastAbilityPoisonChanceShot();
                MyPrioritet = Prioritet.Middle;
                break;
            case 49:
                properties = new CritConfuseOrStun();
                MyPrioritet = Prioritet.Middle;
                break;
            case 50://event
                properties = new AOEChance();
                MyPrioritet = Prioritet.High;
                break;
            case 51:
                properties = new SecondAOE();
                MyPrioritet = Prioritet.Middle;
                break;
            case 52://event
                properties = new StunChanceAndFirstTarget();
                MyPrioritet = Prioritet.Middle;
                break;
            case 53://event
                properties = new WeaknessProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 54:
                properties = new CooldownAndRangeUp();
                MyPrioritet = Prioritet.Middle;
                break;
            case 55:
                properties = new CooldownAndRangeUpStunUnity();
                MyPrioritet = Prioritet.Middle;
                break;
            case 56:
                properties = new CastAbilityLightningChanceShot();
                MyPrioritet = Prioritet.Middle;
                break;
            case 57:
                properties = new SalfBuffCooldownKillUnitStunOrDisorientationProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 58://event
                properties = new CritChanceUpBleedingTargetProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 59://event
                properties = new StunChanceDisorientation();
                MyPrioritet = Prioritet.Middle;
                break;
            case 60:
                properties = new SalfBuffCooldownKillUnitProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 61:
                properties = new UpDamageMagicTowerProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 62://event
                properties = new MagicShockProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 63://event
                properties = new CritChanceUpMagicStatusProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 64:
                properties = new CastAbilityLightningRandomTargetProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 65://event
                properties = new UpCritChanceProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 66:
                properties = new CooldownBleedingUnitProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 67:
                properties = new AOEChanceKillUnitProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 68:
                properties = new CooldownUpDmgDownProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 69:
                properties = new VulnerabilityProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 70:
                properties = new MalisonDebuffProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 71:
                properties = new IgnoreArmorDmgDownProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 72:
                properties = new CastSecondAbilityLightningProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 73:
                properties = new SalfBuffCooldownCountShotProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 74:
                properties = new UpCostDebuffProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 75:
                properties = new AddManaForDeadDebufProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 76:
                properties = new IcyShieldProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 77:
                properties = new FrostbiteProperties();
                MyPrioritet = Prioritet.High;
                break;
            case 78:
                properties = new UpChanceOfTowerFrostbiteProperties();
                MyPrioritet = Prioritet.Middle;
                break;

            case 79:
                properties = new SlowPoisonAbPropeties();//Яд п
                MyPrioritet = Prioritet.Middle;
                break;
            case 80:
                properties = new UpSlowPoisonAbProperties();//Яд т1
                MyPrioritet = Prioritet.Low;
                break;
            case 81:
                properties = new UpDmgAndBreakPoisonAbProperties();//Яд т1
                MyPrioritet = Prioritet.Low;
                break;
            case 82:
                properties = new MoveAndShootHeroProperties();//Пулеметчик п
                MyPrioritet = Prioritet.Middle;
                break;
            case 83:
                properties = new DmgDivisionAllSwordsmanPropecties();//Мечник п
                MyPrioritet = Prioritet.Middle;
                break;
            case 84:
                properties = new UpArrmorFromAttackPropreties();//Мечник t1
                MyPrioritet = Prioritet.Middle;
                break;
            case 85:
                properties = new UpInHealAndInShildProperties();//Мечник t1
                MyPrioritet = Prioritet.Middle;
                break;


            case 86:
                properties = new HealingUpCountV2();//Медик t2
                MyPrioritet = Prioritet.Middle;
                break;
            case 87:
                properties = new AyraRegenMedicShieldProperties();//Медик t2
                MyPrioritet = Prioritet.Middle;
                break;
            case 88:
                properties = new UpLessTargetDmgProperties();//Метеорит т2
                MyPrioritet = Prioritet.Last;
                break;
            case 89:
                properties = new UpMoreTargetDmgProperties();//Метеорит т2
                MyPrioritet = Prioritet.Last;
                break;
            case 90:
                properties = new SlowAllTowerApplyAbilityProperties();//Заморозка т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 91:
                properties = new SlowAllUnitInIcyShieldProperties();//Заморозка т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 92:
                properties = new UpRangeAndHeavyDmgAbPoisonProperties();//Яд т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 93:
                properties = new ThrowingIcyGrenades();//Пулеметчик т2
                MyPrioritet = Prioritet.Low;
                break;
            case 94:
                properties = new WallShieldHeroProperties();//Мечник т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 95:
                properties = new SwordsmanDisorientationProperties();//Мечник т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 96://event
                properties = new PoisonMagicShockProperties();//Яд т2
                MyPrioritet = Prioritet.Middle;
                break;


            case 97:
                properties = new OneMeteorAbProperties();//Метеорит т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 98:
                properties = new UpCountMeteorAbProperties();//Метеорит т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 99:
                properties = new IcyShockProperties();//Заморозка т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 100:
                properties = new UpgradeSlowInTimeAbPropreties();//Заморозка т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 101:
                properties = new UpPoisonDmgProperties();//Яд т3
                MyPrioritet = Prioritet.Low;
                break;
            case 102:
                properties = new DisorientationInEndPoisonAbilityProperties();//Яд т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 103:
                properties = new BleedingRiflemanProperties();//Пулеметчик т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 104:
                properties = new ThrowingFireGrenades();//Пулеметчик т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 105:
                properties = new UpCountTargetMeProperties();//Мечник т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 106:
                properties = new AyraRegenHPProperties();//Медик т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 107:
                properties = new HealingUpgrade();//Медик т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 108:
                properties = new StunChanceDisorientationSwordsman();//Мечник т3
                MyPrioritet = Prioritet.Middle;
                break;


            case 109:
                properties = new UpDmgBoss();//Метеорит т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 110:
                properties = new MineTimerAbProperties();//Метеорит т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 111:
                properties = new IcingProperties();//Заморозка т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 112:
                properties = new CastAbilityLightningChanceSlowAbProperties();//Заморозка т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 113:
                properties = new ExplosionDeadUnitAbProperties();//Яд т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 114:
                properties = new MadnessPropreties();//Яд т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 115:
                properties = new AOERiflemanPropreties();//Пулеметчик т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 116:
                properties = new ThrowingBeaconProperties();//Пулеметчик т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 117:
                properties = new UpCountWallShieldPropreties();//Мечник т4
                MyPrioritet = Prioritet.Low;
                break;
            case 118:
                properties = new AuraVulnerabilityProperties();//Мечник т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 119:
                properties = new InvulnerableShieldProperties();//Медик т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 120:
                properties = new ResurrectionHeroProperties();//Медик т4
                MyPrioritet = Prioritet.Middle;
                break;


            case 121:
                properties = new SingleHeroStrongerProperties();//снайпер п
                MyPrioritet = Prioritet.Middle;
                break;
            case 122:
                properties = new CastAbilityBaseLightningChanceShot();//снайпер т1
                MyPrioritet = Prioritet.Middle;
                break;
            case 123:
                properties = new SevenAttackUpDmgAndBaseRangeProperties();//снайпер т1
                MyPrioritet = Prioritet.Middle;
                break;
            case 124:
                properties = new SniperRageProperties();//Снайпер t3
                MyPrioritet = Prioritet.Middle;
                break;
            case 125:
                properties = new StudyTargetProperties();//снайпер т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 126:
                properties = new BaseLighningUpAOEProperties();//снайпер т2
                MyPrioritet = Prioritet.Low;
                break;
            case 127:
                properties = new BaceLighningCastTwiceAndUpDmgProperties();//снайпер т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 128:
                properties = new IgnoreArmorDmgDownSniperProperties();//Снайпер t4
                MyPrioritet = Prioritet.Low;
                break;
            case 129:
                properties = new UpDmgForBossAndRageAfterCritProperties();//снайпер т4
                MyPrioritet = Prioritet.Middle;
                break;


            case 130:
                properties = new UpAOEHeroProperties();//Гранатаметчик t1
                MyPrioritet = Prioritet.Middle;
                break;
            case 131:
                properties = new ChanceTwoAttackOneShotPropreties();//Гранатаметчик t1
                MyPrioritet = Prioritet.Middle;
                break;
            case 132:
                properties = new FireDotHeroProperties();//Гранатаметчик t2
                MyPrioritet = Prioritet.Middle;
                break;
            case 133:
                properties = new CritHeroProperties();//Гранатаметчик t2
                MyPrioritet = Prioritet.Middle;
                break;
            case 134:
                properties = new UpDmgLessTargetsProperties();//Гранатаметчик t3
                MyPrioritet = Prioritet.Middle;
                break;
            case 135:
                properties = new UpDmgLargerTargetsProperties();//Гранатаметчик t3
                MyPrioritet = Prioritet.Middle;
                break;
            case 136:
                properties = new ChanceShotSmallRoketProperties();//Гранатаметчик t4
                MyPrioritet = Prioritet.Middle;
                break;
            case 137:
                properties = new ChanceShotDelayedActionRoketProperties();//Гранатаметчик t4
                MyPrioritet = Prioritet.Middle;
                break;



            case 138:
                properties = new UpMaxManaAndManaForCreepDeadInRangeProperties();//Маг p
                MyPrioritet = Prioritet.Middle;
                break;
            case 139:
                properties = new CastAbilityMeteirutAndAOEProperties();//Маг t1
                MyPrioritet = Prioritet.Middle;
                break;
            case 140:
                properties = new CastAbilityPlayerProperties();//Маг t1
                MyPrioritet = Prioritet.UltraHigh;
                break;
            case 141:
                properties = new MageRageProperties();//Маг t2
                MyPrioritet = Prioritet.Middle;
                break;
            case 142:
                properties = new UpChanceCastAbPlayerDownCooldownAbProperties();//Маг t2
                MyPrioritet = Prioritet.High;
                break;
            case 143:
                properties = new MagicShockMageProperties();//Маг t3
                MyPrioritet = Prioritet.Middle;
                break;
            case 144:
                properties = new UpChanceCastAbPlayerDownCostAbProperties();//Маг t3
                MyPrioritet = Prioritet.Middle;
                break;
            case 145:
                properties = new UpDmgForCountManaProperties();//Маг t4
                MyPrioritet = Prioritet.Middle;
                break;
            case 146:
                properties = new UpChanceCastAbPlayerUpMaxAndRegenManaProperties();//Маг t4
                MyPrioritet = Prioritet.Low;
                break;


            case 147:
                properties = new BaseLightningStunChanceProperties();//снайпер т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 148:
                properties = new StunAfterCritProperties();//снайпер т3
                MyPrioritet = Prioritet.Middle;
                break;


            case 149:
                properties = new KillAndDownCooldownAbProperties();//взрыв p
                MyPrioritet = Prioritet.Middle;
                break;
            case 150:
                properties = new MoreHPMoreDamageProperties();//взрыв т1
                MyPrioritet = Prioritet.Middle;
                break;
            case 151:
                properties = new LessHPMoreDamageProperties();//взрыв т1
                MyPrioritet = Prioritet.Middle;
                break;
            case 152:
                properties = new UpDamageExplosionProperties();//взрыв т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 153:
                properties = new CastAbilityCentrExplosionProperties();//взрыв т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 154:
                properties = new ExplosionDisorientationProperties();//взрыв т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 155:
                properties = new ExplosionShockProperties();//взрыв т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 156:
                properties = new PoisonBloodKillUnitExplosionProperties();//взрыв т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 157:
                properties = new ExplosionDebaffProperties();//взрыв т4
                MyPrioritet = Prioritet.Middle;
                break;


            case 158:
                properties = new DebaffUpCooldownProperties();//масс заморозка т0
                MyPrioritet = Prioritet.Middle;
                break;
            case 159:
                properties = new UpDurationAbProperties();//масс заморозка т1
                MyPrioritet = Prioritet.Middle;
                break;
            case 160:
                properties = new MassSlowDisorientationProperties();//масс заморозка т1
                MyPrioritet = Prioritet.Middle;
                break;
            case 161:
                properties = new UpPowerSlowEffectProperties();//масс заморозка т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 162:
                properties = new MassSlowStunProperties();//масс заморозка т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 163:
                properties = new UpAllTowerDmgAbProperties();//масс заморозка т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 164:
                properties = new ChanceVulnerabilityMassSlowProperties();//масс заморозка т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 165:
                properties = new ShockMassShowAbProperties();//масс заморозка т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 166:
                properties = new MadnessMassSlowProperties();//масс заморозка т4
                MyPrioritet = Prioritet.Middle;
                break;


            case 167:
                properties = new ArrowAbChanceStunProperties();//Стрелы т0
                MyPrioritet = Prioritet.Middle;
                break;
            case 168:
                properties = new ArrowPoisonDebaffProperties();//Стрелы т1
                MyPrioritet = Prioritet.Middle;
                break;
            case 169:
                properties = new ArrowFireDebaffProperties();//Стрелы т1
                MyPrioritet = Prioritet.Middle;
                break;
            case 170:
                properties = new ArrowUpCostDebuffProperties();//Стрелы т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 171:
                properties = new ArrowAddManaForDeadDebufProperties();//Стрелы т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 172:
                properties = new ArrowUpDmgProperties();//Стрелы т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 173:
                properties = new ArrowIgnorArmorTypeDownDmgPropreties();//Стрелы т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 174:
                properties = new MarkLightDamageTowerDebaffProperties();//Стрелы т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 175:
                properties = new MarkDamageHeroDebufProperties();//Стрелы т4
                MyPrioritet = Prioritet.Middle;
                break;


            case 176:
                properties = new LightningStunProperties();//Молния т0
                MyPrioritet = Prioritet.High;
                break;
            case 177:
                properties = new LightningStunAndUpTimeProperties();//Молния т1
                MyPrioritet = Prioritet.Middle;
                break;
            case 178:
                properties = new LightningStunNextDisorientationProperties();//Молния т1
                MyPrioritet = Prioritet.Middle;
                break;
            case 179:
                properties = new LightningUpDmgWaveProperties();//Молния т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 180:
                properties = new LightningUpTimeStunWaveProperties();//Молния т2
                MyPrioritet = Prioritet.Middle;
                break;
            case 181:
                properties = new LightningShockProperties();//Молния т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 182:
                properties = new LightningCooldownProperties();//Молния т3
                MyPrioritet = Prioritet.Middle;
                break;
            case 183:
                properties = new ElectrifiedDebuffProperties();//Молния т4
                MyPrioritet = Prioritet.Middle;
                break;
            case 184:
                properties = new ElectrifiedAyraDebuffProperties();//Молния т4
                MyPrioritet = Prioritet.Middle;
                break;


            //Звезды - Tower
            case 185:
                properties = new UpDmgCurrentTypePropeties();
                MyPrioritet = Prioritet.UltraHigh;
                break;           
            case 186:
                properties = new ChanceIgnoreArmorProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 187:
                properties = new TowerAddLevelSettingProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 188:
                properties = new SelectedAnyProperties();
                MyPrioritet = Prioritet.Middle;
                break;

            //Звезды - Hero
            case 189:
                properties = new RegenHPProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 190:
                properties = new UpHpDmgSpeedHeroProperties();
                MyPrioritet = Prioritet.High;
                break;
            case 191:
                properties = new DownNextCostProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 192:
                properties = new RespawnHeroProperties();
                MyPrioritet = Prioritet.Middle;
                break;

            //Звезды - Ability
            case 193:
                properties = new DownCostAbilityProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 194:
                properties = new SlowAllUnitDebuffAbProperties();
                MyPrioritet = Prioritet.Middle;
                break;
            case 195:
                properties = new CastAnyAbilityProperties();
                MyPrioritet = Prioritet.Middle;
                break;




            //Легендарные
            case 1013:
                properties = new OneShot();//+
                MyPrioritet = Prioritet.High;
                break;
            case 1014:
                properties = new NewBaseRange();
                MyPrioritet = Prioritet.UltraHigh;
                break;
            case 1015:
                properties = new ChangeTargerMode();
                MyPrioritet = Prioritet.High;
                break;
            case 1016:
                properties = new UpCooldown();
                MyPrioritet = Prioritet.High;
                break;
            case 1017:
                properties = new SlowAura();
                MyPrioritet = Prioritet.High;
                break;
            case 1018:
                properties = new ContusionAura();
                MyPrioritet = Prioritet.High;
                break;

            //сэт
            case 2001:
                properties = new UpPoisonCountTik();//+
                MyPrioritet = Prioritet.Last;
                Requirements.Add(new RequirementsProperties(4, new object[] { 8 }));
                break;
            case 2002:
                properties = new UpFireDot();//+
                MyPrioritet = Prioritet.Last;
                Requirements.Add(new RequirementsProperties(4, new object[] { 7 }));
                break;
            case 2003:
                properties = new CritSlow();//+
                MyPrioritet = Prioritet.Middle;
                break;
            case 2004:
                properties = new UpStanPropSlow();
                MyPrioritet = Prioritet.Middle;
                Requirements.Add(new RequirementsProperties(4, new object[] { 6 }));
                break;
            case 2005:
                properties = new CritBoss();//+
                MyPrioritet = Prioritet.Middle;
                break;
            

            case IDCoomplect:
                properties = new Coomplect();
                MyPrioritet = Prioritet.Last;
                break;

            default:
                Debug.LogError("Card Properties not exist. id:" + id);
                return;
        }
        try
        {
            properties.SetProperties(Param);
        }
        catch(Exception ex)
        {
            Debug.LogError(string.Format("Error set param properties, id:{0} Ex:{1}\r\n{2}",ID,ex.Message,ex.StackTrace));
        }

        if(properties is basePropertiesGrade)
        {
            (properties as basePropertiesGrade).SetLevel(Level);
        }
        
    }

    public void SetLevel(Card mainCard)
    {
        Level = CraftLevelData.GetMyLevelGrade(mainCard, ID);
       // Debug.LogWarningFormat("Properties {0} set level {1}", ID, Level);
        if (properties is basePropertiesGrade)
        {
            (properties as basePropertiesGrade).SetLevel(Level);
        }
    }



    public void Apply(Unit unit)
    {
        if (bApplay)
            Debug.LogErrorFormat("properties id {0} apply already apply! obj: {1}", ID, unit.name);
        if (bActive)
            Debug.LogErrorFormat("properties id {0} apply already active! obj: {1}", ID, unit.name);
        //Debug.Log("CardProperties " + ID + " applay:" + unit.unitName+", card id "+ unit.MyCard.ID);
        _unit = unit;
        SetLevel(_unit.MyCard);
        bApplay = true;
    }

    public void Apply(Ability ab)
    {
        if (bApplay)
            Debug.LogErrorFormat("properties id {0} apply already apply! obj: {1}", ID, ab.name);
        if(bActive)
            Debug.LogErrorFormat("properties id {0} apply already active! obj: {1}", ID, ab.name);
        //Debug.Log("CardProperties " + ID + " applay:" + tower.unitName+", card id "+tower.MyCard.ID);
        _ability = ab;
        SetLevel(_ability.MyCard);
        bApplay = true;
    }

    public void Cancel()
    {
        if(bApplay)
        {
            OnDeactive();
            _unit = null;
            _ability = null;
            bApplay = false;
        }
    }

    public void Refresh(bool Active)
    {
        //Debug.Log("CardProperties -> " + ID + " refresh:" + _tower.unitName + ", card id " + _tower.MyCard.ID);

        if (properties==null)
        {
            Debug.LogError("Properties tower " + ID + " is null");
            return;
        }

        if (bApplay == false)
        {
            MyLog.LogError("Properties  " + ID + " is not Applay");
        }
        else
        {
            if(Active==false)
            {
                OnDeactive();
                return;
            }


            if (CanActive())
            {
                OnActive();
            }
            else
            {
                OnDeactive();
            }
        }
    }


    private void OnActive()
    {
        if (bActive == false)
        {
            if (_unit != null)
                properties.Apply(_unit);
            if (_ability != null)
                properties.Apply(_ability);
        }
        bActive = true;
    }

    private void OnDeactive()
    {
        if (bActive == true)
        {
            if (_unit != null)
                properties.Cancel(_unit);
            if (_ability != null)
                properties.Cancel(_ability);
        }
        bActive = false;
    }


    public bool CanActive()
    {
        if(bApplay==false)
        {
            Debug.LogError("Properties " + ID + " is not Applay");
            return false;
        }
        Card c=null;
        if (_unit != null)
            c = _unit.MyCard;
        if (_ability != null)
            c = _ability.MyCard;

        if (c == null)
            Debug.LogError("Properties " + ID + " CanActive: card is null");

        return CanActive(c) && CanApplyIsSampleTower();
    }

    public bool CanActive(Card card)
    {
        for (int i = 0; i < Requirements.Count; i++)
        {
            if (Requirements[i].CanActiveProperties(card) == false)
            {
                return false;
            }
        }
        return true;
    }

    public bool CanApplyIsSampleTower()
    {
        if(_unit!= null && _unit is UnitTower && (_unit as UnitTower).IsSampleTower)
        {
            return properties.ApplyIsSampleTower;
        }
        return true;            
    }


    public CardProperties Clone()
    {
        RequirementsProperties[] arrayRP = new RequirementsProperties[Requirements.Count];
        for (int i = 0; i < Requirements.Count; i++)
        {
            arrayRP[0] = Requirements[0].Clone();
        }

        CardProperties cp = new CardProperties(ID,SetParam, Level, arrayRP);
        if(ID == IDCoomplect)
        {
            Coomplect coom = (Coomplect)properties;
            Coomplect coomClone = (Coomplect)cp.properties;
            coomClone.CoomplectProperties = new List<CardProperties>(); // coom.CoomplectProperties;

            for (int i = 0; i < coom.CoomplectProperties.Count; i++)
            {
                coomClone.CoomplectProperties.Add( coom.CoomplectProperties[i].Clone());
            }
        }
        return cp;
    }

    
}
