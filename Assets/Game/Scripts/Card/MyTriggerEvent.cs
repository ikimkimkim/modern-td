﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class MyTriggerEvent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {

    public static Action OnShow; 

    public CardInfoUI linkCardUI;

    void Start()
    {
        OnShow += HideButton;
    }

    void OnDestroy()
    {
        OnShow -= HideButton;
    }

    void HideButton()
    {
        linkCardUI.HideButton();
    }


    public void OnPointerEnter(PointerEventData evd)
    {
        if (OnShow != null)
            OnShow();

        linkCardUI.OnPointerEnter();
        linkCardUI.ShowButton();

        
    }
    public void OnPointerExit(PointerEventData evd)
    {
        linkCardUI.OnPointerExit();
        linkCardUI.CorHideButton();
    }
}
