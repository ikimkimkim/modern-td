﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class ChanceShotDelayedActionRoketProperties : basePropertiesGrade, INameAndIcon
{
    protected float chance, dmg, dmgAOE, rangeAOE, delayTime;

    public virtual string getName { get { return "Богатый внутренний мир"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/ChanceShotDelayedActionRoketProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceLastE += LastProcessAttack;
        return true;
    }

    private void LastProcessAttack(AttackInstance attackInstance)
    {
        if(Random.Range(0f,1f)<chance)
        {
            attackInstance.damage = 0;
            attackInstance.isAOE = false;
            attackInstance.listEffects.Clear();
            attackInstance.listEffects.Add(new ExplosionUnitDebaffEffect(rangeAOE, _GradeParam, dmgAOE, delayTime));
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceLastE -= LastProcessAttack;
        return true;
    }

    public override bool Apply(Ability ability)
    {
        ability.ApplayEffectTargetE += Ability_ApplayEffectTargetE;
        return true;
    }

    private void Ability_ApplayEffectTargetE(Ability ability, AbilityEffect effect, Vector3 position, Unit target, int idCast)
    {
        if (Random.Range(0f, 1f) < chance)
        {
            target.EffectsManager.AddEffect(null, new ExplosionUnitDebaffEffect(rangeAOE, _GradeParam, dmgAOE, delayTime));
        }
    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectTargetE -= Ability_ApplayEffectTargetE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("С шансом в {0}% выпустить ракету замедленного действия. Ракета при контакте с врагом не взрывается а проникает в тело и взрывается изнутри через {1} с. Взрыв наносит {2}% урона от здоровья монстра, а всем окружающим наносится урон равный {3}% от здоровья.",
            Mathf.Round(chance*100f), delayTime, Mathf.Round(dmg * 100f), Mathf.Round(dmgAOE * 100f));
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0] / 100f;
        delayTime = (float)param[1];
        dmg = (float)param[2] / 100f;
        dmgAOE = (float)param[3] / 100f;
        rangeAOE = (float)param[4];
        _GradeParam = dmg;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("С шансом в {0}% выпустить ракету замедленного действия. Ракета при контакте с врагом не взрывается а проникает в тело и взрывается изнутри через {1} с. Взрыв наносит {2}(<color=#19c62e>+{4}</color>)% урона от здоровья монстра, а всем окружающим наносится урон равный {3}% от здоровья.",
                Mathf.Round(chance * 100f), delayTime,
                System.Math.Round(dmg * 100f, 2), Mathf.Round(dmgAOE * 100f), 
                System.Math.Round(dmg * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmg);
    }
}
