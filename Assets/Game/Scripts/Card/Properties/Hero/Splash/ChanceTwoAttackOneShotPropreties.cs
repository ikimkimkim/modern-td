﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class ChanceTwoAttackOneShotPropreties : basePropertiesGrade, INameAndIcon
{
    private float chance;
    private Unit rootUnit;

    public string getName { get { return "Добавка"; } }
    public string getUrlIcon { get { return "EffectIcon/ChanceTwoAttackOneShotPropreties.png"; } }

    public override bool Apply(Unit unit)
    {
        rootUnit = unit;
        unit.onShotTurretE += Unit_onShotTurretE;
        return true;
    }

    private void Unit_onShotTurretE(Unit target)
    {
        if (Random.Range(0f, 1f) < _GradeParam)
        {
            rootUnit.StartCoroutine(Shoot(target));
            
        }
    }

    private IEnumerator Shoot(Unit target)
    {
        yield return new WaitForSeconds(0.1f);

        Transform sp = rootUnit.shootPoints[0];
        Transform objT = ObjectPoolManager.Spawn(rootUnit.GetShootObjectT(), sp.position, sp.rotation);
        ShootObject shootObj = objT.GetComponent<ShootObject>();

        AttackInstance attInstance = new AttackInstance();
        attInstance.srcUnit = rootUnit;
        attInstance.tgtUnit = target;

        shootObj.Shoot(attInstance, sp);

        yield break;
    }

    public override bool Cancel(Unit unit)
    {
        rootUnit.onShotTurretE -= Unit_onShotTurretE;
        rootUnit = null;
        return true;
    }

    public override string GetTextInfo()
    {
       return string.Format("С {0}% шансом выстреливает две ракеты вместо одной.",Mathf.Round(chance*100f));
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0] / 100f;
        _GradeParam = chance;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("С {0}(<color=#19c62e>+{1}</color>)% шансом выстреливает две ракеты вместо одной.",
                Mathf.Round(chance * 100f),
                System.Math.Round(chance * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, chance);
    }
}
