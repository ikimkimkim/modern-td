﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class ChanceShotSmallRoketProperties : basePropertiesGrade, INameAndIcon
{
    private int countRoket = 8;
    private float chance, dmgRoket;

    private _TargetMode lastMod;
    private GameObject objShootObject;
    private Unit rootUnit;
    private PropertiesAttak roket;
    private float AOERoket=2;

    public string getName { get { return "Катюша"; } }

    public string getUrlIcon { get { return "EffectIcon/ChanceShotSmallRoketProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        rootUnit = unit;

        objShootObject = DataBase<GameObject>.Load(DataBase<GameObject>.NameBase.ShootObject)[1];

        lastMod = unit.targetMode;
        unit.targetMode = _TargetMode.Hybrid;
        unit.onShotTurretE += ShotTurret;

        roket = new PropertiesAttak();
        roket.myOutAttakInstanceInitE += AOE;
        roket.mask = 1 << LayerManager.LayerCreepF() | 1 << LayerManager.LayerCreep();
        roket.damageType = unit.damageType;

        return true;
    }

    private void AOE(AttackInstance attackInstance)
    {
        attackInstance.SetAOERadius(AOERoket);
    }

    private void ShotTurret(Unit target)
    {
        if(Random.Range(0f,1f)<chance)
        {
            rootUnit.StartCoroutine(Shoot(target));
        }
    }

    private IEnumerator Shoot(Unit target)
    {
        for (int i = 0; i < countRoket; i++)
        {
            yield return new WaitForSeconds(0.05f);
            Transform sp = rootUnit.shootPoints[0];
            GameObject obj = ObjectPoolManager.Spawn(objShootObject, sp.position, sp.rotation);
            ShootObject shootObj = obj.GetComponent<ShootObject>();

            roket.stats[0].damageMin = rootUnit.GetDamageMin() * _GradeParam;
            roket.stats[0].damageMax = rootUnit.GetDamageMax() * _GradeParam;

            AttackInstance attInstance = new AttackInstance();
            attInstance.srcPropretiesAttak = roket;
            attInstance.tgtUnit = target;

            shootObj.Shoot(attInstance, sp);
        }
        yield break;
    }

    public override bool Cancel(Unit unit)
    {
        unit.targetMode = lastMod;
        unit.onShotTurretE -= ShotTurret;
        rootUnit = null;
        roket.myOutAttakInstanceInitE -= AOE;
        roket = null;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("С шансом в {0}% выпускает {1} малых ракет, каждая наносит {2}% урона. Помимо этого появляется возможность стрелять по летающим монстрам.",
            Mathf.Round(chance*100f), countRoket, dmgRoket * 100f);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0] / 100f;
        dmgRoket = (float)param[1] / 100f;
        _GradeParam = dmgRoket;
        countRoket = System.Convert.ToInt32(param[2]);

    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("С шансом в {0}% выпускает {1} малых ракет, каждая наносит {2}(<color=#19c62e>+{3}</color>)% урона. Помимо этого появляется возможность стрелять по летающим монстрам.",
                Mathf.Round(chance * 100f), countRoket, dmgRoket * 100f,
                System.Math.Round(dmgRoket * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmgRoket);
    }
}
