﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpAOEHeroProperties : UpAOE, INameAndIcon
{
    public string getName
    {
        get
        {
            return "Бомбэрмэн";
        }
    }

    public string getUrlIcon
    {
        get
        {
            return "EffectIcon/UpAOEHeroProperties.png";
        }
    }

    public override string GetTextInfo()
    {
        return string.Format("Одна ракета с увеличены радиусом урона по площади на {0}%", AOERange);
    }
    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Одна ракета с увеличены радиусом урона по площади на {0}(<color=#19c62e>+{1}</color>)%", AOERange,
                System.Math.Round(AOERange * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, AOERange);
    }
}
