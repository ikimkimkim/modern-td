﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class UpDmgLessTargetsProperties : basePropertiesGrade, INameAndIcon
{
    private float upDmg;
    private float countLimit;

    public string getName { get { return "Минимализм"; } }

    public string getUrlIcon { get { return "EffectIcon/UpDmgLessTargetsProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceLastE += lastAttakProgress;
        return true;
    }

    private void lastAttakProgress(AttackInstance attackInstance)
    {
        if(attackInstance.isAOE)
            attackInstance.AOECounterE += AOECounter;
    }

    private void AOECounter(int countTargets, AttackInstance attackInstance)
    {
        attackInstance.AOECounterE -= AOECounter;
        if (countTargets < countLimit+1)
            attackInstance.damage *= upDmg;
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceLastE -= lastAttakProgress;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("По группам из {0}-х и менее юнитов увеличен урон на {1}%", 
            countLimit, Mathf.Round(upDmg * 100f));
    }

    public override void SetProperties(object[] param)
    {
        countLimit = (float)param[0];
        upDmg = (float)param[1]/100f;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("По группам из {0}-х и менее юнитов увеличен урон на {1}(<color=#19c62e>+{2}</color>)%", countLimit,
                System.Math.Round(upDmg * 100f, 2),
                System.Math.Round(upDmg * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, upDmg);
    }
}
