﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class SingleHeroStrongerProperties : baseProperties
{
    private float rangeSearchHero, upRangeProcent, chanceCrit, dmgCritM;

    UnitHero rootHero;
    Coroutine corSearchUpdate;
    bool bActive;

    public override bool Apply(Unit unit)
    {
        if (unit.IsHero == false)
            return false;

        bActive = false;
        rootHero = unit.GetUnitHero;
        corSearchUpdate = rootHero.StartCoroutine(SearchUpdate());
        rootHero.myOutAttakInstanceFirstE += RootHero_myOutAttakInstanceFirstE;

        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (corSearchUpdate != null)
            rootHero.StopCoroutine(corSearchUpdate);

        Deactive();
        rootHero.myOutAttakInstanceFirstE -= RootHero_myOutAttakInstanceFirstE;
        rootHero = null;

        return true;
    }

    IEnumerator SearchUpdate()
    {
        while(rootHero!=null && !rootHero.dead)
        {
            var herosInRange = UIHeroSpawn.getActiveHeroList.FindAll(i => i != rootHero && i.prefabID!= rootHero.prefabID && Vector3.Distance(rootHero.getPosition, i.getPosition) < rangeSearchHero);
            if(herosInRange.Count>0)
            {
                Deactive();
            }
            else
            {
                Active();
            }
            yield return new WaitForSeconds(0.2f);
        }

        yield break;
    }
    UpRangeAndChanceCritBuffEffect effect;
    private void Active()
    {
        if (bActive)
            return;
        effect = new UpRangeAndChanceCritBuffEffect(1f + upRangeProcent, chanceCrit, dmgCritM);
        rootHero.EffectsManager.AddEffect(rootHero, effect);

        //rootHero.stats[0].range *= 1f + upRangeProcent;

        bActive = true;
    }

    private void RootHero_myOutAttakInstanceFirstE(AttackInstance attackInstance)
    {
        if(Random.value<attackInstance.GetChanceCritical(bActive? chanceCrit:0))
        {
            attackInstance.SetMultiplierDmgCritical(dmgCritM);
        }
    }

    private void Deactive()
    {
        if (bActive == false || effect == null)
            return;

        effect.workСonditionEffect.Destroy();
        effect = null;

        bActive = false;
    }

    public override string GetTextInfo()
    {
        return string.Format("Если рядом со снайпером в {0} радиусе нет союзных юнитов, то он получает +{1}% к дальности атаки и возможность наносить критический урон.",
            rangeSearchHero, Mathf.Round(upRangeProcent*100));
    }

    public override void SetProperties(object[] param)
    {
        rangeSearchHero = (float)param[0];
        upRangeProcent = (float)param[1] / 100f;
        chanceCrit = (float)param[2] / 100f;
        dmgCritM = (float)param[3] / 100f;
    }
}
