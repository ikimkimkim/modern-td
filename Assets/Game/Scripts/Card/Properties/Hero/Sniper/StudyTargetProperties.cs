﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class StudyTargetProperties : basePropertiesGrade, INameAndIcon
{

    private IUnitAttacked lastTarget;
    private int counterShot;
    private float upChanceCrit;
    private float maxUpChance = .15f;

    public string getName { get { return "Изучение цели"; } }

    public string getUrlIcon { get { return "EffectIcon/StudyTargetProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceInitE += Unit_myOutAttakInstanceInitE;
        return true;
    }

    private void Unit_myOutAttakInstanceInitE(AttackInstance attackInstance)
    {
        if(lastTarget==attackInstance.tgtUnit)
        {
            counterShot++;
            attackInstance.AddChanceCritical += Mathf.Clamp(_GradeParam * counterShot, 0, maxUpChance);
        }
        else
        {
            counterShot = 0;
            lastTarget = attackInstance.tgtUnit;
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceInitE -= Unit_myOutAttakInstanceInitE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Каждый выстрел по одной и той же цели получает {0}% к шансу крита. Сбрасывается после смерти монстра. Сбрасывается если переключился на другую цель",
           Mathf.Round(upChanceCrit * 100f));
    }

    public override void SetProperties(object[] param)
    {
        upChanceCrit = (float)param[0] / 100f;
        maxUpChance = (float)param[1] / 100f;
        _GradeParam = upChanceCrit;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Каждый выстрел по одной и той же цели получает {0}(<color=#19c62e>+{1}</color>)% к шансу крита. Сбрасывается после смерти монстра. Сбрасывается если переключился на другую цель",
              System.Math.Round(upChanceCrit * 100f,2),
              System.Math.Round(upChanceCrit * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, upChanceCrit);
    }
}
