﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class BaseLightningStunChanceProperties : basePropertiesGrade, INameAndIcon
{

    private float StunDuration;
    private Unit rootUnit;

    public string getName { get { return "Повышение напряжения"; } }
    public string getUrlIcon { get { return "EffectIcon/BaseLightningStunChanceProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        rootUnit = unit;
        CastAbilityBaseLightningChanceShot.OnCastBaseAbilityE += CastLightning;
        return true;
    }

    private void CastLightning(Ability ability, Unit root, Unit target)
    {
        if(root==rootUnit)
        {
            target.EffectsManager.AddEffect(rootUnit, new StunControlEffect(_GradeParam));
        }
    }

    public override bool Cancel(Unit unit)
    {
        rootUnit = null;
        CastAbilityBaseLightningChanceShot.OnCastBaseAbilityE -= CastLightning;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Молния оглушает врагов на {0} секунды",
            StunDuration);
    }

    public override void SetProperties(object[] param)
    {
        StunDuration = (float)(param[0]);
        _GradeParam = StunDuration;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Молния оглушает врагов на {0}(<color=#19c62e>+{1}</color>) секунды",
                StunDuration,
                System.Math.Round(StunDuration * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, StunDuration);
    }
}
