﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class StunAfterCritProperties : DisorientationProperties, INameAndIcon
{
    private float StunDuration;
    private bool bWaitCrit;

    public override string getName { get { return "Контрольный выстрел"; } }
    public override string getUrlIcon { get { return "EffectIcon/StunAfterCritProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        bWaitCrit = true;
        unit.myOutAttakInstanceLastE += LastAttack;
        unit.myOutAttakInstanceFirstE += FirstAttack;
        return true;
    }

    private void FirstAttack(AttackInstance instance)
    {
        if (bWaitCrit==false)
        {
            instance.stunned = true;
            instance.listEffects.Add(new StunControlEffect(_GradeParam));
            new TextOverlay(instance.srcUnit.getPosition, "Stun!", Color.yellow, false);
            bWaitCrit = true;
        }
    }

    private void LastAttack(AttackInstance instance)
    {
        if(bWaitCrit && instance.critical)
        {
            bWaitCrit = false;
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceLastE -= LastAttack;
        unit.myOutAttakInstanceFirstE -= FirstAttack;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Следующий выстрел, после критического, оглушает цель на {0} секунду", StunDuration);
    }

    public override void SetProperties(object[] param)
    {
        StunDuration = (float)param[0];
        _GradeParam = StunDuration;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Следующий выстрел, после критического, оглушает цель на {0}(<color=#19c62e>+{1}</color>) секунду",
                StunDuration, System.Math.Round(StunDuration * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, StunDuration);
    }
}
