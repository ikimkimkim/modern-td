﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class UpDmgForBossAndRageAfterCritProperties : basePropertiesGrade, INameAndIcon
{
    private float UpDmgBoss;
    private float TimeRage, UpCooldownRage, UpChanceCrit;
    private bool inRage;
    private Unit rootUnit;

    public string getName { get { return "Элитный снайпер"; } }

    public string getUrlIcon { get { return "EffectIcon/UpDmgForBossAndRageAfterCritProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        inRage = false;
        rootUnit = unit;
        unit.myOutAttakInstanceInitE += Unit_myOutAttakInstanceInitE;
        unit.myOutAttakInstanceLastE += Unit_myOutAttakInstanceLastE;
        return true;
    }

    private void Unit_myOutAttakInstanceInitE(AttackInstance attackInstance)
    {
        if (attackInstance.tgtUnit is UnitCreep)
        {
            if ((attackInstance.tgtUnit as UnitCreep).isBoss)
            {
                attackInstance.damage *= (1f + _GradeParam);
            }
        }

        if(inRage)
        {
            attackInstance.AddChanceCritical += UpChanceCrit;
        }
    }

    private void Unit_myOutAttakInstanceLastE(AttackInstance attackInstance)
    {
        if(attackInstance.critical && inRage==false)
        {
            rootUnit.StartCoroutine(Rage());
        }
    }

    private IEnumerator Rage()
    {
        inRage = true;
        for (int i = 0; i < rootUnit.stats.Count; i++)
        {
            rootUnit.stats[i].cooldown *= (1f - UpCooldownRage);
        }
        yield return new WaitForSeconds(TimeRage);
        for (int i = 0; i < rootUnit.stats.Count; i++)
        {
            rootUnit.stats[i].cooldown /= (1f - UpCooldownRage);
        }
        inRage = false;
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceInitE -= Unit_myOutAttakInstanceInitE;
        unit.myOutAttakInstanceLastE -= Unit_myOutAttakInstanceLastE;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Увеличен урон по боссам на {2}%. После критического удара впадает в раж, во время действия которого получает +{0}% к скорости атаки и +{1}% шанс крита",
            Mathf.Round(UpCooldownRage * 100f), Mathf.Round(UpChanceCrit * 100f),
            UpDmgBoss*100f);
    }

    public override void SetProperties(object[] param)
    {
        UpDmgBoss = (float)param[0] / 100f;
        TimeRage = (float)param[1];
        UpCooldownRage = (float)param[2] / 100f;
        UpChanceCrit = (float)param[3] / 100f;
        _GradeParam = UpDmgBoss;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Увеличен урон по боссам на {2}(<color=#19c62e>+{3}</color>)%. После критического удара впадает в раж, во время действия которого получает +{0}% к скорости атаки и +{1}% шанс крита",
                Mathf.Round(UpCooldownRage * 100f), Mathf.Round(UpChanceCrit * 100f),
                System.Math.Round(UpDmgBoss * 100f,2),
                System.Math.Round(UpDmgBoss *_multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, UpDmgBoss);
    }
}
