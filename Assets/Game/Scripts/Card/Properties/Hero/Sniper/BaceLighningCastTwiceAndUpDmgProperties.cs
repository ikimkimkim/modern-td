﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TDTK;

public class BaceLighningCastTwiceAndUpDmgProperties : baseProperties, INameAndIcon
{
    private const float timeWaitTwoCast = .5f;
    private float UpDmg;

    private Unit rootUnit;
    private CastAbilityBaseLightningChanceShot linkProp;

    public string getName { get { return ""; } }
    public string getUrlIcon { get { return "EffectIcon/BaceLighningCastTwiceAndUpDmgProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        if (unit.IsHero == false)
            return false;
        CardProperties prop = unit.MyCard.Properties.Find(i => i.ID == CastAbilityBaseLightningChanceShot.IDProperties);
        if (prop == null)
            return false;

        linkProp = prop.properties as CastAbilityBaseLightningChanceShot;
        linkProp.dmgMultiplier += UpDmg;
        rootUnit = unit;
        CastAbilityBaseLightningChanceShot.OnCastBaseAbilityE += CastLightning;
        return true;
    }

    private void CastLightning(Ability ability, Unit root, Unit target)
    {
        if (root == rootUnit)
        {
            root.StartCoroutine(CastAb(timeWaitTwoCast, target, ability.Clone()));
        }
    }

    private IEnumerator CastAb(float timeWait,Unit target,Ability ability)
    {
        yield return new WaitForSeconds(timeWait);
        AbilityManager.instanceCastAbility(ability, target.thisT.position);

        yield break;
    }

    public override bool Cancel(Unit unit)
    {
        linkProp.dmgMultiplier -= UpDmg;
        linkProp = null;
        rootUnit = null;
        CastAbilityBaseLightningChanceShot.OnCastBaseAbilityE -= CastLightning;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Вместо одной вызываются две молнии. Помимо этого на {0}% увеличен урон",
            Mathf.Round(UpDmg*100f));
    }

    public override void SetProperties(object[] param)
    {
        UpDmg = (float) param[0]/100f;
    }

}
