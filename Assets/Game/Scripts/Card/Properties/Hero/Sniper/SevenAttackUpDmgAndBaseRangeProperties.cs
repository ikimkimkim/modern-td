﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class SevenAttackUpDmgAndBaseRangeProperties : basePropertiesGrade, INameAndIcon
{
    private const int limitAttack = 3;
    private int counterAttack;

    private float dmgUp, rangeUp;

    private Unit rootUnit;

    public override bool ApplyIsSampleTower { get { return true; } }

    public string getName { get { return "Пристрелка"; } }

    public string getUrlIcon { get { return "EffectIcon/SevenAttackUpDmgAndBaseRangeProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        rootUnit = unit;
        counterAttack = 0;

        for (int i = 0; i < rootUnit.stats.Count; i++)
        {
            rootUnit.stats[i].range *= (1f + rangeUp);
        }

        rootUnit.myOutAttakInstanceFirstE += RootUnit_myOutAttakInstanceFirstE;

        return true;
    }

    private void RootUnit_myOutAttakInstanceFirstE(AttackInstance attackInstance)
    {
        counterAttack++;
        if (counterAttack==limitAttack)
        {
            attackInstance.damage *= (1f + _GradeParam);
            counterAttack = 0;
        }
    }

    public override bool Cancel(Unit unit)
    {
        for (int i = 0; i < rootUnit.stats.Count; i++)
        {
            rootUnit.stats[i].range /= (1f + rangeUp);
        }
        rootUnit.myOutAttakInstanceFirstE -= RootUnit_myOutAttakInstanceFirstE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Каждая {0} атака наносит {1}% урона. Увеличен радиус на {2}%",
            limitAttack, Mathf.Round(dmgUp*100f), Mathf.Round(rangeUp *100f));
    }

    public override void SetProperties(object[] param)
    {
        dmgUp = (float)param[0]/100f;
        rangeUp = (float)param[1]/100f;
        _GradeParam = dmgUp;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Каждая {0} атака наносит {1}(<color=#19c62e>+{3}</color>)% урона. Увеличен радиус на {2}%",
                limitAttack, System.Math.Round(dmgUp * 100f, 2), Mathf.Round(rangeUp * 100f),
                System.Math.Round(dmgUp * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmgUp);
    }
}
