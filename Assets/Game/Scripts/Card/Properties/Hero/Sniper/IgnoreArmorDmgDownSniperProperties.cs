﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreArmorDmgDownSniperProperties : IgnoreArmorDmgDownProperties
{

    public override string getName { get { return "Насквозь"; } }
    public override string getUrlIcon { get { return "EffectIcon/IgnoreArmorDmgDown.png"; } }

}
