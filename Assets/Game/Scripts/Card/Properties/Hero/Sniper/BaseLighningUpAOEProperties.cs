﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class BaseLighningUpAOEProperties : baseProperties, INameAndIcon
{

    private float upAOELinghning;
    private CastAbilityBaseLightningChanceShot linkProp;

    public string getName { get { return ""; } }

    public string getUrlIcon { get { return "EffectIcon/BaseLighningUpAOEProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        if (unit.IsHero == false)
            return false;
        CardProperties prop = unit.MyCard.Properties.Find(i => i.ID == CastAbilityBaseLightningChanceShot.IDProperties);
        if (prop == null)
            return false;

        linkProp = prop.properties as CastAbilityBaseLightningChanceShot;
        linkProp.ability.aoeRadius *= (1f+upAOELinghning);

        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (linkProp == null)
            return false;
        linkProp.ability.aoeRadius /=(1f + upAOELinghning);
        linkProp = null;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Радиус поражения молнии увеличен на {0}%", upAOELinghning);
    }

    public override void SetProperties(object[] param)
    {
        upAOELinghning = (float)param[0] / 100f;
    }
}
