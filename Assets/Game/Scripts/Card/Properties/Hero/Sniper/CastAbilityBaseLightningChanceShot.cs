﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using Translation;
using UnityEngine;

public class CastAbilityBaseLightningChanceShot : CastAbilityLightningChanceShot
{
    public const int IDProperties = 122;

    public static event OnCastAbilityDelegate OnCastBaseAbilityE;

    public override string getName { get { return "Громовержец"; } }

    public override string getUrlIcon { get { return "EffectIcon/CastAbilityBaseLightningChanceShot.png"; } }


    protected override void CastAbility(Unit target)
    {
        ability.effect.damageMax = unitlink.stats[unitlink.currentActiveStat].damageMax * _GradeParam; 
        ability.effect.damageMin = unitlink.stats[unitlink.currentActiveStat].damageMin * _GradeParam;
        ability.effect.baseEffects.Clear();

        AbilityManager.instanceCastAbility(ability, target.thisT.position);
        new TextOverlay(unitlink.getPosition, "Magic!", new Color(1f, 0.5f, 0, 1f), false);

        CallCastAbilityEvent(target);
    }

    protected override void CallCastAbilityEvent(Unit target)
    {
        if (OnCastBaseAbilityE != null)
            OnCastBaseAbilityE(ability, unitlink, target);
    }

    public override string GetTextInfo()
    {
        Translator trans = TranslationEngine.Instance.Trans;
        return string.Format("Вызывает \"{0}\" с небольшим радиусом поражения. Молния имеет {1}% урона снайпера",
           trans[ability.name], dmgMultiplier * 100f);
    }

    public override void SetProperties(object[] param)
    {
        idAbility = 9;

        var bdAbility = AbilityDB.Load();
        ability = bdAbility[idAbility].Clone();

        Chance = (float)param[0] / 100f;
        dmgMultiplier = (float)param[1] / 100f;
        RangeEffect = (float)param[2];
        _GradeParam = dmgMultiplier;

        ability.aoeRadius = RangeEffect;
    }

    public override string GetTextInfo(int level)
    {
        Translator trans = TranslationEngine.Instance.Trans;
        if (level > 0)
            return string.Format("Вызывает \"{0}\" с небольшим радиусом поражения. Молния имеет {1}(<color=#19c62e>+{2}</color>)% урона снайпера",
                trans[ability.name],
                System.Math.Round(dmgMultiplier * 100f, 2),
                System.Math.Round(dmgMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }
}
