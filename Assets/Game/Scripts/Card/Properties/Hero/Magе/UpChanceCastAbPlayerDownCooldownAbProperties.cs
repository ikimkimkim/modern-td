﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class UpChanceCastAbPlayerDownCooldownAbProperties : basePropertiesGrade, INameAndIcon
{
    private float lastChance, newChance, pCooldown;

    private static int CountActive = 0;

    CastAbilityPlayerProperties capp;

    public string getName { get { return "Мастер"; } }
    public string getUrlIcon { get { return "EffectIcon/UpChanceCastAbPlayerDownCooldownAbProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        var prop = unit.MyCard.Properties.Find(i => i.ID == CastAbilityPlayerProperties.ID);
        if (prop == null)
            return false;

        capp = prop.properties as CastAbilityPlayerProperties;

        lastChance = capp.chance;
        capp.chance = newChance;

        PerkManager.instance.globalAbilityModifier.cooldown = _GradeParam;
        CountActive++;

        return true;
    }

    public override bool Cancel(Unit unit)
    {
        capp.chance = lastChance;
        capp = null;
        CountActive--;
        if(CountActive<=0)
            PerkManager.instance.globalAbilityModifier.cooldown = 0;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Шанс повтора увеличен до {0}%. Также восстановление заклинаний уменьшено на {1}%",
            Mathf.Round(newChance*100f),Mathf.Round(pCooldown*100f));
    }

    public override void SetProperties(object[] param)
    {
        newChance = (float)param[0]/100f;
        pCooldown = (float)param[1]/100f;
        _GradeParam = pCooldown;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Шанс повтора увеличен до {0}%. Также восстановление заклинаний уменьшено на {1}(<color=#19c62e>+{2}</color>)%",
                Mathf.Round(newChance * 100f),
                System.Math.Round(pCooldown * 100f, 2),
                System.Math.Round(pCooldown * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, pCooldown);
    }
}
