﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicShockMageProperties : MagicShockProperties
{
    public override string getName { get { return "Выжигание еретиков"; } }
    public override string getUrlIcon
    {
        get
        {
            return "EffectIcon/MagicShockMageProperties.png";
        }
    }

}
