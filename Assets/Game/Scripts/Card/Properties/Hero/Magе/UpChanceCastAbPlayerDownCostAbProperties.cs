﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class UpChanceCastAbPlayerDownCostAbProperties : basePropertiesGrade, INameAndIcon
{
    private float lastChance, newChance, pCost;

    private static int CountActive = 0;

    CastAbilityPlayerProperties capp;

    public string getName { get { return "Грандмастер"; } }

    public string getUrlIcon { get { return "EffectIcon/UpChanceCastAbPlayerDownCostAbProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        var prop = unit.MyCard.Properties.Find(i => i.ID == CastAbilityPlayerProperties.ID);
        if (prop == null)
            return false;

        capp = prop.properties as CastAbilityPlayerProperties;

        lastChance = capp.chance;
        capp.chance = newChance;

        PerkManager.instance.globalAbilityModifier.cost = _GradeParam;
        if (CountActive == 0 && UIAbilityButton.instance!=null)
            UIAbilityButton.instance.UpdateDataAbility();
        CountActive++;

        return true;
    }

    public override bool Cancel(Unit unit)
    {
        capp.chance = lastChance;

        CountActive--;
        if (CountActive <= 0)
        {
            PerkManager.instance.globalAbilityModifier.cost = 0;
            UIAbilityButton.instance.UpdateDataAbility();
        }

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Шанс повтора увеличен до {0}%. Стоимость заклинаний игрока уменьшена на {1}%.",
            Mathf.Round(newChance*100f), Mathf.Round(pCost*100f));
    }

    public override void SetProperties(object[] param)
    {
        newChance = (float)param[0] / 100f;
        pCost = (float)param[1] / 100f;
        _GradeParam = pCost;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Шанс повтора увеличен до {0}%. Стоимость заклинаний игрока уменьшена на {1}(<color=#19c62e>+{2}</color>)%.",
                Mathf.Round(newChance * 100f),
                System.Math.Round(pCost * 100f,2),
                System.Math.Round(pCost * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, pCost);
    }
}