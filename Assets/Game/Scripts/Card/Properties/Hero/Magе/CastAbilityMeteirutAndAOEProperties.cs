﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class CastAbilityMeteirutAndAOEProperties : CastAbilityMeteoritChanceShot
{
    private float AOERange;

    public override string getName { get { return "Хадукен"; } }

    public override string getUrlIcon { get { return "EffectIcon/CastAbilityMeteirutAndAOEProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        base.Apply(unit);
        unit.myOutAttakInstanceInitE += Unit_myOutAttakInstanceInitE;
        return true;
    }

    private void Unit_myOutAttakInstanceInitE(AttackInstance attackInstance)
    {
        attackInstance.SetAOERadius(AOERange);
    }

    public override bool Cancel(Unit unit)
    {
        base.Cancel(unit);
        unit.myOutAttakInstanceInitE -= Unit_myOutAttakInstanceInitE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Маг с шансом {0}% вызывает метеор c уроном {2}%. Также автоатаки наносят урон по площади {1}.",
            Mathf.Round(Chance * 100f), AOERange,
            System.Math.Round(dmgMultiplier*100f));
    }

    public override void SetProperties(object[] param)
    {
        base.SetProperties(param);
        AOERange = (float)param[2];
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Маг с шансом {0}% вызывает метеор c уроном {2}(<color=#19c62e>+{3}</color>)%. Также автоатаки наносят урон по площади {1}.",
                 Mathf.Round(Chance * 100f), AOERange,
                System.Math.Round(dmgMultiplier * 100f,2),
                System.Math.Round(dmgMultiplier * _multiplierLevel * level * 100f,2));
        else
            return GetTextInfo();
    }
}
