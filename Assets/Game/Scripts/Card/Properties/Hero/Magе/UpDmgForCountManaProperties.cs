﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class UpDmgForCountManaProperties : basePropertiesGrade, INameAndIcon
{
    private float procentDmgUp;

    public string getName { get { return "Магический симбиот"; } }
    public string getUrlIcon { get { return "EffectIcon/UpDmgForCountManaProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceInitE += Unit_myOutAttakInstanceInitE;
        return true;
    }

    private void Unit_myOutAttakInstanceInitE(AttackInstance attackInstance)
    {
        attackInstance.damage *= (1f + _GradeParam * ResourceManager.GetResourceArray()[1]);
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceInitE -= Unit_myOutAttakInstanceInitE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Урон автоатак мага зависит от максимальной маны. Чем больше маны - тем больше урон. За каждую единицу маны маг наносит на {0}% больше урона",
            Mathf.Round(procentDmgUp * 100f));
    }

    public override void SetProperties(object[] param)
    {
        procentDmgUp = (float)param[0]/100f;
        _GradeParam = procentDmgUp;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Урон автоатак мага зависит от максимальной маны. Чем больше маны - тем больше урон. За каждую единицу маны маг наносит на {0}(<color=#19c62e>+{1}</color>)% больше урона",
                System.Math.Round(procentDmgUp * 100f, 2),
                System.Math.Round(procentDmgUp * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, procentDmgUp);
    }
}
