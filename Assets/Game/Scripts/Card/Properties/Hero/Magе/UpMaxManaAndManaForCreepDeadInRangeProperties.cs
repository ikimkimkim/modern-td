﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class UpMaxManaAndManaForCreepDeadInRangeProperties : basePropertiesGrade
{
    protected float addMaxMana,addMana, range;
    private Unit rootUnit;

    public override bool Apply(Unit unit)
    {
        rootUnit = unit;
        ResourceManager.instance.rscMaxRateList[1] += addMaxMana;
        UnitCreep.onDestroyedE += CreepDead;
        return true;
    }

    private void CreepDead(Unit unit)
    {
        if(unit.IsCreep && unit.DistanceForUnit(rootUnit) < range)
        {
            ResourceManager.GainIncomeResource(new List<float>() { 0, _GradeParam });
        }
    }

    public override bool Cancel(Unit unit)
    {
        rootUnit = null;
        ResourceManager.instance.rscMaxRateList[1] -= addMaxMana;
        UnitCreep.onDestroyedE -= CreepDead;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Каждый живой маг увеличивает максимальный запас маны на {0}. За убийство каждого монстра рядом с магом восстанавливается {1} маны.", 
            addMaxMana, addMana);
    }

    public override void SetProperties(object[] param)
    {
        addMaxMana = (float)param[0];
        addMana = (float)param[1];
        range = (float)param[2];
        _GradeParam = addMana;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Каждый живой маг увеличивает максимальный запас маны на {0}. За убийство каждого монстра рядом с магом восстанавливается {1} маны.",
                addMaxMana, addMana,
                System.Math.Round(addMana * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, addMana);
    }
}
