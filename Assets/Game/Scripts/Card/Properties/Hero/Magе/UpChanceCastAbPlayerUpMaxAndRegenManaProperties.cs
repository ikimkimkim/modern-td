﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class UpChanceCastAbPlayerUpMaxAndRegenManaProperties : UpMaxManaAndManaForCreepDeadInRangeProperties,INameAndIcon
{
    private float lastChance, newChance;
    CastAbilityPlayerProperties capp;

    public string getName { get { return "Ученик превзошёл учителя"; } }

    public string getUrlIcon { get { return "EffectIcon/UpChanceCastAbPlayerUpMaxAndRegenManaProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        var prop = unit.MyCard.Properties.Find(i => i.ID == CastAbilityPlayerProperties.ID);
        if (prop == null)
            return false;

        capp = prop.properties as CastAbilityPlayerProperties;

        lastChance = capp.chance;
        capp.chance = newChance;

        return base.Apply(unit);
    }

    public override bool Cancel(Unit unit)
    {
        capp.chance = lastChance;
        capp = null;
        return base.Cancel(unit);
    }

    public override string GetTextInfo()
    {
        return string.Format("Шанс повтора увеличен до {0}%. " +
            "Восстановление маны за убийство монстров рядом с магом увеличено на {1} и максимальная мана увеличена на {2}",
            Mathf.Round(newChance * 100f), addMana, addMaxMana);
    }

    public override void SetProperties(object[] param)
    {
        base.SetProperties(param);
        newChance = (float)param[3]/100f;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Шанс повтора увеличен до {0}%. " +
                "Восстановление маны за убийство монстров рядом с магом увеличено на {1}(<color=#19c62e>+{2}</color>) и максимальная мана увеличена на {3}",
                Mathf.Round(newChance * 100f), addMana,
                System.Math.Round(addMana * _multiplierLevel * level, 2), addMaxMana);
        else
            return GetTextInfo();
    }
}
