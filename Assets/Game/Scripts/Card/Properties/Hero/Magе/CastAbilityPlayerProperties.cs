﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class CastAbilityPlayerProperties : basePropertiesGrade, INameAndIcon
{
    public const int ID = 140;

    private const float delay = 2f;
    public float chance;

    private Unit rootUnit;
    private Coroutine corUpdate;

    public string getName { get { return "Ученик чародея"; } }

    public string getUrlIcon { get { return "EffectIcon/CastAbilityPlayerProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        rootUnit = unit;
        AbilityManager.onAbilityActivatedE += AbilityManager_onAbilityActivatedE;
        return true;
    }

    private void AbilityManager_onAbilityActivatedE(Ability ab)
    {
        if (Random.Range(0f, 1f) < _GradeParam && rootUnit.target != null)
        {
            corUpdate = rootUnit.StartCoroutine(CastDelayAbility(ab, rootUnit.target));
        }
    }

    IEnumerator CastDelayAbility(Ability ability, Unit target)
    {
        yield return new WaitForSeconds(delay);
        AbilityManager.instanceCastAbility(ability, target.getPosition, target);
        yield break;
    }

    public override bool Cancel(Unit unit)
    {
        if(corUpdate!=null)
            rootUnit.StopCoroutine(corUpdate);
        rootUnit = null;
        AbilityManager.onAbilityActivatedE -= AbilityManager_onAbilityActivatedE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("С шансом {0}% повторяет за игроком заклинание (кроме взрыва и массовой заморозки) на противника радиусе атаки",
            Mathf.Round(chance * 100f));
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0]/100f;
        _GradeParam = chance;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("С шансом {0}(<color=#19c62e>+{1}</color>)% повторяет за игроком заклинание (кроме взрыва и массовой заморозки) на противника радиусе атаки",
               System.Math.Round(chance * 100f, 2),
               System.Math.Round(chance * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, chance);
    }
}
