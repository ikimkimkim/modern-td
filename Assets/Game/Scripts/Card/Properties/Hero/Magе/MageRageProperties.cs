﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageRageProperties : InRage
{
    public override string getName { get { return "Дикость"; } }
    public override string getUrlIcon { get { return "EffectIcon/MageRageProperties.png"; } }

}
