﻿using UnityEngine;
using System.Collections;
using TDTK;

public class UpCountTargetMeProperties : basePropertiesGrade, INameAndIcon
{
    private float UpCount;
    private int baseCount;
    private const float UpHP = 0.5f;

    public string getName { get { return "Все на меня!"; } }
    public string getUrlIcon { get { return "EffectIcon/UpCountTargetMeProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.fullHP *= (1f + _GradeParam);
        unit.HP *= (1f + _GradeParam);
        baseCount = unit.MaxTargetМе;
        unit.MaxTargetМе = Mathf.RoundToInt(baseCount + UpCount);

        return true;
    }

    public override bool Cancel(Unit unit)
    {
        unit.fullHP /= (1f + _GradeParam);
        unit.HP /= (1f + _GradeParam);
        unit.MaxTargetМе = baseCount;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Количество монстров, которое может атаковать мечника, увеличено на {0}. Максимальное здоровье увеличено на {1}%",
           UpCount, UpHP * 100f);
    }

    public override void SetProperties(object[] param)
    {
        UpCount = (float)param[0];
        _GradeParam = UpHP;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Количество монстров, которое может атаковать мечника, увеличено на {0}. Максимальное здоровье увеличено на {1}(<color=#19c62e>+{2}</color>)%",
               UpCount,
               System.Math.Round(UpHP * 100f,2),
               System.Math.Round(UpHP * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, UpHP);
    }
}