﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class UpInHealAndInShildProperties : basePropertiesGrade, INameAndIcon
{
    private float Procent;

    private Unit root;
    public string getName { get { return "Метаболизм"; } }
    public string getUrlIcon { get { return "EffectIcon/SwordmanUpHealAndShieldProperties.png"; } }
    
    public override bool Apply(Unit unit)
    {
        root = unit;
        root.HealEvent += Unit_HealEvent;
        root.EffectsManager.startAddMyEffectE += EffectsManager_startAddMyEffectE;

        return true;
    }

    private void EffectsManager_startAddMyEffectE(BaseEffect effect)
    {
        //Debug.Log("Effect:"+effect.IDStack);
        if(effect.IDStack == IDEffect.ShieldBuff)
        {
            ShieldMedicBuffEffect ef = effect as ShieldMedicBuffEffect;
            ef.deffFullShield *= (1f + _GradeParam);
        }
    }

    private void Unit_HealEvent(float countHeal)
    {
        root.AddHP(countHeal * (1f + _GradeParam));
    }

    public override bool Cancel(Unit unit)
    {
        root.HealEvent -= Unit_HealEvent;
        root.EffectsManager.startAddMyEffectE -= EffectsManager_startAddMyEffectE;
        root = null;
        return true;
    }
    
    public override string GetTextInfo()
    {
        return string.Format("Восстановление здоровья и щиты, накладываемые медиком, в {0} раза эффективнее",
            Math.Round(1f+Procent,1).ToString());
    }

    public override void SetProperties(object[] param)
    {
        Procent = (float)param[0] / 100f;
        _GradeParam = Procent;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Восстановление здоровья и щиты, накладываемые медиком, в {0}(<color=#19c62e>+{1}</color>) раза эффективнее",
                Math.Round(1f + Procent, 2),
                Math.Round(Procent * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, Procent);
    }
}
