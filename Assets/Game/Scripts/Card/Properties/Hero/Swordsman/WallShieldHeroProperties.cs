﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class WallShieldHeroProperties : basePropertiesGrade, INameAndIcon
{
    public int AddCountClon;
    private int CountClon;
    private int countClon { get { return CountClon + AddCountClon; } }

    private float HPProcent;
    private float TimeLife;
    public float Cooldown;

    private float lastTimeAcitve;

    private GameObject prefab;

    private UnitHero root;

    public string getName { get { return "Стена щитов"; } }
    public string getUrlIcon { get { return "EffectIcon/WallShieldHeroProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        if (unit is UnitHero == false)
            return false;

        if (Holograms==null)
            Holograms = new List<Unit>();
        else
            Holograms.Clear();

        root = unit as UnitHero;
        clonCard = root.MyCard.Clone();
        clonCard.Properties.Clear();

        prefab = EffectDB.Load()[13];
        ObjectPoolManager.New(prefab);
        root.myInAttakInstanceLastE += Root_myInAttakInstanceLastE;
        LastActive = -Cooldown - TimeLife;

        return true;
    }

    GameObject obj;

    public List<Unit> Holograms;
    private FakeUnitHero holo;

    private Card clonCard;
    private float LastActive;

    private void Root_myInAttakInstanceLastE(AttackInstance attackInstance)
    {
        for (int i = 0; i < Holograms.Count; i++)
        {
            if(Holograms[i].dead)
            {
                Holograms.RemoveAt(i);
                i--;
            }
        }

        if (root.TargetMe.Count>0 && Holograms.Count<=0 && Time.time>=LastActive + Cooldown + _GradeParam)
        {
            //Debug.LogWarning("InAttack " + root.TargetMe.Count + " h:" + Holograms.Count + " t:" + (Time.time >= LastActive + Cooldown + TimeLife));
            Vector3 spawnPos = root.getPosition + root.thisT.forward * 0.2f + root.thisT.right * countClon / 2f;
            for (int i = 0; i < countClon; i++)
            {
                obj = ObjectPoolManager.Spawn(prefab);
                Transform objT = obj.transform;
                spawnPos -= root.thisT.right * i / 2f;
                spawnPos.y = root.thisT.position.y;
                holo = obj.GetComponent<FakeUnitHero>();
                holo.InitHero(-1,clonCard,root.currentActiveStat);
                objT.position = spawnPos;
                holo._Agent.SetDestination(spawnPos);
                holo.TimeLife = _GradeParam;
                holo.fullHP *= HPProcent;
                holo.HP = holo.fullHP;
                if (i<root.TargetMe.Count)
                {
                    root.TargetMe[i].priorityTarget = holo;
                    objT.LookAt(root.TargetMe[i].thisT);
                }
                Holograms.Add(holo);
            }
            LastActive = Time.time;
        }
    }

    public override bool Cancel(Unit unit)
    {
        root.myInAttakInstanceLastE -= Root_myInAttakInstanceLastE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Мечник создаёт рядом с собой {0} копии (голограммы) на {2}c. Копии имеют {1}% кол-во здоровья от оригинала и заставляют врагов атаковать себя",
            CountClon, Mathf.Round(HPProcent*100f), TimeLife);
    }

    public override void SetProperties(object[] param)
    {
        CountClon = System.Convert.ToInt32(param[0]);
        AddCountClon = 0;
        TimeLife = (float)param[1];
        HPProcent = (float)param[2]/100f;
        Cooldown = (float)param[3];
        _GradeParam = TimeLife;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Мечник создаёт рядом с собой {0} копии (голограммы) на {2}(<color=#19c62e>+{3}</color>)c. Копии имеют {1}% кол-во здоровья от оригинала и заставляют врагов атаковать себя",
                CountClon, Mathf.Round(HPProcent * 100f), TimeLife,
                 System.Math.Round(TimeLife * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, TimeLife);
    }
}
