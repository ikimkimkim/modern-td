﻿using UnityEngine;
using System.Collections;
using TDTK;

public class StunChanceDisorientationSwordsman : StunChanceDisorientation
{
    public override string getName { get { return "Нокаут"; } }
    public override string getUrlIcon { get { return "EffectIcon/StunChanceDisorientationSwordsman.png"; } }


    private float upHP;

    public override bool Apply(Unit unit)
    {
        unit.fullHP *= (1f + upHP);
        unit.HP *= (1f + upHP);
        return base.Apply(unit);
    }

    public override bool Cancel(Unit unit)
    {
        unit.fullHP /= (1f + upHP);
        unit.HP /= (1f + upHP);
        return base.Cancel(unit);
    }


    public override string GetTextInfo()
    {
        return string.Format("При попадании по дезориентированному противнику с вероятностью {0}% оглушает цель на {1} секунды, и получает +{2}% к здоровью",
            StunChance * 100f, StunDuration,upHP*100);
    }

    public override void SetProperties(object[] param)
    {
        base.SetProperties(param);
        upHP = 0.5f;// (float)param[2] / 100f;
    }


    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("При попадании по дезориентированному противнику с вероятностью {0}(<color=#19c62e>+{2}</color>)% оглушает цель на {1} секунды, и получает +{3}% к здоровью",
                System.Math.Round(StunChance * 100f, 2), StunDuration,
                System.Math.Round(StunChance * _multiplierLevel * level * 100f, 2), upHP * 100);
        else
            return GetTextInfo();
    }
}
