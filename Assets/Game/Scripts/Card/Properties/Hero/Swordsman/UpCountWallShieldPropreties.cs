﻿using UnityEngine;
using System.Collections;
using TDTK;

public class UpCountWallShieldPropreties : basePropertiesGrade, INameAndIcon
{
    public string getName { get { return "Боевое построение"; } }
    public string getUrlIcon { get { return "EffectIcon/UpCountWallShield.png"; } }

    private float addClon, procentCooldown;

    private WallShieldHeroProperties linkProperties;

    public override bool Apply(Unit unit)
    {
        CardProperties wallShield = unit.MyCard.Properties.Find(i => i.ID == 94);
        if (wallShield == null)
            return false;

        linkProperties = wallShield.properties as WallShieldHeroProperties;
        linkProperties.Cooldown *= 1f - _GradeParam;
        linkProperties.AddCountClon = (int) addClon;
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if(linkProperties!=null)
        {
            linkProperties.AddCountClon = 0;
            linkProperties.Cooldown /= 1f - _GradeParam;
            linkProperties = null;
        }
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Усиливается \"Стена щитов\" на {0} голограмы. Сокращается восстановление способности \"Стена щитов\" на {1}%",
            addClon, procentCooldown * 100f);
    }

    public override void SetProperties(object[] param)
    {
        addClon = System.Convert.ToInt32(param[0]);
        procentCooldown = (float) param[1] / 100f;
        _GradeParam = procentCooldown;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Усиливается \"Стена щитов\" на {0} голограмы. Сокращается восстановление способности \"Стена щитов\" на {1}(<color=#19c62e>+{2}</color>)%",
                addClon, System.Math.Round(procentCooldown * 100f, 2),
                System.Math.Round(procentCooldown * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, procentCooldown);
    }
}
