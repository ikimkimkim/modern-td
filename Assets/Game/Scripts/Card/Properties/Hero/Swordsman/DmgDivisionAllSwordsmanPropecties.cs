﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;
using System.Collections.Generic;

public class DmgDivisionAllSwordsmanPropecties : baseProperties
{
    private float Range;
    private float Procent;

    private bool bApply;

    public UnitHero hero;


    public override bool Apply(Unit unit)
    {
        if (bApply)
            return false;

        if (unit is UnitHero == false)
            return false;

        hero = unit as UnitHero;
        bApply = true;
        hero.myInAttakInstanceLastE += Hero_myInAttakInstanceLastE;

        return true;
    }

    List<UnitHero> activeHero;
    private void Hero_myInAttakInstanceLastE(AttackInstance attackInstance)
    {
        activeHero = UIHeroSpawn.instance.ActiveHeroList.FindAll(i => i.prefabID == hero.prefabID && i != hero && 
                                                hero.getDistanceFromTarget(i,hero.getPosition)<=Range);

        if (activeHero.Count <= 0)
            return;
        float dmg = attackInstance.damage * Procent / (activeHero.Count + 1);
        attackInstance.damage *= 1-Procent+Procent/(activeHero.Count+1);
        for (int i = 0; i < activeHero.Count; i++)
        {
            activeHero[i].ApplyDamage(hero, dmg * attackInstance.dmgModifier,attackInstance.srcUnit.DamageType(), false);
        }
    }

    public override bool Cancel(Unit unit)
    {
        if (!bApply)
            return false;

        hero.myInAttakInstanceLastE -= Hero_myInAttakInstanceLastE;
        hero = null;
        bApply = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Часть урона распределяется равномерно между всеми мечниками стоящими рядом в {0} радиусе",
            Math.Round(Range, 1).ToString());
    }

    public override void SetProperties(object[] param)
    {
        bApply = false;
        Range = (float)(Convert.ToDouble(param[0]));
        Procent = (float)param[1]/100f;
    }
}
