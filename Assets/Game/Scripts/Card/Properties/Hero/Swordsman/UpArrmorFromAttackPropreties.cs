﻿using UnityEngine;
using System.Collections;
using TDTK;

public class UpArrmorFromAttackPropreties : basePropertiesGrade, INameAndIcon
{
    private float Procent;
    private float TimeOff;
    private int MaxStack;

    private Unit root;
    public string getName { get { return "Закалка стали"; } }
    public string getUrlIcon { get { return "EffectIcon/SwordmanUpArmorProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        root = unit;
        root.myInAttakInstanceLastE += Root_myInAttakInstanceLastE;

        return true;
    }

    private void Root_myInAttakInstanceLastE(AttackInstance attackInstance)
    {
        root.EffectsManager.AddEffect(root, new UpArmorBuffEffect(Procent, _GradeParam, MaxStack));
    }

    public override bool Cancel(Unit unit)
    {
        root.myInAttakInstanceLastE -= Root_myInAttakInstanceLastE;
        root = null;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("От каждой автоатаки врага у мечника увеличивается на {0}% броня. Усиление исчезает если его не атакуют {1} с.",
            Mathf.Round(Procent*100f),TimeOff);
    }

    public override void SetProperties(object[] param)
    {
        Procent = (float)param[0]/100f;
        TimeOff = (float)param[1];
        MaxStack = System.Convert.ToInt32(param[2]);
        _GradeParam = TimeOff;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("От каждой автоатаки врага у мечника увеличивается на {0}% броня. Усиление исчезает если его не атакуют {1}(<color=#19c62e>+{2}</color>) с.",
                Mathf.Round(Procent * 100f), TimeOff,
                System.Math.Round(TimeOff * _multiplierLevel * level, 2));
        else
            return GetTextInfo();

    }

    public override void SetLevel(int level)
    {
        SetLevel(level, TimeOff);
    }
}
