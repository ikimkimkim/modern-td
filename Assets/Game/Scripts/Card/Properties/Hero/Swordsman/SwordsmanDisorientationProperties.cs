﻿using UnityEngine;
using System.Collections;
using TDTK;

public class SwordsmanDisorientationProperties : DisorientationProperties
{
    public override string getName { get { return "Тяжёлая рука"; } }
    public override string getUrlIcon { get { return "EffectIcon/SwordsmanDisorientation.png"; } }

    private float upHP;

    public override bool Apply(Unit unit)
    {
        unit.fullHP *= (1f + upHP);
        unit.HP *= (1f + upHP);
        return base.Apply(unit);
    }

    public override bool Cancel(Unit unit)
    {
        unit.fullHP /= (1f + upHP);
        unit.HP /= (1f + upHP);
        return base.Cancel(unit);
    }

    public override string GetTextInfo()
    {
        return string.Format("Автоатаки мечника накладывают на врагов статус Дезориентации на {1}c и получает +{0}% к здоровью",
            upHP * 100f, duration);
    }

    public override void SetProperties(object[] param)
    {
        base.SetProperties(param);
        upHP = (float)param[4] / 100f;
    }


    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Автоатаки мечника накладывают на врагов статус Дезориентации на {1}(<color=#19c62e>+{2}</color>)c и получает +{0}% к здоровью",
                upHP * 100f, duration,
                System.Math.Round(duration * _multiplierLevel * level,2));
        else
            return GetTextInfo();
    }
}
