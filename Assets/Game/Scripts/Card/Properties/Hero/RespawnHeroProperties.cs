﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class RespawnHeroProperties : baseProperties
{
    private float _timeOff;
    private UnitHero _hero;

    public override bool Apply(Unit unit)
    {
        if (unit is UnitHero == false)
            return false;

        _hero = unit as UnitHero;
        _hero.ImmortalityOn(_timeOff);
        return true;
    }


    public override bool Cancel(Unit unit)
    {
        if (unit is UnitHero == false)
            return false;

        _hero.ImmortalityOff();

        _hero = null;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("После смерти через {0}с воскрешается",_timeOff);
    }

    public override void SetProperties(object[] param)
    {
        _timeOff = (float)param[0];
    }
}
