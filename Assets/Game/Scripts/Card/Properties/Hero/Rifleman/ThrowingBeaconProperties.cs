﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;
using System;

public class ThrowingBeaconProperties : basePropertiesGrade, IPropertiesEvent, INameAndIcon
{
    public event EventHandler<eventPropertiesArgs> eventUpdateStatus;
    public eventPropertiesArgs Status;
    public string getName { get { return "Воздушная помощь"; } }
    public string getUrlIcon { get { return "EffectIcon/Beacon.png"; } }

    private float timer, cooldown, dmg/*, range*/;
    public PropertiesAttak attak;
    public GameObject shootObjPrefab;
    private Ability ability;

    UnitHero hero;
    Coroutine throwing;

    public override bool Apply(Unit unit)
    {
        if (unit.IsHero == false)
            return false;

        hero = unit as UnitHero;
        throwing = hero.StartCoroutine(CorThrowingBeacon());

        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if(hero!=null)
        {
            if (throwing != null)
                hero.StopCoroutine(throwing);
            hero = null;
        }
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Пулемётчик кидает маячок и через 3 с. в данную точку вызывается массивная бомбардировка нанося {0}% урона",
            dmg * 100f);
    }

    public override void SetProperties(object[] param)
    {
        Status = new eventPropertiesArgs();
        shootObjPrefab = DataBase<GameObject>.Load(DataBase<GameObject>.NameBase.ShootObject)[0]; //EffectDB.Load()[17];
        ability = AbilityDB.Load()[0].Clone();

        timer = (float)param[0];
        cooldown = (float)param[1];
        dmg = (float)param[2] / 100f;
        _GradeParam = dmg;
        attak = new PropertiesAttak();
        attak.stats[0].cooldown = cooldown;
       // range = (float)param[3];
    }

    public IEnumerator CorThrowingBeacon()
    {
        Debug.LogWarning("Beacon Cor Start");


        Collider[] cols;
        Unit unit;
        List<Unit> tgtList = new List<Unit>();
        LayerMask mask = 1 << LayerManager.LayerCreep();

        hero.PropertiesAttakLock = false;
        attak.mask = 1 << LayerManager.LayerCreep(); //hero.GetTargetMask();

        if (hero.GrenadesShootPoint == null) { hero.GrenadesShootPoint = hero.thisT; }

        attak.shootPoints.Clear();
        attak.shootPoints.Add(hero.GrenadesShootPoint);

        if (hero.GrenadesEffect != null) ObjectPoolManager.New(hero.GrenadesEffect, 3);


        yield return null;

        while (true)
        {
            if (hero.dead || hero.isMoveEnd() == false || hero.PropertiesAttakLock == true)
            {
                yield return null;
                continue;
            }

            cols = Physics.OverlapSphere(hero.thisT.position, hero.GetRange(), mask);

            if (cols.Length > 0)
            {
                tgtList.Clear();
                for (int i = 0; i < cols.Length; i++)
                {
                    unit = cols[i].gameObject.GetComponent<Unit>();
                    if (unit.dead == false)
                        tgtList.Add(unit);
                }

                if (tgtList.Count == 0)
                {
                    yield return null;
                    continue;
                }

                unit = tgtList[UnityEngine.Random.Range(0, tgtList.Count)];

                MyLog.Log("Beacon ATAK: cd" + attak.stats[0].cooldown);

                hero.bGrenadeEventAnim = false;
                hero.animationUnit.StartGrenadeMecanim();
                hero.PropertiesAttakLock = true;

                hero.thisT.LookAt(unit.getPosition);
                while (hero.bGrenadeEventAnim == false /*&& hero.target!=null*/ && hero.dead == false)
                {
                    yield return null;
                }

                if (hero.dead || hero.isMoveEnd() == false)
                {
                    yield return null;
                    continue;
                }

                Unit currentTarget = unit;

                attak.stats[0].damageMin =0;
                attak.stats[0].damageMax =0;

                AttackInstance attInstance = new AttackInstance();
                attInstance.srcPropretiesAttak = attak;
                attInstance.tgtUnit = currentTarget;
                //attInstance.Process();


                GameObject obj = ObjectPoolManager.Spawn(shootObjPrefab, hero.GrenadesShootPoint.position, hero.GrenadesShootPoint.rotation);  // (Transform)Instantiate(GetShootObjectT(), sp.position, sp.rotation);
                Debug.Log("Name becon:" + obj.name);
                ShootObject shootObj = obj.GetComponent<ShootObject>();
                shootObj.TimerBombLife = timer;
                shootObj.abilityBomb = ability;
                shootObj.abilityBomb.effect.damageMin = hero.GetDamageMin() * _GradeParam;
                shootObj.abilityBomb.effect.damageMax = hero.GetDamageMax() * _GradeParam;


                shootObj.Shoot(attInstance, hero.GrenadesShootPoint);

                //Debug.LogWarning("CD 1:" + cd);

                hero.PropertiesAttakLock = false;

                float time = attak.GetCooldown()/* - hero.GrenadesAnimDelay*/;
                while (time > 0)
                {
                    yield return null;
                    time -= Time.deltaTime;
                    Status.Cooldown = time;
                    if (eventUpdateStatus != null)
                        eventUpdateStatus(this, Status);
                }
                //yield return new WaitForSeconds(attak.GetCooldown() - hero.GrenadesAnimDelay);
            }
            else
                yield return null;

        }
    }

    public void SetListener(int id, EventHandler<eventPropertiesArgs> eventUpdate)
    {
        Status.id = id;
        eventUpdateStatus += eventUpdate;
    }

    public void RemovListener(EventHandler<eventPropertiesArgs> eventUpdate)
    {
        eventUpdateStatus -= eventUpdate;
        Status.id = -1;
    }
    public eventPropertiesArgs getStatus()
    {
        return Status;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Пулемётчик кидает маячок и через 3 с. в данную точку вызывается массивная бомбардировка нанося {0}(<color=#19c62e>+{1}</color>)% урона",
                Math.Round(dmg * 100f, 2),
                Math.Round(dmg * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmg);
    }
}
