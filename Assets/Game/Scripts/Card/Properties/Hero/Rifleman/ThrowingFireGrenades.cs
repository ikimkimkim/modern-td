﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;
using System.Collections.Generic;

public class ThrowingFireGrenades : basePropertiesGrade, IPropertiesEvent, INameAndIcon
{
    public event EventHandler<eventPropertiesArgs> eventUpdateStatus;
    public eventPropertiesArgs Status;
    public bool bApply = false;
    private Coroutine coroutineAtak;

    UnitHero hero;
    public float dmgProcent;
    public PropertiesAttak attak;

    public string getName { get { return "Молотов"; } }
    public string getUrlIcon { get { return "EffectIcon/GrenadeFireProperties.png"; } }

    private float FireDuration, FireDmgMultiplier, CountTik;

    public override bool Apply(Unit unit)
    {
        if (bApply == false)
        {
            if (unit.IsHero)
            {
                hero = unit.GetUnitHero;
                if (coroutineAtak != null)
                    unit.StopCoroutine(coroutineAtak);
                coroutineAtak = unit.StartCoroutine(CorThrowingGrenades());
            }
        }
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply)
        {
            if (coroutineAtak != null)
                unit.StopCoroutine(coroutineAtak);
        }
        bApply = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return String.Format("Пулемётчик начинает кидать во врагов Напалм-гранату поджигающие врага нанося {0}% дополнительного урона за {1}с",
            FireDmgMultiplier*100f, FireDuration);
    }

    public override void SetProperties(object[] param)
    {
        Status = new eventPropertiesArgs();
        attak = new PropertiesAttak();
        attak.damageType = 1;
        attak.stats = new System.Collections.Generic.List<UnitStat>();
        attak.stats.Add(new UnitStat());
        dmgProcent = (float)param[0] / 100f;
        attak.stats[0].aoeRadius = 2;
        attak.stats[0].cooldown = (float)param[1];

        FireDmgMultiplier = (float)param[2] / 100f;
        FireDuration = (float)param[3];
        CountTik = FireDuration / FireDot.FireInterval;
        _GradeParam = FireDmgMultiplier;
    }

    public IEnumerator CorThrowingGrenades()
    {
        Debug.LogWarning("Granat Cor Start");


        Collider[] cols;
        Unit unit;
        List<Unit> tgtList = new List<Unit>();
        LayerMask mask = 1 << LayerManager.LayerCreep();

        hero.PropertiesAttakLock = false;
        attak.mask = 1 << LayerManager.LayerCreep(); //hero.GetTargetMask();

        if (hero.GrenadesShootPoint == null) { hero.GrenadesShootPoint = hero.thisT; }

        attak.shootPoints.Clear();
        attak.shootPoints.Add(hero.GrenadesShootPoint);

        if (hero.GrenadesEffect != null) ObjectPoolManager.New(hero.GrenadesEffect, 3);


        yield return null;

        while (true)
        {
            if (hero.dead || hero.isMoveEnd() == false || hero.PropertiesAttakLock == true)
            {
                yield return null;
                continue;
            }

            cols = Physics.OverlapSphere(hero.thisT.position, hero.GetRange(), mask);

            if (cols.Length > 0)
            {
                tgtList.Clear();
                for (int i = 0; i < cols.Length; i++)
                {
                    unit = cols[i].gameObject.GetComponent<Unit>();
                    if (unit.dead == false)
                        tgtList.Add(unit);
                }

                if (tgtList.Count == 0)
                {
                    yield return null;
                    continue;
                }

                unit = tgtList[UnityEngine.Random.Range(0, tgtList.Count)];

                Debug.Log("Granat ATAK: cd" + attak.stats[0].cooldown);

                hero.bGrenadeEventAnim = false;
                hero.animationUnit.StartGrenadeMecanim();
                hero.PropertiesAttakLock = true;

                hero.thisT.LookAt(unit.getPosition);
                while (hero.bGrenadeEventAnim == false /*&& hero.target!=null*/ && hero.dead == false)
                {
                    yield return null;
                }

                if (hero.dead || hero.isMoveEnd() == false)
                {
                    yield return null;
                    continue;
                }

                Unit currentTarget = unit;

                attak.stats[0].damageMin = hero.stats[hero.currentActiveStat].damageMin * dmgProcent;
                attak.stats[0].damageMax = hero.stats[hero.currentActiveStat].damageMax * dmgProcent;

                AttackInstance attInstance = new AttackInstance();
                attInstance.srcPropretiesAttak = attak;
                attInstance.tgtUnit = currentTarget;
                attInstance.SetAOERadius(attak.GetAOERadius());
                //attInstance.Process();

                attInstance.listEffects.Add(new FireDotEffect(CountTik, FireDot.FireInterval, (attak.stats[0].damageMin + attak.stats[0].damageMax)/2f * _GradeParam / CountTik, hero.damageType));

                GameObject obj = ObjectPoolManager.Spawn(hero.GrenadesEffect, hero.GrenadesShootPoint.position, hero.GrenadesShootPoint.rotation);  // (Transform)Instantiate(GetShootObjectT(), sp.position, sp.rotation);

                ShootObject shootObj = obj.GetComponent<ShootObject>();
                shootObj.Shoot(attInstance, hero.GrenadesShootPoint);

                //Debug.LogWarning("CD 1:" + cd);

                hero.PropertiesAttakLock = false;

                float time = attak.GetCooldown()/* - hero.GrenadesAnimDelay*/;
                while (time > 0)
                {
                    yield return null;
                    time -= Time.deltaTime;
                    Status.Cooldown = time;
                    if (eventUpdateStatus != null)
                        eventUpdateStatus(this, Status);
                }
                //yield return new WaitForSeconds(attak.GetCooldown() - hero.GrenadesAnimDelay);
            }
            else
                yield return null;

        }
    }

    public void SetListener(int id, EventHandler<eventPropertiesArgs> eventUpdate)
    {
        Status.id = id;
        eventUpdateStatus += eventUpdate;
    }

    public void RemovListener(EventHandler<eventPropertiesArgs> eventUpdate)
    {
        eventUpdateStatus -= eventUpdate;
        Status.id = -1;
    }
    public eventPropertiesArgs getStatus()
    {
        return Status;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return String.Format("Пулемётчик начинает кидать во врагов Напалм-гранату поджигающие врага нанося {0}(<color=#19c62e>+{2}</color>)% дополнительного урона за {1}с",
                Math.Round(FireDmgMultiplier * 100f, 2), FireDuration,
                Math.Round(FireDmgMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, FireDmgMultiplier);
    }
}