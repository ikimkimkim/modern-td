﻿using UnityEngine;
using System.Collections;
using TDTK;

public class BleedingRiflemanProperties : basePropertiesGrade, INameAndIcon
{
    private float dmgMultiplier,duration, countTik;

    public string getName { get { return "Реж по живому"; } }
    public string getUrlIcon { get { return "EffectIcon/BleedingRiflemanProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += Unit_myOutAttakInstanceFirstE;
        return true;
    }

    private void Unit_myOutAttakInstanceFirstE(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;

        if (instance.tgtUnit.EffectsManager.isActiveEffect(IDEffect.BleedingDot)==false)
        {
            instance.listEffects.Add(new BleedingDotEffect(countTik, BleedingDot.Interval, instance.damage * _GradeParam / countTik, instance.srcUnit.DamageType()));
            new TextOverlay(instance.srcUnit.getPosition, "Bleeding!", new Color(1f, 0.5f, 0f, 1f), false);
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= Unit_myOutAttakInstanceFirstE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Если на цели нет кровотечения, пулемётчик накладывает кровотечение на {0} сек. с увеличенным на {1}% уроном.",
           duration, dmgMultiplier*100f);
    }

    public override void SetProperties(object[] param)
    {
        dmgMultiplier = (float)param[0] / 100f;
        duration = (float)param[1];
        countTik = duration / BleedingDot.Interval;
        _GradeParam = dmgMultiplier;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Если на цели нет кровотечения, пулемётчик накладывает кровотечение на {0} сек. с увеличенным на {1}(<color=#19c62e>+{2}</color>)% уроном.",
               duration, System.Math.Round(dmgMultiplier * 100f,2),
               System.Math.Round(dmgMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmgMultiplier);
    }
}