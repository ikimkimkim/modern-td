﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class MoveAndShootHeroProperties : baseProperties
{

    public float Procent;

    private bool bApply;

    UnitHero hero;
    Coroutine process;

    public override bool Apply(Unit unit)
    {
        if (bApply)
            return false;

        if (unit is UnitHero == false)
            return false;

        bApply = true;
        hero = unit as UnitHero;
        hero.CanMoveAndShoot = true;
        process = hero.StartCoroutine(processUpdated());

        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply == false)
            return false;

        hero.StopCoroutine(process);
        hero.CanMoveAndShoot = false;

        hero = null;
        bApply = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Может стрелять на бегу, но при этом скорость атаки снижается до {0}%",
            Math.Round(Procent * 100f));
    }


    public override void SetProperties(object[] param)
    {
        bApply = false;
        Procent = (float)(Convert.ToDouble(param[0]) / 100);
    }


    bool active;
    private IEnumerator processUpdated()
    {
        active = false;
        while (bApply && hero!=null && hero.dead==false)
        {
            if(active==false)
            {
                if (hero.isMoveEnd()==false)
                {
                    hero.stats[hero.currentActiveStat].cooldown /= Procent;
                    active = true;
                }
            }
            else
            {
                if (hero.isMoveEnd())
                {
                    hero.stats[hero.currentActiveStat].cooldown *= Procent;
                    active = false;
                }
            }            

            yield return new WaitForSeconds(0.2f);
        }
    }
}
