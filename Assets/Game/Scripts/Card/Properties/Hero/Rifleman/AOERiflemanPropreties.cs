﻿using UnityEngine;
using System.Collections;
using System;

public class AOERiflemanPropreties : AOE, INameAndIcon
{
    public string getName { get { return "Отправляйтесь в ад"; } }
    public string getUrlIcon { get { return "EffectIcon/AOERifleman.png"; } }

    public override string GetTextInfo()
    {
        return string.Format("Автоатаки пулемётчика наносят урон по площади с радиусом {0}", AOERange);
    }


    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Автоатаки пулемётчика наносят урон по площади с радиусом {0}(<color=#19c62e>+{1}</color>)",
                AOERange, Math.Round(AOERange * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }
}
