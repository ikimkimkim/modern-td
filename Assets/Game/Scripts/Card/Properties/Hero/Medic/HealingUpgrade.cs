﻿using UnityEngine;
using System.Collections;
using TDTK;

public class HealingUpgrade : basePropertiesGrade, INameAndIcon
{
    private const int IDHealing = 26;

    private bool bApply = false;
    public float addRange, addHP;
    Healing heal;

    public string getName { get { return "Мобилизация"; } }
    public string getUrlIcon { get { return "EffectIcon/HealingUpgrade.png"; } }

    public override bool Apply(Unit unit)
    {
        if (unit.IsHero)
        {
            CardProperties healP = unit.GetUnitHero.Properties.Find(i => i.ID == IDHealing);
            if (healP != null)
            {
                heal = (Healing)healP.properties;
                heal.Range *= (addRange + 1f);
                heal.HealPoint *= (_GradeParam + 1f);
                bApply = true;
            }
        }
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply == true && heal != null)
        {
            heal.Range /= (addRange + 1f);
            heal.HealPoint /= (_GradeParam + 1f);
            bApply = false;
            heal = null;
        }
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Усиление эффекта восстановления здоровья увеличено на {0}%. Увеличивается радиус в котором восстанавливает здоровье.",
            addHP * 100f);
    }

    public override void SetProperties(object[] param)
    {
        bApply = false;
        addHP = (float)param[0] / 100f;
        addRange = (float)param[1] / 100f;
        _GradeParam = addHP;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Усиление эффекта восстановления здоровья увеличено на {0}(<color=#19c62e>+{1}</color>)%. Увеличивается радиус в котором восстанавливает здоровье.",
              System.Math.Round(addHP * 100f, 2),
              System.Math.Round(addHP * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, addHP);
    }
}
