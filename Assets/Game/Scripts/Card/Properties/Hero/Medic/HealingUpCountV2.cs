﻿using UnityEngine;
using System.Collections;
using TDTK;

public class HealingUpCountV2 : HealingUpCount
{
    public override string getName { get { return "Длинный рикошет"; } }
    public override string getUrlIcon { get { return "EffectIcon/HealingRicochetV2.png"; } }

    public override bool Apply(Unit unit)
    {
        if (unit.IsHero)
        {
            CardProperties healP = unit.GetUnitHero.Properties.Find(i => i.ID == IDHealing);
            if (healP != null)
            {
                heal = (Healing)healP.properties;
                heal.CountHeal += addCount;
                heal.ProcentHealNext2Unit = _GradeParam;
            }
        }
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (heal != null)
        {
            heal.CountHeal -= addCount;
            heal.ProcentHealNext2Unit = 0.25f;
            heal = null;
        }
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Увеличение количества рикошетов до трёх. Третьему пациенту восстанавливается здоровье в размере {0}% от изначального значения лечения.",
            25);
    }

    public override void SetProperties(object[] param)
    {
        base.SetProperties(param);
        _GradeParam = 0.25f;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Увеличение количества рикошетов до трёх. Третьему пациенту восстанавливается здоровье в размере {0}(<color=#19c62e>+{1}</color>)% от изначального значения лечения.",
                        25, System.Math.Round(25f * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, 0.25f);
    }

}
