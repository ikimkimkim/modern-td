﻿using UnityEngine;
using System.Collections;
using TDTK;

public class ResurrectionHeroProperties : basePropertiesGrade, INameAndIcon
{
    
    public string getName { get { return "Реанимация"; } }
    public string getUrlIcon { get { return "EffectIcon/ResurrectionHero.png"; } }

    private float delayTime=1.5f,lastRes, hpPercent,cooldown,range;
    private Vector3 positionDead;
    private Unit rootUnit;
    private Card card;
    public override bool Apply(Unit unit)
    {
        rootUnit = unit;
        lastRes = Time.time - _GradeParam - 1000;
        UnitHero.unitHeroDead += UnitHero_unitHeroDead;
        return true;
    }

    private void UnitHero_unitHeroDead(object sender, System.EventArgs e)
    {
        if (sender is FakeUnitHero)
            return;

        UnitHero hero = (UnitHero)sender;

        if(Time.time>lastRes + _GradeParam && Vector3.Distance(hero.getPosition,rootUnit.getPosition)<=range)
        {
            positionDead = hero.getPosition;
            card = hero.MyCard;
            rootUnit.StartCoroutine(Resurrection(hero));
            lastRes = Time.time;
        }
    }

    public override bool Cancel(Unit unit)
    {
        rootUnit = null;
        UnitHero.unitHeroDead -= UnitHero_unitHeroDead;
        return true;
    }

    IEnumerator Resurrection(UnitHero unit)
    {
        MyLog.LogWarning("Resurrection " + unit.name + " t:" + delayTime+ " level:" + card.LevelTower);
        yield return new WaitForSeconds(delayTime);
        UnitHero hero = UIHeroSpawn.SpawnHero(unit.instanceID, card, positionDead/* rootUnit.thisT.position + rootUnit.thisT.forward * unit.SizeMyCollider*/, card.LevelTower-1);
        hero.HP = hero.fullHP * hpPercent;

        yield break;
    }


    public override string GetTextInfo()
    {
        return string.Format("Раз в {1}с  может реанимировать умерающего рядом союзника. Здоровье реанимированного равно {0}% от максимального.",
            hpPercent*100f,cooldown);
    }

    public override void SetProperties(object[] param)
    {
        hpPercent = (float)param[0]/100f;
        cooldown = (float)param[1];
        range = (float)param[2];
        _GradeParam = cooldown;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Раз в {1}(<color=#19c62e>-{2}</color>)с  может реанимировать умерающего рядом союзника. Здоровье реанимированного равно {0}% от максимального.",
                hpPercent * 100f, cooldown,
                System.Math.Round(cooldown * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, cooldown);
    }
}
