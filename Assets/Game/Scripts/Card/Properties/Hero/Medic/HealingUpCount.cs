﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class HealingUpCount : basePropertiesGrade, INameAndIcon
{
    protected const int IDHealing = 26;

    public int addCount;
    protected Healing heal;

    public virtual string getName { get { return "Рикошет"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/HealingRicochet.png"; } }

    public override bool Apply(Unit unit)
    {
        if (unit.IsHero)
        {
            CardProperties healP = unit.GetUnitHero.Properties.Find(i=>i.ID==IDHealing);
            if(healP!=null)
            {
                heal = (Healing) healP.properties;
                heal.CountHeal += addCount;
                heal.ProcentHealNext1Unit = _GradeParam;
            }
        }
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (heal!=null)
        {
            heal.CountHeal -= addCount;
            heal.ProcentHealNext1Unit = 0.5f;
            heal = null;
        }
        return true;
    }

    public override string GetTextInfo()
    {
        return String.Format("Восстанавливает союзнику здоровье. Если рядом с целью находится другой юнит, то ему восстанавливается здоровье в размере {0}% от изначального значения лечения",
                    50);
    }

    public override void SetProperties(object[] param)
    {
        addCount = Convert.ToInt32(param[0]);
        _GradeParam = 0.5f;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return String.Format("Восстанавливает союзнику здоровье. Если рядом с целью находится другой юнит, то ему восстанавливается здоровье в размере {0}(<color=#19c62e>+{1}</color>)% от изначального значения лечения",
                        50, Math.Round(50f * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, 0.5f);
    }

}
