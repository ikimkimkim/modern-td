﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class AyraRegenHPProperties : basePropertiesGrade, INameAndIcon,IRangeActionProperties
{
    private bool bApply = false;
    private float Range;
    private float HPforSec;

    private UnitHero root;
    private Coroutine ayraCor;

    public string getName { get { return "Целительное поле"; } }
    public string getUrlIcon { get { return "EffectIcon/AyraRegenHP.png"; } }

    public float getRangeAction
    {
        get
        {
            return Range;
        }
    }

    public override bool Apply(Unit unit)
    {
        if (bApply)
            return false;
        if (unit is UnitHero == false)
            return false;

        root = unit as UnitHero;
        ayraCor = root.StartCoroutine(Ayra());

        bApply = true;
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply == false)
            return false;

        bApply = false;
        root.StopCoroutine(ayraCor);

        return true;
    }

    private IEnumerator Ayra()
    {
        float delta = 0.5f;
        List<UnitHero> tgtList;
        while (bApply)
        {
            tgtList = UIHeroSpawn.instance.ActiveHeroList.FindAll(i => i != root && Vector3.Distance(i.getPosition, root.getPosition) <= Range
                && i.HP<i.fullHP);

            for (int i = 0; i < tgtList.Count; i++)
            {
                tgtList[i].AddHP(tgtList[i].fullHP * _GradeParam * delta);
            }

            yield return new WaitForSeconds(delta);
        }

        yield return null;
    }


    public override string GetTextInfo()
    {
        return string.Format("У медика появляется аура постоянно восстанавливающая {0}% здоровья союзников в ридиусе {1}",
            HPforSec*100f, Range);
    }

    public override void SetProperties(object[] param)
    {
        bApply = false;
        Range = (float)param[0];
        HPforSec = (float)param[1]/100f;
        _GradeParam = HPforSec;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("У медика появляется аура постоянно восстанавливающая {0}(<color=#19c62e>+{2}</color>)% здоровья союзников в ридиусе {1}",
              System.Math.Round(HPforSec * 100f,2), Range,
              System.Math.Round(HPforSec * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, HPforSec);
    }
}
