﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class HealingUpRange : baseProperties
{
    private const int IDHealing = 26;

    public float addRange;
    Healing heal;

    public override bool Apply(Unit unit)
    {
        if (unit.IsHero)
        {
            CardProperties healP = unit.GetUnitHero.Properties.Find(i => i.ID == IDHealing);
            if (healP != null)
            {
                heal = (Healing)healP.properties;
                heal.Range *= addRange;
            }
        }
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (heal != null)
        {
            heal.Range /= addRange;
            heal = null;
        }
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Радиус лечения увеличен на {0}%",
            addRange*100f);
    }

    public override void SetProperties(object[] param)
    {
        addRange =(float) Convert.ToDouble(param[0])/100f;
    }

}
