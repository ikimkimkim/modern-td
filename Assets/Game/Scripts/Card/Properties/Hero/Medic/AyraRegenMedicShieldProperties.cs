﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class AyraRegenMedicShieldProperties : basePropertiesGrade, INameAndIcon
{
    private bool bApply = false;
    private float Range;
    private float Procent;

    private UnitHero root;
    private Coroutine ayraCor;

    public string getName    { get { return "Защитное поле"; } }
    public string getUrlIcon { get { return "EffectIcon/AyraRegenMedicShield.png"; } }

    public override bool Apply(Unit unit)
    {
        if (bApply)
            return false;
        if (unit is UnitHero == false)
            return false;

        root = unit as UnitHero;
        ayraCor = root.StartCoroutine(Ayra());

        bApply = true;
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply == false)
            return false;

        bApply = false;
        root.StopCoroutine(ayraCor);
        return true;
    }

    private IEnumerator Ayra()
    {
        float delta = 0.5f;
        List<UnitHero> tgtList;
        while (bApply)
        {
            tgtList = UIHeroSpawn.instance.ActiveHeroList.FindAll(i => i != root && Vector3.Distance(i.getPosition, root.getPosition) <= Range 
                && i.EffectsManager.isActiveEffect(IDEffect.ShieldBuff) && i.EffectsManager.isActiveEffect(IDEffect.RegenShildBuff));

            for (int i = 0; i < tgtList.Count; i++)
            {
                tgtList[i].EffectsManager.AddEffect(root, new RegenShieldMedicBuffEffect(ShieldMedicBuffEffect.IDShield, Range, _GradeParam));
            }

            yield return new WaitForSeconds(delta);
        }

        yield return null;
    }


    public override string GetTextInfo()
    {
        return string.Format("Вокруг медика появляется поле радиусом {1}, постоянно {0}% восстанавливающее щит. При условии, что у союзников есть хотя-бы 1 ед. прочности щита",
            Procent*100f, Range);
    }

    public override void SetProperties(object[] param)
    {
        bApply = false;
        Range = (float)param[0];
        Procent = (float)param[1]/100f;
        _GradeParam = Procent;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Вокруг медика появляется поле радиусом {2}, постоянно {0}(<color=#19c62e>+{1}</color>)% восстанавливающее щит. При условии, что у союзников есть хотя-бы 1 ед. прочности щита",
                System.Math.Round(Procent * 100f,2),
                System.Math.Round(Procent * _multiplierLevel * level * 100f, 2), Range);
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, Procent);
    }
}
