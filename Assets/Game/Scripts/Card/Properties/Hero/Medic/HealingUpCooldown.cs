﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class HealingUpCooldown : baseProperties
{
    private const int IDHealing = 26;

    private bool bApply = false;
    public float cooldown;
    Healing heal;
    
    public override bool Apply(Unit unit)
    {
        if (unit.IsHero)
        {
            CardProperties healP = unit.GetUnitHero.Properties.Find(i => i.ID == IDHealing);
            if (healP != null)
            {
                heal = (Healing)healP.properties;
                heal.Cooldown *=(1 + cooldown);
                bApply = true;
            }
        }
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply == true && heal != null)
        {
            heal.Cooldown  /= (1 + cooldown);
            bApply = false;
            heal = null;
        }
        return true;
    }

    public override string GetTextInfo()
    {
        return "Cкорость лечения увеличена на " + cooldown*100f + "%";
    }

    public override void SetProperties(object[] param)
    {
        cooldown =(float)Convert.ToDouble(param[0]);
        //cooldown = 1f / cooldown;
    }

}
