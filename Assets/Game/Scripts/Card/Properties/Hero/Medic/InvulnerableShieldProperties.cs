﻿using UnityEngine;
using System.Collections;
using TDTK;

public class InvulnerableShieldProperties : basePropertiesGrade, INameAndIcon
{
    public string getName { get { return "Последний шанс"; } }
    public string getUrlIcon { get { return "EffectIcon/InvulnerableShield.png"; } }

    private float timeShield, hpProcentCritical, range;
    private Unit rootUnit;

    public override bool Apply(Unit unit)
    {
        rootUnit = unit;
        UnitHero.onDamagedE += UnitHero_onDamagedE;
        return true;
    }

    private void UnitHero_onDamagedE(Unit unit, float damag = 0)
    {
        if (unit is FakeUnitHero)
            return;

        if (unit.IsHero == false || unit.HP<=0)
            return;

        if(unit.HP/unit.fullHP<=hpProcentCritical && Vector3.Distance(rootUnit.getPosition,unit.getPosition)<= range 
            && unit.EffectsManager.isActiveEffect(IDEffect.InvulnerableShieldBuff)==false)
        {
            unit.EffectsManager.AddEffect(rootUnit, new InvulnerableShieldBuffEffect(_GradeParam));
        }
    }

    public override bool Cancel(Unit unit)
    {
        rootUnit = null;
        UnitHero.onDamagedE -= UnitHero_onDamagedE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Когда здоровье союзника опускается до критического уровня медик накладывает на него щит. Щит игнорирует весь входящий урон на {0} сек.",
            timeShield);
    }

    public override void SetProperties(object[] param)
    {
        timeShield = (float)param[0];
        hpProcentCritical = (float)param[1] / 100f;
        range = 5;
        _GradeParam = timeShield;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Когда здоровье союзника опускается до критического уровня медик накладывает на него щит. Щит игнорирует весь входящий урон на {0}(<color=#19c62e>+{1}</color>) сек.",
               timeShield, System.Math.Round(timeShield * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, timeShield);
    }
}
