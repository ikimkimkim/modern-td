﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;
using System;
using System.Linq;

public class Healing : baseProperties, IPropertiesEvent
{
    public event EventHandler<eventPropertiesArgs> eventUpdateStatus;
    public eventPropertiesArgs Status;
    public float Range;
    public float HealPoint;
    public float Cooldown, BetweenАttacks;
    public int CountHeal;
    public float ProcentHealNext1Unit, ProcentHealNext2Unit;

    public bool bApply = false;
    public UnitHero unitlink;
    private Coroutine coroutineCast;

    public GameObject prefabShotObj;
    public GameObject effectHeal;


    private float MultiplierXP
    { get
        {
            switch (unitlink.MyCard.getIntRareness)
            {
                case 1:
                    return 3.5f;
                case 2:
                    return 6f;
                case 3:
                    return 10f;
            }
            return 2;
        }
    }

    private float GetProcentHealNextUnit(int index)
    {
        switch(index)
        {
            case 1:
                return ProcentHealNext1Unit;
            case 2:
                return ProcentHealNext2Unit;
            default:
                return 0.5f * index;
        }
    }


    public override bool Apply(Unit unit)
    {
        if (bApply)
            return false;
        if(unit is UnitHero==false)
        {
            Debug.LogError("Apply propertioes healing on not hero!");
            return false;
        }

        unitlink = unit as UnitHero;
        bApply = true;
        if (coroutineCast != null)
            unit.StopCoroutine(coroutineCast);
        coroutineCast = unit.StartCoroutine(Cast());
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply == false)
            return false;

        if (coroutineCast != null)
            unit.StopCoroutine(coroutineCast);
        bApply = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return  string.Format("Восстанавливает {0} здоровья за {1}с. в радиусе {2}",
            HealPoint, Cooldown, Range);
    }

    public override void SetProperties(object[] param)
    {
        bApply = false;
        Status = new eventPropertiesArgs();
        HealPoint = (float)Convert.ToDouble(param[0]);

        var bdeffect = EffectDB.Load();
        effectHeal = bdeffect[2];
        prefabShotObj = bdeffect[3];
        
        Range = (float)Convert.ToDouble(param[1]);
        Cooldown = (float)Convert.ToDouble(param[2]);
        CountHeal = Convert.ToInt32(param[3]);
        ProcentHealNext1Unit = 0.5f;
        ProcentHealNext2Unit = 0.25f;
    }

    IEnumerator Cast()
    {
        //Collider[] cols;

        List<UnitHero> patients = new List<UnitHero>();

        List<UnitHero> targetOrder = new List<UnitHero>();


        //int countFind = 0;
        //LayerMask mask = 1 << LayerManager.LayerHero();

        List<HealingUnit> listCorHeal = new List<HealingUnit>();

        bool isHealing;
        int findCountTarget = 0;

        while (bApply)
        {
            if (unitlink.isMoveEnd() == false)
            {
                yield return null;
                continue;
            }

            patients = UIHeroSpawn.instance.ActiveHeroList.FindAll(h => h != unitlink && h.HP < h.fullHP);

            if (patients.Count > 0)
            {


                var patientsInRange = patients.FindAll(h => Vector3.Distance(h.getPosition, unitlink.getPosition) <= Range).OrderBy(i => i.HP);
                if (patientsInRange.Count() == 0)
                {
                    unitlink.PropertiesAttakLock = false;
                    unitlink.healing = false;
                    yield return null;
                    continue;
                }

                unitlink.PropertiesAttakLock = true;
                unitlink.healing = true;
                while (unitlink.GetUnitHero.getWeightHealing < 0.9f)
                {
                    yield return null;
                }

                targetOrder.Clear();
                findCountTarget = 1;
                targetOrder.Add(patientsInRange.ElementAt(0));

                for (int i = 1; i < CountHeal; i++)
                {
                    var findlist = patients.FindAll(h => Vector3.Distance(h.getPosition, targetOrder[i - 1].getPosition) <= Range && targetOrder.Contains(h) == false).OrderBy(h => h.HP);
                    if (findlist.Count() > 0)
                    {
                        findCountTarget++;
                        targetOrder.Add(findlist.ElementAt(0));
                    }
                    else
                    {
                        break;
                    }
                }

                listCorHeal.Clear();
                for (int i = 0; i < targetOrder.Count; i++)
                {
                    if(listCorHeal.Count<=i)
                        listCorHeal.Add(new HealingUnit());
                    else if(listCorHeal[i].isHealing)
                    {
                        Debug.LogError("Start Healing already started!");
                    }

                    if(i==0)
                        unitlink.StartCoroutine(listCorHeal[i].HealTarget(targetOrder[i], HealPoint, unitlink, MultiplierXP, prefabShotObj, unitlink.shootPoints[0], Cooldown));
                    else
                        unitlink.StartCoroutine(listCorHeal[i].HealTarget(targetOrder[i], HealPoint * GetProcentHealNextUnit(i) , unitlink, MultiplierXP, prefabShotObj, targetOrder[i-1].GetTargetT(), Cooldown));

                }

                isHealing = true;
                while (isHealing)
                {
                    isHealing = false;
                    for (int i = 0; i < listCorHeal.Count; i++)
                    {
                        if (listCorHeal[i].isHealing == true)
                        {
                            isHealing = true;
                            break;
                        }
                    }
                    yield return null;
                }

                yield return null;
            }
            else
            {
                unitlink.PropertiesAttakLock = false;
                unitlink.healing = false;
                yield return null;
            }
        }
    }

    

    public void SetListener(int id, EventHandler<eventPropertiesArgs> eventUpdate)
    {
        Status.id = id;
        eventUpdateStatus += eventUpdate;
    }

    public void RemovListener(EventHandler<eventPropertiesArgs> eventUpdate)
    {
        eventUpdateStatus -= eventUpdate;
        Status.id = -1;
    }

    public eventPropertiesArgs getStatus()
    {
        return Status;
    }


    private class HealingUnit
    {
        public bool isHealing;
        public IEnumerator HealTarget(UnitHero healUnit,float HealPoint, UnitHero unitlink, float MultiplierXP, 
            GameObject prefabShotObj, Transform pointSpawnShotObj, float Time)
        {
            isHealing = true;
            float time = 0;
            float deltaTime = 0.2f;
            float healPoint = HealPoint / (Time / deltaTime);


            while (time <= Time)
            {
                if (unitlink.isMoveEnd() == false)
                    break;
                healUnit.AddHeal(healPoint);

                unitlink.addXP(healPoint * MultiplierXP);
                
                AttackInstance attInstance = new AttackInstance();
                attInstance.tgtUnit = healUnit;
                attInstance.isHealing = true;

                GameObject obj = ObjectPoolManager.Spawn(prefabShotObj, unitlink.shootPoints[0].position, unitlink.shootPoints[0].rotation);

                ShootObject shootObj = obj.GetComponent<ShootObject>();

                shootObj.Shoot(attInstance, pointSpawnShotObj);

                if (unitlink.isMoveEnd() == false)
                {
                    isHealing = false;
                    yield break;
                }

                time += deltaTime;
                yield return new WaitForSeconds(deltaTime);
            }
            isHealing = false;
            yield break;
        }
    }

    
}
