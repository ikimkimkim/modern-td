﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class DownNextCostProperties : baseProperties
{
    private float procent;

    public override bool Apply(Unit unit)
    {
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Уменьшение стоймости последующих вызовов.");
    }

    public override void SetProperties(object[] param)
    {
        procent = (float) param[0] / 100f;
    }
}
