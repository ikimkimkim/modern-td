﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class UpHpDmgSpeedHeroProperties : baseProperties
{
    private const int idMoveMultiplier = -2;
    private float procentHP, procentDmg, procentSpeed;
    private UnitHero rootHero;

    public override bool Apply(Unit unit)
    {
        if(unit is UnitHero)
        {
            rootHero = unit as UnitHero;
            //rootHero.unitUpdateLevel += RootHero_unitUpdateLevel;
            UpdateParam();
            return true;
        }
        return false;
    }

    private void RootHero_unitUpdateLevel(object sender, System.EventArgs e)
    {
        UpdateParam();
    }

    private void UpdateParam()
    {
        if (rootHero == null)
            return;


        rootHero.SetMoveMultiplier(idMoveMultiplier, 1f + procentSpeed);
        for (int i = 0; i < rootHero.stats.Count; i++)
        {
            rootHero.stats[i].maxHP *= (1f + procentHP);
            rootHero.stats[i].damageMax *= (1f + procentDmg);
            rootHero.stats[i].damageMin *= (1f + procentDmg);
        }
    }

    public override bool Cancel(Unit unit)
    {
        if (unit is UnitHero)
        {
            //rootHero.unitUpdateLevel -= RootHero_unitUpdateLevel;
            rootHero = null;
            return true;
        }
        return false;
    }

    public override string GetTextInfo()
    {
        return string.Format("Увеличения максимального здоровья на {0}%, урона на {1}% и скорости перемещения на {2}%",
            procentHP * 100, procentDmg * 100, procentSpeed * 100);
    }

    public override void SetProperties(object[] param)
    {
        procentHP = (float)param[0] / 100f;
        procentDmg = (float)param[1] / 100f;
        procentSpeed = (float)param[2] / 100f;
    }
}
