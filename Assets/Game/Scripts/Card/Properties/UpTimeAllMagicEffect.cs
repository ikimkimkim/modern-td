﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class UpTimeAllMagicEffect : baseProperties
{
    public float Multiplier;
    public float Procent;

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceLastE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;

        foreach (BaseEffect effect in instance.listEffects)
        {
            if(effect is FireDotEffect)
            {
                ((FireDotEffect)effect).countTik *= Multiplier;
            }
            else if (effect is PoisonDotEffect)
            {
                ((PoisonDotEffect)effect).countTik *= Multiplier;
            }
            else if (effect is SlowControlEffect)
            {
                ((SlowControlEffect)effect).TimeLife *= Multiplier;
            }
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceLastE -= ApplyInAttackInstance;

        return true;
    }

    public override string GetTextInfo()
    {
        return "Горение, замедление и отравление действуют на " + Procent + "% дольше";
    }

    public override void SetProperties(object[] param)
    {
        Procent = (float)Convert.ToDouble(param[0]);
        Multiplier = (1 + Procent / 100f);
    }
}
