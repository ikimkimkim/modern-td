﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;

public class Cooldown : basePropertiesGrade
{
    public float cooldown;

    public override bool ApplyIsSampleTower { get { return true; } }

    private bool isAb = false;

    public override bool Apply(Unit unit)
    {
        isAb = false;
        for (int i = 0; i < unit.stats.Count; i++)
        {
            unit.stats[i].cooldown *= (1f - _GradeParam);
        }
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        for (int i = 0; i < unit.stats.Count; i++)
        {
            unit.stats[i].cooldown /= (1f - _GradeParam);
        }
        return true;
    }

    public override bool Apply(Ability ability)
    {
        isAb = true;
        ability.cooldown *= (1f - _GradeParam);
        MyLog.LogWarning("ab cool:" + ability.cooldown);
        return true;
    }

    public override bool Cancel(Ability ability)
    {
        ability.cooldown /= (1f - _GradeParam);
        return true;
    }

    public override string GetTextInfo()
    {
        if (isAb)
            return string.Format("Уменьшение времени перезарядки заклинания на {0}%",cooldown * 100f);
        return string.Format("Скорострельность увеличена на {0}%",cooldown * 100f);
    }

    public override void SetProperties(object[] param)
    {
        cooldown = (float)Convert.ToDouble(param[0]) / 100f;
        _GradeParam = cooldown;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
        {
            if (isAb)
                return string.Format("Уменьшение времени перезарядки заклинания на {0}(<color=#19c62e>+{1}</color>)%",
                   Math.Round(cooldown * 100f, 2),
                   Math.Round(cooldown * _multiplierLevel * level * 100f, 2));
            return string.Format("Скорострельность увеличена на {0}(<color=#19c62e>+{1}</color>)%",
                Math.Round(cooldown * 100f, 2),
                   Math.Round(cooldown * _multiplierLevel * level * 100f, 2));
        }
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, cooldown);
    }
}
