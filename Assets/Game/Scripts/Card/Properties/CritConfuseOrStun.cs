﻿using UnityEngine;
using System.Collections;
using TDTK;

public class CritConfuseOrStun : basePropertiesGrade, INameAndIcon
{

    public float CritChance;
    public float CritdmgMultiplier;

    public string getName { get { return "Бей зевак"; } }
    public string getUrlIcon { get { return "EffectIcon/CritConfuseOrStun.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if ((instance.tgtUnit.EffectsManager.isActiveEffect(IDEffect.StunControl) || instance.tgtUnit.EffectsManager.isActiveEffect(IDEffect.DisorientationControl))
            && UnityEngine.Random.Range(0f, 1f) <= instance.GetChanceCritical(_GradeParam))
        {
            instance.SetMultiplierDmgCritical(CritdmgMultiplier);
        }
    }

    public override bool Cancel(Unit unit)
    {
            unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("{0}% шанс нанести {1}% урона по дезориентированным или оглушенным врагам",
            CritChance*100f,CritdmgMultiplier*100f);
    }

    public override void SetProperties(object[] param)
    {
        CritChance = (float)(param[0]) / 100f;
        CritdmgMultiplier = (float)(param[1]) / 100f;
        _GradeParam = CritChance;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("{0}(<color=#19c62e>+{2}</color>)% шанс нанести {1}% урона по дезориентированным или оглушенным врагам",
                CritChance * 100f, CritdmgMultiplier * 100f, 
                System.Math.Round(CritChance * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();

    }

    public override void SetLevel(int level)
    {
        SetLevel(level, CritChance);
    }
}
