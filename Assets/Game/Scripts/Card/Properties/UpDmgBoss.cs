﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class UpDmgBoss : basePropertiesGrade, INameAndIcon
{
    public float upDmg;

    public string getName { get { return "Уничтожитель боссов"; } }
    public string getUrlIcon { get { return "EffectIcon/UpDmgBoss.png"; } }
    
    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.tgtUnit is UnitCreep)
        {
            if ((instance.tgtUnit as UnitCreep).isBoss)
            {
                instance.damage *= _GradeParam;
            }
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override bool Apply(Ability ability)
    {
        ability.ApplayEffectTargetE += Ability_ApplayEffectTargetE;
        return true;
    }

    private void Ability_ApplayEffectTargetE(Ability ability, AbilityEffect effect, Vector3 position, Unit target, int idCast)
    {
        if (target is UnitCreep)
        {
            if ((target as UnitCreep).isBoss)
            {
                effect.damageMultiplierTarget = upDmg;
            }
        }
    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectTargetE -= Ability_ApplayEffectTargetE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Наносит на {0}% больше урона по боссам", _GradeParam * 100f);
    }

    public override void SetProperties(object[] param)
    {
        upDmg = (float)Convert.ToDouble(param[0]) / 100f;
        _GradeParam = 1f + upDmg;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Наносит на {0}(<color=#19c62e>+{1}</color>)% больше урона по боссам",
                (upDmg + 1f) * 100f,
               System.Math.Round(upDmg * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, upDmg);
    }
}
