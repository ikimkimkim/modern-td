﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;

//id 1
public class AOE : basePropertiesGrade
{
    public float AOERange;


    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }


    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;

        instance.SetAOERadius(_GradeParam);
    }

    public override bool Cancel(Unit unit)
    {
            unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }


    public override string GetTextInfo()
    {
        return "Снаряд наносит урон по площади, радиус " + AOERange;
    }
    

    public override void SetProperties(object[] param)
    {
        AOERange = (float) param[0];
        _GradeParam = AOERange;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Снаряд наносит урон по площади, радиус {0} (<color=#19c62e>+{1}</color>)", 
                Math.Round(AOERange, 2),
                Math.Round(AOERange * _multiplierLevel * level), 2);
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, AOERange);
    }
}
