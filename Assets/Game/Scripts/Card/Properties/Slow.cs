﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;

//id 3
public class Slow : baseProperties
{
    public float SlowDuration;
    public float SlowMultiplier;


    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;

        instance.listEffects.Add(new SlowControlEffect(SlowMultiplier, SlowDuration));
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override bool Apply(Ability ability)
    {
        ability.effect.baseEffects.Add(new SlowControlEffect(SlowMultiplier, SlowDuration));

        return true;
    }

    public override bool Cancel(Ability ability)
    {
        int index = ability.effect.baseEffects.FindIndex(i => i.IDStack == IDEffect.SlowControl);
        if (index > -1)
            ability.effect.baseEffects.RemoveAt(index);

        return true;
    }

    public override string GetTextInfo()
    {
        return "Замедляет врага на " + Mathf.Round((SlowMultiplier) *1000f)/10f + "% на "+SlowDuration+"c";
    }

    public override void SetProperties(object[] param)
    {
        SlowMultiplier = (float)(Convert.ToDouble(param[0])/100f);
        SlowDuration  = (float)Convert.ToDouble(param[1]);
    }

}
