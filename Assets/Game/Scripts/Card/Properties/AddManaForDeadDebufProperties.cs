﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class AddManaForDeadDebufProperties : basePropertiesGrade, INameAndIcon
{
    public float Power;
    public virtual string getName { get { return "Источник силы"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/AddManaForDead.png"; } }

    public override bool Apply(Unit unit)
    {
            unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;

        instance.listEffects.Add(new AddManaForDeadDebufEffect(_GradeParam, 5));
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override bool Apply(Ability ability)
    {
        ability.effect.baseEffects.Add(new AddManaForDeadDebufEffect(_GradeParam, 3));
        return true;
    }

    public override bool Cancel(Ability ability)
    {
            int id = ability.effect.baseEffects.FindIndex(i => i is AddManaForDeadDebufEffect);
            if (id > -1)
            {
                ability.effect.baseEffects.RemoveAt(id);
            }
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Добивание монстров даёт {0} маны за каждого", Power);
    }

    public override void SetProperties(object[] param)
    {
        Power = (float)Convert.ToDouble(param[0]);
        _GradeParam = Power;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Добивание монстров даёт {0}(<color=#19c62e>+{1}</color>) маны за каждого", Power,
                System.Math.Round(Power * _multiplierLevel * level,2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, Power);
    }
}
