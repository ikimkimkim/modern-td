﻿using UnityEngine;
using System.Collections;
using TDTK;
using Translation;
using System.Collections.Generic;

public class CastAbilityPoisonChanceShot : basePropertiesGrade, INameAndIcon
{
    public int idAbility;

    public float Chance;
    public float DurationMultiplier;
    public float PowerMultiplier;
    public bool bApply = false;
    public Ability ability;
    public Unit unitlink;
    public CardProperties slowProp;

    public string getName { get { return "Снаряды концентрата"; } }
    public string getUrlIcon { get { return "EffectIcon/CastAbilityMeteorit.png"; } }

    private float duration, countTik, dmg;
    Dictionary<int, List<Unit>> units;
    public override bool Apply(Unit unit)
    {
        if (bApply == false)
        {
            slowProp = unit.MyCard.Properties.Find(i => i.ID == 8);
            if (slowProp != null)
            {
                float p = ((float)slowProp.SetParam[0] / 100f) * _GradeParam;
                duration = (float)slowProp.SetParam[1] * DurationMultiplier;
                countTik = duration / PoisonDot.Interval;
                dmg = unit.stats[0].damageMax + unit.stats[0].damageMin;
                dmg = (dmg * p) / (2f * countTik);

                Debug.Log("Cast poison " + p + " " + duration);
                if (units == null)
                    units = new Dictionary<int, List<Unit>>();
                else
                    units.Clear();

                ability.StartCastE += Ability_StartCastE;
                ability.ApplayEffectE += Ability_ApplayEffectE;
                ability.EndCastE += Ability_EndCastE;
                //ability.effect.baseEffects.Add(new PoisonDotEffect(countTik, 0.5f, dmg / countTik, ability.damageType));
                unit.onShotTurretE += ShotEvent;
                unitlink = unit;
                bApply = true;
            }
            else
                Debug.LogError("Cast Poison ability properties, not data, tower not have slow properties");

        }
        return true;
    }


    public void ShotEvent(Unit target)
    {
        if (UnityEngine.Random.Range(0f, 1f) < Chance)
        {
            AbilityManager.instanceCastAbility(ability, target.thisT.position);
            new TextOverlay(unitlink.getPosition, "Magic!", Color.green, false);
        }
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        if(units.ContainsKey(idCast))
        {
            units[idCast].Clear();
        }
        else
        {
            units.Add(idCast, new List<Unit>());
        }
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> targets, List<Unit> tower, int idCast)
    {
        if (units.ContainsKey(idCast)==false)
        {
            Debug.LogError("Cast ab poison, id not find!");
            return;
        }

        for (int i = 0; i < targets.Count; i++)
        {
            if (units[idCast].Contains(targets[i]) == false)
            {
                targets[i].EffectsManager.AddEffect(unitlink, new PoisonDotEffect(countTik, PoisonDot.Interval, dmg, ability.damageType));
                units[idCast].Add(targets[i]);
            }
        }
    }

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        if (units.ContainsKey(idCast))
        {
            units[idCast].Clear();
            units.Remove(idCast);
        }
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply)
        {
            ability.StartCastE -= Ability_StartCastE;
            ability.ApplayEffectE -= Ability_ApplayEffectE;
            ability.EndCastE -= Ability_EndCastE;
            unit.onShotTurretE -= ShotEvent;
            unitlink = null;
        }
        bApply = false;
        return false;
    }

    public override string GetTextInfo()
    {
        Translator trans = TranslationEngine.Instance.Trans;
        return string.Format("С вероятностью {0}% кастует заклинание {1} при выстреле. Урон от отравления увеличена в {2}%",
            Chance * 100f, trans[ability.name], System.Math.Round(PowerMultiplier * 100f));
    }

    public override void SetProperties(object[] param)
    {

        idAbility = 4;// Convert.ToInt32(param[0]);

        var bdAbility = AbilityDB.Load();
        ability = bdAbility[idAbility].Clone();


        Chance = (float)param[0] / 100f;

        //ability.aoeRadius = (float)param[1];
        PowerMultiplier = (float)param[1] / 100f;
        DurationMultiplier = (float)param[2] / 100f;
        _GradeParam = PowerMultiplier;
        bApply = false;

    }

    public override string GetTextInfo(int level)
    {
        Translator trans = TranslationEngine.Instance.Trans;
        if (level > 0)
            return string.Format("С вероятностью {0}% кастует заклинание {1} при выстреле. Урон от отравления увеличена в {2}(<color=#19c62e>+{3}</color>)%",
                Chance * 100f, trans[ability.name],
                System.Math.Round(PowerMultiplier * 100f, 2),
                System.Math.Round(PowerMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, PowerMultiplier);
    }
}
