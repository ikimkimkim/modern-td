﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;
using System.Collections.Generic;

public class FrostbiteProperties : baseProperties
{
    public delegate float CalcChanceDelegate(float baseChance, Unit target, Unit root);
    public CalcChanceDelegate calcChance;

    public float Power;
    public float Chance;
    public float Duration;
    public float SlowPower;
    private List<Unit> unitCreeps;

    public override bool Apply(Ability ability)
    {
        calcChance = CalcChance;
        ability.StartCastE += Ability_StartCastE;
        ability.EndCastE += Ability_EndCastE;
        ability.ApplayEffectE += Ability_ApplayEffectE;
        return true;
    }

    private void ClearList()
    {
        //Debug.Log("Frostbite ClearList");
        if (unitCreeps == null)
        {
            unitCreeps = new List<Unit>();
            return;
        }

        for (int i = 0; i < unitCreeps.Count; i++)
        {
            unitCreeps[i].myInAttakInstanceFirstE -= UnitInAttack;
        }
        unitCreeps.Clear();
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect eff, Vector3 position, List<Unit> targets, List<Unit> tower, int idCast)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            if(targets[i].IsCreep && unitCreeps.Contains(targets[i])==false)
            {
                unitCreeps.Add(targets[i]);
                targets[i].myInAttakInstanceFirstE += UnitInAttack;
            }
        }
    }

    private float CalcChance(float baseChance, Unit target, Unit root)
    {
        return baseChance;
    }

    private void UnitInAttack(AttackInstance attackInstance)
    {
        if (attackInstance.isDot)
            return;

        float c = calcChance(Chance, attackInstance.tgtUnit as Unit, attackInstance.srcUnit as Unit);
        if (UnityEngine.Random.Range(0f, 1f) <= c)
        {
            attackInstance.listEffects.Add(new FrostbiteDebuffEffect(Power, SlowPower, Duration));

            new TextOverlay(attackInstance.srcUnit.getPosition, "Frostbite!", Color.blue, false);
        }
    }

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        ClearList();
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        ClearList();
    }

    public override bool Cancel(Ability ability)
    {
        ability.StartCastE -= Ability_StartCastE;
        ability.EndCastE -= Ability_EndCastE;
        ability.ApplayEffectE -= Ability_ApplayEffectE;

        ClearList();
        return true;
    }

    public override string GetTextInfo()
    {
        string text = string.Format("С шансом {0}% на врагов накладывается статус \"Обморожение\". Находясь под действием статуса враги получают на {1}% больше урона",
            Mathf.Round(Chance*100f), Mathf.Round(Power * 100f) );
        return text;
    }

    public override void SetProperties(object[] param)
    {
        Chance = (float)Convert.ToDouble(param[0])/100f;
        Power = (float)Convert.ToDouble(param[1]) / 100f;
        Duration = (float)Convert.ToDouble(param[2]);
        if (Duration <= 0)
            Debug.LogError("Frostbite propeties, duration effect less 0");

        SlowPower =(float)Convert.ToDouble(param[3]) / 100f;
    }
}
