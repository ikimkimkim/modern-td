﻿using UnityEngine;
using System.Collections;
using TDTK;

public class CooldownAndRangeUp : basePropertiesGrade, INameAndIcon
{
    public float cooldownM, rangeM;

    public bool bApply = false;

    public string getName { get { return "Магический шквал"; } }
    public string getUrlIcon { get { return "EffectIcon/CooldownAndRangeUp.png"; } }

    public override bool ApplyIsSampleTower { get { return true; } }

    public override bool Apply(Unit unit)
    {
            unit.stats[unit.currentActiveStat].cooldown /= (1 + cooldownM);
            unit.stats[unit.currentActiveStat].range *= (1 + _GradeParam);

        return true;
    }

    public override bool Cancel(Unit unit)
    {
            unit.stats[unit.currentActiveStat].cooldown *= (1 + cooldownM);
            unit.stats[unit.currentActiveStat].range /= (1 + _GradeParam);     
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("+{0}% к скорострельности, +{1}% к радиусу атаки",cooldownM*100f,rangeM*100f);
    }

    public override void SetProperties(object[] param)
    {
        cooldownM = (float)(param[0]) / 100f;
        rangeM = (float)(param[1]) / 100f;
        bApply = false;
        _GradeParam = rangeM;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("+{0}% к скорострельности, +{1}(<color=#19c62e>+{2}</color>)% к радиусу атаки",
                cooldownM * 100f, rangeM * 100f,
                System.Math.Round(rangeM * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, rangeM);
    }
}
