﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;

public class FireDot : basePropertiesGrade
{
    //Настройки [уровень_карточки, процент_урона_от_основного, время_длительности_горения]

    public int idStat;
    public UnitHero hero;

    public float FireDuration;
    public float FireDmgMultiplier;
    public const float FireInterval=0.5f;
    public float CountTik;


   // public string getName { get { return "Поджиг"; } }
   // public string getUrlIcon { get { return ""; } }

    public bool bApply;

    public override bool Apply(Unit unit)
    {
        if(bApply==false)
        {
            unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        }
        bApply = true;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;
        
        instance.listEffects.Add(new FireDotEffect(CountTik, FireInterval, instance.dmgModifier * instance.damage * _GradeParam / CountTik, instance.srcUnit.DamageType()));
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply)
        {
            unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        }
        bApply = false;
        return true;
    }

    public override bool Apply(Ability ability)
    {
        if (bApply == false)
        {
            ability.effect.baseEffects.Add(new FireDotEffect(CountTik, FireInterval, (ability.effect.damageMax + ability.effect.damageMin) / 2 * _GradeParam / CountTik, ability.damageType));
        }
        bApply = true;
        return true;
    }

    public override bool Cancel(Ability ability)
    {
        if (bApply)
        {
            int id = ability.effect.baseEffects.FindIndex(i => i is FireDotEffect);
            if (id > -1)
            {
                ability.effect.baseEffects.RemoveAt(id);
            }
        }
        bApply = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Поджигает врага и наносит {0}% дополнительного урона за {1}с",
            FireDmgMultiplier*100f, FireDuration);
    }

    public override void SetProperties(object[] param)
    {
        FireDmgMultiplier = (float)Convert.ToDouble(param[0]) / 100f;
        FireDuration = (float)Convert.ToDouble(param[1]);
        CountTik = FireDuration / FireInterval;
        _GradeParam = FireDmgMultiplier;
        bApply = false;

    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Поджигает врага и наносит {0}(<color=#19c62e>+{2}</color>)% дополнительного урона за {1}с",
                Math.Round(FireDmgMultiplier * 100f, 2), FireDuration,
                Math.Round(FireDmgMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, FireDmgMultiplier);
    }
}
