﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class SlowAllUnitDebuffAbProperties : baseProperties
{
    private float procentSlow, duration;


    public override bool Apply(Ability ability)
    {
        ability.StartCastE += Ability_StartCastE;
        return true;
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        foreach (var creep in SpawnManager.instance.ActiveCreeper)
        {
            if (creep == null || creep.dead)
                continue;

            creep.EffectsManager.AddEffect(null, new SlowStackedControlEffect(procentSlow, duration));            
        }
        
    }

    public override bool Cancel(Ability ability)
    {
        ability.StartCastE -= Ability_StartCastE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Накладывает на всех монстров снижение скорости в {0}% на {1}c. Эффект складывается до 10 раз.", procentSlow*100f, duration);
    }

    public override void SetProperties(object[] param)
    {
        procentSlow = (float) param[0] / 100f;
        duration = (float)param[1];
    }
}
