﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class OneMeteorAbProperties : basePropertiesGrade, INameAndIcon
{
    private float DmgUp;

    private int buffCountCast;
    private GameObject buffEffectObj;

    public string getName { get { return "Большой взрыв"; } }
    public string getUrlIcon { get { return "EffectIcon/OneMeteorAbProperties.png"; } }
    
    public override bool Apply(Ability ability)
    {
        buffCountCast = ability.MultiCastAbility? ability.MultiCastNumber:1;
        buffEffectObj = ability.effectObj;

        ability.effect.damageMax *= _GradeParam * buffCountCast;
        ability.effect.damageMin *= _GradeParam * buffCountCast;
        ability.MultiCastNumber = 1;
        ability.effectObj = EffectDB.Load()[14];
        ability.MultiCastAbility = false;

        return true;
    }

    public override bool Cancel(Ability ability)
    {
        ability.effect.damageMax /= _GradeParam * buffCountCast;
        ability.effect.damageMin /= _GradeParam * buffCountCast;
        ability.MultiCastNumber= buffCountCast;
        ability.effectObj= buffEffectObj;
        ability.MultiCastAbility = buffCountCast > 1;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Количество метеоров сокращается до одного с увеличенным уроном на {0}%", DmgUp * 100f);
    }

    public override void SetProperties(object[] param)
    {
        DmgUp = (float) param[0] / 100f;
        _GradeParam = DmgUp + 1f;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Количество метеоров сокращается до одного с увеличенным уроном на {0}(<color=#19c62e>+{1}</color>)%",
                DmgUp * 100f,
                System.Math.Round(DmgUp * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, DmgUp);
    }
}
