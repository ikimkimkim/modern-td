﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class MineTimerAbProperties : basePropertiesGrade, INameAndIcon
{

    private float timeLife, rangeSplash, dmgMultiplay;

    private GameObject prefabObj, prefabEffect;

    public string getName { get { return "Отложенный взрыв"; } }
    public string getUrlIcon { get { return "EffectIcon/MeteorMineTimer.png"; } }

    PropertiesAttak propAttak;

    public override bool Apply(Ability ability)
    {
        ability.ApplayEffectE += ApplyAbility;
        
        LayerMask mask = /*1 << LayerManager.LayerCreepF() |*/ 1 << LayerManager.LayerCreep();
        prefabObj = EffectDB.Load()[15];

        propAttak = new PropertiesAttak(ability.damageType,ability.effect.damageMin * _GradeParam, ability.effect.damageMax * _GradeParam, mask);
        return true;
    }

    private void ApplyAbility(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> targets, List<Unit> tower, int idCast)
    {
        AttackInstance attInstance = new AttackInstance();
        attInstance.srcPropretiesAttak = propAttak;
        attInstance.SetAOERadius(rangeSplash);

        GameObject obj = ObjectPoolManager.Spawn(prefabObj, position.RandomOffset(ability.aoeRadius/2f), Quaternion.identity); 
        ShootObject shootObj = obj.GetComponent<ShootObject>();
        shootObj.TimerBombLife = timeLife;
        shootObj.Shoot(attInstance);
    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectE -= ApplyAbility;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("После падения через {0} с. метеоры взрываются нанося {1}% урона", timeLife, dmgMultiplay*100f);
    }

    public override void SetProperties(object[] param)
    {
        dmgMultiplay = (float)param[0] / 100f;
        timeLife = (float)param[1];
        rangeSplash = (float)param[2];
        _GradeParam = dmgMultiplay;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("После падения через {0} с. метеоры взрываются нанося {1}(<color=#19c62e>+{2}</color>)% урона", timeLife,
                 dmgMultiplay * 100f,
                 System.Math.Round(dmgMultiplay * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmgMultiplay);
    }
}
