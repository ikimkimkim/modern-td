﻿using UnityEngine;
using System.Collections;
using TDTK;

public class UpCountMeteorAbProperties : basePropertiesGrade, INameAndIcon
{
    private float DmgUp;
    private float Range;

    private int buffCountCast;

    public string getName { get { return "Огненный дождь"; } }
    public string getUrlIcon { get { return "EffectIcon/UpCountMeteorAbProperties.png"; } }

    public override bool Apply(Ability ability)
    {
        buffCountCast = ability.MultiCastAbility ? ability.MultiCastNumber : 1;

        ability.effect.damageMax *= (_GradeParam + 1f) * buffCountCast / 8f;
        ability.effect.damageMin *= (_GradeParam + 1f) * buffCountCast / 8f;
        ability.MultiCastNumber = 8;
        ability.MultiCastAbility = true;
        ability.offsetRadius = ability.GetAOERadius() * Range;

        return true;
    }

    public override bool Cancel(Ability ability)
    {
        ability.effect.damageMax /= (_GradeParam + 1f) * buffCountCast / 8f;
        ability.effect.damageMin /= (_GradeParam + 1f) * buffCountCast / 8f;
        ability.MultiCastNumber = buffCountCast;

        ability.MultiCastAbility = buffCountCast > 1;
        ability.offsetRadius = 0;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Количество метеоров возрастает до 8ми. Покрываемая площадь увеличивается на {0}%, а общий урон на {1}%",
            Range * 100f, DmgUp * 100f);
    }

    public override void SetProperties(object[] param)
    {
        DmgUp = (float)param[0] / 100;
        Range = (float)param[1] / 100f;
        _GradeParam = DmgUp;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Количество метеоров возрастает до 8ми. Покрываемая площадь увеличивается на {0}%, а общий урон на {1}(<color=#19c62e>+{2}</color>)%", 
                Range * 100f, 
                System.Math.Round(DmgUp * 100f, 2),
                System.Math.Round(DmgUp * _multiplierLevel * level * 100f));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, DmgUp);
    }
}
