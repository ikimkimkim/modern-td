﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class DownCostAbilityProperties : baseProperties
{
    private float procent;

    public override bool Apply(Ability ability)
    {
        ability.cost *= (1f - procent);
        return true;
    }

    public override bool Cancel(Ability ability)
    {
        ability.cost *= (1f + procent);
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Снижения стоймости заклинания на {0}%", procent*100f);
    }

    public override void SetProperties(object[] param)
    {
        procent = (float) param[0] / 100f;
    }
}
