﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class UpDurationAbProperties : basePropertiesGrade, INameAndIcon
{
    public virtual string getName { get { return "Ледниковый период"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/UpDurationAbProperties.png"; } }
    private float procent;
    
    public override bool Apply(Ability ability)
    {
        ability.MultiCastNumber = Mathf.CeilToInt(ability.MultiCastNumber * _GradeParam);
        return true;
    }

    public override bool Cancel(Ability ability)
    {
        ability.MultiCastNumber = Mathf.FloorToInt(ability.MultiCastNumber / _GradeParam);
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Увеличивается на {0}% время действия заклинания",procent*100);
    }

    public override void SetProperties(object[] param)
    {
        procent = (float)param[0]/100f;
        _GradeParam = procent;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Увеличивается на {0}(<color=#19c62e>+{1}</color>)% время действия заклинания",
                procent * 100f,
                System.Math.Round(procent * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, procent);
    }
}
