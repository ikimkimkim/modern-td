﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class MadnessMassSlowProperties : basePropertiesGrade, INameAndIcon
{
    public string getName { get { return "Выживает сильнейший"; } }
    public string getUrlIcon { get { return "EffectIcon/MadnessMassSlowProperties.png"; } }

    private float time, dmg, regen,chance;

    bool NewCast;
    public override bool Apply(Ability ability)
    {
        NewCast = true;
        ability.ApplayEffectE += Ability_ApplayEffectE;
        ability.EndCastE += Ability_EndCastE;
        return true;
    }

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        NewCast = true;
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> creeps, List<Unit> towers, int idCast)
    {
        if (!NewCast)
            return;
        for (int i = 0; i < creeps.Count; i++)
        {
            if(Random.Range(0f,1f)<chance)
            {
                creeps[i].EffectsManager.AddEffect(null, new MadnessControlEffect(_GradeParam, dmg, regen));
            }
        }
        NewCast = false;
    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("На некоторых монстров с шансом {0}% накладывает статус \"Безумие\" на {1}c. Монстры начинают атаковать друг друга восстанавливая себе здоровье за счет своих жертв.",
            chance * 100f, time);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0]/100f;
        time = (float)param[1];
        dmg = (float)param[2] / 100f;
        regen = (float)param[3] / 100f;
        _GradeParam = time;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("На некоторых монстров с шансом {0}% накладывает статус \"Безумие\" на {1}(<color=#19c62e>+{2}</color>)c. Монстры начинают атаковать друг друга восстанавливая себе здоровье за счет своих жертв.",
                System.Math.Round(chance * 100f,2), time,
                System.Math.Round(time * _multiplierLevel * level,2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, time);
    }
}