﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockMassShowAbProperties : ShockProperties
{

    public override string getName { get { return "Морозный удар"; } }
    public override string getUrlIcon { get { return "EffectIcon/ShockMassShowAbProperties.png"; } }
    public override string GetTextInfo()
    {
        return string.Format("С шансом {0}% на монстров накладывается эффект шока на {1} с.", Chance * 100, Duration);
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("С шансом {0}% на монстров накладывается эффект шока на {1}(<color=#19c62e>+{3}</color>)с.",
                Chance * 100f, Duration, Power * 100f, 
                System.Math.Round(Duration * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }
}
