﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class MassSlowStunProperties : basePropertiesGrade, INameAndIcon
{
    public virtual string getName { get { return "Остолбенение"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/MassSlowStunProperties.png"; } }

    private List<Unit> units;
    private float chance, duration;

    public override bool Apply(Ability ability)
    {
        if (units == null)
            units = new List<Unit>();
        else
            units.Clear();
        ability.ApplayEffectTargetE += Ability_ApplayEffectTargetE;
        ability.EndCastE += Ability_EndCastE;
        return false;
    }

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        units.Clear();
    }

    private void Ability_ApplayEffectTargetE(Ability ability, AbilityEffect effect, Vector3 position, Unit target, int idCast)
    {
        if(units.Contains(target) == false)
        {
            if(Random.Range(0f,1f)<chance)
            {
                target.EffectsManager.AddEffect(null, new StunControlEffect(_GradeParam));
            }
            units.Add(target);
        }
    }

    public override bool Cancel(Ability ability)
    {
        if (units != null)
            units.Clear();
        ability.ApplayEffectTargetE -= Ability_ApplayEffectTargetE;
        ability.EndCastE -= Ability_EndCastE;
        return false;
    }

    public override string GetTextInfo()
    {
        return string.Format("С шансом {0}% некоторые монстры оглушаются на {1} с.", chance * 100, duration);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0] / 100f;
        duration = (float)param[1];
        _GradeParam = duration;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("С шансом {0}% некоторые монстры оглушаются на {1}(<color=#19c62e>+{2}</color>) с.",
               chance * 100f, duration, System.Math.Round(duration * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, duration);
    }
}
