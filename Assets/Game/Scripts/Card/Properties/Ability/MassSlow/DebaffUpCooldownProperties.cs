﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class DebaffUpCooldownProperties : baseProperties
{
    private float procent;
    private float time;

    private UpCooldownDebuffEffect debuff;

    public override bool Apply(Ability ability)
    {
        debuff = new UpCooldownDebuffEffect(procent, time);
        ability.effect.baseEffects.Add(debuff);
        return true;
    }

    public override bool Cancel(Ability ability)
    {
        ability.effect.baseEffects.Remove(debuff);
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("У всех монстров снижается скорость атаки на 50% на 5 с", procent * 100, time);
    }

    public override void SetProperties(object[] param)
    {
        procent = (float)param[0] / 100f;
        time = (float)param[1];
    }
}
