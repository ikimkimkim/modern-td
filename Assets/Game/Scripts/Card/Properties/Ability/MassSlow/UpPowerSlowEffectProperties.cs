﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class UpPowerSlowEffectProperties : basePropertiesGrade, INameAndIcon
{
    public virtual string getName { get { return "Обморожение конечностей"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/UpPowerSlowEffectProperties.png"; } }

    private float procent;
    private Slow linkProp;

    public override bool Apply(Ability ability)
    {
        CardProperties cp = ability.MyCard.Properties.Find(i => i.ID == 2);
        if (cp == null)
            return false;
        linkProp = cp.properties as Slow;
        linkProp.SlowMultiplier += _GradeParam;
        return true;
    }

    public override bool Cancel(Ability ability)
    {
        if (linkProp != null)
            linkProp.SlowMultiplier -= _GradeParam;
        linkProp = null;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Увеличивается сила замедления на {0}%", procent * 100);
    }

    public override void SetProperties(object[] param)
    {
        procent = (float)param[0]/100f;
        _GradeParam = procent;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Увеличивается сила замедления на {0}(<color=#19c62e>+{1}</color>)%",
               System.Math.Round(procent * 100f, 2) , System.Math.Round(procent * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, procent);
    }
}
