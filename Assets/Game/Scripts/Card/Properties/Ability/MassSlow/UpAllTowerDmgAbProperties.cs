﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class UpAllTowerDmgAbProperties : basePropertiesGrade, INameAndIcon
{
    private const int indexEffect = -1;
    public virtual string getName { get { return "Глыбомёт"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/UpAllTowerDmgAbProperties.png"; } }

    private float procent;

    public override bool Apply(Ability ability)
    {
        ability.ApplayEffectE += Ability_ApplayEffectE;
        return false;
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> creeps, List<Unit> towers, int idCast)
    {
        for (int i = 0; i < towers.Count; i++)
        {
            UpDmgUniqueEffect eff = new UpDmgUniqueEffect(_GradeParam, indexEffect);
            towers[i].EffectsManager.AddEffect(null, eff);
            eff.workСonditionEffect = new EffectTimeLimiteWC(ability.effectDelay * ability.MultiCastNumber);
        }
    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        return false;
    }

    public override string GetTextInfo()
    {
        return string.Format("На башни накладывается статус под дейтсвием которого их урон увеличен на {0}%. Длительность равна длительности заклинания",
            procent * 100);
    }

    public override void SetProperties(object[] param)
    {
        procent = (float)param[0] / 100f;
        _GradeParam = procent;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("На башни накладывается статус под дейтсвием которого их урон увеличен на {0}(<color=#19c62e>+{1}</color>)%. Длительность равна длительности заклинания",
                procent * 100f,
                System.Math.Round(procent * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, procent);
    }
}
