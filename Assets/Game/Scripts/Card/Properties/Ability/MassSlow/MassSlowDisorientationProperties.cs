﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassSlowDisorientationProperties : DisorientationProperties
{

    public override string getName { get { return "Затерянные во льдах"; } }
    public override string getUrlIcon { get { return "EffectIcon/MassSlowDisorientationProperties.png"; } }
    public override string GetTextInfo()
    {
        return string.Format("C шансом {0}% на монстров накладывается эффект дизориентации на {1} с.", chance * 100f, duration);
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("C шансом {0}% на монстров накладывается эффект дизориентации на {1}(<color=#19c62e>+{3}</color>) с.",
                chance * 100, duration, slow * 100,
                System.Math.Round(duration * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }
}
