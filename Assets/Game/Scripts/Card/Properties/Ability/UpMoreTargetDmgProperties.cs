﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class UpMoreTargetDmgProperties : basePropertiesGrade, INameAndIcon
{

    private bool bApply = false;
    private int LimitCountUnit;
    private float Procent;

    Vector2 deffDmg, upDmg;

    public string getName { get { return "Удар по площади"; } }
    public string getUrlIcon { get { return "EffectIcon/UpMoreTargetDmg.png"; } }

    public override bool Apply(Ability ability)
    {
        if (bApply)
            return false;
        deffDmg = new Vector2(ability.effect.damageMin, ability.effect.damageMax);
        upDmg = deffDmg * (1f + _GradeParam);
        ability.ApplayEffectE += Ability_ApplayEffectE;

        bApply = true;
        return true;
    }


    private void Ability_ApplayEffectE(Ability ability, AbilityEffect eff, Vector3 position, List<Unit> targets, List<Unit> tower, int idCast)
    {
        if (targets.Count > LimitCountUnit)
        {
            eff.damageMin = upDmg.x;
            eff.damageMin = upDmg.y;
        }
        else
        {
            eff.damageMin = deffDmg.x;
            eff.damageMin = deffDmg.y;
        }
    }

    public override bool Cancel(Ability ability)
    {
        if (bApply == false)
            return false;
        ability.ApplayEffectE -= Ability_ApplayEffectE;

        bApply = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Группам врагов (больше {1}) наносится на {0}% больше урона",
            System.Math.Round(( Procent) * 100f, 1), LimitCountUnit);
    }

    public override void SetProperties(object[] param)
    {
        bApply = false;
        Procent = (float)param[0]/100f;
        LimitCountUnit = System.Convert.ToInt32(param[1]);
        _GradeParam = Procent;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Группам врагов (больше {1}) наносится на {0}(<color=#19c62e>+{2}</color>)% больше урона",
                System.Math.Round(Procent * 100f, 1), LimitCountUnit, System.Math.Round(Procent * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, Procent);
    }
}
