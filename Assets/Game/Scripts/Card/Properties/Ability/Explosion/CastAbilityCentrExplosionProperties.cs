﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class CastAbilityCentrExplosionProperties : basePropertiesGrade, INameAndIcon
{
    public int idAbility;

    public float dmgMultiplier, splash;
    public Ability castAbility;
    public Ability abilitylink;

    public float lastTime;

    public virtual string getName { get { return "Эпицентр"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/CastAbilityCentrExplosionProperties.png"; } }


    public override bool Apply(Ability ability)
    {
        ability.StartCastE += Ability_StartCastE;
        return true;
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        castAbility.effect.damageMax = ability.effect.damageMax * _GradeParam; //DamageMax;
        castAbility.effect.damageMin = ability.effect.damageMin * _GradeParam;
        AbilityManager.instanceCastAbility(castAbility, position);
    }

    public override bool Cancel(Ability ability)
    {
        ability.StartCastE -= Ability_StartCastE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("В центре зоны поражения падает большой метеорит который наносит {0}% урона взрыва. Сплеш радиусом {1}",
            dmgMultiplier * 100f, splash);
    }

    public override void SetProperties(object[] param)
    {
        idAbility = 0;

        var bdAbility = AbilityDB.Load();
        castAbility = bdAbility[idAbility].Clone();
        castAbility.MultiCastAbility = false;

        dmgMultiplier = (float)param[0] / 100f;
        splash = (float)param[1];
        _GradeParam = dmgMultiplier;
        castAbility.aoeRadius = splash;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("В центре зоны поражения падает большой метеорит который наносит {0}(<color=#19c62e>+{2}</color>)% урона взрыва. Сплеш радиусом {1}",
                dmgMultiplier * 100f, splash, dmgMultiplier * _multiplierLevel * level * 100f);
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmgMultiplier);
    }
}
