﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class ExplosionDebaffProperties : ChanceShotDelayedActionRoketProperties
{
    public override string getName { get { return "Отложенный взрыв"; } }
    public override string getUrlIcon { get { return "EffectIcon/ExplosionDebaffProperties.png"; } }

    public override string GetTextInfo()
    {
        return string.Format("С шансом {0}% некоторые монстры взрываются изнутри через {1} сек.Взрыв наносит {2}% урона от здоровья монстра, а всем окружающим наносится урон равный {3}% от здоровья.",
            chance*100, delayTime, dmg*100,dmgAOE*100,rangeAOE);
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("С шансом {0}% некоторые монстры взрываются изнутри через {1} сек. Взрыв наносит {2}(<color=#19c62e>+{4}</color>)% урона от здоровья монстра, а всем окружающим наносится урон равный {3}% от здоровья.",
                Mathf.Round(chance * 100f), delayTime,
                System.Math.Round(dmg * 100f, 2), Mathf.Round(dmgAOE * 100f),
                System.Math.Round(dmg * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }
}