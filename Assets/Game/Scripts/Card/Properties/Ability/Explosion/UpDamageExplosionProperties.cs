﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDamageExplosionProperties : UpDmg,INameAndIcon
{

    public string getName { get { return "Большой взрыв"; } }

    public string getUrlIcon { get { return "EffectIcon/UpDamageExplosionProperties.png"; } }

    public override string GetTextInfo()
    {
        return string.Format("Урон по всей площади взрыва увеличен на {0}%", upDmg);
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Урон по всей площади взрыва увеличен на {0}(<color=#19c62e>+{1}</color>)%",
                upDmg, System.Math.Round(upDmg * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }
}
