﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class KillAndDownCooldownAbProperties : baseProperties
{
    private float time, maxTime;
    private Ability rootAb;

    public override bool Apply(Ability ability)
    {
        rootAb = ability;
        Unit.onStartDestroyedE += Unit_onDestroyedE;
        return true;
    }

    private void Unit_onDestroyedE(Unit unit)
    {
        if(unit.KillMyUnit == rootAb)
        {
            rootAb.currentCD -= time;
        }
    }

    public override bool Cancel(Ability ability)
    {
        Unit.onDestroyedE -= Unit_onDestroyedE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Если Взрывом удалось добить какого-либо монстра то перезарядка умения сокращается на {0} с. за каждого добитого монстра. Вплоть до {1} с.",
            time, maxTime);
    }

    public override void SetProperties(object[] param)
    {
        time = (float)param[0];
        maxTime = (float)param[1];
    }
}
