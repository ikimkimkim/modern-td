﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class LessHPMoreDamageProperties : basePropertiesGrade, INameAndIcon
{
    public string getName { get { return "Добивание"; } }
    public string getUrlIcon { get { return "EffectIcon/LessHPMoreDamageProperties.png"; } }

    private float hpProcent, dmgM;

    public override bool Apply(Ability ability)
    {
        ability.ApplayEffectTargetE += Ability_ApplayEffectTargetE;
        return true;
    }

    private void Ability_ApplayEffectTargetE(Ability ability, AbilityEffect effect, Vector3 position, Unit target, int idCast)
    {
        if (target.HP / target.fullHP < hpProcent)
        {
            effect.damageMultiplierTarget += _GradeParam;
        }
    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectTargetE -= Ability_ApplayEffectTargetE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Монстрам, у которых меньше {0}% ХП, наносит на {1}% больше урона",
            Mathf.Round(hpProcent * 100f), Mathf.Round(dmgM * 100f));
    }

    public override void SetProperties(object[] param)
    {
        hpProcent = (float)param[0] / 100f;
        dmgM = (float)param[1] / 100f;
        _GradeParam = dmgM;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Монстрам, у которых меньше {0}% ХП, наносит на {1}(<color=#19c62e>+{2}</color>)% больше урона",
                Mathf.Round(hpProcent * 100f), System.Math.Round(dmgM * 100f, 2),
                System.Math.Round(dmgM * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmgM);
    }
}
