﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionDisorientationProperties : DisorientationProperties
{

    public override string getName { get { return "Звуковая волна"; } }

    public override string getUrlIcon { get { return "EffectIcon/ExplosionDisorientationProperties.png"; } }

    public override string GetTextInfo()
    {
        return string.Format("C шансом {0}% на монстров накладывается эффект дизориентации на {1} с.", chance * 100f, duration);
    }
    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("C шансом {0}% на монстров накладывается эффект дизориентации на {1}(<color=#19c62e>+{2}</color>) с.",
                chance * 100, duration, duration * _multiplierLevel * level);
        else
            return GetTextInfo();
    }
}
