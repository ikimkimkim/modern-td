﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class PoisonBloodKillUnitExplosionProperties : basePropertiesGrade, INameAndIcon
{
    public virtual string getName { get { return "Полураспад"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/PoisonBloodKillUnitExplosionProperties.png"; } }

    private Ability rootAb;
    private GameObject blood;
    private float dmg, dmgPoison, durationPoison, countUnit;

    public override bool Apply(Ability ability)
    {
        NormalDeadUnit ndu = blood.GetComponent<NormalDeadUnit>();

        ndu.Procent = _GradeParam;
        ndu.ProcentDot = dmgPoison;
        ndu.TimeDot = durationPoison;
        ndu.countUnitDamage = Mathf.RoundToInt(countUnit);

        rootAb = ability;
        Unit.onStartDestroyedE += Unit_onStartDestroyedE;
        return true;
    }

    private void Unit_onStartDestroyedE(Unit unit)
    {
        if(unit.KillMyUnit == rootAb)
        {
            ManagerSpecialEffects msf = unit.GetComponent<ManagerSpecialEffects>();
            if(msf!=null)
            {
                PlayerManager._instance.StartCoroutine(retornDeffBlood(msf, msf.prefabDeadEffect));
                msf.prefabDeadEffect = blood;                
            }
        }
    }

    IEnumerator retornDeffBlood(ManagerSpecialEffects msf, GameObject blood)
    {
        while(msf.gameObject.activeSelf)
        {
            yield return null;
        }
        msf.prefabDeadEffect = blood;

        yield break;
    }

    public override bool Cancel(Ability ability)
    {
        rootAb = null;
        Unit.onStartDestroyedE -= Unit_onStartDestroyedE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Уничтоженные взрывом монстры оставляют после себя токсичную кровь, которая отравляет на {1}% урон от здоровья всех кто её коснётся. Максимум может отравить {0}",
            countUnit, dmg * 100f);
    }

    public override void SetProperties(object[] param)
    {
        dmg = (float)param[0] / 100f;
        dmgPoison = (float)param[1] / 100f;
        durationPoison = (float)param[2];
        countUnit = (float)param[3];
        _GradeParam = dmg;
        blood = DataBase<GameObject>.LoadIndex(DataBase<GameObject>.NameBase.Blood, 0);
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Уничтоженные взрывом монстры оставляют после себя токсичную кровь, которая отравляет на {1}(<color=#19c62e>+{2}</color>)% урон от здоровья всех кто её коснётся. Максимум может отравить {0}",
                countUnit, dmg * 100f,
                System.Math.Round(dmg * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmg);
    }
}
