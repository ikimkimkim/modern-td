﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionShockProperties : ShockProperties
{
    public override string getName { get { return "Ударная волна"; } }
    public override string getUrlIcon { get { return "EffectIcon/ExplosionShockProperties.png"; } }

    public override string GetTextInfo()
    {
        return string.Format("С шансом {0}% на монстров накладывается эффект шока на {1} с.", Chance * 100, Duration);
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("С шансом {0}% на монстров накладывается эффект шока на {1}(<color=#19c62e>+{2}</color>) с.",
                Chance * 100f, Duration, Duration * _multiplierLevel * level);
        else
            return GetTextInfo();

    }
}
