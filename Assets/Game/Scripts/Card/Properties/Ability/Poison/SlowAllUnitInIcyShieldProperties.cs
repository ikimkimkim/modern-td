﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;
using System.Collections.Generic;

public class SlowAllUnitInIcyShieldProperties : basePropertiesGrade, INameAndIcon {

    public float SlowDuration;
    public float SlowMultiplier;
    public bool bApply;

    public List<Unit> units;

    public string getName { get { return "Ледобой"; } }
    public string getUrlIcon { get { return "EffectIcon/SlowAllUnitInIcyShield.png"; } }

    public override bool Apply(Ability ability)
    {
        if (bApply)
            return false;

        if (units == null)
            units = new List<Unit>();
        else
        {
            for (int i = 0; i < units.Count; i++)
            {
                if(units[i]!=null)
                {
                    units[i].myOutAttakInstanceFirstE -= Unit_myOutAttakInstanceFirstE;
                }
            }
            units.Clear();
        }

        EffectManager.addEffectE += EffectManager_addEffectE;
        EffectManager.removeEffectE += EffectManager_removeEffectE;

        bApply = true;
        return true;
    }

    private void EffectManager_addEffectE(Unit unit, IDEffect typeEffect)
    {
        if (bApply && typeEffect == IDEffect.IceShieldBuff && units.Contains(unit) == false)
        {
            unit.myOutAttakInstanceFirstE += Unit_myOutAttakInstanceFirstE;
            units.Add(unit);
        }
    }

    private void EffectManager_removeEffectE(Unit unit, IDEffect typeEffect)
    {
        if (typeEffect == IDEffect.IceShieldBuff && units.Contains(unit) == true)
        {
            unit.myOutAttakInstanceFirstE -= Unit_myOutAttakInstanceFirstE;
            units.Remove(unit);
        }
    }

    private void Unit_myOutAttakInstanceFirstE(AttackInstance attackInstance)
    {
        if(bApply)
            attackInstance.listEffects.Add(new SlowControlEffect(SlowMultiplier, _GradeParam));
    }


    public override bool Cancel(Ability ability)
    {
        if (bApply == false)
            return false;
        EffectManager.addEffectE -= EffectManager_addEffectE;
        EffectManager.removeEffectE -= EffectManager_removeEffectE;

        bApply = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Автоатаки бойцов поддержки под \"Ледяным щитом\" замедляют врагов на {0}% на {1}c",
             Math.Round(SlowMultiplier * 100f), Math.Round(SlowDuration));
    }

    public override void SetProperties(object[] param)
    {
        SlowMultiplier = (float)(Convert.ToDouble(param[0]) / 100f);
        SlowDuration = (float)Convert.ToDouble(param[1]);
        _GradeParam = SlowDuration;
        bApply = false;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Автоатаки бойцов поддержки под \"Ледяным щитом\" замедляют врагов на {0}% на {1}(<color=#19c62e>+{2}</color>)c",
                 Math.Round(SlowMultiplier * 100f), SlowDuration,
                 Math.Round(SlowDuration * _multiplierLevel * level,2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, SlowDuration);
    }
}
