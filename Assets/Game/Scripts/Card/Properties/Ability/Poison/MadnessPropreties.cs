﻿using UnityEngine;
using System.Collections;
using TDTK;

public class MadnessPropreties : basePropertiesGrade, INameAndIcon
{
    public string getName { get { return "Уничтожение разума"; } }
    public string getUrlIcon { get { return "EffectIcon/Madness.png"; } }
    private float time, dmg, regen;

    public override bool Apply(Ability ability)
    {
        ability.ApplayEffectTargetE += Ability_ApplayEffectTargetE;
        return true;
    }

    private void Ability_ApplayEffectTargetE(Ability ability, AbilityEffect effect, Vector3 position, Unit target, int idCast)
    {
        if(target.EffectsManager.isActiveEffect(IDEffect.MadnessDebuff)==false)
            target.EffectsManager.AddEffect(null, new MadnessControlEffect(_GradeParam, dmg, regen));
    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectTargetE -= Ability_ApplayEffectTargetE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Облако накладывает на монстров внутри статус \"Безумие\" на {0}c. Монстры начинают атаковать друг друга восстанавливая себе здоровье за счет своих жертв.",
            time); 
    }

    public override void SetProperties(object[] param)
    {
        time = (float)param[0];
        dmg = (float)param[1] / 100f;
        regen = (float)param[2] / 100f;
        _GradeParam = time; 
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Облако накладывает на монстров внутри статус \"Безумие\" на {0}(<color=#19c62e>+{1}</color>)c. Монстры начинают атаковать друг друга восстанавливая себе здоровье за счет своих жертв.",
                time, System.Math.Round(time * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, time);
    }
}
