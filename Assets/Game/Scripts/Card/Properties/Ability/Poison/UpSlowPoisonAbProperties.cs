﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class UpSlowPoisonAbProperties : basePropertiesGrade, INameAndIcon
{
    public float UpProcent;
    private float bufProcent;

    public string getName { get { return "Нулевая видимость"; } }
    public string getUrlIcon { get { return "EffectIcon/PoisonSlowMovePropertise.png"; } }

    SlowPoisonAbPropeties basePropertise;

    public override bool Apply(Ability ability)
    {
        CardProperties properties = ability.MyCard.Properties.Find(i => i.ID == 79);
        if(properties == null)
        {
            Debug.LogError("Properties Up slow poison, not find base properties!");
            return false;
        }

        basePropertise = properties.properties as SlowPoisonAbPropeties;
        bufProcent = basePropertise.SlowMultiplier;
        basePropertise.SlowMultiplier = _GradeParam;

        return true;
    }

    public override bool Cancel(Ability ability)
    {
        basePropertise.SlowMultiplier = bufProcent;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Замедление увеличено до {0}%",
            Math.Round(UpProcent * 100f, 1).ToString());
    }

    public override void SetProperties(object[] param)
    {
        UpProcent = (float)(Convert.ToDouble(param[0]) / 100);
        _GradeParam = UpProcent;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Замедление увеличено до {0}(<color=#19c62e>+{1}</color>)%",
                Math.Round(UpProcent * 100f, 2).ToString(),
                Math.Round(UpProcent * _multiplierLevel * level * 100f, 2).ToString());
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, UpProcent);
    }
}
