﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class UpRangeAndHeavyDmgAbPoisonProperties : basePropertiesGrade, INameAndIcon
{
    private GameObject deffEffectObj;
    private GameObject NewEffectObj;
    private float DmgUp;

    private List<Unit> units;

    private Vector3 startPos;
    private float Range;

    public string getName
    {
        get { return "Взрывоопасные пары"; }
    }
    public string getUrlIcon { get { return "EffectIcon/PoisonUpRangeAndHeavyDmg.png"; } }

    public override bool Apply(Ability ability)
    {
        deffEffectObj = ability.effectObj;
        ability.effectObj = NewEffectObj;
        ability.aoeRadius *= 2f;
        Range = ability.aoeRadius;

        ability.StartCastE += Ability_StartCastE;
        ability.EndCastE += Ability_EndCastE;
        ability.ApplayEffectE += Ability_ApplayEffectE;

        return true;
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        if (units == null)
            units = new List<Unit>();
        else
            ClearDB();

        startPos = position;
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect eff, Vector3 position, List<Unit> targets, List<Unit> tower, int idCast)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            if(Vector3.Distance(targets[i].getPosition,startPos) <= Range && units.Contains(targets[i])==false)
            {
                units.Add(targets[i]);
                targets[i].myInAttakInstanceFirstE += UnitInAttakInstanceFirstE;
            }
        }
    }

    private void UnitInAttakInstanceFirstE(AttackInstance attackInstance)
    {
        if (attackInstance.getDamageType == 1)//Для тежелого
        {
            if (Vector3.Distance(attackInstance.tgtUnit.getPosition, startPos) <= Range)
            {
                (attackInstance.tgtUnit as Unit).myInAttakInstanceFirstE -= UnitInAttakInstanceFirstE;
                units.Remove((attackInstance.tgtUnit as Unit));
                return;
            }
       
            attackInstance.damage *= 1f + _GradeParam;
        }
    }

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        ClearDB();
    }

    private void ClearDB()
    {
        for (int i = 0; i < units.Count; i++)
        {
            units[i].myInAttakInstanceFirstE -= UnitInAttakInstanceFirstE;
        }
        units.Clear();
    }


    public override bool Cancel(Ability ability)
    {
        ability.effectObj = deffEffectObj;
        ability.aoeRadius /= 2f;

        ability.StartCastE -= Ability_StartCastE;
        ability.EndCastE -= Ability_EndCastE;
        ability.ApplayEffectE -= Ability_ApplayEffectE;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Рядиус облака увеличен. Помимо этого в облаке увеличен тяжёлый урон по врагам на {0}%",
            System.Math.Round(DmgUp*100f,1));
    }

    public override void SetProperties(object[] param)
    {
        NewEffectObj = EffectDB.Load()[11];
        DmgUp = (float)param[0]/100f;
        _GradeParam = DmgUp;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Рядиус облака увеличен. Помимо этого в облаке увеличен тяжёлый урон по врагам на {0}(<color=#19c62e>+{1}</color>)%",
                System.Math.Round(DmgUp * 100f, 1), System.Math.Round(DmgUp * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, DmgUp);
    }
}
