﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;
using System.Collections.Generic;

public class SlowPoisonAbPropeties : baseProperties//,INameAndIcon
{
   // public string getName { get { return "Низкая видимость"; } }

    public float SlowMultiplier;

    public List<Unit> units;
    public List<EffectToggleWC> ToggleEffects;

    public override bool Apply(Ability ability)
    {
        ability.StartCastE += Ability_StartCastE;
        ability.ApplayEffectE += Ability_ApplayEffectE;
        ability.EndCastE += Ability_EndCastE;
        return true;
    }

    public Vector3 posStart;
    public float range;
    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        range = ability.GetAOERadius();
        posStart = position;
        ClearArray();
    }

    List<Unit> newUnits;


    private void Ability_ApplayEffectE(Ability ability, AbilityEffect eff, Vector3 position, List<Unit> targets, List<Unit> tower, int idCast)
    {
        for (int i = 0; i < units.Count; i++)
        {
            if(units[i].getDistanceFromTarget(units[i],posStart)>range)
            {
                units.RemoveAt(i);
                ToggleEffects[i].Toggle = false;
                ToggleEffects.RemoveAt(i);
                i--;
            }
        }

        newUnits = targets.FindAll(i => units.Contains(i) == false);
        for (int i = 0; i < newUnits.Count; i++)
        {
            SlowControlEffect sce = new SlowControlEffect(SlowMultiplier, 0, IDEffect.SlowControlPoisonAb);
            newUnits[i].EffectsManager.AddEffect(null, sce);
            EffectToggleWC newToggle = new EffectToggleWC(true);
            sce.workСonditionEffect = newToggle;
            ToggleEffects.Add(newToggle);
            units.Add(newUnits[i]);
        }
    }   

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        ClearArray();
    }

    public override bool Cancel(Ability ability)
    {
        ability.StartCastE -= Ability_StartCastE;
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        ability.EndCastE -= Ability_EndCastE;

        ClearArray();

        return true;
    }
    
    private void ClearArray()
    {
        units.Clear();
        foreach (var t in ToggleEffects)
        {
            t.Toggle = false;
        }
        ToggleEffects.Clear();
    }

    public override string GetTextInfo()
    {
        return string.Format("Враги, находящиеся внутри облака, двигаются на {0}% медленнее",
           Math.Round(SlowMultiplier*100f,1).ToString());
    }

    public override void SetProperties(object[] param)
    {
        units = new List<Unit>();
        ToggleEffects = new List<EffectToggleWC>();
        SlowMultiplier = (float)(Convert.ToDouble(param[0]) / 100);
    }
}
