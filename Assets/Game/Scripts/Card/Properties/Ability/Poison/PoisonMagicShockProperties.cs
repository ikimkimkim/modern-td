﻿using UnityEngine;
using System.Collections;

public class PoisonMagicShockProperties : MagicShockProperties
{
    public override string getName
    {
        get { return "Психо"; }
    }

    public override string getUrlIcon { get { return "EffectIcon/PoisonMagicShock.png"; } }

    /*public override string GetTextInfo()
    {
        return "Яд накладывает статус магического шока";
    }*/
}
