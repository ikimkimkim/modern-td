﻿using UnityEngine;
using System.Collections;
using TDTK;

public class DisorientationInEndPoisonAbilityProperties : basePropertiesGrade, INameAndIcon
{
    public float duration;
    public float slow;
    public float chanceConfuse;

    public string getName { get { return "Кислородный удар"; } }
    public string getUrlIcon { get { return "EffectIcon/DisorientationInEndPoison.png"; } }

    public override bool Apply(Ability ability)
    {
        ability.EndCastE += Ability_EndCastE;
        return true;
    }

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        LayerMask mask = 1 << LayerManager.LayerCreep();
        Collider[] cols;
        cols = Physics.OverlapSphere(position, ability.GetAOERadius(), mask);
        Unit unit;
        if (cols.Length > 0)
        {
            for (int i = 0; i < cols.Length; i++)
            {
                unit = cols[i].gameObject.GetComponent<Unit>();
                unit.EffectsManager.AddEffect(null, new DisorientationControlEffect(chanceConfuse, slow, _GradeParam));
            }
        }
        new TextOverlay(position, "Confuse!", new Color(1f, 0.5f, 0, 1f), false);
    }


    public override bool Cancel(Ability ability)
    {
        ability.EndCastE -= Ability_EndCastE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Как только заканчивается время действия облака, на всех врагов находившихся в зоне действия, накладывается статус \"Дезориентация\" на {0}c",
            duration);
    }

    public override void SetProperties(object[] param)
    {
        duration = (float)param[0];
        slow = (float)param[1] / 100f;
        chanceConfuse = (float)param[2] / 100f;
        _GradeParam = duration;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Как только заканчивается время действия облака, на всех врагов находившихся в зоне действия, накладывается статус \"Дезориентация\" на {0}(<color=#19c62e>+{1}</color>)c",
                duration, System.Math.Round(duration * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, duration);
    }
}
