﻿using UnityEngine;
using System.Collections;
using TDTK;

public class UpPoisonDmgProperties : basePropertiesGrade, INameAndIcon
{
    private float power;

    public string getName { get { return "Концентрат"; } }
    public string getUrlIcon { get { return "EffectIcon/UpPoisonDmgProperties.png"; } }

    public override bool Apply(Ability ability)
    {
        ability.effect.damageMax *= 1f + _GradeParam;
        ability.effect.damageMin *= 1f + _GradeParam;

        return true;
    }

    public override bool Cancel(Ability ability)
    {
        ability.effect.damageMax /= 1f + _GradeParam;
        ability.effect.damageMin /= 1f + _GradeParam;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Яд наносит на {0}% больше урон",
            power * 100f);
    }

    public override void SetProperties(object[] param)
    {
        power = (float)param[0]/100f;
        _GradeParam = power;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Яд наносит на {0}(<color=#19c62e>+{1}</color>)% больше урон",
               System.Math.Round(power * 100f, 2),
               System.Math.Round(power * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, power);
    }
}