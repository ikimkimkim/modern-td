﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;
using System.Collections.Generic;

public class UpDmgAndBreakPoisonAbProperties : basePropertiesGrade, INameAndIcon
{
    public float UpDmg;
    
    public List<Unit> units;

    public string getName { get { return "Токсичные пары"; } }
    public string getUrlIcon { get { return "EffectIcon/PoisonInCloudUpDmgProperties.png"; } }

    public override bool Apply(Ability ability)
    {
        ability.StartCastE += Ability_StartCastE;
        ability.ApplayEffectE += Ability_ApplayEffectE;
        ability.EndCastE += Ability_EndCastE;
        return true;
    }

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        ClearArray();
    }

    public Vector3 posStart;
    public float range;
    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        range = ability.GetAOERadius();
        posStart = position;
        ClearArray();
    }

    List<Unit> newUnits;
    private void Ability_ApplayEffectE(Ability ability, AbilityEffect eff, Vector3 position, List<Unit> targets, List<Unit> tower, int idCast)
{
        for (int i = 0; i < units.Count; i++)
        {
            if (units[i].getDistanceFromTarget(units[i], posStart) > range)
            {
                units[i].myInAttakInstanceFirstE -= AttaInstanceIn;
                if (units[i].EffectsManager.isActiveEffect(IDEffect.PoisonDot))
                {
                    units[i].EffectsManager.Stacks[(int)IDEffect.PoisonDot].ClearData();
                    units[i].EffectsManager.Stacks.Remove((int)IDEffect.PoisonDot);
                }
                units.RemoveAt(i);
                i--;
            }
        }

        newUnits = targets.FindAll(i => units.Contains(i) == false);
        for (int i = 0; i < newUnits.Count; i++)
        {
            newUnits[i].myInAttakInstanceFirstE += AttaInstanceIn;
            units.Add(newUnits[i]);
        }
    }

    private void AttaInstanceIn(AttackInstance attackInstance)
    {
        attackInstance.damage *= 1f + _GradeParam;
    }

    public override bool Cancel(Ability ability)
    {
        ability.StartCastE -= Ability_StartCastE;
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        ability.EndCastE -= Ability_EndCastE;

        ClearArray();
        return true;
    }

    private void ClearArray()
    {
        for (int i = 0; i < units.Count; i++)
        {
            units[i].myInAttakInstanceFirstE -= AttaInstanceIn;
            if (units[i].EffectsManager.isActiveEffect(IDEffect.PoisonDot))
            {
                units[i].EffectsManager.Stacks[(int)IDEffect.PoisonDot].ClearData();
                units[i].EffectsManager.Stacks.Remove((int)IDEffect.PoisonDot);
            }
        }
        units.Clear();
    }

    public override string GetTextInfo()
    {
        return string.Format("В облаке наносится на {0}% больше урон, но яд пропадает при выходе из него",
           Math.Round(UpDmg* 100f, 2).ToString());
    }

    public override void SetProperties(object[] param)
    {
        units = new List<Unit>();
        UpDmg = (float)param[0] / 100f;
        _GradeParam = UpDmg;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("В облаке наносится на {0}(<color=#19c62e>+{1}</color>)% больше урон, но яд пропадает при выходе из него",
               Math.Round(UpDmg * 100f, 2),
               Math.Round(UpDmg * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, UpDmg);
    }
}
