﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class ExplosionDeadUnitAbProperties : basePropertiesGrade, INameAndIcon
{
    private float HpDmg, AOERadius;
    private GameObject Splash;
    private LayerMask mask;

    public string getName { get { return "Интоксикация"; } }
    public string getUrlIcon { get { return "EffectIcon/ExplosionDead.png"; } }

    public override bool Apply(Ability ability)
    {
        ability.ApplayEffectE += Ability_ApplayEffectE;

        Splash = EffectDB.Load()[16];
        mask = 1 << LayerManager.LayerCreep() | 1 << LayerManager.LayerCreepF();
        ObjectPoolManager.New(Splash);

        return true;
    }



    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> targets, List<Unit> tower, int idCast)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            if (units.Contains(targets[i])==false)
            {
                targets[i].onMyStartDestroyedE += StartDestroyedE;
                units.Add(targets[i]);
            }
        }
        
    }

    private void StartDestroyedE(Unit unit)
    {
        unit.onMyStartDestroyedE -= StartDestroyedE;

        if (unit.EffectsManager.isActiveEffect(IDEffect.PoisonDot) == false)
            return;

        GameObject objSplash = ObjectPoolManager.Spawn(Splash, unit.getPosition, Quaternion.identity);
        objSplash.GetComponent<SplashSpecialEffect>().Emit(_GradeParam);


        Collider[] colsAOE = Physics.OverlapSphere(unit.getPosition, _GradeParam, mask);

        if (colsAOE.Length > 0)
        {
            for (int i = 0; i < colsAOE.Length; i++)
            {
                Unit target = colsAOE[i].GetComponent<Unit>();
                if (target != null && !target.dead)
                {
                    AttackInstance attIns = new AttackInstance();
                    attIns.tgtUnit = target;
                    attIns.damage = unit.fullHP * HpDmg;
                    attIns.SetAOERadius(_GradeParam);
                    attIns.ApplyDistance(Vector3.Distance(unit.getPosition, target.getPosition));
                    target.ApplyEffect(attIns);

                }
            }
        }

        units.Remove(unit);
    }

    List<Unit> units;
    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Монстры убитые под статусом яда взрываются при смерти нанося {0}% урона от здоровья всем в радиусе {1}",
            HpDmg * 100f, AOERadius);
    }


    public override void SetProperties(object[] param)
    {
        HpDmg = (float)param[0]/100f;
        AOERadius = (float)param[1];
        _GradeParam = AOERadius;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Монстры убитые под статусом яда взрываются при смерти нанося {0}% урона от здоровья всем в радиусе {1}(<color=#19c62e>+{2}</color>)",
                HpDmg * 100f, AOERadius, System.Math.Round(AOERadius * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, AOERadius);
    }
}
