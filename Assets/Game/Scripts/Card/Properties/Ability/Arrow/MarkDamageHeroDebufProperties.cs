﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class MarkDamageHeroDebufProperties : basePropertiesGrade, INameAndIcon
{
    public string getName { get { return "Вынос избранных"; } }
    public string getUrlIcon { get { return "EffectIcon/MarkDamageHeroDebuff.png"; } }

    private float chance, power, duration;
    private bool newCast = false;

    public override bool Apply(Ability ability)
    {
        newCast = true;
        ability.ApplayEffectE += Ability_ApplayEffectE;
        ability.EndCastE += Ability_EndCastE;
        return true;
    }

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        newCast = true;
    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        ability.EndCastE -= Ability_EndCastE;
        newCast = false;
        return true;
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> creeps, List<Unit> towers, int idCast)
    {
        if (!newCast)
            return;

        for (int i = 0; i < creeps.Count; i++)
        {
            if (Random.Range(0f, 1f) <= chance)
            {
                creeps[i].EffectsManager.AddEffect(null, new MarkDamageHeroEffect(power, _GradeParam));
            }
        }
        newCast = false;
    }

    public override string GetTextInfo()
    {
        return string.Format("С шансом {0}% на некоторых врагов вешается метка. Атака поддержки по этому монстру нанесёт {1}% доп. урона каждым выстрелом. Время действия {2}с.",
            chance * 100, power * 100, duration);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0] / 100f;
        power = (float)param[1] / 100f;
        duration = (float)param[2];
        _GradeParam = duration;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("С шансом {0}% на некоторых врагов вешается метка. Атака поддержки по этому монстру нанесёт {1}% доп. урона каждым выстрелом. Время действия {2}(<color=#19c62e>+{3}</color>)с.",
                chance * 100, power * 100, duration,
                System.Math.Round(duration * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, duration);
    }
}
