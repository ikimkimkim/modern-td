﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowPoisonDebaffProperties : PoisonDot, INameAndIcon
{

    public string getName { get { return "Отравленные стрелы"; } }
    public string getUrlIcon { get { return "EffectIcon/ArrowPoisonDebaffProperties.png"; } }
}
