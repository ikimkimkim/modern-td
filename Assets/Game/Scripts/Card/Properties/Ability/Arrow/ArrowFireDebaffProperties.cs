﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowFireDebaffProperties : FireDot, INameAndIcon
{

    public string getName { get { return "Огненные стрелы"; } }
    public string getUrlIcon { get { return "EffectIcon/ArrowFireDebaffProperties.png"; } }

}
