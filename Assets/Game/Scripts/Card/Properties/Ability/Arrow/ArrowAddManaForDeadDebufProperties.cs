﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowAddManaForDeadDebufProperties : AddManaForDeadDebufProperties
{

    public override string getName { get { return "Мерлинова проказа"; } }
    public override string getUrlIcon { get { return "EffectIcon/ArrowAddManaForDeadDebufProperties.png"; } }
}
