﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowUpCostDebuffProperties : UpCostDebuffProperties
{

    public override string getName { get { return "Робин Гуд"; } }
    public override string getUrlIcon { get { return "EffectIcon/ArrowUpCostDebuffProperties.png"; } }

}
