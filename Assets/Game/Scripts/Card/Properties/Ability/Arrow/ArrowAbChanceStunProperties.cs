﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowAbChanceStunProperties : StunProp
{

    public override string GetTextInfo()
    {
        return string.Format("С шансом {0}% некоторые монстры оглушаются на {1} с.", StunChance * 100, StunDuration);
    }

}
