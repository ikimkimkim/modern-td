﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowUpDmgProperties : UpDmg,INameAndIcon
{

    public string getName { get { return "Заточка наконечников"; } }
    public string getUrlIcon { get { return "EffectIcon/ArrowUpDmgProperties.png"; } }

}
