﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowIgnorArmorTypeDownDmgPropreties : IgnoreArmorDmgDownProperties
{

    public override string getName { get { return "Бронебойные стрелы"; } }
    public override string getUrlIcon { get { return "EffectIcon/ArrowIgnorArmorTypeDownDmgPropreties.png"; } }
}
