﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class ElectrifiedDebuffProperties : basePropertiesGrade, INameAndIcon
{
    public string getName { get { return "Опасный форсаж"; } }
    public string getUrlIcon { get { return "EffectIcon/ElectrifiedDebuffProperties.png"; } }

    private float speed, time, dmgP;
    private List<Unit> targets;
    private int counter;

    public override bool Apply(Ability ability)
    {
        targets = new List<Unit>();
        ability.StartCastE += Ability_StartCastE;
        ability.ApplayEffectE += Ability_ApplayEffectE;
        ability.EndCastE += Ability_EndCastE;
        return true;
    }

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        float dmg = (ability.effect.damageMin + ability.effect.damageMax) / 2f * _GradeParam;
        for (int i = 0; i < targets.Count; i++)
        {
            targets[i].EffectsManager.AddEffect(null, new ElectrifiedEffect(speed, time, dmg));
        }
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> creeps, List<Unit> towers, int idCast)
    {
        if(counter>0)
        {
            for (int i = 0; i < targets.Count; i++)
            {
                if(creeps.Contains(targets[i])==false)
                {
                    targets.RemoveAt(i);
                    i--;
                }
            }
        }
        else
        {
            targets.AddRange(creeps);
        }
        counter++;
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        counter = 0;
        targets.Clear();
    }

    public override bool Cancel(Ability ability)
    {
        ability.StartCastE -= Ability_StartCastE;
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        ability.EndCastE -= Ability_EndCastE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Скорость передвижения монстров получивших урон от всех трёх волн увеличивается на {0}% на {1}с.Но по истечении времени в каждого из них прилетает молния, наносящая {2}% урона.",
            speed * 100, time, dmgP * 100);
    }

    public override void SetProperties(object[] param)
    {
        speed = (float)param[0] / 100f;
        time = (float)param[1];
        dmgP = (float)param[2] / 100f;
        _GradeParam = dmgP;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Скорость передвижения монстров получивших урон от всех трёх волн увеличивается на {0}% на {1}с.Но по истечении времени в каждого из них прилетает молния, наносящая {2}(<color=#19c62e>+{3}</color>)% урона.",
                speed * 100, time, dmgP * 100,
                System.Math.Round(dmgP * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmgP);
    }
}
