﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class LightningUpDmgWaveProperties : basePropertiesGrade, INameAndIcon
{
    public string getName { get { return "Нарастающая мощь"; } }
    public string getUrlIcon { get { return "EffectIcon/LightningUpDmgWaveProperties.png"; } }

    private float dmg, conter;

    public override bool Apply(Ability ability)
    {
        ability.StartCastE += Ability_StartCastE;
        ability.ApplayEffectE += Ability_ApplayEffectE;
        return true;
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> creeps, List<Unit> towers, int idCast)
    {
        effect.damageMax *= (1f + _GradeParam * conter);
        effect.damageMin *= (1f + _GradeParam * conter);
        conter++;
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        conter = 0;
    }

    public override bool Cancel(Ability ability)
    {
        ability.StartCastE -= Ability_StartCastE;
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Каждая следующая волна наносит на {0}% больше урона чем предыдущая", dmg * 100);
    }

    public override void SetProperties(object[] param)
    {
        dmg = (float)param[0] / 100f;
        _GradeParam = dmg;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Каждая следующая волна наносит на {0}(<color=#19c62e>+{1}</color>)% больше урона чем предыдущая",
              System.Math.Round(dmg * 100f,2), 
              System.Math.Round(dmg * _multiplierLevel * level*100f,2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmg);
    }
}
