﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class LightningStunAndUpTimeProperties : basePropertiesGrade, INameAndIcon
{
    public string getName { get { return "Симбиоз"; } }
    public string getUrlIcon { get { return "EffectIcon/LightningStunAndUpTimeProperties.png"; } }

    LightningStunProperties baseProp;
    LightningStunProperties.ApplyStunTargetsHandler handlerBuff;

    private float timeUp, maxTimeUp;

    public override bool Apply(Ability ability)
    {
        baseProp = ability.MyCard.Properties.Find(i => i.properties is LightningStunProperties).properties as LightningStunProperties;
        handlerBuff = baseProp.applyStunTargets;
        baseProp.applyStunTargets = _ApplyStunTarget;
        return true;
    }

    private void _ApplyStunTarget(AbilityEffect effect, float durationStun)
    {
        effect.effects.Add(new StunAndUpTimeControlEffect(durationStun,timeUp, _GradeParam));        
    }

    public override bool Cancel(Ability ability)
    {
        baseProp.applyStunTargets = handlerBuff;
        baseProp = null;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Энергетические башни своими автоатакой продлевают время оглушения на {0} с. Максимум на {1} с.",
            timeUp, maxTimeUp);
    }

    public override void SetProperties(object[] param)
    {
        timeUp = (float)param[0];
        maxTimeUp = (float)param[1];
        _GradeParam = maxTimeUp;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Энергетические башни своими автоатакой продлевают время оглушения на {0} с. Максимум на {1}(<color=#19c62e>+{2}</color>) с.",
                timeUp, maxTimeUp, System.Math.Round(maxTimeUp * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, maxTimeUp);
    }
}
