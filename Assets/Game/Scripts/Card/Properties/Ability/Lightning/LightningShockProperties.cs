﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class LightningShockProperties : ShockProperties
{

    public override string getName { get { return "Избранное шокирование"; } }
    public override string getUrlIcon { get { return "EffectIcon/LightningShockProperties.png"; } }
    private float counter;

    public override bool Apply(Ability ability)
    {
        counter = 0;
        ability.ApplayEffectE += Ability_ApplayEffectE;
        ability.StartCastE += Ability_StartCastE;
        return true;
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        counter = 0;
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> creeps, List<Unit> towers, int idCast)
    {
        for (int i = 0; i < creeps.Count; i++)
        {
            if (UnityEngine.Random.Range(0f, 1f) <= Chance)
                creeps[i].EffectsManager.AddEffect(null, new ShockDebufEffect(Power, _GradeParam));
        }
        counter++;
    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        ability.StartCastE -= Ability_StartCastE;
        return true;
    }
}
