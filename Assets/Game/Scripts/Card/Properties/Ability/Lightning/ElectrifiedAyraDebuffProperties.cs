﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class ElectrifiedAyraDebuffProperties : basePropertiesGrade, INameAndIcon
{
    public string getName { get { return "Наэлектризованность"; } }
    public string getUrlIcon { get { return "EffectIcon/ElectrifiedAyraDebuffProperties.png"; } }

    private float range, time, dmgP;
    private List<Unit> targets;
    private int counter;

    public override bool Apply(Ability ability)
    {
        targets = new List<Unit>();
        ability.StartCastE += Ability_StartCastE;
        ability.ApplayEffectE += Ability_ApplayEffectE;
        ability.EndCastE += Ability_EndCastE;
        return true;
    }

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            targets[i].EffectsManager.AddEffect(null, new ElectrifiedAyraEffect(_GradeParam, time, dmgP));
        }
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> creeps, List<Unit> towers, int idCast)
    {
        if (counter > 0)
        {
            for (int i = 0; i < targets.Count; i++)
            {
                if (creeps.Contains(targets[i]) == false)
                {
                    targets.RemoveAt(i);
                    i--;
                }
            }
        }
        else
        {
            targets.AddRange(creeps);
        }
        counter++;
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        counter = 0;
        targets.Clear();
    }

    public override bool Cancel(Ability ability)
    {
        ability.StartCastE -= Ability_StartCastE;
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        ability.EndCastE -= Ability_EndCastE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Если монстра поразили все три волны, то следующие {0} сек. каждую секнуду всем монстрам в радиусе {1} наносится Энергетический урон.",
            time, range);
    }

    public override void SetProperties(object[] param)
    {
        time = (float)param[0];
        range = (float)param[1];
        dmgP = (float)param[2] / 100f;
        _GradeParam = range;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Если монстра поразили все три волны, то следующие {0} сек. каждую секнуду всем монстрам в радиусе {1}(<color=#19c62e>+{2}</color>) наносится Энергетический урон.",
                time, range, System.Math.Round(range * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, range);
    }
}