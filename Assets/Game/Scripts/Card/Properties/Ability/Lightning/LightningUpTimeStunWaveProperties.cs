﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class LightningUpTimeStunWaveProperties : basePropertiesGrade, INameAndIcon
{
    public string getName { get { return "Глушилка"; } }
    public string getUrlIcon { get { return "EffectIcon/LightningUpTimeStunWaveProperties.png"; } }

    private float timeUp, counter;

    public override bool Apply(Ability ability)
    {
        ability.StartCastE += Ability_StartCastE;
        ability.ApplayEffectE += Ability_ApplayEffectE;
        return true;
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> creeps, List<Unit> towers, int idCast)
    {
        if (counter != 0)
        {

            var baseEffect = effect.effects.Find(i => i is StunControlEffect);
            if (baseEffect == null)
                return;
            StunControlEffect sce = baseEffect as StunControlEffect;
            sce.TimeLife += _GradeParam * counter;
        }
        counter++;
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        counter = 0;
    }

    public override bool Cancel(Ability ability)
    {
        ability.StartCastE -= Ability_StartCastE;
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Каждая следующая волна оглушает на {0}с. дольше чем предыдущая",timeUp);
    }

    public override void SetProperties(object[] param)
    {
        timeUp = (float)param[0];
        _GradeParam = timeUp;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Каждая следующая волна оглушает на {0}(<color=#19c62e>+{1}</color>) с. дольше чем предыдущая",
              System.Math.Round(timeUp, 3),
              System.Math.Round(timeUp * _multiplierLevel * level,3));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, timeUp);
    }
}
