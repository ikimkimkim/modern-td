﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class LightningStunNextDisorientationProperties : basePropertiesGrade, INameAndIcon
{
    public string getName { get { return "Приди в себя"; } }
    public string getUrlIcon { get { return "EffectIcon/LightningStunNextDisorientationProperties.png"; } }
    
    LightningStunProperties baseProp;
    LightningStunProperties.ApplyStunTargetsHandler handlerBuff;

    private float time,slow,chance;

    public override bool Apply(Ability ability)
    {
        baseProp = ability.MyCard.Properties.Find(i => i.properties is LightningStunProperties).properties as LightningStunProperties;
        handlerBuff = baseProp.applyStunTargets;
        baseProp.applyStunTargets = _ApplyStunTarget;
        return true;
    }

    private void _ApplyStunTarget(AbilityEffect effect, float durationStun)
    {
        effect.effects.Add(new StunNextDisoriesntationControlEffect(durationStun,chance,slow, _GradeParam));        
    }
    
    public override bool Cancel(Ability ability)
    {
        baseProp.applyStunTargets = handlerBuff;
        baseProp = null;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("После окончания оглушения, враги получают статус дезориентации на {0} с.", time);
    }

    public override void SetProperties(object[] param)
    {
        time = (float)param[0];
        slow = (float)param[1]/100f;
        chance = (float)param[2] / 100f;
        _GradeParam = time;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("После окончания оглушения, враги получают статус дезориентации на {0}(<color=#19c62e>+{1}</color>) с.",
                time, System.Math.Round(time * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, time);
    }
}