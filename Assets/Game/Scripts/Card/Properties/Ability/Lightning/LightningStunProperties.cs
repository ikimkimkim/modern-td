﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class LightningStunProperties : baseProperties
{
    public float StunChance;
    public float StunDuration;

    public delegate void ApplyStunTargetsHandler(AbilityEffect effect, float durationStun);
    public ApplyStunTargetsHandler applyStunTargets;

    public override bool Apply(Ability ability)
    {
        ability.ApplayEffectE += Ability_ApplayEffectE;
        applyStunTargets = _ApplyStunTarget;
        return true;
    }

    protected void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> creeps, List<Unit> towers, int idCast)
    {
        if (UnityEngine.Random.Range(0f, 1f) <= StunChance)
        {
            if (applyStunTargets == null)
            {
                Debug.LogError("Lightning Stun Delegate is null");
                return;
            }
            applyStunTargets(effect, StunDuration);
        }
    }

    private void _ApplyStunTarget(AbilityEffect effect, float durationStun)
    {
        effect.effects.Add(new StunControlEffect(durationStun));        
    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("С шансом {0}% может оглушить всех врагов на {1} с", StunChance * 100f, StunDuration);
    }

    public override void SetProperties(object[] param)
    {
        StunChance = (float)(param[0]) / 100f;
        StunDuration = (float)(param[1]);
    }





}
