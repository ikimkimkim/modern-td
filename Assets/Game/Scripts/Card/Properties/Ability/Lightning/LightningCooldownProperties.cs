﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningCooldownProperties : Cooldown, INameAndIcon
{

    public string getName { get { return "Время пришло"; } }
    public string getUrlIcon { get { return "EffectIcon/LightningCooldownProperties.png"; } }
}
