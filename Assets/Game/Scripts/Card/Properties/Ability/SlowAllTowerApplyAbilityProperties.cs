﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class SlowAllTowerApplyAbilityProperties : basePropertiesGrade, INameAndIcon
{
    public float SlowDuration;
    public float SlowMultiplier;
    

    float StartTime, LifeTime;

    public string getName { get { return "Сотни ледяных пуль";  } }
    public string getUrlIcon { get { return "EffectIcon/SlowAllTower.png"; } }


    public override bool Apply(Ability ability)
    {
        StartTime = -_GradeParam;
        ability.StartCastE += Ability_StartCastE;

        return true;
    }

    private void AttackInstance_ProgressE(AttackInstance attack)
    {
        if (attack.srcUnit != null && attack.srcUnit is UnitTower)
        {
            if(StartTime + _GradeParam < Time.time)
                attack.listEffects.Add(new SlowControlEffect(SlowMultiplier, SlowDuration));
            else
                AttackInstance.ProgressE -= AttackInstance_ProgressE;

        }
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        StartTime = Time.time;
        AttackInstance.ProgressE += AttackInstance_ProgressE;
    }

    public override bool Cancel(Ability ability)
    {
        if (StartTime + _GradeParam >= Time.time)
            AttackInstance.ProgressE -= AttackInstance_ProgressE;
        ability.StartCastE -= Ability_StartCastE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Все башни на карте на {0} с. начинают автоатаками накладывать статус замедления ({1}% на {2}c) на противников",
            LifeTime, Math.Round(SlowMultiplier * 100), SlowDuration);
    }

    public override void SetProperties(object[] param)
    {
        SlowMultiplier = (float)(Convert.ToDouble(param[0]) / 100f);
        SlowDuration = (float)Convert.ToDouble(param[1]);
        LifeTime = (float)Convert.ToDouble(param[2]);
        _GradeParam = LifeTime;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Все башни на карте на {0}(<color=#19c62e>+{3}</color>) с. начинают автоатаками накладывать статус замедления ({1}% на {2}c) на противников",
                LifeTime, Math.Round(SlowMultiplier * 100), SlowDuration,
                Math.Round(LifeTime * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, LifeTime);
    }
}
