﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class IcyShockProperties : basePropertiesGrade, INameAndIcon
{
    private float power, maxPower, duration;

    public string getName { get { return "Нарастающее обморожение"; } }
    public string getUrlIcon { get { return "EffectIcon/IcyShockProperties.png"; } }

    public override bool Apply(Ability ability)
    {
        ability.ApplayEffectE += Ability_ApplayEffectE;
        return true;
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect eff, Vector3 position, List<Unit> targets, List<Unit> tower, int idCast)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            if(targets[i].EffectsManager.isActiveEffect(IDEffect.IcyShock)==false)
            {
                targets[i].EffectsManager.AddEffect(null, new IcyShockDebuffEffect(power, maxPower, _GradeParam));
            }
        }
    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("С каждой секундой под замедлением, урон по данному монстру увеличивается на {0}%. Действует {2} с. Увеличение урона складывается до {1}%",
            power*100f, maxPower*100f, duration);
    }


    public override void SetProperties(object[] param)
    {
        power = (float)param[0]/100f;
        maxPower = (float)param[1]/100f;
        duration = (float)param[2];
        _GradeParam = duration;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("С каждой секундой под замедлением, урон по данному монстру увеличивается на {0}%. Действует {2}(<color=#19c62e>+{3}</color>) с. Увеличение урона складывается до {1}%",
                power * 100f, maxPower * 100f, duration,
                System.Math.Round(duration * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, duration);
    }
}
