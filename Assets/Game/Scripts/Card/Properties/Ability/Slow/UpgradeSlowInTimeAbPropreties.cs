﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class UpgradeSlowInTimeAbPropreties : basePropertiesGrade, INameAndIcon
{
    private float power;

    public string getName { get { return "Промораживание"; } }
    public string getUrlIcon { get { return "EffectIcon/UpgradeSlowInTimeAbPropreties.png"; } }

    private float StartTime;
    Slow slowPropertiesLink;
    float baseSlowM;

    public override bool Apply(Ability ability)
    {
        slowPropertiesLink = (Slow)ability.MyCard.Properties.Find(i => i.ID == 2).properties;
        ability.StartCastE += Ability_StartCastE;
        ability.ApplayEffectE += Ability_ApplayEffectE;
        ability.EndCastE += Ability_EndCastE;

        return true;
    }

    float sec;
    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> targets, List<Unit> tower, int idCast)
    {
        sec = Mathf.Floor(Time.time - StartTime);
        if(sec>0)
            slowPropertiesLink.SlowMultiplier = baseSlowM + sec * _GradeParam;
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        baseSlowM = slowPropertiesLink.SlowMultiplier;
        StartTime = Time.time;
    }

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        slowPropertiesLink.SlowMultiplier = baseSlowM;
    }

    public override bool Cancel(Ability ability)
    {
        slowPropertiesLink = null;
        ability.StartCastE -= Ability_StartCastE;
        ability.ApplayEffectE -= Ability_ApplayEffectE;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("С каждой секундой действия заклинания, замедления увеличивается на {0}%.",
            power*100f);
    }


    public override void SetProperties(object[] param)
    {
        power = (float)param[0]/100f;
        _GradeParam = power;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("С каждой секундой действия заклинания, замедления увеличивается на {0}(<color=#19c62e>+{1}</color>)%.",
                power * 100f, 
                System.Math.Round(power * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, power);
    }
}
