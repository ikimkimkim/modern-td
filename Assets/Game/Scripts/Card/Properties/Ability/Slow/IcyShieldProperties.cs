﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;
using System.Collections.Generic;

public class IcyShieldProperties : basePropertiesGrade, INameAndIcon
{
    public float Power;

    public string getName { get { return "Ледяной щит"; } }
    public string getUrlIcon { get { return "EffectIcon/IcyShieldProperties.png"; } }
    
    public override bool Apply(Ability ability)
    {
        ability.ApplayEffectE += Ability_ApplayEffect;
        return true;
    }

    private void Ability_ApplayEffect(Ability ability, AbilityEffect eff, Vector3 position, List<Unit> units, List<Unit> tower, int idCast)
    {
        LayerMask mask = 1 << LayerManager.LayerHero();
        float radius = ability.requireTargetSelection ? ability.GetAOERadius() : 1000f;
        Collider[] cols = Physics.OverlapSphere(position, radius, mask);


        if (cols.Length > 0)
        {
            for (int i = 0; i < cols.Length; i++)
            {
                UnitHero unit = cols[i].gameObject.GetComponent<UnitHero>();
                if(unit == null)
                {
                    Debug.LogWarning("IceShield find not hero unit");
                    continue;
                }

                unit.EffectsManager.AddEffect(null, new ShieldIcyBuffEffect(60, unit.fullHP * _GradeParam));

            }
        }

    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectE -= Ability_ApplayEffect;
        return true;
    }

    public override string GetTextInfo()
    {
        string text = string.Format("Всем бойцам поддержки, оказавшимся в зоне действия заклинания, накладывается \"Ледяной щит\". Щит даёт доп. щит (вторая полоска) равный {0}% от здоровья юнита. \"Ледяной щит\" уменьшается со временем.", 
            Power * 100f);
        return text;
    }

    public override void SetProperties(object[] param)
    {
        Power = (float)Convert.ToDouble(param[0]) / 100f;
        _GradeParam = Power;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Всем бойцам поддержки, оказавшимся в зоне действия заклинания, накладывается \"Ледяной щит\". Щит даёт доп. щит (вторая полоска) равный {0}(<color=#19c62e>+{1}</color>)% от здоровья юнита. \"Ледяной щит\" уменьшается со временем.",
                Power * 100f, 
                System.Math.Round(Power * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, Power);
    }
}
