﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class IcingProperties : basePropertiesGrade, INameAndIcon
{
    private float chance, timeStun, timeUpDmg, upDmg;

    public string getName
    {
        get { return "Ледяная статуя"; }
    }
    public string getUrlIcon { get { return "EffectIcon/Icing.png"; } }

    Dictionary<int, List<Unit>> castList;
    public override bool Apply(Ability ability)
    {
        if (castList == null)
            castList = new Dictionary<int, List<Unit>>();
        else
            castList.Clear();

        ability.StartCastE += Ability_StartCastE;
        ability.ApplayEffectE += Ability_ApplayEffectE;
        ability.EndCastE += Ability_EndCastE;

        return true;
    }


    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        if (castList.ContainsKey(idCast))
        {
            castList[idCast].Clear();
        }
        else
        {
            castList.Add(idCast, new List<Unit>());
        }
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> targets, List<Unit> tower, int idCast)
    {
        if (castList.ContainsKey(idCast) == false)
        {
            Debug.LogError("Cast ab and icing effect, id not find!");
            return;
        }

        for (int i = 0; i < targets.Count; i++)
        {
            if (castList[idCast].Contains(targets[i]) == false)
            {
                if(Random.value <= _GradeParam)
                {
                    targets[i].EffectsManager.AddEffect(null, new IcingControlEffect(timeStun, timeUpDmg, upDmg));
                }

                castList[idCast].Add(targets[i]);
            }
        }
    }

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        if (castList.ContainsKey(idCast))
        {
            castList[idCast].Clear();
            castList.Remove(idCast);
        }
    }
    public override bool Cancel(Ability ability)
    {
        ability.StartCastE -= Ability_StartCastE;
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        ability.EndCastE -= Ability_EndCastE;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Замедленный противник с шансом {0}% может заледенеть и превратиться в ледяную статую на {1} с. После разморозки урон по цели увеличен на {2}%",
            chance * 100f, timeStun, upDmg * 100f);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0]/100f;
        timeStun = (float)param[1];
        timeUpDmg = (float)param[2];
        upDmg = (float)param[3]/100f;
        _GradeParam = chance;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Замедленный противник с шансом {0}(<color=#19c62e>+{3}</color>)% может заледенеть и превратиться в ледяную статую на {1} с. После разморозки урон по цели увеличен на {2}%",
                System.Math.Round(chance * 100f, 2), timeStun, upDmg * 100f,
                System.Math.Round(chance * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, chance);
    }
}
