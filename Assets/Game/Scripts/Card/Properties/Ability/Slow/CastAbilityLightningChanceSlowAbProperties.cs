﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class CastAbilityLightningChanceSlowAbProperties : basePropertiesGrade, INameAndIcon
{
    public string getName { get { return "Ледные молнии"; } }
    public string getUrlIcon { get { return "EffectIcon/LightningChanceSlow.png"; } }

    private float lastActiveTime;
    private float chance, timeStun, cooldown;

    public Ability Lightning;
    public override bool Apply(Ability ability)
    {
        ability.ApplayEffectE += Ability_ApplayEffectE;
        Lightning.ApplayEffectE += Lightning_ApplayEffectE;

        return true;
    }

    private void Lightning_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> targets, List<Unit> tower, int idCast)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            if(Random.value<chance)
            {
                targets[i].EffectsManager.AddEffect(null, new StunControlEffect(_GradeParam));
            }
        }
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> targets, List<Unit> tower, int idCast)
    {
        if(targets.Count>0 && Time.time > lastActiveTime + cooldown)
        {
            Unit target = targets[Random.Range(0, targets.Count)];

            AbilityManager.instanceCastAbility(Lightning, target.thisT.position);

            lastActiveTime = Time.time;
        }
    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        Lightning.ApplayEffectE -= Lightning_ApplayEffectE;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("По противникам, находящимся в зоне действия заклинания, ударяют молнии с {0}% шансом оглушения на {1}c. Молнии выбирают цель рандомно.",
            chance * 100f,timeStun);
    }

    public override void SetProperties(object[] param)
    {
        var bdAbility = AbilityDB.Load();
        Lightning = bdAbility[9].Clone();
        Lightning.effect.damageMax = 1; 
        Lightning.effect.damageMin = 1;

        cooldown = (float)param[0];
        chance = (float)param[1]/100f;
        timeStun = (float)param[2];
        _GradeParam = timeStun;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("По противникам, находящимся в зоне действия заклинания, ударяют молнии с {0}% шансом оглушения на {1}(<color=#19c62e>+{2}</color>)c. Молнии выбирают цель рандомно.",
                chance * 100f, timeStun, 
                System.Math.Round(timeStun * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, timeStun);
    }
}
