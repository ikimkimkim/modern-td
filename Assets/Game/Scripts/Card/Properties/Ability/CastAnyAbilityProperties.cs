﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class CastAnyAbilityProperties : baseProperties
{
    private float chance;
    private const float delay = 2f;
    private Coroutine corUpdate;

    public override bool Apply(Ability ability)
    {
        ability.StartCastE += Ability_StartCastE;
        return true;
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        if (Random.Range(0f, 1f) <= chance)
        {
            corUpdate = PlayerManager._instance.StartCoroutine(CastDelayAbility(position));
        }
    }

    public override bool Cancel(Ability ability)
    {
        if (corUpdate != null)
            PlayerManager._instance.StopCoroutine(corUpdate);
        ability.StartCastE -= Ability_StartCastE;
        return true;
    }



    IEnumerator CastDelayAbility(Vector3 position)
    {
        yield return new WaitForSeconds(delay);       
        AbilityManager.CastAnyAbility(position);
        yield break;
    }

    public override string GetTextInfo()
    {
        return string.Format("С шансом {0}% после произносения заклинания будет применено любое доступное заклинание в туже точку.",chance*100f);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0] / 100f;
    }
}
