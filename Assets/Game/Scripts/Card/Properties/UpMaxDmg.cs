﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;

public class UpMaxDmg : baseProperties
{
    public float upMaxDmg;


    public override bool ApplyIsSampleTower { get { return true; } }
    public override bool Apply(Unit unit)
    {
        unit.stats[0].damageMax = unit.stats[0].damageMax * (1f + upMaxDmg / 100f);
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        unit.stats[0].damageMax = unit.MyCard.stats[0].damageMax;
        return true;
    }
    public override bool Apply(Ability ability)
    {
        ability.effect.damageMax = ability.effect.damageMax * (1f + upMaxDmg / 100f);
        return true;
    }

    public override bool Cancel(Ability ability)
    {
        AbilityManager.SetAbilityDmg(ability, ability.MyCard);
        return true;
    }
    public override string GetTextInfo()
    {
        return "Максимальный урон увеличен на " + upMaxDmg + "%";
    }

    public override void SetProperties(object[] param)
    {
        upMaxDmg = (float)Convert.ToDouble(param[0]);
    }
}