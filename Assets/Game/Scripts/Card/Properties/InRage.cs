﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class InRage : basePropertiesGrade, IPropertiesEvent,INameAndIcon
{
    private const int idMoveM = -3;

    public event EventHandler<eventPropertiesArgs> eventUpdateStatus;
    public eventPropertiesArgs Status;

    private bool bApply = false;
    public float SpeedMultiplier;
    public float CooldownMultiplier;
    public int idStatus;

    public float duration;
    public float cooldown;

    private Unit unitLink;
    private Coroutine coroutineRage;

    public GameObject effectEventRage, effectRage;
     
    public virtual string getName { get { return "Ярость"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/RageProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        if (bApply == false)
        {
            unitLink = unit;
            bApply = true;

            if (unit.IsHero)
            {
                var listEffect = EffectDB.Load();
                effectEventRage = listEffect[0];
                effectRage = listEffect[1];

                GameObject go = GameObject.Instantiate(effectRage);
                go.transform.position = unit.thisT.position;
                go.transform.SetParent(unit.thisT);
                go.SetActive(false);
                effectRage = go;
                if (coroutineRage != null)
                    unit.StopCoroutine(coroutineRage);
                coroutineRage = unit.StartCoroutine(enumeratorRage());
            }
        }
        MyLog.Log("Apply inRage");
        return true;
    }


    public override bool Cancel(Unit unit)
    {
        if (coroutineRage != null)
        {
            unit.StopCoroutine(coroutineRage);
        }

        bApply = false;
        unitLink = null;
        idStatus = -1;
        return false;
    }

    public override string GetTextInfo()
    {
        return String.Format("Каждые {0}с на {1}с скорость бега увеличивается на {2} %, а скорость атаки на {3} % ",
            cooldown,duration,SpeedMultiplier*100f,CooldownMultiplier*100f);
    }

    public override void SetProperties(object[] param)
    {
        Status = new eventPropertiesArgs();
        bApply = false;
        SpeedMultiplier = (float)(param[0]);
        CooldownMultiplier = (float)(param[1]);
        cooldown = (float)(param[2]);
        duration = (float)(param[3]);
        _GradeParam = duration;
        //MyLog.Log("Set Rage duration:" + duration);
    }



    private IEnumerator enumeratorRage()
    {
        while(bApply)
        {
            if (unitLink.GetUnitHero.PropertiesAttakLock == true || unitLink.target == null || unitLink.stunned || unitLink.IsInConstruction() || !unitLink.targetInLOS || GameControl.GetGameState() != _GameState.Play)
            {
                yield return null;
            }
            else
            {
                GameObject go = GameObject.Instantiate(effectEventRage);
                go.transform.position = unitLink.thisT.position;
                go.transform.SetParent(unitLink.thisT);
                heroApply(unitLink.GetUnitHero);
                effectRage.SetActive(true);
                MyLog.Log("Rage duration:" + _GradeParam);
                float time = _GradeParam;
                Status.Cooldown = 0;
                while (time > 0)
                {
                    yield return null;
                    time -= Time.deltaTime;
                    Status.TimeDuration = time;
                    if (eventUpdateStatus != null)
                        eventUpdateStatus(this, Status);
                }

                effectRage.SetActive(false);
                heroCancel(unitLink.GetUnitHero);
                GameObject.Destroy(go);

                time = cooldown;
                Status.TimeDuration = 0;
                while (time > 0)
                {
                    yield return null;
                    time -= Time.deltaTime;
                    Status.Cooldown = time;
                    if (eventUpdateStatus != null)
                        eventUpdateStatus(this, Status);
                }
            }
        }
    }
    private void heroApply(UnitHero hero)
    {
        idStatus = hero.currentActiveStat;
        hero.SetMoveMultiplier(idMoveM, SpeedMultiplier);
        hero.stats[idStatus].cooldown *= CooldownMultiplier;
        hero.unitUpdateXP += Hero_unitUpdateXP;
    }
    private void heroCancel(UnitHero hero)
    {
        if (bApply && idStatus == hero.currentActiveStat)
        {
            hero.RemoveMoveMultiplier(idMoveM);
            hero.stats[idStatus].cooldown /= CooldownMultiplier;
        }
        hero.unitUpdateXP -= Hero_unitUpdateXP;
    }

    private void Hero_unitUpdateXP(object sender, System.EventArgs e)
    {
        if (bApply && idStatus != unitLink.currentActiveStat)
        {
            heroCancel(unitLink.GetUnitHero);
            idStatus = unitLink.currentActiveStat;
            heroApply(unitLink.GetUnitHero);
        }
    }

    public void SetListener(int id, EventHandler<eventPropertiesArgs> eventUpdate)
    {
        Status.id = id;
        eventUpdateStatus += eventUpdate;
    }

    public void RemovListener(EventHandler<eventPropertiesArgs> eventUpdate)
    {
        eventUpdateStatus -= eventUpdate;
        Status.id = -1;
    }
    public eventPropertiesArgs getStatus()
    {
        return Status;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return String.Format("Каждые {0}с на {1}(<color=#19c62e>+{4}</color>)с скорость бега увеличивается на {2} %, а скорость атаки на {3} % ",
                cooldown, Math.Round(duration, 2), SpeedMultiplier * 100f, CooldownMultiplier * 100f,
                Math.Round(duration * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, duration);
    }
}
