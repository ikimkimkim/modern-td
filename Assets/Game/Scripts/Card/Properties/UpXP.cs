﻿using UnityEngine;
using System.Collections;
using TDTK;

public class UpXP : baseProperties
{
    public float XpMultiple;

    public override bool Apply(Unit unit)
    {
        unit.XPIncrement += XpMultiple;
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        unit.XPIncrement -= XpMultiple;
        return false;
    }

    public override string GetTextInfo()
    {
        return "Получаемый в бою опыт увеличен на " + Mathf.Ceil(XpMultiple * 100f) + "%";
    }

    public override void SetProperties(object[] param)
    {
        XpMultiple = (float)(System.Convert.ToDouble(param[0]) / 100f);
    }
}
