﻿using UnityEngine;
using System.Collections;
using TDTK;

public class BleedingDot : basePropertiesGrade, INameAndIcon
{
    public float chance;
    public float Duration;
    public float DmgMultiplier;
    public const float Interval = 0.5f;
    public float CountTik;

    public string getName { get { return "Разрывные пули"; } }
    public string getUrlIcon { get { return "EffectIcon/BleedingDot.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;

        if (Random.Range(0f, 1f) < chance * instance.MultiplierChanceBleeding)
        {
            instance.listEffects.Add(new BleedingDotEffect(CountTik, Interval, instance.damage * _GradeParam / CountTik, instance.srcUnit.DamageType()));
            new TextOverlay(instance.srcUnit.getPosition, "Bleeding!", new Color(1f, 0.5f, 0f, 1f), false);
        }
    }
    public override bool Cancel(Unit unit)
    {
            unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;            
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("C {0}% шансом накладывает на цель \"кровотечение\", которое наносит {1}% урона башни за {2} секунд",
            chance * 100f,DmgMultiplier * 100f,Duration);
    }

    public override void SetProperties(object[] param)
    {
        DmgMultiplier = (float)param[0] / 100f;
        Duration = (float)param[1];
        chance = (float)param[2] / 100f;
        CountTik = Duration / Interval;
        _GradeParam = DmgMultiplier;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("C {0}% шансом накладывает на цель \"кровотечение\", которое наносит {1}(<color=#19c62e>+{3}</color>)% урона башни за {2} секунд",
                chance * 100f, DmgMultiplier * 100f, Duration, 
                System.Math.Round(DmgMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, DmgMultiplier);
    }
}