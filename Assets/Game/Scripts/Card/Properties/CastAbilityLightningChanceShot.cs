﻿using UnityEngine;
using System.Collections;
using TDTK;
using Translation;

public class CastAbilityLightningChanceShot : basePropertiesGrade, INameAndIcon
{
    public delegate void OnCastAbilityDelegate(Ability ability, Unit root, Unit target);
    public static event OnCastAbilityDelegate OnCastAbilityE;

    public int idAbility;

    public float Chance;
    public float dmgMultiplier;
    public float ChanceEffect, PowerEffect, DurationEffect, RangeEffect=3;
    public Ability ability;
    public Unit unitlink;
    
    public virtual string getName { get { return "Разряд"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/CastAbilityLightning.png"; } }

    public override bool Apply(Unit unit)
    {
            unit.onShotTurretE += ShotEvent;
            unitlink = unit;
        return true;
    }

    public void ShotEvent(Unit target)
    {
        if (UnityEngine.Random.Range(0f, 1f) < Chance)
        {
            CastAbility(target);
        }
    }

    protected virtual void CastAbility(Unit target)
    {
        ability.effect.damageMax = unitlink.stats[unitlink.currentActiveStat].damageMax * _GradeParam; //DamageMax;
        ability.effect.damageMin = unitlink.stats[unitlink.currentActiveStat].damageMin * _GradeParam;
        ability.effect.baseEffects.Clear();
        if (UnityEngine.Random.Range(0f, 1f) < ChanceEffect)
            ability.effect.baseEffects.Add(new ShockDebufEffect(PowerEffect, DurationEffect));

        AbilityManager.instanceCastAbility(ability, target.thisT.position);
        new TextOverlay(unitlink.getPosition, "Magic!", new Color(1f, 0.5f, 0, 1f), false);

        CallCastAbilityEvent(target);
    }

    protected virtual void CallCastAbilityEvent(Unit target)
    {
        if (OnCastAbilityE != null)
            OnCastAbilityE(ability, unitlink, target);
    }

    public override bool Cancel(Unit unit)
    {
            unit.onShotTurretE -= ShotEvent;
            unitlink = null;
        return false;
    }

    public override string GetTextInfo()
    {
        Translator trans = TranslationEngine.Instance.Trans;
        return string.Format("{0}% шанс вызывать \"{1}\" при выстреле. \"{1}\" наносит {2}% урона башни и накладывает \"Шок\" с вероятностью {3}%",
            Chance * 100f, trans[ability.name], dmgMultiplier * 100f, ChanceEffect * 100f);
    }

    public override void SetProperties(object[] param)
    {
        idAbility = 9;

        var bdAbility = AbilityDB.Load();
        ability = bdAbility[idAbility].Clone();

        Chance = (float)param[0] / 100f;
        dmgMultiplier = (float)param[1] / 100f;
        ChanceEffect = (float)param[2] / 100f;
        PowerEffect = (float)param[3] / 100f;
        DurationEffect = (float)param[4];
        _GradeParam = dmgMultiplier;
        ability.aoeRadius = RangeEffect;
    }

    public override string GetTextInfo(int level)
    {
        Translator trans = TranslationEngine.Instance.Trans;
        if (level > 0)
            return string.Format("{0}% шанс вызывать \"{1}\" при выстреле. \"{1}\" наносит {2}(<color=#19c62e>+{4}</color>)% урона башни и накладывает \"Шок\" с вероятностью {3}%",
                Chance * 100f, trans[ability.name], 
                System.Math.Round(dmgMultiplier * 100f, 2), ChanceEffect * 100f,
                System.Math.Round(dmgMultiplier * _multiplierLevel * level*100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmgMultiplier);
    }
}
