﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class CritSlow : baseProperties
{
    public float CritChance;
    public float CritdmgMultiplier;

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;
        if (instance.tgtUnit.EffectsManager.isActiveEffect(IDEffect.SlowControl) &&
            UnityEngine.Random.Range(0f, 1f) <= instance.GetChanceCritical(CritChance))
        {
            instance.SetMultiplierDmgCritical(CritdmgMultiplier);
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return String.Format("{0}% нанести {1}% урона по замедленному врагу",
           CritChance * 100f,CritdmgMultiplier * 100f);
    }

    public override void SetProperties(object[] param)
    {
        CritChance = (float)Convert.ToDouble(param[0]) / 100f;
        CritdmgMultiplier = (float)Convert.ToDouble(param[1]) / 100f;
    }
}
