﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;
using System.Collections.Generic;

public class Coomplect : baseProperties
{
    
    public int CoomplectID;
    public List<CardProperties> CoomplectProperties;

    public override bool Apply(Unit unit)
    {
        if (CoomplectProperties == null)
            Debug.LogError("Coomplect properties is null");

        List<CardProperties> newListPrioritet;
        for (int prioritet = 0; prioritet < 5; prioritet++)
        {
            newListPrioritet = CoomplectProperties.FindAll(i => i.MyPrioritet == (CardProperties.Prioritet)prioritet);
            foreach (CardProperties propertie in newListPrioritet)
            {
                propertie.Apply(unit);
                propertie.Refresh(true);
            }
        }

        return true;
    }

    public override bool Cancel(Unit unit)
    {
        List<CardProperties> newListPrioritet;
        for (int prioritet = 0; prioritet < 5; prioritet++)
        {
            newListPrioritet = CoomplectProperties.FindAll(i => i.MyPrioritet == (CardProperties.Prioritet)prioritet);
            foreach (CardProperties propertie in newListPrioritet)
            {
                propertie.Refresh(false);
            }
        }
        return true;
    }

    public override bool Apply(Ability ability)
    {
        MyLog.LogError("Apply properties in ability not can!");
        return false;
    }

    public override bool Cancel(Ability ability)
    {
        MyLog.LogError("Cancel properties in ability not can!");
        return false;
    }

    public override string GetTextInfo()
    {
        return CoomplectName(CoomplectID);
    }

    public override void SetProperties(object[] param)
    {
        CoomplectID = Convert.ToInt32(param[0]); 
    }


    public static string CoomplectName(int id)
    {
        switch(id)
        {
            case 1: return "Коомплект \"Яд и Огонь\"";
            case 2: return "Коомплект \"Горячие головы\"";
            case 3: return "Коомплект \"Ты не пройдешь\"";
                 
        }

        return "Coomplect Not Name:" + id;
    }

}
