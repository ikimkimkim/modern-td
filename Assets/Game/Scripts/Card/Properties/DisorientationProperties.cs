﻿using UnityEngine;
using System.Collections;
using TDTK;

public class DisorientationProperties : basePropertiesGrade, INameAndIcon//,IPropertiesGrade
{
    public float chance;
    public float duration;
    public float slow;
    public float chanceConfuse;

    public virtual string getName { get { return "Громкая бомба"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/DisorientationProperties.png"; } }


    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;
        if (Random.Range(0f, 1f) < chance * instance.MultiplierChanceDisorientation)
        {
            instance.listEffects.Add(new DisorientationControlEffect(chanceConfuse, slow, _GradeParam));
            new TextOverlay(instance.srcUnit.getPosition, "Confuse!", new Color(1f,0.5f,0,1f), false);
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override bool Apply(Ability ability)
    {
        ability.StartCastE += Ability_StartCastE;
        return true;
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        if (Random.Range(0f, 1f) < chance)
        {
            ability.effect.effects.Add(new DisorientationControlEffect(chanceConfuse, slow, _GradeParam));
        }
    }

    public override bool Cancel(Ability ability)
    {
        ability.StartCastE -= Ability_StartCastE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("При попадании по противнику с вероятностью {0}% на {1} секунд накладывает статус \"Дезориентация\", при котором цель движется на {2}% медленнее и может временно портерять направление движения",
            chance*100,duration,slow*100);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0]/100f;
        duration = (float)param[1];
        slow = (float)param[2]/100f;
        chanceConfuse = (float)param[3] / 100f;
        _GradeParam = duration;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("При попадании по противнику с вероятностью {0}% на {1}(<color=#19c62e>+{3}</color>) секунд накладывает статус \"Дезориентация\", при котором цель движется на {2}% медленнее и может временно портерять направление движения",
                chance * 100,
                System.Math.Round(duration, 2), slow * 100,
                System.Math.Round(duration *  _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, duration);
    }
}
