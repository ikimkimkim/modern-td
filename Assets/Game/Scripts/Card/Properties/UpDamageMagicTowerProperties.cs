﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class UpDamageMagicTowerProperties : basePropertiesGrade, INameAndIcon,IRangeActionProperties
{
    public float Power;
    public float Range;
    public int Index;

    public Unit _unit;

    public string getName { get { return "Кооперация"; } }
    public string getUrlIcon { get { return "EffectIcon/UpDamageMagicTower.png"; } }

    public float getRangeAction
    {
        get
        {
            return _GradeParam;
        }
    }

    public override bool Apply(Unit unit)
    {
        if (unit.IsTower == false)
            return false;

        Index = unit.MyCard.IDTowerData;
        _unit = unit;
        UnitTower.onConstructionCompleteE += TowerBild;
        StartPropertis();

        return true;
    }

    public void TowerBild(UnitTower tower)
    {
        if (Vector3.Distance(_unit.getPosition, tower.getPosition) <= _GradeParam && tower as Unit != _unit)
        {
            tower.EffectsManager.AddEffect(_unit, new UpDmgUniqueEffect(Power, Index));
        }
        else
            MyLog.Log("UpDmg, new tower out range");
    }

    public void StartPropertis()
    {
        _unit.EffectsManager.AddEffect(_unit, new UpDmgUniqueEffect(Power, Index));

        LayerMask mask = 1 << LayerManager.LayerTower();

        Collider[] cols = Physics.OverlapSphere(_unit.thisT.position, _GradeParam, mask);
        UnitTower unit;

        foreach (Collider col in cols)
        {
            unit = col.GetComponent<UnitTower>();
            if(unit != null && unit as Unit != _unit && unit.dead == false)
                unit.EffectsManager.AddEffect(_unit, new UpDmgUniqueEffect(Power, Index));
        }        
    }

    public override bool Cancel(Unit unit)
    {
            UnitTower.onConstructionCompleteE -= TowerBild;
            _unit = null;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Увеличивает на {0}% урон себе и всем башням стоящих в радиусе {1}", //башни могут получать не более одного бонуса от каждого типа магической башни
            Power *100f, Range);
    }

    public override void SetProperties(object[] param)
    {
        Power = (float)param[0] / 100f;
        Range = (float)param[1];
        _GradeParam = Range;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Увеличивает на {0}% урон себе и всем башням стоящих в радиусе {1}(<color=#19c62e>+{2}</color>)", //башни могут получать не более одного бонуса от каждого типа магической башни
                Power * 100f, Range,
                System.Math.Round(Range * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, Range);
    }
}
