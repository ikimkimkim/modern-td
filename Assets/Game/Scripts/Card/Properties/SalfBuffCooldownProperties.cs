﻿using UnityEngine;
using System.Collections;
using TDTK;

public class SalfBuffCooldownProperties : baseProperties
{
    public float chance;
    public float Multiplier;
    public float Duration;


    float lastTimeActive;
    private Unit root;

    public override bool Apply(Unit unit)
    {
        lastTimeActive = -99;
        root = unit;
        unit.onShotTurretE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(Unit target)
    {
        if (lastTimeActive + Duration <= Time.time && Random.Range(0f, 1f) < chance)
        {
            root.EffectsManager.AddEffect(root, new CooldownBuffEffect(Multiplier, Duration));
            new TextOverlay(root.getPosition, "Rage!", Color.red, false);

            lastTimeActive = Time.time;
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.onShotTurretE -= ApplyInAttackInstance;
        root = null;
        return false;
    } 

    public override string GetTextInfo()
    {
        return string.Format("При выстреле с шансом {2}% может повысить свою скорострельность на {0}% на {1} секунд.",
            Multiplier*100f, Duration,chance*100f);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0]/100f;
        Multiplier = (float)param[1]/100f;
        Duration = (float)param[2];
    }

}
