﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class CritOneShot : baseProperties
{
    public float Chance;
    public float CreeperHP;

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceLastE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.critical && instance.tgtUnit.GetHP() / instance.tgtUnit.GetFullHP() <= CreeperHP && UnityEngine.Random.Range(0f, 1f) <= Chance)
        {
            instance.instantKill = true;
            instance.damage = instance.tgtUnit.GetHP();
            //if (instance.isClone == false)
                new TextOverlay(instance.srcUnit.getPosition, "INSTANT KILL", Color.red, false);
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceLastE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return String.Format("Критический урон в {0}% случаев мгновенно убивает врага, у которого осталось не более {1}% здоровья",
            Chance * 100f,CreeperHP * 100f);
    }

    public override void SetProperties(object[] param)
    {
        Chance = (float)Convert.ToDouble(param[0]) / 100f;
        CreeperHP = (float)Convert.ToDouble(param[1]) / 100f;
    }
}
