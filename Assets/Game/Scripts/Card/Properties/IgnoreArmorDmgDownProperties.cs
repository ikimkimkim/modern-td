﻿using UnityEngine;
using System.Collections;
using TDTK;

public class IgnoreArmorDmgDownProperties : basePropertiesGrade, INameAndIcon
{
    public float dmg;
    public int idStat;
    public virtual string getName { get { return "Прожигание хитина"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/IgnoreArmorDmgDown.png"; } }

    public override bool ApplyIsSampleTower { get { return true; } }

    public override bool Apply(Unit unit)
    {
        idStat = unit.currentActiveStat;
        unit.stats[idStat].damageMin *= (1f - _GradeParam);
        unit.stats[idStat].damageMax *= (1f - _GradeParam);
        unit.ignoreTypeArmor = true;
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        unit.stats[idStat].damageMin /= (1f - _GradeParam);
        unit.stats[idStat].damageMax /= (1f - _GradeParam);
        unit.ignoreTypeArmor = false;
        return true;
    }

    public override bool Apply(Ability ab)
    {
        ab.effect.damageMin *= (1f - _GradeParam);
        ab.effect.damageMax *= (1f - _GradeParam);
        ab.ignoreTypeArmor = true;
        return true;
    }

    public override bool Cancel(Ability ab)
    {
        ab.effect.damageMin /= (1f - _GradeParam);
        ab.effect.damageMax /= (1f - _GradeParam);
        ab.ignoreTypeArmor = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Игнорирует все типы брони, но получает -{0}% к урону",dmg*100f);
    }

    public override void SetProperties(object[] param)
    {
        dmg = (float)param[0]/100f;
        _GradeParam = dmg;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Игнорирует все типы брони, но получает -{0}(<color=#19c62e>-{1}</color>)% к урону",
                System.Math.Round(dmg * 100f, 2), 
                System.Math.Round(dmg * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmg);
    }
}
