﻿using UnityEngine;
using System.Collections;
using TDTK;

public class UpChanceOfTowerFrostbiteProperties : basePropertiesGrade, INameAndIcon
{
    private bool bApply = false;
    public float addChance;
    FrostbiteProperties fpLink;
    
    public string getName { get { return "Ледяные выстрелы"; } }
    public string getUrlIcon { get { return "EffectIcon/UpChanceFrostbite.png"; } }

    public override bool Apply(Unit unit)
    {
        if (bApply == false)
        {
            fpLink = null;
            for (int i = 0; i < unit.MyCard.Properties.Count; i++)
            {
                if (unit.MyCard.Properties[i].ID == 77)
                {
                    fpLink = unit.MyCard.Properties[i].properties as FrostbiteProperties;
                    fpLink.calcChance = CalcChance;
                    break;
                }
            }
            bApply = true;
        }
        return true;
    }
    
    public override bool Cancel(Unit unit)
    {
        if (bApply == true)
        {
            bApply = false;
        }
        return true;
    }

    public override bool Apply(Ability ab)
    {
        if(bApply==false)
        {
            fpLink = null;
            for (int i = 0; i < ab.MyCard.Properties.Count; i++)
            {
                if(ab.MyCard.Properties[i].ID==77)
                {
                    fpLink = ab.MyCard.Properties[i].properties as FrostbiteProperties;
                    fpLink.calcChance = CalcChance;
                    break;
                }
            } 
            bApply = true;
        }
        return true;
    }

    public override bool Cancel(Ability ab)
    {
        if (bApply == true)
        {
            bApply = false;
        }
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Для автоатак башен по замедленным противникам шанс вызвать \"Обморожение\" увеличен на {0}%",
            Mathf.Round(addChance*100f));
    }

    public override void SetProperties(object[] param)
    {
        addChance = (float)param[0]/100f;
        _GradeParam = addChance;
    }

    private float CalcChance(float baseChance, Unit target, Unit root)
    {
        if (root.IsTower)
            return baseChance + _GradeParam;
        return baseChance;
    }
    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Для автоатак башен по замедленным противникам шанс вызвать \"Обморожение\" увеличен на {0}(<color=#19c62e>+{1}</color>)%",
                System.Math.Round(addChance * 100f, 2),
                System.Math.Round(addChance * _multiplierLevel * level * 100f,2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, addChance);
    }
}
