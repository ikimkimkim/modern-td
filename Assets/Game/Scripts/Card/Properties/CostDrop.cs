﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;

public class CostDrop : baseProperties
{

    public float costDrop;
    public override bool ApplyIsSampleTower { get { return true; } }

    public override bool Apply(Unit unit)
    {
        ApplyAllLevel(unit);
        return true;
    }

    private void ApplyAllLevel(Unit unit)
    {
        for (int i = 0; i < unit.stats[0].cost.Count; i++)
        {
            unit.stats[0].cost[i] -= unit.stats[0].cost[i] * costDrop;
        }

        if (unit.IsTower)
        {
            if (unit.GetUnitTower.nextLevelTowerList.Count > 0)
            {
                if (unit.GetUnitTower.nextLevelTowerList[0] != null)
                {
                    ApplyAllLevel(unit.GetUnitTower.nextLevelTowerList[0]);
                }
            }
        }
    }

    public override bool Cancel(Unit unit)
    {
        CanselAllLevel(unit);
        return true;
    }

    private void CanselAllLevel(Unit unit)
    {
        for (int i = 0; i < unit.stats[0].cost.Count; i++)
        {
            unit.stats[0].cost[i] += unit.stats[0].cost[i] * costDrop;
        }

        if (unit.GetUnitTower.nextLevelTowerList.Count > 0)
        {
            if (unit.GetUnitTower.nextLevelTowerList[0] != null)
            {
                CanselAllLevel(unit.GetUnitTower.nextLevelTowerList[0]);
            }
        }
    }

    public override string GetTextInfo()
    {
        return string.Format("Стоимость постройки и улучшения башни снижена на {0}%", costDrop*100);
    }

    public override void SetProperties(object[] param)
    {
        costDrop = (float)param[0]/100f;
    }
}
