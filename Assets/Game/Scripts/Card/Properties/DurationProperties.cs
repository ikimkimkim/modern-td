﻿using UnityEngine;
using System.Collections;
using TDTK;

public class DurationProperties : baseProperties
{

    public int idPropreties;
    public float duration;

    public override bool Apply(Unit unit)
    {
        if (unit.IsHero)
        {
            var listP = unit.GetUnitHero.Properties;
            for (int i = 0; i < listP.Count; i++)
            {
                if (listP[i].ID == idPropreties)
                {
                    ApplyProperties(unit, listP[i]);
                    break;
                }
            }
        }
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (unit.IsHero)
        {
            var listP = unit.GetUnitHero.Properties;
            for (int i = 0; i < listP.Count; i++)
            {
                if (listP[i].ID == idPropreties)
                {
                    ApplyProperties(unit, listP[i]);
                    break;
                }
            }
        }
        return true;
    }

    public override string GetTextInfo()
    {
        switch (idPropreties)
        {
            case 20:
                return string.Format("Ярость действует дольше на {0}%",
                    duration * 100f);
            default:
                return "Увеличения действия свойтва, Не определен текст для свойтсва под ID:" + idPropreties;
        }
    }

    public override void SetProperties(object[] param)
    {
        idPropreties = System.Convert.ToInt32(param[0]);
        duration = (float)param[1];
    }

    public void ApplyProperties(Unit unit, CardProperties properties)
    {
        switch (properties.ID)
        {
            case 20:
                if (unit.IsHero)
                {
                    InRage rage = (InRage)properties.properties;
                    rage.duration *= duration;
                }
                break;
        }
    }

    public void CancelProperties(Unit unit, CardProperties properties)
    {
        switch (properties.ID)
        {
            case 20:
                if (unit.IsHero)
                {
                    InRage rage = (InRage)properties.properties;
                    rage.duration /= duration;
                }
                break;
        }
    }

}
