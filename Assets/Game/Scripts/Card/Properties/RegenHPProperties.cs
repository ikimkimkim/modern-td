﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class RegenHPProperties : baseProperties
{
    private float procentHP;
    private float buffHPRegen;
    private Unit rootUnit;

    public override bool Apply(Unit unit)
    {
        rootUnit = unit;
        buffHPRegen = rootUnit.HPRegenRate;
        rootUnit.HPRegenRate = procentHP * rootUnit.fullHP / 60f;
        if(rootUnit is UnitHero)
        {
            (rootUnit as UnitHero).unitUpdateLevel += RegenHPProperties_unitUpdateLevel;
        }
        return true;
    }

    private void RegenHPProperties_unitUpdateLevel(object sender, System.EventArgs e)
    {
        rootUnit.HPRegenRate = procentHP * rootUnit.fullHP / 60f;
    }

    public override bool Cancel(Unit unit)
    {
        rootUnit.HPRegenRate = buffHPRegen;
        if (rootUnit is UnitHero)
        {
            (rootUnit as UnitHero).unitUpdateLevel -= RegenHPProperties_unitUpdateLevel;
        }
        rootUnit = null;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Восстановление {0}% здоровья в минуту", procentHP*100 );
    }

    public override void SetProperties(object[] param)
    {
        procentHP = (float) param[0] / 100f;
    }
}
