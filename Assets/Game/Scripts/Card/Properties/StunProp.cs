﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;
using System.Collections.Generic;

public class StunProp : baseProperties
{
    public float StunChance;
    public float StunDuration;

    public bool bApply = false;
    public override bool ApplyIsSampleTower { get { return false; } }

    public override bool Apply(Unit unit)
    {
        if(bApply == false)
        {
            unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
            bApply = true;
        }
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (UnityEngine.Random.Range(0f, 1f) <= StunChance * instance.MultiplierChanceStun)
        {
            instance.stunned = true;
            instance.listEffects.Add(new StunControlEffect(StunDuration));
            //if (instance.isClone == false)
                new TextOverlay(instance.srcUnit.getPosition, "Stun!", Color.yellow, false);
        }
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply)
        {
            unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        }
        bApply = false;
        return true;
    }
    public override bool Apply(Ability ability)
    {
        ability.ApplayEffectE += Ability_ApplayEffectE;
        return true;
    }

    protected virtual void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> creeps, List<Unit> towers, int idCast)
    {
        for (int i = 0; i < creeps.Count; i++)
        {
            if(UnityEngine.Random.Range(0f,1f)<=StunChance)
            {
                creeps[i].EffectsManager.AddEffect(null, new StunControlEffect(StunDuration));
            }
        }
    }

    public override bool Cancel(Ability ability)
    {
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        return true;
    }

    public override string GetTextInfo()
    {
        return (StunChance * 100f) + "% шанс оглушить врага на "+StunDuration+"c";
    }

    public override void SetProperties(object[] param)
    {
        StunChance = (float)Convert.ToDouble(param[0]) / 100f;
        StunDuration = (float)Convert.ToDouble(param[1]);
        bApply = false;
    }

}
