﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;

public class UpDmg : basePropertiesGrade
{
    public bool bApply;

    public float upDmg;

    public int idStat;
    public UnitHero hero;

    public override bool ApplyIsSampleTower { get { return true; } }

    public override bool Apply(Unit unit)
    {
        if (bApply)
            return false;

        if (unit.IsHero)
        {
            hero = unit.GetUnitHero;
            idStat = hero.currentActiveStat;
            unit.stats[idStat].damageMin  *= (1f + _GradeParam / 100f);
            unit.stats[idStat].damageMax  *= (1f + _GradeParam / 100f);
            hero.unitUpdateLevel += Crit_unitUpdateLevel;
        }
        else
        {

            unit.stats[0].damageMin = unit.stats[0].damageMin * (1f + _GradeParam / 100f);
            unit.stats[0].damageMax = unit.stats[0].damageMax * (1f + _GradeParam / 100f);
        }

        bApply = true;
        //Debug.LogWarning("UpDmg min " + tower.stats[0].damageMin);
        return true;
    }

    private void Crit_unitUpdateLevel(object sender, EventArgs e)
    {
        if (hero != null)
        {
            Cancel(hero);
            Apply(hero);
        }
    }

    public override bool Cancel(Unit unit)
    {
        if(bApply)
        {
            if(hero!=null)
            {
                unit.stats[idStat].damageMin /= (1f + _GradeParam / 100f);
                unit.stats[idStat].damageMax /= (1f + _GradeParam / 100f);
                hero.unitUpdateLevel -= Crit_unitUpdateLevel;
            }
            else
            {
                unit.stats[0].damageMin = unit.MyCard.stats[0].damageMin;
                unit.stats[0].damageMax = unit.MyCard.stats[0].damageMax;
            }
            bApply = false;
        }

        return true;
    }

    public override bool Apply(Ability ability)
    {
        ability.effect.damageMin = ability.effect.damageMin * (1f + _GradeParam / 100f);
        ability.effect.damageMax = ability.effect.damageMax * (1f + _GradeParam / 100f);
        return true;
    }

    public override bool Cancel(Ability ability)
    {
        AbilityManager.SetAbilityDmg(ability, ability.MyCard);
        return true;
    }

    public override string GetTextInfo()
    {
        return "Урон увеличен на " + upDmg+"%";
    }

    public override void SetProperties(object[] param)
    {
        bApply = false;
        upDmg = (float)Convert.ToDouble(param[0]);
        _GradeParam = upDmg;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Урон увеличен на {0}(<color=#19c62e>+{1}</color>)%",
                upDmg,
                System.Math.Round(upDmg * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, upDmg);
    }
}
