﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class PropertiesAttak
{
    public event MyAttakInstanceHandler myOutAttakInstanceInitE, myOutAttakInstanceFirstE, myOutAttakInstanceLastE;

    public int prefabID;
    public int currentActiveStat = 0;
    public List<UnitStat> stats = new List<UnitStat>() { new UnitStat() };

    public List<Transform> shootPoints = new List<Transform>();
    public int GetShootPointCount() { return shootPoints.Count>0 ? shootPoints.Count : 1; }

    public int damageType = 0;
    public LayerMask mask;

    public PropertiesAttak()
    {

    }

    public PropertiesAttak(int DamageType, float MinDmg, float MaxDmg, LayerMask Mask)
    {
        damageType = DamageType;

        mask = Mask;

        if (stats == null)
            stats = new List<UnitStat>() { new UnitStat() };
        else if (stats.Count < 1)
            stats.Add(new UnitStat());

        currentActiveStat = 0;

        stats[currentActiveStat].damageMin = MinDmg;
        stats[currentActiveStat].damageMax = MaxDmg;
    }

    #region AttackOutEvent
    public void CallEventOutAttakInit(AttackInstance attackInstance)
    {
        if (myOutAttakInstanceInitE != null)
            myOutAttakInstanceInitE(attackInstance);
    }
    public void CallEventOutAttakFirst(AttackInstance attackInstance)
    {
        if (myOutAttakInstanceFirstE != null)
            myOutAttakInstanceFirstE(attackInstance);
    }
    public void CallEventOutAttakLast(AttackInstance attackInstance)
    {
        if (myOutAttakInstanceLastE != null)
            myOutAttakInstanceLastE(attackInstance);
    }
    #endregion

    public LayerMask GetTargetMask()
    {
        return mask;
    }

    public float GetAOERadius() { return stats[currentActiveStat].aoeRadius; }


    public float GetDamageMin() { return Mathf.Max(0, stats[currentActiveStat].damageMin ); }
    public float GetDamageMax() { return Mathf.Max(0, stats[currentActiveStat].damageMax ); }
    public float GetCooldown() { return Mathf.Max(0.01f, stats[currentActiveStat].cooldown ); }
    public int GetClipSize() { return (int)(stats[currentActiveStat].clipSize ); }
    public float GetReloadDuration() { return Mathf.Max(0.05f, stats[currentActiveStat].reloadDuration ); }

   // public float GetCritChance() { return stats[currentActiveStat].crit.chance ; }
   // public float GetCritMultiplier() { return stats[currentActiveStat].crit.dmgMultiplier ; }

    public float GetShieldBreak() { return stats[currentActiveStat].shieldBreak ; }
    public float GetShieldPierce() { return stats[currentActiveStat].shieldPierce ; }
    public bool DamageShieldOnly() { return stats[currentActiveStat].damageShieldOnly; }

   // public Stun GetStun() { return PerkManager.ModifyStunWithPerkBonus(stats[currentActiveStat].stun.Clone(), prefabID, 1); }   //pass 1 to indicate this is for FPSWeapon
   // public TDTK.Slow GetSlow() { return PerkManager.ModifySlowWithPerkBonus(stats[currentActiveStat].slow.Clone(), prefabID, 1); }
  //  public Dot GetDot() { return PerkManager.ModifyDotWithPerkBonus(stats[currentActiveStat].dot.Clone(), prefabID, 1); }
  //  public InstantKill GetInstantKill() { return PerkManager.ModifyInstantKillWithPerkBonus(stats[currentActiveStat].instantKill.Clone(), prefabID, 1); }



    public Transform GetShootObject() { return stats[currentActiveStat].shootObject.transform; }
}
