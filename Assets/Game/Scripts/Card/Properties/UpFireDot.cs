﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class UpFireDot : baseProperties
{
    public float Multiplier;

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceLastE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;
        FireDotEffect pe = instance.listEffects.Find(i => i is FireDotEffect) as FireDotEffect;
        if (pe != null)
            pe.damage *= Multiplier;
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceLastE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return "Горение наносит в "+Multiplier+" раза больше урона за то же время";
    }

    public override void SetProperties(object[] param)
    {
        Multiplier = (float)Convert.ToDouble(param[0]);
    }
}
