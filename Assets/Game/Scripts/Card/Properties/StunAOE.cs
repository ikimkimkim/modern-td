﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class StunAOE : baseProperties
{
    

    public int idStat;
    public UnitHero hero;

    private void unitUpdateLevel(object sender, EventArgs e)
    {
        if (hero != null)
        {
            Cancel(hero);
            Apply(hero);
        }
    }

    public override string GetTextInfo()
    {
        return "Может оглушить врагов, задетых уроном по площади";
        //return "Стан на " + StunDuration + "с с шансом " + (StunChance * 100f) + "%";
    }

    public override void SetProperties(object[] param)
    {

    }
}
