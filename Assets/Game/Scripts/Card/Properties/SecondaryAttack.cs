﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;

public class SecondaryAttack : baseProperties
{
    public int CountSecondaryAttack;


    public override bool Apply(Unit unit)
    {
        unit.stats[0].CountAttakcs = unit.MyCard.stats[0].CountAttakcs + CountSecondaryAttack;
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        unit.stats[0].CountAttakcs = unit.MyCard.stats[0].CountAttakcs;
        return true;
    }

    public override string GetTextInfo()
    {
        return "Производит " + CountSecondaryAttack + " доп. атак";
    }

    public override void SetProperties(object[] param)
    {
        CountSecondaryAttack = (int)Convert.ToInt32(param[0]);
    }
}
