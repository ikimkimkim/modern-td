﻿using UnityEngine;
using System.Collections;
using TDTK;
using Translation;

public class CastAbilitySlowChanceShot : basePropertiesGrade, INameAndIcon
{
    public int idAbility;

    public float Chance;
    public float DurationMultiplier;
    public float PowerMultiplier;
    public Ability ability;
    public Unit unitlink;
    public CardProperties slowProp;

    public string getName { get { return "Снаряды концентрата"; } }
    public string getUrlIcon { get { return "EffectIcon/CastAbilityMeteorit.png"; } }

    public override bool Apply(Unit unit)
    {
        slowProp = unit.MyCard.Properties.Find(i => i.ID == 2);
        if (slowProp != null)
        {
            float p = (float)slowProp.SetParam[0] * (1f + PowerMultiplier) / 100f;
            float d = (float)slowProp.SetParam[1] * (1f + _GradeParam);
            ability.effect.baseEffects.Add(new SlowControlEffect(p, d));
            unit.onShotTurretE += ShotEvent;
            unitlink = unit;
        }
        else
            Debug.LogError("Cast Slow ability properties, not data, tower not have slow properties");


        return true;
    }

    public void ShotEvent(Unit target)
    {
        if (UnityEngine.Random.Range(0f, 1f) < Chance)
        {
            AbilityManager.instanceCastAbility(ability, target.thisT.position);
            new TextOverlay(unitlink.getPosition, "Magic!", Color.blue, false);
        }
    }

    public override bool Cancel(Unit unit)
    {
            unit.onShotTurretE -= ShotEvent;
            unitlink = null;
        return false;
    }

    public override string GetTextInfo()
    {
        Translator trans = TranslationEngine.Instance.Trans;
        return string.Format("С вероятностью {0}% кастует заклинание {1} при выстреле.",
            Chance * 100f, trans[ability.name]);
    }

    public override void SetProperties(object[] param)
    {
        idAbility = 2;

        var bdAbility = AbilityDB.Load();
        ability = bdAbility[idAbility].Clone();        

        Chance = (float)param[0] / 100f;

       // ability.aoeRadius = (float)param[1];
        PowerMultiplier = (float)param[1]/100f;
        DurationMultiplier = (float)param[2]/100f;
        _GradeParam = DurationMultiplier;
    }

    public override string GetTextInfo(int level)
    {
        Translator trans = TranslationEngine.Instance.Trans;
        if (level > 0)
            return string.Format("С вероятностью {0}% кастует заклинание {1} при выстреле. Длительность заморожки увеличено на {2}(<color=#19c62e>+{3}</color>)%.",
                Chance * 100f, trans[ability.name],
                System.Math.Round(DurationMultiplier * 100f,2),
                System.Math.Round(DurationMultiplier * _multiplierLevel * level * 100f,2));
        else
            return GetTextInfo();

    }

    public override void SetLevel(int level)
    {
        SetLevel(level, DurationMultiplier);
    }
}
