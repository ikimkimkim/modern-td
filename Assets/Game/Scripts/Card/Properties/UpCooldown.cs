﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class UpCooldown : baseProperties
{
    public float chance;
    public float cooldown;
    public float duration;

    public override string GetTextInfo()
    {
        return String.Format("{0}% шанс при выстреле увеличить свою скорострельность на {1}% на {2} секунд",
            chance*100f, cooldown * 100f, duration);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)Convert.ToDouble(param[0]) / 100f;
        cooldown = (float)Convert.ToDouble(param[1]) / 100f;
        duration = (float)Convert.ToDouble(param[2]);
    }
}
