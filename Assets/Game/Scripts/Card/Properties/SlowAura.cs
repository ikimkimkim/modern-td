﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class SlowAura : baseProperties
{
    public float SlowMultiplier;
    public float Range;

    public GameObject prefab;
    public GameObject go;

    public override string GetTextInfo()
    {
        return String.Format("Аура замедляет передвижение противников на {0}% в радиусе {1}",
            (1f - SlowMultiplier) * 100f, Range);
    }

    public override void SetProperties(object[] param)
    {
        SlowMultiplier = (float)(1f - Convert.ToDouble(param[0]) / 100);
        Range = (float)(Convert.ToDouble(param[1]));
        var bdeffect = EffectDB.Load();
        prefab = bdeffect[6];
    }
}
