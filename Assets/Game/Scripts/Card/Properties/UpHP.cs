﻿using UnityEngine;
using System.Collections;
using TDTK;

public class UpHP : baseProperties
{

    public float hpMultiple;

    public override bool Apply(Unit unit)
    {
        unit.fullHP *= hpMultiple;
        unit.HP *= hpMultiple;
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        unit.fullHP /= hpMultiple;
        unit.HP /= hpMultiple;
        return false;
    }

    public override string GetTextInfo()
    {
        return "Здоровье увеличено на "+ Mathf.Ceil((hpMultiple-1)*100f)+"%";
    }

    public override void SetProperties(object[] param)
    {
        hpMultiple = (float) (1f + System.Convert.ToDouble(param[0])/100f); 
    }
}
