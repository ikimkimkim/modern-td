﻿using UnityEngine;
using System.Collections;
using TDTK;

public class CooldownProperties : baseProperties
{
    private const int IDGrenades = 19;
    
    public int idPropreties;
    public float cooldown;

    public override bool Apply(Unit unit)
    {
        if (unit.IsHero)
        {
            var listP = unit.GetUnitHero.Properties;
            for (int i = 0; i < listP.Count; i++)
            {
                if(listP[i].ID == idPropreties)
                {
                    ApplyProperties(unit, listP[i]);
                }
            }
        }
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (unit.IsHero)
        {
            var listP = unit.GetUnitHero.Properties;
            for (int i = 0; i < listP.Count; i++)
            {
                if (listP[i].ID == idPropreties)
                {
                    CancelProperties(unit, listP[i]);
                    break;
                }
            }
        }
        return true;
    }

    public override string GetTextInfo()
    {
        switch (idPropreties)
        {
            case IDGrenades:
                return string.Format("Перезарядка гранаты уменьшена на {0}%",
                    cooldown*100f);

            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
                return string.Format("Заклинания восстанавливаются быстрее на {0}%",
                    cooldown*100f);
            default:
                return "Не определен текст для свойтсва под ID:"+idPropreties;
        }
    }

    public override void SetProperties(object[] param)
    {
        idPropreties = System.Convert.ToInt32(param[0]);
        cooldown = (float)param[1];
    }


    public void ApplyProperties(Unit unit, CardProperties properties)
    {
        switch (properties.ID)
        {
            case IDGrenades:
                if (unit.IsHero)
                {
                    ThrowingGrenades grenades = (ThrowingGrenades)properties.properties;
                    Debug.Log("Apply cooldown grenad: cd:" + grenades.attak.stats[0].cooldown);
                    grenades.attak.stats[0].cooldown *= cooldown;
                    //unit.GetUnitHero.GrenadesWeapon.stats[0].cooldown *= cooldown;
                }
                break;
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
                CastAbility castAbility = (CastAbility)properties.properties;
                castAbility.Cooldown *= cooldown;
                break;
        }
    }

    public void CancelProperties(Unit unit, CardProperties properties)
    {
        switch (properties.ID)
        {
            case IDGrenades:
                if (unit.IsHero)
                {
                    ThrowingGrenades grenades = (ThrowingGrenades)properties.properties;
                    Debug.Log("Cancel cooldown grenad: cd:" + grenades.attak.stats[0].cooldown);
                    grenades.attak.stats[0].cooldown /= cooldown;
                    //Debug.Log("Cancel cooldown grenad: cd:" + unit.GetUnitHero.GrenadesWeapon.stats[0].cooldown);
                    //unit.GetUnitHero.GrenadesWeapon.stats[0].cooldown /= cooldown;
                }
                break;
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
                CastAbility castAbility = (CastAbility)properties.properties;
                castAbility.Cooldown /= cooldown;
                break;
        }
    }
}
