﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;

public class UpAOE : basePropertiesGrade
{
    public int AOERange;

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;
        instance.SetAOERadius(instance.AOERadius * (1f + _GradeParam/100f));
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Радиус урона по площади увеличен на {0}%", AOERange);
    }

    public override void SetProperties(object[] param)
    {
        AOERange = Convert.ToInt32(param[0]);
        //AOEMyltiplay = (1f + AOERange / 100f);
        _GradeParam = AOERange;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Радиус урона по площади увеличен на {0}(<color=#19c62e>+{1}</color>)%", AOERange,
                Math.Round(AOERange * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, AOERange);
    }
}
