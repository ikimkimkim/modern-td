﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;
using Translation;

public class CastAbilityMeteoritChanceShot : basePropertiesGrade, INameAndIcon
{
    public int idAbility;

    public float Chance;
    public float dmgMultiplier;
    public Ability ability;
    public Unit unitlink;

    public float lastTime;

    public virtual string getName { get { return "Снаряды концентрата"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/CastAbilityMeteorit.png"; } }

    public override bool Apply(Unit unit)
    {
        lastTime = Time.time;
        unit.onShotTurretE += ShotEvent;
        unitlink = unit;

        return true;
    }

    public void ShotEvent(Unit target)
    {
        if(UnityEngine.Random.Range(0f,1f)<Chance && Time.time>=lastTime+4)
        {
            ability.effect.damageMax = unitlink.stats[unitlink.currentActiveStat].damageMax * _GradeParam; //DamageMax;
            ability.effect.damageMin = unitlink.stats[unitlink.currentActiveStat].damageMin * _GradeParam;
            AbilityManager.instanceCastAbility(ability, target.thisT.position);
            new TextOverlay(unitlink.getPosition, "Magic!", new Color(1f, 0.5f, 0, 1f), false);
            lastTime = Time.time;
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.onShotTurretE -= ShotEvent;
        unitlink = null;

        return false;
    }

    public override string GetTextInfo()
    {
        Translator trans = TranslationEngine.Instance.Trans;
        return string.Format("С вероятностью {0}% кастует заклинание {1} при выстреле. Заклинание наносит {2}% урона башни.",
            Chance * 100f, trans[ability.name], dmgMultiplier * 100f);
    }

    public override void SetProperties(object[] param)
    {
        idAbility = 0;

        var bdAbility = AbilityDB.Load();
        ability = bdAbility[idAbility].Clone();

        Chance = (float)param[0]/100f;
        dmgMultiplier = (float)param[1]/100f;
        _GradeParam = dmgMultiplier;
    }

    public override string GetTextInfo(int level)
    {
        Translator trans = TranslationEngine.Instance.Trans;
        if (level > 0)
            return string.Format("С вероятностью {0}% кастует заклинание {1} при выстреле. Заклинание наносит {2}(<color=#19c62e>+{3}</color>)% урона башни.",
                Chance * 100f, trans[ability.name],
                 System.Math.Round(dmgMultiplier * 100f,2),
                 System.Math.Round(dmgMultiplier * _multiplierLevel * level * 100f,2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmgMultiplier);
    }
}
