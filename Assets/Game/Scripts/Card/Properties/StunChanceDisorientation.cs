﻿using UnityEngine;
using System.Collections;
using TDTK;

public class StunChanceDisorientation : basePropertiesGrade, INameAndIcon
{
    public float StunChance;
    public float StunDuration;
    
    public virtual string getName { get { return "Глуши зевак"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/StunChanceDisorientation.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (UnityEngine.Random.Range(0f, 1f) <= _GradeParam * instance.MultiplierChanceStun && instance.tgtUnit.EffectsManager.isActiveEffect(IDEffect.DisorientationControl))
        {
            instance.stunned = true;
            instance.listEffects.Add(new StunControlEffect(StunDuration, false));
            new TextOverlay(instance.srcUnit.getPosition, "Stun!", Color.yellow, false);
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("При попадании по дезориентированному противнику с веростностью {0}% оглушает цель на {1} секунды",
            StunChance * 100f, StunDuration);
    }

    public override void SetProperties(object[] param)
    {
        StunChance = (float)(param[0]) / 100f;
        StunDuration = (float)(param[1]);
        _GradeParam = StunChance;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("При попадании по дезориентированному противнику с веростностью {0}(<color=#19c62e>+{2}</color>)% оглушает цель на {1} секунды",
                System.Math.Round(StunChance * 100f, 2), StunDuration,
                System.Math.Round(StunChance * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, StunChance);
    }
}
