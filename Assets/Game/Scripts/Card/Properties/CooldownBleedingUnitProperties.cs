﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class CooldownBleedingUnitProperties : basePropertiesGrade, INameAndIcon,IRangeActionProperties
{
    public float procent, range;
    public int maxCount;
    private Coroutine coroutineUpdate;
    public bool bApply = false;

    private Unit _unitLink;
    private int count;

    public string getName { get { return "Кровавый дождь"; } }
    public string getUrlIcon { get { return "EffectIcon/CooldownBleedingUnit.png"; } }

    public float getRangeAction
    {
        get
        {
            return range;
        }
    }

    public override bool Apply(Unit unit)
    {
        if (unit.IsTower == false)
            return false;
        if (bApply == false)
        {
            count = 0;
            if (listUnit == null)
                listUnit = new List<Unit>();
            else
                listUnit.Clear();

            _unitLink = unit;

            if (coroutineUpdate != null)
                _unitLink.StopCoroutine(coroutineUpdate);

            EffectManager.addEffectE += AddEffect;
            EffectManager.removeEffectE += RemoveEffect;


            bApply = true;

            coroutineUpdate = _unitLink.StartCoroutine(Update());
        }
        return true;
    }

    public List<Unit> listUnit;

    public void AddEffect(Unit unit, IDEffect typeEffect)
    {
        if (unit.IsCreep == false)
            return;
        if (typeEffect == IDEffect.BleedingDot)
        {
            //MyLog.Log("Add unit " + unit.name);
            listUnit.Add(unit);
            UpdateProcent();
        }
    }

    public void RemoveEffect(Unit unit, IDEffect typeEffect)
    {
        if (unit.IsCreep == false)
            return;
        if (typeEffect == IDEffect.BleedingDot && listUnit.Contains(unit))
        {
            //MyLog.Log("Remove unit " + unit.name);
            listUnit.Remove(unit);
            UpdateProcent();
        }
    }


    public void UpdateProcent()
    {
        int newCount = 0;
        foreach (Unit unit in listUnit)
        {
            if (Vector3.Distance(_unitLink.getPosition, unit.getPosition) <= range)
            {
                newCount++;
            }
        }
        newCount = Mathf.Clamp(newCount, 0, maxCount);
        if (newCount != count)
        {
            int change = newCount - count;
            if (change > 0)
            {
                UpStat(change);
            }
            else
            {
                DownStat(Mathf.Abs(change));
            }
            count = newCount;
        }
    }

    public void UpStat(int change)
    {
        _unitLink.stats[_unitLink.currentActiveStat].cooldown /= (1 + _GradeParam * change);
    }

    public void DownStat(int change)
    {
        _unitLink.stats[_unitLink.currentActiveStat].cooldown *= (1 + _GradeParam * (change));
    }

    public IEnumerator Update()
    {
        while (bApply)
        {
            yield return new WaitForSeconds(1);
            UpdateProcent();
        }
        yield break;
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply)
        {
            EffectManager.addEffectE -= AddEffect;
            EffectManager.removeEffectE -= RemoveEffect;

            if (coroutineUpdate != null)
                _unitLink.StopCoroutine(coroutineUpdate);

            DownStat(count);
            _unitLink = null;
        }
        bApply = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("+{0}% к скорострельности за каждого врага с кровотечением в радиусе {1}, бонус складывается максимум {2} раз", 
            procent * 100f, range,maxCount);
    }

    public override void SetProperties(object[] param)
    {
        procent = (float)(param[0]) / 100f;
        range = (float)(param[1]);
        maxCount = System.Convert.ToInt32(param[2]);
        _GradeParam = procent;
        bApply = false;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("+{0}(<color=#19c62e>+{3}</color>)% к скорострельности за каждого врага с кровотечением в радиусе {1}, бонус складывается максимум {2} раз",
                procent * 100f, range, maxCount, 
                System.Math.Round(procent * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, procent);
    }
}