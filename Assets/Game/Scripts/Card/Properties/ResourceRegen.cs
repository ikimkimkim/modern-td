﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;
using System;

public class ResourceRegen : baseProperties
{
    public float Count;
    public float Speed;

    public float oldSpeed;
    List<float> oldCount;


    //public bool ApplyIsSampleTower { get { return true; } }

    public override bool Apply(Unit unit)
    {
        if(unit.IsTower==false)
        {
            MyLog.Log("Can't apply ResourceRegen, unit is not Tower");
            return false;
        }

        if (unit.stats[0].rscGain == null || unit.stats[0].rscGain.Count < 2)
        {
            oldCount = new List<float>() { 0, 0 };
        }
        else
            oldCount = unit.stats[0].rscGain;
        oldSpeed = unit.stats[0].cooldown;

        unit.stats[0].rscGain = new List<float>() { 0, Count  };
        unit.stats[0].cooldown = Speed;

        return true;
    }

    public override bool Cancel(Unit unit)
    {
        unit.stats[0].rscGain = oldCount;
        unit.stats[0].cooldown = oldSpeed;
        return true;
    }

    public override string GetTextInfo()
    {
        return "Восстанавливает " + Count + " маны за " + Speed + "c";
    }

    public override void SetProperties(object[] param)
    {
        Count = (float)( Convert.ToDouble(param[0]));
        Speed = (float)Convert.ToDouble(param[1]);
    }
}
