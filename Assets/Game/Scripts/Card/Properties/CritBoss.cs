﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class CritBoss : baseProperties
{
    public float CritChance;
    public float CritdmgMultiplier;

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;
        if (instance.tgtUnit is UnitCreep)
        {
            if ((instance.tgtUnit as UnitCreep).isBoss && UnityEngine.Random.Range(0f, 1f) <= instance.GetChanceCritical(CritChance))
            {
                instance.SetMultiplierDmgCritical(CritdmgMultiplier);
            }
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return "" + (CritChance) * 100 + "% шанс нанести " + (CritdmgMultiplier * 100f) + "% урона по боссу";
    }

    public override void SetProperties(object[] param)
    {
        CritChance = (float)Convert.ToDouble(param[0]) / 100f;
        CritdmgMultiplier = (float)Convert.ToDouble(param[1]) / 100f;
    }
}
