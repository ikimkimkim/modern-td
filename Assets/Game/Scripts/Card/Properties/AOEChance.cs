﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class AOEChance : basePropertiesGrade, INameAndIcon
{
    public int AOERange;
    public float Chance;
    
    public string getName { get { return "Подствольный гранатомёт"; } }
    public string getUrlIcon { get { return "EffectIcon/AOEChance.png"; } }

    public override bool Apply(Unit unit)
    {
            unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;
        if (UnityEngine.Random.Range(0f, 1f) < _GradeParam)
        {
            instance.SetAOERadius(AOERange);
            if(_GradeParam<0.9f)
                new TextOverlay(instance.srcUnit.getPosition, "Splash!", Color.white, false);
        }
    }

    public override bool Cancel(Unit unit)
    {
            unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("{0}% шанс произвести атаку по площади радиусом {1}", Chance*100f, AOERange);
    }

    public override void SetProperties(object[] param)
    {
        Chance = (float)param[0] / 100f;
        AOERange = System.Convert.ToInt32(param[1]);
        _GradeParam = Chance;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("{0}(<color=#19c62e>+{2}</color>)% шанс произвести атаку по площади радиусом {1}",
                Chance * 100f, AOERange, 
                Math.Round(Chance * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, Chance);
    }
}
