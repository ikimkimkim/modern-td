﻿using UnityEngine;
using System.Collections;
using TDTK;

public class WeaknessProperties : basePropertiesGrade, INameAndIcon
{
    public float chance;
    public float duration;
    public float slow, timeMultiplier;

    public IDEffect PropertiesUnitType;

    public string getName { get { return "Трёхкратное пробитие"; } }
    public string getUrlIcon { get { return "EffectIcon/WeaknessProperties.png"; } }

    public override bool Apply(Unit unit)
    {
            PropertiesUnitType = unit.MyCard.Properties.Find(i => i.ID == 2) != null ? IDEffect.SlowControl : IDEffect.none;
            if(PropertiesUnitType == IDEffect.none)
            {
                PropertiesUnitType = unit.MyCard.Properties.Find(i => i.ID == 7) != null ? IDEffect.FireDot : IDEffect.none;
            }
            if (PropertiesUnitType == IDEffect.none)
            {
                PropertiesUnitType = unit.MyCard.Properties.Find(i => i.ID == 8) != null ? IDEffect.PoisonDot : IDEffect.none;
            }

            unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (getCountEffect(instance.tgtUnit)>=2 && Random.Range(0f, 1f) < _GradeParam)
        {
            instance.listEffects.Add(new WeaknessDebufEffect(slow,timeMultiplier,duration));
            new TextOverlay(instance.srcUnit.getPosition, "Weakness!", new Color(0f, 1f, 1f, 1f), false);
        }
    }

    public int getCountEffect(IUnitAttacked unit)
    {
        int count = 0;
        if (PropertiesUnitType != IDEffect.SlowControl && unit.EffectsManager.isActiveEffect(IDEffect.SlowControl))
            count++;
        if (PropertiesUnitType != IDEffect.FireDot && unit.EffectsManager.isActiveEffect(IDEffect.FireDot))
            count++;
        if (PropertiesUnitType != IDEffect.PoisonDot && unit.EffectsManager.isActiveEffect(IDEffect.PoisonDot))
            count++;

        return count;
    }

    public override bool Cancel(Unit unit)
    {
            unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Если на цели есть 2 других магических статуса, то при попадании с вероятностью {2}% накладывает статус \"слабость\", при котором все статусы действуют в {0} раза дольше, плюс цель двигается на {1}% медленнее",
            timeMultiplier, slow * 100f, chance * 100f);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0] / 100f;
        duration = (float)param[1];
        slow = (float)param[2] / 100f;
        timeMultiplier = (float)param[3] / 100f;
        _GradeParam = chance;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Если на цели есть 2 других магических статуса, то при попадании с вероятностью {2}(<color=#19c62e>+{3}</color>)% накладывает статус \"слабость\", при котором все статусы действуют в {0} раза дольше, плюс цель двигается на {1}% медленнее",
                timeMultiplier, slow * 100f,
                chance * 100f, 
                System.Math.Round(chance * _multiplierLevel * level*100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, chance);
    }
}
