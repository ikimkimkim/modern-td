﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class UpCostDebuffProperties : basePropertiesGrade, INameAndIcon
{
    public float Power;
    
    public virtual string getName { get { return "Золотая жила"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/UpCost.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;

        instance.listEffects.Add(new UpCostDebufEffect(_GradeParam, 4));
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override bool Apply(Ability ability)
    {
        ability.effect.baseEffects.Add(new UpCostDebufEffect(_GradeParam, 4));
        return true;
    }

    public override bool Cancel(Ability ability)
    {
        int id = ability.effect.baseEffects.FindIndex(i => i is UpCostDebufEffect);
        if (id > -1)
        {
            ability.effect.baseEffects.RemoveAt(id);
        }

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("За добивания монстров игрок получает на {0}% больше монет", Power * 100f);
    }

    public override void SetProperties(object[] param)
    {
        Power = (float)Convert.ToDouble(param[0]) / 100f;
        _GradeParam = Power;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("За добивания монстров игрок получает на {0}(<color=#19c62e>+{1}</color>)% больше монет",
               Math.Round(Power * 100f, 2),
                 Math.Round(Power * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, Power);
    }
}