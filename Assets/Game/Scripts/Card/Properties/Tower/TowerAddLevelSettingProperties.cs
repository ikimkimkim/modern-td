﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class TowerAddLevelSettingProperties : baseProperties
{
    private int countAddLevel;

    public override bool Apply(Unit unit)
    {
        return false;
    }

    public override bool Cancel(Unit unit)
    {
        return false;
    }

    public override string GetTextInfo()
    {
        return string.Format("Максимальный уровень башни увеличен на {0}",countAddLevel);
    }

    public override void SetProperties(object[] param)
    {
        countAddLevel = (int)param[0];
    }
}
