﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class ChanceIgnoreArmorProperties : baseProperties
{
    private float chance;

    public override bool Apply(Unit unit)
    {
        if (!(unit is UnitTower))
            return false;

        unit.myOutAttakInstanceFirstE += Unit_myOutAttakInstanceFirstE;

        return true;
    }

    private void Unit_myOutAttakInstanceFirstE(AttackInstance attackInstance)
    {
        if(Random.value<=chance)
        {
            attackInstance.ignoreArmor = true;
            new TextOverlay(attackInstance.srcUnit.getPosition, "Ignore Armor!", Color.red, false);
        }
    }

    public override bool Cancel(Unit unit)
    {
        if (!(unit is UnitTower))
            return false;

        unit.myOutAttakInstanceFirstE -= Unit_myOutAttakInstanceFirstE;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("{0}% шанс нанести урон игнорируя тип брони", chance * 100f);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0] / 100f;
    }
}
