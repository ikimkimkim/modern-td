﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class SelectedAnyProperties : baseProperties
{
    public override bool Apply(Unit unit)
    {
        Debug.LogWarning("Set ignore");
        unit.MyCard.IgnorePropertiesDB = true;
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        Debug.LogWarning("Remove ignore");
        unit.MyCard.IgnorePropertiesDB  = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Выборы свойств для башни не ограницено другими постройеными башнями.");
    }

    public override void SetProperties(object[] param)
    {

    }
}
