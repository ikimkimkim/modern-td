﻿using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine;

public class UpDmgCurrentTypePropeties : baseProperties
{
    private float dmg;
    
    public override bool Apply(Unit unit)
    {
        if (!(unit is UnitTower))
            return false;

        unit.myOutAttakInstanceInitE += Unit_myOutAttakInstanceInitE;

        return true;
    }

    float modifier;
    private void Unit_myOutAttakInstanceInitE(AttackInstance attackInstance)
    {
        modifier = DamageTable.GetModifier(attackInstance.tgtUnit.ArmorType(), attackInstance.srcUnit.DamageType());
        if(modifier == 1)
        {
            attackInstance.damage *= (1f + dmg);
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceInitE -= Unit_myOutAttakInstanceInitE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Увеличения урона на {0}% против монстров своего типа", dmg*100);
    }

    public override void SetProperties(object[] param)
    {
        dmg = (float)param[0] / 100f;
    }
}
