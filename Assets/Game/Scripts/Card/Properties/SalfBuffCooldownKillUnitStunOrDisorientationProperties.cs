﻿using UnityEngine;
using System.Collections;
using TDTK;

/// <summary>
/// Изменен под убийство любого моба, раньше считались только мобы под станом и дезориентации
/// </summary>
public class SalfBuffCooldownKillUnitStunOrDisorientationProperties : basePropertiesGrade, INameAndIcon
{
    float Multiplier;
    float Duration;
    int MaxStack;

    Unit _unit;

    public string getName { get { return "Раж"; } }
    public string getUrlIcon { get { return "EffectIcon/SalfBuffCooldownKillUnitStunOrDisorientation.png"; } }

    public override bool Apply(Unit unit)
    {
        _unit = unit;
        Unit.onDestroyedE += Unit_onDestroyedE;
        return true;
    }

    private void Unit_onDestroyedE(Unit unit)
    {
        if(unit.IsCreep)
        {
            if(unit.KillMyUnit is Unit && unit.KillMyUnit as Unit == _unit)
            {
                _unit.EffectsManager.AddEffect(_unit, new CooldownBuffEffect(Multiplier, _GradeParam, 
                    MaxStack, IDEffect.CooldownBuffStecked));
                new TextOverlay(_unit.getPosition, "Rage!", Color.red, false);
            }
        }
    }

    public override bool Cancel(Unit unit)
    {
        Unit.onDestroyedE -= Unit_onDestroyedE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("При убийстве врага повышает свою скорострельность на {0}% на {1} сек, бонус складывается максимум {2} раза",
            Multiplier*100, Duration, MaxStack);
    }

    public override void SetProperties(object[] param)
    {
        Multiplier = (float)param[0] / 100f;
        Duration = (float)param[1];
        MaxStack = Mathf.Clamp(System.Convert.ToInt32(param[2]), 0, int.MaxValue);
        _GradeParam = Duration;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("При убийстве врага повышает свою скорострельность на {0}% на {1}(<color=#19c62e>+{3}</color>) сек, бонус складывается максимум {2} раза",
                Multiplier * 100, Duration, MaxStack,
                System.Math.Round(Duration * _multiplierLevel * level, 2));
        else
            return GetTextInfo();

    }

    public override void SetLevel(int level)
    {
        SetLevel(level, Duration);
    }
}
