﻿using UnityEngine;
using System.Collections;
using TDTK;

public class CritDownAOE : basePropertiesGrade, INameAndIcon
{
    public float CritChance;
    public float CritdmgMultiplier;
    public float AOEMultiplier;

    public string getName { get { return "Точечный выстрел"; } }
    public string getUrlIcon { get { return "EffectIcon/CritAndDownAOE.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;
        if (UnityEngine.Random.Range(0f, 1f) <= instance.GetChanceCritical(CritChance))
        {
            instance.SetMultiplierDmgCritical(_GradeParam);
        }        
        instance.DownAOERadius(instance.AOERadius * (1f - AOEMultiplier));
    }
    

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("{0}% нанести {1}% урона, и радиус сплэша уменьшается на {2}%",
                CritChance * 100f, CritdmgMultiplier * 100f, AOEMultiplier * 100f);
    }

    public override void SetProperties(object[] param)
    {
        CritChance = (float)(param[0]) / 100f;
        CritdmgMultiplier = (float)(param[1]) / 100f;
        _GradeParam = CritdmgMultiplier;
        AOEMultiplier = (float)param[2]/100;

    }

    public override string GetTextInfo(int level)
    {
        if(level>0)
        return string.Format("{0}% нанести {1}(<color=#19c62e>+{3}</color>)% урона, и радиус сплэша уменьшается на {2}%",
                CritChance * 100f, 
                System.Math.Round(CritdmgMultiplier * 100f, 2), AOEMultiplier * 100f,
                System.Math.Round(CritdmgMultiplier *  _multiplierLevel * level*100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, CritdmgMultiplier);
    }
}
