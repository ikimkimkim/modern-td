﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;

public class UpMinDmg : baseProperties
{
    public float upMinDmg;

    public override bool ApplyIsSampleTower { get { return true; } }

    public override bool Apply(Unit unit)
    {
        unit.stats[0].damageMin = unit.stats[0].damageMin * (1f + upMinDmg / 100f);
        if (unit.stats[0].damageMin > unit.stats[0].damageMax)
            unit.stats[0].damageMax = unit.stats[0].damageMin;

        //Debug.LogWarning("UpMinDmg " + tower.stats[0].damageMin);
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        unit.stats[0].damageMin = unit.MyCard.stats[0].damageMin;
        return true;
    }
    public override  bool Apply(Ability ability)
    {
        ability.effect.damageMin = ability.effect.damageMin * (1f + upMinDmg / 100f);
        return true;
    }

    public override bool Cancel(Ability ability)
    {
        AbilityManager.SetAbilityDmg(ability, ability.MyCard);
        return true;
    }
    public override string GetTextInfo()
    {
        return "Минимальный урон увеличен на " + upMinDmg + "%";
    }

    public override void SetProperties(object[] param)
    {
        upMinDmg = (float)Convert.ToDouble(param[0]);
    }
}