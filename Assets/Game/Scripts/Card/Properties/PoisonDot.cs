﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;
using System.Collections.Generic;

public class PoisonDot : basePropertiesGrade
{

    public float Duration;
    public float DmgMultiplier;
    public const float Interval = 0.5f;
    public float CountTik;



    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;

        instance.listEffects.Add(new PoisonDotEffect(CountTik, Interval, instance.dmgModifier * instance.damage * _GradeParam / CountTik,instance.srcUnit.DamageType()));
    }

    public override bool Cancel(Unit unit)
    {
            unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    Dictionary<int, List<Unit>> units;
    public override bool Apply(Ability ability)
    {

        if (units == null)
            units = new Dictionary<int, List<Unit>>();
        else
            units.Clear();

        ability.StartCastE += Ability_StartCastE;
        ability.ApplayEffectE += Ability_ApplayEffectE;
        ability.EndCastE += Ability_EndCastE;
        return true;
    }


    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        if (units.ContainsKey(idCast))
        {
            units[idCast].Clear();
        }
        else
        {
            units.Add(idCast, new List<Unit>());
        }
    }

    private void Ability_ApplayEffectE(Ability ability, AbilityEffect effect, Vector3 position, List<Unit> targets, List<Unit> towers, int idCast)
    {
        if (units.ContainsKey(idCast) == false)
        {
            Debug.LogError("Cast ab and poison effect, id not find!");
            return;
        }

        for (int i = 0; i < targets.Count; i++)
        {
            if (units[idCast].Contains(targets[i]) == false)
            {
                if (ability.MultiCastAbility)
                {
                    targets[i].EffectsManager.AddEffect(null, new PoisonDotEffect(CountTik, Interval,
                        (effect.damageMin + effect.damageMax) * ability.MultiCastNumber / 2f * _GradeParam / CountTik, ability.damageType));
                }
                else
                {
                    targets[i].EffectsManager.AddEffect(null, new PoisonDotEffect(CountTik, Interval,
                        (effect.damageMin + effect.damageMax) / 2f * _GradeParam / CountTik, ability.damageType));

                }

                units[idCast].Add(targets[i]);
            }
        }
    }

    private void Ability_EndCastE(Ability ability, Vector3 position, int idCast)
    {
        if (units.ContainsKey(idCast))
        {
            units[idCast].Clear();
            units.Remove(idCast);
        }
    }

    public override bool Cancel(Ability ability)
    {
        ability.StartCastE -= Ability_StartCastE;
        ability.ApplayEffectE -= Ability_ApplayEffectE;
        ability.EndCastE -= Ability_EndCastE;

        return true;
    }
    public override string GetTextInfo()
    {
        return string.Format("Отравляет врага и наносит {0}% дополнительного урона за {1}с",
            DmgMultiplier * 100f, Duration);
    }


    public override void SetProperties(object[] param)
    {
        DmgMultiplier = (float)Convert.ToDouble(param[0]) / 100f;
        Duration = (float)Convert.ToDouble(param[1]);
        CountTik = Duration / Interval;
        _GradeParam = DmgMultiplier;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Отравляет врага и наносит {0}(<color=#19c62e>+{2}</color>)% дополнительного урона за {1}с",
                Math.Round(DmgMultiplier * 100f, 2), Duration,
                Math.Round(DmgMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, DmgMultiplier);
    }
}