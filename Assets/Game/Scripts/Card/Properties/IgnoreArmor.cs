﻿using UnityEngine;
using System.Collections;
using TDTK;

public class IgnoreArmor : baseProperties
{
    public override bool Apply(Unit unit)
    {
        unit.ignoreTypeArmor = true;
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        unit.ignoreTypeArmor = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return "Наносит максимальный урон противникам с любой броней";
    }

    public override void SetProperties(object[] param)
    {

    }

}
