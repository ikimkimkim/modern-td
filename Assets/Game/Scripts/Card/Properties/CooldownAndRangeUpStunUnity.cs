﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class CooldownAndRangeUpStunUnity : basePropertiesGrade, INameAndIcon,IRangeActionProperties
{
    private const int maxStack = 5;

    public float procent,range;
    public Coroutine coroutineUpdate;
    public bool bApply = false;

    public Unit _unitLink;
    
    public string getName { get { return "Дальнобой"; } }
    public string getUrlIcon { get { return "EffectIcon/CooldownAndRangeUpStunUnity.png"; } }

    public float getRangeAction
    {
        get
        {
            return range;
        }
    }

    public override bool Apply(Unit unit)
    {
        if (unit.IsTower == false)
            return false;

        if (bApply == false)
        {
            count = 0;
            if (listUnit == null)
                listUnit = new List<Unit>();
            else
                listUnit.Clear();

            _unitLink = unit;

            if (coroutineUpdate != null)
                _unitLink.StopCoroutine(coroutineUpdate);

            EffectManager.addEffectE += AddEffect;
            EffectManager.removeEffectE += RemoveEffect;

            
            bApply = true;

            coroutineUpdate = _unitLink.StartCoroutine(Update());
        }
        return true;
    }

    public List<Unit> listUnit;

    public void AddEffect(Unit unit, IDEffect typeEffect)
    {
        if (unit.IsCreep==false)
            return;
        if(typeEffect == IDEffect.StunControl)
        {
            listUnit.Add(unit);
            UpdateProcent();
        }
    }

    public void RemoveEffect(Unit unit, IDEffect typeEffect)
    {
        if (unit.IsCreep == false)
            return;
        if (typeEffect == IDEffect.StunControl && listUnit.Contains(unit))
        {
            listUnit.Remove(unit);
            UpdateProcent();
        }
    }

    public int count;
    public void UpdateProcent()
    {
        int newCount = 0;
        foreach (Unit unit in listUnit)
        {
            if(Vector3.Distance(_unitLink.getPosition,unit.getPosition)<=range)
            {
                newCount++;
                if (newCount >= maxStack)
                    break;
            }
        }
        if(newCount!=count)
        {
            int change = newCount - count;
            if(change>0)
            {
                UpStat(change);
            }
            else
            {
                DownStat(Mathf.Abs(change));
            }
            count = newCount;
            MyLog.Log("Up stat, count unity in stun:" + count);
        }
    }

    public void UpStat(int change)
    {
        _unitLink.stats[_unitLink.currentActiveStat].cooldown /= (1 + _GradeParam * change);
        _unitLink.stats[_unitLink.currentActiveStat].range *= (1 + _GradeParam * change);
    }

    public void DownStat(int change)
    {
        _unitLink.stats[_unitLink.currentActiveStat].cooldown *= (1 + _GradeParam * (change));
        _unitLink.stats[_unitLink.currentActiveStat].range /= (1 + _GradeParam * (change));
    }

    public IEnumerator Update()
    {
        while(bApply)
        {
            yield return new WaitForSeconds(1);
            UpdateProcent();
        }
        yield break;
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply)
        {
            EffectManager.addEffectE -= AddEffect;
            EffectManager.removeEffectE -= RemoveEffect;

            if (coroutineUpdate != null)
                _unitLink.StopCoroutine(coroutineUpdate);

            DownStat(count);
            _unitLink = null;
        }
        bApply = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Радиус атаки и скорострельность повышается на {0}% за каждого оглушенного врага в радиусе {1}",
            procent*100f, range);
    }

    public override void SetProperties(object[] param)
    {
        procent = (float)(param[0]) / 100f;
        range = (float)(param[1]);
        _GradeParam = procent;
        bApply = false;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Радиус атаки и скорострельность повышается на {0}(<color=#19c62e>+{2}</color>)% за каждого оглушенного врага в радиусе {1}",
               System.Math.Round(procent * 100f,2), range, System.Math.Round(procent * _multiplierLevel * level*100f,2));
        else
            return GetTextInfo();

    }

    public override void SetLevel(int level)
    {
        SetLevel(level, procent);
    }
}
