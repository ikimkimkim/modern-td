﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;
using System;
using Translation;

public class CastAbility : baseProperties, IPropertiesEvent {

    public event EventHandler<eventPropertiesArgs> eventCooldown;
    public eventPropertiesArgs Status;

    public int idAbility;
    public float Range;
    public float DamageMax, DamageMin;
    public float Cooldown, BetweenАttacks;
    public int CountAttak;

    public bool bApply = false;
    public Ability ability;
    public Unit unitlink;
    private UnitHero heroLink;
    private Coroutine coroutineCast;


    public override bool Apply(Unit unit)
    {
        if (bApply)
            return false;

        unitlink = unit;
        if (unit.IsHero)
            heroLink = unit.GetUnitHero;
        bApply = true;
        if (coroutineCast != null)
            unit.StopCoroutine(coroutineCast);
        coroutineCast = unit.StartCoroutine(Cast());

        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply == false)
            return false;

        if (coroutineCast!=null)
            unit.StopCoroutine(coroutineCast);
        bApply = false;
        return true;
    }

    public override string GetTextInfo()
    {
        Translator trans = TranslationEngine.Instance.Trans;
        return string.Format("Вызывает {0}. Урон {1}-{2}, перезарядка {3}с."
            ,trans[ability.name], ability.effect.damageMin*CountAttak, ability.effect.damageMax*CountAttak, Cooldown);
    }

    public override void SetProperties(object[] param)
    {
        bApply = false;
        Status = new eventPropertiesArgs();

        idAbility = Convert.ToInt32(param[0]);

        var bdAbility = AbilityDB.Load();
        ability = bdAbility[idAbility].Clone();

        DamageMin = (float)Convert.ToDouble(param[1]);
        DamageMax = (float)Convert.ToDouble(param[2]);
        Range = (float)Convert.ToDouble(param[3]);
        Cooldown = (float)Convert.ToDouble(param[4]);
        CountAttak = Convert.ToInt32(param[5]);

        ability.effect.damageMax = DamageMax;
        ability.effect.damageMin = DamageMin;
    }

    
    IEnumerator Cast()
    {
        Collider[] cols;
        Unit unit;
        List<Unit> tgtList = new List<Unit>();
        while (bApply)
        {
            if(unitlink.isMoveEnd()==false)
            {
                yield return null;
                continue;
            }

            cols  = Physics.OverlapSphere(unitlink.thisT.position, Range, unitlink.GetTargetMask());
            if (cols.Length > 0)
            {
                
                tgtList.Clear();
                for (int i = 0; i < cols.Length; i++)
                {
                    unit = cols[i].gameObject.GetComponent<Unit>();
                    if (!unit.dead) tgtList.Add(unit);
                }

                if (tgtList.Count > 0)
                {
                    heroLink.PropertiesAttakLock = true;
                    heroLink.ControlLock = true;
                    heroLink.castAbility = true;
                    
                    float timeAnim = 0;
                    while (heroLink.getWeightHealing < 0.9f)
                    {
                        yield return null;
                        timeAnim += Time.deltaTime;
                        unitlink.thisT.LookAt(tgtList[0].getPosition);
                    }


                    if(ability.CastEffect!=null)
                        unitlink.GetUnitHero.CastSpell(ability.CastEffect);
                    else
                        unitlink.GetUnitHero.CastSpell();

                    for (int i = 0; i < CountAttak; i++)
                    {
                        Unit target = tgtList[UnityEngine.Random.Range(0, tgtList.Count)];

                        AbilityManager.instanceCastAbility(ability, target.thisT.position);

                        yield return new WaitForSeconds(BetweenАttacks);
                    }

                    heroLink.PropertiesAttakLock = false;
                    heroLink.ControlLock = false;
                    heroLink.castAbility = false;
                    float timeCooldown = Cooldown - timeAnim;
                    while (timeCooldown > 0)
                    {
                        yield return null;
                        timeCooldown -= Time.deltaTime;
                        Status.Cooldown = timeCooldown;
                        if (eventCooldown != null)
                            eventCooldown(this, Status);
                    }
                }
            }

            yield return null;
        }
    }

    public void SetListener(int id, EventHandler<eventPropertiesArgs> delegateAction)
    {
        Status.id = id;
        eventCooldown += delegateAction;
    }

    public void RemovListener(EventHandler<eventPropertiesArgs> delegateAction)
    {
        eventCooldown -= delegateAction;
        Status.id = -1;
    }
    public eventPropertiesArgs getStatus()
    {
        return Status;
    }
}
