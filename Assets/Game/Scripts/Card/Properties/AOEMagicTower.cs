﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class AOEMagicTower : basePropertiesGrade, INameAndIcon,IRangeActionProperties
{
    List<Unit> listTower;

    public int AOERange;
    public float ChanceOneTower;
    public float Range;

    public string getName { get { return "Взрывные боеприпасы"; } }
    public string getUrlIcon { get { return "EffectIcon/AOEMagic.png"; } }

    public float getRangeAction
    {
        get
        {
            return Range;
        }
    }

    public Unit _unit;
    public override bool Apply(Unit unit)
    {
        if (unit.IsTower == false)
            return false;

        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        UnitTower.onConstructionCompleteE += TowerBild;
        UnitTower.onSoldE += TowerDestoy;
        Unit.onDestroyedE += DestoyUnit;
        _unit = unit;
        StartPropertis();

        return true;
    }
    
    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;
        //MyLog.Log("AOEMagicTower tower count:"+ CountUnicumTower);
        float chance = ChanceOneTower + ChanceOneTower * Mathf.Clamp(CountUnicumTower, 0, 2);
        if (Random.Range(0f, 1f) < chance)
        {
            instance.SetAOERadius(_GradeParam);
            if (chance < 0.9f)
                new TextOverlay(instance.srcUnit.getPosition, "Splash!", Color.white, false);
        }
    }

    public int CountUnicumTower;
    public void TowerBild(UnitTower tower)
    {
        if (isNeedTower(tower))
        {
            if (Vector3.Distance(_unit.getPosition, tower.getPosition) <= Range)
            {
                if (listTower.Exists(i => i.MyCard.IDTowerData == tower.MyCard.IDTowerData) == false)
                {
                    CountUnicumTower++;
                }
                listTower.Add(tower);
            }
            else
                MyLog.Log("AOE Magic, new tower out range");
        }
    }

    public void DestoyUnit(Unit unit)
    {
        if (unit.IsTower == false)
            return;
        TowerDestoy(unit.GetUnitTower);
    }

    public void TowerDestoy(UnitTower tower)
    {
        if (isNeedTower(tower))
        {
            if(listTower.Contains(tower))
            {
                listTower.Remove(tower);
                if(listTower.Exists(i=> i.MyCard.IDTowerData == tower.MyCard.IDTowerData)==false)
                {
                    CountUnicumTower--;
                }
            }
        }
    }

    private bool isNeedTower(UnitTower tower)
    {
        return (tower.MyCard.IDTowerData != _unit.MyCard.IDTowerData && tower.MyCard.IDTowerData == 7 && tower.MyCard.IDTowerData == 8 && tower.MyCard.IDTowerData == 9);
    }

    public void StartPropertis()
    {
        LayerMask mask = 1 << LayerManager.LayerTower();

        Collider[] cols = Physics.OverlapSphere(_unit.thisT.position, Range, mask);
        UnitTower unit;
        bool[] unicum = new bool[3];
        for (int i = 0; i < 3; i++)
        {
            unicum[i] = false;
        }
        foreach (Collider col in cols)
        {
            unit = col.GetComponent<UnitTower>();
            if (unit.MyCard.IDTowerData != _unit.MyCard.IDTowerData)
            {
                switch (unit.MyCard.IDTowerData)
                {
                    case 7:
                        if (unicum[0]==false)
                        {
                            unicum[0] = true;
                            CountUnicumTower++;
                        }
                        break;
                    case 8:
                        if (unicum[1] == false)
                        {
                            unicum[1] = true;
                            CountUnicumTower++;
                        }
                        break;
                    case 9:
                        if (unicum[2] == false)
                        {
                            unicum[2] = true;
                            CountUnicumTower++;
                        }
                        break;
                }
                listTower.Add(unit);
            }
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        UnitTower.onConstructionCompleteE -= TowerBild;
        UnitTower.onSoldE -= TowerDestoy;
        Unit.onDestroyedE -= DestoyUnit;
        _unit = null;

        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Cплеш рудиусом {1} с шансом {0}%, плюс {0}% за магические башни других типов в радиусе {3} (не более {0}% от каждого типа башен, итого максимум +{2}%)",
            ChanceOneTower * 100f,AOERange,
            System.Math.Round(ChanceOneTower * 300f, 2),Range);
    }

    public override void SetProperties(object[] param)
    {
        listTower = new List<Unit>();
        AOERange = System.Convert.ToInt32(param[0]);
        Range = (float)param[1];
        ChanceOneTower = (float)param[2]/100f;
        _GradeParam = AOERange;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Cплеш рудиусом {1}(<color=#19c62e>+{4}</color>) с шансом {0}%, плюс {0}% за магические башни других типов в радиусе {3} (не более {0}% от каждого типа башен, итого максимум +{2}%)",
                ChanceOneTower * 100, AOERange, System.Math.Round(ChanceOneTower * 300f, 2), Range,
                System.Math.Round(AOERange * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, AOERange);
    }
}
