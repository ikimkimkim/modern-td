﻿using UnityEngine;
using System.Collections;
using TDTK;

public class ShockProperties : basePropertiesGrade, INameAndIcon
{
    public float Chance, Power,Duration;

    public virtual string getName { get { return "Знание слабостей"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/ShockProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;

        if (UnityEngine.Random.Range(0f, 1f) <= Chance)
        {
            instance.listEffects.Add(new ShockDebufEffect(Power, _GradeParam));
            new TextOverlay(instance.srcUnit.getPosition, "Shock!", new Color(0.75f, 0f, 1f, 1f), false);
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override bool Apply(Ability ability)
    {
        ability.StartCastE += Ability_StartCastE;
        return true;
    }

    private void Ability_StartCastE(Ability ability, Vector3 position, int idCast)
    {
        if (UnityEngine.Random.Range(0f, 1f) <= Chance)
        {
            ability.effect.effects.Add(new ShockDebufEffect(Power, _GradeParam));
            new TextOverlay(position, "Shock!", new Color(0.75f, 0f, 1f, 1f), false);
        }
    }

    public override bool Cancel(Ability ability)
    {
        ability.StartCastE -= Ability_StartCastE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("{0}% шанс наложить на цель \"шок\" на {1}c, при котором цель получает на {2}% больше урона от всех источников урона",
            Chance * 100f, Duration, Power * 100f);
    }

    public override void SetProperties(object[] param)
    {
        Chance = (float)param[0] / 100f;
        Power = (float)param[1] / 100f;
        Duration = (float)param[2];
        _GradeParam = Duration;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("{0}% шанс наложить на цель \"шок\" на {1}(<color=#19c62e>+{3}</color>)c, при котором цель получает на {2}% больше урона от всех источников урона",
                Chance * 100f, Duration, Power * 100f, Duration * _multiplierLevel * level);
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, Duration);
    }
}
