﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;
using System.Collections.Generic;

public class ResourceUpMax : baseProperties
{
    public float AddCount;

    public float oldMax;

    public override bool Apply(Unit unit)
    {
        if (ResourceManager.instance != null && ResourceManager.instance.rscMaxRateList.Count < 2)
        {
            MyLog.LogWarning("rscMaxRateList count < 2!, создаю пустое ограничения для своства");
            ResourceManager.instance.rscMaxRateList = new List<float>() { 0, 0 };
        }

        if (ResourceManager.instance == null || ResourceManager.instance.rscMaxRateList.Count < 2)
        {
            oldMax = 0;
        }
        else
            oldMax = ResourceManager.instance.rscMaxRateList[1];

        ResourceManager.instance.rscMaxRateList[1] += AddCount;
        ResourceManager.SpendResource(new List<float>() { 0, 0 });
        return true;
    }

    public override bool Cancel(Unit unit)
    {
            ResourceManager.instance.rscMaxRateList[1] -= AddCount;
            ResourceManager.SpendResource(new List<float>() { 0, 0 });
        return true;
    }

    public override string GetTextInfo()
    {
        return "Увеличивает максимальное количество маны на " + AddCount;
    }

    public override void SetProperties(object[] param)
    {
        AddCount = (float)(Convert.ToDouble(param[0]));
    }

}