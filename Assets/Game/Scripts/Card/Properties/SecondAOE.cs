﻿using UnityEngine;
using System.Collections;
using TDTK;

public class SecondAOE : basePropertiesGrade, INameAndIcon
{
    private int AOERange;
    private float Chance, dmgMultiplier, timeDelay;
    private GameObject SplashObject;

    private Unit _unitLink;

    public string getName { get { return "Двойной заряд"; } }
    public string getUrlIcon { get { return "EffectIcon/SecondAOE.png"; } }

    public override bool Apply(Unit unit)
    {
        _unitLink = unit;
        Unit.ApplyAttackE += Unit_ApplyAttackE;
        return true;
    }

    private void Unit_ApplyAttackE(Unit from, AttackInstance attackInstance)
    {
        if (from is UnitCreep && attackInstance != null && attackInstance.isCloneSplash == false && attackInstance.srcUnit is Unit && attackInstance.srcUnit as Unit == _unitLink)
        {
            if (Random.Range(0f, 1f) < Chance)
            {
                _unitLink.StartCoroutine(InvokeAOE(attackInstance.impactPoint, attackInstance));
            }
        }
    }

    public override bool Cancel(Unit unit)
    {
        Unit.ApplyAttackE -= Unit_ApplyAttackE;
        _unitLink = null;
        return true;
    }

    public IEnumerator InvokeAOE(Vector3 point, AttackInstance instance)
    {
        yield return new WaitForSeconds(timeDelay);
        OnAOE(point,instance);
        yield break;
    }

    Collider[] colsAOE;
    GameObject objSplash;


    public void OnAOE(Vector3 point, AttackInstance attInstance)
    {
        if(SplashObject != null)
        {
            objSplash = ObjectPoolManager.Spawn(SplashObject, point, Quaternion.identity);
            objSplash.GetComponent<SplashSpecialEffect>().Emit(AOERange);
        }

        new TextOverlay(attInstance.srcUnit.getPosition, "Second splash!", Color.white, false);

        colsAOE = Physics.OverlapSphere(point, AOERange, _unitLink.GetTargetMask());
        if (colsAOE.Length > 0)
        {
            for (int i = 0; i < colsAOE.Length; i++)
            {
                Unit unit = colsAOE[i].GetComponent<Unit>();
                if (unit != null && !unit.dead)
                {
                    AttackInstance attIns = attInstance.CloneNewTargetProcess(unit);
                    attIns.damage *= _GradeParam;
                    attIns.SetAOERadius(AOERange);
                    attIns.ApplyDistance(Vector3.Distance(point, unit.transform.position));
                    unit.ApplyEffect(attIns);
                }
            }
        }
    }

    public override string GetTextInfo()
    {
        // {0}% Шанс произвести второй взрыв при попадании. 
        return string.Format("С шансом {0}% производит второй взрыв при попадании. Взрыв наносит {1}% урона башни в радиусе {2}", Chance*100f, dmgMultiplier*100f, AOERange);
    }

    public override void SetProperties(object[] param)
    {
        
        Chance = (float)param[0] / 100f;
        AOERange = System.Convert.ToInt32(param[1]);
        dmgMultiplier = (float)param[2] / 100f;
        _GradeParam = dmgMultiplier;
        timeDelay = (float)param[3];
        SplashObject = DataBase<GameObject>.LoadIndex(DataBase<GameObject>.NameBase.SplashObject, 0);
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("С шансом {0}% производит второй взрыв при попадании. Взрыв наносит {1}(<color=#19c62e>+{3}</color>)% урона башни в радиусе {2}",
                Chance * 100f, 
                System.Math.Round(dmgMultiplier * 100f, 2), AOERange, 
                System.Math.Round(dmgMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmgMultiplier);
    }
}
