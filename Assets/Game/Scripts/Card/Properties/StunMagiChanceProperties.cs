﻿using UnityEngine;
using System.Collections;
using TDTK;

public class StunMagiChanceProperties : basePropertiesGrade, INameAndIcon
{

    public float Chance, AddChance;
    public float StunDuration;

    public string getName { get { return "Выжигатель мозгов"; } }
    public string getUrlIcon { get { return "EffectIcon/StunMagiChance.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;

        float c = getChance(instance.tgtUnit);
        if (UnityEngine.Random.Range(0f, 1f) < c * instance.MultiplierChanceStun)
        {
            instance.stunned = true;
            instance.listEffects.Add(new StunControlEffect(_GradeParam));
            new TextOverlay(instance.srcUnit.getPosition, "Stun!", Color.yellow, false);
        }
    }

    private float getChance(IUnitAttacked unit)
    {
        float CurrentChance = Chance;
        if (unit.EffectsManager.isActiveEffect(IDEffect.FireDot))
            CurrentChance += AddChance;

        if (unit.EffectsManager.isActiveEffect(IDEffect.PoisonDot))
            CurrentChance += AddChance;

        if (unit.EffectsManager.isActiveEffect(IDEffect.SlowControl))
            CurrentChance += AddChance;

        return CurrentChance;
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("{0}% шанс оглушить на {1}c врага при попадании. Шанс оглушения увеличивается на {2}% за каждый магический статус (горение, замедление, отравление), который есть на цели",
            Chance * 100f, StunDuration, AddChance * 100f);
    }

    public override void SetProperties(object[] param)
    {
        Chance = (float)param[0] / 100f;
        AddChance = (float)param[1] / 100f;
        StunDuration = (float)param[2];
        _GradeParam = StunDuration;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("{0}% шанс оглушить на {1}(<color=#19c62e>+{3}</color>)c врага при попадании. Шанс оглушения увеличивается на {2}% за каждый магический статус (горение, замедление, отравление), который есть на цели",
                Chance * 100f, StunDuration, AddChance * 100f, 
                System.Math.Round(StunDuration * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, StunDuration);
    }
}
