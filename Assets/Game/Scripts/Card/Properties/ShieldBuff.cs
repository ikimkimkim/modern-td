﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;
using System.Collections.Generic;

public class ShieldBuff : basePropertiesGrade, IPropertiesEvent,INameAndIcon
{    
    public event EventHandler<eventPropertiesArgs> eventUpdateStatus;
    public eventPropertiesArgs Status;

    public float Range;
    public float ShieldPoint;
    public float Cooldown;

    public bool bApply = false;
    public Unit unitlink;
    private Coroutine coroutineCast;
    
    public GameObject effectShield;
    public string getName { get { return "Держись!"; } }
    public string getUrlIcon { get { return "EffectIcon/ShieldMedic.png"; } }


    public override bool Apply(Unit unit)
    {
        unitlink = unit;
        bApply = true;
        if (coroutineCast != null)
            unit.StopCoroutine(coroutineCast);
        coroutineCast = unit.StartCoroutine(Cast());
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        if (coroutineCast != null)
            unit.StopCoroutine(coroutineCast);
        bApply = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return String.Format("Накладывает щит союзнику с самым минммальным количеством здоровья раз в {0}c, при условии что у него ещё нет щита",
            Cooldown);
    }

    public override void SetProperties(object[] param)
    {
        Status = new eventPropertiesArgs();
        ShieldPoint = (float)Convert.ToDouble(param[0]);

        var bdeffect = EffectDB.Load();
        effectShield = bdeffect[5];

        Range = (float)Convert.ToDouble(param[1]);
        Cooldown = (float)Convert.ToDouble(param[2]);
        _GradeParam = Cooldown;
        _multiplierLevel = -0.1f;
    }



    IEnumerator Cast()
    {
        Collider[] cols;
        Unit unit,target;
        LayerMask mask = 1 << LayerManager.LayerHero();


        while (bApply)
        {

            cols = Physics.OverlapSphere(unitlink.thisT.position, Range, mask);
            if (cols.Length > 0 && unitlink.isMoveEnd())
            {
                //tgtList.Clear();
                target = null;
                for (int i = 0; i < cols.Length; i++)
                {
                    unit = cols[i].gameObject.GetComponent<Unit>();
                    if (unit is FakeUnitHero==false && !unit.dead && unit.EffectsManager.isActiveEffect(IDEffect.ShieldBuff) == false)
                    {
                        if(target==null || (target.HP>unit.HP && unit.HP<unit.fullHP))
                        {
                            target = unit;
                        }
                    }
                }

                if (target!=null)
                {
                    unitlink.PropertiesAttakLock = true;

                    target.EffectsManager.AddEffect(unitlink, new ShieldMedicBuffEffect(ShieldPoint));

                    GameObject go = GameObject.Instantiate(effectShield);
                    go.transform.position = target.thisT.position;
                    go.transform.SetParent(target.thisT);
                    GameObject.Destroy(go, 10);

                }


                if (target != null)// (tgtList.Count != 0)
                {
                    unitlink.PropertiesAttakLock = false;
                    float time = _GradeParam;
                    Status.TimeDuration = 0;
                    while (time > 0)
                    {
                        yield return null;
                        time -= Time.deltaTime;
                        Status.Cooldown = time;
                        if (eventUpdateStatus != null)
                            eventUpdateStatus(this, Status);
                    }
                }
                else
                    yield return null;
            }
            else
                yield return null;

            unitlink.PropertiesAttakLock = false;
        }
    }

    public void SetListener(int id, EventHandler<eventPropertiesArgs> eventUpdate)
    {
        Status.id = id;
        eventUpdateStatus += eventUpdate;
    }

    public void RemovListener(EventHandler<eventPropertiesArgs> eventUpdate)
    {
        eventUpdateStatus -= eventUpdate;
        Status.id = -1;
    }
    public eventPropertiesArgs getStatus()
    {
        return Status;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return String.Format("Накладывает щит союзнику с самым минммальным количеством здоровья раз в {0}(<color=#19c62e>{1}</color>)c, при условии что у него ещё нет щита",
               Cooldown,
                Math.Round(Cooldown * _multiplierLevel * level,2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, Cooldown);
    }
}
