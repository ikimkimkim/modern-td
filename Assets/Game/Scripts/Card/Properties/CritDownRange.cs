﻿using UnityEngine;
using System.Collections;
using TDTK;

public class CritDownRange : basePropertiesGrade, INameAndIcon
{
    public float CritChance;
    public float CritdmgMultiplier;
    public float RangeMultiplier;

    public string getName { get { return "Подрыв"; } }
    public string getUrlIcon { get { return "EffectIcon/CritAndDownAOE.png"; } }

    public override bool ApplyIsSampleTower { get { return true; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        if (unit.IsTower)
        {
            unit.stats[unit.currentActiveStat].range *= (1 - RangeMultiplier);
        }
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;
        if (UnityEngine.Random.Range(0f, 1f) <= instance.GetChanceCritical(CritChance))
        {
            instance.SetMultiplierDmgCritical(_GradeParam);
        }
    }


    public override bool Cancel(Unit unit)
    {
            unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
            unit.stats[unit.currentActiveStat].range /= (1 - RangeMultiplier);
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("{0}% нанести {1}% урона, и  уменьшается радиус атаки на {2}%",
                        CritChance * 100f, CritdmgMultiplier * 100f,  RangeMultiplier * 100f);
    }

    public override void SetProperties(object[] param)
    {
        CritChance = (float)(param[0]) / 100f;
        CritdmgMultiplier = (float)(param[1]) / 100f;
        RangeMultiplier = (float)param[2] / 100;
        _GradeParam = CritdmgMultiplier;

    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("{0}% нанести {1}(<color=#19c62e>+{3}</color>)% урона, и  уменьшается радиус атаки на {2}%",
                            CritChance * 100f, CritdmgMultiplier * 100f, RangeMultiplier * 100f, 
                            System.Math.Round(CritdmgMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, CritdmgMultiplier);
    }
}
