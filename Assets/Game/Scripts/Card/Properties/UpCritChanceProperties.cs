﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class UpCritChanceProperties : basePropertiesGrade, INameAndIcon, IRangeActionProperties
{
    public float procent, range,maxProcent;
    public Coroutine coroutineUpdate;
    public int count;
    public bool bApply = false;

    public Unit _unitLink;
    public List<Unit> listUnit;

    public string getName { get { return "Не подходи!"; } }
    public string getUrlIcon { get { return "EffectIcon/UpCritChance.png"; } }

    public float getRangeAction
    {
        get
        {
            return range;
        }
    }

    public override bool Apply(Unit unit)
    {
        if (unit.IsTower == false)
            return false;
        if (bApply == false)
        {
            count = 0;
            if (listUnit == null)
                listUnit = new List<Unit>();
            else
                listUnit.Clear();

            _unitLink = unit;

            if (coroutineUpdate != null)
                _unitLink.StopCoroutine(coroutineUpdate);

            EffectManager.addEffectE += AddEffect;
            EffectManager.removeEffectE += RemoveEffect;

            unit.myOutAttakInstanceInitE += InstanceAtacInit;

            bApply = true;

            coroutineUpdate = _unitLink.StartCoroutine(Update());
        }
        return true;
    }

    private void InstanceAtacInit(AttackInstance instance)
    {
        instance.AddChanceCritical += Mathf.Clamp(procent * count, 0, maxProcent);
    }


    public void AddEffect(Unit unit, IDEffect typeEffect)
    {
        if (unit.IsCreep == false)
            return;
        if (typeEffect == IDEffect.StunControl || typeEffect == IDEffect.DisorientationControl)
        {
            listUnit.Add(unit);
            UpdateProcent();
        }
    }

    public void RemoveEffect(Unit unit, IDEffect typeEffect)
    {
        if (unit.IsCreep == false)
            return;
        if ((typeEffect == IDEffect.StunControl || typeEffect == IDEffect.DisorientationControl) 
            && listUnit.Contains(unit))
        {
            //MyLog.Log("Remove unit " + unit.name);
            listUnit.Remove(unit);
            UpdateProcent();
        }
    }

    public void UpdateProcent()
    {
        int newCount = 0;
        foreach (Unit unit in listUnit)
        {
            if (Vector3.Distance(_unitLink.getPosition, unit.getPosition) <= _GradeParam)
            {
                newCount++;
            }
        }
        count = newCount;
    }

    public IEnumerator Update()
    {
        while (bApply)
        {
            yield return new WaitForSeconds(1);
            UpdateProcent();
        }
        yield break;
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply)
        {
            EffectManager.addEffectE -= AddEffect;
            EffectManager.removeEffectE -= RemoveEffect;

            unit.myOutAttakInstanceInitE -= InstanceAtacInit;

            if (coroutineUpdate != null)
                _unitLink.StopCoroutine(coroutineUpdate);
            
            _unitLink = null;
        }
        bApply = false;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Шанс критического урона возрастает на {0}% за каждого дезориентированного или оглушенного врага в радиусе {1}. Максимум +{2}%",
            procent * 100f, range, maxProcent * 100f);
    }

    public override void SetProperties(object[] param)
    {
        procent = (float)(param[0]) / 100f;
        range = (float)(param[1]);
        maxProcent = (float)param[2]/100f;
        _GradeParam = range;
        bApply = false;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Шанс критического урона возрастает на {0}% за каждого дезориентированного или оглушенного врага в радиусе {1}(<color=#19c62e>+{3}</color>). Максимум +{2}%",
                procent * 100f, range, maxProcent * 100f,
                System.Math.Round(range * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, range);
    }
}
