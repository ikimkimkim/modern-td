﻿using UnityEngine;
using System.Collections;
using TDTK;

public class CastSecondAbilityLightningProperties : basePropertiesGrade, INameAndIcon
{
    public float delay, chance;
    public Unit unitlink;

    public Coroutine _corCast;
    public string getName { get { return "Двойной разряд"; } }
    public string getUrlIcon { get { return "EffectIcon/CastSecondAbilityLightning.png"; } }

    public override bool Apply(Unit unit)
    {
        unitlink = unit;
        CastAbilityLightningRandomTargetProperties.OnCastAbilityE += CastAbility;
        CastAbilityLightningChanceShot.OnCastAbilityE += CastAbility;
        return true;
    }

    private void CastAbility(Ability ability, Unit root, Unit target)
    {
        if(root == unitlink && Random.Range(0f,1f)<= _GradeParam)
        {
            if (_corCast != null)
                unitlink.StopCoroutine(_corCast);
            _corCast = unitlink.StartCoroutine(Cast(ability, target));
        }
    }

    public IEnumerator Cast(Ability ability,Unit target)
    {
        do
        {
            yield return new WaitForSeconds(delay);
            AbilityManager.instanceCastAbility(ability, target.thisT.position);
        }
        while (Random.Range(0f, 1f) <= _GradeParam);

        yield break;
    }

    public override bool Cancel(Unit unit)
    {
        if (_corCast != null)
            unitlink.StopCoroutine(_corCast);
        CastAbilityLightningRandomTargetProperties.OnCastAbilityE -= CastAbility;
        CastAbilityLightningChanceShot.OnCastAbilityE -= CastAbility;
        unitlink = null;
        return false;
    }

    public override string GetTextInfo()
    {
        return string.Format("Молния сработает дважды с шансом в {0}%",
            chance*100f);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0]/100f;
        delay = (float)param[1];
        _GradeParam = chance;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Молния сработает дважды с шансом в {0}(<color=#19c62e>+{1}</color>)%",
              System.Math.Round(chance * 100f, 1),
              System.Math.Round(chance * _multiplierLevel * level * 100f, 1));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, chance);
    }
}
