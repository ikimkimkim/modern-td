﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class UpStanPropSlow : baseProperties
{
    Stun oldStun;

    public float StunChance;
    public float StunDuration;

    public override string GetTextInfo()
    {
        return String.Format("{0}% оглушить замороженного врага на {1}с",
             StunChance * 100f, StunDuration);
    }


    public override void SetProperties(object[] param)
    {
        StunChance = (float)Convert.ToDouble(param[0]) / 100f;
        StunDuration = (float)Convert.ToDouble(param[1]);
    }
}
