﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class MagicShockProperties : basePropertiesGrade, INameAndIcon
{
    public float Chance, Power, Duration;

    public virtual string getName { get { return "Расплавленный залп"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/MagicShock.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;
        if (UnityEngine.Random.Range(0f, 1f) <= Chance)
        {
            instance.listEffects.Add(new MagicShockDebufEffect(_GradeParam, Duration));
            new TextOverlay(instance.srcUnit.getPosition, "Magic Shock!", new Color(0.75f, 0f, 1f, 1f), false);
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override bool Apply(Ability ability)
    {
        ability.effect.baseEffects.Add(new MagicShockDebufEffect(_GradeParam, Duration));
        return true;
    }

    public override bool Cancel(Ability ability)
    {
        int id = ability.effect.baseEffects.FindIndex(i => i.IDStack == IDEffect.MagicShock);
        if (id>-1)
        {
            ability.effect.baseEffects.RemoveAt(id);
        }
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("С вероятностью {0}% накладывает \"Магический шок\", который на {2}% увеличивает весь магический урон, получаемый целью на {1} с",
            Chance * 100f, Duration, Power * 100f);
    }

    public override void SetProperties(object[] param)
    {
        Chance = (float)param[0] / 100f;
        Power = (float)param[1] / 100f;
        Duration = (float)param[2];
        _GradeParam = Power;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("С вероятностью {0}% накладывает \"Магический шок\", который на {2}(<color=#19c62e>+{3}</color>)% увеличивает весь магический урон, получаемый целью на {1} с",
                Chance * 100f, Duration, Power * 100f,
                System.Math.Round(Power * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, Power);
    }
}
