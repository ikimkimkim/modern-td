﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class UpPoisonCountTik : baseProperties
{
    public float Multiplier;

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceLastE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;
        PoisonDotEffect pe = instance.listEffects.Find(i => i is PoisonDotEffect) as PoisonDotEffect;
        if(pe != null)
            pe.countTik *= Multiplier;
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceLastE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return "Длительность действия яда и суммарный урон за все время увеличиваются в "+Multiplier+" раз";
    }

    public override void SetProperties(object[] param)
    {
        Multiplier = (float)Convert.ToDouble(param[0]);
    }
}
