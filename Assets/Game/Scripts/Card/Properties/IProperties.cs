﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

/*public interface IProperties
{

    bool ApplyIsSampleTower { get; }

    void SetProperties(object[] param);

    bool Apply(Unit unit);

    bool Cancel(Unit unit);

    bool Apply(Ability ability);

    bool Cancel(Ability ability);

    string GetTextInfo();

}*/

public interface INameAndIcon
{
    string getName { get; }
    string getUrlIcon { get; }
}

public interface IRangeActionProperties
{
    float getRangeAction { get; }
}

/*public interface IPropertiesGrade
{
    string GetTextInfo(int level);
    void SetLevel(int level);
}
*/

public abstract class baseProperties
{
    public virtual bool ApplyIsSampleTower { get { return false; } }
    
    public virtual bool Apply(Unit unit)
    {
        throw new System.NotImplementedException("Cannot Apply property to Unit! " + GetType());
    }

    public virtual bool Cancel(Unit unit)
    {
        throw new System.NotImplementedException("Cannot Cancel property to Ability! " + GetType());
    }

    public virtual bool Apply(Ability ability)
    {
        throw new System.NotImplementedException("Cannot Apply property to Ability! " + GetType());
    }

    public virtual bool Cancel(Ability ability)
    {
        throw new System.NotImplementedException("Cannot Cancel property to Ability! " + GetType());
    }

    public abstract string GetTextInfo();

    public abstract void SetProperties(object[] param);
}

public abstract class basePropertiesGrade : baseProperties
{
    protected float _multiplierLevel = 0.1f, _GradeParam;

    public virtual string GetTextInfo(int level)
    {
        return GetTextInfo();
    }
    public abstract void SetLevel(int level);

    protected void SetLevel(int level, float paramGrade)
    {
        _GradeParam = paramGrade * (1f + _multiplierLevel * level);
    }
}


public interface IPropertiesEvent
{
    void SetListener(int id, EventHandler<eventPropertiesArgs> eventUpdate);
    void RemovListener(EventHandler<eventPropertiesArgs> eventUpdate);
    eventPropertiesArgs getStatus();
}

public class eventPropertiesArgs:EventArgs
{
    public int id;
    public float TimeDuration;
    public float Cooldown;
}
