﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class NewBaseRange : baseProperties
{
    public float newRange;

    public override bool ApplyIsSampleTower { get { return true; } }

    public override bool Apply(Unit unit)
    {
        unit.stats[0].range = newRange;
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        unit.stats[0].range = unit.MyCard.stats[0].range;
        return true;
    }

    public override string GetTextInfo()
    {
        return "Радиус атаки: " + newRange + "";
    }

    public override void SetProperties(object[] param)
    {
        newRange = (float)Convert.ToDouble(param[0]);
    }
}
