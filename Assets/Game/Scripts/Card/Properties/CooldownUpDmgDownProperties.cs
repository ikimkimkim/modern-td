﻿using UnityEngine;
using System.Collections;
using TDTK;

public class CooldownUpDmgDownProperties : basePropertiesGrade, INameAndIcon
{
    public float cooldown, dmgMultiplier;

    public int idStat;
    public string getName { get { return "Ультразвуковой калибр"; } }
    public string getUrlIcon { get { return "EffectIcon/CooldownUpDmgDown.png"; } }

    public override bool ApplyIsSampleTower { get { return true; } }

    public override bool Apply(Unit unit)
    {
        idStat = unit.currentActiveStat;
        unit.stats[idStat].damageMin *= (1f - _GradeParam);
        unit.stats[idStat].damageMax *= (1f - _GradeParam);
        unit.stats[idStat].cooldown /= (1f + cooldown);
        return true;
    }



    public override bool Cancel(Unit unit)
    {
        unit.stats[idStat].damageMin /= (1f - _GradeParam);
        unit.stats[idStat].damageMax /= (1f - _GradeParam);
        unit.stats[idStat].cooldown *= (1f + cooldown);
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("+{0}% скорострельность, -{1}% урон",cooldown*100f,dmgMultiplier*100f);
    }

    public override void SetProperties(object[] param)
    {
        cooldown = (float)(param[0]) / 100f;
        dmgMultiplier = (float)(param[1])/100f;
        _GradeParam = dmgMultiplier;
        _multiplierLevel = -0.1f;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("+{0}% скорострельность, -{1}(<color=#19c62e>{2}</color>)% урон", cooldown * 100f, 
                dmgMultiplier * 100f,
                System.Math.Round(dmgMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmgMultiplier);
    }
}
