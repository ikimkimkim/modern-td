﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class ChangeTargerMode : baseProperties
{
    public _TargetMode targetMode;
    public _TargetMode oldTargetMode;

    public override bool Apply(Unit unit)
    {
        oldTargetMode = unit.targetMode;
        unit.targetMode = targetMode;
        unit.StartCoroutine(unit.ScanForTargetRoutine());
        return true;
    }

    public override bool Cancel(Unit unit)
    {
        unit.targetMode = oldTargetMode;
        unit.StartCoroutine(unit.ScanForTargetRoutine());
        return true;
    }

    public override string GetTextInfo()
    {
        switch(targetMode)
        {
            case _TargetMode.Air:
                return "Атакует только воздушные цели";
            case _TargetMode.Ground:
                return "Атакует только наземные цели";
            case _TargetMode.Hybrid:
                return "Атакует воздушные и наземные цели";
        }

        return "Error: targetmod is "+targetMode;
        
    }

    public override void SetProperties(object[] param)
    {
        targetMode =  (_TargetMode) Convert.ToInt32(param[0]);
    }
}
