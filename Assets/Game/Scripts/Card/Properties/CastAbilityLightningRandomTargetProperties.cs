﻿using UnityEngine;
using System.Collections;
using TDTK;
using Translation;

public class CastAbilityLightningRandomTargetProperties : basePropertiesGrade, INameAndIcon
{
    public delegate void OnCastAbilityDelegate(Ability ability, Unit root, Unit target);
    public static event OnCastAbilityDelegate OnCastAbilityE;

    public int idAbility;

    public float coolddown;
    public float dmgMultiplier;

    public bool bApply = false;
    public Ability ability;
    public Unit unitlink;

    public Coroutine _corCast;

    public string getName { get { return "Cлучайный разряд"; } }
    public string getUrlIcon { get { return "EffectIcon/CastAbilityLightningRandomTarget.png"; } }

    public override bool Apply(Unit unit)
    {
        if (bApply == false)
        {
            unitlink = unit;
            bApply = true;
            _corCast = unit.StartCoroutine(Cast());
        }
        return true;
    }

    public void Shot(Unit target)
    {
        ability.effect.damageMax = unitlink.stats[unitlink.currentActiveStat].damageMax * _GradeParam;
        ability.effect.damageMin = unitlink.stats[unitlink.currentActiveStat].damageMin * _GradeParam;

        AbilityManager.instanceCastAbility(ability, target.thisT.position);

        new TextOverlay(unitlink.getPosition, "Magic!", new Color(1f, 0.5f, 0, 1f), false);

        if (OnCastAbilityE != null)
            OnCastAbilityE(ability, unitlink, target);
    }

    public IEnumerator Cast()
    {
        Debug.Log("CALRT Cast start");
        LayerMask mask = 1 << LayerManager.LayerCreep() | 1 << LayerManager.LayerCreepF();
        UnitCreep unit;
        Collider[] cols;
        while (bApply)
        {
            yield return new WaitForSeconds(coolddown);

            cols = Physics.OverlapSphere(unitlink.thisT.position, unitlink.stats[unitlink.currentActiveStat].range, mask);
            if (cols.Length > 0)
            {
                unit = cols[Random.Range(0, cols.Length)].GetComponent<UnitCreep>();
                if(unit!=null)
                {
                    Shot(unit);
                }
            }
        }
        yield break;
    }

    public override bool Cancel(Unit unit)
    {
        if (bApply)
        {
            unitlink.StopCoroutine(_corCast);
            unitlink = null;
        }
        bApply = false;
        return false;
    }

    public override string GetTextInfo()
    {
        return string.Format("Раз в {0} сек бьет молнией по случайному врагу в радиусе атаки, нанося {1}% урона башни",
            coolddown,dmgMultiplier * 100f);
    }

    public override void SetProperties(object[] param)
    {
        idAbility = 9;

        var bdAbility = AbilityDB.Load();
        ability = bdAbility[idAbility].Clone();
        coolddown = (float)param[0];
        dmgMultiplier = (float)param[1] / 100f;
        _GradeParam = dmgMultiplier;
        bApply = false;

    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Раз в {0} сек бьет молнией по случайному врагу в радиусе атаки, нанося {1}(<color=#19c62e>+{2}</color>)% урона башни",
                coolddown, dmgMultiplier * 100f,
                System.Math.Round(dmgMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmgMultiplier);
    }
}
