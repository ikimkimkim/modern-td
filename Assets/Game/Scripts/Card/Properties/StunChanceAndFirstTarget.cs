﻿using UnityEngine;
using System.Collections;
using TDTK;

public class StunChanceAndFirstTarget : basePropertiesGrade, INameAndIcon
{

    public float StunChance;
    public float ChanceFirst;
    public float StunDuration;

    public string getName { get { return "Вакуумный взрыв"; } }
    public string getUrlIcon { get { return "EffectIcon/StunChanceAndFirstTarget.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (instance.isCloneSplash)
            return;

        float c = StunChance;
        if (instance.tgtUnit.GetListUnitDamageMy().Contains(instance.srcUnit) == false)
        {
            c = ChanceFirst;
        }

        if (UnityEngine.Random.Range(0f, 1f) <= c * instance.MultiplierChanceStun)
        {
            instance.stunned = true;
            instance.listEffects.Add(new StunControlEffect(_GradeParam));
            new TextOverlay(instance.srcUnit.getPosition, "Stun!", Color.yellow, false);
        }
    }

    public override bool Cancel(Unit unit)
    {
            unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("{0}% оглушить цель при попадании на {1} секунды. При первом попадании по цели шанс увеличивается до {2}%",
            StunChance*100f, StunDuration, ChanceFirst*100f);
    }

    public override void SetProperties(object[] param)
    {
        StunChance = (float)(param[0]) / 100f;
        ChanceFirst = (float)(param[1]) / 100f;
        StunDuration = (float)(param[2]);
        _GradeParam = StunDuration;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("{0}% оглушить цель при попадании на {1}(<color=#19c62e>+{3}</color>) секунды. При первом попадании по цели шанс увеличивается до {2}%",
                StunChance * 100f,
                StunDuration, ChanceFirst * 100f,
                System.Math.Round(StunDuration * _multiplierLevel * level, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, StunDuration);
    }
}
