﻿using UnityEngine;
using System.Collections;
using TDTK;

public class CritChanceUpMagicStatusProperties : basePropertiesGrade, INameAndIcon
{
    public float CritChance;
    public float CritdmgMultiplier;

    public string getName { get { return "Двойная мощность"; } }
    public string getUrlIcon { get { return "EffectIcon/CritChanceUpMagicStatusProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += Unit_myOutAttakInstanceFirstE;
        return true;
    }

    private void Unit_myOutAttakInstanceFirstE(AttackInstance instance)
    {
        if (UnityEngine.Random.Range(0f, 1f) <= instance.GetChanceCritical(getChance(instance.tgtUnit)))
        {
            instance.SetMultiplierDmgCritical(_GradeParam);
        }
    }

    public float getChance(IUnitAttacked target)
    {
        float c = CritChance;
        if (target.EffectsManager.isActiveEffect(IDEffect.FireDot))
            c += CritChance;
        if (target.EffectsManager.isActiveEffect(IDEffect.PoisonDot))
            c += CritChance;
        if (target.EffectsManager.isActiveEffect(IDEffect.SlowControl))
            c += CritChance;

        return c;
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= Unit_myOutAttakInstanceFirstE;
        return false;
    }

    public override string GetTextInfo()
    {
        return string.Format("{0}% шанс нанести {1}% урона, шанс повышается на {0}% за каждый магический статус, который есть на цели",
            CritChance * 100f, CritdmgMultiplier * 100f);
    }

    public override void SetProperties(object[] param)
    {
        CritChance = (float)(param[0]) / 100f;
        CritdmgMultiplier = (float)(param[1]) / 100f;
        _GradeParam = CritdmgMultiplier;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("{0}% шанс нанести {1}(<color=#19c62e>+{2}</color>)% урона, шанс повышается на {0}% за каждый магический статус, который есть на цели",
                CritChance * 100f, CritdmgMultiplier * 100f,
                System.Math.Round(CritdmgMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, CritdmgMultiplier);
    }
}
