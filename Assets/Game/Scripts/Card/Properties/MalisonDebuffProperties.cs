﻿using UnityEngine;
using System.Collections;
using TDTK;

public class MalisonDebuffProperties : basePropertiesGrade, INameAndIcon
{
    public float chance;
    public float duration;
    public float dmg;

    public IDEffect PropertiesUnitType; 
    public string getName { get { return "Проклятый снаряд"; } }
    public string getUrlIcon { get { return "EffectIcon/MalisonProperties.png"; } }

    public override bool Apply(Unit unit)
    {
        PropertiesUnitType = unit.MyCard.Properties.Find(i => i.ID == 2) != null ? IDEffect.SlowControl : IDEffect.none;
        if (PropertiesUnitType == IDEffect.none)
        {
            PropertiesUnitType = unit.MyCard.Properties.Find(i => i.ID == 7) != null ? IDEffect.FireDot : IDEffect.none;
        }
        if (PropertiesUnitType == IDEffect.none)
        {
            PropertiesUnitType = unit.MyCard.Properties.Find(i => i.ID == 8) != null ? IDEffect.PoisonDot : IDEffect.none;
        }

        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (Random.Range(0f, 1f) < _GradeParam && instance.tgtUnit.EffectsManager.isActiveEffect(IDEffect.MagicShock))// CountStatuc(instance.tgtUnit)>=2)
        {
            instance.listEffects.Add(new MalisonDebuffEffect(dmg, duration));
            new TextOverlay(instance.srcUnit.getPosition, "Doom!", Color.red, false);
        }
    }

    public int CountStatuc(Unit unit)
    {
        int count = 0;
        if (PropertiesUnitType != IDEffect.SlowControl && unit.EffectsManager.isActiveEffect(IDEffect.SlowControl))
            count++;
        if (PropertiesUnitType != IDEffect.FireDot && unit.EffectsManager.isActiveEffect(IDEffect.FireDot))
            count++;
        if (PropertiesUnitType != IDEffect.PoisonDot && unit.EffectsManager.isActiveEffect(IDEffect.PoisonDot))
            count++;

        return count;
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Если цель имеет \"Магический шок\" при попадании с вероятностью {1}% наложить \"Проклятие\", при котором магические статусы длятся бесконечно и цель получается +{0}% урона от всех источников урона",
            dmg * 100,chance*100f);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0] / 100f;
        dmg = (float)param[1] / 100f;
        duration = (float)param[2];
        _GradeParam = chance;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Если цель имеет \"Магический шок\" при попадании с вероятностью {1}(<color=#19c62e>+{2}</color>)% наложить \"Проклятие\", при котором магические статусы длятся бесконечно и цель получается +{0}% урона от всех источников урона",
            dmg * 100, chance * 100f, 
            System.Math.Round(chance * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, chance);
    }
}
