﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;

public class UpRange : baseProperties
{
    public float upRange;

    public int idStat;
    public UnitHero hero;

    public override bool ApplyIsSampleTower { get { return true; } }

    public override bool Apply(Unit unit)
    {
        idStat = unit.currentActiveStat;
        unit.stats[idStat].range = unit.stats[idStat].range * (1f + upRange/100f);
        if (unit.IsHero)
        {
            hero = unit.GetUnitHero;
            hero.unitUpdateLevel += unitUpdateLevel;
        }
        return true;
    }

    private void unitUpdateLevel(object sender, EventArgs e)
    {
        if (hero != null)
        {
            Cancel(hero);
            Apply(hero);
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.stats[idStat].range = unit.stats[idStat].range / (1f + upRange / 100f);
        if (hero != null)
        {
            hero.unitUpdateLevel -= unitUpdateLevel;
        }
        return true;
    }

    public override string GetTextInfo()
    {
        if (upRange >= 999)
            return "Очень большой радиус стрельбы";
        return "Радиус атаки увеличен на " + upRange+"%";
    }

    public override void SetProperties(object[] param)
    {
        upRange = (float)Convert.ToDouble(param[0]);
    }
}
