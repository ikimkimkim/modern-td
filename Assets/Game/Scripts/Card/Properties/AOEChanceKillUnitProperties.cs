﻿using UnityEngine;
using System.Collections;
using TDTK;

public class AOEChanceKillUnitProperties : basePropertiesGrade, INameAndIcon
{
    private const float TimeEffect=10;
    public float AOERange;
    public float Chance, dmgMultiplier;

    public Unit _unitLink;
    public string getName { get { return "Проникающая взрывчатка"; } }
    public string getUrlIcon { get { return "EffectIcon/AOEChanceKill.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += Unit_myOutAttakInstanceFirstE;

        return true;
    }

    private void Unit_myOutAttakInstanceFirstE(AttackInstance attackInstance)
    {
        attackInstance.listEffects.Add(new ExplosionAfterDeathDebaffEffect(Chance, AOERange, _GradeParam, TimeEffect));
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= Unit_myOutAttakInstanceFirstE;
        return true;
    }

    public override string GetTextInfo()
    {
        return string.Format("Накладывает дебафф на {3} сек который с вероятностью {0}% производит взрыв после смерти противника, нанося врагам в радиусе {2} урон в размере {1}% от максимального запаса здоровья",
            Chance * 100f, dmgMultiplier * 100f, AOERange, TimeEffect);
    }

    public override void SetProperties(object[] param)
    {
        Chance = (float)param[0] / 100f;
        AOERange = (float)(param[1]);
        dmgMultiplier = (float)param[2] / 100f;
        _GradeParam = dmgMultiplier;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("Накладывает дебафф на {3} сек который с вероятностью {0}% производит взрыв после смерти противника, нанося врагам в радиусе {2} урон в размере {1}(<color=#19c62e>+{4}</color>)% от максимального запаса здоровья",
                Chance * 100f, dmgMultiplier * 100f, AOERange, TimeEffect,
                System.Math.Round(dmgMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, dmgMultiplier);
    }
}
