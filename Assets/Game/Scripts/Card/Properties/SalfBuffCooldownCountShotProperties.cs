﻿using UnityEngine;
using System.Collections;
using TDTK;

public class SalfBuffCooldownCountShotProperties : basePropertiesGrade, INameAndIcon
{
    public float chance;
    public float Multiplier;
    public int CountShot;

    public string getName { get { return "Свинцовый дождь"; } }
    public string getUrlIcon { get { return "EffectIcon/Cooldown.png"; } }

    public override bool Apply(Unit unit)
    {
        unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if (Random.Range(0f, 1f) < chance && instance.srcUnit.EffectsManager.isActiveEffect(IDEffect.CooldownBuffShotCount)==false)
        {
            instance.srcUnit.EffectsManager.AddEffect(instance.srcUnit as Unit, new CooldownBuffCountShotEffect(_GradeParam, CountShot));
            new TextOverlay(instance.srcUnit.getPosition, "Rage!", Color.red, false);
        }
    }

    public override bool Cancel(Unit unit)
    {
        unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return false;
    }

    public override string GetTextInfo()
    {
        return string.Format("При выстреле с шансом {2}% может повысить свою скорострельность на {0}% на {1} выстрелов.", Multiplier * 100f, CountShot, chance * 100f);
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("При выстреле с шансом {2}% может повысить свою скорострельность на {0}(<color=#19c62e>+{3}</color>)% на {1} выстрелов.",
                Multiplier * 100f, CountShot, chance * 100f,
                System.Math.Round(Multiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, Multiplier);
    }

    public override void SetProperties(object[] param)
    {
        chance = (float)param[0] / 100f;
        Multiplier = (float)param[1] / 100f;
        CountShot = System.Convert.ToInt32(param[2]);
        _GradeParam = Multiplier;
    }
}
