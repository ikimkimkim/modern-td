﻿using UnityEngine;
using System.Collections;
using System;
using TDTK;

public class Crit : basePropertiesGrade, INameAndIcon//,IPropertiesGrade
{
    //Critical oldCrit;

    public int idStat;
    public UnitHero hero;
    
    public float CritChance;
    public float CritdmgMultiplier;

    public virtual string getName { get { return "Улучшенный боезапас"; } }
    public virtual string getUrlIcon { get { return "EffectIcon/Crit.png"; } }

    public override bool ApplyIsSampleTower { get { return false; } }

    public override bool Apply(Unit unit)
    {
            unit.myOutAttakInstanceFirstE += ApplyInAttackInstance;
        return true;
    }

    public void ApplyInAttackInstance(AttackInstance instance)
    {
        if(instance.isCloneSplash)
            return;

        if(UnityEngine.Random.Range(0f,1f)<= instance.GetChanceCritical(CritChance))
        {
            instance.SetMultiplierDmgCritical(_GradeParam);
        }
    }

    public override bool Cancel(Unit unit)
    {
            unit.myOutAttakInstanceFirstE -= ApplyInAttackInstance;
        return true;
    }


    public override string GetTextInfo()
    {
        return string.Format("{0}% шанс нанести {1}% урона",
            CritChance * 100f, CritdmgMultiplier*100f);
    }

    public override void SetProperties(object[] param)
    {
        CritChance = (float)Convert.ToDouble(param[0])/100f;
        CritdmgMultiplier = (float)Convert.ToDouble(param[1])/100f;
        _GradeParam = CritdmgMultiplier;
    }

    public override string GetTextInfo(int level)
    {
        if (level > 0)
            return string.Format("{0}% шанс нанести {1}(<color=#19c62e>+{2}</color>)% урона",
                CritChance * 100f, 
                Math.Round(CritdmgMultiplier * 100f, 2),
                Math.Round(CritdmgMultiplier * _multiplierLevel * level * 100f, 2));
        else
            return GetTextInfo();
    }

    public override void SetLevel(int level)
    {
        SetLevel(level, CritdmgMultiplier);
    }
}
