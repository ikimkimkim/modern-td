﻿using UnityEngine;
using System.Collections;
using TDTK;
using System;

public class ContusionAura : baseProperties
{    
    public float Multiplier;
    public float Range;

    public GameObject prefab;
    public GameObject go;

    public override string GetTextInfo()
    {
        return String.Format("Ауря замедляет скорость атаки всех противников на {0}% в радиусе {1}",
           (1f - Multiplier) * 100f, Range);
    }


    public override void SetProperties(object[] param)
    {
        Multiplier = (float)(1f + Convert.ToDouble(param[0]) / 100f);
        Range = (float)(Convert.ToDouble(param[1]));
        var bdeffect = EffectDB.Load();
        prefab = bdeffect[7];
    }
}
