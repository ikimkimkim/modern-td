﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardUI : MonoBehaviour
{
    [SerializeField] private Image _fonCard;

    [SerializeField] private Image _icon;

    public void SetCard(Card card)
    {

        StartCoroutine(TextureDB.instance.load(Card.UrlFonCard[(int)card.rareness], setSpriteFonCard));
        _icon.sprite = card.iconSprite;
    }

    public void setSpriteFonCard(Sprite sprite)
    {
        if (_fonCard != null)
            _fonCard.sprite = sprite;
    }
}
