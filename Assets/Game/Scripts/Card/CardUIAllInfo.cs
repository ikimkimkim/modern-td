﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using TDTK;
using Translation;

public class CardUIAllInfo : MonoBehaviour
{
    public static event Action OnShow, OnHide;
    public static event Action StartUpgradeCard, EndUpgradeCard;
    public enum Mode { info, gameBild, gameUpgrade, ApplayUpgrade, onlyCancel }

    public GameObject Popup;

    public Sprite[] iconsLevel;

    public GameObject PanelDPS, PanelDPA, PanelDamagType, PanelCD, PanelRadius,
        PanelTextAbility, ManaPanel, SlowPanel, DurationPanel, CooldownAbPanel;

    public Text NameAndLevel,
        DPS, DPSadd,
        DPA, DPAadd, DPAtext,
        DamagType,
        CD, CDadd,
        Radius, RadiusAdd,
        TextAbility,
        Mana, TimeMana, TimeManaAdd,
        Duration, DurationAdd,
        CooldownAb,
        Slow, SlowAdd;

    public Image[] iconLevelSetting;
    public Image[] iconLevelSettingAbility;

    public static CardUIAllInfo instance;
    public static Card selectCard { get; private set; }

    public CardInfoUI cardInfoUI;

    public RectTransform PanelContent;
    public GameObject PrefabPropertiesInfo, PrefabAbilityInfo;
    public RectTransform PanelPropertiesInfo;

    public GameObject ButtonUpgrade, ButtonSoldInUpgradePanel;
    public Button infoButtonUpgrade, infoButtonSold, infoButtonClose;
    public Text ButtonUpgradeText, ButtonSoldCardText, ButtonSoldTowerText;
    public GameObject ButtonUpgradeIconGold;
    public GameObject PanelInfo, PanelBildMode, PanelUpgradeMode, PanelApplayUpgrade, PanelOnlyCancel;

    public Scrollbar Scroll;

    public GameObject TooltipLowCardCount;
    public Text TextTooltipLowCardCount;

    public GameObject TooltipNotCardCountSela;
    public Text TextTooltipNotCardCountSela;

    public SkillPanelInCardAllInfo skillPanel;

    private Animator AnimatorController;
    private bool AnimProgressBar = false;
    public bool isUpgradeNow = false;

    public List<GameObject> goPropertiesInfo;

    private Mode lastMode;
    public AudioSource Audio, AudioClickParam;

    [Header("Mini panel card upgrade")]
    public GameObject PanelCardUpgrade;
    public CardInfoUI cardInfoUpgrade;
    public Text NameCardUpgrade;
    public Text DespTextCardUpgrade;

    [Header("Tutorial")]
    public GameObject TutorialUpgradeButton;
    public bool lockHide;
    public bool lockSale;

    public static bool isShow {
        get {
            if (instance == null)
                return false;
            else
                return instance.Popup.activeSelf; }
    }

    void Awake()
    {
        instance = this;
        AnimatorController = GetComponent<Animator>();
        Hide();
    }

    public void Show(Card card, Mode mode)
    {
        MyLog.Log("Show CardUIAllInfo " + card);

        selectCard = card;

        if (card.Type == _CardType.Upgrade)
        {
            ShowMiniPanelCardUpgrade();
        }
        else
        {
            lastMode = mode;
            Popup.SetActive(true);
            setDataCard(selectCard, mode);
        }

        _AddStatusTutorial(StatusTutorial.waitOpen);
        if (OnShow != null)
            OnShow();
    }


    private void setDataCard(Card card, Mode mode)
    {

        PanelInfo.SetActive(false);
        PanelBildMode.SetActive(false);
        PanelUpgradeMode.SetActive(false);
        PanelApplayUpgrade.SetActive(false);
        PanelOnlyCancel.SetActive(false);
        TimeScaleManager.SetTimeScale(0);
        switch (mode)
        {
            case Mode.info:
                PanelInfo.SetActive(true);
                ButtonSoldCardText.text = "Разобрать";//+card.price_sell_one; // + card.Exp;
                if (card.next_level_count == 0)
                {
                    ButtonUpgradeText.text = "Улучшить";
                    ButtonUpgradeIconGold.SetActive(false);
                }
                else
                {
                    ButtonUpgradeText.text = "Улучшить за " + card.price_upgrade;
                    ButtonUpgradeIconGold.SetActive(true);
                }
                infoButtonSold.interactable = card.count > 0;
                infoButtonClose.interactable = true;

                if (card.count < card.next_level_count || card.next_level_count == 0)
                {
                    infoButtonUpgrade.interactable = false;
                }
                else
                {
                    infoButtonUpgrade.interactable = true;
                }
                break;
            case Mode.gameBild:
                PanelBildMode.SetActive(true);
                break;
            case Mode.gameUpgrade:
                PanelUpgradeMode.SetActive(true);
                if (card.NextLevelTower != null && card.NextLevelTower.Count > 0)
                {
                    ButtonUpgrade.SetActive(true);
                    //ButtonUpgradeText.text = "Улучшить за " + (System.Math.Floor(card.NextLevelTower[0].GetCost()[0] * 100) / 100f).ToString();
                }
                else
                {
                    ButtonUpgrade.SetActive(false);
                }

                ButtonSoldInUpgradePanel.SetActive(card.Type != _CardType.Ability);

                //ButtonSoldTowerText.text = "Продать за " + (System.Math.Floor(card.GetCost()[0] * GameControl.GetSellTowerRefundRatio() * 100) / 100f).ToString();
                break;
            case Mode.ApplayUpgrade:
                PanelApplayUpgrade.SetActive(true);
                break;
            case Mode.onlyCancel:
                PanelOnlyCancel.SetActive(true);
                break;
        }

        cardInfoUI.SetInfoTower(card, false, null, AnimProgressBar);
        AnimProgressBar = false;

        NameAndLevel.text = card.unitName/*+","+card.LevelCard+" уровня"*/;



        for (int i = 0; i < goPropertiesInfo.Count; i++)
        {
            GameObject.Destroy(goPropertiesInfo[i]);
        }
        goPropertiesInfo.Clear();

        ManaPanel.SetActive(false);
        SlowPanel.SetActive(false);
        DurationPanel.SetActive(false);

        if (card.Type != _CardType.Upgrade)
        {
            if (card.stats.Count == 0 || card.stats[0].cost.Count == 0)
            {
                Debug.LogError("Card stats or cost is null");
                return;
            }
        }

        switch (card.Type)
        {
            case _CardType.Ability:
                PanelDPS.SetActive(false);
                PanelDPA.SetActive(false);
                PanelCD.SetActive(false);
                PanelDamagType.SetActive(false);
                PanelRadius.SetActive(false);
                PanelTextAbility.SetActive(true);
                PanelPropertiesInfo.gameObject.SetActive(true);
                CooldownAbPanel.SetActive(true);
                WriteDataAbility(card);
                break;
            case _CardType.Tower:
                PanelDPS.SetActive(true);
                PanelDPA.SetActive(true);
                PanelCD.SetActive(true);
                PanelDamagType.SetActive(true);
                PanelRadius.SetActive(true);
                PanelTextAbility.SetActive(false);
                PanelPropertiesInfo.gameObject.SetActive(true);
                CooldownAbPanel.SetActive(false);
                WriteDataTower(card);
                break;
            case _CardType.Hero:
                PanelDPS.SetActive(true);
                PanelDPA.SetActive(true);
                PanelCD.SetActive(true);
                PanelDamagType.SetActive(true);
                PanelRadius.SetActive(true);
                PanelTextAbility.SetActive(false);
                PanelPropertiesInfo.gameObject.SetActive(true);
                CooldownAbPanel.SetActive(false);
                WriteDataHero(card);
                break;
            case _CardType.Upgrade:
                PanelInfo.SetActive(false);
                PanelBildMode.SetActive(false);
                PanelUpgradeMode.SetActive(false);
                PanelApplayUpgrade.SetActive(false);
                PanelOnlyCancel.SetActive(true);

                PanelDPS.SetActive(false);
                PanelDPA.SetActive(false);
                PanelCD.SetActive(false);
                PanelDamagType.SetActive(false);
                PanelRadius.SetActive(false);
                PanelTextAbility.SetActive(true);
                PanelPropertiesInfo.gameObject.SetActive(false);
                CooldownAbPanel.SetActive(false);
                WriteDataUpgradeCard(card);
                break;
        }

        SetIconLevel(card);
        Scroll.value = 1;
        OnExitUpdateButton();
    }

    private void ShowMiniPanelCardUpgrade()
    {
        PanelCardUpgrade.SetActive(true);
        cardInfoUpgrade.SetInfoTower(selectCard);
        NameCardUpgrade.text = selectCard.unitName;
        DespTextCardUpgrade.text = selectCard.Desp; //.UpgradeData.get desp[selectCard.getIntRareness]; 
    }

    private void WriteDataUpgradeCard(Card card)
    {
        if (skillPanel != null)
        {
            skillPanel.gameObject.SetActive(false);
        }

        TextAbility.text = card.Desp;// card.UpgradeData.desp[card.getIntRareness];
    }

    private void SetIconLevel(Card card)
    {
        int lvl;
        switch (card.Type)
        {
            case _CardType.Tower:
            case _CardType.Hero:
                lvl = CraftLevelData.GetMyLevelGrade(card, -1);
                iconLevelSetting[0].gameObject.SetActive(lvl > 0);
                iconLevelSetting[0].sprite = iconsLevel[lvl];//Урон
                lvl = CraftLevelData.GetMyLevelGrade(card, -2);
                iconLevelSetting[1].gameObject.SetActive(lvl > 0);
                iconLevelSetting[1].sprite = iconsLevel[lvl];//Радиус
                lvl = CraftLevelData.GetMyLevelGrade(card, -3);
                iconLevelSetting[2].gameObject.SetActive(lvl > 0);
                iconLevelSetting[2].sprite = iconsLevel[lvl];//Скоростельность
                break;
            case _CardType.Ability:
                lvl = CraftLevelData.GetMyLevelGrade(card, -1);
                iconLevelSettingAbility[0].gameObject.SetActive(lvl > 0);
                iconLevelSettingAbility[0].sprite = iconsLevel[lvl];//Урон
                //iconLevelSettingAbility[1].sprite = iconsLevel[CraftLevelData.GetMyLevelGrade(card, -2)];//Радиус
                lvl = CraftLevelData.GetMyLevelGrade(card, -3);
                iconLevelSettingAbility[2].gameObject.SetActive(lvl > 0);
                iconLevelSettingAbility[2].sprite = iconsLevel[lvl];//Перезарятка

                break;
        }
    }

    private void WriteDataTower(Card card)
    {

        if (card.stats[0].damageMax == 0)
        {
            PanelDPS.SetActive(false);
            PanelDPA.SetActive(false);
            PanelCD.SetActive(false);
            PanelRadius.SetActive(false);
            PanelTextAbility.SetActive(true);
            PanelDamagType.SetActive(false);
            TextAbility.text = card.towerObj.GetComponent<UnitTower>().desp;
        }
        else
        {
            SetDPS_DPA_CD_R_data(card,card.stats[0]);

        }
        //levelCard.text = "Уровень карты:" + card.LevelCard;

        CardProperties p = card.Properties.Find(i => i.ID == 14);
        if(p!=null)
        {
            ManaPanel.SetActive(true);
            ResourceRegen rr = (ResourceRegen)p.properties;
            Mana.text = "+" + rr.Count;
            TimeMana.text = rr.Speed + "c";
            TimeManaAdd.gameObject.SetActive(false);
            //SetParam("","+" + rr.Count , " ед. маны за", rr.Speed + "c");
        }

        p = card.Properties.Find(i => i.ID == 15);
        if (p != null)
        {
            ResourceUpMax rr = (ResourceUpMax)p.properties;
            SetParam("","+" + rr.AddCount, "к максимальному запасу маны");
        }


        for (int i = 0; i < card.Properties.Count; i++)
        {
            p = card.Properties[i];
            if (p.ID == 14 || p.ID == 15)
                continue;
            if (p.Requirements.Find(g => g.ID == 5) != null)
                continue;

            SetProperties(p);
            if (p.ID == CardProperties.IDCoomplect)
            {
                foreach (CardProperties properCoomlect in ((Coomplect)p.properties).CoomplectProperties)
                {
                    SetProperties(properCoomlect);
                }
            }
        }

        if(skillPanel!=null)
        {
            skillPanel.gameObject.SetActive(true);
            skillPanel.SetData(card, card.Properties);
        }
    }

    private void SetDPS_DPA_CD_R_data(Card card, UnitStat stat, int textTopSize = 24, int textSize = 16)
    {
        int lvlD, lvlC, lvlR;
        lvlD = CraftLevelData.GetMyLevelGrade(card, -1);
        lvlR = CraftLevelData.GetMyLevelGrade(card, -2);
        lvlC = CraftLevelData.GetMyLevelGrade(card, -3);

        var statGrade = CraftLevelData.GetStatGrade(card, stat);
        float d = (stat.damageMin + stat.damageMax) / 2f;
        d = d / stat.cooldown;
        DPS.text = d.RoundDamage().ToString();
        DPS.fontSize = textTopSize;
        DPSadd.fontSize = textTopSize;
        if (lvlC > 0 || lvlD > 0)
        {
            float d2 = statGrade.damageMin + statGrade.damageMax;
            d2 = d2 / 2f;
            DPSadd.text = string.Format("(+{0})", (d2 / statGrade.cooldown - d).RoundDamage());
        }
        else
            DPSadd.text = "";


        DamagType.text = "Тип урона:" + DamageTable.GetAllDamageType()[card.damageType].name;


        DPAtext.text = "ед. урона за атаку";
        DPA.fontSize = textSize;
        DPAadd.fontSize = textSize;
        DPA.text = stat.damageMin.RoundDamage() + "-" + stat.damageMax.RoundDamage();
        DPAadd.gameObject.SetActive(lvlD > 0);
        if (lvlD > 0)
            DPAadd.text = string.Format("(+{0}-{1})",
                (statGrade.damageMin - stat.damageMin).RoundDamage(), (statGrade.damageMax - stat.damageMax).RoundDamage());
        else
            DPAadd.text = "";


        CD.text = Math.Round((1f / stat.cooldown), 2).ToString();
        CDadd.gameObject.SetActive(lvlC > 0);
        if (lvlC > 0)
            CDadd.text = string.Format("(+{0})", Math.Round(1f / statGrade.cooldown - 1f/stat.cooldown, 2));
        else
            CDadd.text = "";


        Radius.text = Math.Round(stat.range, 2).ToString();
        RadiusAdd.gameObject.SetActive(lvlR > 0);
        if (lvlR > 0)
            RadiusAdd.text = string.Format("(+{0})", Math.Round(statGrade.range - stat.range, 2).ToString());
        else
            RadiusAdd.text = "";
    }


    private void WriteDataAbility(Card c)
    {
        if(c.abilityObj==null)
        {
            Debug.LogError("Card ability is null!");
            return;
        }
        Translator trans = TranslationEngine.Instance.Trans;
        TextAbility.text = trans[c.abilityObj.GetDesp()];
        List<CardProperties> p;
        var stat = c.stats[0];
        var statNew = CraftLevelData.GetStatGrade(c);
        if (stat.damageMax > 0)
        {
            PanelDPA.SetActive(true);
            DPA.text = stat.damageMin.RoundDamage() + "-" + stat.damageMax.RoundDamage();
            DPAadd.text = (statNew.damageMin - stat.damageMin).RoundDamage() + "-" + (statNew.damageMax - stat.damageMax).RoundDamage();
            p = c.Properties.FindAll(i => i.ID == 8);
            if (p == null || p.Count==0)
            {
                //SetParam("", c.stats[0].damageMin + "-" + c.stats[0].damageMax, "ед. урона за атаку");
                DPAtext.text = "ед. урона за атаку";
            }
            else
            { //SetParam("", c.stats[0].damageMin + "-" + c.stats[0].damageMax, );
                DPAtext.text = "ед. урона за " + ((PoisonDot)p[0].properties).Duration + " секунд";
            }


            SetParam("", DamageTable.GetAllDamageType()[c.damageType].name, "тип урона");

            SetIconLevel(c);
        }

        p = c.Properties.FindAll(i => i.ID == 2);
        if(p!=null && p.Count>0)
        {
            
            Slow s = (Slow)p[c.LevelTower-1].properties;
            SlowPanel.SetActive(true);
            Slow.text = Mathf.Round((s.SlowMultiplier) * 1000) / 10f + "%";
            SlowAdd.gameObject.SetActive(false);
            //SetParam("",,  "замедление");

            DurationPanel.SetActive(true);
            Duration.text = s.SlowDuration + " сек";
            DurationAdd.gameObject.SetActive(false);

            //SetParam("", s.SlowDuration+ " сек",  "длительность");
        }

        CooldownAb.text = stat.cooldown + " сек";
        //SetParam("", c.stats[0].cooldown+ " сек",  "таймаут");

        CardProperties ostP;
        for (int i = 0; i < c.Properties.Count; i++)
        {
            ostP = c.Properties[i];
            if (ostP.ID == 14 || ostP.ID == 15 || ostP.ID == 8 || ostP.ID == 2)
                continue;
            if (ostP.Requirements.Find(g => g.ID == 5) != null)
                continue;
            SetProperties(ostP);
            if (ostP.ID == CardProperties.IDCoomplect)
            {
                foreach (CardProperties properCoomlect in ((Coomplect)ostP.properties).CoomplectProperties)
                {
                    SetProperties(properCoomlect);
                }
            }
        }

        if (skillPanel != null)
        {
            skillPanel.gameObject.SetActive(true);
            skillPanel.SetData(c, c.Properties);
        }
    }

    private void WriteDataHero(Card card)
    {
        SetDPS_DPA_CD_R_data(card, card.stats[0]);

        SetIconLevel(card);

        CardProperties p = card.Properties.Find(i => i.ID == 14);
        if (p != null)
        {
            ManaPanel.SetActive(true);
            ResourceRegen rr = (ResourceRegen)p.properties;
            Mana.text = "+" + rr.Count;
            TimeMana.text = rr.Speed + "c";
            TimeManaAdd.gameObject.SetActive(false);
            //SetParam("","+" + rr.Count , " ед. маны за", rr.Speed + "c");
        }

        p = card.Properties.Find(i => i.ID == 15);
        if (p != null)
        {
            ResourceUpMax rr = (ResourceUpMax)p.properties;
            SetParam("", "+" + rr.AddCount, "к максимальному запасу маны");
        }


        for (int i = 0; i < card.Properties.Count; i++)
        {
            p = card.Properties[i];
            if (p.ID == 14 || p.ID == 15)
                continue;
            if (p.Requirements.Find(g => g.ID == 5) != null)
                continue;
            SetProperties(p);
            if (p.ID == CardProperties.IDCoomplect)
            {
                foreach (CardProperties properCoomlect in ((Coomplect)p.properties).CoomplectProperties)
                {
                    SetProperties(properCoomlect);
                }
            }
        }

        if (skillPanel != null)
        {
            skillPanel.gameObject.SetActive(true);
            skillPanel.SetData(card, card.Properties);
        }
       /* if (skillPanel != null)
        {
            skillPanel.gameObject.SetActive(false);
        }*/
    }

    public void ShowInfoTooltipNotCardCountSela()
    {

        if (infoButtonSold.interactable == false)
        {
            if (isUpgradeNow == true)
            {
                TextTooltipNotCardCountSela.text = "Дождитесь улучшения карты";
            }
            else if (selectCard.count == 0)
            {
                TextTooltipNotCardCountSela.text = "Недостаточно карт";
            }

            TooltipNotCardCountSela.SetActive(true);
        }
    }

    public void ShowInfoTooltipLowCountCard()
    {
        if (infoButtonUpgrade.interactable == false)
        {
            if (isUpgradeNow == true)
            {
                TextTooltipLowCardCount.text = "Дождитесь улучшения карты";
            }
            else if(selectCard.next_level_count==0)
            {
                TextTooltipLowCardCount.text = "Достигнут максимальный уровень карты";
            }
            else 
            {
                TextTooltipLowCardCount.text = "Недостаточно карт";
            }
            TooltipLowCardCount.SetActive(true);
        }
    }

    public void OnHoverUpdateInfoButton()
    {
        if (selectCard.next_damage == null || infoButtonUpgrade.interactable == false)
            return;
        UnitStat next_stat;
        if (selectCard.next_damage.Length > 0)
        {
            next_stat = selectCard.stats[0].Clone();
            next_stat.damageMin = selectCard.next_damage[0];
            next_stat.damageMax = selectCard.next_damage[1];
        }
        else
            next_stat = selectCard.stats[0];

        SetDPS_DPA_CD_R_data(selectCard, next_stat, 26, 18);

        CardProperties p;
        if (selectCard.Type == _CardType.Ability)
        {
            p = selectCard.Properties.Find(i => i.ID == 2);
            if (p != null)
            {
                Slow s = (Slow)p.properties;
                DurationAdd.gameObject.SetActive(true);
                DurationAdd.text = "+" + (selectCard.next_damage[0] - s.SlowDuration);
            }
        }
        else if (selectCard.stats[0].damageMax <= 0)
        {
            p = selectCard.Properties.Find(i => i.ID == 14);
            if (p != null)
            {
                ResourceRegen rr = (ResourceRegen)p.properties;
                TimeManaAdd.gameObject.SetActive(true);
                TimeManaAdd.text = "-" + Math.Round((rr.Speed - selectCard.next_damage[0]) * 100f) / 100f + "c";
            }
         
        }

    }

    public void OnHoverUpdateButton()
    {
        if (selectCard.NextLevelTower == null || selectCard.NextLevelTower.Count < 1)
            return;

        SetDPS_DPA_CD_R_data(selectCard, selectCard.NextLevelTower[0].stats[0], 26, 18);

       

        if (selectCard.Type == _CardType.Ability)
        {
            List<CardProperties> p = selectCard.NextLevelTower[0].Properties.FindAll(i => i.ID == 2);
            if (p != null && p.Count>1)
            {
                float Dur = 0;
                float NewDur = 0;

                Slow s = (Slow)p[selectCard.NextLevelTower[0].LevelTower - 2].properties;
                Dur = s.SlowDuration;
                s = (Slow)p[selectCard.NextLevelTower[0].LevelTower - 1].properties;
                NewDur = s.SlowDuration;


                if (NewDur > 0)
                {
                    DurationAdd.gameObject.SetActive(true);
                    DurationAdd.text = "+" + (NewDur - Dur);
                }
            }
        }
        else
        {
            List<CardProperties> p = selectCard.NextLevelTower[0].Properties.FindAll(i => i.ID == 14);
            if (p != null && p.Count>1)
            {
                float Speed = 0;
                float NewSpeed = 0;

                ResourceRegen s = (ResourceRegen)p[selectCard.NextLevelTower[0].LevelTower - 2].properties;
                Speed = s.Speed;
                s = (ResourceRegen)p[selectCard.NextLevelTower[0].LevelTower - 1].properties;
                NewSpeed = s.Speed;


                if (NewSpeed > 0)
                {
                    TimeManaAdd.gameObject.SetActive(true);
                    TimeManaAdd.text = "-" + Math.Round((Speed - NewSpeed) * 100f) / 100f + "c";
                }
            }
        }
    }


    public void OnExitUpdateButton()
    {
        if (isUpgradeNow == true)
            return;

        /*DPSadd.gameObject.SetActive(false);
        DPSadd.text = "";
        DPAadd.gameObject.SetActive(false);
        DPAadd.text = "";
        CDadd.gameObject.SetActive(false);
        CDadd.text = "";
        RadiusAdd.gameObject.SetActive(false);
        RadiusAdd.text = "";*/
        SetDPS_DPA_CD_R_data(selectCard, selectCard.stats[0]);

        DurationAdd.gameObject.SetActive(false);
        TimeManaAdd.gameObject.SetActive(false);
        SlowAdd.gameObject.SetActive(false);
    }

    private void SetProperties(CardProperties proper)
    {
        GameObject propertInfo = Instantiate(PrefabPropertiesInfo, PanelPropertiesInfo);
        propertInfo.GetComponent<CardUIAllInfoProperties>().SetProperties(proper,selectCard);
        goPropertiesInfo.Add(propertInfo);
        propertInfo.transform.localScale = new Vector3(1, 1, 1);
    }

    private void SetParam(String text1, String text2,String text3="", String text4="")
    {
        GameObject ParamInfo = Instantiate(PrefabAbilityInfo, PanelPropertiesInfo) ;
        ParamInfo.transform.localScale = new Vector3(1, 1, 1);

        if (text1.Length > 0)
            ParamInfo.transform.GetChild(0).GetComponent<Text>().text = text1;
        else
            ParamInfo.transform.GetChild(0).gameObject.SetActive(false);

        if (text2.Length > 0)
            ParamInfo.transform.GetChild(1).GetComponent<Text>().text =text2;
        else
            ParamInfo.transform.GetChild(1).gameObject.SetActive(false);

        if (text3.Length > 0)
            ParamInfo.transform.GetChild(2).GetComponent<Text>().text = text3;
        else
            ParamInfo.transform.GetChild(2).gameObject.SetActive(false);

        if (text4.Length > 0)
            ParamInfo.transform.GetChild(3).GetComponent<Text>().text = text4;
        else
            ParamInfo.transform.GetChild(3).gameObject.SetActive(false);

        goPropertiesInfo.Add(ParamInfo);
    }

    public void Hide()
    {
        if (lockHide)
            return;

        Popup.SetActive(false);
        if(PanelCardUpgrade)
            PanelCardUpgrade.SetActive(false);
        UI.ClearSelectedTower();
        UIBuildButton.Hide();

        if (OnHide != null)
            OnHide();
    }

    public void SaleCard()
    {
        if (lockSale)
            return;
        CardSalePanel.instance.Show(selectCard);
        CardSalePanel.instance.HidePanel += CBSaleCard;
    }

    public void CBSaleCard(bool result)
    {
        if (result)
        {
            
            var c = PlayerManager.CardsList.Find(i => i.ID == selectCard.ID);
            if (c != null)
                Show(c, lastMode);
            else
                Hide();
        }
        CardSalePanel.instance.HidePanel -= CBSaleCard;
    }

    public void UpgradeGameCard()
    {
        /*CardUpgradeManager.instance.Show(selectCard.ID);
        CardUpgradeManager.instance.UpdateModeChange();
        CardAndLevelSelectPanel.instance.Hide();
        Hide();*/
        isUpgradeNow = true;
        infoButtonUpgrade.interactable = false;
        infoButtonClose.interactable = false;
        infoButtonSold.interactable = false;
        SetDPS_DPA_CD_R_data(selectCard, selectCard.stats[0],26, 18);
        PlayerManager.DM.UpgradeTower(selectCard.IDTowerData + 1, (int)selectCard.rareness + 1, resultUpgradeCard);
    }

    private Card UpdateInfoCard;

    public void resultUpgradeCard(ResponseData rp)
    {
        if (rp.ok == 0)
        {
            Debug.LogError("UpgradeTower Error: " + rp.error.code + " -> " + rp.error.text);
            if (rp.error.code.Contains("no_gold"))
            {
                StartCoroutine(ChangeColorGold());
            }
            else
            {
                TooltipMessageServer.Show(rp.error.text);
            }


            infoButtonSold.interactable = selectCard.count > 0;
            infoButtonClose.interactable = true;

            if (selectCard.count < selectCard.next_level_count || selectCard.next_level_count == 0)
            {
                infoButtonUpgrade.interactable = false;
            }
            else
            {
                infoButtonUpgrade.interactable = true;
            }
            isUpgradeNow = false;
        }

        if(rp.ok==1)
        {
            countOldDamag = new float[] { selectCard.stats[0].damageMin, selectCard.stats[0].damageMax };
            addCountOldDamag =    new float[] { selectCard.next_damage[0] - selectCard.stats[0].damageMin, selectCard.next_damage[1] - selectCard.stats[0].damageMax };

        }

        PlayerManager.SetPlayerData(rp.user);

        if (rp.ok == 1)
        {
            if (PlayerManager.isSoundOn() && Audio!=null)
                Audio.Play();
            AnimProgressBar = true;
            UpdateInfoCard = PlayerManager.CardsList.Find(i => i.ID == selectCard.ID);
            AnimatorController.SetBool("Upgrade", true);


            MyLog.Log("Start UpgradeCard");

            _AddStatusTutorial(StatusTutorial.startGrade);

            if (StartUpgradeCard != null)
                StartUpgradeCard();
        }
    }

    public void SetNewLevel()
    {
        cardInfoUI.level.text = (UpdateInfoCard.LevelCard).ToString();
    }

    public void StartUpdateInfo()
    {
        AnimatorController.SetBool("Upgrade", false);

        MyLog.Log("Start UpgradeInfo");

        int lvlD, lvlC, lvlR;
        lvlD = CraftLevelData.GetMyLevelGrade(selectCard, -1);
        lvlR = CraftLevelData.GetMyLevelGrade(selectCard, -2);
        lvlC = CraftLevelData.GetMyLevelGrade(selectCard, -3);

        UnitStat stat = selectCard.stats[0];
        UnitStat statGrade = CraftLevelData.GetStatGrade(selectCard);

        UnitStat next_stat = stat.Clone();
        next_stat.damageMin = selectCard.next_damage[0];
        next_stat.damageMax = selectCard.next_damage[1];
        UnitStat next_statGrade = CraftLevelData.GetStatGrade(selectCard, next_stat);



        float d = (stat.damageMin + stat.damageMax)/2f;
        float cd = 1f / stat.cooldown;
        float dps_stat = (d * cd).RoundDamage();

        d = (next_stat.damageMin + next_stat.damageMax) / 2f;
        cd = 1f / next_stat.cooldown;
        float dps_next_stat = (d * cd).RoundDamage();

        StartCoroutine(AnimationText(DPS, dps_stat, dps_next_stat));

        if (lvlD>0 || lvlC>0)
        {
            d = (statGrade.damageMin + statGrade.damageMax) / 2f;
            cd = 1f / statGrade.cooldown;
            float dps_statGrade = (d * cd).RoundDamage();

            d = (next_statGrade.damageMin + next_statGrade.damageMax) / 2f;
            cd = 1f / next_statGrade.cooldown;
            float dps_next_statGrade = (d * cd).RoundDamage();

            StartCoroutine(AnimationText(DPSadd, dps_statGrade-dps_stat, dps_next_statGrade-dps_next_stat,"(+{0})"));
        }

        StartCoroutine(AnimationText(DPA, stat.damageMin, stat.damageMax, next_stat.damageMin, next_stat.damageMax));
        if(lvlD>0)
            StartCoroutine(AnimationText(DPAadd, statGrade.damageMin - stat.damageMin, statGrade.damageMax - stat.damageMax,
                next_statGrade.damageMin - next_stat.damageMin, next_statGrade.damageMax - next_stat.damageMax, "(+{0}-{1})"));

        /*
        float d2 = next_stat.damageMin + next_stat.damageMax;
        if (selectCard.next_damage != null)
            d2 = selectCard.next_damage[0]* (1f + valueGrade * level) + selectCard.next_damage[1]* (1f + valueGrade * level);

        d2 = d2 / 2f;
        float cd2 = 1f / CurrentStat.cooldown;

        Show(UpdateInfoCard, lastMode);
        infoButtonUpgrade.interactable = false;
        infoButtonClose.interactable = false;
        infoButtonSold.interactable = false;


        StartCoroutine(UpgrateDPSInfo(DPS, DPSadd, (d * cd).RoundDamage(), ((d2 * cd2) - (d * cd)).RoundDamage()));
        if(selectCard.next_damage != null)
            StartCoroutine(UpgrateDPAInfo(DPA, DPAadd));

        */
    }

    IEnumerator AnimationText(Text text, float StartValue, float EndValue, string format="{0}")
    {
        float currentValue = StartValue;
        text.text = string.Format(format, currentValue);
        
        float duration = 1f;
        float inc_for_sec = (EndValue - StartValue) / duration;

        while (currentValue<EndValue)
        {
            currentValue += inc_for_sec * 0.05f;
            text.text = string.Format(format, currentValue.RoundDamage());
            yield return new WaitForSecondsRealtime(0.05f);
        }

        text.text = string.Format(format, EndValue);

        EndUpgreadCard();
        yield break;
    }

    IEnumerator AnimationText(Text text, float StartValue1,float StartValue2,float EndValue1, float EndValue2, string format = "{0}-{1}")
    {
        float currentValue1 = StartValue1;
        float currentValue2 = StartValue2;
        text.text = string.Format(format, currentValue1,currentValue2);

        float duration = 1f;
        float inc_for_sec1 = (EndValue1 - StartValue1) / duration;
        float inc_for_sec2 = (EndValue2 - StartValue2) / duration;

        while (currentValue1 < EndValue1)
        {
            currentValue1 += inc_for_sec1 * 0.05f;
            currentValue2 += inc_for_sec2 * 0.05f;
            text.text = string.Format(format, currentValue1.RoundDamage(), currentValue2.RoundDamage());
            yield return new WaitForSecondsRealtime(0.05f);
        }

        text.text = string.Format(format, EndValue1,EndValue2);

        EndUpgreadCard();
        yield break;
    }

    private float[] countOldDPS, addCountOldDPS;
    IEnumerator UpgrateDPSInfo(Text textValue, Text textAdd,float Count, float AddCount)
    {
        if (AudioClickParam.isPlaying == false && PlayerManager.isSoundOn())
            AudioClickParam.Play();
        int sec = 1;
        float inc = AddCount / sec;
        float NewAddCount = AddCount;
        float NewCount = Count;
        while(NewAddCount > 0)
        {
            NewAddCount -= inc* Time.unscaledDeltaTime;
            if (NewAddCount < 0)
                break;
            NewCount += inc* Time.unscaledDeltaTime;
            if (NewCount > Count + AddCount)
                NewCount = Count + AddCount;

            textValue.text = Math.Round(NewCount, 2).ToString();
            textAdd.text = string.Format("+" + Math.Round(NewAddCount, 2).ToString());

            yield return null;
        }

        textAdd.gameObject.SetActive(false);
        textAdd.text = "";
        textValue.text = (Count + AddCount).ToString();
        EndUpgreadCard();
    }


    private float[] countOldDamag, addCountOldDamag;
    IEnumerator UpgrateDPAInfo(Text textValue, Text textAdd/*, float[] Count, float[] AddCount*/)
    {
        if (AudioClickParam.isPlaying==false && PlayerManager.isSoundOn())
            AudioClickParam.Play();
        //9.9-14.3 +1-1.4
        MyLog.Log("UpgrateDPAInfo:" + countOldDamag[0] + "-" + countOldDamag[1] + " +" + addCountOldDamag[0] + "-" + addCountOldDamag[1]);
        int sec = 1;
        float inc = addCountOldDamag[0] / sec; //1
        float inc2 = addCountOldDamag[1] / sec;//1.4
        float[] NewAddCount = addCountOldDamag;
        float[] NewCount = countOldDamag;
        while (NewAddCount[0] > 0)
        {
            NewAddCount[0] -= inc * Time.unscaledDeltaTime;
            NewAddCount[1] -= inc2 * Time.unscaledDeltaTime;
            if (NewAddCount[0] < 0)
                break;
            NewCount[0] += inc * Time.unscaledDeltaTime;
            NewCount[1] += inc2 * Time.unscaledDeltaTime;

            textValue.text = Math.Round(NewCount[0], 2).ToString()+"-"+ Math.Round(NewCount[1], 2).ToString();
            textAdd.text = "+" + Math.Round(NewAddCount[0], 2).ToString()+"-"+Math.Round(NewAddCount[1], 2).ToString();

            yield return null;
        }

        textAdd.gameObject.SetActive(false);
        textAdd.text = "";
        textValue.text = (countOldDamag[0] + addCountOldDamag[0]).ToString()+"-"+ (countOldDamag[1] + addCountOldDamag[1]).ToString();
        EndUpgreadCard();
    }

    public void EndUpgreadCard()
    {
        MyLog.Log("EndUpgreadCard");
        if (AudioClickParam.isPlaying == true)
            AudioClickParam.Stop();
        isUpgradeNow = false;
        infoButtonClose.interactable = true;
        AnimatorController.SetBool("Upgrade", false);
        Show(PlayerManager.CardsList.Find(i => i.ID == selectCard.ID), lastMode);
    }


    IEnumerator ChangeColorGold()
    {
        Image gold = ButtonUpgradeIconGold.GetComponent<Image>();

        for (int i = 0; i < 4; i++)
        {
            gold.color = new Color(1, 0.7f, 0.7f, 1f);
            yield return new WaitForSecondsRealtime(0.1f);// StartCoroutine(TimeScaleManager.WaitForRealSeconds(0.1f));
            gold.color = Color.white;
            yield return new WaitForSecondsRealtime(0.1f);// StartCoroutine(TimeScaleManager.WaitForRealSeconds(0.1f));
        }
    }

    public void BildGameTower()
    {
        string exception = "";
        exception = BuildManager.BuildTower(selectCard);
        if (exception == "")
        {
            UIBuildButton.Hide();
            Hide();
        }
        else
            Debug.LogError(exception);
    }


    public void UpgradeGameTower()
    {
        if (selectCard.LevelCard < selectCard.NextLevelTower[0].LevelTower)
            return;
        setDataCard(selectCard.NextLevelTower[0], Mode.ApplayUpgrade);

    }

    public void ApplayUpgradeGameTower()
    {
        if (selectCard.Type != _CardType.Ability)
        {
            if (UITowerInfo.instance.UpgradeTower() == "")
            {
                Popup.SetActive(false);
                if (OnHide != null)
                    OnHide();
            }
            else
                StartCoroutine(ErrorUpgrade());
        }
        else
        {
            string s = UIAbilityButton.instance.OnUpgradeButton(selectCard);
            switch(s)
            {
                case "":
                    Hide();
                    break;
                case "Low Cost":
                    StartCoroutine(ErrorUpgrade());
                    break;
            }
        }
    }

    public void CancelUpgradeGameTower()
    {
        setDataCard(selectCard, Mode.gameUpgrade);
    }

    IEnumerator ErrorUpgrade()
    {
        for (int i = 0; i < 4; i++)
        {
            ButtonUpgradeText.color = new Color(1, 0.7f, 0.7f, 1f);
            yield return new WaitForSecondsRealtime(0.1f);// StartCoroutine(TimeScaleManager.WaitForRealSeconds(0.1f));
            ButtonUpgradeText.color = Color.white;
            yield return new WaitForSecondsRealtime(0.1f);// StartCoroutine(TimeScaleManager.WaitForRealSeconds(0.1f));

        }
    }

    public void SaleGameTower()
    {
        UITowerInfo.instance.OnSellButton();

        UIBuildButton.Hide();
        Hide();
    }



    void Update()
    {
        if(Popup.activeSelf)
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                Hide();
            }
        }
    }

    #region tutor
    private enum StatusTutorial { waitOpen, startGrade, end }
    [SerializeField] private StatusTutorial _tutorialStatus;
    private Action _endTutorialEvent;
    private bool isTutorial = false;


    public static void StartTutorial(Action endTutorial)
    {
        instance._StartTutorial(endTutorial);
    }

    private void _StartTutorial(Action endTutorial)
    {
        isTutorial = true;
        _endTutorialEvent = endTutorial;
        _tutorialStatus = StatusTutorial.waitOpen;
    }

    private void _AddStatusTutorial(StatusTutorial status)
    {
        if (isTutorial == false)
            return;
        if (_tutorialStatus != status)
            return;
        Debug.Log("_AddStatusTutorial");
        _tutorialStatus++;
        _UpdateStatus();
    }

    private void _UpdateStatus()
    {
        switch (_tutorialStatus)
        {
            case StatusTutorial.startGrade:
                _TutorialShowUpgrade(true);
                break;

            case StatusTutorial.end:
                _EndTutorial();
                break;
        }
    }

    private void _EndTutorial()
    {
        if (isTutorial == false)
            return;
        _TutorialShowUpgrade(false);
        isTutorial = false;
        _endTutorialEvent();
    }

    public void _TutorialShowUpgrade(bool active)
    {
        if (TutorialUpgradeButton.activeSelf != active)
            TutorialUpgradeButton.SetActive(active);
        lockHide = active;
        lockSale = active;
    }

    #endregion
}
