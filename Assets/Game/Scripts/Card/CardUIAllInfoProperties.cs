﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class CardUIAllInfoProperties : MonoBehaviour
{

    public Text TextInfo;
    public Text TextRequirementsInfo;
    public Image Icon;

    CardProperties properties;

    public void SetProperties(CardProperties proper, Card card)
    {
        properties = proper;

        string addtext = ""; string endtext = "";
        bool apply = true;
        if (properties.Requirements != null && properties.Requirements.Count>0)
        {
            for (int i = 0; i < properties.Requirements.Count; i++)
            {
                if (properties.Requirements[i].CanActiveProperties(card) == false)
                {
                    apply = false;
                    break;
                }
            }
            RequirementsProperties RP = properties.Requirements.Find(i => i.ID == 1);
            if (RP != null)
            {
                if((float) RP.Param[0]>1)
                    endtext = " (" + Convert.ToString(RP.Param[0]) + " ур. карты)";
            }

            RP = properties.Requirements.Find(i => i.ID == 2);
            if (RP != null)
            {
                if (Convert.ToDouble(RP.Param[0]) > 1)
                {
                    switch (card.Type)
                    {
                        case _CardType.Ability:
                            endtext = " (" + Convert.ToString(RP.Param[0]) + " ур. заклинания) ";
                            break;
                        case _CardType.Hero:
                            endtext = " (" + Convert.ToString(RP.Param[0]) + " ур. героя) ";
                            break;
                        case _CardType.Tower:
                            endtext = " (" + Convert.ToString(RP.Param[0]) + " ур. башни) ";
                            break;
                    }
                }
            }

            RP = properties.Requirements.Find(i => i.ID == 3);
            if (RP!=null)
            {
                addtext ="("+ Convert.ToString(RP.Param[1])+") ";
            }
        }

        if(apply)
            Icon.color = new Color(0.1f, 0.9f, 0.1f, 1f);
        else
            Icon.color = new Color(0.7f, 0.7f, 0.7f, 1f);

        if (properties.ID == CardProperties.IDCoomplect)
            Icon.gameObject.SetActive(false);

        TextInfo.text = addtext+ properties.getInfoText() + endtext;

        TextRequirementsInfo.text = "";
        foreach (RequirementsProperties Requirements in proper.Requirements)
        {
            TextRequirementsInfo.text += Requirements.requirements.GetTextInfo()+"\r\n";
        }

        
    }


}
