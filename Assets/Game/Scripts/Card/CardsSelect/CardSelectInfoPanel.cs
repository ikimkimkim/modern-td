﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class CardSelectInfoPanel : MonoBehaviour {


    public Text DamagText,RangeText, CDText;


    public void SetInfo(Card card)
    {
        DamagText.text = card.stats[0].damageMin + "-" + card.stats[0].damageMax;
        CDText.text = Math.Round(1f/card.stats[0].cooldown,2).ToString();
        RangeText.text = card.stats[0].range.ToString();
    }


}
