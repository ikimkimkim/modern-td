﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PanelBigPortal : MonoBehaviour
{

    public static float chestProchentOpen;
    public Dropdown LevelDifficulty;
    public Text RewardText;

    public void SelectNewDifficulty()
    {
        CardAndLevelSelectPanel.SetLevelDifficulty(LevelDifficulty.value);
        chestProchentOpen = Mathf.Ceil(700f * Mathf.Pow(0.99f, CardAndLevelSelectPanel.LevelDifficultySelect)) / 10f;
        RewardText.text = "Время открытия уменьшено до "+ chestProchentOpen + "%";
    }

    void OnEnable ()
    {
        LevelDifficulty.ClearOptions();
        List<string> index = new List<string>();
        for (int i = 1; i <= PlayerManager.CountLevelBigPortal; i++)
        {
            index.Add(i.ToString());
        }
	    LevelDifficulty.AddOptions(index);
        LevelDifficulty.value = PlayerManager.CountLevelBigPortal - 1;

        SelectNewDifficulty();
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
