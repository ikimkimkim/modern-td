﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class CardLevelPanel : MonoBehaviour {


    /*
    Обычная - 2/4/16
    Магическая - 4/8/32
    Редкая - 8/16/64
    Комплектная и легендарная - 16/32/128
    */

    public static CardLevelPanel instance;


    public Text levelText, levelProcentText;
    public Image levelProgressBar,AddLevelProgressBar;
    private RectTransform tAddLevelProgressBar,tLevelProgressBar;
    private RectTransform thisRect;
    private Card _selectCard;

    void Awake()
    {
        instance = this;
        tAddLevelProgressBar = AddLevelProgressBar.GetComponent<RectTransform>();
        tLevelProgressBar = levelProgressBar.GetComponent<RectTransform>();
        thisRect = GetComponent<RectTransform>();
    }

    void Start()
    {
        CardUpgradeManager.instance.AddCardResurse += AddCardResurse;
        CardUpgradeManager.OnUpdateCard += OnUpdateCard;
    }

    void OnDestroy()
    {
        CardUpgradeManager.instance.AddCardResurse -= AddCardResurse;
        CardUpgradeManager.OnUpdateCard -= OnUpdateCard;
    }

    public float new_P, P, select_P;
    public int new_UpLvl, UpLvl, select_UpLvl;
    public float SpeedMoveBar=1f;

    public void AddCardResurse(List<GameObject> resurse)
    {
        if (UpdateCard)
            return;
        
        //Card card;
        levelProgressBar.gameObject.SetActive(true);
        /*foreach (GameObject go in resurse)
        {
            card = go.GetComponent<CardInfoUI>().linkCard;

            if (card.damageType == _selectCard.damageType)
            {
                AddXP += card.Exp;
            }
            else
            {
                AddXP += Mathf.Floor(card.Exp / 2);
            }
        }*/
        //Debug.Log("LevelCard : " + LevelCard(_selectCard.rareness, _selectCard.Exp));

        new_P = 0;
        new_UpLvl = -1;
        do
        {
            new_UpLvl++;
            //new_P = (_selectCard.Exp + AddXP - expCard(_selectCard, new_UpLvl)) / (expCard(_selectCard, new_UpLvl + 1) - expCard(_selectCard, new_UpLvl));
        }
        while (new_P > 1);
        
        if (P >= 1 && new_UpLvl+new_P>=1)
            StartCoroutine(ChangColorProgressText());

        if(_selectCard.LevelCard+new_UpLvl>5)
        {
            new_P = 1;
            new_UpLvl = 5 - _selectCard.LevelCard;           
        }


        if (resurse.Count > 0)
            levelProcentText.text = System.Math.Round(select_P * 100f, 2) + " + " + System.Math.Round((new_UpLvl + new_P - select_P) * 100f, 2) + "%";
        else
            levelProcentText.text = System.Math.Round(select_P * 100f, 2) + "%";

    }

    public void SetInfo(Card card)
    {
        _selectCard = card;
        levelText.text = "Уровень " + card.LevelCard;

        //new_P = (_selectCard.Exp  - expCard(_selectCard)) / (expCard(_selectCard, 1) - expCard(_selectCard));
        P = new_P;
        select_P = new_P;
        UpLvl = 0;
        new_UpLvl = UpLvl;

        if(card.LevelCard>=5)
        {
            if (select_P > 1)
                select_P = 1;
        }

        levelProcentText.text = System.Math.Round(select_P * 100f, 2) +  "%";
        levelProgressBar.gameObject.SetActive(true);
        tLevelProgressBar.sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x * P,  0);
        tAddLevelProgressBar.sizeDelta = new Vector2(thisRect.sizeDelta.x * Mathf.Clamp(P, 0, 1f), 0);
        UpdateCard = false;
    }

    public float expCard(Card c,int Uplevel=0)
    {
        int sv = startValue(c.rareness);
        return sv * Mathf.Pow(2, c.LevelCard + Uplevel - 1);
    }


    public float LevelCard(_CardRareness rareness, float xp)
    {
        int sv = startValue(rareness);
        return Mathf.Log(xp / sv, 2) + 1;
    }
    private int startValue(_CardRareness rareness)
    {
        int startValue = 0;
        switch (rareness)
        {
            case _CardRareness.Usual:
                startValue = 2;
                break;
            case _CardRareness.Magic:
                startValue = 6;
                break;
            case _CardRareness.Rare:
                startValue = 18;
                break;
            case _CardRareness.Legendary:
            case _CardRareness.Complete:
                startValue = 54;
                break;
            default:
                Debug.LogError("NewxLevelCard card is not rareness indetificate");
                break;
        }
        return startValue;
    }

    public void OnUpdateCard()
    {
        P = select_P;
        UpLvl = 0;
        UpdateCard = true;
    }

    public bool UpdateCard = false;
    void FixedUpdate()
    {
        if (UpdateCard == false)
        {

            if (Mathf.Abs(P - new_P - new_UpLvl) > 0.01f * SpeedMoveBar && P <= 1 && P >= 0)
            {
                if (P > new_P + new_UpLvl)
                {
                    P -= Time.fixedDeltaTime * SpeedMoveBar;
                }
                else
                {
                    P += Time.fixedDeltaTime * SpeedMoveBar;
                }
            }
            else if (P > 1)
            {
                P = 1;
            }
            else
                P = new_P;

            tAddLevelProgressBar.sizeDelta = new Vector2(thisRect.sizeDelta.x * Mathf.Clamp(P, 0, 1f), 0);

        }
        else
        {
            if (new_UpLvl!=UpLvl)
            {
                if (new_UpLvl > UpLvl)
                {
                    if (P >= 1)
                    {
                        UpLvl++;
                        StartCoroutine(ChangColorLevelText());
                        P = 0;
                    }
                    else
                    {
                        P += Time.fixedDeltaTime * SpeedMoveBar;
                    }
                }
                else
                {
                    if (P <= 0)
                    {
                        UpLvl--;
                        //StartCoroutine(ChangColorLevelText());
                        P = 1;
                    }
                    else
                    {
                        P -= Time.fixedDeltaTime * SpeedMoveBar;
                    }
                }

                tAddLevelProgressBar.sizeDelta = new Vector2(thisRect.sizeDelta.x, 0);
            }
            else if(Mathf.Abs(new_P - P) > 0.01f * SpeedMoveBar)
            {
                if (new_P > P)
                {
                    P += Time.fixedDeltaTime * SpeedMoveBar;
                }
                else
                {
                    P -= Time.fixedDeltaTime * SpeedMoveBar;
                }

                tAddLevelProgressBar.sizeDelta = new Vector2(thisRect.sizeDelta.x * Mathf.Clamp(new_P, 0, 1f), 0);
            }
            else
            {
                if (P >= 1 - 0.01f * SpeedMoveBar)
                {
                    StartCoroutine(ChangColorLevelText());
                }
                P = new_P;
                UpLvl = 0;
                new_UpLvl = UpLvl;
                UpdateCard = false;
                CardUpgradeManager.instance.SelectCardAndLoad(_selectCard.ID);
            }


            levelText.text = "Уровень " + (_selectCard.LevelCard+ UpLvl);
            MyLog.Log(P.ToString());
            tLevelProgressBar.sizeDelta = new Vector2(thisRect.sizeDelta.x * Mathf.Clamp(P, 0, 1f), 0);
        }
    }

    IEnumerator ChangColorLevelText()
    {
        for (int i = 0; i < 4; i++)
        {
            levelText.color = Color.yellow;
            yield return new WaitForSeconds(0.1f);
            levelText.color = Color.white;
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator ChangColorProgressText()
    {
        for (int i = 0; i < 4; i++)
        {
            levelProcentText.color = Color.yellow;
            yield return new WaitForSeconds(0.1f);
            levelProcentText.color = Color.white;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
