﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class CardAndLevelSelectPanel : MonoBehaviour
{
    private enum ShowTypeEnum { All, Tower, Ability, Hero }
    
    public static MapType mapType { get; private set; }

    public static int MAX_COUNT_CARD_SELECT=6;
    
    public static int LevelDifficultySelect = 0;
    public static void SetLevelDifficulty(int Difficulty)
    {
        MyLog.Log("Set Dif:" + Difficulty);
        LevelDifficultySelect = Difficulty;
        if (instance != null)
        {
            //instance.ShowChest();
            instance.ChengeCountEnergy(); 
        }
    }

    public void setDifficulty(int def)
    {
        SetLevelDifficulty(def);
    }

    public Text textCountLeftSelectCard;

    public GameObject Panel;
    //public GameObject Loading;
    public EnergyPopap energyPopap;

    //public GameObject[] ListActiveObj;

    public Toggle[] toggleDifficulty;
    [SerializeField]
    private ToggleGroup toggleGroupDifficulty;

    public GameObject prefabCardInfo;
    public List<GameObject> AllCards;
    public RectTransform PanelAllCards;
    public GameObject LoadingCardWait;
    public Scrollbar scrollbarAllCards;
    public UIScrollRectHideObj rectHideObj;
    public UIGroupGameObject groupGameObject;

    [SerializeField]
    private CardInfoUI[] SelectedCardsTower = new CardInfoUI[6];
    [SerializeField]
    private CardInfoUI[] SelectedCardsAbility = new CardInfoUI[6];
    [SerializeField]
    private CardInfoUI[] SelectedCardsHero = new CardInfoUI[6];
    public CardInfoUI[] SelectedCard {
        get {
            CardInfoUI[] array = new CardInfoUI[18];
            for (int i = 0; i < 6; i++)
            {
                array[i] = SelectedCardsTower[i];
                array[i + 6] = SelectedCardsAbility[i];
                array[i + 12] = SelectedCardsHero[i];
            }
            return array;
        } }

    public GameObject PanelBigPortal, PanelLevelOrPortal;

    [SerializeField]
    private MiniSelectedCardUpgradeUI[] CardsBonusArray;

    public GameObject PanelReward, PanelLevelInfo, PanelTopLevelInfo;

    public Image ChestImage;
    public Text ChestTextInfo, ChestTextTime,NameMapText;
    public Button ButtonStartLevel;
    public Text textButtonStartLevel;

    public Text NameLevelInfo, NameLevelTopInfo;
    public Image LevelIcon,LevelTopIcon;
    public Text TopPlaceText, TopTimeText;

    public GameObject TooltipError;
    public Text textTooltipError;
    public TooltipInfoCard tooltipInfoCard;

    public static CardAndLevelSelectPanel instance { get; private set; }

    public static bool BigPortal { get { return mapType == MapType.Portal; } }
    public static bool Survival { get { return mapType == MapType.Survival; } }
    public static int IDLevel { get; set; }
    //public static int IDScane { get; set; }
    public static string BundleNameScene { get; set; }
    public static int IDMap { get; set; }
    public static int RND { get; set; }
    public static int[] StarsConfig { get; set; }
    public static bool isSetData{ get; private set; }
    public static int CountStars { get; private set; }
    public static string Name;

    public static bool isRefreshOrReturn { get { return isReturnLevel || isRefreshData; } }
    public static bool isRefreshData=false, isReturnLevel=false,
#if UNITY_EDITOR
        isLastLevelWin = true;
#else
        isLastLevelWin = false;
#endif
    public Text textNameLevel,textNameChest;

    public static event Action OnShow;
    public static event Action OnHide;
    public static event Action<MapType> OnStartLevel;
    public static event Action OnChangeSelectCard;

    public bool isShow { get { return Panel.activeSelf; } }

    List<MapDB.MapData> mapData;


    public ToggleCardLevelSelect[] listAllCardToggle, listSelectCardToggle;
    public _CardType IndexSelect = _CardType.Tower;

    [Header("Prize icon")]
    [SerializeField]
    private Image PrizesIcon;
    [SerializeField]
    private Color ActiveColor, DeactiveColor;
    [SerializeField]
    private GameObject PrizesCheck;

    private int[] CountTypeCard;

    [Header("Tutorial")]
    [SerializeField] private GameObject[] ArrowCardPanel;
    [SerializeField] private GameObject ArrowCardSelecter;
    [SerializeField] private GameObject BlockCard, BlockRigthPanel;

    public void ChangeSelectTrigger(int id)
    {
        if (id < 0 && id >= listSelectCardToggle.Length)
            return;
        if (listSelectCardToggle[id].isOn == false)
            return;

        IndexSelect = (_CardType)(id);
        listAllCardToggle[id + 1].isOn = true;
        if(ArrowCardPanel[id].activeSelf)
            _AddStatusTutorial();
    }

    private ShowTypeEnum ShowType = ShowTypeEnum.All;
    public void ChangeAllTrigger(int id)
    {
        if (id<0 && id >= listAllCardToggle.Length) return;
        if (listAllCardToggle[id].isOn == false) return;

        ShowType = (ShowTypeEnum)id;

        Load();
        CardChecked();
    }


    public void ShowTop(bool ShowTop)
    {
        Hide();
        UITopDayPanel.ShowTop = ShowTop;
        UITopDayPanel.TypeShow = IDLevel;
        MyLog.Log("Show top idType:" + IDLevel);
        TopManager.instance.ShowTopDay();
    }

    public static void SetDataLevel(string nameLevel, int levelID, string bundleName, int countStars, bool isRefresh=false)
    {
        Debug.Log("Name:" + nameLevel + " levelID:" + levelID + " bundleName:" + bundleName + " countStars:" + countStars + " Refresh:" + isRefresh);
        IDMap = 1;
        MapDB.MapData map=new MapDB.MapData();
        TimeSpan ts;
       switch (levelID)
        {
            case -3:
                mapType = MapType.Test;
                //IDMap = PlayerManager.topDaysData.now_info[levelID.ToString()].map_id;
                BundleNameScene = bundleName;
                //instance.NameLevelTopInfo.text = TopManager.getNameMap(IDMap, mapType);
                //instance.TopPlaceText.text = PlayerManager.topDaysData.now_info[levelID.ToString()].user_res.place.ToString();
                //ts = DateTime.Parse(PlayerManager.topDaysData.now_info[levelID.ToString()].dates.end) - DateTime.Now;
                //instance.TopTimeText.text = Math.Floor(ts.TotalHours) + ":" + (ts.Minutes < 10 ? "0" + ts.Minutes : ts.Minutes.ToString()) + ":" + (ts.Seconds < 10 ? "0" + ts.Seconds : ts.Seconds.ToString());


                break;
            case -2:
                mapType = MapType.Portal;
                IDMap = PlayerManager.topDaysData.now_info[levelID.ToString()].map_id;
                BundleNameScene = TopManager.getBundleMap(IDMap, mapType);
                instance.NameLevelTopInfo.text = TopManager.getNameMap(IDMap, mapType);
                instance.TopPlaceText.text = PlayerManager.topDaysData.now_info[levelID.ToString()].user_res.place.ToString();
                ts = DateTime.Parse(PlayerManager.topDaysData.now_info[levelID.ToString()].dates.end)- DateTime.Now;
                instance.TopTimeText.text = Math.Floor(ts.TotalHours)+ ":" + (ts.Minutes < 10 ? "0" + ts.Minutes : ts.Minutes.ToString()) + ":" + (ts.Seconds < 10 ? "0" + ts.Seconds : ts.Seconds.ToString());
                break;
            case -1:
                mapType = MapType.Survival;
                IDMap = PlayerManager.topDaysData.now_info[levelID.ToString()].map_id;
                BundleNameScene = TopManager.getBundleMap(IDMap, mapType);
                instance.NameLevelTopInfo.text = TopManager.getNameMap(IDMap, mapType);
                instance.TopPlaceText.text = PlayerManager.topDaysData.now_info[levelID.ToString()].user_res.place.ToString();
                ts = DateTime.Parse(PlayerManager.topDaysData.now_info[levelID.ToString()].dates.end) - DateTime.Now;
                instance.TopTimeText.text = Math.Floor(ts.TotalHours) + ":" + (ts.Minutes<10 ? "0"+ ts.Minutes : ts.Minutes.ToString()) + ":" + (ts.Seconds < 10 ? "0" + ts.Seconds : ts.Seconds.ToString());
                break;
            default:
                mapType = MapType.Company;
                IDMap = levelID;
                if (levelID != 0)
                    map = instance.mapData.Find(i => i.LevelID == levelID);
                else
                    map = instance.mapData.Find(i => i.NameBundle == bundleName);
                BundleNameScene = map.NameBundle;
                instance.NameMapText.text = map.Name;// TopManager.getNameMap(IDMap, mapType);
                break;
        }


        //if(isRefresh==false)
        //    LevelDifficultySelect = 0;

        //IDScane = sceneID;
        IDLevel = levelID;
        CountStars = countStars;
        isSetData = true;
        Name = nameLevel;

        instance.UpdateCountCard();

        if (mapType == MapType.Company)
        {
            if (CountStars >= 3)
            {
                if (isRefresh == false)
                    SetLevelDifficulty(1);
                instance.toggleDifficulty[1].interactable = true;
            }
            else
            {
                if (isRefresh == false)
                    SetLevelDifficulty(0);
                instance.toggleDifficulty[1].interactable = false;
            }
            instance.LevelIcon.sprite = map.Icon;
            instance.StartCoroutine(TextureDB.instance.load(map.getUrlIcon, (Sprite value) => { instance.LevelIcon.sprite = value; }));

            instance.InitPrizes(CountStars);
        }
        else
        {
            instance.LevelTopIcon.sprite = TopManager.getIconMap(IDMap, mapType);
            instance.StartCoroutine(TextureDB.instance.load(TopManager.getUrlIconMap(IDMap, mapType), (Sprite value) => { instance.LevelTopIcon.sprite = value; }));
            if (mapType == MapType.Survival)
            {
                for (int i = 0; i < instance.toggleDifficulty.Length; i++)
                {
                    instance.toggleDifficulty[i].interactable = true;
                }
            }
            instance.InitPrizes(-1);
        }

        if (instance != null)
        {
            //instance.ShowChest();
            instance.Show(nameLevel);
            instance.Load();

            instance.CardChecked();

        }


        if (OnShow != null)
            OnShow();
    }

    private void InitPrizes(int countStar)
    {
        PrizesIcon.gameObject.SetActive(countStar >= 0);
        PrizesIcon.color = countStar >= 3 ? ActiveColor : DeactiveColor;
        PrizesCheck.SetActive(countStar >= 5);
    }

    public void ChengeCountEnergy()
    {
        /*if (IDLevel == -2)
        {
            if(LevelDifficultySelect+1<10)
                textButtonStartLevel.text = "Играть за 1";
            else if (LevelDifficultySelect+1 < 20)
                textButtonStartLevel.text = "Играть за 2";
            else if (LevelDifficultySelect+1 < 30)
                textButtonStartLevel.text = "Играть за 3";
            else
                textButtonStartLevel.text = "Играть за 4";
        }
        else */
        if (IDLevel<0)
        {
            textButtonStartLevel.text = "Играть за 1";
        }
        else
        {
            if (LevelDifficultySelect + 1 == 2 && CompanyEnergyStarPerk.isActive)
                textButtonStartLevel.text = "Играть за 0";
            else
                textButtonStartLevel.text = "Играть за 1";
        }
    }


    public void setSpriteFonChest(Sprite sprite)
    {
        ChestImage.sprite = sprite;
    }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        mapType = MapType.Portal;
        AllCards = new List<GameObject>();
        mapData = MapDB.Load();
        if (isRefreshData==true)
        {
            SetDataLevel(Name, IDLevel, BundleNameScene,CountStars,true);
        }
    }

    public void SetDataLevelOldLevel()
    {
        SetDataLevel(Name, IDLevel, BundleNameScene, CountStars, true);
    }

    void Show(string NameLevel)
    {
        Panel.SetActive(true);
        PanelReward.SetActive(false);
        PanelLevelInfo.SetActive(false);
        PanelTopLevelInfo.SetActive(false);
        PanelBigPortal.SetActive(false);
        PanelLevelOrPortal.SetActive(false);
        textNameLevel.text = NameLevel;
        ChestTextInfo.text = "Максимальный приз:";
        ChestTextTime.text = "";

        int dif = LevelDifficultySelect;
        switch (mapType)
        {
            case MapType.Company:
                {
                    for (int i = 0; i < toggleDifficulty.Length; i++)
                    {
                        toggleDifficulty[i].isOn = false;
                    }
                    toggleDifficulty[dif].isOn = true;
                    //toggleGroupDifficulty.NotifyToggleOn(toggleDifficulty[dif]);

                    PanelLevelOrPortal.SetActive(true);
                    PanelLevelInfo.SetActive(true);
                }
                break;
            case MapType.Portal:
                PanelBigPortal.SetActive(true);
                PanelTopLevelInfo.SetActive(true);
                break;
            case MapType.Survival:
                {
                    dif = Mathf.Clamp(dif, 0, toggleDifficulty.Length-1);
                    for (int i = 0; i < toggleDifficulty.Length; i++)
                    {
                        toggleDifficulty[i].isOn = false;
                    }
                    toggleDifficulty[dif].isOn = true;
                    //toggleGroupDifficulty.NotifyToggleOn(toggleDifficulty[dif]);

                    PanelLevelOrPortal.SetActive(true);
                    PanelTopLevelInfo.SetActive(true);
                }
                break;
        }
    }

    public void Hide()
    {
        /*for (int i = 0; i < toggleDifficulty.Length; i++)
        {
            toggleDifficulty[i].isOn = false;
        }
        toggleDifficulty[1].isOn = true;*/

        Panel.SetActive(false);
        PanelBigPortal.SetActive(false);
        PanelLevelOrPortal.SetActive(false);

        if (OnHide != null)
            OnHide();
    }

    public void Load()
    {
        //GameObject go;

        //PanelAllCards.sizeDelta = new Vector2(0,15+ 125 * Mathf.Ceil(PlayerManager.CardsList.Count/6f));
        MAX_COUNT_CARD_SELECT = 6;

        bool bAbility = PlayerManager.CardsList.Find(i => i.Type == _CardType.Ability) != null;
        bool bHero = PlayerManager.CardsList.Find(i => i.Type == _CardType.Hero) != null;

        if (bAbility && bHero)
        {
            MAX_COUNT_CARD_SELECT = 10;
        }
        else if(bAbility || bHero)
            MAX_COUNT_CARD_SELECT = 8;

        switch (ShowType)
        {
            case ShowTypeEnum.All:
                StartCoroutine(loadCard(PlayerManager.CardsList.FindAll(i => i.Type != _CardType.Upgrade)));
                break;
            case ShowTypeEnum.Ability:
            case ShowTypeEnum.Hero:
            case ShowTypeEnum.Tower:
                StartCoroutine(loadCard(PlayerManager.CardsList.FindAll(i => i.Type == (_CardType)(ShowType - 1))));
                break;
        }

    }

    public IEnumerator loadCard(List<Card> cards)
    {
        int count = 0, countWait = 24;
        LoadingCardWait.SetActive(true);
        GameObject go;        
        count = 0;
        _cardNotSelect = null;
        for (int i = 0; i < cards.Count; i++)
        {
            if(i < AllCards.Count)
            {
                if (AllCards[i].activeSelf == false)
                    AllCards[i].SetActive(true);
                AllCards[i].GetComponent<CardInfoUI>().SetInfoTower(cards[i]);
                AllCards[i].name = string.Format("card_{0}", cards[i].unitName);
            }
            else
            {
                go = Instantiate(prefabCardInfo, PanelAllCards);
                go.transform.position = Vector3.one * -1000;
                go.transform.localScale = Vector3.one;
                go.GetComponent<CardInfoUI>().SetInfoTower(cards[i]);
                go.GetComponent<CardInfoUI>().PanelUpgrade = false;

                go.name = string.Format("card_{0}", cards[i].unitName);
                AllCards.Add(go);
            }

            if(_cardNotSelect == null && cards[i].disableInBuildManager)
            {
                _cardNotSelect = AllCards[i].transform;
            }

            count++;
            if (count > countWait)
            {
                yield return null;
                count = 0;
            }
        }

        for (int i = cards.Count; i < AllCards.Count; i++)
        {
            if (AllCards[i].activeSelf)
            {
                AllCards[i].GetComponent<CardInfoUI>().DeletCardInfo();
                AllCards[i].SetActive(false);
            }
        }

        LoadingCardWait.SetActive(false);
        groupGameObject.UpdateGroup(true);
        rectHideObj.ChangeCountObj(true);
        scrollbarAllCards.value = 1;
        yield break;
    }



    private void UpdateCountCard()
    {
        CountTypeCard = new int[3];
        for (int i = 0; i < 3; i++)
            CountTypeCard[i] = PlayerManager.CardsList.FindAll(f => f.Type == (_CardType)(i)).Count;
    }

    public void StartGameTest()
    {
        if (ShowOnlyAdmin.isAdmin)
        {
            LevelDifficultySelect = 0;

            LevelModification.Clear();
            for (int i = 0; i < CardsBonusArray.Length; i++)
            {
                if (CardsBonusArray[i].selectedCard != null)
                    LevelModification.AddCard(CardsBonusArray[i].selectedCard);
            }

            BundleManager.StartLoadingScene(BundleNameScene);
        }
    }


    public void StartGame()
    {
        if(mapType == MapType.Test)
        {
            StartGameTest();
            return;
        }

        isReturnLevel = false;
        isRefreshData = false;
        isLastLevelWin = false;

        if (isSetData == false)
            Debug.Log("LevelSelect: New data is not installed!");

        int levelDif = Mathf.CeilToInt(LevelDifficultySelect + 1);

        List<int[]> idTowers = new List<int[]>();
        foreach (Card c in PlayerManager.CardsList)
        {
            if(c.disableInBuildManager == false)
            {
                idTowers.Add(new int[] { c.IDTowerData + 1, (int)c.rareness + 1 });
            }
        }
        List<int[]> idCardBonus = new List<int[]>();
        for (int i = 0; i < CardsBonusArray.Length; i++)
        {
            if (CardsBonusArray[i].selectedCard != null)
                idCardBonus.Add(new int[] { CardsBonusArray[i].selectedCard.IDTowerData + 1, (int)CardsBonusArray[i].selectedCard.rareness + 1 } );
        }

        PlayerManager.DM.StartLevel(IDLevel, levelDif, idTowers, idCardBonus, resultStart);
    }

    private Coroutine errorCorShow;

    private void resultStart(ResponseData response)
    {
        if (response.ok == 1)
        {
            isSetData = false;
            Hide();
            PlayerManager.SetPlayerData(response.user);
            RND = response.rnd;
            StarsConfig = new int[3];
           
            if (response.stars_config != null)
            {
                foreach (var star_conf in response.stars_config)
                {
                    switch (star_conf.Key)
                    {
                        case "1":
                            StarsConfig[0] = star_conf.Value;
                            break;
                        case "2":
                            StarsConfig[1] = star_conf.Value;
                            break;
                        case "3":
                            StarsConfig[2] = star_conf.Value;
                            break;

                    }
                }
            }

            LevelModification.Clear();
            for (int i = 0; i < CardsBonusArray.Length; i++)
            {
                if (CardsBonusArray[i].selectedCard != null)
                    LevelModification.AddCard(CardsBonusArray[i].selectedCard);
            }

            BundleManager.StartLoadingScene(BundleNameScene);

            if (OnStartLevel != null)
                OnStartLevel(mapType);
        }
        else
        {
            Debug.LogError("Error startGame-> " + response.error.code + ": " + response.error.text);

            if (response.error.code == "no_energy")
            {
                energyPopap.Show("У вас закончилась энергия");
                Hide();
            }
            else
            {

                if (errorCorShow != null)
                    StopCoroutine(errorCorShow);
                errorCorShow = StartCoroutine(ShowError(response.error.text));
            }
        }
    }

    IEnumerator ShowError(string text)
    {
        Color c = new Color(1f, 0.1f, 0.1f, 1f);
        TooltipError.SetActive(true);
        textTooltipError.text = text;
        for (int i = 0; i < 4; i++)
        {
            yield return new WaitForSeconds(0.12f);
            textButtonStartLevel.color = c;
            yield return new WaitForSeconds(0.12f);
            textButtonStartLevel.color = Color.white;
        }
        yield return new WaitForSeconds(5);
        TooltipError.SetActive(false);
    }


    //int indexSelcet;

    public int getCountSelectCard(_CardType type)
    {
        if (indexSelect == null)
            return 999;
        switch(type)
        {
            case _CardType.Tower:
                return indexSelect[0] ;
            case _CardType.Ability:
                return indexSelect[1] ;
            case _CardType.Hero:
                return indexSelect[2] ;
        }
        return 999;
    }

    private int[] indexSelect;
    private Transform _cardNotSelect;
    public int getCountOstSelect { get { return countCardOstSelect; } }
    private int countCardOstSelect;
    public void CardChecked( bool update=false)
    {

        for (int i = 0; i < 6; i++)
        {
            switch (IndexSelect)
            {
                case _CardType.Tower:
                    SelectedCardsTower[i].DeletCardInfo();
                    break;
                case _CardType.Ability:
                    SelectedCardsAbility[i].DeletCardInfo();
                    break;
                case _CardType.Hero:
                    SelectedCardsHero[i].DeletCardInfo();
                    break;
            }
        }
        //indexSelcet = 0;
        indexSelect = new int[3];
        for (int i = 0; i < 3; i++)
            indexSelect[i] = 0;

        int countSelect = 0;
        for (int i = 0; i < PlayerManager.CardsList.Count; i++)
        {
            if (PlayerManager.CardsList[i].disableInBuildManager == false)
            {

                countSelect++;
                switch (PlayerManager.CardsList[i].Type)
                {
                    case _CardType.Tower:
                        if (indexSelect[0] >= 6) break;
                        if (IndexSelect == PlayerManager.CardsList[i].Type)
                            SelectedCardsTower[indexSelect[0]].SetInfoTower(PlayerManager.CardsList[i]);
                        indexSelect[0]++;
                        break;
                    case _CardType.Ability:
                        if (indexSelect[1] >= 6) break;
                        if (IndexSelect == PlayerManager.CardsList[i].Type)
                            SelectedCardsAbility[indexSelect[1]].SetInfoTower(PlayerManager.CardsList[i]);
                        indexSelect[1]++;
                        break;
                    case _CardType.Hero:
                        if (indexSelect[2] >= 6) break;
                        if (IndexSelect == PlayerManager.CardsList[i].Type)
                            SelectedCardsHero[indexSelect[2]].SetInfoTower(PlayerManager.CardsList[i]);
                        indexSelect[2]++;
                        break;
                }


            }
           /* else if(_cardNotSelect == null)
            {
                _cardNotSelect = 
                indexCardNotSelect = i;
            }*/
        }

        ButtonStartLevel.interactable = indexSelect[0] > 0 || indexSelect[1]>0 || indexSelect[2]>0;
        countCardOstSelect = MAX_COUNT_CARD_SELECT - countSelect;
        textCountLeftSelectCard.text = countCardOstSelect.ToString();

        int max=0, ost;
        for (int index = 0; index < 3; index++)
        {
            listSelectCardToggle[index].thisEnable = CountTypeCard[index] > 0;
            listAllCardToggle[index + 1].thisEnable = listSelectCardToggle[index].thisEnable;
            if (listSelectCardToggle[index].thisEnable)
            {
                max = Mathf.Min(/*CountTypeCard[0],*/ countCardOstSelect + indexSelect[index], 6);
                ost = (indexSelect[index]);
                listSelectCardToggle[index].SetCountCard(ost, max);

                for (int i = 5; i > max - 1; i--)
                {
                    switch(index)
                    {
                        case 0:
                            SelectedCardsTower[i].Block();
                            break;
                        case 1:
                            SelectedCardsAbility[i].Block();
                            break;
                        case 2:
                            SelectedCardsHero[i].Block();
                            break;
                    }
                }
            }
            
        }

        if (update)
        {
            _AddStatusTutorial();
            if (OnChangeSelectCard != null)
                OnChangeSelectCard();
        }
    }

    public void deSelectCard(int index)
    {
        switch(IndexSelect)
        {
            case _CardType.Tower:
                if (SelectedCardsTower[index].linkCard != null)
                    SelectedCardsTower[index].linkCard.disableInBuildManager = true;
                break;
            case _CardType.Ability:
                if (SelectedCardsAbility[index].linkCard != null)
                    SelectedCardsAbility[index].linkCard.disableInBuildManager = true;
                break;
            case _CardType.Hero:
                if (SelectedCardsHero[index].linkCard != null)
                    SelectedCardsHero[index].linkCard.disableInBuildManager = true;
                break;
        }
        
            CardChecked(true);
            Load();
        
    }

    TimeSpan ts;
    // Update is called once per frame
    void Update () {
        if (TutorialOpenChest.instance==null && CardUIAllInfo.isShow == false && Panel.activeSelf == true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Hide();
            }
        }

        if (mapType == MapType.Test)
            return;

        if (IDLevel < 0 && Panel.activeSelf==true)
        {
            ts = DateTime.Parse(PlayerManager.topDaysData.now_info[IDLevel.ToString()].dates.end) - DateTime.Now;
            if (ts.TotalSeconds > 0)
            {
                double H = Math.Floor(ts.TotalHours);
                TopTimeText.text = (H < 10 ? "0" + H : H.ToString()) + ":" + (ts.Minutes < 10 ? "0" + ts.Minutes : ts.Minutes.ToString()) + ":" + (ts.Seconds < 10 ? "0" + ts.Seconds : ts.Seconds.ToString());
            }
            else
            {
                TopTimeText.text = "00:00:00";
                //PlayerManager.InitializationСompleted+= TopManager.instance.UpdateTopInfo;
                SocialManager.UpdateInfoPlayer();
                Hide();
            }
        }
    }



    private enum StatusTutorialGetAbility { OpenAbility, SelectedCard, end }
    [SerializeField] private StatusTutorialGetAbility _statusTutorial;
    private Action _endTutorial;
    private bool isTutorial;
    private _CardType _typeCartAdd;
    public static void StartTutorial(_CardType cardTypeAdd, Action endTutorial)
    {
        instance._StartTutorial(cardTypeAdd,endTutorial);
    }
    private void _StartTutorial(_CardType cardTypeAdd, Action endTutorial)
    {
        if ((int)cardTypeAdd < 0 || (int)cardTypeAdd > 2)
        {
            endTutorial();
            return;
        }

        isTutorial = true;
        BlockRigthPanel.SetActive(true);
        _endTutorial = endTutorial;
        _typeCartAdd = cardTypeAdd;
        _statusTutorial = StatusTutorialGetAbility.OpenAbility;
        _UpdateStatusTutorial();
    }

    private void _AddStatusTutorial()
    {
        if (isTutorial == false)
            return;
        _statusTutorial++;
        _UpdateStatusTutorial();
    }

    private void _UpdateStatusTutorial()
    {
        switch(_statusTutorial)
        {
            case StatusTutorialGetAbility.OpenAbility:
                ArrowCardPanel[(int)_typeCartAdd].SetActive(true);
                BlockCard.SetActive(true);
                break;
            case StatusTutorialGetAbility.SelectedCard:
                if (_cardNotSelect == null)
                {
                    _EndTutorial();
                    return;
                }
                ArrowCardPanel[(int)_typeCartAdd].SetActive(false);
                BlockCard.SetActive(false);
                Debug.Log(_cardNotSelect.name + "" + _cardNotSelect.position);
                ArrowCardSelecter.transform.position = _cardNotSelect.position + Vector3.right*11;
                ArrowCardSelecter.SetActive(true);
                break;
            case StatusTutorialGetAbility.end:
                _EndTutorial();
                break;
        }
    }

    private void _EndTutorial()
    {
        if (isTutorial == false)
            return;
        isTutorial = false;
        foreach (var item in ArrowCardPanel)
        {
            if(item.activeSelf)
                item.SetActive(false);
        }
        if (BlockCard.activeSelf)
            BlockCard.SetActive(false);
        if (BlockRigthPanel.activeSelf)
            BlockRigthPanel.SetActive(false);
        ArrowCardSelecter.SetActive(false);
        if (_endTutorial != null)
            _endTutorial();
    }
}
