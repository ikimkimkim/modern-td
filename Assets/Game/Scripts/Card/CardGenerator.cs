﻿using UnityEngine;
using System.Collections;
using TDTK;

public class CardGenerator : MonoBehaviour {

   /* public static Card GenerateCard()
    {
        int r = Random.Range(0, 100);
        if(r<=60)
        {
            return GenerateCard(_CardRareness.Usual);
        }
        else if(r<=85)
        {
            return GenerateCard(_CardRareness.Rare);
        }
        else if (r<=95)
        {
            return GenerateCard(_CardRareness.Complete);
        }
        else
        {
            return GenerateCard(_CardRareness.Legendary);
        }
        
    }


    public static Card GenerateCard(_CardRareness rareness)
    {
        Card newCard = new Card();

        switch (rareness)
        {
            case _CardRareness.Usual:
                newCard = CardUsual(newCard);
                break;
        }

        return newCard;
    }


    private static Card CardUsual(Card c)
    {

        c.disableInBuildManager = true;
        c.unitName = "Usual;" + Random.Range(1, 999999);
        //t.prefabID = 3;
        /*if (c.stats.Count == 0)
            c.stats.Add(new UnitStat());
        c.stats[0].cooldown = Random.Range(0.1f, 3f);
        c.stats[0].damageMin = Random.Range(1, 10);
        c.stats[0].damageMax = Random.Range(c.stats[0].damageMin, c.stats[0].damageMin+10);
        c.stats[0].cost.Add(Random.Range(2,4));
        c.stats[0].range = Random.Range(7,10);
        c.stats[0].hit = 1;
        c.IDTowerData = Random.Range(0,19);
        //c.NextLevelTower = new System.Collections.Generic.List<UnitTower>();


        NextLevel(c);
        NextLevel(c.NextLevelTower[0]);
        NextLevel(c.NextLevelTower[0].NextLevelTower[0]);
        NextLevel(c.NextLevelTower[0].NextLevelTower[0].NextLevelTower[0]);
        
        c.NextLevelTower.Add(new Card());

        c.NextLevelTower[0].unitName = c.unitName + " L2";
        if (c.NextLevelTower[0].stats.Count == 0)
            c.NextLevelTower[0].stats.Add(new UnitStat());
        c.NextLevelTower[0].stats[0].cost.Add(c.stats[0].cost[0] + 10);
        c.NextLevelTower[0].stats[0].cooldown = c.stats[0].cooldown - Random.Range(0.1f, c.stats[0].cooldown / 2f);
        c.NextLevelTower[0].stats[0].damageMin = Random.Range(1, 10);
        c.NextLevelTower[0].stats[0].damageMax = Random.Range(c.stats[0].damageMin, c.stats[0].damageMin + 10);
        c.NextLevelTower[0].stats[0].range = 15;
        c.NextLevelTower[0].stats[0].hit = 1;
        c.NextLevelTower[0].LevelTower=c.LevelTower+1;
        c.NextLevelTower[0].IDTowerData = c.IDTowerData;
        
        return c;
    }
    */
    //Создания информации о след уровне
    public static void NextLevel(Card c)
    {
        c.NextLevelTower.Add(new Card());

        c.NextLevelTower[0].unitName = c.unitName;// " L"+ (c.LevelTower + 1);
        if (c.NextLevelTower[0].stats.Count == 0)
            c.NextLevelTower[0].stats.Add(new UnitStat());
        c.NextLevelTower[0].TowerType = c.TowerType;
        c.NextLevelTower[0].rareness = c.rareness;
        c.NextLevelTower[0].stats[0].cost.Add(c.stats[0].cost[0] + 10);
        c.NextLevelTower[0].stats[0].cooldown = c.stats[0].cooldown - Random.Range(0.1f, c.stats[0].cooldown / 2f);
        c.NextLevelTower[0].stats[0].damageMin = Random.Range(1, 10);
        c.NextLevelTower[0].stats[0].damageMax = Random.Range(c.stats[0].damageMin, c.stats[0].damageMin + 10);
        c.NextLevelTower[0].stats[0].range = 15;
        c.NextLevelTower[0].stats[0].hit = 1;
        c.NextLevelTower[0].LevelTower = c.LevelTower + 1;
        c.NextLevelTower[0].IDTowerData = c.IDTowerData;
        c.NextLevelTower[0].Properties = c.Properties;
        c.NextLevelTower[0].damageType = c.damageType;
        c.NextLevelTower[0].targetMode = c.targetMode;
        c.NextLevelTower[0].ID = c.ID;
        c.NextLevelTower[0].count = c.count;
        c.NextLevelTower[0].next_level_count = c.next_level_count;
        c.NextLevelTower[0].Type = c.Type;

    }
    
    public static void setPropertiesNextLevelCard(Card nextLevel, Card current)
    {
        nextLevel.Properties = current.Properties;
        foreach (Card c in nextLevel.NextLevelTower)
        {
            setPropertiesNextLevelCard(c, current);
        }
    }

    public static void setTowerObjNextLevelCard(Card nextLevel, Card current)
    {
        switch(current.Type)
        {
            case _CardType.Ability:
                nextLevel.SetAbilityObj(current.abilityObj,false);
                break;
            case _CardType.Tower:
                nextLevel.SetTowerObj(current.towerObj);
                break;
            case _CardType.Hero:
                nextLevel.SetHeroObj(current.heroObj);
                break;
            default:
                Debug.LogError("setTONLC: not type:" + current.Type);
                break;
        }

        nextLevel.prefabID = current.prefabID;
        foreach (Card c in nextLevel.NextLevelTower)
        {
            setTowerObjNextLevelCard(c, current);
        }
    }

   /* // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}*/
}
