﻿using UnityEngine;
using System.Collections;
using TDTK;
using System.Collections.Generic;

public class RequirementsProperties
{
    public int ID { get; private set; }
    public object[] Param { get; private set; }
    /*public static RequirenebtsProperties[] getListRequirenebts(string dataList)
    {
        if (dataList == "")
            return new RequirenebtsProperties[0];
        RequirenebtsProperties[] data = (RequirenebtsProperties[])StringSerializationAPI.Deserialize(typeof(RequirenebtsProperties[]), dataList);
        return data;
    }*/

    public IRequirements requirements;

    public RequirementsProperties(int id, object[] param)
    {
        ID = id;
        Param = param;
        switch (id)
        {
            case 1:
                requirements = new RLevelCard();
                break;
            case 2:
                requirements = new RLevelTower();
                break;
            case 3:
                requirements = new RTowerCoomlect();
                break;
            case 4:
                requirements = new RHavePropertieslCard();
                break;
            case 5:
                requirements = new RTowerUpgradeLevel();
                break;
            default:
                Debug.LogError("RequirementsProperties id is not exist!");
                break;
        }
        requirements.SetProperties(Param);
    }


    public bool CanActiveProperties(Card card)
    {
        if (requirements != null)
            return requirements.CanApply(card);
        else
            Debug.LogError("RequirenebtsProperties requirements is null!");


        return false;
    }
    

    public RequirementsProperties Clone()
    {
        RequirementsProperties clone = new RequirementsProperties(ID, Param);
        return clone;
    }
}
