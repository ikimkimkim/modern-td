﻿using UnityEngine;
using System.Collections;
using System.Linq;
using TDTK;
using System.Collections.Generic;
using UnityEngine.UI;

public enum _CardRareness { none=-1, Usual, Magic, Rare, Complete=3, Legendary=4 }
public enum _CardType { none = -1, Tower, Ability, Hero, Upgrade }

public class Card 
{
    public const int MAX_LEVEL=5;
    public static string[] UrlFonCard = new string[] {
        "Card/WhiteCard.png",
        "Card/BlueCard.png",
        "Card/YellowCard.png",
        "Card/GreenCard.png",
        "Card/GoldCard.png"
        };


    public GameObject towerObj { get; private set; }
    public Ability abilityObj { get; private set; }
    public GameObject heroObj { get; private set; }
    public UnitHero heroUnit { get; private set; }
    public CardUpgradeData UpgradeData { get; private set; }
    public string ID { get; set; }

    public _TowerType TowerType = _TowerType.Turret;
    public _CardType Type = _CardType.Tower;


    private bool _disableInBuildManager;
    public bool disableInBuildManager
    {
        get { return _disableInBuildManager; }
        set { _disableInBuildManager = value; /*Debug.LogWarning("Change Card "+ID+" disable:" + value);*/ }
    }
    public bool CheckOldBuild=false;
    public int prefabID { get { return _prefabId; } set { /*Debug.Log(ID+" SetPrefID:" + value);*/ _prefabId = value; } }
    private int _prefabId;
    public _CardRareness rareness = _CardRareness.Usual;
    public int getIntRareness {
        get
        {
            if (rareness == _CardRareness.Complete || rareness == _CardRareness.Legendary)
                return (int)_CardRareness.Complete;
            return (int)rareness;
        }
    }

    public _TargetMode targetMode = _TargetMode.Hybrid;
    public int damageType=0;
    public int IDTowerData = -1;
    public int LevelCard = 1;
    public int LevelTower = 1;

    public int count;
    public int AddCount;
    public int next_level_count;
    public float[] next_damage;
    public int price_upgrade;
    public int price_sell_one;

    public List<CardProperties> Properties;

    public List<UnitStat> stats;

    public List<Card> NextLevelTower;

    public string unitName;
    public Sprite iconSprite;


    private bool ignorePropertiesDB = false;
    public bool IgnorePropertiesDB { get { return ignorePropertiesDB; }
        set {
            if (NextLevelTower.Count > 0)
                NextLevelTower[0].ignorePropertiesDB = value;
            ignorePropertiesDB = value;
        }
    }


    public string Desp {
        get
        {
            switch(Type)
            {
                case _CardType.Upgrade:
                    return UpgradeData.GetDesp(rareness);
                default:
                    return "";                         
            }
        }
    }

    public string CraftDesp
    {
        get
        {
            switch (Type)
            {
                case _CardType.Upgrade:
                    return UpgradeData.GetCraftDesp(rareness);
                default:
                    return "";
            }
        }
    }

    public string ShortDesp
    {
        get
        {
            switch (Type)
            {
                case _CardType.Upgrade:
                    return UpgradeData.GetShortDesp(rareness);
                default:
                    return "";
            }
        }
    }

    public Color getColorRareness()
    {
        return getColorRareness(rareness);
    }

    public static Color getColorRareness(_CardRareness rareness)
    {
        Color c = Color.white;
        switch (rareness)
        {
            case _CardRareness.Rare:
                return Color.yellow;
            case _CardRareness.Magic:
                return new Color(0.1f, 0.1f, 1f, 1f);
            case _CardRareness.Legendary:
                return new Color(1f,0.8f,0f,1f);
            case _CardRareness.Complete:
                return Color.green;
        }
        return c;
    }

    public Card()
    {
        NextLevelTower = new List<Card>();
        Properties = new List<CardProperties>();
        stats = new List<UnitStat>();
    }

    public void initHero(TowersData towerData, GameObject HeroObj)
    {
        Type = _CardType.Hero;
        Init(towerData);

        if (HeroObj != null)
            SetHeroObj(HeroObj);

        AddPerkProperties();

        InitNextLevel(towerData);
    }

    public Card(TowersData towerData, Ability abilityObj)
    {
        Type = _CardType.Ability;
        //Debug.LogWarning("Select Card " + towerData.rarity + "_" + towerData.type + ":" + towerData.selected);
        Init(towerData);

        if (abilityObj != null)
            SetAbilityObj(abilityObj);


        AddPerkProperties();

        InitNextLevel(towerData);
    }


    public Card(TowersData towerData,GameObject setObj,_CardType Type,int prefabId)
    {
        prefabID = prefabId;
        switch(Type)
        {
            case _CardType.Tower:
                InitTower(towerData, setObj);
                break;
            case _CardType.Hero:
                initHero(towerData, setObj);
                break;

        }        
    }

    public Card(TowersData data, CardUpgradeData upgradeData)
    {
        Type = _CardType.Upgrade;
        UpgradeData = upgradeData;
        InitUpgradeCard(data);
    }

    private void InitUpgradeCard(TowersData data)
    {
        Init(data);
        iconSprite = UpgradeData.icon;
    }

    public void InitTower(TowersData towerData, GameObject setTowerObj)
    {
        Type = _CardType.Tower;
        TowerType = _TowerType.Turret;
        Init(towerData);

        if (setTowerObj != null)
            SetTowerObj(setTowerObj);

        if(IDTowerData != 12)
            AddPerkProperties();

        InitNextLevel(towerData);
    }

    private void Init(TowersData towerData)
    {
        NextLevelTower = new List<Card>();
        Properties = new List<CardProperties>();
        stats = new List<UnitStat>();
        stats.Add(new UnitStat());

        ID = towerData.rarity + "_" + towerData.type;
        unitName = towerData.name;

        count = towerData.count;
        next_damage = (float[]) towerData.next_damage;
        next_level_count = towerData.next_level_count;
        price_sell_one = towerData.price_sell_one;
        price_upgrade = towerData.price_upgrade;



        LevelCard = towerData.card_level;
        if (towerData.rarity == 0)
            Debug.LogError("Tower rarity is 0!");
        else
            rareness = (_CardRareness)(towerData.rarity - 1);
        if (towerData.type == 0)
        {
            Debug.LogError("Card: Type id can not be 0");
            IDTowerData = towerData.type;
        }
        else
            IDTowerData = towerData.type - 1;
        
        CheckOldBuild = towerData.selected == 1;
        disableInBuildManager = !CheckOldBuild;

        foreach (var prop in towerData.properties)
        {
            object[] param = new object[prop.Value.Length - 1];
            for (int i = 1; i < prop.Value.Length; i++)
            {
                param[i - 1] = prop.Value[i];
            }

            int idReqirement = (int)prop.Value[0];

            Properties.Add(new CardProperties(int.Parse(prop.Key), param, CraftLevelData.GetMyLevelGrade(this, int.Parse(prop.Key)), new RequirementsProperties[1]));
            if(idReqirement<0)
                Properties[Properties.Count - 1].Requirements[0] = new RequirementsProperties(5, new object[] { Mathf.Abs(idReqirement) });
            else
                Properties[Properties.Count - 1].Requirements[0] = new RequirementsProperties(2, new object[] { (idReqirement) });
        }


        if (towerData.set_properties.Count > 0)
        {
            CardProperties cp = new CardProperties(CardProperties.IDCoomplect, new object[] { towerData.set_id }, 0, new RequirementsProperties[0]);

            Coomplect comp = (Coomplect)cp.properties;
            comp.CoomplectProperties = new List<CardProperties>();
            int i = 0;
            foreach (var prop in towerData.set_properties)
            {
                object[] param = new object[prop.Value.Length - 1];
                for (int j = 1; j < prop.Value.Length; j++)
                {
                    param[j - 1] = prop.Value[j];
                }

                CardProperties cp2 = new CardProperties(int.Parse(prop.Key), param, CraftLevelData.GetMyLevelGrade(this, int.Parse(prop.Key)), new RequirementsProperties[1]);
                cp2.Requirements[0] = new RequirementsProperties(3, new object[] { towerData.set_id, prop.Value[0] });
                comp.CoomplectProperties.Add(cp2);
                i++;
            }

            Properties.Add(cp);
        }
    }

    private bool bAddPerk = false;
    private void AddPerkProperties()
    {
        if (bAddPerk == true) return;
        bAddPerk = true;

        List<Perk> perks = PerkDB.Load();
        _PerkType pt=_PerkType.LightDamage;
        switch(Type)
        {
            case _CardType.Tower:
                switch(damageType)
                {
                    case 0:
                        pt = _PerkType.LightDamage;
                        break;
                    case 1:
                        pt = _PerkType.HeavyDamage;
                        break;
                    case 2:
                        pt = _PerkType.MagicalDamage;
                        break;
                    case 3:
                        pt = _PerkType.EnergyDamage;
                        break;
                }
                break;
            case _CardType.Ability:
                pt = _PerkType.Ability;
                break;
            case _CardType.Hero:
                pt = _PerkType.Support;
                break;
        }

        for (int group = 0; group < Perk.MaxGroup; group++)
        {
            Perk[] active = perks.Where(i => i.type == pt && i.Group==group && PlayerManager._instance.isUpgradeBought(i.ID)).OrderBy(i=>i.Group).ToArray();

            //Properties.Add(new CardProperties(active.PropertiesID, active.getPropertSetting, new RequirementsProperties[0]));

            //MyLog.Log("AddPerk active count:" + active.Length);
            if (active.Length > 0)
            {
                Perk p = active[active.Length-1];
                if(p.PropertiesID>0 && p.PropertiesSetting.Length>0)
                    Properties.Add(new CardProperties(p.PropertiesID, p.getPropertSetting, CraftLevelData.GetMyLevelGrade(this, p.PropertiesID), new RequirementsProperties[0]));
            }
        }
    }

    private void InitNextLevel(TowersData towerData)
    {
        Card selectCard = this;
        if (towerData.settings.Length > 0)
        {
            SetDataLevel(selectCard, towerData.settings[0]);
            /*if (selectCard.Type == _CardType.Ability)
                Debug.Log("set next " + unitName + ":" + towerData.settings.Length);*/
            for (int i = 1; i < towerData.settings.Length; i++)
            {
                /*if(selectCard.Type== _CardType.Ability)
                    Debug.Log("set next "+i+":" + unitName);*/
                selectCard.unitName = towerData.name;
                CardGenerator.NextLevel(selectCard);
                selectCard = selectCard.NextLevelTower[0];
                SetDataLevel(selectCard, towerData.settings[i]);
                selectCard.LevelCard = this.LevelCard;
            }
        }
    }

    private void SetDataLevel(Card c,TowersData.SettingsData data)
    {
        if (c.stats[0].cost.Count == 0)
            c.stats[0].cost.Add(data.price);
        else
            c.stats[0].cost[0] = data.price;

        if (c.stats[0].cost.Count < 2)
            c.stats[0].cost.Add(data.mana);
        else
            c.stats[0].cost[1] = data.mana;

        switch(c.Type)
        {
            case _CardType.Ability:
                c.stats[0].cooldown = data.cooldown;
                //Debug.Log("SetAb Cool " + unitName + ":" + data.cooldown);
                if (data.slow > 0 && data.duration > 0)//замедление
                {
                    Properties.Add(new CardProperties(2, new object[] { data.slow * 100f, data.duration }, 0, new RequirementsProperties[1]));
                    Properties[Properties.Count - 1].Requirements[0] = new RequirementsProperties(2, new object[] { (float)c.LevelTower });
                }
                else if (IDTowerData == 204)//яд
                {
                    Properties.Add(new CardProperties(8, new object[] { 100f, 15f }, 0, new RequirementsProperties[1]));
                    Properties[Properties.Count - 1].Requirements[0] = new RequirementsProperties(2, new object[] { (float)c.LevelTower });
                }
                break;
            case _CardType.Tower:
                if (data.count > 0 && data.speed > 0 && c.IDTowerData == 12)
                    RegisterRegenTower(c, data);
                else
                    c.stats[0].cooldown = 1f / data.speed;
                break;
            case _CardType.Hero:
                c.stats[0].maxHP = data.hp;
                c.stats[0].moveSpeed = data.speedmove;
                //c.stats[0].xpNextLevel = data.xpnextlevel;

                c.stats[0].cooldown = 1f / data.speed;
                break;
        }


        if (data.dmg != null)
        {
            c.stats[0].damageMin = data.dmg[0];
            c.stats[0].damageMax = data.dmg[1];
        }
        else
        {
            c.stats[0].damageMin = c.stats[0].damageMax = 0;
        }
        c.stats[0].range = data.range;
        c.stats[0].hit = 1;
    }

    public void SetHeroObj(GameObject hero)
    {
        heroObj = hero;
        heroUnit = hero.GetComponent<UnitHero>(); 
        iconSprite = heroUnit.iconSprite;
        damageType = heroUnit.damageType;
        targetMode = heroUnit.targetMode;
    }

    public void SetAbilityObj(Ability ability,bool clone=true)
    {
        abilityObj = clone ? ability.Clone() : ability;
        iconSprite = ability.icon;
        damageType = ability.damageType;
    }

    public void SetTowerObj(GameObject tower)
    {
        towerObj = tower;
        UnitTower t = tower.GetComponent<UnitTower>();
        iconSprite = t.iconSprite;
        damageType = t.damageType;
        targetMode = t.targetMode;

        //towerObj.WriteValueCardInTower(this, false);
    }

    public List<float> GetCost(int ID = 0)
    {
        /*UnitTower tower = towerObj.GetComponent<UnitTower>();
        return tower.GetCost();*/
        int index = Properties.FindIndex(i => i.ID == 16);

        List<float> cost = new List<float>();
        for (int i = 0; i < stats[ID].cost.Count; i++)
        {
            if (index >= 0)
                cost.Add(stats[ID].cost[i] - stats[ID].cost[i]*(float)Properties[index].SetParam[0]/100f);
            else
                cost.Add(stats[ID].cost[i]);
        }

        return cost;
    }


    public Card Clone(Card main = null)
    {
        Card clone = new Card();
        stats.Add(new UnitStat());

        clone.ID = ID;
        clone.unitName = unitName;
        clone.prefabID = prefabID;

        clone.count = count;
        clone.next_damage = next_damage;
        clone.next_level_count = next_level_count;
        clone.price_sell_one = price_sell_one;
        clone.price_upgrade = price_upgrade;

        clone.LevelCard = LevelCard;
        clone.LevelTower = LevelTower;
        clone.rareness = rareness;
        clone.TowerType = TowerType;
        clone.IDTowerData = IDTowerData;

        clone.CheckOldBuild = CheckOldBuild;
        clone.disableInBuildManager = disableInBuildManager;

        clone.Type = Type;
        clone.heroUnit = heroUnit;
        clone.heroObj = heroObj;
        clone.towerObj = towerObj;
        clone.abilityObj = abilityObj;

        clone.towerObj = towerObj;
        clone.iconSprite = iconSprite;
        clone.damageType = damageType;
        clone.targetMode = targetMode;

        if(main == null)
        {
            List<CardProperties> cp = new List<CardProperties>();
            for (int i = 0; i < Properties.Count; i++)
            {
                cp.Add(Properties[i].Clone());
            }
            clone.Properties = cp;
            main = clone;
        }
        else
        {
            clone.Properties = main.Properties;
        }

       
        clone.stats = stats;

        foreach (Card cnext in NextLevelTower)
        {
            clone.NextLevelTower.Add(cnext.Clone(main));
        }

        return clone;
    }


    private void RegisterRegenTower(Card c, TowersData.SettingsData data)
    {
        if (Properties.Find(i => i.ID == 15) == null)
        {
            Properties.Add(new CardProperties(15, new object[] { 1f }, CraftLevelData.GetMyLevelGrade(this, -1), new RequirementsProperties[1]));
            Properties[Properties.Count - 1].Requirements[0] = new RequirementsProperties(1, new object[] { 1f });
        }
        Properties.Add(new CardProperties(14, new object[] { data.count, data.speed }, CraftLevelData.GetMyLevelGrade(this, -2), new RequirementsProperties[1]));
        Properties[Properties.Count - 1].Requirements[0] = new RequirementsProperties(2, new object[] { (float)c.LevelTower });
    }
}
