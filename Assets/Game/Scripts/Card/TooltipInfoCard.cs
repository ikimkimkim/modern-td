﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TDTK;

public class TooltipInfoCard : MonoBehaviour {
    
    public Text DPS, TypeDamage, Cost;
    public Vector3 Shift;
    private RectTransform Target;
    private GameObject thisObj;
    private RectTransform _transform;
    private float max, min;
    void Awake()
    {
        thisObj = gameObject;
        _transform = GetComponent<RectTransform>();
        _transform.position = new Vector3(100000, 100000, 0);
    }

    public void SetInfo(Card card, RectTransform target)
    {
        if (card == null)
            return;
        Target = target;
        min = card.stats[0].damageMin;
        max = card.stats[0].damageMax;
        min = min * 1f / card.stats[0].cooldown;
        max = max * 1f / card.stats[0].cooldown;
        DPS.text = Mathf.Round(min + (max - min) / 2f).ToString();
        TypeDamage.text =  DamageTable.GetAllDamageType()[card.damageType].name;
        Cost.text = (System.Math.Ceiling(card.GetCost()[0] * 10) / 10f).ToString();
        _transform.position = Target.position + Shift;
        thisObj.SetActive(true);
    }
    
    void Update()
    {
        if(Target!=null)
        _transform.position = Target.position + Shift;
    }

	public void Hide()
    {
        if (thisObj == null)
            thisObj = gameObject;
        thisObj.SetActive(false);
        Target = null;
    }
}
