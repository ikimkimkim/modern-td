﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class CardUpgradeManager : MonoBehaviour {

    public delegate void ShowPanel();
    public static event ShowPanel OnShowPanel;

    public delegate void HidePanel();
    public static event HidePanel OnHidePanel;

    public delegate void ChangeSelectCard();
    public static event ChangeSelectCard OnChangeSelectCard;

    public delegate void ChangeMode();
    public static event ChangeMode OnChangeMode;

    public delegate void UpdateCard();
    public static event UpdateCard OnUpdateCard;

    public GameObject Panel;

    public GameObject prefabCardInfo;

    public CardInfoUI CardSelect;

    public RectTransform PanelAllCards;
    public static CardUpgradeManager instance { get; private set; }

    public List<GameObject> AllCards;
    public List<GameObject> DeletCards;
    public  Action<List<GameObject>> AddCardResurse;

    public int CountSelect { get; private set; }


    public Text TextNameCard;
    public Text TextCountCards;

    public bool isShow { get { return Panel.activeSelf; } }
    //-----
    public Image iconImageGold;

    public Button UpgradeButton,SaleButton;
    public Text SaleButtonText;
    public Button ComplitUpgradeButton,CancelUpgradeButton;

    public Text TextComplitUpgradeButton;

    public bool UpgradeMode=false;

    public CardLevelPanel cardLevelPanel;
    public CardCalcMergeCost cardCalcMergeCost;
    public CardSelectInfoPanel cardSelectInfoPanel;
    public TooltipInfoCard tooltipInfoCard;


    private CardInfoUI selectCard;
    private string LastIDSelectCard;

    [Header("Tutorial")]
    public GameObject TutorialUpgradeCard;

    private bool bLockHide;

    public void SelectCardAndLoad(string id)
    {
        LastIDSelectCard = id;
        Load();
    }

    public void SelectCard(CardInfoUI cardUI)
    {
        if (UpgradeMode == false)
        {
            if (selectCard != null)
                selectCard.OnSelect(false);
            cardUI.OnSelect(true);
            selectCard = cardUI;
            UpdateSelectCard();
            LastIDSelectCard = selectCard.linkCard.ID;
            if (OnChangeSelectCard!=null)
                OnChangeSelectCard();
        }
        else
        {
            if (selectCard != cardUI)
            {
                if (cardUI.OnSelectUpdate())
                {
                    DeletCards.Add(cardUI.gameObject);
                }
                else
                {
                    DeletCards.Remove(cardUI.gameObject);
                }
                if (AddCardResurse != null)
                    AddCardResurse(DeletCards);
            }
        }
    }


    public void UpdateSelectCard()
    {
        /*CardSelect.SetInfoTower(selectCard.linkCard);
        TextNameCard.text = selectCard.linkCard.unitName;
        SaleButtonText.text = "Продать за error";// +selectCard.linkCard.Exp;
        UpgradeButton.gameObject.SetActive(true);
        ComplitUpgradeButton.gameObject.SetActive(false);
        cardLevelPanel.SetInfo(selectCard.linkCard);
        cardCalcMergeCost.SetInfo(selectCard.linkCard);
        cardSelectInfoPanel.SetInfo(selectCard.linkCard);*/
    }

    public void UpdateModeChange()
    {
        UpgradeMode = !UpgradeMode;

        UpgradeButton.gameObject.SetActive(!UpgradeMode);
        
        SaleButton.gameObject.SetActive(!UpgradeMode);
        ComplitUpgradeButton.gameObject.SetActive(UpgradeMode);
        CancelUpgradeButton.gameObject.SetActive(UpgradeMode);
        CardInfoUI c;
        for (int i = 0; i < AllCards.Count; i++)
        {
            c = AllCards[i].GetComponent<CardInfoUI>();
            if(selectCard.linkCard.ID!=c.linkCard.ID)
                c.CheckBoxSetActive(UpgradeMode);
        }

        if(UpgradeMode==false)
        {
            foreach (GameObject go in DeletCards)
            {
                go.GetComponent<CardInfoUI>().OnSelectUpdate();
            }
            DeletCards.Clear();
            if (AddCardResurse != null)
                AddCardResurse(DeletCards);

        }

        if (OnChangeMode != null)
            OnChangeMode();
    }


    void Awake()
    {
        instance = this;
        LastIDSelectCard = "";
    }

    // Use this for initialization
    void Start()
    {
        AllCards = new List<GameObject>();
        DeletCards = new List<GameObject>();
    }

    public void Show(string idCard = "")
    {
        Panel.SetActive(true);

        LastIDSelectCard = idCard;
        

        Load();
        if(OnShowPanel!=null)
            OnShowPanel();
    }

    public void Hide()
    {
        if (bLockHide)
            return;
        if (UpgradeMode)
            UpdateModeChange();
        Panel.SetActive(false);

        if (OnHidePanel != null)
            OnHidePanel();
    }

    public void Load(bool selectCard = true)
    {
        GameObject go;

        foreach (GameObject g in AllCards)
        {
            Destroy(g);
        }

        AllCards.Clear();

        int i = 0;
        foreach (Card t in PlayerManager.CardsList)
        {
            go = Instantiate(prefabCardInfo);
            CardInfoUI c = go.GetComponent<CardInfoUI>();
            c.PanelUpgrade = true;
            c.SetInfoTower(t, true);
            c.CheckBoxSetActive(false);
            go.name = "card_" + t.unitName;
            go.transform.SetParent(PanelAllCards);
            AllCards.Add(go);
            if (selectCard)
            {
                if (i == 0 && LastIDSelectCard == "")
                {
                    SelectCard(c);
                }
                else if (t.ID == LastIDSelectCard)
                {
                    SelectCard(c);
                }
            }

            i++;
        }
        CountSelect = i;
        PanelAllCards.sizeDelta = new Vector2(0, 120 * (float) Math.Ceiling(CountSelect/6f) + 25);
        TextCountCards.text = CountSelect + "/"+ PlayerManager.CardsList.Count;
    }


    public void UpgradeCard()
    {
       /* List<string> delete = new List<string>();
        foreach(GameObject go in DeletCards)
        {
            delete.Add(go.GetComponent<CardInfoUI>().linkCard.ID);
        }*/


    }

    public void resultUpgradeCard(ResponseData rp)
    {
        if (rp.ok == 0)
        {
            Debug.LogError("UpgradeTower Error: " + rp.error.code + " -> " + rp.error.text);
            if(rp.error.code == "no_gold")
            {
                StartCoroutine(ChangeColorGold());
            }
            else
            {
                TooltipMessageServer.Show(rp.error.text);
            }
        } 

        PlayerManager.SetPlayerData(rp.user);

        if (rp.ok == 1)
        {
            foreach (GameObject go in DeletCards)
                GameObject.Destroy(go);
            DeletCards.Clear();
            foreach (GameObject go in AllCards)
                GameObject.Destroy(go);
            AllCards.Clear();

            if (OnUpdateCard != null)
                OnUpdateCard();

            UpdateModeChange();
            Load(false);
        }
    }

    IEnumerator ChangeColorGold()
    {
        for (int i = 0; i < 4; i++)
        {
            TextComplitUpgradeButton.color = iconImageGold.color = new Color(1,0.7f,0.7f,1f);
            yield return new WaitForSeconds(0.1f);
            TextComplitUpgradeButton.color = iconImageGold.color = Color.white;
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void SaleCard()
    {
        CardSalePanel.instance.Show(selectCard.linkCard);
        CardSalePanel.instance.HidePanel += CBSaleCard;
        //PlayerManager.DM.SellTower(selectCard.linkCard.ID, CBSellCard);
    }

    public void CBSaleCard(bool result)
    {
        if (result)
        {
            foreach (GameObject go in DeletCards)
                GameObject.Destroy(go);
            DeletCards.Clear();
            foreach (GameObject go in AllCards)
                GameObject.Destroy(go);
            LastIDSelectCard = "";
            AllCards.Clear();
            Load();
        }
        CardSalePanel.instance.HidePanel -= CBSaleCard;
    }

    void Update()
    {
        if (CardUIAllInfo.isShow == false && Panel.activeSelf == true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Hide();
            }
        }
    }

    public static void TutorialSelectCardUpgrade() { instance._TutorialSelectCardUpgrade(); }
    public void _TutorialSelectCardUpgrade()
    {
        for (int i = 0; i < PlayerManager.CardsList.Count; i++)
        {
            if(PlayerManager.CardsList[i].count >= PlayerManager.CardsList[i].next_level_count)
            {
                if(TutorialUpgradeCard.activeSelf==false)
                    TutorialUpgradeCard.SetActive(true);
                TutorialUpgradeCard.GetComponent<StayInSameWorldPos>().Target = AllCards[i];
                bLockHide = true;
                return;
            }
        }
    }

    public static void TutorialHide()
    {
        if (instance.TutorialUpgradeCard.activeSelf==true)
            instance.TutorialUpgradeCard.SetActive(false);
        instance.bLockHide = false;
    }
}
