﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TDTK;
using System;
using UnityEngine.EventSystems;

public class CardInfoUI : MonoBehaviour, IPointerEnterHandler,IPointerExitHandler {

    
    public Image Icon;
    public Animator AnimatorCard;
    public Text level, textAddCount;
    public RectTransform LevelShining;

    public Image FonCards;
    public Sprite NotCard;
    public Sprite BlockCard;

    public TooltipInfoCard tooltipInfoCard;


    public string[] UrlCards;

    public GameObject buttonAllInfo;

    public Image SelectImage, SelectImage2, CheckBoxImage, CheckImage;

    public UnitTower Tower;
    [SerializeField]
    private bool OffProgressBar = false;
    public ProgressBarUI ProgressBar;
    public GameObject ButtonInfo, ButtonUpgrade;


    public delegate void ClickCardDelegate(CardInfoUI cardE);
    private event ClickCardDelegate ClickCardE;

    public Card linkCard { get; private set; }

    public bool PanelUpgrade = false;

    public bool SelectUpdate { get; private set; }
    public bool CanChecked=true;
    public bool CanUpdatePlayerData = true;

    public GameObject ArrowGreen, ArrowBlue;

    private RectTransform thisTransform;

    float min, max;

    private bool OnHover;

    private Coroutine HideButtonCor;

    void Awake()
    {
        /*if(linkCard==null)
            DeletCardInfo();*/
        thisTransform = GetComponent<RectTransform>();
    }

    void Start()
    {
        if(CanUpdatePlayerData)
        PlayerManager.InitializationСompleted += UpdatePlayerData;
    }

    void OnDestroy()
    {
        PlayerManager.InitializationСompleted -= UpdatePlayerData;
        ClickCardE = null;
    }

    private void UpdatePlayerData()
    {
        if (linkCard != null && linkCard.ID != "")
        {
            Card newLink = PlayerManager.CardsList.Find(i => i.ID == linkCard.ID);

            if(newLink==null)
            {                
                return;
            }

            if(newLink.Type == _CardType.Upgrade)
            {
                SetUpgradeCard(newLink);
                return;
            }

            if (ProgressBar != null)
            {
                linkCard = newLink;

                level.text = linkCard.LevelCard.ToString();

                if (ProgressBar.gameObject.activeSelf == false && !OffProgressBar)
                    ProgressBar.gameObject.SetActive(true);

                float p = (float)linkCard.count / (float)linkCard.next_level_count;
                string text = linkCard.count + "/" + linkCard.next_level_count;
                if (linkCard.next_level_count == 0)
                {
                    text = linkCard.count.ToString();
                    p = 0;
                }
                if (p < 1)
                {
                    ProgressBar.SetProgress(linkCard.count, linkCard.next_level_count, text, 0);

                    if (ArrowBlue != null) ArrowBlue.SetActive(true);
                    if (ArrowGreen != null) ArrowGreen.SetActive(false);
                }
                else
                {
                    ProgressBar.SetProgress(linkCard.count, linkCard.next_level_count, text, 1);


                    if (ArrowBlue != null) ArrowBlue.SetActive(false);
                    if (ArrowGreen != null) ArrowGreen.SetActive(true);
                }

            }
        }
    }

    private void SetUpgradeCard(Card card)
    {
        if (card != null && ProgressBar != null)
        {
            linkCard = card;

            level.text = "";

            if (ProgressBar.gameObject.activeSelf == false && !OffProgressBar)
                ProgressBar.gameObject.SetActive(true);

            ProgressBar.SetProgress(1,1, linkCard.count.ToString(), 1);
            if (ArrowBlue != null) ArrowBlue.SetActive(false);
            if (ArrowGreen != null) ArrowGreen.SetActive(false);            

        }
    }

    public void DeletCardInfo()
    {
        level.text = "";
        Icon.gameObject.SetActive(false);
        buttonAllInfo.SetActive(false);
        FonCards.sprite = NotCard;
        if (tooltipInfoCard != null)
            tooltipInfoCard.Hide();
        linkCard = null;
        if(ProgressBar!=null)
            ProgressBar.gameObject.SetActive(false);

    }

    public void Block()
    {
        DeletCardInfo();
        FonCards.sprite = BlockCard;
    }

    public void AddOpenChest()
    {
        ChestOpenPanel.instance.AddCard(this);
    }

    public void SetInfoTower(Card card, ClickCardDelegate ClickCardEvent)
    {
        ClickCardE = ClickCardEvent;
        SetInfoTower(card);
    }

    public void SetInfoTower(Card card, bool RefreshSelect = false, /*bool buttonCliked=true,*/TooltipInfoCard tooltipInfo=null, bool AnimProgressBar=false)
    {
        if(card == null)
        {
            DeletCardInfo();
            return;
        }

        if(tooltipInfoCard==null || tooltipInfo != null)
            tooltipInfoCard = tooltipInfo;
        SelectUpdate = false;
        linkCard = card;
        Icon.sprite = card.iconSprite;
        Icon.gameObject.SetActive(true);

        SelectUpdate = card.disableInBuildManager == false;

        if (RefreshSelect)
        {
            SelectUpdate = false;
        }

        if (CanChecked)
        {
            CheckImage.gameObject.SetActive(SelectUpdate);
        }

        if(CheckBoxImage!=null)
            CheckBoxImage.gameObject.SetActive(CanChecked);

        StartCoroutine(TextureDB.instance.load(UrlCards[(int)card.rareness], setSpriteFonCard));
        level.text = card.LevelCard.ToString();


        RectTransform rectTransform = FonCards.GetComponent<RectTransform>();
        if ((int)card.rareness > 2)
        {
            rectTransform.offsetMax = new Vector2(4, 4);
            rectTransform.offsetMin = new Vector2(-4, -2);
            level.GetComponent<RectTransform>().anchoredPosition = new Vector2(4, -1);
            if (LevelShining != null)
                LevelShining.anchoredPosition = new Vector2(14f, -10);
            //buttonAllInfo.GetComponent<RectTransform>().anchoredPosition = new Vector2(-1, 5.5f); 
        }
        else
        {
            rectTransform.offsetMin = new Vector2(0, 0);
            rectTransform.offsetMax = new Vector2(0, 0);
            level.GetComponent<RectTransform>().anchoredPosition = new Vector2(4, -4);
            if(LevelShining!=null)
                LevelShining.anchoredPosition = new Vector2(14f, -13);
            //buttonAllInfo.GetComponent<RectTransform>().anchoredPosition = new Vector2(-3.5f, 6f);
        }
        //buttonAllInfo.GetComponent<Button>().enabled = buttonCliked;
        //buttonAllInfo.SetActive(true);

        if (OnHover)
            OnPointerEnter();
        if (textAddCount != null)
        {
            if (card.AddCount > 0)
                textAddCount.text = "+" + card.AddCount.ToString();
            else
                textAddCount.text = "";
        }

        if (ProgressBar != null)
        {
            if(ProgressBar.gameObject.activeSelf== false && !OffProgressBar)
                ProgressBar.gameObject.SetActive(true);

            float p = (float)card.count / (float)card.next_level_count;
            string text = card.count + "/" + card.next_level_count;
            if (card.next_level_count==0)
            {
                text = card.count.ToString();
                p = 0;
            }
            if (p<1)
            {
                if (AnimProgressBar == false)
                    ProgressBar.SetProgress(card.count, card.next_level_count, text, 0);
                else
                {
                    StartCoroutine(ProgressBar.SetProgressAmin(card.count, card.next_level_count, text, 0));
                    StartCoroutine(ColorManager.ChangeColor(level, Color.yellow, Color.white));
                }
                if (ArrowBlue != null) ArrowBlue.SetActive(true);
                if (ArrowGreen != null) ArrowGreen.SetActive(false);
            }
            else
            {
                if (AnimProgressBar == false)
                    ProgressBar.SetProgress(card.count, card.next_level_count, text, 1);
                else
                {
                    StartCoroutine(ProgressBar.SetProgressAmin(card.count, card.next_level_count, text, 1));
                    StartCoroutine(ColorManager.ChangeColor(level, Color.yellow, Color.white));
                }
                if (ArrowBlue != null) ArrowBlue.SetActive(false);
                if (ArrowGreen != null) ArrowGreen.SetActive(true);
            }
        }
           
        if(card.Type == _CardType.Upgrade)
        {
            SetUpgradeCard(card);
        }


        if (ProgressBar != null && OffProgressBar)
            ProgressBar.gameObject.SetActive(false);
    }

    public void CheckBoxSetActive(bool active)
    {
        CheckBoxImage.gameObject.SetActive(active);
    }

    public void setSpriteFonCard(Sprite sprite)
    {
        if(FonCards!=null)
            FonCards.sprite = sprite;
    }

    public void showLvlTooltip()
    {
        StayInSameWorldPos.setPos(level.transform.position, "Уровень карты");
    }

    public void showInfoTooltip()
    {
        StayInSameWorldPos.setPos(buttonAllInfo.transform.position + new Vector3(-8, 28, 0), "Подробнее");
    }

    public void hideTooltip()
    {
        StayInSameWorldPos.Hide();
    }

    public void SetAddCount()
    {
        for (int i = 0; i < PlayerManager.CardsList.Count; i++)
        {
            if (PlayerManager.CardsList[i].IDTowerData == linkCard.IDTowerData && PlayerManager.CardsList[i].rareness == linkCard.rareness)
            {
                PlayerManager.CardsList[i].count += PlayerManager.CardsList[i].AddCount;
                PlayerManager.CardsList[i].AddCount = 0;
                SetInfoTower(PlayerManager.CardsList[i], false,/* true, */tooltipInfoCard);
                StartCoroutine(ColorManager.ChangeColor(ProgressBar.BarText, Color.yellow, Color.white));
                break;
            }
        }
    }

    public void OnChecked()
    {
        if (ClickCardE != null)
            ClickCardE(this);

        if (CanChecked == false)
            return;

        if (PanelUpgrade)
            CardUpgradeManager.instance.SelectCard(this);
        else
        {
            if ((CardAndLevelSelectPanel.instance.getCountSelectCard(linkCard.Type)< 6 && CardAndLevelSelectPanel.instance.getCountOstSelect>0) || linkCard.disableInBuildManager==false)
                linkCard.disableInBuildManager =!OnSelectUpdate();

            CardAndLevelSelectPanel.instance.listSelectCardToggle[(int)linkCard.Type].isOn = true;
            CardAndLevelSelectPanel.instance.CardChecked(true);

        }
            
    }

   /* private int CountSelectCards()
    {
        int i = 0;
        foreach (Card c in PlayerManager.CardsList)
        {
            if(c.disableInBuildManager==false)
            {
                i++;
            }
        }
        return i;
    }*/

    public void ShowAllInfo()
    {
        if (CardUIAllInfo.instance != null)
            CardUIAllInfo.instance.Show(linkCard,CardUIAllInfo.Mode.info);
    }

    public void OnSelect(bool select)
    {
        if (select)
        {
            if((int)linkCard.rareness>2)
                SelectImage2.color = new Color(1f, 1f, 1f, 1f);
            else
                SelectImage.color = new Color(1f, 1f, 1f, 1f);
            //ShowButton();
        }
        else
        {
            if ((int)linkCard.rareness > 2)
                SelectImage2.color = new Color(1f, 1f, 1f, 0f);
            else
                SelectImage.color = new Color(1f, 1f, 1f, 0f);
            //HideButton();
        }
    }

    public void ShowButton()
    {
        if (linkCard == null)
            return;

        if (ButtonInfo == null || ButtonUpgrade == null)
            return;

        if (HideButtonCor != null)
            StopCoroutine(HideButtonCor);

        if (linkCard.Type==_CardType.Upgrade || linkCard.count < linkCard.next_level_count || linkCard.next_level_count==0)
            ButtonInfo.SetActive(true);
        else
            ButtonUpgrade.SetActive(true);
    }


    public void HideButton()
    {
        if (ButtonUpgrade != null)
            ButtonUpgrade.SetActive(false);
        if (ButtonInfo != null)
            ButtonInfo.SetActive(false);
    }

    public void CorHideButton()
    {
        HideButtonCor = StartCoroutine(_HideButton());
    }

    private IEnumerator _HideButton()
    {
        yield return new WaitForSecondsRealtime(0.1f);// StartCoroutine(TimeScaleManager.WaitForRealSeconds(0.1f));
        if (ButtonUpgrade != null)
            ButtonUpgrade.SetActive(false);
        if (ButtonInfo != null)
            ButtonInfo.SetActive(false);
        yield break;
    }

    public bool OnSelectUpdate()
    {
        SelectUpdate = !SelectUpdate;
        CheckImage.gameObject.SetActive(SelectUpdate);
        return SelectUpdate;
    }


    public void OnPointerEnter()
    {
        if(tooltipInfoCard!=null)
            tooltipInfoCard.SetInfo(linkCard, thisTransform);
        OnHover = true;
    }

    public void OnPointerExit()
    {
        if (tooltipInfoCard != null)
            tooltipInfoCard.Hide();
        OnHover = false;
    }

    void Update()
    {
        if(isEnterPoint && linkCard != null)
        {
            if (Input.GetMouseButtonUp(1))
                UIMiniMenu.Show(linkCard);
        }
    }

    private bool isEnterPoint = false;
    public void OnPointerExit(PointerEventData eventData)
    {
        isEnterPoint = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isEnterPoint = true;
    }
}
