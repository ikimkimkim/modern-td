﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public struct CardSaleConfig
{
    public _CardRareness rareness;
    public float[] resourseMin, resourseMax;
}


public class CardSalePanel : MonoBehaviour
{
    [Serializable]
    public struct ResourcesPanel
    {
        public GameObject obj;
        public Text text;
    }

    public GameObject Panel;
    public CardInfoUI cardInfo;
    public Text textNameCard,textInfoCountCard;
    public ResourcesPanel[] resourcesPrice;
    public GameObject panelCountSaleCard;
    public Slider slider;
    [SerializeField] private Button buttonSale, buttonCancel;

    public static CardSalePanel instance { get; private set; }
    private List<CardSaleConfig> saleConfig;
    public Action<bool> HidePanel;
    private Card _cardSale;

    [Header("SalePopap")]
    public GameObject PanelSale;
    public ResourcesPanel[] resourcesResult;
    [SerializeField]
    private Text textBonusRes;
    public AudioSource audioSource;


    [Header("Tutorial")]
    [SerializeField] private GameObject ArrowCounter;
    [SerializeField] private GameObject ArrowSale;

    public void Show(Card cardSale)
    {
        Panel.SetActive(true);
        _cardSale = cardSale;
        cardInfo.SetInfoTower(_cardSale);
        textNameCard.text = _cardSale.unitName;

        saleConfig = PlayerManager._instance.CardPriceSellConfigArray;

        slider.maxValue = cardSale.count;
        slider.value = cardSale.count;
        panelCountSaleCard.SetActive(cardSale.count > 1);

        ChangeValueSlider();
        _AddStatusTutorial(StatusTutorial.waitOpen);
    }

    public void ChangeValueSlider()
    {
        int count = (int)slider.value;
        textInfoCountCard.text = count.ToString();
        var conf = saleConfig.Find(i => i.rareness == _cardSale.rareness);
        for (int i = 0; i < resourcesPrice.Length; i++)
        {
            if (conf.resourseMax[i] <= 0)
                resourcesPrice[i].obj.SetActive(false);
            else
            {
                resourcesPrice[i].obj.SetActive(true);
                resourcesPrice[i].text.text = string.Format("{0}-{1}",
                    conf.resourseMin[i] * count, conf.resourseMax[i] * count);
            }
        }
        _AddStatusTutorial( StatusTutorial.selectedCount);
    }

    public void Hide(bool Result)
    {
        Panel.SetActive(false);

        if (CardAndLevelSelectPanel.instance != null && CardAndLevelSelectPanel.instance.isShow)
        {
            CardAndLevelSelectPanel.instance.Load();
            CardAndLevelSelectPanel.instance.CardChecked();
        }

        if (CardUpgradeManager.instance != null && CardUpgradeManager.instance.isShow)
            CardUpgradeManager.instance.SelectCardAndLoad("");

        if (isTutorial && Result==false)
            _BreakTutorial();

        if (HidePanel!=null)
            HidePanel(Result);
    }

    public void SaleCard()
    {
        PlayerManager.DM.SellTower((int)_cardSale.IDTowerData+1,(int)_cardSale.rareness+1, Mathf.RoundToInt(slider.value), CBSaleCard);
    }

    public void CBSaleCard(ResponseData rp)
    {
        if (rp.ok == 0)
        {
            Debug.LogError("Sale Error: " + rp.error.code + " -> " + rp.error.text);
            TooltipMessageServer.Show(rp.error.text);
        }
        else
        {
            _AddStatusTutorial( StatusTutorial.sale);
            ShowResult(rp.prizes.craft);
        }
        PlayerManager.SetPlayerData(rp.user);
        Hide(rp.ok == 1);
    }


    public void ShowResult(Dictionary<string, int> res)
    {
        PanelSale.SetActive(true);
        for (int i = 0; i < resourcesResult.Length; i++)
        {
            resourcesResult[i].obj.SetActive(false);
        }
        int index;
        foreach (var r in res.Keys)
        {
            index = int.Parse(r) - 1;
            resourcesResult[index].obj.SetActive(true);
            resourcesResult[index].text.text = UpResourceCraftStarPerk.GetTextCountRes(res[r]);
        }

        textBonusRes.text = string.Format("(бонус за технологии: {0}%)", UpResourceCraftStarPerk.RealProcent);

        if (PlayerManager.isSoundOn())
            audioSource.Play();
    }

    public void HideResult()
    {
        PanelSale.SetActive(false);
        _AddStatusTutorial( StatusTutorial.giveRes);
    }

    void Start ()
    {
        instance = this;
        Hide(false);
	}

    void Update()
    {
        if (Panel.activeSelf == true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Hide(false);
            }
        }
    }

    private enum StatusTutorial { waitOpen, selectedCount, sale, giveRes, end}
    [SerializeField] private StatusTutorial _tutorialStatus;
    private Action _endTutorialEvent;
    private bool isTutorial = false;
    public static bool isTutorialBreak { get; private set; }


    public static void StartTutorial( Action endTutorial)
    {
        instance._StartTutorial(endTutorial);
    }

    private void _StartTutorial(Action endTutorial)
    {
        isTutorial = true;
        isTutorialBreak = false;
        _endTutorialEvent = endTutorial;
        _tutorialStatus = StatusTutorial.waitOpen;
        buttonCancel.interactable = true;
        buttonSale.interactable = false;
    }

    private void _AddStatusTutorial(StatusTutorial status)
    {
        if (isTutorial == false)
            return;
        if (_tutorialStatus != status)
            return;
        Debug.Log("_AddStatusTutorial");
        _tutorialStatus++;
        _UpdateStatus();
    }

    private void _UpdateStatus()
    {
        switch(_tutorialStatus)
        {
            case StatusTutorial.selectedCount:
                if (_cardSale.count <= 1)
                    _AddStatusTutorial(_tutorialStatus);
                else
                    ArrowCounter.SetActive(true);
                break;
            case StatusTutorial.sale:
                ArrowCounter.SetActive(false);
                ArrowSale.SetActive(true);
                buttonSale.interactable = true;
                break;
            case StatusTutorial.giveRes:
                ArrowSale.SetActive(false);
                break;

            case StatusTutorial.end:
                _EndTutorial();
                break;
        }
    }

    private void _BreakTutorial()
    {
        isTutorialBreak = true;
        _EndTutorial();
    }

    private void _EndTutorial()
    {
        if (isTutorial == false)
            return;
        buttonCancel.interactable = true;
        isTutorial = false;
        ArrowCounter.SetActive(false);
        ArrowSale.SetActive(false);
        _endTutorialEvent();
    }


}
