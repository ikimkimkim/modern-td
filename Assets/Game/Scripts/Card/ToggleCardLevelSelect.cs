﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ToggleCardLevelSelect : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public Sprite spriteCountActive, spriteCountDeactive;
    public Color textColorActive, textColorDeactive;


    public Toggle thisToggle;
    public Text thisText;
    public GameObject countPanelObj;
    public Text countText;
    public Image countImage;

    public bool isOn {
        get { return thisToggle.isOn; }
        set { thisToggle.isOn = value; }
    }

    public bool thisEnable
    {
        get { return thisToggle.interactable; }
        set {
            thisToggle.interactable = value;
            thisText.color = value ? textColorActive : textColorDeactive;
            if (value == false) HideCountPanel();
        }
    }

    public void SetCountCard(int countNow,int countMax)
    {
        countPanelObj.SetActive(true);
        countText.text = countNow + "/" + countMax;
        countImage.sprite = countNow < countMax ? spriteCountActive : spriteCountDeactive;
    }

    public void HideCountPanel()
    {
        if(countPanelObj!=null)
            countPanelObj.SetActive(false);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        StayInSameWorldPos.Hide();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(thisEnable==false)
            StayInSameWorldPos.setPos(transform.position + transform.up * 10f, "Пока недоступно");
    }
}
