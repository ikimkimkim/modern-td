﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TDTK;

public class CardManager : MonoBehaviour {

    public GameObject prefabCardInfo;

    public Transform PanelAllCards;
    public Transform PanelSelectCards;

    public static CardManager instance { get; private set; }

    public List<GameObject> gos;


    public GameObject UpCard;

    public int CountSelect { get; private set; }

    void Awake()
    {
        instance = this;
    }

	// Use this for initialization
	void Start ()
    {
        gos = new List<GameObject>();
        Load();
    }

    public void Load()
    {

        GameObject go;

        foreach (GameObject g in gos)
        {
            Destroy(g);
        }

        gos.Clear();

        int i = 0;
        int j = 0;
        foreach (Card t in PlayerManager.CardsList)
        {
            go = Instantiate(prefabCardInfo);
            go.GetComponent<CardInfoUI>().SetInfoTower(t);
            if (t.disableInBuildManager == false)
            {
                go.transform.SetParent(PanelSelectCards);
                go.GetComponent<RectTransform>().anchoredPosition = new Vector2(50 + i * 100, PanelSelectCards.GetComponent<RectTransform>().sizeDelta.y/2f);
                //go.GetComponent<CardInfoUI>().Tower.prefabID = i;
                i++;
            }
            else
            {
                go.transform.SetParent(PanelAllCards);
                go.GetComponent<RectTransform>().anchoredPosition = new Vector2(50 + j * 100, PanelAllCards.GetComponent<RectTransform>().sizeDelta.y / 2f);
                j++;
            }
            gos.Add(go);
        }
        CountSelect = i + 1;

    }

    public void Save()
    {
        /*List<Card> cards = new List<Card>();
        foreach (GameObject go in gos)
        {
            cards.Add(go.GetComponent<CardInfoUI>().linkCard);
        }

        PlayerManager._instance.SetCards(cards);*/
    }

    public void OnChecked(CardInfoUI card)
    {
        if (card.linkCard.disableInBuildManager == true)
        {
            if (CardManager.instance.CountSelect < 7)
            {
                card.linkCard.disableInBuildManager = !card.linkCard.disableInBuildManager;
                CardManager.instance.Load();
            }
        }
        else
        {
            if (CardManager.instance.CountSelect > 2)
            {
                card.linkCard.disableInBuildManager = !card.linkCard.disableInBuildManager;
                CardManager.instance.Load();
            }
        }
    }



    // Update is called once per frame
    void Update () {
	
	}
}
