﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using TDTK;

/// <summary>
/// Перка таувера в scenes/Upgrades
/// </summary>
public class TowerUpgradeItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public int Index { get { return _perk.ID; } }
    public Sprite IconPurchased;
    UpgradeManager _upgradeManager;
    public Perk _perk;
    public TowerUpgradeItem NextPerk = null;
    public GameObject GoldLine;
    Perk _previousPerk = null;
    Image _icon;
    Sprite _iconUnPurchased;
    Image _image;
    Button _button;

    ColorBlock _activeColors;
    ColorBlock _passiveColors;
    void Awake()
    {
        _button = GetComponent<Button>();
    }
    public void Initialize(Perk perk, TowerUpgradeItem previousPerk)
    {
        _perk = perk;

        if (previousPerk != null)
        {
            _previousPerk = previousPerk._perk;
            previousPerk.NextPerk = this;
        }
        _icon = GetComponentsInChildren<Image>()[1];
        _icon.sprite = perk.icon;
        _upgradeManager = GameObject.FindGameObjectWithTag("UpgradeManager").GetComponent<UpgradeManager>();
        GetComponent<Button>().onClick.AddListener(() => { _upgradeManager.OnUpgradeClick(this); });
        _image = GetComponent<Image>();
        _iconUnPurchased = _image.sprite;
        _activeColors = _button.colors;
        _passiveColors = new ColorBlock();
        _passiveColors.normalColor = Color.white;
        _passiveColors.pressedColor = Color.white;
        _passiveColors.highlightedColor = Color.white;
        _passiveColors.disabledColor = Color.white;
        if (!perk.purchased)
        {
            UnPurchase();
        }
        else
        {
            Purchase();
        }
    }
    public bool PreviousPerkPurchased()
    {
        if (_previousPerk == null)
        {
            return true;
        }
        if (_previousPerk.purchased)
        {
            return true;
        }
        return false;
    }
    //public bool isPrereqPerkPurchased()
    //{
    //    return _upgradeManager.PrereqPerksPurchaised(_perk);
    //}
    public bool isPurchased()
    {
        return _perk.purchased;
    }
    public void Purchase()
    {
        GoldLine.SetActive(true);
        _image.sprite = IconPurchased;
        _image.SetNativeSize();
        var color = _icon.color;
        _perk.purchased = true;
        _icon.color = new Color(color.r, color.g, color.b, 1f);
        _button.colors = _passiveColors;
        //_button.transition = Selectable.Transition.None;
        if (NextPerk != null)
        {
            NextPerk.PerkAvailable();
        }
    }
    public void PerkAvailable()
    {
        var color = _icon.color;
        _icon.color = new Color(color.r, color.g, color.b, 1f);
        _button.colors = _activeColors;
        //  _button.transition = Selectable.Transition.ColorTint;
    }
    void PerkUnavailable()
    {
        var color = _icon.color;
        _icon.color = new Color(color.r, color.g, color.b, 1 / 2f);
        _button.colors = _passiveColors;
        //  _button.transition = Selectable.Transition.None;
    }

    public void HideIcon()
    {
        _icon.enabled = false;
    }
    public void ShowIcon()
    {
        _icon.enabled = true;
    }
    public void UnPurchase()
    {
        _perk.purchased = false;
        GoldLine.SetActive(false);
        _image.sprite = _iconUnPurchased;
        _image.SetNativeSize();

        if (PreviousPerkPurchased())
        {
            PerkAvailable();
        }
        else
        {
            PerkUnavailable();
        }
        if (NextPerk != null)
        {
            NextPerk.UnPurchase();
        }

    }
    void OnEnable()
    {
        PlayerManager.OnCrystalsChangedE += OnCrystalsChanged;
    }
    void OnDisable()
    {
        PlayerManager.OnCrystalsChangedE -= OnCrystalsChanged;
    }
    //хак используется чтобы при сбрасывании перков
    public void OnCrystalsChanged(int currentCrystals)
    {
        if (!_perk.purchased)
        {
            UnPurchase();
        }
        else
        {
            Purchase();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
        _upgradeManager.Tooltip.Show(_upgradeManager.CanAfford(this), this);
#endif
    }

    public void OnPointerExit(PointerEventData eventData)
    {
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
        _upgradeManager.Tooltip.Hide();
#endif
    }
}
