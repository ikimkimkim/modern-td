﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TDTK;
using UnityEngine.SceneManagement;
/// <summary>
/// Главный класс upgrade сцены инициализирует все всетки перков и управляет покупкой продажей
/// </summary>
public class UpgradeManager : MonoBehaviour
{
    public Text NotSpentStars;
    public GameObject TowerUpgrades;
    public GameObject TowerUpgradesAbilitys;
    public RectTransform UpgradesPanel;
    public Vector3 StartPosition;
    public int OffsetX;
    public int OffsetY;
    public int UpgradeTreesPerLine;
    public UpgradesTooltip Tooltip;
    public UpgradeItemSelect ItemSelect;

    public GameObject Tooltip_Recommended;

    PlayerManager _playerInfo;

    TowerUpgradesInfo[] _towerUpgradeInfos;
    TowerUpgradeItem _selectedUpgrade;
    List<Perk> _perks;



    void Start()
    {
        Debug.LogError("Loading UpgradeManager!");
        _perks = PerkDB.LoadClone();
        ResetPerksID();//Сбрасываем ID перков, чтобы можно было связать перки с конкретными башнями, потому что в TDTK они выдаются автоматически 
        _playerInfo = GameObject.FindGameObjectWithTag("PlayerInfo").GetComponent<PlayerManager>();
        _towerUpgradeInfos = GetComponents<TowerUpgradesInfo>();
        for (int i = 0; i < _perks.Count; i++)
        {
            if (_playerInfo.isUpgradeBought(i + 1))
            {
                GetPerkByID(i + 1).purchased = true;
            }
        }

        NotSpentStars.text = _playerInfo.GetNotSpendStars(GetSpendStars()).ToString();
        //Раскидываем перки по тауверам, в соответствии с требуемыми ID 
        for (int i = 0; i < _towerUpgradeInfos.Length; i++)
        {
            for (int q = 0; q < _perks.Count; q++)
            {
                if (_towerUpgradeInfos[i].IDs1.Contains(_perks[q].ID))
                {
                    _towerUpgradeInfos[i].Perks1.Add(_perks[q]);
                }
                else if (_towerUpgradeInfos[i].IDs2.Contains(_perks[q].ID))
                {
                    _towerUpgradeInfos[i].Perks2.Add(_perks[q]);
                }
                else if (_towerUpgradeInfos[i].IDs3.Contains(_perks[q].ID))
                {
                    _towerUpgradeInfos[i].Perks3.Add(_perks[q]);
                }
            }
        }
        

        //Инициализируем ветки прокачки каждого таувера
        float x = 0;
        float y = 0;
        for (int i = 0; i < _towerUpgradeInfos.Length - 1; i++)
        {
            InitializeUpgradeTree(TowerUpgrades, ref x, ref y, i);
        }
        //Для способностей
        x = 0f;
        y = 2f;
        InitializeUpgradeTree(TowerUpgradesAbilitys, ref x, ref y, 4);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(1);
            //Application.LoadLevel(1);
        }

        Tooltip_Recommended.SetActive(GetSpendStars() == 0 && _playerInfo.GetNotSpendStars(GetSpendStars()) > 0);
    }
    private void InitializeUpgradeTree(GameObject towerUpg, ref float x, ref float y, int i)
    {
        var towerUpgrades = (Instantiate(towerUpg) as GameObject).GetComponent<TowerUpgrades>();
        towerUpgrades.Initialize(_towerUpgradeInfos[i]);
        towerUpgrades.transform.SetParent(UpgradesPanel.transform, false);
        RectTransform rect = towerUpgrades.GetComponent<RectTransform>();
        rect.localPosition = StartPosition;
        rect.localPosition += new Vector3(x * OffsetX, y * OffsetY, 0);
        x++;
        if (x == UpgradeTreesPerLine)
        {
            x = 0;
            y++;
        }
    }
    public bool CanAfford(TowerUpgradeItem item)
    {
        return item._perk.cost[0] <= _playerInfo.GetNotSpendStars(GetSpendStars()) || item._perk.purchased;
    }
    public void OnselectedUpgradeClick()
    {
        if (_selectedUpgrade != null)
        {
           // OnUpgradeClick(_selectedUpgrade);
            //Покупаем/продаем при повторном клике
            if (ItemSelect.gameObject.activeInHierarchy && CanAfford(_selectedUpgrade) && _selectedUpgrade.PreviousPerkPurchased())
            {
                OnBuyClick();
                //Обновим тултип чтобы на нем появилась надпись купленно/цена 
                Tooltip.Show(CanAfford(_selectedUpgrade), _selectedUpgrade);
                _selectedUpgrade.ShowIcon();
                return;
            }
        }
    }
    public void OnUpgradeClick(TowerUpgradeItem item)
    {
        //Закрываем тултип если кликнули на недоступный апгрейд
        if (!item.PreviousPerkPurchased() || !item.isPurchased() && !CanAfford(item))// ||  item.isPurchased() && item.NextPerk != null && item.NextPerk.isPurchased()) //|| item._perk.purchased)
        {
            ItemSelect.Hide();

#if UNITY_ANDROID || UNITY_IOS
            // На мобилках показываем, но покупать не даем.
            Tooltip.Show(false, item);
            ItemSelect.Show(false, item);
            _selectedUpgrade = item;
#endif
          //  _selectedUpgrade = null;
            return;
        }

        //Покупаем/продаем при повторном клике
        if (_selectedUpgrade == item && ItemSelect.gameObject.activeInHierarchy && CanAfford(_selectedUpgrade) && _selectedUpgrade.PreviousPerkPurchased())
        {
            OnBuyClick();
            //Обновим тултип чтобы на нем появилась надпись купленно/цена 
            Tooltip.Show(CanAfford(_selectedUpgrade), _selectedUpgrade);
            _selectedUpgrade.ShowIcon();
            return;
        }


        _selectedUpgrade = item;
        _selectedUpgrade.HideIcon();
#if UNITY_ANDROID || UNITY_IOS
        Tooltip.Show(CanAfford(item), item);
#endif
        Tooltip.SetSpecText();
        GetComponent<SoundFXHelper>().Play();
        Perk selectedPerk = GetPerkByID(item.Index);
        if (_selectedUpgrade.PreviousPerkPurchased() && !_selectedUpgrade.isPurchased() && selectedPerk.cost[0] <= _playerInfo.GetNotSpendStars(GetSpendStars()))
        {
            ItemSelect.Show(true, item);
        }
        else
        {
            ItemSelect.Show(false, item);
        }

    }
    public void OnBuyClick()
    {
        
        if (_selectedUpgrade._perk.purchased)
        {
            SellUpgrade();
        }
        else
        {
            _playerInfo.BuyUpgrade(_selectedUpgrade.Index);
            _selectedUpgrade.Purchase();
        }
   //     _selectedUpgrade = null;
        ItemSelect.Hide();
#if UNITY_ANDROID || UNITY_IOS
        Tooltip.Hide();
#endif
        NotSpentStars.text = _playerInfo.GetNotSpendStars(GetSpendStars()).ToString();

    }

    private void SellUpgrade()
    {
        List<int> selledPerks = new List<int>();

        if (_selectedUpgrade.NextPerk != null && _selectedUpgrade.NextPerk.isPurchased())
        {
            selledPerks.Add(_selectedUpgrade.NextPerk._perk.ID);
            if (_selectedUpgrade.NextPerk.NextPerk != null && _selectedUpgrade.NextPerk.NextPerk.isPurchased())
            {
                selledPerks.Add(_selectedUpgrade.NextPerk.NextPerk._perk.ID);
            }
        }
        _selectedUpgrade.UnPurchase();
        selledPerks.Add(_selectedUpgrade.Index);
        _playerInfo.SellUpgrades(selledPerks);
    }
    public void OnResetClick()
    {
#if UNITY_ANDROID || UNITY_IOS
        Tooltip.Hide();
        ItemSelect.Hide();
#endif
        _playerInfo.ResetUpgrades();
    }
    void OnEnable()
    {
        PlayerManager.OnCrystalsChangedE += OnCrystalsChanged;
    }
    void OnDisable()
    {
        PlayerManager.OnCrystalsChangedE -= OnCrystalsChanged;
    }
    //хак используется чтобы при сбрасывании перков
    public void OnCrystalsChanged(int currentCrystals)
    {
        for (int i = 0; i < _perks.Count; i++)
        {
            if (_playerInfo.isUpgradeBought(i + 1))
            {
                GetPerkByID(i + 1).purchased = true;
            }
            else
            {
                GetPerkByID(i + 1).purchased = false;
            }
        }
        NotSpentStars.text = _playerInfo.GetNotSpendStars(GetSpendStars()).ToString();
        
    }
    float GetSpendStars()
    {
        float spendStars = 0;
        for (int i = 0; i < _perks.Count; i++)
        {
            if (_perks[i].purchased)
            {
                spendStars += _perks[i].cost[0];
            }
        }
        return spendStars;
    }
    Perk GetPerkByID(int id)
    {
        for (int i = 0; i < _perks.Count; i++)
        {
            if (_perks[i].ID == id)
            {
                return _perks[i];
            }
        }
        return null;
    }
    void ResetPerksID()
    {
        for (int i = 0; i < _perks.Count; i++)
        {
            _perks[i].ID = i + 1;
        }
    }
}

