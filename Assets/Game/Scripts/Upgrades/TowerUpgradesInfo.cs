﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TDTK;
/// <summary>
/// Используется UpgradeManager'ом для понятия какому тауверу соотвецтвут какие id 
/// </summary>
public class TowerUpgradesInfo : MonoBehaviour
{
    /// <summary>
    /// Главная иконка таувера 
    /// </summary>
    public Sprite TowerIcon;
    public string Caption;
    public List<int> IDs1 = new List<int>();
    public List<int> IDs2 = new List<int>();
    public List<int> IDs3 = new List<int>();
    [HideInInspector]
    public List<Perk> Perks1 = new List<Perk>();
    [HideInInspector]
    public List<Perk> Perks2 = new List<Perk>();
    [HideInInspector]
    public List<Perk> Perks3 = new List<Perk>();

}
