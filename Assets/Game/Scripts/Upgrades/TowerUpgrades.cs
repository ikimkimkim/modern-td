﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TDTK;
using System.Collections.Generic;
using Translation;
/// <summary>
/// Ветка прокачики таувера, генерирует TowerUpgradeItem и передает в них информацию.
/// </summary>
public class TowerUpgrades : MonoBehaviour
{
    public Image Icon;
    public Text Caption;
    public List<TowerUpgradeItem> Line1;
    public List<TowerUpgradeItem> Line2;
    public List<TowerUpgradeItem> Line3;


    public void Initialize(TowerUpgradesInfo info)
    {
        Icon.sprite = info.TowerIcon;
        Caption.text = TranslationEngine.Instance.Trans[info.Caption]; 
        InitalizeUpgradeLine(info.Perks1, Line1);
        InitalizeUpgradeLine(info.Perks2, Line2);
        InitalizeUpgradeLine(info.Perks3, Line3);
    }
    void InitalizeUpgradeLine(List<Perk> perks, List<TowerUpgradeItem> items)
    {
        for (int i = 0; i < perks.Count; i++)
        {
            if (i == 0)
            {
                items[i].Initialize(perks[i], null);
            }
            else
            {
                items[i].Initialize(perks[i], items[i - 1]);
            }
        }
    }
}
