﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Translation;

public class UpgradesTooltip : MonoBehaviour
{
    public Text Caption;
    public Text Desc;
    public Text Cost;
    public Image Star;
    public Text CostSpecText; //Используется для недоступно и куплено

    public Image ArrowDown;
    public Image ArrowUp;
    public Image ArrowLeftRight;


    GradientText _gradient;
    //Outline _outline;
    TowerUpgradeItem _item;
    public UpgradeItemSelect ItemSelect;

    public Vector3 CostSpecTextBuyPosition;
    public Vector3 StarBuyPosition;
    public Vector3 CostBuyPosition;

    Vector3 CostSpecTextInitPosition;
    Vector3 StarInitPosition;
    Vector3 CostInitPosition;
    //bool _mobileScaleAlreadySet = false;

    void Awake()
    {
        _gradient = Cost.GetComponent<GradientText>();
        //_outline = Cost.GetComponent<Outline>();

        InitPositions();
        Hide();
    }

    //1/3 по высоте нужно сделать 
    void SetMobileScale()
    {
#if UNITY_ANDROID || UNITY_IOS
        if (_mobileScaleAlreadySet)
        {
            return;
        }
        else
        {
            _mobileScaleAlreadySet = true;
        }
        float globalScale = GameObject.FindObjectOfType<Canvas>().scaleFactor;
        // Debug.Log(globalScale);
        float addScale = (Screen.height / 2.2f) / (160f * globalScale);
        transform.localScale *= addScale;
#endif
    }
    void InitPositions()
    {
        CostSpecTextInitPosition = CostSpecText.rectTransform.localPosition;
        StarInitPosition = Star.rectTransform.localPosition;
        CostInitPosition = Cost.rectTransform.localPosition;
    }
    void ResetPositions()
    {
        CostSpecText.rectTransform.localPosition = CostSpecTextInitPosition;
        Star.rectTransform.localPosition = StarInitPosition;
        Cost.rectTransform.localPosition = CostInitPosition;
    }

    void SetBuyPositions()
    {
        CostSpecText.rectTransform.localPosition = CostSpecTextBuyPosition;
        Star.rectTransform.localPosition = StarBuyPosition;
        Cost.rectTransform.localPosition = CostBuyPosition;
    }


    /// <summary>
    /// Если апгрейд выбран и он не куплен показываем "купить за 3*", если куплен показываем "продать"
    /// </summary>
    public void SetSpecText()
    {
        Translator trans = TranslationEngine.Instance.Trans;
        ResetPositions();
        if (_item.isPurchased())
        {
            Star.enabled = false;
            Cost.enabled = false;
            CostSpecText.enabled = true;
            CostSpecText.color = Color.red;
            CostSpecText.text = trans["UpgradesTooltip.Sell"];
        }
        else
        {
            SetBuyPositions();
            Star.enabled = true;
            Cost.enabled = true;
            CostSpecText.enabled = true;
            CostSpecText.color = Color.green;
            CostSpecText.text = trans["UpgradesTooltip.Buy"] + " ";
        }
    }


    public void Show(bool affordAble, TowerUpgradeItem item)
    {
        Translator trans = TranslationEngine.Instance.Trans;
        ResetPositions();
        _item = item;
        gameObject.SetActive(true);
        var name = item._perk.name.Split(';');

        Caption.text = trans[name[0]] + " " + name[1];
        var desc = item._perk.desp.Split(';');
        var descValues = new string[desc.Length - 1];
        for (int i = 0; i < descValues.Length; i++)
        {
            descValues[i] = desc[i + 1];
        }
        Desc.text = string.Format(trans[desc[0]], descValues);

        Cost.text = item._perk.cost[0].ToString();
        CostSpecText.enabled = false;
        Star.enabled = true;
        Cost.enabled = true;
        _gradient.enabled = true;
        if (!affordAble)
        {
            //Star.enabled = false;
            //Cost.enabled = false;
            //CostSpecText.enabled = true;
            //CostSpecText.color = Color.red;
            //CostSpecText.text = "Недоступно";
            _gradient.enabled = false;
            Cost.color = Color.red;
        }
        else
            Cost.color = new Color(255, 195, 0, 255);
        if (item._perk.purchased)
        {
            Star.enabled = false;
            Cost.enabled = false;
            CostSpecText.enabled = true;
            CostSpecText.color = Color.green;
            CostSpecText.text = trans["UpgradesTooltip.Bought"];
        }
        //Показываем спец текст в тултипе над выбранным апгрейдом
        if (ItemSelect.gameObject.activeInHierarchy && ItemSelect._item != null)
        {
            if (ItemSelect._item == _item)
            {
                SetSpecText();
            }
        }

        transform.position = item.transform.position;
        ArrowDown.gameObject.SetActive(false);
        ArrowUp.gameObject.SetActive(false);
        ArrowLeftRight.gameObject.SetActive(false);

        SetMobileScale();
        float scale = GameObject.FindObjectOfType<Canvas>().scaleFactor * transform.localScale.x;

        if (transform.position.x > Screen.width - 100f * scale)//700f)
        {
            ArrowLeftRight.gameObject.SetActive(true);
            transform.position = item.transform.position + new Vector3(-118 * scale, 0, 0);
        }
        else
        {
            if (transform.position.y > Screen.height - 160f * scale) //450f)
            {
                ArrowUp.gameObject.SetActive(true);
                transform.position = item.transform.position + new Vector3(0, -95 * scale, 0);
            }
            else
            {
                ArrowDown.gameObject.SetActive(true);
                transform.position = item.transform.position + new Vector3(0, 88 * scale, 0);
            }
        }
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
