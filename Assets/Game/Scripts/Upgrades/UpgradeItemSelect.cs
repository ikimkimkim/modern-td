﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using TDTK;
public class UpgradeItemSelect : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Image Img;
    public Sprite Buy;
    public Sprite Sell;
    UpgradeManager _upgradeManager;
    public TowerUpgradeItem _item;
    void Awake()
    {
        _upgradeManager = GameObject.FindGameObjectWithTag("UpgradeManager").GetComponent<UpgradeManager>();
    }
    public void Show(bool hideBuy, TowerUpgradeItem item)
    {
        //При переключение с одного апгрейда на другой не забываем на старом вернуть иконку
        if (_item != null && _item != item)
        {
            _item.ShowIcon();
        }

        _item = item;
        transform.position = item.transform.position;
        gameObject.SetActive(true);


        if (item._perk.purchased)
        {
            Img.sprite = Sell;
            Img.SetNativeSize();
            Img.gameObject.SetActive(true);
        }
        else
        {
            Img.sprite = Buy;
            Img.SetNativeSize();
            Img.gameObject.SetActive(hideBuy);
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        if (_item != null)
            _item.ShowIcon();
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
        _upgradeManager.Tooltip.Show(_upgradeManager.CanAfford(_item), _item);
#endif
    }

    public void OnPointerExit(PointerEventData eventData)
    {
#if (UNITY_WEBGL || UNITY_WEBPLAYER)
        _upgradeManager.Tooltip.Hide();
#endif
    }
}
