﻿using UnityEngine;
using System.Collections;

public class SplashSpecialEffect : MonoBehaviour {

    public ParticleSystem Splash;
    [SerializeField]
    private float Range=1;
    

    public void Emit(float range)
    {
        Range = range;
        var main = Splash.main;
        main.startSizeMultiplier = Range;
        Splash.Emit(1);
    }

}
