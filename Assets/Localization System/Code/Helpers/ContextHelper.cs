using System;
#if UNITY_WINRT
using System.Globalization;
#else
using System.Threading;
#endif
using UnityEngine;

namespace Translation
{
#if UNITY_4
	[NotRenamed]
	[NotConverted]
#endif
	public class ContextHelper
	{
		public static string GetContextLanguage ()
		{
			#if UNITY_WINRT
			return CultureInfo.CurrentUICulture.Name;
			#else
			return Thread.CurrentThread.CurrentUICulture.Name;
			#endif
		}
	}
}

