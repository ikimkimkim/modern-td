using System;
using System.Xml;
using UnityEngine;

namespace Translation
{
	public static class CsvIO
	{		
		public static void DetectLanguages(Translator translator, string file, char delimiter)
		{
			try {
				var lines = FileHelper.ReadLines(file);

				if(lines.Length == 0) 
					throw new Exception("Empty lang file");

				var langsLine = lines[0].Substring(lines[0].IndexOf(delimiter) + 1);
				var langs = langsLine.Split(delimiter);

				foreach(var lang in langs) {
					translator.AddLanguageReference(lang, lang);
				}
			}
			catch(Exception ex) {
				Debug.Log ("Error loading the CSV file. Details:");
				Debug.LogError(ex);
				throw ex;
			}
		}
		
		/// <summary>
		/// Imports a dictionary from a CSV file.
		/// </summary>
		/// <param name="file">File path.</param>
		public static void ImportFromCSV(Translator translator, string file, char delimiter, string lang)
		{
			var lines = FileHelper.ReadLines(file);
			var langsLine = lines[0].Substring(lines[0].IndexOf(delimiter) + 1);
			var langs = langsLine.Split(new char[]{delimiter}, StringSplitOptions.RemoveEmptyEntries);

			int index = 0;

			for(int i = 0; i < langs.Length; i++) {
				if(langs[i].Equals(lang)) {
					index = i;
					break;
				}
			}

			if(index == langs.Length) {
				throw new Exception("Language not founded");
			}
			
			for (int i = 1; i < lines.Length; i++)
			{
				if (string.IsNullOrEmpty(lines[i]) || (lines[i].StartsWith("#") && lines[i].EndsWith(delimiter.ToString())))
					continue;
				
				var words = lines[i].Split(delimiter);
				var key = words[0];
               	translator.AddWord(key, words[index+1]);
			}
			
			translator.LangName = file;
			translator.LangFile = file;
			translator.LangCulture = lang;
			translator.LangTitle = lang;
		}
	}
}

