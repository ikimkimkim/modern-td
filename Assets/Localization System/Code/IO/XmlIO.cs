using System;
using System.Xml;
using UnityEngine;
#if UNITY_4
using UnityEngine.Flash;
#endif
#if UNITY_WINRT
using EX = Translation.WindowsSDK;
#else
using EX = System;
#endif
namespace Translation
{
#if UNITY_4
	[NotRenamed]
	[NotConverted]
#endif
	public static class XmlIO
	{
		public static object OpenXML(string filename)
		{
			XmlDocument xDoc = new XmlDocument();
            Exception error = null;
			try
            {
				xDoc.LoadXml(FileHelper.Read(filename));
            }
            catch(Exception ex)
            {
				//Debug.LogError("Error capturado, file: "+filename);
				//Debug.LogError (ex);
				error = ex;
            }

			if(error != null)
				throw error;
			return xDoc;
		}
		
		public static void DetectLanguages(Translator translator)
		{
			try
			{
				XmlDocument xDoc = (XmlDocument)XmlIO.OpenXML("translator.cfg");
	
	            var root = (XmlElement)xDoc.GetElementsByTagName("root")[0];
	            int version = int.Parse(root.GetElementsByTagName("spec-version")[0].InnerText);
	
	            switch (version)
	            {
	                case 1:
	                    {
	                        var languagesNode = (XmlElement)root.GetElementsByTagName("languages")[0];
	                        var languagesNodeList = languagesNode.GetElementsByTagName("language");
	                        foreach (XmlElement pNode in languagesNodeList)
	                        {
	                            string nodeFile = pNode.GetAttribute("file");
	                            string nodeCode =  pNode.GetAttribute("code");
								translator.AddLanguageReference(nodeCode, nodeFile);
								// Debug.Log ("Language detected: "+nodeCode);
	                        }
	                    }
	                    break;
	                default:
	                    throw new EX.InvalidProgramException("@str/ex.admin-invalid-version.msg");
	            }
				
				// Debug.Log ("Translator configured.");
			}
			catch(Exception ex){
				Debug.Log ("Translator NOT configured. Details:");
				Debug.LogError(ex);
				throw ex;
			}
		}
		
		/// <summary>
        /// Imports a dictionary from a XML file.
        /// </summary>
        /// <param name="file">File path.</param>
        public static void ImportFromXML(Translator translator, string file)
        {
			//Debug.Log("Loading "+file);
            XmlDocument xDoc = (XmlDocument)XmlIO.OpenXML(file);

            var root = (XmlElement)xDoc.GetElementsByTagName("root")[0];
            int version = int.Parse(root.GetElementsByTagName("spec-version")[0].InnerText);

            switch (version)
            {
                case 1:
                    {
                        var languageNode = (XmlElement)root.GetElementsByTagName("language")[0];
						translator.LangName = file;
						translator.LangFile = file;
                        translator.LangCulture = languageNode.GetAttribute("name");
                        translator.LangTitle = languageNode.GetAttribute("title");
                        var wordsNodeList = languageNode.GetElementsByTagName("word");
                        foreach (XmlElement pNode in wordsNodeList)
                        {
                            var wordName = pNode.GetAttribute("name");
                            var wordText = pNode.InnerText;
                            switch(pNode.GetAttribute("type").ToLower())
                            {
                                case "menu":
                                    wordText = wordText.Replace('_', '&');
                                    break;
                                default:
                                    break;
                            }

                            translator.Words.Set(wordName, wordText);
                        }
                    }
                    break;
                default:
					throw new EX.InvalidProgramException("@str/ex.admin-invalid-version.msg");
            }
        }
		
		public static void ExportToXML(Translator translator, string file)
		{
 			XmlDocument xml = new XmlDocument(); 
        	xml.CreateXmlDeclaration("1.0", "utf-8", null);
			
			XmlNode root = xml.CreateNode (XmlNodeType.Element, "root", null);
			xml.AppendChild(root);
			
			XmlNode child = xml.CreateNode(XmlNodeType.Element, "spec-version", null);
			child.InnerText = "1";
			root.AppendChild(child);
			
			var atName = xml.CreateAttribute ("name");
			var atTitle = xml.CreateAttribute("title");
			atName.Value = translator.LangCulture;
			atTitle.Value = translator.LangTitle;
			
			child = xml.CreateNode(XmlNodeType.Element, "language", null);
			child.Attributes.Append (atName);
			child.Attributes.Append (atTitle);
			root.AppendChild(child);
			
			XmlNode wordNode;
			foreach(string key in translator.Words.GetKeys ())
			{
				wordNode = xml.CreateNode (XmlNodeType.Element, "word", null);
				var wName = xml.CreateAttribute ("name");
				wName.Value = key;
				wordNode.Attributes.Append (wName);
				wordNode.InnerText = translator.Words.Get (key);
				child.AppendChild(wordNode);
			}
			
        	xml.Save(file);
		}
	}
}

