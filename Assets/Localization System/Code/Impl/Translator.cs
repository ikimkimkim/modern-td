using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;

namespace Translation
{
    public class Translator: ITranslator
    {
		#region Attributes
        string langTitle;
        /// <summary>
        /// Title from loaded language.
        /// </summary>
        public string LangTitle
        {
            get { return langTitle; }
            set { langTitle = value; }
        }

        string langCulture;
        /// <summary>
        /// Loaded language code.
        /// </summary>
        public string LangCulture
        {
            get { return langCulture; }
            set { langCulture = value; }
        }

        string langName;
        /// <summary>
        /// Language name.
        /// </summary>
        public string LangName
        {
            get { return langName; }
            set { langName = value; }
        }
		
		string langFile;
		public string LangFile {
			get {
				return this.langFile;
			}
			set {
				langFile = value;
			}
		}        
        Dict languages = new Dict();
        /// <summary>
        /// Available languages.
        /// </summary>
        public Dict Languages
        {
            get { return languages; }
            set { languages = value; }
        }
        
        Dict words = new Dict();
        /// <summary>
        /// Loaded dictionary content.
        /// </summary>
        public Dict Words
        {
            get { return words; }
            set { words = value; }
        }

		ELangFormat format;
		object[] formatArgs;

		List<FileReference> references = new List<FileReference>();
		
        /// <summary>
        /// Gets the selected language.
        /// </summary>
        public string SelectedLanguage
        {
            get { return TranslationEngine.Instance.Language; }
        }

		/// <summary>
		/// Gets the context's default language.
		/// </summary>
		/// <value>
		/// The context language.
		/// </value>
		public string ContextLanguage
		{
			get {return ContextHelper.GetContextLanguage();}	
		}

        /// <summary>
        /// Gets a translated string.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string this[string name]
        {
            get
            {
                string _return;
                if (!string.IsNullOrEmpty(name) && Words.ContainsKey(name))
                {
                	_return = Words.Get(name);
                }
                else
                {
                    _return = string.Empty;
                }

                return _return.Replace("{\\n}", "\n");
            }
        }
		
		#endregion
		
        /// <summary>
        /// Constructor.
        /// </summary>
        public Translator(bool init)
        {
			if(!init) return;
			Configure();
			LoadDictionary(ContextLanguage);
		}

		public Translator()
			: this(true)
		{
		}

		public Translator(bool init, ELangFormat format, params object[] args) {
			if(init)
				Init (format, args);
		}
		
		
		/// <summary>
		/// Constructor. 
		/// </summary>
		/// <param name='format'>File format.</param>
		/// <param name='args'>Extra arguments. For CSV: [file, delimiter].</param>
		public Translator(ELangFormat format, params object[] args) {
			Configure (format, args);
			LoadDictionary(ContextLanguage);
		}

		#region Protected methods

		protected void Configure() {
			Configure (ELangFormat.Xml, null);
		}

		/// <summary>
		/// Loads the configuration.
		/// </summary>
		/// <param name='config'>Config.</param>
		/// <param name='args'>Extra arguments. For CSV: [file, delimiter].</param>
		protected void Configure(ELangFormat format, params object[] args)
		{
			this.format = format;
			this.formatArgs = args;

			switch(format) {
			case ELangFormat.Xml: {

					if(references != null && references.Count>0) {
						Debug.LogWarning("You can use FileReference script only with CSV format. XML works only with local files.");
					}

					XmlIO.DetectLanguages (this);
				}	
				break;

			case ELangFormat.Csv:{
					if(args.Length < 2) {
						throw new Exception("You must specify: filename, delimiter");	
					}

					string file = FileHelper.GetFile(this.references, args[0].ToString());

					CsvIO.DetectLanguages (this, file, args[1].ToString()[0]);
				}
				break;

			}
		}

		protected void ImportDictionary(string code) {

			if(format == ELangFormat.Xml) {

				string reference = Languages.Get(code);
				ImportFromXML(reference);

			}
			else if(format == ELangFormat.Csv) {

				string path = FileHelper.GetFile(this.references, formatArgs[0].ToString());
				ImportFromCSV(path, formatArgs[1].ToString()[0], code);
			}

		}

        /// <summary>
        /// Imports a dictionary from a XML file.
        /// </summary>
        /// <param name="file">File path.</param>
        protected void ImportFromXML(string file)
        {
            XmlIO.ImportFromXML(this, file);
        }
		
		/// <summary>
		/// Imports a dictionary from a CSV file.
		/// </summary>
		/// <param name="file">File path.</param>
		/// <param name="delimiter">File delimiter. Default: ';'.</param>
		protected void ImportFromCSV(string file, char delimiter, string lang) {
			CsvIO.ImportFromCSV(this, file, delimiter, lang);
		}
		
		#endregion
		
		#region Public methods

		public void Init(ELangFormat format, params object[] args) {
			Configure (format, args);
			LoadDictionary(ContextLanguage);
		}

		public void AddFileReference(FileReference file) {
			references.Add(file);
		}

		public void ClearFileReferences() {
			references.Clear();
		}

		public void AddWord (string key, string val)
		{
			this.words.Set(key, val);
		}
		
		/// <summary>
		/// Adds a language reference.
		/// </summary>
		/// <param name='code'>
		/// Language code.
		/// </param>
		/// <param name='filename'>
		/// Filename.
		/// </param>
		public void AddLanguageReference(string code, string filename)
		{
			Languages.Set (code, filename);
		}
		
		/// <summary>
		/// Removes the language reference.
		/// </summary>
		/// <param name='code'>
		/// Code.
		/// </param>
		/// <exception cref='NotImplementedException'>
		/// Is thrown when the not implemented exception.
		/// </exception>
		public void RemoveLanguageReference (string code)
		{
			Languages.Remove(code);
		}
		
		/// <summary>
		/// Gets a pair language name/code, using an index.
		/// </summary>
		/// <returns>
		/// The language data from index.
		/// </returns>
		/// <param name='index'>
		/// Index.
		/// </param>
		public string GetLanguageDataFromIndex(int index)
		{
			
			foreach(string key in this.Languages.GetKeys())
			{
				if(index--==0)
				{
					string name = this.Languages.Get(key);
					return name+","+key;
				}
			}
			
			return string.Empty;
		}
		
		public string GetLanguageNameFromIndex(int index)
		{
			return this.Languages.Get (GetLanguageCodeFromIndex(index));
		}
		
		public string GetLanguageCodeFromIndex(int index)
		{
			var keys = this.Languages.GetKeys();
			if(index>keys.Length)
			{
				return string.Empty;	
			}
			return keys[index];
		}

        /// <summary>
        /// Loads a dicionary. If not exists, look for the dictionary country. If not exists (again), loads the first one.
        /// </summary>
        /// <param name="code">Language code. Example #1: en-US. Example #2: en.</param>
        public void LoadDictionary(string code)
        {
			var backup = words;
			try
			{
				words = new Dict();
                MyLog.Log("Translator:"+code, MyLog.Type.build);
                MyLog.Log(StringSerializationAPI.Serialize(this.GetAvailableLanguages().GetType(),this.GetAvailableLanguages()), MyLog.Type.build);

	            if (Languages.ContainsKey (code))
	            {
					ImportDictionary(code);
	            }
				else if(this.GetAvailableLanguages ().Length > 0) {
					if(!this.GetAvailableLanguages ()[0].Equals(code))
		            {
                        MyLog.LogError("Translation: the language "+code+" not exists. Loading another...", MyLog.Type.build);
						LoadDictionary(this.GetAvailableLanguages ()[0]);
		            }
					else {
                        MyLog.LogError("Translation: the language "+code+" not exists.", MyLog.Type.build);
					}
				}
			}
			catch(Exception ex)
			{
                MyLog.LogError("Error loading "+code, MyLog.Type.build);
				words = backup;
				throw ex;
			}
        }

        /// <summary>
        /// Loads a dicionary. If not exists, look for the dictionary country. If not exists (again), loads the first one.
        /// </summary>
        /// <param name="name">Language name.</param>
        public void LoadDictionaryFromValue(string name)
        {
			var backup = words;
			try
			{
				words = new Dict();
				ImportDictionary(name);
			}
			catch(Exception ex)
			{
                MyLog.LogError("Error loading "+name, MyLog.Type.build);
				words = backup;
				throw ex;
			}
        }

		public void SaveDictionary ()
		{
			if(string.IsNullOrEmpty(LangTitle))
			{
				LangTitle = LangCulture;
			}
			
			XmlIO.ExportToXML(this, LangFile);
		}
		
		/// <summary>
		/// Gets the available languages.
		/// </summary>
		/// <returns>
		/// The available languages, or null.
		/// </returns>
        public string[] GetAvailableLanguages()
        {
			if(Languages.GetCount() == 0)
				return null;
            var list = new string[Languages.GetCount()];
			int i = 0;
			foreach(string lang in Languages.GetValues())
			{
				list[i++] = lang;	
			}
			return list;
        }
		
		/// <summary>
		/// Gets the available languages.
		/// </summary>
		/// <returns>
		/// The available languages (code), or null.
		/// </returns>
        public string[] GetCodeAvailableLanguages()
        {
			if(Languages.GetCount() == 0)
				return null;
            var list = new string[Languages.GetCount()];
			int i = 0;
			foreach(string lang in Languages.GetKeys())
			{
				list[i++] = lang;	
			}
			return list;
        }
		
		#endregion
    }
}