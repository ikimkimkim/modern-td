using UnityEngine;
using System.Collections;
using System;
using Translation;

public class ObjectTranslation : MonoBehaviour, ITranslatable {
	
	public enum TranslationType
	{
		ObjectActivation
	}
		
	[Serializable]
	public class Translatable
	{
		public string language = "en-US";
		public GameObject translatable = null;
	}
	
	public TranslationType mode = TranslationType.ObjectActivation;
	public Translatable[] items;
	
	// Use this for initialization
	void Start () {
		Translate ();
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	public void Translate() {
		var language = TranslationEngine.Instance.Language;
		
		foreach(Translatable item in items)
		{
			if(mode == TranslationType.ObjectActivation)
			{
				item.translatable.SetActive(item.language.Equals (language));
				//Debug.Log (item.translatable.active);
			}
		}
	}
}
