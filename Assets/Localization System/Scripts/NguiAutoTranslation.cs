﻿using UnityEngine;
using System.Collections;

public class NguiAutoTranslation : MonoBehaviour {

	public bool recursive = true;
	public bool includeInactive = true;

	void Start () {
		TranslationEngine.Instance.AutoTranslateNgui(gameObject, recursive, includeInactive);
	}

	public void Reload() {
		TranslationEngine.Instance.AutoTranslateNgui(gameObject, recursive, includeInactive);
	}
}
