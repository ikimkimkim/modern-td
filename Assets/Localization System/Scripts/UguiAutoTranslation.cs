﻿using UnityEngine;
using System.Collections;

public class UguiAutoTranslation : MonoBehaviour {

	//public bool reload = false;
	public bool recursive = true;
	public bool includeInactive = true;

	void Start () {
		TranslationEngine.Instance.AutoTranslateUgui(gameObject, recursive, includeInactive);
	}

	public void Reload() {
		TranslationEngine.Instance.AutoTranslateUgui(gameObject, recursive, includeInactive);
	}
	
	/*void Update () {
		if(reload) {
			TranslationEngine.Instance.AutoTranslateUgui(gameObject, recursive, includeInactive);
			reload = false;
		}
	}*/
}
