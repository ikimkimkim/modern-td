﻿using UnityEngine;
using Translation;
using System.Collections;
using System;

/// <summary>
/// CSV Initializer.
/// </summary>
[Serializable]
public class FormatCsv : MonoBehaviour {

	public string delimiter = ";";
	public string filename = "translation.csv";

}
