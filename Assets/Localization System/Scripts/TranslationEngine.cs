using UnityEngine;
using System.Collections;
using Translation;
using System;
using System.Collections.Generic;
using System.Reflection;

public class TranslationEngine : MonoBehaviour
{

    private static TranslationEngine instance = null;
    public static TranslationEngine Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType(typeof(TranslationEngine)) as TranslationEngine;

                if (instance == null)
                {
                    Debug.LogError("TranslationEngine doesn't exists");
                }
            }
            return instance;
        }
    }

    private Translator trans;
    /// <summary>
    /// Translator instance.
    /// </summary>
    /// <value>
    /// The translation engine.
    /// </value>
    public Translator Trans
    {
        get
        {
            return this.trans;
        }
        set
        {
            trans = value;
        }
    }

    /// <summary>
    /// Selected language.
    /// </summary>
    public string Language = "en-US";

    public ELangFormat Format = ELangFormat.Xml;

    protected MonoBehaviour FormatComponent = null;

    public void SelectLanguage(string langCode)
    {
        Translator translator = TranslationEngine.Instance.Trans;

        if (translator == null)
        {
            Debug.LogWarning("Translator not found. Please, select a language after an awake method.");
            return;
        }

        translator.LoadDictionary(langCode);
        Language = langCode;

        Translate();
    }

    public void SelectNextLanguage()
    {

        Translator translator = TranslationEngine.Instance.Trans;
        string[] langs = translator.Languages.GetKeys();

        if (langs.Length == 0)
            return;

        int index = 0;
        while (index < langs.Length && !langs[index].Equals(translator.LangCulture))
        {
            index++;
        }
        index = (index + 1) % langs.Length;

        translator.LoadDictionary(langs[index]);
        Language = langs[index];

        Translate();
    }

    /// <summary>
    /// Translates all "ObjectTranslation" in the scene. This is needed 
    /// </summary>
    public void Translate()
    {
        var objects = GameObject.FindObjectsOfType(typeof(ObjectTranslation));

        foreach (var obj in objects)
        {
            var translatable = (ITranslatable)obj;
            translatable.Translate();
        }

        //	Reload UGUI
		UguiAutoTranslation ugui = GameObject.FindObjectOfType<UguiAutoTranslation>();
        if (ugui != null)
        {
			ugui.Reload();
        }
    }

    private List<Component> GetComponents(Type t, GameObject parent, bool recursive, bool includeInactive)
    {
        List<Component> components = new List<Component>();
        components.AddRange(parent.GetComponents(t));

        if (recursive)
        {
            components.AddRange(parent.GetComponentsInChildren(t, includeInactive));
        }

        return components;
    }

    public void AutoTranslateNgui(GameObject parent, bool recursive, bool includeInactive)
    {
#if UNITY_WSA && !UNITY_EDITOR

        Type t = Type.GetType("UILabel");
        System.Reflection.PropertyInfo p = t.GetTypeInfo().GetDeclaredProperty("text");

        foreach (Component comp in GetComponents(t, parent, recursive, includeInactive))
        {

            string text = (p.GetValue(comp, null) ?? "").ToString().Trim();
            string key = "";

            if (text.StartsWith("{") && text.EndsWith("}"))
            {
                key = text.Remove(text.LastIndexOf("}")).Substring(1);

                TranslationInfo info = comp.gameObject.AddComponent<TranslationInfo>();
                info.key = key;
            }
            else
            {
                TranslationInfo info = comp.gameObject.GetComponent<TranslationInfo>();
                if (info != null)
                {
                    key = info.key;
                }
            }

            if (!string.IsNullOrEmpty(key))
            {
                string newText = Trans[key];
                p.SetValue(comp, newText, null);
                if (string.IsNullOrEmpty(newText))
                {
                    Debug.LogWarning("NGUI Translation: Empty string from key: '" + key + "'");
                }
            }
        }

#else

        Type t = Type.GetType("UILabel");
        System.Reflection.PropertyInfo p = t.GetProperty("text");

        foreach (Component comp in GetComponents(t, parent, recursive, includeInactive))
        {

            string text = (p.GetValue(comp, null) ?? "").ToString().Trim();
            string key = "";

            if (text.StartsWith("{") && text.EndsWith("}"))
            {
                key = text.Remove(text.LastIndexOf("}")).Substring(1);

                TranslationInfo info = comp.gameObject.AddComponent<TranslationInfo>();
                info.key = key;
            }
            else
            {
                TranslationInfo info = comp.gameObject.GetComponent<TranslationInfo>();
                if (info != null)
                {
                    key = info.key;
                }
            }

            if (!string.IsNullOrEmpty(key))
            {
                string newText = Trans[key];
                p.SetValue(comp, newText, null);
                if (string.IsNullOrEmpty(newText))
                {
                    Debug.LogWarning("NGUI Translation: Empty string from key: '" + key + "'");
                }
            }
        }
#endif
    }

    public void AutoTranslateUgui(GameObject parent, bool recursive, bool includeInactive)
    {
        List<UnityEngine.UI.Text> components = new List<UnityEngine.UI.Text>();
        components.AddRange(parent.GetComponents<UnityEngine.UI.Text>());

        if (recursive)
        {
            components.AddRange(parent.GetComponentsInChildren<UnityEngine.UI.Text>(includeInactive));
        }

        foreach (UnityEngine.UI.Text uguiText in components)
        {
            string text = uguiText.text.Trim();
            string key = "";

            if (text.StartsWith("{") && text.EndsWith("}"))
            {
                key = text.Remove(text.LastIndexOf("}")).Substring(1);

                TranslationInfo info = uguiText.gameObject.AddComponent<TranslationInfo>();
                info.key = key;
            }
            else
            {

                TranslationInfo info = uguiText.gameObject.GetComponent<TranslationInfo>();
                if (info != null)
                {
                    key = info.key;
                }

            }

            if (!string.IsNullOrEmpty(key))
            {
                uguiText.text = Trans[key];
                TranslationInfo info = uguiText.gameObject.GetComponent<TranslationInfo>();
                if (info.MultiLine)
                {
                    uguiText.text = uguiText.text.Replace("\\n","\n");
                }
             
                if (string.IsNullOrEmpty(uguiText.text))
                {
                    Debug.LogWarning("UGUI Translation: Empty string from key: '" + text + "'");
                }
            }
        }
    }

    void Awake()
    {
        try
        {
            //	CSV Initialization
            if (Format == ELangFormat.Csv)
            {
                var component = this.GetComponent<FormatCsv>();
                this.FormatComponent = component;

                // Creates a new translator
                Trans = new Translator(false);

                //	Add special references
                foreach (var re in this.GetComponents<FileReference>())
                {
                    Trans.AddFileReference(re);
                }

                Trans.Init(Format, component.filename, component.delimiter);

            }
            else
            {	//	Initialization (XML)
                this.FormatComponent = null;
                // Creates a new translator
                Trans = new Translator(Format);
            }

            // Gets all available languages
            //selections = Trans.GetAvailableLanguages();
            // Selects the first one
            //selections[selection]
            Trans.LoadDictionary(Language);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
        }
    }
}