﻿using UnityEngine;
using System.Collections;
using System;
using Translation;

/// <summary>
/// Optional. Specifies a different URL for the language file.
/// </summary>
[Serializable]
public class FileReference : MonoBehaviour {

	public EPlatform[] platforms;
	public string pathOrUrl = "";

}
