﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Translation.Editor.Common
{
    public static class StringHelper
    {	
		public static int Count(string str, char find)
		{
			int count = 0;
			for(int i = 0; i < str.Length; i++)
			{
				if(str[i] == find)
					++count;
			}
			
			return count;
		}

        public static string[] SplitAndTrim(string str, char separator)
        {
			if(string.IsNullOrEmpty (str))
				return null;
			
            List<string> result = new List<string>();
            foreach (var s in str.Split(separator))
            {
                if(!string.IsNullOrEmpty(s))
                    result.Add(s.Trim());
            }
            return result.ToArray();
        }
    }
}
