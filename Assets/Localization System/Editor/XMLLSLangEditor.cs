#if UNITY_EDITOR

using System;
using System.IO;
using Translation;
using Translation.Editor;
using UnityEditor;
using UnityEngine;
using System.Text;
using System.Collections.Generic;

/// <summary>
/// XMLLSLangEditor. It's a easy language file editor.
/// </summary>
public class XMLLSLangEditor : EditorWindow 
{
	#region Static attributes
	static Translator translator = new Translator(true);
	static XMLLSLangEditor editor;
	static Rect window;
	#endregion
	
	#region Attributes
	Vector2 scrollPosition = Vector2.zero;	
	
	int menu = 0;
	
	int padding = 10;
	int buttonH = 24;
	int buttonW = 70;
	int itemH = 30;
	int imgButtonW = 24;
	int fieldWidth = 400;
	int toolbarLocation = 50;
	
	// First menu
	string selectedFile; // Only clicked
	string selectedCode; // Only clicked
	string textboxFile = translator.GetLanguageNameFromIndex(0);
	string textboxCulture = translator.GetLanguageCodeFromIndex(0);
	string filename;
	string code;
	string txtfilename;
	string txtcode;
	
	// Second menu
	string newTitle = "";
	string addKeyText = "";
	string addValText = "";
	bool saved = true;
	
	#endregion
	
	#region Constructor & generator
	[MenuItem ("Window/Localization Editor (XML)")]
	public static void GenerateMenu()
	{
		editor = (XMLLSLangEditor)EditorWindow.GetWindow (typeof (XMLLSLangEditor), true, "Localization System Editor");
		translator = new Translation.Translator(true);
		
		Install();
	}
	
	#endregion
	
	#region Unity Methods
	
	void OnGUI () 
	{
		bool exit = false;
		try{
			window = editor.position;
			
			switch(menu)
			{
			case 0:
				MenuAbm();
				break;
			case 1:
				MenuLanguage();
				break;
			default:
				exit = true;
				break;
			}
		}
		catch(Exception ex)
		{
			Debug.LogError(ex.ToString());	
			exit = true;
		}
		
		if(exit)
		{
			this.Close ();
		}
	}
	
	#endregion
	
	#region First Menu GUI Methods
	void MenuAbm()
	{
		SetTitle("General Configuration");
		Box();
		AbmToolbar();
		AbmContent();
		FeedbackButton();
		SyncButton();
		if(BottomButton("Close"))
			menu --;
	}
	
	void AbmContent()
	{
		int xLocation = padding*2;
		int yLocation = padding*6+buttonH;
		int bottom = buttonH+padding*3+yLocation;
		var languages =translator.GetAvailableLanguages ();
		
		GUI.Box(new Rect(xLocation, yLocation, window.width - xLocation*2, window.height-bottom), "");
		scrollPosition = GUI.BeginScrollView(
			new Rect(xLocation, yLocation, window.width - xLocation*2, window.height-bottom),
			scrollPosition,
			new Rect(0, 0, window.width - xLocation*2, padding*2+languages.Length*itemH));
		
		string text, code, name;
		for(int i = 0; i < languages.Length;i++)
		{
			text = translator.GetLanguageDataFromIndex(i);
			name = text.Split(',')[0];
			code = text.Split(',')[1];

			GUI.Label(
				new Rect(
					padding+padding/2, 
					padding + (i*itemH), 
					window.width - xLocation*2 - padding*2,
					padding*2), 
				text);
			
			// List buttons
			GUI.SetNextControlName("ButtonSelect");
			if(ImgButton (
				new Rect(window.width - xLocation*2 - padding*2 - buttonW*2, 
					padding + (i*itemH),buttonW , padding*2), 
				"select", "Select"))
			{
				GUI.FocusControl("ButtonSelect");
				selectedFile = name;
				textboxFile = name;
				selectedCode = code;
				textboxCulture = code;
			}
			
			GUI.SetNextControlName("ButtonOpen");
			if(ImgButton (
				new Rect(window.width - xLocation*2 - padding - buttonW, 
					padding + (i*itemH),buttonW , padding*2), 
				"open", "Open"))
			{
				GUI.FocusControl("ButtonOpen");
				selectedFile = name;
				textboxFile = name;
				selectedCode = code;
				textboxCulture = code;
				LoadLanguage(selectedCode);
				menu = 1;
			}
		}
		
		GUI.EndScrollView ();
	}
	
	void AbmToolbar()
	{
		int yLocation = toolbarLocation;
		
		textboxFile = EditorGUI.TextField(
			new Rect(padding*2, yLocation, buttonW*4, padding*2), 
			"Name", textboxFile);
		
		textboxCulture = EditorGUI.TextField(
			new Rect(padding*3+buttonW*4, yLocation, buttonW*3, padding*2), 
			"Culture", textboxCulture);
		
		if(!string.IsNullOrEmpty(textboxFile) && !string.IsNullOrEmpty(textboxCulture))
		{
			if(ImgButton(new Rect(window.width-padding*4-imgButtonW*3, yLocation, imgButtonW, buttonH), 
				"page_add", "Add"))
			{
				if(translator.Languages.ContainsKey(textboxCulture))
				{
					EditorUtility.DisplayDialog("Error", "The language '"+textboxCulture+"' already exists", "Ok");
				}
				else
				{
					string langFullFilename = GetLangFilename(textboxFile);
					translator.AddLanguageReference(textboxCulture, textboxFile);
					try
					{
						List<string> keys;
						try{
							keys = GetKeys(new Translation.Translator(true));
						}
						catch{
							keys = new List<string>();
						}
						using(StreamWriter sw = new StreamWriter(langFullFilename, false, Encoding.UTF8))
						{
							sw.WriteLine("<?xml version='1.0' encoding='utf-8' ?>");
							sw.WriteLine("<root>");
							sw.WriteLine("\t<spec-version>1</spec-version>");
							sw.WriteLine(string.Format("\t<language name='{0}' title='{0}'>", textboxCulture));
							if(keys.Count>0)
							{
								foreach(string key in keys)
								{
									sw.WriteLine("\t\t<word name='"+key+"'>"+key+"</word>");
								}
							}
							else
							{
								sw.WriteLine("\t\t<word name='hello'>hello</word>");
							}
							sw.WriteLine("\t</language>");
							sw.WriteLine("</root>");
							sw.Close();
						}						
						UpdateReferences();
					}
					catch(Exception ex)
					{
						Debug.LogError (ex);	
						return;
					}
				}
				return;
			}
			if(ImgButton(new Rect(window.width-padding*3-imgButtonW*2, yLocation, imgButtonW, buttonH), 
				"page_edit", "Edit"))
			{
				try
				{
					string newFullFilename = GetLangFilename(textboxFile);
					string oldFullFilename = GetLangFilename(selectedFile);
					if(File.Exists(oldFullFilename) && !oldFullFilename.Equals (newFullFilename))
					{
						translator.LoadDictionary(selectedCode);
						File.Copy(oldFullFilename, newFullFilename, true);
						File.Delete (oldFullFilename);
						
						translator.LangCulture = textboxCulture;
						translator.LangName = textboxFile;
						translator.LangFile = newFullFilename;
						translator.SaveDictionary();
						
						translator.RemoveLanguageReference(selectedCode);
						translator.AddLanguageReference(textboxCulture, textboxFile);
						UpdateReferences();
					}
				}
				catch(Exception ex)
				{
					Debug.LogError (ex);	
				}
				return;
			}
			if(ImgButton(new Rect(window.width-padding*2-imgButtonW, yLocation, imgButtonW, buttonH), 
				"page_delete", "Remove") &&
				EditorUtility.DisplayDialog("Delete "+textboxFile, 
					"Do you want to delete the language '"+textboxFile+" ("+textboxCulture+")' permanently?", "yes", "no")
				)
			{
				try
				{
					string langFullFilename = GetLangFilename(textboxFile);
					if(File.Exists(langFullFilename))
					{
						File.Delete(langFullFilename);
					}
				}
				catch(Exception ex)
				{
					Debug.LogError (ex);	
				}
				translator.RemoveLanguageReference(textboxCulture);
				UpdateReferences();
				return;
			}
		}
	}
	
	void SyncButton()
	{
		GUI.SetNextControlName("ButtonSync");
		if(GUI.Button(new Rect(padding, window.height-buttonH-padding, buttonW, buttonH), "Sync Keys"))
		{
			if(EditorUtility.DisplayDialog("Sync Keys ", "Do you want to synchronize the keys from all languages?", "yes", "no"))
			{
				int total = 0;
				int counter = 0;
				Translator t = new Translator(true);
				var langs = t.GetCodeAvailableLanguages();
				
				List<string> keys = GetKeys(t);
				total = keys.Count;
				
				foreach(string lang in langs)
				{
					if(Sync(t, lang, keys))
					{
						counter++;
					}
				}
				
				EditorUtility.DisplayDialog("Sync Keys", "Languages synchronized: "+counter+"/"+total, "OK");
				
				AssetDatabase.Refresh();
			}
		}
		
		GUI.SetNextControlName("ButtonRefresh");
		if(GUI.Button(new Rect(padding*2+buttonW, window.height-buttonH-padding, buttonW, buttonH), "Refresh"))
		{
			UpdateReferences();
			EditorUtility.DisplayDialog("Refresh", "Success", "OK");
		}
	}
	
	void FeedbackButton()
	{		
		GUI.SetNextControlName("ButtonFeedback");
		if(GUI.Button(new Rect(window.width-buttonW-padding, padding, buttonW, buttonH), "Feedback"))
		{
			Application.OpenURL("http://www.assetrepository.com/Home/Contact");
			//GUI.FocusControl("ButtonFeedback");
			//menu = 2;
		}
	}
	#endregion
	
	#region Second Menu GUI Methods
	void MenuLanguage()
	{
		SetTitle("Language Configuration");
		Box ();
		LngContent();
		LngToolbar();
		
		switch(BottomButtons())
		{
			// Save
			case 0:
			{
				translator.LangTitle = newTitle;
				SaveLanguage();
				AssetDatabase.Refresh ();
				saved = true;
				Debug.Log ("Saved");
			}
			break;
			
			// Reload
			case 1:
			{
				LoadLanguage(selectedCode);
				saved = true;
				Debug.Log ("Loaded");
			}
			break;
			
			// Cancel
			case 2:
			break;
			
			// Nothing
			default:
			break;
		}
	}
	
	void LngToolbar()
	{
		newTitle = EditorGUI.TextField(
			new Rect(padding*2, toolbarLocation, fieldWidth, padding*2), 
			"Title", newTitle);
	}
	
	void LngContent()
	{
		// Some help
		int iH = itemH + padding;
		int textFieldW = padding*30;
		int xLocation = padding*2;
		int yLocation = padding*6+buttonH;
		int bottom = buttonH+padding*3+yLocation;
		float contentWidth = window.width - xLocation*2;
		float contentHeight = window.height-bottom-iH-padding;
		var wordKeys = translator.Words.GetKeys();
		string key, val;
		
		// Content box
		GUI.Box(new Rect(xLocation, yLocation, window.width - xLocation*2, contentHeight), "");
		
		scrollPosition = GUI.BeginScrollView(
			new Rect(xLocation, yLocation, contentWidth, contentHeight),
			scrollPosition,
			//new Rect(0, 0, contentWidth, window.height));
			new Rect(0, 0, contentWidth, padding*2+wordKeys.Length*(itemH+padding*2)));
		
		for(int i = 0; i < wordKeys.Length;i++)
		{
			key = wordKeys[i];
			val = translator.Words.Get (key);
			
			// Item box
			GUI.Box(
				new Rect(
					padding, 
					padding*(i+1) + i*iH, 
					contentWidth - padding*2 ,
					iH), "");
			
			// Key field
			string newKey = EditorGUI.TextField(
				new Rect(
					padding*2, 
					padding/2 + padding*(i+1) + (i*iH), 
					textFieldW,
					itemH), (i+1).ToString (), key);
			
			// Value field
			string newVal = EditorGUI.TextField(
				new Rect(
					padding*44, 
					padding/2 + padding*(i+1) + (i*iH), 
					contentWidth - padding*50,
					itemH), "", val);
			
			// Both
			if(!newKey.Equals (key))
			{
				translator.Words.Remove(key);
				translator.Words.Set(newKey, val);
				key = newKey ;
			}
			if(!newVal.Equals (val))
			{
				val = newVal;
				translator.Words.Set (key, val);
			}
			
			// Delete button
			int bW = padding*3;
			if(ImgButton ( new Rect(contentWidth - padding*2 - bW, 
						padding/2 + padding*(i+1) + i*iH,
						bW, 
						itemH), 
				"delete", "Delete"))
			{
				if(EditorUtility.DisplayDialog("Delete "+key, 
					"Do you want to delete the word '"+key+
					"' permanently?", "yes", "no")
				)
				{
					DeleteWord(key);
					saved = false;
					break;
				}
			}
		}
		
		GUI.EndScrollView ();
		
		// Add box
		GUI.Box(new Rect(xLocation, contentHeight+padding+yLocation, contentWidth, iH), "");
		int itemsY = (int)contentHeight+padding*2+yLocation;
		
		addKeyText = EditorGUI.TextField(new Rect(xLocation, itemsY, textFieldW, iH-padding*2), "Key", addKeyText);
		addValText = EditorGUI.TextField(new Rect(xLocation+textFieldW+padding, itemsY, contentWidth - textFieldW - padding*10, iH-padding*2), "Value", 
			addValText);
		
		GUI.SetNextControlName("addButton");
		if(ImgButton (new Rect(xLocation+contentWidth-buttonW-padding, itemsY, buttonW, buttonH), "add", "Add"))
		{
			GUI.FocusControl("addButton");
			translator.Words.Set (addKeyText, addValText);
			saved = false;
			addKeyText = "";
			addValText = "";
		}
		
	}
	
	int BottomButtons()
	{
		// Left
		if(!saved)
		{
			GUI.Label(new Rect(buttonW*2+padding*3, window.height-buttonH-padding, buttonW*3, buttonH), "* Please save the changes.");
		}
		
		GUI.SetNextControlName("ButtonSave");
		if(GUI.Button(new Rect(padding, window.height-buttonH-padding, buttonW, buttonH), "Save"))
		{
			GUI.FocusControl("ButtonSave");
			return 0;
		}
		
		GUI.SetNextControlName("ButtonReset");
		if(GUI.Button(new Rect(buttonW+padding*2, window.height-buttonH-padding, buttonW, buttonH), "Reload"))
		{
			GUI.FocusControl("ButtonReset");
			return 1;
		}
		
		// Right
		GUI.SetNextControlName("ButtonCancel");
		if(BottomButton("Back"))
		{
			GUI.FocusControl("ButtonCancel");
			menu = 0;
			saved = true;
			return 2;
		}
		return -1;
	}	
	
	#endregion

	#region General GUI Methods

	bool ImgButton(Rect rect, string iconName, string altText)
	{
		Texture2D icon;
		try
		{
			icon = Icons.Get (iconName);
		}
		catch
		{
			icon = null;	
		}
		
		if(icon == null)
			return GUI.Button(rect, altText);
		else
			return GUI.Button(rect, icon);
	}
	
	void Box ()
	{
		GUI.Box(new Rect(padding, padding*4, window.width-padding*2, window.height-padding*6-buttonH),"");
	}
	
	bool BottomButton(string text)
	{
		return BottomButton(text, 0);
	}
	
	bool BottomButton(string text, int offset)
	{
		if(GUI.Button(new Rect(window.width-buttonW-padding+offset, window.height-buttonH-padding, buttonW, buttonH), text))
		{
			return true;
		}
		else
		{
			return false;	
		}
	}
	
	void SetTitle(string title)
	{
		GUI.Label(new Rect(padding, padding, window.width-padding*2, padding*2), title);
	}
	
	#endregion
	
	#region Methods
		
	List<string> GetKeys(Translator t)
	{
		List<string> keys = new List<string>();
		var langs = t.GetCodeAvailableLanguages();
		
		foreach(string lang in langs)
		{
			t.LoadDictionary(lang);
			foreach(var key in t.Words.GetKeys())
			{
				if(!string.IsNullOrEmpty(key) && !keys.Contains(key))
				{
					keys.Add (key);	
				}
			}
		}
		
		return keys;
	}
	
	bool Sync(Translator t, string lang, List<string> keys)
	{
		bool save = false;
		//Translator t = new Translator();
		t.LoadDictionary(lang);
		foreach(string key in keys)
		{
			if(!t.Words.ContainsKey(key))
			{
				save = true;
				t.Words.Set(key, string.Empty);
			}
		}
		
		if(save)
		{
			t.LangFile = GetLangFilename(t);
			t.SaveDictionary();
		}
		return save;
	}
	
	string GetLangFilename()
	{
		return GetLangFilename(translator);
	}
	
	string GetLangFilename(Translator translator)
	{
		return MakePath(GetLangName() + ".xml");
	}
	
	string GetLangFilename(string langName)
	{
		return MakePath(langName + ".xml");
	}
	
	string GetLangName()
	{
		return translator.Languages.Get (translator.LangCulture);
	}
	
	/// <summary>
	/// Updates the references from Asset Database and translator.cfg.xml.
	/// </summary>
	static void UpdateReferences ()
	{
		try
		{
			using(StreamWriter sw = new StreamWriter(MakePath("translator.cfg.xml"), false, Encoding.UTF8))
			{
				sw.WriteLine("<?xml version='1.0' encoding='utf-8' ?>");
				sw.WriteLine("<root>");
				sw.WriteLine("\t<!-- Document format -->");
				sw.WriteLine("\t<spec-version>1</spec-version>");
				sw.WriteLine("\t<!-- References -->");
				sw.WriteLine("\t<languages>");
				for(int i = 0; i < translator.GetAvailableLanguages ().Length; i++)
				{
					string pair = translator.GetLanguageDataFromIndex (i);
					string[] bananasplit = Translation.Editor.Common.StringHelper.SplitAndTrim(pair, ',');
					sw.Write ("\t\t");
					sw.WriteLine (string.Format("<language file='{0}' code='{1}' />", 
						bananasplit[0], bananasplit[1]));
				}
				sw.WriteLine("\t</languages>");
				sw.WriteLine("</root>");
				sw.Close();
			}
			
			AssetDatabase.Refresh ();
		}
		catch(Exception ex)
		{
			Debug.LogError (ex);
		}
	}
	
	void LoadLanguage(string culture)
	{
		translator.LoadDictionary(culture);
		newTitle = translator.LangTitle;
	}
	
	void SaveLanguage()
	{
		translator.LangFile = GetLangFilename();
		translator.SaveDictionary();
	}
	
	static void Install()
	{
		string folder = MakePath("");
		string file = MakePath("translator.cfg.xml");
		
		if(!Directory.Exists (folder))
		{
			Directory.CreateDirectory (folder);
		}
		
		if(!File.Exists (file))
		{
			UpdateReferences();
		}
	}
	
	static string MakePath(string file)
	{
		return Application.dataPath+"/Localization System/Resources/"+file;
	}
	
	/// <summary>
	/// Makes the code and path from a text.
	/// </summary>
	/// <param name='text'>
	/// Text. Format: file,code
	/// </param>
	/// <param name='code'>
	/// Code.
	/// </param>
	/// <param name='filename'>
	/// Filename.
	/// </param>
	void MakeCodeAndPath(string text, out string code, out string filename)
	{
		try
		{
			if(text.Contains (","))
			{
				var bananasplit = Translation.Editor.Common.StringHelper.SplitAndTrim(text, ',');
				filename = MakePath(bananasplit[0]+".xml");
				if(bananasplit.Length>1)
					code = bananasplit[1];
				else
					code = "";
			}
			else
			{
				code = "";
				filename = "";
			}
		}
		catch
		{
			code = "";
			filename = "";
		}
	}
	
	void DeleteWord(string key)
	{
		translator.Words.Remove(key);
	}
	
	#endregion
}

#endif