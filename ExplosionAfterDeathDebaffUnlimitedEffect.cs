﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionAfterDeathDebaffUnlimitedEffect : ExplosionAfterDeathDebaffEffect
{
    public ExplosionAfterDeathDebaffUnlimitedEffect(float Chance, float Range, float Dmg, float Time) : base(Chance, Range, Dmg, Time)
    {
    }
}
